package com.packt.cardatabase.domain;

import javax.persistence.*;

@Entity
public class WeeklyProjectLevelTestExecutionProgress {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private String numberOfInProjectAutomationTestScriptsExecuted;
    private String numberOfAPIAutomationTestScriptsExecuted;
    private String numberOfTestCasesExecutedWithMigratedData;
    private String numberOfModulesNotDelivered;
    private String totalNumberOfManualTestCasesNotExecuted;
    private String totalNumberOfAutomationTestCasesNotExecuted;
    private String numberOfTestCasesSuccessfullyExecutedWithoutDefects;
    private String numberOfDefectsReportedByTestingTeam;
    private String numberOfDefectsRejected;
    private String numberOfDefectsFoundUsingManualRegression;
    private String numberOfDefectsFoundUsingAutomatedRegression;
    private String numberOfValidDefectsLoggedInSIT;
    private String numberOfValidDefectsUAT;
    private String totalNumberOfDefectsInProduction;
    private String totalNumberOfDefectsFixed;
    private String totalNumberOfDefectsReopened;
    private String averageTimeTakenToResolveSev1Defects;
    private String environmentDowntimeDuringTestExecutionPerPersonInHours;
    private String manualTestCaseExecutionEffort;
    private String regressionAutomationTestCaseExecutionEffort;
    private String inProjectAutomationTestCaseExecutionEffort;
    private String apiAutomationTestCaseExecutionEffort;
    private String numberOfResourcesForManualTestCaseExecution;
    private String numberOfResourceForRegressionAutomationExecution;
    private String numberOfResourceForInProjectAutomationExecution;
    private String numberOfResourceForAPIAutomationExecution;

    private String manualTestCaseExecutionEffortStartDate;
    private String regressionAutomationTestCaseExecutionEffortStartDate;
    private String inProjectAutomationTestCaseExecutionEffortStartDate;
    private String apiAutomationTestCaseExecutionEffortStartDate;
    private String manualTestCaseExecutionEffortEndDate;
    private String regressionAutomationTestCaseExecutionEffortEndDate;
    private String inProjectAutomationTestCaseExecutionEffortEndDate;
    private String apiAutomationTestCaseExecutionEffortEndDate;

    private int weekNumber;
    private int monthNumber;
    private int yearNumber;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;

    public int getWeekNumber() {
        return weekNumber;
    }

    public String getWeekNumberName() {
        return "Week";
    }
    public void setWeekNumber(int weekNumber) {
        this.weekNumber = weekNumber;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }



    public WeeklyProjectLevelTestExecutionProgress() {
    }

    public WeeklyProjectLevelTestExecutionProgress(String numberOfInProjectAutomationTestScriptsExecuted,
                                                   String numberOfAPIAutomationTestScriptsExecuted,
                                                   String numberOfTestCasesExecutedWithMigratedData,
                                                   String numberOfModulesNotDelivered,
                                                   String totalNumberOfManualTestCasesNotExecuted,
                                                   String totalNumberOfAutomationTestCasesNotExecuted,
                                                   String numberOfTestCasesSuccessfullyExecutedWithoutDefects,
                                                   String numberOfDefectsReportedByTestingTeam,
                                                   String numberOfDefectsRejected,
                                                   String numberOfValidDefectsLoggedInSIT,
                                                   String numberOfValidDefectsUAT,
                                                   String totalNumberOfDefectsFixed,
                                                   String totalNumberOfDefectsReopened,
                                                   String averageTimeTakenToResolveSev1Defects,
                                                   String environmentDowntimeDuringTestExecutionPerPersonInHours,
                                                   String manualTestCaseExecutionEffort,
                                                   String regressionAutomationTestCaseExecutionEffort,
                                                   String inProjectAutomationTestCaseExecutionEffort,
                                                   String apiAutomationTestCaseExecutionEffort,
                                                   String numberOfResourcesForManualTestCaseExecution,
                                                   String numberOfResourceForRegressionAutomationExecution,
                                                   String numberOfResourceForInProjectAutomationExecution,
                                                   String numberOfResourceForAPIAutomationExecution,
                                                   String totalNumberOfDefectsInProduction,
                                                   String numberOfDefectsFoundUsingManualRegression,
                                                   String numberOfDefectsFoundUsingAutomatedRegression,
                                                   int weekNumber,
                                                   int monthNumber,
                                                   int yearNumber,
                                                   Project project) {

        super();

        this.numberOfInProjectAutomationTestScriptsExecuted = numberOfInProjectAutomationTestScriptsExecuted;
        this.numberOfAPIAutomationTestScriptsExecuted = numberOfAPIAutomationTestScriptsExecuted;
        this.numberOfTestCasesExecutedWithMigratedData = numberOfTestCasesExecutedWithMigratedData;
        this.numberOfModulesNotDelivered = numberOfModulesNotDelivered;
        this.totalNumberOfManualTestCasesNotExecuted = totalNumberOfManualTestCasesNotExecuted;
        this.totalNumberOfAutomationTestCasesNotExecuted = totalNumberOfAutomationTestCasesNotExecuted;
        this.numberOfTestCasesSuccessfullyExecutedWithoutDefects = numberOfTestCasesSuccessfullyExecutedWithoutDefects;
        this.numberOfDefectsReportedByTestingTeam = numberOfDefectsReportedByTestingTeam;
        this.numberOfDefectsRejected = numberOfDefectsRejected;
        this.numberOfValidDefectsLoggedInSIT = numberOfValidDefectsLoggedInSIT;
        this.numberOfValidDefectsUAT = numberOfValidDefectsUAT;
        this.totalNumberOfDefectsFixed = totalNumberOfDefectsFixed;
        this.totalNumberOfDefectsReopened = totalNumberOfDefectsReopened;
        this.averageTimeTakenToResolveSev1Defects = averageTimeTakenToResolveSev1Defects;
        this.environmentDowntimeDuringTestExecutionPerPersonInHours = environmentDowntimeDuringTestExecutionPerPersonInHours;
        this.manualTestCaseExecutionEffort = manualTestCaseExecutionEffort;
        this.regressionAutomationTestCaseExecutionEffort = regressionAutomationTestCaseExecutionEffort;
        this.inProjectAutomationTestCaseExecutionEffort = inProjectAutomationTestCaseExecutionEffort;
        this.apiAutomationTestCaseExecutionEffort = apiAutomationTestCaseExecutionEffort;
        this.numberOfResourcesForManualTestCaseExecution = numberOfResourcesForManualTestCaseExecution;
        this.numberOfResourceForRegressionAutomationExecution = numberOfResourceForRegressionAutomationExecution;
        this.numberOfResourceForInProjectAutomationExecution = numberOfResourceForInProjectAutomationExecution;
        this.numberOfResourceForAPIAutomationExecution = numberOfResourceForAPIAutomationExecution;
        this.totalNumberOfDefectsInProduction = totalNumberOfDefectsInProduction;
        this.numberOfDefectsFoundUsingManualRegression = numberOfDefectsFoundUsingManualRegression;
        this.numberOfDefectsFoundUsingAutomatedRegression = numberOfDefectsFoundUsingAutomatedRegression;
        this.weekNumber = weekNumber;
        this.monthNumber = monthNumber;
        this.yearNumber = yearNumber;
        this.project = project;
    }

    public int getMonthNumber() {
        return monthNumber;
    }
    public String getMonthNumberName() {
        return "Month";
    }
    public void setMonthNumber(int monthNumber) {
        this.monthNumber = monthNumber;
    }


    public int getYearNumber() {
        return yearNumber;
    }
    public String getYearNumberName() {
        return "Year";
    }
    public void setYearNumber(int yearNumber) {
        this.yearNumber = yearNumber;
    }


    public int getNumberOfInProjectAutomationTestScriptsExecuted() {

        if(numberOfInProjectAutomationTestScriptsExecuted == null)
        {
            return 0;
        }

        return Integer.parseInt(numberOfInProjectAutomationTestScriptsExecuted);
    }

    public static String getNumberOfInProjectAutomationTestScriptsExecutedName() {
        return "Number Of InProject Automation Test Scripts Executed";
    }

    public void setNumberOfInProjectAutomationTestScriptsExecuted(String numberOfInProjectAutomationTestScriptsExecuted) {
        this.numberOfInProjectAutomationTestScriptsExecuted = numberOfInProjectAutomationTestScriptsExecuted;
    }

    public int getNumberOfAPIAutomationTestScriptsExecuted() {
        if(numberOfAPIAutomationTestScriptsExecuted == null)
        {
            return 0;
        }

        return Integer.parseInt(numberOfAPIAutomationTestScriptsExecuted);
    }

    public static String getNumberOfAPIAutomationTestScriptsExecutedName() {
        return "Number Of API Automation Test Scripts Executed";
    }

    public void setNumberOfAPIAutomationTestScriptsExecuted(String numberOfAPIAutomationTestScriptsExecuted) {
        this.numberOfAPIAutomationTestScriptsExecuted = numberOfAPIAutomationTestScriptsExecuted;
    }

    public int getNumberOfTestCasesExecutedWithMigratedData() {
        if(numberOfTestCasesExecutedWithMigratedData == null)
        {
            return 0;
        }

        return Integer.parseInt(numberOfTestCasesExecutedWithMigratedData);
    }

    public static String getNumberOfTestCasesExecutedWithMigratedDataName() {
        return "Number Of TestCases Executed With Migrated Data";
    }

    public void setNumberOfTestCasesExecutedWithMigratedData(String numberOfTestCasesExecutedWithMigratedData) {
        this.numberOfTestCasesExecutedWithMigratedData = numberOfTestCasesExecutedWithMigratedData;
    }



    public int getNumberOfModulesNotDelivered() {

        if(numberOfModulesNotDelivered == null)
        {
            return 0;
        }

        return Integer.parseInt(numberOfModulesNotDelivered);

    }

    public static String getNumberOfModulesNotDeliveredName() {
        return "Number Of Modules Not Delivered";
    }

    public void setNumberOfModulesNotDelivered(String numberOfModulesNotDelivered) {
        this.numberOfModulesNotDelivered = numberOfModulesNotDelivered;
    }


    public int getTotalNumberOfManualTestCasesNotExecuted() {
        if(totalNumberOfManualTestCasesNotExecuted == null)
        {
            return 0;
        }

        return Integer.parseInt(totalNumberOfManualTestCasesNotExecuted);

    }

    public static String getTotalNumberOfManualTestCasesNotExecutedName() {
        return "Total Number Of Manual TestCases Not Executed";
    }

    public void setTotalNumberOfManualTestCasesNotExecuted(String totalNumberOfManualTestCasesNotExecuted) {
        this.totalNumberOfManualTestCasesNotExecuted = totalNumberOfManualTestCasesNotExecuted;
    }

    public int getTotalNumberOfAutomationTestCasesNotExecuted() {
        if(totalNumberOfAutomationTestCasesNotExecuted == null)
        {
            return 0;
        }

        return Integer.parseInt(totalNumberOfAutomationTestCasesNotExecuted);

    }

    public static String getTotalNumberOfAutomationTestCasesNotExecutedName() {
        return "Total Number Of Automation TestCases Not Executed";
    }

    public void setTotalNumberOfAutomationTestCasesNotExecuted(String totalNumberOfAutomationTestCasesNotExecuted) {
        this.totalNumberOfAutomationTestCasesNotExecuted = totalNumberOfAutomationTestCasesNotExecuted;
    }

    public int getNumberOfTestCasesSuccessfullyExecutedWithoutDefects() {

        if(numberOfTestCasesSuccessfullyExecutedWithoutDefects == null)
        {
            return 0;
        }

        return Integer.parseInt(numberOfTestCasesSuccessfullyExecutedWithoutDefects);

    }

    public static String getNumberOfTestCasesSuccessfullyExecutedWithoutDefectsName() {
        return "Number Of TestCases Successfully Executed Without Defects";
    }

    public void setNumberOfTestCasesSuccessfullyExecutedWithoutDefects(String numberOfTestCasesSuccessfullyExecutedWithoutDefects) {
        this.numberOfTestCasesSuccessfullyExecutedWithoutDefects = numberOfTestCasesSuccessfullyExecutedWithoutDefects;
    }

    public int getNumberOfDefectsReportedByTestingTeam() {

        if(numberOfDefectsReportedByTestingTeam == null)
        {
            return 0;
        }

        return Integer.parseInt(numberOfDefectsReportedByTestingTeam);


    }

    public static String getNumberOfDefectsReportedByTestingTeamName() {
        return "Number Of Defects Reported By Testing Team";
    }

    public void setNumberOfDefectsReportedByTestingTeam(String numberOfDefectsReportedByTestingTeam) {
        this.numberOfDefectsReportedByTestingTeam = numberOfDefectsReportedByTestingTeam;
    }

    public int getNumberOfDefectsRejected() {

        if(numberOfDefectsRejected == null)
        {
            return 0;
        }

        return Integer.parseInt(numberOfDefectsRejected);

    }

    public static String getNumberOfDefectsRejectedName() {
        return "Number Of Defects Rejected";
    }

    public void setNumberOfDefectsRejected(String numberOfDefectsRejected) {
        this.numberOfDefectsRejected = numberOfDefectsRejected;
    }

    public int getNumberOfValidDefectsLoggedInSIT() {

        if(numberOfValidDefectsLoggedInSIT == null)
        {
            return 0;
        }

        return Integer.parseInt(numberOfValidDefectsLoggedInSIT);

    }

    public static String getNumberOfValidDefectsLoggedInSITName() {
        return "Number Of Valid Defects Logged In SIT";
    }

    public void setNumberOfValidDefectsLoggedInSIT(String numberOfValidDefectsLoggedInSIT) {
        this.numberOfValidDefectsLoggedInSIT = numberOfValidDefectsLoggedInSIT;
    }

    public int getNumberOfValidDefectsLoggedInUAT() {

        if(numberOfValidDefectsUAT == null)
        {
            return 0;
        }

        return Integer.parseInt(numberOfValidDefectsUAT);

    }


    public static String getNumberOfValidDefectsLoggedInUATName() {
        return "Number Of Valid Defects Logged In UAT";
    }


    public void setNumberOfValidDefectsLoggedInUAT(String numberOfValidDefectsUAT) {
        this.numberOfValidDefectsUAT = numberOfValidDefectsUAT;
    }

    public int getTotalNumberOfDefectsFixed() {

        if(totalNumberOfDefectsFixed == null)
        {
            return 0;
        }

        return Integer.parseInt(totalNumberOfDefectsFixed);

    }

    public static String getTotalNumberOfDefectsFixedName() {
        return "Total Number Of Defects Fixed";
    }

    public void setTotalNumberOfDefectsFixed(String totalNumberOfDefectsFixed) {
        this.totalNumberOfDefectsFixed = totalNumberOfDefectsFixed;
    }

    public int getTotalNumberOfDefectsReopened() {

        if(totalNumberOfDefectsReopened == null)
        {
            return 0;
        }

        return Integer.parseInt(totalNumberOfDefectsReopened);

    }

    public static String getTotalNumberOfDefectsReopenedName() {
        return "Total Number Of Defects Reopened";
    }

    public void setTotalNumberOfDefectsReopened(String totalNumberOfDefectsReopened) {
        this.totalNumberOfDefectsReopened = totalNumberOfDefectsReopened;
    }

    public int getAverageTimeTakenToResolveSev1Defects() {

        if(averageTimeTakenToResolveSev1Defects == null)
        {
            return 0;
        }

        return Integer.parseInt(averageTimeTakenToResolveSev1Defects);

    }

    public static String getAverageTimeTakenToResolveSev1DefectsName() {
        return "Average Time Taken To Resolve Sev1 Defects";
    }

    public void setAverageTimTakenToResolveSev1Defects(String averageTimeTakenToResolveSev1Defects) {
        this.averageTimeTakenToResolveSev1Defects = averageTimeTakenToResolveSev1Defects;
    }

    public int getEnvironmentDowntimeDuringTestExecutionPerPersonInHours() {

        if(environmentDowntimeDuringTestExecutionPerPersonInHours == null)
        {
            return 0;
        }

        return Integer.parseInt(environmentDowntimeDuringTestExecutionPerPersonInHours);

    }

    public static String getEnvironmentDowntimeDuringTestExecutionPerPersonInHoursName() {
        return "Environment Downtime During Test Execution Per Person In Hours";
    }

    public void setEnvironmentDowntimeDuringTestExecutionPerPersonInHours(String environmentDowntimeDuringTestExecutionPerPersonInHours) {
        this.environmentDowntimeDuringTestExecutionPerPersonInHours = environmentDowntimeDuringTestExecutionPerPersonInHours;
    }


    public int getManualTestCaseExecutionEffort() {

        if(manualTestCaseExecutionEffort == null)
        {
            return 0;
        }

        return Integer.parseInt(manualTestCaseExecutionEffort);

    }

    public static String getManualTestCaseExecutionEffortName() {
        return "Manual TestCase Execution Effort";
    }


    public void setManualTestCaseExecutionEffort(String manualTestCaseExecutionEffort) {
        this.manualTestCaseExecutionEffort = manualTestCaseExecutionEffort;
    }



    public int getRegressionAutomationTestCaseExecutionEffort() {

        if(regressionAutomationTestCaseExecutionEffort == null)
        {
            return 0;
        }

        return Integer.parseInt(regressionAutomationTestCaseExecutionEffort);

    }

    public static String getRegressionAutomationTestCaseExecutionEffortName() {
        return "Regression Automation TestCase Execution Effort";
    }

    public void setRegressionAutomationTestCaseExecutionEffort(String regressionAutomationTestCaseExecutionEffort) {
        this.regressionAutomationTestCaseExecutionEffort = regressionAutomationTestCaseExecutionEffort;
    }

    public int getInProjectAutomationTestCaseExecutionEffort() {

        if(inProjectAutomationTestCaseExecutionEffort == null)
        {
            return 0;
        }

        return Integer.parseInt(inProjectAutomationTestCaseExecutionEffort);

    }

    public static String getInProjectAutomationTestCaseExecutionEffortName() {
        return "InProject Automation TestCase Execution Effort";
    }


    public void setInProjectAutomationTestCaseExecutionEffort(String inProjectAutomationTestCaseExecutionEffort) {
        this.inProjectAutomationTestCaseExecutionEffort = inProjectAutomationTestCaseExecutionEffort;
    }

    public int getAPIAutomationTestCaseExecutionEffort() {

        if(apiAutomationTestCaseExecutionEffort == null)
        {
            return 0;
        }

        return Integer.parseInt(apiAutomationTestCaseExecutionEffort);

    }

    public static String getAPIAutomationTestCaseExecutionEffortName() {
        return "API Automation TestCase Execution Effort";
    }


    public void setAPIAutomationTestCaseExecutionEffort(String apiAutomationTestCaseExecutionEffort) {
        this.apiAutomationTestCaseExecutionEffort = apiAutomationTestCaseExecutionEffort;
    }

    public int getNumberOfResourcesForManualTestCaseExecution() {

        if(numberOfResourcesForManualTestCaseExecution == null)
        {
            return 0;
        }

        return Integer.parseInt(numberOfResourcesForManualTestCaseExecution);

    }

    public static String getNumberOfResourcesForManualTestCaseExecutionName() {
        return "Number Of Resources For Manual TestCase Execution";
    }

    public void setNumberOfResourcesForManualTestCaseExecution(String numberOfResourcesForManualTestCaseExecution) {
        this.numberOfResourcesForManualTestCaseExecution = numberOfResourcesForManualTestCaseExecution;
    }

    public int getNumberOfResourceForRegressionAutomationExecution() {

        if(numberOfResourceForRegressionAutomationExecution == null)
        {
            return 0;
        }

        return Integer.parseInt(numberOfResourceForRegressionAutomationExecution);

    }

    public static String getNumberOfResourceForRegressionAutomationExecutionName() {
        return "Number Of Resources For Regression Automation Execution";
    }

    public void setNumberOfResourceForRegressionAutomationExecution(String numberOfResourceForRegressionAutomationExecution) {
        this.numberOfResourceForRegressionAutomationExecution = numberOfResourceForRegressionAutomationExecution;
    }


    public int getNumberOfResourceForInProjectAutomationExecution() {

        if(numberOfResourceForInProjectAutomationExecution == null)
        {
            return 0;
        }

        return Integer.parseInt(numberOfResourceForInProjectAutomationExecution);

    }

    public static String getNumberOfResourceForInProjectAutomationExecutionName() {
        return "Number Of Resources For InProject Automation Execution";
    }

    public void setNumberOfResourceForInProjectAutomationExecution(String numberOfResourceForInProjectAutomationExecution) {
        this.numberOfResourceForInProjectAutomationExecution = numberOfResourceForInProjectAutomationExecution;
    }


    public int getNumberOfResourceForAPIAutomationExecution() {

        if(numberOfResourceForAPIAutomationExecution == null)
        {
            return 0;
        }

        return Integer.parseInt(numberOfResourceForAPIAutomationExecution);

    }

    public static String getNumberOfResourceForAPIAutomationExecutionName() {
        return "Number Of Resources For API Automation Execution";
    }

    public void setNumberOfResourceForAPIAutomationExecution(String numberOfResourceForAPIAutomationExecution) {
        this.numberOfResourceForAPIAutomationExecution = numberOfResourceForAPIAutomationExecution;
    }


    public int getTotalNumberOfDefectsInProduction() {

        if(totalNumberOfDefectsInProduction == null)
        {
            return 0;
        }

        return Integer.parseInt(totalNumberOfDefectsInProduction);

    }

    public static String getTotalNumberOfDefectsInProductionName() {

        return "Total Number Of Defects In Production";
    }

    public void setTotalNumberOfDefectsInProduction(String totalNumberOfDefectsInProduction) {
        this.totalNumberOfDefectsInProduction = totalNumberOfDefectsInProduction;
    }

    public int getNumberOfDefectsFoundUsingAutomatedRegression() {

        if(numberOfDefectsFoundUsingAutomatedRegression == null)
        {
            return 0;
        }

        return Integer.parseInt(numberOfDefectsFoundUsingAutomatedRegression);

    }

    public static String getNumberOfDefectsFoundUsingAutomatedRegressionName() {

        return "Number Of Defects Found Using Automated Regression";
    }

    public void setNumberOfDefectsFoundUsingAutomatedRegression(String numberOfDefectsFoundUsingAutomatedRegression) {
        this.numberOfDefectsFoundUsingAutomatedRegression = numberOfDefectsFoundUsingAutomatedRegression;
    }

    public int getNumberOfDefectsFoundUsingManualRegression() {

        if(numberOfDefectsFoundUsingManualRegression == null)
        {
            return 0;
        }

        return Integer.parseInt(numberOfDefectsFoundUsingManualRegression);

    }

    public static String getNumberOfDefectsFoundUsingManualRegressionName() {

        return "Number Of Defects Found Using Manual Regression";
    }

    public void setNumberOfDefectsFoundUsingManualRegression(String numberOfDefectsFoundUsingManualRegression) {
        this.numberOfDefectsFoundUsingManualRegression = numberOfDefectsFoundUsingManualRegression;
    }



    public String getNumberOfValidDefectsUAT() {
        return numberOfValidDefectsUAT;
    }

    public void setNumberOfValidDefectsUAT(String numberOfValidDefectsUAT) {
        this.numberOfValidDefectsUAT = numberOfValidDefectsUAT;
    }

    public void setAverageTimeTakenToResolveSev1Defects(String averageTimeTakenToResolveSev1Defects) {
        this.averageTimeTakenToResolveSev1Defects = averageTimeTakenToResolveSev1Defects;
    }

    public String getApiAutomationTestCaseExecutionEffort() {
        return apiAutomationTestCaseExecutionEffort;
    }

    public void setApiAutomationTestCaseExecutionEffort(String apiAutomationTestCaseExecutionEffort) {
        this.apiAutomationTestCaseExecutionEffort = apiAutomationTestCaseExecutionEffort;
    }

    public String getManualTestCaseExecutionEffortStartDate() {
        if(manualTestCaseExecutionEffortStartDate == null)
            return "";
        else
            return manualTestCaseExecutionEffortStartDate;
    }

    public void setManualTestCaseExecutionEffortStartDate(String manualTestCaseExecutionEffortStartDate) {
        this.manualTestCaseExecutionEffortStartDate = manualTestCaseExecutionEffortStartDate;
    }

    public String getRegressionAutomationTestCaseExecutionEffortStartDate() {
        if(regressionAutomationTestCaseExecutionEffortStartDate == null)
            return "";
        else
            return regressionAutomationTestCaseExecutionEffortStartDate;
    }

    public void setRegressionAutomationTestCaseExecutionEffortStartDate(String regressionAutomationTestCaseExecutionEffortStartDate) {
        this.regressionAutomationTestCaseExecutionEffortStartDate = regressionAutomationTestCaseExecutionEffortStartDate;
    }

    public String getInProjectAutomationTestCaseExecutionEffortStartDate() {
        if(inProjectAutomationTestCaseExecutionEffortStartDate == null)
            return "";
        else
            return inProjectAutomationTestCaseExecutionEffortStartDate;
    }

    public void setInProjectAutomationTestCaseExecutionEffortStartDate(String inProjectAutomationTestCaseExecutionEffortStartDate) {
        this.inProjectAutomationTestCaseExecutionEffortStartDate = inProjectAutomationTestCaseExecutionEffortStartDate;
    }

    public String getApiAutomationTestCaseExecutionEffortStartDate() {
        if(apiAutomationTestCaseExecutionEffortStartDate == null)
            return "";
        else
            return apiAutomationTestCaseExecutionEffortStartDate;
    }

    public void setApiAutomationTestCaseExecutionEffortStartDate(String apiAutomationTestCaseExecutionEffortStartDate) {
        this.apiAutomationTestCaseExecutionEffortStartDate = apiAutomationTestCaseExecutionEffortStartDate;
    }

    public String getManualTestCaseExecutionEffortEndDate() {
        if(manualTestCaseExecutionEffortEndDate == null)
            return "";
        else
            return manualTestCaseExecutionEffortEndDate;
    }

    public void setManualTestCaseExecutionEffortEndDate(String manualTestCaseExecutionEffortEndDate) {
        this.manualTestCaseExecutionEffortEndDate = manualTestCaseExecutionEffortEndDate;
    }

    public String getRegressionAutomationTestCaseExecutionEffortEndDate() {
        if(regressionAutomationTestCaseExecutionEffortEndDate == null)
            return "";
        else
            return regressionAutomationTestCaseExecutionEffortEndDate;
    }

    public void setRegressionAutomationTestCaseExecutionEffortEndDate(String regressionAutomationTestCaseExecutionEffortEndDate) {
        this.regressionAutomationTestCaseExecutionEffortEndDate = regressionAutomationTestCaseExecutionEffortEndDate;
    }

    public String getInProjectAutomationTestCaseExecutionEffortEndDate() {
        if(inProjectAutomationTestCaseExecutionEffortEndDate == null)
            return "";
        else
            return inProjectAutomationTestCaseExecutionEffortEndDate;
    }

    public void setInProjectAutomationTestCaseExecutionEffortEndDate(String inProjectAutomationTestCaseExecutionEffortEndDate) {
        this.inProjectAutomationTestCaseExecutionEffortEndDate = inProjectAutomationTestCaseExecutionEffortEndDate;
    }

    public String getApiAutomationTestCaseExecutionEffortEndDate() {
        if(apiAutomationTestCaseExecutionEffortEndDate == null)
            return "";
        else
            return apiAutomationTestCaseExecutionEffortEndDate;
    }

    public void setApiAutomationTestCaseExecutionEffortEndDate(String apiAutomationTestCaseExecutionEffortEndDate) {
        this.apiAutomationTestCaseExecutionEffortEndDate = apiAutomationTestCaseExecutionEffortEndDate;
    }



    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }



}