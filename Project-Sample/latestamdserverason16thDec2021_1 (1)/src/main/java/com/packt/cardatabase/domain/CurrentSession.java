package com.packt.cardatabase.domain;


import javax.persistence.*;


@Entity
public class CurrentSession {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private int projectId;


    public CurrentSession() {}

    public CurrentSession(int projectId) {
        super();
        this.projectId = projectId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

}