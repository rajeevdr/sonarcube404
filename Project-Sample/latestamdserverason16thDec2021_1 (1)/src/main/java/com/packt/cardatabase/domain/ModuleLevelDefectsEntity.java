package com.packt.cardatabase.domain;

import javax.persistence.*;
import java.util.ArrayList;

@Entity
public class ModuleLevelDefectsEntity {
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private long id;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "project")
	private Project project;


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	private String defectModuleFieldName;
	private ArrayList<String> modules = null;


	public String getDefectModuleFieldName() {
		return defectModuleFieldName;
	}

	public void setDefectModuleFieldName(String defectModuleFieldName) {
		this.defectModuleFieldName = defectModuleFieldName;
	}

	public ArrayList<String> getModules() {
		return modules;
	}

	public void setModules(ArrayList<String> modules) {
		this.modules = modules;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

}
