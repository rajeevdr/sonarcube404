package com.packt.cardatabase.domain;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class MonthData {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private int monthNumber;
    private int yearNumber;
    private String monthStartDate;
    private String monthEndDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public MonthData() {}


    public MonthData(int monthNumber, int yearNumber, String monthStartDate, String monthEndDate) {
        super();
        this.monthNumber = monthNumber;
        this.yearNumber = yearNumber;
        this.monthStartDate = monthStartDate;
        this.monthEndDate = monthEndDate;
    }


    public int getMonthNumber() {
        return monthNumber;
    }

    public void setMonthNumber(int monthNumber) {
        this.monthNumber = monthNumber;
    }

    public int getYearNumber() {
        return yearNumber;
    }

    public void setYearNumber(int yearNumber) {
        this.yearNumber = yearNumber;
    }

    public String getMonthStartDate() {
        return monthStartDate;
    }

    public void setMonthStartDate(String weekStartDate) {
        this.monthStartDate = monthStartDate;
    }

    public String getMonthEndDate() {
        return monthEndDate;
    }

    public void setMonthEndDate(String weekEndDate) {
        this.monthEndDate = monthEndDate;
    }

}