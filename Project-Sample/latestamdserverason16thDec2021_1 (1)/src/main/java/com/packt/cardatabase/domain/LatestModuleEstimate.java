package com.packt.cardatabase.domain;


import javax.persistence.*;


@Entity
public class LatestModuleEstimate {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;
    private long originalModuleId;

    private String name;
    private String estimatedManualTestCases;
    private String estimatedRegressionAutomationTestCases;
    private String estimatedSanityAutomationTestCases;
    private String moduleTimeStamp;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;

    public LatestModuleEstimate() {}

    public LatestModuleEstimate(Long originalModuleId,
                                String moduleTimeStamp,
                                String name,
                                String estimatedManualTestCases,
                                String estimatedRegressionAutomationTestCases,
                                String estimatedSanityAutomationTestCases,
                                Project project) {
        super();
        this.originalModuleId = originalModuleId;
        this.moduleTimeStamp = moduleTimeStamp;
        this.name = name;
        this.estimatedManualTestCases = estimatedManualTestCases;
        this.estimatedRegressionAutomationTestCases = estimatedRegressionAutomationTestCases;
        this.estimatedSanityAutomationTestCases = estimatedSanityAutomationTestCases;
        this.project = project;
    }

    public LatestModuleEstimate(ModuleEstimate moduleEstimate) {
        super();
        this.originalModuleId = moduleEstimate.getOriginalModuleId();
        this.moduleTimeStamp = moduleEstimate.getModuleTimeStamp();
        this.name = moduleEstimate.getName();
        this.estimatedManualTestCases = moduleEstimate.getEstimatedManualTestCases();
        this.estimatedRegressionAutomationTestCases = moduleEstimate.getEstimatedRegressionAutomationTestCases();
        this.estimatedSanityAutomationTestCases = moduleEstimate.getEstimatedSanityAutomationTestCases();
        this.project = moduleEstimate.getProject();
    }

    public Long getOriginalModuleId() {
        return originalModuleId;
    }

    public void setOriginalModuleId(long originalModuleId) {
        this.originalModuleId = originalModuleId;
    }


    public String getModuleTimeStamp() {
        return moduleTimeStamp;
    }

    public void setModuleTimeStamp(String moduleTimeStamp) {
        this.moduleTimeStamp = moduleTimeStamp;
    }



    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEstimatedManualTestcases() {
        return estimatedManualTestCases;
    }

    public void setEstimatedManualTestCases(String estimatedManualTestcases) {
        this.estimatedManualTestCases = estimatedManualTestcases;
    }


    public String getEstimatedRegressionAutomationTestCases() {
        return estimatedRegressionAutomationTestCases;
    }

    public void setEstimatedRegressionAutomationTestCases(String estimatedRegressionAutomationTestCases) {
        this.estimatedRegressionAutomationTestCases = estimatedRegressionAutomationTestCases;
    }

    public String getEstimatedSanityAutomationTestCases() {
        return estimatedSanityAutomationTestCases;
    }

    public void setEstimatedSanityAutomationTestCases(String estimatedSanityAutomationTestCases) {
        this.estimatedSanityAutomationTestCases = estimatedSanityAutomationTestCases;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

}