package com.packt.cardatabase;

import com.google.gson.Gson;
import com.packt.cardatabase.domain.*;
import com.packt.cardatabase.domain.Module;

import com.packt.cardatabase.property.*;
import org.apache.http.HttpHost;
import org.elasticsearch.action.admin.indices.refresh.RefreshRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@SpringBootApplication
@EnableConfigurationProperties({
		FileStorageProperties.class
})
@EnableSwagger2
public class CardatabaseApplication {
	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private CurrentSessionRepository currentSessionRepository;

	@Autowired
	private ProjectEstimateRepository projectEstimateRepository;

	@Autowired
	private ModuleRepository moduleRepository;

	@Autowired
	private ModuleEstimateRepository moduleEstimateRepository;

	@Autowired
	private LatestModuleEstimateRepository latestModuleEstimateRepository;

	@Autowired
	private LatestProjectEstimateRepository latestProjectEstimateRepository;

	@Autowired
	private QuarterlyProgressRepository quarterlyProgressRepository;

	@Autowired
	private MonthlyTestDesignProgressRepository monthlyTestDesignProgressRepository;

	@Autowired
	private MonthlyTestExecutionProgressRepository monthlyTestExecutionProgressRepository;

	@Autowired
	private WeeklyProjectLevelTestDesignProgressRepository weeklyProjectLevelTestDesignProgressRepository;

	@Autowired
	private WeeklyModuleLevelTestDesignProgressRepository  weeklyModuleLevelTestDesignProgressRepository;

	@Autowired
	private WeeklyProjectLevelTestExecutionProgressRepository weeklyProjectLevelTestExecutionProgressRepository;

	@Autowired
	private WeeklyModuleLevelTestExecutionProgressRepository  weeklyModuleLevelTestExecutionProgressRepository;

	@Autowired
	private WeeklyMetricsRepo weeklyMetricsRepo;

	@Autowired
	private MonthlyMetricsRepository monthlyMetricsRepository;

	@Autowired
	private MonthlyGovernanceRepository monthlyGovernanceRepository;

	@Autowired
	private WeeklyProjectGovernanceRepository weeklyProjectGovernanceRepository;

	@Autowired
	private WeekDataRepository weekDataRepository;

	@Autowired
	private MonthDataRepository monthDataRepository;

	@Autowired
	private QuarterDataRepository quarterDataRepository;

	@Autowired
	private TestDesignTestExecutionProductivityRepository testDesignTestExecutionProductivityRepository;
	
	@Autowired
	private WeekMonthDataRepository weekMonthDataRepository;

	@Autowired
	private HeatMapSettingsRepository HeatMapSettingsRepository;

	@Autowired
	private UserRepository urepository;

	@Autowired
	private FirstProjectEstimateRepository firstProjectEstimateRepository;

	@Autowired
	private FirstModuleEstimateRepository firstModuleEstimateRepository;

	@Autowired
	private HeatMapSettingsRepository heatMapSettingsRepository;


	public static void main(String[] args) {
		SpringApplication.run(CardatabaseApplication.class,args);
	}

	@Bean
	public Docket productApi() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.packt.cardatabase.web")).build();
	}


	@Bean
	CommandLineRunner runner() {
		return args -> {
			String sDate="01-05-2021";
			String eDate="01-05-2022";
			String lDate="15-04-2021";

			/*
			urepository.save(new User("user", "$2a$04$1.YhMIgNX/8TkCKGFUONWO1waedKhQ5KrnB30fl0Q01QKqmzLf.Zi","USER"));
			urepository.save(new User("admin", "$2a$04$KNLUwOWHVQZVpXyMBNc7JOzbLiBjb9Tk9bP7KNcPI12ICuvzXQQKG","ADMIN"));
*/

			Project project = new Project("abcd","abcd","abcd","abcd","abcd","abcd","abcd",lDate,sDate,eDate);
			projectRepository.save(project);

			long projectId = project.getId();


			setDefaultValues(projectId, WeeklyMetrics.getManualTestCaseAuthoringProductivityName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getRegressionTestAutomationScriptingProductivityName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getApiTestAutomationScriptingProductivityName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getInProjectAutomationScriptingProductivityName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getManualTestExecutionProductivityName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getRegressionAutomationTestExecutionProductivityName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getInProjectAutomationTestExecutionProductivityName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getAPIAutomationTestExecutionProductivityName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getDesignCoverageName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getSanityAutomationCoverageName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getPercentageOfRegressionAutomationName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getMigratedDataExecutionName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getDefectLeakageSITToUATName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getDefectLeakageToProductionName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getNumberOfDefectsRejectedForTheWeekName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getTotalNumberOfDefectsReopenedForTheWeekName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getDefectRejectionName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getTestCaseNotExecutedManualName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getTestCaseNotExecutedAutomationName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getPercentageOfPendingRegressionAutomationName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getPercentageOfRequirementsNotCoveredName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getAverageTimeTakenToResolveSev1DefectsName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getDowntimeAndImpactAnalysisDesignAvgProductivityLostName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getDowntimeAndImpactAnalysisExecutionAvgProductivityLostName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getTestReviewEfficiencyName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getCurrentQualityRatioName(), "");
			setDefaultValues(projectId, WeeklyMetrics.getQualityOfFixesName(), "");

			setDefaultValues(projectId, MonthlyMetrics.getCostVarianceName(), "");
			setDefaultValues(projectId, MonthlyMetrics.getEffortVarianceName(), "");
			setDefaultValues(projectId, MonthlyMetrics.getScopeVarianceName(), "");
			setDefaultValues(projectId, MonthlyMetrics.getScheduleVarianceName(), "");
			setDefaultValues(projectId, MonthlyMetrics.getCostPerDefectName(), "");
			setDefaultValues(projectId, MonthlyMetrics.getCostPerTestCaseName(), "");
			setDefaultValues(projectId, MonthlyMetrics.getSITDefectDensityName(), "");
			setDefaultValues(projectId, MonthlyMetrics.getUATDefectDensityName(), "");
			setDefaultValues(projectId, MonthlyMetrics.getDefectLeakageSITToUATName(), "");
			setDefaultValues(projectId, MonthlyMetrics.getDefectLeakageToProductionName(), "");
			setDefaultValues(projectId, MonthlyMetrics.getInProjectAutomationPercentageExtentToPotentialName(), "");
			setDefaultValues(projectId, MonthlyMetrics.getApiAutomationPercentageExtentToPotentialName(), "");
			setDefaultValues(projectId, MonthlyMetrics.getPercentageOfTCsIdentifiedForMigrationName(), "");
			setDefaultValues(projectId, MonthlyMetrics.getManualEffectivenessName(), "");
			setDefaultValues(projectId, MonthlyMetrics.getAutomationEffectivenessName(), "");

			setDefaultValues(projectId, QuarterlyMetrics.getRevenueLeakageName(), "");

			CurrentSession currentSession = new CurrentSession(100);
			currentSessionRepository.save(currentSession);


			LocalDateTime myDateObj = LocalDateTime.now();
			System.out.println("Before formatting: "+ myDateObj);
			DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSSSSS");

			String formattedDate = myDateObj.format(myFormatObj);
			System.out.println("After formatting: "+ formattedDate);



			Module module1 = new Module("Module1","5000","10000","1000","Simple",project);
			Module module2 = new Module("Module2","5000","10000","1000","Medium",project);
			moduleRepository.save(module1);
			moduleRepository.save(module2);

			FirstModuleEstimate firstModuleEstimate = new FirstModuleEstimate(module1);
			firstModuleEstimateRepository.save(firstModuleEstimate);

			firstModuleEstimate = new FirstModuleEstimate(module2);
			firstModuleEstimateRepository.save(firstModuleEstimate);


			ProjectEstimate projectEstimate = new ProjectEstimate(formattedDate,sDate,eDate,"10000","10000","10000","100","200","200","200","200","200","200","200","200","200","200","5000","100","100","100","100","100","100","100","1000","10000","1","2",project);
			projectEstimateRepository.save(projectEstimate);

			FirstProjectEstimate firstProjectEstimate = new FirstProjectEstimate(projectEstimate);
			firstProjectEstimateRepository.save(firstProjectEstimate);

			LatestProjectEstimate latestProjectEstimate = new LatestProjectEstimate(projectEstimate);

			System.out.println("Automation Test Case execution effort : "+ projectEstimate.getAutomationTestCaseExecutionEffort());
			System.out.println("Latest Automation Test Case execution effort : "+ latestProjectEstimate.getAutomationTestCaseExecutionEffort());

			latestProjectEstimateRepository.save(latestProjectEstimate);


			Iterable<Module> allModules = moduleRepository.findAll();
			Iterator<Module> allModulesIterator = allModules.iterator();

			while (allModulesIterator.hasNext()) {
				Module nextModule = allModulesIterator.next();
				ModuleEstimate tempModuleEstimate = new ModuleEstimate();
				tempModuleEstimate.setModuleTimeStamp(formattedDate);
				tempModuleEstimate.setOriginalModuleId(nextModule.getId());
				tempModuleEstimate.setName(nextModule.getName());
				tempModuleEstimate.setEstimatedManualTestCases(nextModule.getEstimatedManualTestCases());
				tempModuleEstimate.setEstimatedRegressionAutomationTestCases(nextModule.getEstimatedRegressionAutomationTestCases());
				tempModuleEstimate.setEstimatedSanityAutomationTestCases(nextModule.getEstimatedSanityAutomationTestCases());
				tempModuleEstimate.setProject(nextModule.getProject());
				moduleEstimateRepository.save(tempModuleEstimate);

				LatestModuleEstimate latestModuleEstimate = new LatestModuleEstimate(tempModuleEstimate);
				latestModuleEstimateRepository.save(latestModuleEstimate);
			}


			QuarterlyProgress quarterlyProgress = new QuarterlyProgress("100","1000","1100","100","100","100",1,2021,project);
			quarterlyProgressRepository.save(quarterlyProgress);

			quarterlyProgress = new QuarterlyProgress("100","1000","900","100","100","100",2,2021,project);
			quarterlyProgressRepository.save(quarterlyProgress);

			quarterlyProgress = new QuarterlyProgress("100","1000","800","100","100","100",3,2021,project);
			quarterlyProgressRepository.save(quarterlyProgress);

			quarterlyProgress = new QuarterlyProgress("100","1000","1200","100","100","100",4,2021,project);
			quarterlyProgressRepository.save(quarterlyProgress);


			MonthlyTestDesignProgress monthlyTestDesignProgress = null;
			monthlyTestDesignProgress = new MonthlyTestDesignProgress("1000","592","100",1,2021,"10-06-2021","10-10-2022",project);monthlyTestDesignProgressRepository.save(monthlyTestDesignProgress);
			monthlyTestDesignProgress = new MonthlyTestDesignProgress("2000","645","200",2,2021,"10-07-2021","10-10-2022",project);monthlyTestDesignProgressRepository.save(monthlyTestDesignProgress);
            monthlyTestDesignProgress = new MonthlyTestDesignProgress("3000","679","300",3,2021,"10-08-2021","10-10-2022",project);monthlyTestDesignProgressRepository.save(monthlyTestDesignProgress);
			monthlyTestDesignProgress = new MonthlyTestDesignProgress("4000","730","400",4,2021,"10-09-2021","10-10-2022",project);monthlyTestDesignProgressRepository.save(monthlyTestDesignProgress);


			MonthlyGovernance monthlyGovernance = null;
			monthlyGovernance = new MonthlyGovernance("10000","9000", "11000","2000", "2","1","1","100","0","0",  "4","4","2","2","1", "0","1",1,2021,project);monthlyGovernanceRepository.save(monthlyGovernance);
			monthlyGovernance = new MonthlyGovernance("20000","16000","24000","8000", "4","2","1","200","1","1",  "4","2","1","1","4", "2","2",2,2021,project);monthlyGovernanceRepository.save(monthlyGovernance);
			monthlyGovernance = new MonthlyGovernance("30000","25000","30000","5000", "6","3","1","300","0","0",  "4","9","5","4","10","6","4",3,2021,project);monthlyGovernanceRepository.save(monthlyGovernance);
			monthlyGovernance = new MonthlyGovernance("40000","30000","50000","20000","8","4","2","400","2","1",  "3","1","1","1","6", "4","2",4,2021,project);monthlyGovernanceRepository.save(monthlyGovernance);

			MonthlyTestExecutionProgress monthlyTestExecutionProgress = null;

			monthlyTestExecutionProgress = new MonthlyTestExecutionProgress("200","100","10","301","674","189","128","128","40","10","200",1,2021,"10-09-2021","10-10-2022",project);monthlyTestExecutionProgressRepository.save(monthlyTestExecutionProgress);
			monthlyTestExecutionProgress = new MonthlyTestExecutionProgress("150","120","20","297","617","223","97", "97", "20","12","300",2,2021,"10-10-2021","10-10-2022",project);monthlyTestExecutionProgressRepository.save(monthlyTestExecutionProgress);
			monthlyTestExecutionProgress = new MonthlyTestExecutionProgress("180","140","30","283","566","176","115","115","50","14","400",3,2021,"10-11-2021","10-10-2022",project);monthlyTestExecutionProgressRepository.save(monthlyTestExecutionProgress);
			monthlyTestExecutionProgress = new MonthlyTestExecutionProgress("200","150","40","445","785","273","137","137","60","16","500",4,2021,"10-12-2021","10-10-2022",project);monthlyTestExecutionProgressRepository.save(monthlyTestExecutionProgress);

			WeeklyProjectLevelTestDesignProgress weeklyProjectLevelTestDesignProgress = null;
			weeklyProjectLevelTestDesignProgress = new  WeeklyProjectLevelTestDesignProgress( "13","115","113","29","2","4","30","2","5","1","3","16","1","5",1,1,2021,project);weeklyProjectLevelTestDesignProgressRepository.save(weeklyProjectLevelTestDesignProgress);
			weeklyProjectLevelTestDesignProgress = new  WeeklyProjectLevelTestDesignProgress( "30","127","134","27","2","4","30","1","5","1","3","21","3","3",2,1,2021,project);weeklyProjectLevelTestDesignProgressRepository.save(weeklyProjectLevelTestDesignProgress);
			weeklyProjectLevelTestDesignProgress = new  WeeklyProjectLevelTestDesignProgress( "26","111","130","18","1","4","50","4","5","3","2","10","1","5",3,1,2021,project);weeklyProjectLevelTestDesignProgressRepository.save(weeklyProjectLevelTestDesignProgress);
			weeklyProjectLevelTestDesignProgress = new  WeeklyProjectLevelTestDesignProgress( "26","128","104","29","2","3","30","4","5","3","3","23","5","5",4,1,2021,project);weeklyProjectLevelTestDesignProgressRepository.save(weeklyProjectLevelTestDesignProgress);
			weeklyProjectLevelTestDesignProgress = new  WeeklyProjectLevelTestDesignProgress( "14","117","102","10","1","2","30","5","5","1","3","27","2","4",5,2,2021,project);weeklyProjectLevelTestDesignProgressRepository.save(weeklyProjectLevelTestDesignProgress);
			weeklyProjectLevelTestDesignProgress = new  WeeklyProjectLevelTestDesignProgress( "29","138","122","27","2","3","50","1","4","1","1","27","4","2",6,2,2021,project);weeklyProjectLevelTestDesignProgressRepository.save(weeklyProjectLevelTestDesignProgress);
			weeklyProjectLevelTestDesignProgress = new  WeeklyProjectLevelTestDesignProgress( "24","144","127","16","2","1","40","4","3","3","3","22","3","2",7,2,2021,project);weeklyProjectLevelTestDesignProgressRepository.save(weeklyProjectLevelTestDesignProgress);
			weeklyProjectLevelTestDesignProgress = new  WeeklyProjectLevelTestDesignProgress( "14","137","107","28","1","5","40","4","2","1","2","22","3","2",8,2,2021,project);weeklyProjectLevelTestDesignProgressRepository.save(weeklyProjectLevelTestDesignProgress);
			weeklyProjectLevelTestDesignProgress = new  WeeklyProjectLevelTestDesignProgress( "22","120","101","27","1","3","20","2","5","3","3","11","4","5",9,3,2021,project);weeklyProjectLevelTestDesignProgressRepository.save(weeklyProjectLevelTestDesignProgress);
			weeklyProjectLevelTestDesignProgress = new  WeeklyProjectLevelTestDesignProgress( "14","140","117","24","2","1","20","4","1","3","1","12","5","5",10,3,2021,project);weeklyProjectLevelTestDesignProgressRepository.save(weeklyProjectLevelTestDesignProgress);
			weeklyProjectLevelTestDesignProgress = new  WeeklyProjectLevelTestDesignProgress( "11","143","150","27","1","4","50","4","1","3","2","29","2","1",11,3,2021,project);weeklyProjectLevelTestDesignProgressRepository.save(weeklyProjectLevelTestDesignProgress);
			weeklyProjectLevelTestDesignProgress = new  WeeklyProjectLevelTestDesignProgress( "27","146","145","11","2","4","50","2","4","2","1","23","2","1",12,3,2021,project);weeklyProjectLevelTestDesignProgressRepository.save(weeklyProjectLevelTestDesignProgress);
			weeklyProjectLevelTestDesignProgress = new  WeeklyProjectLevelTestDesignProgress( "13","127","149","29","1","4","40","4","4","2","2","15","3","1",13,4,2021,project);weeklyProjectLevelTestDesignProgressRepository.save(weeklyProjectLevelTestDesignProgress);
			weeklyProjectLevelTestDesignProgress = new  WeeklyProjectLevelTestDesignProgress( "28","114","120","22","1","1","40","3","5","2","3","20","4","5",14,4,2021,project);weeklyProjectLevelTestDesignProgressRepository.save(weeklyProjectLevelTestDesignProgress);
			weeklyProjectLevelTestDesignProgress = new  WeeklyProjectLevelTestDesignProgress( "25","122","108","18","1","1","50","2","4","2","1","12","3","4",15,4,2021,project);weeklyProjectLevelTestDesignProgressRepository.save(weeklyProjectLevelTestDesignProgress);
			weeklyProjectLevelTestDesignProgress = new  WeeklyProjectLevelTestDesignProgress( "24","149","143","27","1","5","50","5","2","1","1","11","4","1",16,4,2021,project);weeklyProjectLevelTestDesignProgressRepository.save(weeklyProjectLevelTestDesignProgress);
			weeklyProjectLevelTestDesignProgress = new  WeeklyProjectLevelTestDesignProgress( "24","110","132","30","1","4","40","5","5","2","2","27","3","3",17,4,2021,project);weeklyProjectLevelTestDesignProgressRepository.save(weeklyProjectLevelTestDesignProgress);

			WeeklyModuleLevelTestDesignProgress moduleLevelTestDesignProgress = null;
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module1",module1.getId(),"61","270","21",1,1,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module1",module1.getId(),"95","281","15",2,1,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module1",module1.getId(),"64","210","12",3,1,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module1",module1.getId(),"58","250","13",4,1,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module1",module1.getId(),"77","240","29",5,2,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module1",module1.getId(),"58","264","29",6,2,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module1",module1.getId(),"79","297","27",7,2,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module1",module1.getId(),"89","293","28",8,2,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module1",module1.getId(),"61","204","21",9,3,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module1",module1.getId(),"98","230","12",10,3,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module1",module1.getId(),"98","256","10",11,3,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module1",module1.getId(),"91","203","24",12,3,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module1",module1.getId(),"54","205","13",13,4,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module1",module1.getId(),"58","300","26",14,4,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module1",module1.getId(),"94","260","15",15,4,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module1",module1.getId(),"79","204","18",16,4,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module1",module1.getId(),"85","231","29",17,4,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);

			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module2",module2.getId(),"79","250","30",1,1,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module2",module2.getId(),"83","300","17", 2,1,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module2",module2.getId(),"96","261","26", 3,1,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module2",module2.getId(),"56","223","20", 4,1,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module2",module2.getId(),"93","285","13", 5,2,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module2",module2.getId(),"86","269","26", 6,2,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module2",module2.getId(),"81","249","23", 7,2,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module2",module2.getId(),"82","272","24", 8,2,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module2",module2.getId(),"71","207","29", 9,3,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module2",module2.getId(),"76","285","18",10,3,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module2",module2.getId(),"92","208","10",11,3,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module2",module2.getId(),"92","215","19",12,3,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module2",module2.getId(),"89","256","28",13,4,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module2",module2.getId(),"56","230","10",14,4,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module2",module2.getId(),"73","239","17",15,4,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module2",module2.getId(),"65","256","14",16,4,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);
			moduleLevelTestDesignProgress = new WeeklyModuleLevelTestDesignProgress("Module2",module2.getId(),"77","246","24",17,4,2021,project);weeklyModuleLevelTestDesignProgressRepository.save(moduleLevelTestDesignProgress);

			WeeklyModuleLevelTestExecutionProgress moduleLevelTestExecutionProgress = null;

			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module1","44","64","93",1,1,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module1","37","59","76",2,1,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module1","33","80","60",3,1,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module1","32","81","50",4,1,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module1","48","68","80",5,2,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module1","30","89","93",6,2,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module1","31","99","96",7,2,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module1","37","96","85",8,2,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module1","34","54","90",9,3,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module1","30","82","55",10,3,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module1","41","52","84",11,3,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module1","31","73","60",12,3,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module1","34","58","51",13,4,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module1","41","74","52",14,4,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module1","45","69","73",15,4,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module1","46","96","68",16,4,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module1","49","59","80",17,4,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);


			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module2","30","92","74",1,1,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module2","35","57","96",2,1,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module2","43","52","84",3,1,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module2","47","87","141",4,1,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module2","45","61","112",5,2,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module2","44","83","19",6,2,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module2","31","58","42",7,2,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module2","31","94","90",8,2,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module2","39","68","61",9,3,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module2","36","58","81",10,3,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module2","35","97","65",11,3,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module2","37","69","70",12,3,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module2","50","78","137",13,4,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module2","48","78","120",14,4,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module2","42","67","85",15,4,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module2","48","58","85",16,4,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);
			moduleLevelTestExecutionProgress = new WeeklyModuleLevelTestExecutionProgress("Module2","42","97","34",17,4,2021,project);weeklyModuleLevelTestExecutionProgressRepository.save(moduleLevelTestExecutionProgress);


			WeeklyProjectLevelTestExecutionProgress weeklyProjectLevelTestExecutionProgress = null;
			weeklyProjectLevelTestExecutionProgress = new  WeeklyProjectLevelTestExecutionProgress("81","64","70","1","30",   "447","250","167","99", "40","28", "68", "5","17","18","5","1","3","2","4","4","5","3","9","60","107",1,1,2021,project);weeklyProjectLevelTestExecutionProgressRepository.save(weeklyProjectLevelTestExecutionProgress);
			weeklyProjectLevelTestExecutionProgress = new  WeeklyProjectLevelTestExecutionProgress("83","95","99","1","20",  "548","435","172","82", "51","39", "90", "0","14","30","5","1","5","3","3","3","3","3","0","31","141",2,1,2021,project);weeklyProjectLevelTestExecutionProgressRepository.save(weeklyProjectLevelTestExecutionProgress);
			weeklyProjectLevelTestExecutionProgress = new  WeeklyProjectLevelTestExecutionProgress("71","55","73","1","84",   "454","239","144","29", "66","49", "115","1","16","29","5","5","5","5","5","4","3","5","6","42","102",3,1,2021,project);weeklyProjectLevelTestExecutionProgressRepository.save(weeklyProjectLevelTestExecutionProgress);
			weeklyProjectLevelTestExecutionProgress = new  WeeklyProjectLevelTestExecutionProgress("79","76","56","1","35",   "382","178","191","147","32","12","44", "9","20","27","2","4","5","5","5","2","3","4","0","46","145",4,1,2021,project);weeklyProjectLevelTestExecutionProgressRepository.save(weeklyProjectLevelTestExecutionProgress);
			weeklyProjectLevelTestExecutionProgress = new  WeeklyProjectLevelTestExecutionProgress("84","69","65","1","77",   "462","327","192","92", "70","30", "100","4","19","13","3","4","2","5","3","3","2","5","4","56","136",5,2,2021,project);weeklyProjectLevelTestExecutionProgressRepository.save(weeklyProjectLevelTestExecutionProgress);
			weeklyProjectLevelTestExecutionProgress = new  WeeklyProjectLevelTestExecutionProgress("84","89","98","1","70",   "448","270","112","53", "41","18", "59", "2","18","26","1","5","2","5","3","2","4","4","6","47","65",6,2,2021,project);weeklyProjectLevelTestExecutionProgressRepository.save(weeklyProjectLevelTestExecutionProgress);
			weeklyProjectLevelTestExecutionProgress = new  WeeklyProjectLevelTestExecutionProgress("59","93","72","1","98",   "508","310","138","61", "43","34", "77", "5","30","24","5","2","4","2","4","4","4","4","8","33","105",7,2,2021,project);weeklyProjectLevelTestExecutionProgressRepository.save(weeklyProjectLevelTestExecutionProgress);
			weeklyProjectLevelTestExecutionProgress = new  WeeklyProjectLevelTestExecutionProgress("58","84","99","1","103",  "477","326","175","91", "69","15", "84", "2","16","29","4","1","1","2","3","4","4","2","7","59","116",8,2,2021,project);weeklyProjectLevelTestExecutionProgressRepository.save(weeklyProjectLevelTestExecutionProgress);
			weeklyProjectLevelTestExecutionProgress = new  WeeklyProjectLevelTestExecutionProgress("75","77","92","0","59",   "358","184","151","91", "38","22", "60", "7","29","12","4","4","2","2","5","4","2","5","8","40","111",9,3,2021,project);weeklyProjectLevelTestExecutionProgressRepository.save(weeklyProjectLevelTestExecutionProgress);
			weeklyProjectLevelTestExecutionProgress = new  WeeklyProjectLevelTestExecutionProgress("55","90","70","0","108",  "487","353","136","43", "53","40", "93", "5","19","12","4","1","3","1","5","3","5","3","7","51","85",10,3,2021,project);weeklyProjectLevelTestExecutionProgressRepository.save(weeklyProjectLevelTestExecutionProgress);
			weeklyProjectLevelTestExecutionProgress = new  WeeklyProjectLevelTestExecutionProgress("63","74","77","0","114",  "471","352","149","83", "38","28", "66", "7","30","21","4","2","2","4","5","2","2","3","0","44","105",11,3,2021,project);weeklyProjectLevelTestExecutionProgressRepository.save(weeklyProjectLevelTestExecutionProgress);
			weeklyProjectLevelTestExecutionProgress = new  WeeklyProjectLevelTestExecutionProgress("98","89","95","0","115",  "380","279","130","58", "47","25", "72", "9","12","20","1","4","2","2","2","5","2","2","3","49","81",12,3,2021,project);weeklyProjectLevelTestExecutionProgressRepository.save(weeklyProjectLevelTestExecutionProgress);
			weeklyProjectLevelTestExecutionProgress = new  WeeklyProjectLevelTestExecutionProgress("74","53","67","0","59",   "474","273","188","86", "56","46", "102","0","20","16","2","1","5","1","4","5","3","2","7","48","140",13,4,2021,project);weeklyProjectLevelTestExecutionProgressRepository.save(weeklyProjectLevelTestExecutionProgress);
			weeklyProjectLevelTestExecutionProgress = new  WeeklyProjectLevelTestExecutionProgress("69","88","64","0","25",   "455","180","172","108","49","15","64", "1","21","14","5","3","5","2","2","5","3","4","3","32","140",14,4,2021,project);weeklyProjectLevelTestExecutionProgressRepository.save(weeklyProjectLevelTestExecutionProgress);
			weeklyProjectLevelTestExecutionProgress = new  WeeklyProjectLevelTestExecutionProgress("84","89","65","0","80",   "420","242","158","50", "61","47", "108","8","16","15","4","2","5","4","2","3","5","3","2","40","118",15,4,2021,project);weeklyProjectLevelTestExecutionProgressRepository.save(weeklyProjectLevelTestExecutionProgress);
			weeklyProjectLevelTestExecutionProgress = new  WeeklyProjectLevelTestExecutionProgress("51","90","94","0","50",   "457","228","153","81", "59","13", "72", "1","20","15","3","1","5","1","5","4","5","2","3","45","108",16,4,2021,project);weeklyProjectLevelTestExecutionProgressRepository.save(weeklyProjectLevelTestExecutionProgress);
			weeklyProjectLevelTestExecutionProgress = new  WeeklyProjectLevelTestExecutionProgress("54","85","86","0","71",   "424","277","114","50", "48","16", "64", "5","17","23","1","3","2","5","3","5","5","2","3","40","74",17,4,2021,project);weeklyProjectLevelTestExecutionProgressRepository.save(weeklyProjectLevelTestExecutionProgress);



			WeeklyProjectGovernance weeklyProjectGovernance = null;
			weeklyProjectGovernance = new  WeeklyProjectGovernance("1","6","9","4","8","7","3","3","7","1",1,1,2021,project);weeklyProjectGovernanceRepository.save(weeklyProjectGovernance);
			weeklyProjectGovernance = new  WeeklyProjectGovernance("1","1","2","1","3","6","6","7","6","4",2,1,2021,project);weeklyProjectGovernanceRepository.save(weeklyProjectGovernance);
			weeklyProjectGovernance = new  WeeklyProjectGovernance("1","4","4","2","6","5","5","7","3","9",3,1,2021,project);weeklyProjectGovernanceRepository.save(weeklyProjectGovernance);
			weeklyProjectGovernance = new  WeeklyProjectGovernance("1","6","7","7","1","8","2","2","9","8",4,1,2021,project);weeklyProjectGovernanceRepository.save(weeklyProjectGovernance);
			weeklyProjectGovernance = new  WeeklyProjectGovernance("1","4","5","8","6","6","4","1","9","1",5,2,2021,project);weeklyProjectGovernanceRepository.save(weeklyProjectGovernance);
			weeklyProjectGovernance = new  WeeklyProjectGovernance("1","5","9","5","8","1","9","9","2","7",6,2,2021,project);weeklyProjectGovernanceRepository.save(weeklyProjectGovernance);
			weeklyProjectGovernance = new  WeeklyProjectGovernance("1","2","4","4","4","1","4","5","3","2",7,2,2021,project);weeklyProjectGovernanceRepository.save(weeklyProjectGovernance);
			weeklyProjectGovernance = new  WeeklyProjectGovernance("1","8","2","9","6","9","3","8","3","1",8,2,2021,project);weeklyProjectGovernanceRepository.save(weeklyProjectGovernance);
			weeklyProjectGovernance = new  WeeklyProjectGovernance("1","4","5","3","9","7","5","3","5","1",9,3,2021,project);weeklyProjectGovernanceRepository.save(weeklyProjectGovernance);
			weeklyProjectGovernance = new  WeeklyProjectGovernance("1","8","2","7","1","6","1","4","7","9",10,3,2021,project);weeklyProjectGovernanceRepository.save(weeklyProjectGovernance);
			weeklyProjectGovernance = new  WeeklyProjectGovernance("1","2","6","9","5","1","1","6","7","9",11,3,2021,project);weeklyProjectGovernanceRepository.save(weeklyProjectGovernance);
			weeklyProjectGovernance = new  WeeklyProjectGovernance("1","2","3","7","9","3","9","8","7","4",12,3,2021,project);weeklyProjectGovernanceRepository.save(weeklyProjectGovernance);
			weeklyProjectGovernance = new  WeeklyProjectGovernance("0","3","1","3","2","8","1","1","2","3",13,4,2021,project);weeklyProjectGovernanceRepository.save(weeklyProjectGovernance);
			weeklyProjectGovernance = new  WeeklyProjectGovernance("1","9","3","2","4","8","2","8","6","4",14,4,2021,project);weeklyProjectGovernanceRepository.save(weeklyProjectGovernance);
			weeklyProjectGovernance = new  WeeklyProjectGovernance("1","8","1","1","2","3","3","1","9","1",15,4,2021,project);weeklyProjectGovernanceRepository.save(weeklyProjectGovernance);
			weeklyProjectGovernance = new  WeeklyProjectGovernance("1","2","8","1","1","8","6","1","1","4",16,4,2021,project);weeklyProjectGovernanceRepository.save(weeklyProjectGovernance);
			weeklyProjectGovernance = new  WeeklyProjectGovernance("1","3","3","8","3","6","7","9","7","4",17,4,2021,project);weeklyProjectGovernanceRepository.save(weeklyProjectGovernance);


			TestDesignTestExecutionProductivity productivity = new TestDesignTestExecutionProductivity("1","Simple","15","10","10","15","30","15","40","40","30","50");
			testDesignTestExecutionProductivityRepository.save(productivity);

			productivity = new TestDesignTestExecutionProductivity("2","Medium","10","5","5","10","20","8","30","30","20","30");
			testDesignTestExecutionProductivityRepository.save(productivity);

			productivity = new TestDesignTestExecutionProductivity("3","Complex","5","2","2","5","10","4","20","20","20","20");
			testDesignTestExecutionProductivityRepository.save(productivity);

			productivity = new TestDesignTestExecutionProductivity("4","Overall","10","5","5","10","20","8","30","30","20","30");
			testDesignTestExecutionProductivityRepository.save(productivity);


			for(int yearsIndex = 2020; yearsIndex < 2030; yearsIndex++){
				try {
					Calendar cal = Calendar.getInstance();
					String strDate = "1 Jan "+ yearsIndex;
					Date date = new SimpleDateFormat("d MMM yyyy").parse(strDate);
					System.out.println(date);
					cal.setTime(date);
					for(int weekIndex = 1; weekIndex < 54; weekIndex++) {
						cal.set(Calendar.WEEK_OF_YEAR,weekIndex);
						SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
						cal.set(Calendar.DAY_OF_WEEK,Calendar.MONDAY);
						String weekStartDate = formatter.format(cal.getTime());
						String allWeekDays = weekStartDate;

						cal.add(Calendar.DAY_OF_WEEK,1);
						String weekDate =  formatter.format(cal.getTime());
						allWeekDays = allWeekDays + " " + weekDate;

						cal.add(Calendar.DAY_OF_WEEK,1);
						weekDate =  formatter.format(cal.getTime());
						allWeekDays = allWeekDays + " " + weekDate ;

						cal.add(Calendar.DAY_OF_WEEK,1);
						weekDate =  formatter.format(cal.getTime());
						allWeekDays = allWeekDays + " " + weekDate ;

						cal.add(Calendar.DAY_OF_WEEK,1);
						weekDate =  formatter.format(cal.getTime());
						allWeekDays = allWeekDays + " " + weekDate ;

						cal.add(Calendar.DAY_OF_WEEK,1);
						weekDate =  formatter.format(cal.getTime());
						allWeekDays = allWeekDays + " " + weekDate ;


						cal.add(Calendar.DAY_OF_WEEK,1);
						String weekEndDate =  formatter.format(cal.getTime());
						allWeekDays = allWeekDays + " " + weekEndDate ;

						StringTokenizer tokenizer = new StringTokenizer(weekEndDate,"-");
						int dateNumber = Integer.parseInt(tokenizer.nextToken());
						int monthNumber = Integer.parseInt(tokenizer.nextToken());


						WeekData weekData = new WeekData(weekIndex,monthNumber,yearsIndex,weekStartDate,weekEndDate,allWeekDays);
						weekDataRepository.save(weekData);


						Iterable<WeekMonthData> weekMonthDataIterable = weekMonthDataRepository.findAll();
						Iterator<WeekMonthData> weekMonthDataIterator = weekMonthDataIterable.iterator();

						boolean yearMonthRecordFound = false;
						while (weekMonthDataIterator.hasNext()) {

							WeekMonthData next = weekMonthDataIterator.next();
							if ((next.getYearNumber() == yearsIndex) && (next.getMonthNumber() == monthNumber) )  {
								yearMonthRecordFound = true;
								next.getWeekNumbers().add(weekIndex);
								weekMonthDataRepository.save(next);

							}
						}

						if(yearMonthRecordFound == false)
						{
							ArrayList<Integer> weekNumbers = new ArrayList<Integer>();
							weekNumbers.add(weekIndex);
							WeekMonthData next = new WeekMonthData(monthNumber,yearsIndex,weekNumbers);
							weekMonthDataRepository.save(next);
						}



					}
				}catch(Exception e){
				}

			}

			Iterable<WeekData> weekDataIterable = weekDataRepository.findAll();
			Iterator<WeekData> weekDataIterator = weekDataIterable.iterator();

			while (weekDataIterator.hasNext()) {
				WeekData next = weekDataIterator.next();
				if ((next.getWeekNumber() == 50) ||
						(next.getWeekNumber() == 51) ||
						(next.getWeekNumber() == 52)){
					next.setMonthNumber(12);
				}
				else if (next.getWeekNumber() == 53){
					next.setWeekNumber(1);
					next.setMonthNumber(1);
				}
				weekDataRepository.save(next);
			}


			weekDataIterable = weekDataRepository.findAll();
			weekDataIterator = weekDataIterable.iterator();
			WeekData next = weekDataIterator.next();
			int previousWeekNumber = 0;
			previousWeekNumber = next.getWeekNumber();


			while (weekDataIterator.hasNext()) {
				WeekData next1 = weekDataIterator.next();

				if(previousWeekNumber == next1.getWeekNumber())
				{
					weekDataRepository.delete(next1);
				}
				else{
					previousWeekNumber = next1.getWeekNumber();
				}
			}

			weekDataIterable = weekDataRepository.findAll();
			weekDataIterator = weekDataIterable.iterator();
			next = weekDataIterator.next();
			previousWeekNumber = 0;
			previousWeekNumber = next.getWeekNumber();
			int previousYearNumber = next.getYearNumber();



			while (weekDataIterator.hasNext()) {
				WeekData next1 = weekDataIterator.next();

				if(previousWeekNumber == 52)
				{
					next1.setYearNumber(previousYearNumber+1);
					weekDataRepository.save(next1);
					previousWeekNumber = next1.getWeekNumber();
					previousYearNumber = next1.getYearNumber();
				}
				else{
					previousWeekNumber = next1.getWeekNumber();
					previousYearNumber = next1.getYearNumber();
				}
			}



			try {
				for (int yearsIndex = 2020; yearsIndex < 2030; yearsIndex++) {
					for (int monthIndex = 1; monthIndex < 13; monthIndex++) {

						try {
							Calendar cal = Calendar.getInstance();
							String strDate = "1 "+ monthIndex + " "+ yearsIndex;
							Date date = new SimpleDateFormat("d MM yyyy").parse(strDate);
							System.out.println(date);
							cal.setTime(date);

							int res = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
							MonthData monthData = new MonthData(monthIndex,yearsIndex,"01"+ "-"+ monthIndex + "-"+ yearsIndex,res + "-"+ monthIndex + "-"+ yearsIndex);
							monthDataRepository.save(monthData);

						} catch (Exception e) {
						}
					}

				}


				for (int yearsIndex = 2020; yearsIndex < 2030; yearsIndex++) {
					int quarterNumber = 1;
					for (int monthIndex = 0; monthIndex < 10; ) {

						try {
							Calendar cal = Calendar.getInstance();
							String strDate = "1 "+ monthIndex + " "+ yearsIndex;
							Date date = new SimpleDateFormat("d MM yyyy").parse(strDate);
							cal.setTime(date);

							int res = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

							monthIndex = monthIndex + 3;
							QuarterData quarterData = new QuarterData(quarterNumber,yearsIndex,"01"+ "-"+ (monthIndex - 2) + "-"+ yearsIndex,res + "-"+ monthIndex + "-"+ yearsIndex);
							quarterNumber += 1;
							quarterDataRepository.save(quarterData);

						} catch (Exception e) {
						}
					}
				}


			}catch (Exception e) {
			}

			Iterable<WeekMonthData> weekMonthDataIterable = weekMonthDataRepository.findAll();
			Iterator<WeekMonthData> weekMonthDataIterator = weekMonthDataIterable.iterator();

			while (weekMonthDataIterator.hasNext()) {

				WeekMonthData next3 = weekMonthDataIterator.next();
				System.out.print(next3.getYearNumber() + ""+ next3.getMonthNumber() + "");

				for(int ii = 0; ii < next3.getWeekNumbers().size(); ii++)
				{
					System.out.print(next3.getWeekNumbers().get(ii) + ",");
				}

				System.out.println();
			}

		};
	}


	public void setDefaultValues(long id, String metricName, String category) {

		HeatMapSettings heatMapSettings = new HeatMapSettings();
		heatMapSettings.setLCL("30");
		heatMapSettings.setUCL("90");
		heatMapSettings.setGoal("70");
		heatMapSettings.setCategory(category);
		heatMapSettings.setMetricName(metricName);

		Iterable<Project> allProjects = projectRepository.findAll();
		Iterator<Project> iterator = allProjects.iterator();

		while (iterator.hasNext()) {
			Project nextProject = iterator.next();

			if (nextProject.getId() == id) {
				heatMapSettings.setProject(nextProject);
				break;
			}
		}

		heatMapSettingsRepository.save(heatMapSettings);
	}

}
