package com.packt.cardatabase.domain;

import javax.persistence.*;

public class WeeklyProjectLevelTestExecutionProgressPojo {


    private long id;

    private String numberOfInProjectAutomationTestScriptsExecuted;
    private String numberOfAPIAutomationTestScriptsExecuted;
    private String numberOfTestCasesExecutedWithMigratedData;
    private String numberOfTestCasesExecuted;
    private String numberOfModulesNotDelivered;
    private String totalNumberOfManualTestCasesNotExecuted;
    private String totalNumberOfAutomationTestCasesNotExecuted;
    private String numberOfTestCasesSuccessfullyExecutedWithoutDefects;
    private String numberOfDefectsReportedByTestingTeam;
    private String numberOfDefectsRejected;
    private String numberOfValidDefectsLoggedInSIT;
    private String numberOfValidDefectsUAT;
    private String totalNumberOfDefectsFixed;
    private String totalNumberOfDefectsReopened;
    private String averageTimTakenToResolveSev1Defects;
    private String environmentDowntimeExecutionPerPersonInHours;
    private String actualManualTestCaseExecutionEffort;
    private String actualRegressionAutomationTestCaseExecutionEffort;
    private String actualInProjectAutomationTestCaseExecutionEffort;
    private String aPIAutomationTestCaseExecutionEffort;
    private String numberOfResourcesForManualTestCaseExecution;
    private String numberOfResourceForRegressionAutomationExecution;
    private String numberOfResourceForInProjectAutomationExecution;
    private String numberOfResourceForAPIAutomationExecution;
    private String totalNumberOfDefectsInProduction;
    private String numberOfDefectsFoundUsingAutomatedRegression;
    private String numberOfDefectsFoundUsingManualRegression;



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }



    public WeeklyProjectLevelTestExecutionProgressPojo() {
    }


    public String getNumberOfInProjectAutomationTestScriptsExecuted() {
        return numberOfInProjectAutomationTestScriptsExecuted;
    }

    public void setNumberOfInProjectAutomationTestScriptsExecuted(String numberOfInProjectAutomationTestScriptsExecuted) {
        this.numberOfInProjectAutomationTestScriptsExecuted = numberOfInProjectAutomationTestScriptsExecuted;
    }

    public String getNumberOfAPIAutomationTestScriptsExecuted() {
        return numberOfAPIAutomationTestScriptsExecuted;
    }

    public void setNumberOfAPIAutomationTestScriptsExecuted(String numberOfAPIAutomationTestScriptsExecuted) {
        this.numberOfAPIAutomationTestScriptsExecuted = numberOfAPIAutomationTestScriptsExecuted;
    }

    public String getNumberOfTestCasesExecutedWithMigratedData() {
        return numberOfTestCasesExecutedWithMigratedData;
    }

    public void setNumberOfTestCasesExecutedWithMigratedData(String numberOfTestCasesExecutedWithMigratedData) {
        this.numberOfTestCasesExecutedWithMigratedData = numberOfTestCasesExecutedWithMigratedData;
    }

    public String getNumberOfTestCasesExecuted() {
        return numberOfTestCasesExecuted;
    }

    public void setNumberOfTestCasesExecuted(String numberOfTestCasesExecuted) {
        this.numberOfTestCasesExecuted = numberOfTestCasesExecuted;
    }

    public String getNumberOfModulesNotDelivered() {
        return numberOfModulesNotDelivered;
    }

    public void setNumberOfModulesNotDelivered(String numberOfModulesNotDelivered) {
        this.numberOfModulesNotDelivered = numberOfModulesNotDelivered;
    }


    public String getTotalNumberOfManualTestCasesNotExecuted() {
        return totalNumberOfManualTestCasesNotExecuted;
    }

    public void setTotalNumberOfManualTestCasesNotExecuted(String totalNumberOfManualTestCasesNotExecuted) {
        this.totalNumberOfManualTestCasesNotExecuted = totalNumberOfManualTestCasesNotExecuted;
    }

    public String getTotalNumberOfAutomationTestCasesNotExecuted() {
        return totalNumberOfAutomationTestCasesNotExecuted;
    }

    public void setTotalNumberOfAutomationTestCasesNotExecuted(String totalNumberOfAutomationTestCasesNotExecuted) {
        this.totalNumberOfAutomationTestCasesNotExecuted = totalNumberOfAutomationTestCasesNotExecuted;
    }

    public String getNumberOfTestCasesSuccessfullyExecutedWithoutDefects() {
        return numberOfTestCasesSuccessfullyExecutedWithoutDefects;
    }

    public void setNumberOfTestCasesSuccessfullyExecutedWithoutDefects(String numberOfTestCasesSuccessfullyExecutedWithoutDefects) {
        this.numberOfTestCasesSuccessfullyExecutedWithoutDefects = numberOfTestCasesSuccessfullyExecutedWithoutDefects;
    }

    public String getNumberOfDefectsReportedByTestingTeam() {
        return numberOfDefectsReportedByTestingTeam;
    }

    public void setNumberOfDefectsReportedByTestingTeam(String numberOfDefectsReportedByTestingTeam) {
        this.numberOfDefectsReportedByTestingTeam = numberOfDefectsReportedByTestingTeam;
    }

    public String getNumberOfDefectsRejected() {
        return numberOfDefectsRejected;
    }

    public void setNumberOfDefectsRejected(String numberOfDefectsRejected) {
        this.numberOfDefectsRejected = numberOfDefectsRejected;
    }

    public String getNumberOfValidDefectsLoggedInSIT() {
        return numberOfValidDefectsLoggedInSIT;
    }

    public void setNumberOfValidDefectsLoggedInSIT(String numberOfValidDefectsLoggedInSIT) {
        this.numberOfValidDefectsLoggedInSIT = numberOfValidDefectsLoggedInSIT;
    }

    public String getNumberOfValidDefectsUAT() {
        return numberOfValidDefectsUAT;
    }

    public void setNumberOfValidDefectsUAT(String numberOfValidDefectsUAT) {
        this.numberOfValidDefectsUAT = numberOfValidDefectsUAT;
    }

    public String getTotalNumberOfDefectsFixed() {
        return totalNumberOfDefectsFixed;
    }

    public void setTotalNumberOfDefectsFixed(String totalNumberOfDefectsFixed) {
        this.totalNumberOfDefectsFixed = totalNumberOfDefectsFixed;
    }

    public String getTotalNumberOfDefectsReopened() {
        return totalNumberOfDefectsReopened;
    }

    public void setTotalNumberOfDefectsReopened(String totalNumberOfDefectsReopened) {
        this.totalNumberOfDefectsReopened = totalNumberOfDefectsReopened;
    }

    public String getAverageTimTakenToResolveSev1Defects() {
        return averageTimTakenToResolveSev1Defects;
    }

    public void setAverageTimTakenToResolveSev1Defects(String averageTimTakenToResolveSev1Defects) {
        this.averageTimTakenToResolveSev1Defects = averageTimTakenToResolveSev1Defects;
    }

    public String getEnvironmentDowntimeExecutionPerPersonInHours() {
        return environmentDowntimeExecutionPerPersonInHours;
    }

    public void setEnvironmentDowntimeExecutionPerPersonInHours(String environmentDowntimeExecutionPerPersonInHours) {
        this.environmentDowntimeExecutionPerPersonInHours = environmentDowntimeExecutionPerPersonInHours;
    }


    public String getActualManualTestCaseExecutionEffort() {
        return actualManualTestCaseExecutionEffort;
    }

    public void setActualManualTestCaseExecutionEffort(String actualManualTestCaseExecutionEffort) {
        this.actualManualTestCaseExecutionEffort = actualManualTestCaseExecutionEffort;
    }

    public String getActualRegressionAutomationTestCaseExecutionEffort() {
        return actualRegressionAutomationTestCaseExecutionEffort;
    }

    public void setActualRegressionAutomationTestCaseExecutionEffort(String actualRegressionAutomationTestCaseExecutionEffort) {
        this.actualRegressionAutomationTestCaseExecutionEffort = actualRegressionAutomationTestCaseExecutionEffort;
    }

    public String getActualInProjectAutomationTestCaseExecutionEffort() {
        return actualInProjectAutomationTestCaseExecutionEffort;
    }

    public void setActualInProjectAutomationTestCaseExecutionEffort(String actualInProjectAutomationTestCaseExecutionEffort) {
        this.actualInProjectAutomationTestCaseExecutionEffort = actualInProjectAutomationTestCaseExecutionEffort;
    }

    public String getAPIAutomationTestCaseExecutionEffort() {
        return aPIAutomationTestCaseExecutionEffort;
    }

    public void setAPIAutomationTestCaseExecutionEffort(String aPIAutomationTestCaseExecutionEffort) {
        this.aPIAutomationTestCaseExecutionEffort = aPIAutomationTestCaseExecutionEffort;
    }

    public String getNumberOfResourcesForManualTestCaseExecution() {
        return numberOfResourcesForManualTestCaseExecution;
    }

    public void setNumberOfResourcesForManualTestCaseExecution(String numberOfResourcesForManualTestCaseExecution) {
        this.numberOfResourcesForManualTestCaseExecution = numberOfResourcesForManualTestCaseExecution;
    }


    public String getNumberOfResourceForRegressionAutomationExecution() {


        return numberOfResourceForRegressionAutomationExecution;

    }


    public void setNumberOfResourceForRegressionAutomationExecution(String numberOfResourceForRegressionAutomationExecution) {
        this.numberOfResourceForRegressionAutomationExecution = numberOfResourceForRegressionAutomationExecution;
    }


    public String getNumberOfResourceForInProjectAutomationExecution() {
        return numberOfResourceForInProjectAutomationExecution;
    }


    public void setNumberOfResourceForInProjectAutomationExecution(String numberOfResourceForInProjectAutomationExecution) {
        this.numberOfResourceForInProjectAutomationExecution = numberOfResourceForInProjectAutomationExecution;
    }


    public String getNumberOfResourceForAPIAutomationExecution() {
        return numberOfResourceForAPIAutomationExecution;
    }


    public void setNumberOfResourceForAPIAutomationExecution(String numberOfResourceForAPIAutomationExecution) {
        this.numberOfResourceForAPIAutomationExecution = numberOfResourceForAPIAutomationExecution;
    }


    public String getTotalNumberOfDefectsInProduction() {
        return totalNumberOfDefectsInProduction;
    }

    public void setTotalNumberOfDefectsInProduction(String totalNumberOfDefectsInProduction) {
        this.totalNumberOfDefectsInProduction = totalNumberOfDefectsInProduction;
    }

    public String getNumberOfDefectsFoundUsingAutomatedRegression() {
        return numberOfDefectsFoundUsingAutomatedRegression;
    }


    public void setNumberOfDefectsFoundUsingAutomatedRegression(String numberOfDefectsFoundUsingAutomatedRegression) {
        this.numberOfDefectsFoundUsingAutomatedRegression = numberOfDefectsFoundUsingAutomatedRegression;
    }


    public String getNumberOfDefectsFoundUsingManualRegression() {
        return numberOfDefectsFoundUsingManualRegression;
    }


    public void setNumberOfDefectsFoundUsingManualRegression(String numberOfDefectsFoundUsingManualRegression) {
        this.numberOfDefectsFoundUsingManualRegression = numberOfDefectsFoundUsingManualRegression;
    }


}