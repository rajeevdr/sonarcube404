package com.packt.cardatabase.web;

import com.google.gson.Gson;
import com.packt.cardatabase.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@RestController
public class MonthlyGovernanceController {

	@Autowired
	private ProjectRepository projectRepository;


	@Autowired
	private MonthlyGovernanceRepository monthlyGovernanceRepository;


	@RequestMapping(value="/projects/{id}/governance/monthlyProgress/month/{month}/year/{year}", method=RequestMethod.GET)
	@ResponseBody
	public String getMonthlyTestExecutionProgress(@PathVariable("id") long id, @PathVariable(value="month") String month, @PathVariable(value="year") int year) {

		try {
			ArrayList<ProjectProgressPojo> progressPojoArray = new ArrayList<ProjectProgressPojo>();
			int monthNumber = 0;

			if(month.contains("Jan"))
			{
				monthNumber = 1;
			}
			else if(month.contains("Feb"))
			{
				monthNumber = 2;
			}
			else if(month.contains("Mar"))
			{
				monthNumber = 3;
			}
			else if(month.contains("Apr"))
			{
				monthNumber = 4;
			}
			else if(month.contains("May"))
			{
				monthNumber = 5;
			}
			else if(month.contains("Jun"))
			{
				monthNumber = 6;
			}
			else if(month.contains("Jul"))
			{
				monthNumber = 7;
			}
			else if(month.contains("Aug"))
			{
				monthNumber = 8;
			}
			else if(month.contains("Sep"))
			{
				monthNumber = 9;
			}
			else if(month.contains("Oct"))
			{
				monthNumber = 10;
			}
			else if(month.contains("Nov"))
			{
				monthNumber = 11;
			}
			else if(month.contains("Dec"))
			{
				monthNumber = 12;
			}
			else{
				monthNumber = Integer.parseInt(month);
			}


			int yearNumber = year;

			Iterable<MonthlyGovernance> monthlyGovernanceIterable = monthlyGovernanceRepository.findAll();
			Iterator<MonthlyGovernance> monthlyGovernanceIterator = monthlyGovernanceIterable.iterator();

			String response = null;
			boolean recordFound = false;

			while (monthlyGovernanceIterator.hasNext()) {
				MonthlyGovernance next = monthlyGovernanceIterator.next();
				if ((next.getMonthNumber() == monthNumber) && (next.getProject().getId() == id)) {
					recordFound = true;
					int index = 1;

					ProjectProgressPojo projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getBudgetCostForTheMonthName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getBudgetCostForTheMonth()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Basic Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getActualCostForTheMonthName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getActualCostForTheMonth()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Basic Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getActualBillingForTheMonthName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getActualBillingForTheMonth()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Basic Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getGrossMarginForTheMonthName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getGrossMarginForTheMonth()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Basic Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNoOfTestCasesForCostApprovedCRsName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getNoOfTestCasesForCostApprovedCRs()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Basic Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfTestEngineersName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfTestEngineers()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Team Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfLeadsName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfLeads()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Team Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfManagersName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfManagers()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Team Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfResignationsSubmittedName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfResignationsSubmitted()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Team Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfResourcesRelievedName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfResourcesRelieved()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Team Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfFormalGovernanceMeetingsName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfFormalGovernanceMeetings()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Basic Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfStepChangesProposedName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfStepChangesProposed()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Step Change Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfStepChangesInProgressName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfStepChangesInProgress()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Step Change Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfStepChangesImplementedName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfStepChangesImplemented()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Step Change Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfCIsProposedName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfCIsProposed()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("CI Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfCIsInProgressName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfCIsInProgress()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("CI Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfCIsImplementedName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfCIsImplemented()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("CI Information");
					progressPojoArray.add(projectProgressPojo);

					Gson gson = new Gson();
					response = gson.toJson(progressPojoArray);
					response = "{\"progress\":" + response + "}";

					System.out.println(response);
					return response;
				}
			}


			if(recordFound == false){
				MonthlyGovernance next = new MonthlyGovernance();

					int index = 1;

					ProjectProgressPojo projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getBudgetCostForTheMonthName());
					projectProgressPojo.setValue("0");
					projectProgressPojo.setCategory("Basic Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getActualCostForTheMonthName());
					projectProgressPojo.setValue("0");
					projectProgressPojo.setCategory("Basic Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getActualBillingForTheMonthName());
					projectProgressPojo.setValue("0");
					projectProgressPojo.setCategory("Basic Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getGrossMarginForTheMonthName());
					projectProgressPojo.setValue("0");
					projectProgressPojo.setCategory("Basic Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNoOfTestCasesForCostApprovedCRsName());
					projectProgressPojo.setValue("0");
					projectProgressPojo.setCategory("Basic Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfTestEngineersName());
					projectProgressPojo.setValue("0");
					projectProgressPojo.setCategory("Team Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfLeadsName());
					projectProgressPojo.setValue("0");
					projectProgressPojo.setCategory("Team Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfManagersName());
					projectProgressPojo.setValue("0");
					projectProgressPojo.setCategory("Team Information");
					progressPojoArray.add(projectProgressPojo);


					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfResignationsSubmittedName());
					projectProgressPojo.setValue("0");
					projectProgressPojo.setCategory("Team Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfResourcesRelievedName());
					projectProgressPojo.setValue("0");
					projectProgressPojo.setCategory("Team Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfFormalGovernanceMeetingsName());
					projectProgressPojo.setValue("0");
					projectProgressPojo.setCategory("Basic Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfStepChangesProposedName());
					projectProgressPojo.setValue("0");
					projectProgressPojo.setCategory("Step Change Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfStepChangesInProgressName());
					projectProgressPojo.setValue("0");
					projectProgressPojo.setCategory("Step Change Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfStepChangesImplementedName());
					projectProgressPojo.setValue("0");
					projectProgressPojo.setCategory("Step Change Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfCIsProposedName());
					projectProgressPojo.setValue("0");
					projectProgressPojo.setCategory("CI Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfCIsInProgressName());
					projectProgressPojo.setValue("0");
					projectProgressPojo.setCategory("CI Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfCIsImplementedName());
					projectProgressPojo.setValue("0");
					projectProgressPojo.setCategory("CI Information");
					progressPojoArray.add(projectProgressPojo);

					Gson gson = new Gson();
					response = gson.toJson(progressPojoArray);
				response = "{\"progress\":" + response + "}";

					System.out.println(response);
					return response;
				}


		} catch (Exception e) {
		}

		return null;
	}


	@RequestMapping(value="/projects/{id}/governance/totalMonthlyProgress", method=RequestMethod.GET)
	@ResponseBody
	public String getTotalMonthlyTestExecutionProgress(@PathVariable("id") long id) {

		try {
			ArrayList<ArrayList<ProjectProgressPojo>> monthlyProjectGovernanceArray = new ArrayList<ArrayList<ProjectProgressPojo>>();

			Iterable<MonthlyGovernance> monthlyGovernanceIterable = monthlyGovernanceRepository.findAll();
			Iterator<MonthlyGovernance> monthlyGovernanceIterator = monthlyGovernanceIterable.iterator();

			String response = null;

			while (monthlyGovernanceIterator.hasNext()) {
				MonthlyGovernance next = monthlyGovernanceIterator.next();
				if (next.getProject().getId() == id) {
					int index = 1;

					ArrayList<ProjectProgressPojo> progressPojoArray = new ArrayList<ProjectProgressPojo>();
					ProjectProgressPojo projectProgressPojo = new ProjectProgressPojo();

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("Index");
					projectProgressPojo.setName(next.getMonthNumberName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getMonthNumber()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("Index");
					projectProgressPojo.setName(next.getYearNumberName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getYearNumber()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);


					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getBudgetCostForTheMonthName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getBudgetCostForTheMonth()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("BasicInformation");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getActualCostForTheMonthName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getActualCostForTheMonth()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("BasicInformation");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getActualBillingForTheMonthName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getActualBillingForTheMonth()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("BasicInformation");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getGrossMarginForTheMonthName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getGrossMarginForTheMonth()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("BasicInformation");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNoOfTestCasesForCostApprovedCRsName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNoOfTestCasesForCostApprovedCRs()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("BasicInformation");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfTestEngineersName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfTestEngineers()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("TeamInformation");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfLeadsName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfLeads()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("TeamInformation");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfManagersName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfManagers()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("TeamInformation");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfResignationsSubmittedName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfResignationsSubmitted()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("TeamInformation");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfResourcesRelievedName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfResourcesRelieved()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("TeamInformation");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfFormalGovernanceMeetingsName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfFormalGovernanceMeetings()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("BasicInformation");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfStepChangesProposedName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfStepChangesProposed()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("StepChangeInformation");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfStepChangesInProgressName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfStepChangesInProgress()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("StepChangeInformation");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfStepChangesImplementedName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfStepChangesImplemented()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("StepChangeInformation");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfCIsProposedName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfCIsProposed()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("CIInformation");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfCIsInProgressName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfCIsInProgress()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("CIInformation");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfCIsImplementedName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfCIsImplemented()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("CIInformation");
					progressPojoArray.add(projectProgressPojo);

					monthlyProjectGovernanceArray.add(progressPojoArray);
				}
			}

					Gson gson = new Gson();
					response = gson.toJson(monthlyProjectGovernanceArray);
					response = "{\"progress\":" + response + "}";

					System.out.println(response);
					return response;


		} catch (Exception e) {
		}

		return null;
	}

//	http://localhost:8082/springmvc/tutorial?topic=requestParam&author=daniel&site=jcg

	@PostMapping(value = "/projects/{id}/governance/monthlyProgress/month/{month}/year/{year}")
	@ResponseBody
	public String updateMonthlyTestExecutionProgress(@PathVariable("id") long id, @PathVariable(value="month") String month, @PathVariable(value="year") int year, @RequestBody List<ProjectProgressPojo> progressPojoArray) {

		try {
			int monthNumber = 0;

			if(month.contains("Jan"))
			{
				monthNumber = 1;
			}
			else if(month.contains("Feb"))
			{
				monthNumber = 2;
			}
			else if(month.contains("Mar"))
			{
				monthNumber = 3;
			}
			else if(month.contains("Apr"))
			{
				monthNumber = 4;
			}
			else if(month.contains("May"))
			{
				monthNumber = 5;
			}
			else if(month.contains("Jun"))
			{
				monthNumber = 6;
			}
			else if(month.contains("Jul"))
			{
				monthNumber = 7;
			}
			else if(month.contains("Aug"))
			{
				monthNumber = 8;
			}
			else if(month.contains("Sep"))
			{
				monthNumber = 9;
			}
			else if(month.contains("Oct"))
			{
				monthNumber = 10;
			}
			else if(month.contains("Nov"))
			{
				monthNumber = 11;
			}
			else if(month.contains("Dec"))
			{
				monthNumber = 12;
			}
			else{
				monthNumber = Integer.parseInt(month);
			}


			int yearNumber = year;

			boolean matchFound = false;

			Iterable<MonthlyGovernance> monthlyGovernanceIterable = monthlyGovernanceRepository.findAll();
			Iterator<MonthlyGovernance> monthlyGovernanceIterator = monthlyGovernanceIterable.iterator();


			while (monthlyGovernanceIterator.hasNext()) {
				MonthlyGovernance next = monthlyGovernanceIterator.next();

				if ((next.getMonthNumber() == monthNumber) && (next.getProject().getId() == id) && (next.getYearNumber() == yearNumber) ) {
					matchFound = true;

					for (int i = 0; i < progressPojoArray.size(); i++) {

						if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getBudgetCostForTheMonthName())) {
							next.setBudgetCostForTheMonth(progressPojoArray.get(i).getValue());
						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getActualCostForTheMonthName())) {
							next.setActualCostForTheMonth(progressPojoArray.get(i).getValue());
						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getActualBillingForTheMonthName())) {
							next.setActualBillingForTheMonth(progressPojoArray.get(i).getValue());
						}  else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getGrossMarginForTheMonthName())) {
							next.setGrossMarginForTheMonth(progressPojoArray.get(i).getValue());
						}else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfTestEngineersName())) {
							next.setNumberOfTestEngineers(progressPojoArray.get(i).getValue());
						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfLeadsName())) {
							next.setNumberOfLeads(progressPojoArray.get(i).getValue());
						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfManagersName())) {
							next.setNumberOfManagers(progressPojoArray.get(i).getValue());
						}else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfResignationsSubmittedName())) {
							next.setTotalNumberOfResignationsSubmitted(progressPojoArray.get(i).getValue());
						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfResourcesRelievedName())) {
							next.setTotalNumberOfResourcesRelieved(progressPojoArray.get(i).getValue());
						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfFormalGovernanceMeetingsName())) {
							next.setNumberOfFormalGovernanceMeetings(progressPojoArray.get(i).getValue());
						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfStepChangesProposedName())) {
							next.setNumberOfStepChangesProposed(progressPojoArray.get(i).getValue());
						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfStepChangesInProgressName())) {
							next.setNumberOfStepChangesInProgress(progressPojoArray.get(i).getValue());
						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfStepChangesImplementedName())) {
							next.setNumberOfStepChangesImplemented(progressPojoArray.get(i).getValue());
						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfCIsProposedName())) {
							next.setNumberOfCIsProposed(progressPojoArray.get(i).getValue());
						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfCIsInProgressName())) {
							next.setNumberOfCIsInProgress(progressPojoArray.get(i).getValue());
						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfCIsImplementedName())) {
							next.setNumberOfCIsImplemented(progressPojoArray.get(i).getValue());
						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNoOfTestCasesForCostApprovedCRsName())) {
							next.setNoOfTestCasesForCostApprovedCRs(progressPojoArray.get(i).getValue());
						}
					}

					next.setMonthNumber(monthNumber);
					next.setYearNumber(yearNumber);
					monthlyGovernanceRepository.save(next);
					break;
				}
			}
			if (matchFound == true)
				return "OK";
			else {

				MonthlyGovernance progress = new MonthlyGovernance();

				for (int i = 0; i < progressPojoArray.size(); i++) {


					if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getBudgetCostForTheMonthName())) {
						progress.setBudgetCostForTheMonth(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getActualCostForTheMonthName())) {
						progress.setActualCostForTheMonth(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getActualBillingForTheMonthName())) {
						progress.setActualBillingForTheMonth(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getGrossMarginForTheMonthName())) {
						progress.setGrossMarginForTheMonth(progressPojoArray.get(i).getValue());
					}else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getNumberOfTestEngineersName())) {
						progress.setNumberOfTestEngineers(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getNumberOfLeadsName())) {
						progress.setNumberOfLeads(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getNumberOfManagersName())) {
						progress.setNumberOfManagers(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getTotalNumberOfResignationsSubmittedName())) {
						progress.setTotalNumberOfResignationsSubmitted(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getTotalNumberOfResourcesRelievedName())) {
						progress.setTotalNumberOfResourcesRelieved(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getNumberOfFormalGovernanceMeetingsName())) {
						progress.setNumberOfFormalGovernanceMeetings(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getNumberOfStepChangesProposedName())) {
						progress.setNumberOfStepChangesProposed(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getNumberOfStepChangesInProgressName())) {
						progress.setNumberOfStepChangesInProgress(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getNumberOfStepChangesImplementedName())) {
						progress.setNumberOfStepChangesImplemented(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getNumberOfCIsProposedName())) {
						progress.setNumberOfCIsProposed(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getNumberOfCIsInProgressName())) {
						progress.setNumberOfCIsInProgress(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getNumberOfCIsImplementedName())) {
						progress.setNumberOfCIsImplemented(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getNoOfTestCasesForCostApprovedCRsName())) {
						progress.setNoOfTestCasesForCostApprovedCRs(progressPojoArray.get(i).getValue());
					}
				}


				Iterable<Project> allProjects = projectRepository.findAll();
				Iterator<Project> iterator = allProjects.iterator();

				while (iterator.hasNext()) {
					Project nextProject = iterator.next();

					if (nextProject.getId() == id) {
						progress.setProject(nextProject);
						break;
					}
				}
				progress.setMonthNumber(monthNumber);
				progress.setYearNumber(yearNumber);

				monthlyGovernanceRepository.save(progress);
				return "OK";
			}
		}catch (Exception e) {
		}
		return "Error";
	}
}