package com.packt.cardatabase.domain;


public class CurrentSessionPojo {

    private int id;

    private int projectId;

    public CurrentSessionPojo() {}

    public CurrentSessionPojo(int projectId) {
        this.projectId = projectId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

}