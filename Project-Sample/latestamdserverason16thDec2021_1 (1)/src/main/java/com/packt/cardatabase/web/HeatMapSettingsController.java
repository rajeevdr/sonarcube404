package com.packt.cardatabase.web;

import com.google.gson.Gson;
import com.packt.cardatabase.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@RestController
public class HeatMapSettingsController {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private HeatMapSettingsRepository heatMapSettingsRepository;


    @RequestMapping(value="/projects/{id}/heatMapSettings", method=RequestMethod.GET)
    @ResponseBody
    public String getHeatMapSettings(@PathVariable("id") long id) {

        try {

            Iterable<HeatMapSettings> heatMapSettingsIterable = heatMapSettingsRepository.findAll();
            Iterator<HeatMapSettings> heatMapSettingsIterator = heatMapSettingsIterable.iterator();
            int index = 0;
            ArrayList<HeatMapSettingsPojo> heatMapSettingsPojoArray = new ArrayList<HeatMapSettingsPojo>();

            while (heatMapSettingsIterator.hasNext()) {
                HeatMapSettings next = heatMapSettingsIterator.next();

                if (next.getProject().getId() == id) {

                    try {
                        HeatMapSettingsPojo heatMapSettingsPojo = new HeatMapSettingsPojo();
                        heatMapSettingsPojo.setId(index);
                        heatMapSettingsPojo.setMetricName(next.getMetricName());
                        heatMapSettingsPojo.setLCL(next.getLCL());
                        heatMapSettingsPojo.setUCL(next.getUCL());
                        heatMapSettingsPojo.setGoal(next.getGoal());

                            if (next.getMetricName().compareTo(WeeklyMetrics.getManualTestCaseAuthoringProductivityName()) == 0 ||
                                    heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getRegressionTestAutomationScriptingProductivityName()) == 0 ||
                                    heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getApiTestAutomationScriptingProductivityName()) == 0 ||
                                    heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getInProjectAutomationScriptingProductivityName()) == 0 ||
                                    heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getManualTestExecutionProductivityName()) == 0 ||
                                    heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getRegressionAutomationTestExecutionProductivityName()) == 0 ||
                                    heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getInProjectAutomationTestExecutionProductivityName()) == 0 ||
                                    heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getAPIAutomationTestExecutionProductivityName()) == 0)
                                    {
                                       heatMapSettingsPojo.setCategory("Productivity");
                                    }


                        else if (    heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getDesignCoverageName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getSanityAutomationCoverageName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getPercentageOfRegressionAutomationName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getMigratedDataExecutionName()) == 0){
                            heatMapSettingsPojo.setCategory("Coverage");
                        }

                        else if (heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getDefectLeakageSITToUATName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getDefectLeakageToProductionName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getNumberOfDefectsRejectedForTheWeekName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getTotalNumberOfDefectsReopenedForTheWeekName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getDefectRejectionName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(MonthlyMetrics.getSITDefectDensityName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(MonthlyMetrics.getUATDefectDensityName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(MonthlyMetrics.getDefectLeakageSITToUATName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(MonthlyMetrics.getDefectLeakageToProductionName()) == 0){
                            heatMapSettingsPojo.setCategory("Defects");
                        }


                        if (    heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getTestCaseNotExecutedManualName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getTestCaseNotExecutedAutomationName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getPercentageOfPendingRegressionAutomationName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getPercentageOfRequirementsNotCoveredName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getAverageTimeTakenToResolveSev1DefectsName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getDowntimeAndImpactAnalysisDesignAvgProductivityLostName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getDowntimeAndImpactAnalysisExecutionAvgProductivityLostName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getTestReviewEfficiencyName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getCurrentQualityRatioName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(QuarterlyMetrics.getRevenueLeakageName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(WeeklyMetrics.getQualityOfFixesName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(MonthlyMetrics.getManualEffectivenessName()) == 0 ||
                                        heatMapSettingsPojo.getMetricName().compareTo(MonthlyMetrics.getAutomationEffectivenessName()) == 0){


                                    heatMapSettingsPojo.setCategory("Attention");
                        }



                        else if (    heatMapSettingsPojo.getMetricName().compareTo(MonthlyMetrics.getCostVarianceName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(MonthlyMetrics.getEffortVarianceName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(MonthlyMetrics.getScopeVarianceName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(MonthlyMetrics.getScheduleVarianceName()) == 0)
                                {
                            heatMapSettingsPojo.setCategory("Variance");
                        }


                        else if (heatMapSettingsPojo.getMetricName().compareTo(MonthlyMetrics.getCostPerDefectName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(MonthlyMetrics.getCostPerTestCaseName()) == 0)
                                {
                            heatMapSettingsPojo.setCategory("Cost");
                        }

                        else if (heatMapSettingsPojo.getMetricName().compareTo(MonthlyMetrics.getInProjectAutomationPercentageExtentToPotentialName()) == 0 ||
                                heatMapSettingsPojo.getMetricName().compareTo(MonthlyMetrics.getApiAutomationPercentageExtentToPotentialName()) == 0 ||
                            heatMapSettingsPojo.getMetricName().compareTo(MonthlyMetrics.getPercentageOfTCsIdentifiedForMigrationName()) == 0)

                        {
                            heatMapSettingsPojo.setCategory("Attention");
                        }


                        heatMapSettingsPojoArray.add(heatMapSettingsPojo);
                        index += 1;
                    } catch (Exception e) {

                    }
                }
            }

            Gson gson = new Gson();
            String response = gson.toJson(heatMapSettingsPojoArray);
            response = "{\"MetricsSettings\":" + response + "}";
            System.out.println(response);
            return response;

        } catch (Exception e) {

        }

        return "Error";
    }

    @PostMapping(value = "/projects/{id}/updateHeatMapSettings")
    @ResponseBody
    public String updateMonthlyMetricsHeatMapSettings(@PathVariable("id") long id,
                                                      @RequestBody List<HeatMapSettingsPojo> heatMapSettingsPojoArray) {

        try {

            Iterable<HeatMapSettings> heatMapSettingsIterable = heatMapSettingsRepository.findAll();
            Iterator<HeatMapSettings> heatMapSettingsIterator = heatMapSettingsIterable.iterator();

            while (heatMapSettingsIterator.hasNext()) {

                HeatMapSettings next = heatMapSettingsIterator.next();

                if (next.getProject().getId() == id) {

                    for (int i = 0; i < heatMapSettingsPojoArray.size(); i++) {

                        if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(next.getMetricName()) == 0) {
                            next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
                            next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
                            next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
                            next.setCategory(heatMapSettingsPojoArray.get(i).getCategory());
                            heatMapSettingsRepository.save(next);
                        }
                    }
                }
            }
        } catch (Exception e) {

        }

        return "OK";
    }
}