package com.packt.cardatabase.web;

import com.google.gson.Gson;
import com.packt.cardatabase.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fasterxml.jackson.databind.ObjectMapper;


import java.io.BufferedWriter;
import java.io.FileWriter;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.http.HttpHost;
import org.elasticsearch.action.admin.indices.refresh.RefreshRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;



@RestController
public class WeeklyMetricController {

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private ModuleRepository moduleRepository;

	@Autowired
	private WeeklyProjectLevelTestDesignProgressRepository weeklyProjectLevelTestDesignProgressRepository;

	@Autowired
	private WeeklyModuleLevelTestDesignProgressRepository weeklyModuleLevelTestDesignProgressRepository;

	@Autowired
	private WeeklyProjectLevelTestExecutionProgressRepository weeklyProjectLevelTestExecutionProgressRepository;

	@Autowired
	private WeeklyModuleLevelTestExecutionProgressRepository weeklyModuleLevelTestExecutionProgressRepository;

	@Autowired
	private LatestModuleEstimateRepository latestModuleEstimateRepository;

	@Autowired
	private LatestProjectEstimateRepository latestProjectEstimateRepository;

	@Autowired
	private WeeklyMetricsRepo weeklyMetricsRepo;

	@Autowired
	private QuarterlyProgressRepository quarterlyProgressRepository;

	@Autowired
	private QuarterMetricsRepository quarterMetricsRepository;

	@Autowired
	private MonthlyTestDesignProgressRepository monthlyTestDesignProgressRepository;

	@Autowired
	private MonthlyTestExecutionProgressRepository monthlyTestExecutionProgressRepository;

	@Autowired
	private MonthlyMetricsRepository monthlyMetricsRepository;

	@Autowired
	private MonthlyGovernanceRepository monthlyGovernanceRepository;

	@Autowired
	private WeeklyProjectGovernanceRepository weeklyProjectGovernanceRepository;

	@Autowired
	private ModuleLevelMetricsRepository moduleLevelMetricsRepository;

	@Autowired
	private WeekDataRepository weekDataRepository;

	@Autowired
	private WeekMonthDataRepository weekMonthDataRepository;

	@Autowired
	private HeatMapSettingsRepository heatMapSettingsRepository;



	/*
	@Autowired
	private NumberOfTestableRequirementsCoveredSofarRepository numberOfTestableRequirementsCoveredSofarRepository;

	@Autowired
	private RegressionAutomationTestExecutionProductivityRepository regressionAutomationTestExecutionProductivityRepository;

	@Autowired
	private APIAutomationTestExecutionProductivityRepository apiAutomationTestExecutionProductivityRepository;

	@Autowired
	private InProjectAutomationTestExecutionProductivityRepository inProjectAutomationTestExecutionProductivityRepository;

	@Autowired
	private NonExecutedManualTestCasesRepository nonExecutedManualTestCasesRepository;

	@Autowired
	private NonExecutedAutomationTestCasesRepository nonExecutedAutomationTestCasesRepository;

	@Autowired
	private QualityRatioRepository qualityRatioRepository;

	@Autowired
	private DefectRejectionRepository defectRejectionRepository;

	@Autowired
	private TestReviewEfficiencyRepository testReviewEfficiencyRepository;

	@Autowired
	private RegressionCoverageRepository regressionCoverageRepository;

	@Autowired
	private InProjectCoverageRepository inProjectCoverageRepository;

	@Autowired
	private APICoverageRepository apiCoverageRepository;

	@Autowired
	private MigrationCoverageRepository migrationCoverageRepository;

	@Autowired
	private DefectLeakageSIT2UATRepository defectLeakageSIT2UATRepository;

	@Autowired
	private DefectLeakageUAT2ProductionRepository defectLeakageUAT2ProductionRepository;

	@Autowired
	private EnvDowntimeDuringTestDesignRepository envDowntimeDuringTestDesignRepository;

	@Autowired
	private EnvDowntimeDuringTestExecutionRepository envDowntimeDuringTestExecutionRepository;
*/

	private static String WEEKLY_INDEX = "";
	private static String MONTHLY_INDEX = "";
	private static String QUARTERLY_INDEX = "";
	private static String CUMMULATIVE_INDEX = "";
	private static String CUMMULATIVE_INDEX_2 = "";
	private static String CUMMULATIVE_INDEX_9= "";
	private static String CUMMULATIVE_INDEX_TE = "";
	private static String CUMMULATIVE_INDEX_RC = "";
	private static String CUMMULATIVE_INDEX_MigrationExecution = "";
	private static String CUMMULATIVE_INDEX_IPAC = "";
	private static String CUMMULATIVE_INDEX_API = "";
	private static String CUMMULATIVE_INDEX_REG = "";
	private static String MODULE_INDEX = "";
	private static String RCA_INDEX = "";
	private static RestHighLevelClient client;
	private static final ObjectMapper mapper = new ObjectMapper();


	private static IndexRequest indexRequest(String id, String data, String index) throws IOException {

		IndexRequest request = null;

		try {
			request = new IndexRequest(index);
		} catch (Exception e) {
			System.out.println("Exception while creating the index : " + e.getMessage());
		}

		try {
			request.id(id);
			request.source(data, XContentType.JSON);
		} catch (Exception e) {
			System.out.println("The exception message in IndexRequest functiin is : >>>>>>>>> " + e.getMessage());
		}
		return request;
	}

	public static void save(String id, String data, String index) throws IOException {

		try {
			BulkRequest request = new BulkRequest();
			request.add(indexRequest(id, data, index));

			final BulkResponse response = client.bulk(request, RequestOptions.DEFAULT);
			System.out.println("The server response is : " + response);

		} catch (Exception e) {
			System.out.println("The exception message in SAVE function  is : >>>>>>>>> " + e.getMessage());
		}

	}

	public String addMetricsLCLUCLGoal(Long id, String metrics) {


		Iterable<HeatMapSettings> heatMapSettingsIterable = heatMapSettingsRepository.findAll();
		Iterator<HeatMapSettings> heatMapSettingsIterator = heatMapSettingsIterable.iterator();

		while (heatMapSettingsIterator.hasNext()) {
			HeatMapSettings next = heatMapSettingsIterator.next();

			if (next.getProject().getId() == id) {

				metrics += next.getMetricName() + "-" + next.getLCLName();
				metrics += "\":";
				metrics += next.getLCL();
				metrics += ",\"";
				metrics += next.getMetricName() + "-" + next.getUCLName();
				metrics += "\":";
				metrics += next.getUCL();
				metrics += ",\"";
				metrics += next.getMetricName() + "-" + next.getGoalName();
				metrics += "\":";
				metrics += next.getGoal();
				metrics += ",\"";

			}
		}

		return metrics;
	}



	String checkRAGStatus(Double metric, Long projectId, String metricName) {

		try {
			System.out.println("The project id is : " + projectId);
			System.out.println("The metric name is : " + metricName);
			System.out.println("The metric value is : " + metric);

			Iterable<HeatMapSettings> heatMapSettingsIterable = heatMapSettingsRepository.findAll();
			Iterator<HeatMapSettings> heatMapSettingsIterator = heatMapSettingsIterable.iterator();

			while (heatMapSettingsIterator.hasNext()) {
				HeatMapSettings next = heatMapSettingsIterator.next();

				if (next.getProject().getId() == projectId) {

					if (next.getMetricName().compareTo(metricName) == 0) {

						System.out.println("The metric name is : " + metricName);
						System.out.println("The metric value is : " + metric);

						int lclLengthIs = 0;
						int uclLengthIs = 0;
						int goalLengthIs = 0;


						try {
							System.out.println("LCL length is" + next.getLCL().length());
							System.out.println("LCL value is : " + next.getLCL());
							lclLengthIs = next.getLCL().length();
						}catch(Exception e){
							System.out.println("LCL value is NULL>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
						}

						try {
							System.out.println("UCL length is" + next.getUCL().length());
							System.out.println("UCL value is : " + next.getUCL());
							uclLengthIs = next.getUCL().length();
						}catch(Exception e){
							System.out.println("UCL value is NULL>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
						}

						try {
							System.out.println("The goal length is " + next.getGoal().length());
							System.out.println("Goal value is : " + next.getGoal());

							goalLengthIs = next.getGoal().length();
						}catch(Exception e){
							System.out.println("Goal value is NULL>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
						}

						if((lclLengthIs == 0) && (uclLengthIs > 0) && (goalLengthIs > 0))
						{
							double uclDouble = Double.parseDouble(next.getUCL());
							double goalDouble = Double.parseDouble(next.getGoal());

							if (metric <= goalDouble) {
								return "G";
							} else if ((metric > uclDouble)) {
								return "R";
							} else {
								return "A";
							}
						}

						else if((uclLengthIs  == 0) && ( lclLengthIs > 0) && (goalLengthIs > 0))
						{
							double lclDouble = Double.parseDouble(next.getLCL());
							double goalDouble = Double.parseDouble(next.getGoal());

							if (metric >= goalDouble) {
								return "G";
							} else if ((metric < lclDouble)) {
								return "R";
							} else {
								return "A";
							}
						}

					}
				}
			}
		}catch(Exception e){

		}


		return "Error";

	}


	public void generateMetricsStandAlone(Long id) {

		try {
			generateMonthlyMetrics(id);
		}catch(Exception e){

		}

		ArrayList<String> weekNumbers = new ArrayList<String>(); // Array to capture all applicable week numbers
		ArrayList<String> weekNumbersWithTestDesignProgress = new ArrayList<String>();   // Array to capture all applicable test design week numbers
		ArrayList<String> weekNumbersWithTestExecutionProgress = new ArrayList<String>(); // Array to capture all applicable test execution week numbers

		// Creating all the necessary iterators
		Iterable<WeeklyProjectLevelTestDesignProgress> weeklyProjectLevelTestDesignProgressIterable = weeklyProjectLevelTestDesignProgressRepository.findAll();
		Iterator<WeeklyProjectLevelTestDesignProgress> weeklyProjectLevelTestDesignProgressRepositoryIterator = weeklyProjectLevelTestDesignProgressIterable.iterator();

		Iterable<WeeklyProjectLevelTestExecutionProgress> weeklyProjectLevelTestExecutionProgressIterable = weeklyProjectLevelTestExecutionProgressRepository.findAll();
		Iterator<WeeklyProjectLevelTestExecutionProgress> weeklyProjectLevelTestExecutionProgressRepositoryIterator = weeklyProjectLevelTestExecutionProgressIterable.iterator();

		Iterable<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgressIterable = weeklyModuleLevelTestDesignProgressRepository.findAll();
		Iterator<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgressRepositoryIterator = weeklyModuleLevelTestDesignProgressIterable.iterator();

		Iterable<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgressIterable = weeklyModuleLevelTestExecutionProgressRepository.findAll();
		Iterator<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgressRepositoryIterator = weeklyModuleLevelTestExecutionProgressIterable.iterator();


		// The following code collects all the week numbers with both test design and test execution progress
		while (weeklyProjectLevelTestDesignProgressRepositoryIterator.hasNext()) {

			// Collect all the weeks with project level test design data. Both year number and week number is captured
			WeeklyProjectLevelTestDesignProgress next = weeklyProjectLevelTestDesignProgressRepositoryIterator.next();

			if ((next.getProject().getId() == id)) {

				int tempWeekNumber = next.getWeekNumber();

				if (tempWeekNumber < 10)
					weekNumbersWithTestDesignProgress.add(next.getYearNumber() + "-" + "0" + next.getWeekNumber());
				else
					weekNumbersWithTestDesignProgress.add(next.getYearNumber() + "-" + next.getWeekNumber());

			}

		}

		while (weeklyProjectLevelTestExecutionProgressRepositoryIterator.hasNext()) {

			// Collect all the weeks with project level test execution data. Both year number and week number is captured
			WeeklyProjectLevelTestExecutionProgress next = weeklyProjectLevelTestExecutionProgressRepositoryIterator.next();

			if ((next.getProject().getId() == id)) {
				int tempWeekNumber = next.getWeekNumber();
				if (tempWeekNumber < 10)
					weekNumbersWithTestExecutionProgress.add(next.getYearNumber() + "-" + "0" + next.getWeekNumber());
				else
					weekNumbersWithTestExecutionProgress.add(next.getYearNumber() + "-" + next.getWeekNumber());

			}

		}

		// Only those weeks with both test design and test execution data will be considered
		if (weekNumbersWithTestDesignProgress.size() > weekNumbersWithTestExecutionProgress.size()) {
			for (int i = 0; i < weekNumbersWithTestDesignProgress.size(); i++) {
				for (int j = 0; j < weekNumbersWithTestExecutionProgress.size(); j++) {
					if (weekNumbersWithTestDesignProgress.get(i).compareTo(weekNumbersWithTestExecutionProgress.get(j)) == 0) {
						weekNumbers.add(weekNumbersWithTestDesignProgress.get(i));
					}
				}
			}
		} else {
			for (int i = 0; i < weekNumbersWithTestExecutionProgress.size(); i++) {
				for (int j = 0; j < weekNumbersWithTestDesignProgress.size(); j++) {
					if (weekNumbersWithTestExecutionProgress.get(i).compareTo(weekNumbersWithTestDesignProgress.get(j)) == 0) {
						weekNumbers.add(weekNumbersWithTestExecutionProgress.get(i));
					}
				}
			}
		}

		// weekNumbers contains the final week numbers with test design and test execution
		Collections.sort(weekNumbers);
		System.out.println("The total number of weeks are : " + weekNumbers.size());

		try {

			for (int startIndex = 0; startIndex < weekNumbers.size(); startIndex++) {

				System.out.println("The loop should repeat only once for each week and year combination");
				System.out.println("The current startIndex is : " + startIndex);
				System.out.println("The current week and year being processed is : " + weekNumbers.get(startIndex));

				int weekNumberInt = 0;
				int yearNumberInt = 0;

				StringTokenizer tokenizer = new StringTokenizer(weekNumbers.get(startIndex), "-");
				String yearNumber = tokenizer.nextToken();
				String weekNumber = tokenizer.nextToken();

				System.out.println("The year number is : " + yearNumber);
				System.out.println("The week number is : " + weekNumber);
				System.out.println("The loop should repeat only once for each week and year combination : "
						+ weekNumber + " " + yearNumber);


				weekNumberInt = Integer.parseInt(weekNumber);
				yearNumberInt = Integer.parseInt(yearNumber);

				System.out.println("The year number is : " + yearNumberInt);
				System.out.println("The week number is : " + weekNumberInt);

				// Test Design
				int cummulativeNumberOfTestableRequirementsCoveredSoFar = 0; // Every week some of the requirements will be covered. Need to cummulate the same.
				int cummulativeNumberOfManualTestCasesDesignedSoFar = 0; // Every week some manual test cases will be designed. Need to cummulate the same.
				int cummulativeNumberOfAutomationTestCasesDesignedSoFar = 0; // This placeholder collects all the automation test cases designed so far. Need to cummulate the same.
				int cummulativeNumberOfRegressionAutomationTestCasesDesignedSoFar = 0; // This placeholder collects all the regression automation test cases designed so far. Need to cummulate the same.
				int cummulativeNumberOfSanityAutomationTestCasesDesignedSoFar = 0; // This placeholder collects all the sanity automation test cases designed so far. Need to cummulate the same.


				// Test Execution
				int cummulativeNumberOfRegressionAutomationTestCasesExecutedSoFar = 0; // This placeholder collects all the Regression Automation test cases executed so far
				int cummulativeNumberOfTestCasesExecutedSoFar = 0; // This place holder collects all the test cases executed so far


				// Migration
				int cummulativeNumberOfTestCasesExecutedWithMigratedDataSoFar = 0; // This place holder collects all the migration test cases executed so far

				// Defects
				int cummulativeNumberOfValidDefectsLoggedInSITSoFar = 0;
				int cummulativeNumberOfValidDefectsLoggedInUATSoFar = 0;
				int cummulativeTotalNumberOfDefectsInProductionSoFar = 0;
				int cummulativeNumberOfTestDesignReviewDefectsFoundForTheWeek = 0;
				int cummulativeNumberOfDefectsFoundUsingAutomationSoFar = 0;



				for (int cummulateIndex = 0; cummulateIndex < weekNumbers.size(); cummulateIndex++) {

					System.out.println("The loop should repeat until the desired week and year combination is hit : ");

					weeklyProjectLevelTestDesignProgressIterable = weeklyProjectLevelTestDesignProgressRepository.findAll();
					weeklyProjectLevelTestDesignProgressRepositoryIterator = weeklyProjectLevelTestDesignProgressIterable.iterator();

					weeklyProjectLevelTestExecutionProgressIterable = weeklyProjectLevelTestExecutionProgressRepository.findAll();
					weeklyProjectLevelTestExecutionProgressRepositoryIterator = weeklyProjectLevelTestExecutionProgressIterable.iterator();

					weeklyModuleLevelTestDesignProgressIterable = weeklyModuleLevelTestDesignProgressRepository.findAll();
					weeklyModuleLevelTestDesignProgressRepositoryIterator = weeklyModuleLevelTestDesignProgressIterable.iterator();

					weeklyModuleLevelTestExecutionProgressIterable = weeklyModuleLevelTestExecutionProgressRepository.findAll();
					weeklyModuleLevelTestExecutionProgressRepositoryIterator = weeklyModuleLevelTestExecutionProgressIterable.iterator();

					tokenizer = new StringTokenizer(weekNumbers.get(cummulateIndex), "-");
					String yearNumberTemp = tokenizer.nextToken();
					String weekNumberTemp = tokenizer.nextToken();

					int weekNumberTempInt = 0;
					int yearNumberTempInt = 0;

					weekNumberTempInt = Integer.parseInt(weekNumberTemp);
					yearNumberTempInt = Integer.parseInt(yearNumberTemp);


					// At any point => the current year and month will not cross the start index
					if (yearNumberTempInt > yearNumberInt) {
						break;
					} else {
						if (weekNumberTempInt > weekNumberInt) {
							break;
						} else {
							while (weeklyModuleLevelTestDesignProgressRepositoryIterator.hasNext()) {

								WeeklyModuleLevelTestDesignProgress next = weeklyModuleLevelTestDesignProgressRepositoryIterator.next();

								if (next.getProject().getId() == id) {
									if (next.getWeekNumber() == weekNumberTempInt) {
										if (next.getYearNumber() == yearNumberTempInt) {
											try {
												cummulativeNumberOfManualTestCasesDesignedSoFar += next.getNumberOfManualTestCasesDesignedForTheWeek();
												cummulativeNumberOfAutomationTestCasesDesignedSoFar += next.getNumberOfRegressionAutomationTestScriptsDesignedForTheWeek();
												cummulativeNumberOfRegressionAutomationTestCasesDesignedSoFar += next.getNumberOfRegressionAutomationTestScriptsDesignedForTheWeek();
												cummulativeNumberOfSanityAutomationTestCasesDesignedSoFar += next.getNumberOfSanityTestsDesignedForTheWeek();


												System.out.println("Module level Test design metrics cummulation : ");

												System.out.println("The matching week number used for weekly metrics cummulation is : " + weekNumberTempInt);
												System.out.println("The matching year number used for weekly metrics cummulation is : " + yearNumberTempInt);

												System.out.println("cummulativeNumberOfManualTestCasesDesignedSoFar after adding Manual test cases :  " + cummulativeNumberOfManualTestCasesDesignedSoFar + " " + weekNumberTempInt + " " + yearNumberTempInt);
												System.out.println("cummulativeNumberOfAutomationTestCasesDesignedSoFar after adding regression automation test cases : " + cummulativeNumberOfAutomationTestCasesDesignedSoFar + " " + weekNumberTempInt + " " + yearNumberTempInt);
												System.out.println("cummulativeNumberOfRegressionAutomationTestCasesDesignedSoFar after adding regression automation test cases : " + cummulativeNumberOfRegressionAutomationTestCasesDesignedSoFar + " " + weekNumberTempInt + " " + yearNumberTempInt);
												System.out.println("cummulativeNumberOfSanityAutomationTestCasesDesignedSoFar after adding sanity automation test cases : " + cummulativeNumberOfSanityAutomationTestCasesDesignedSoFar + " " + weekNumberTempInt + " " + yearNumberTempInt);



											} catch (Exception e) {
												System.out.println("The exception message is : " + e.getMessage());
											}
											break;

										}
									}
								}
							}


							while (weeklyProjectLevelTestDesignProgressRepositoryIterator.hasNext()) {

								WeeklyProjectLevelTestDesignProgress next = weeklyProjectLevelTestDesignProgressRepositoryIterator.next();

								if (next.getProject().getId() == id) {
									if (next.getWeekNumber() == weekNumberTempInt) {
										if (next.getYearNumber() == yearNumberTempInt) {

											System.out.println("Project level Test design metrics cummulation : ");
											System.out.println("The matching week number used for weekly metrics cummulation is : " + weekNumberTempInt);
											System.out.println("The matching year number used for weekly metrics cummulation is : " + yearNumberTempInt);

											cummulativeNumberOfAutomationTestCasesDesignedSoFar += next.getTotalNumberOfInProjectAutomationTestCasesAutomated();
											System.out.println("cummulativeNumberOfAutomationTestCases after adding InProject test cases: " + cummulativeNumberOfAutomationTestCasesDesignedSoFar + " " + weekNumberTempInt + " " + yearNumberTempInt);
											cummulativeNumberOfAutomationTestCasesDesignedSoFar += next.getTotalNumberOfAPIAutomationTestCasesAutomated();
											System.out.println("cummulativeNumberOfAutomationTestCasesDesignedSoFar after adding API test cases: " + cummulativeNumberOfAutomationTestCasesDesignedSoFar + " " + weekNumberTempInt + " " + yearNumberTempInt);
											cummulativeNumberOfTestableRequirementsCoveredSoFar += next.getNumberOfTestableRequirementsCoveredThisWeek();
											System.out.println("cummulativeNumberOfTestableRequirementsCoveredSoFar : " + cummulativeNumberOfTestableRequirementsCoveredSoFar + " " + weekNumberTempInt + " " + yearNumberTempInt);
											cummulativeNumberOfTestDesignReviewDefectsFoundForTheWeek += next.getTotalNumberOfTestDesignReviewDefectsFound();
											System.out.println("cummulativeNumberOfTestDesignReviewDefectsFoundForTheWeek : " + cummulativeNumberOfTestDesignReviewDefectsFoundForTheWeek + " " + weekNumberTempInt + " " + yearNumberTempInt);
											break;

										}
									}
								}
							}


							while (weeklyModuleLevelTestExecutionProgressRepositoryIterator.hasNext()) {
								System.out.println("Module level Test execution metrics cummulation : ");

								WeeklyModuleLevelTestExecutionProgress next = weeklyModuleLevelTestExecutionProgressRepositoryIterator.next();

								if (next.getProject().getId() == id) {
									if (next.getWeekNumber() == weekNumberTempInt) {
										int diff = next.getYearNumber() - yearNumberTempInt;
										if (diff == 0) {

											System.out.println("The temporary week number is : " + weekNumberTempInt);
											System.out.println("The temporary year number is : " + yearNumberTempInt);

											cummulativeNumberOfTestCasesExecutedSoFar += next.getNumberOfManualTestCasesExecutedForTheWeek();
											System.out.println("cummulativeNumberOfTestCasesExecutedSoFar after adding manual executed test cases: " + cummulativeNumberOfTestCasesExecutedSoFar + " " + weekNumberTempInt + " " + yearNumberTempInt);
											cummulativeNumberOfTestCasesExecutedSoFar += next.getNumberOfRegressionAutomationTestCasesExecutedForTheWeek();
											System.out.println("cummulativeNumberOfTestCasesExecutedSoFar after adding regression executed test cases: " + cummulativeNumberOfTestCasesExecutedSoFar + " " + weekNumberTempInt + " " + yearNumberTempInt);
											cummulativeNumberOfRegressionAutomationTestCasesExecutedSoFar += next.getNumberOfRegressionAutomationTestCasesExecutedForTheWeek();
											System.out.println("cummulativeNumberOfRegressionAutomationTestCasesExecutedSoFar : " + cummulativeNumberOfRegressionAutomationTestCasesExecutedSoFar + " " + weekNumberTempInt + " " + yearNumberTempInt);


											break;
										}

									}
								}
							}


							while (weeklyProjectLevelTestExecutionProgressRepositoryIterator.hasNext()) {
								System.out.println("Project level Test execution metrics cummulation : ");

								WeeklyProjectLevelTestExecutionProgress next = weeklyProjectLevelTestExecutionProgressRepositoryIterator.next();

								if (next.getProject().getId() == id) {
									if (next.getWeekNumber() == weekNumberTempInt) {
										int diff = next.getYearNumber() - yearNumberTempInt;
										if (diff == 0) {

											cummulativeNumberOfTestCasesExecutedWithMigratedDataSoFar += next.getNumberOfTestCasesExecutedWithMigratedData();
											System.out.println("cummulativeNumberOfTestCasesExecutedWithMigratedDataSoFar : " + cummulativeNumberOfTestCasesExecutedWithMigratedDataSoFar + " " + weekNumberTempInt + " " + yearNumberTempInt);

											cummulativeNumberOfValidDefectsLoggedInSITSoFar += next.getNumberOfValidDefectsLoggedInSIT();
											System.out.println("cummulativeNumberOfValidDefectsLoggedInSITSoFar : " + cummulativeNumberOfValidDefectsLoggedInSITSoFar + " " + weekNumberTempInt + " " + yearNumberTempInt);

											cummulativeNumberOfValidDefectsLoggedInUATSoFar += next.getNumberOfValidDefectsLoggedInUAT();
											System.out.println("cummulativeNumberOfValidDefectsLoggedInUATSoFar : " + cummulativeNumberOfValidDefectsLoggedInUATSoFar + " " + weekNumberTempInt + " " + yearNumberTempInt);

											cummulativeTotalNumberOfDefectsInProductionSoFar += next.getTotalNumberOfDefectsInProduction();
											System.out.println("cummulativeTotalNumberOfDefectsInProductionSoFar : " + cummulativeTotalNumberOfDefectsInProductionSoFar + " " + weekNumberTempInt + " " + yearNumberTempInt);

											cummulativeNumberOfTestCasesExecutedSoFar += next.getNumberOfAPIAutomationTestScriptsExecuted();
											System.out.println("cummulativeNumberOfTestCasesExecutedSoFar after adding API test cases executed : " + cummulativeNumberOfTestCasesExecutedSoFar + " " + weekNumberTempInt + " " + yearNumberTempInt);

											cummulativeNumberOfTestCasesExecutedSoFar += next.getNumberOfInProjectAutomationTestScriptsExecuted();
											System.out.println("cummulativeNumberOfTestCasesExecutedSoFar after adding InProject test cases executed : " + cummulativeNumberOfTestCasesExecutedSoFar + " " + weekNumberTempInt + " " + yearNumberTempInt);

											cummulativeNumberOfDefectsFoundUsingAutomationSoFar += next.getNumberOfDefectsFoundUsingAutomatedRegression();
											System.out.println("cummulativeNumberOfDefectsFoundUsingAutomationSoFar : " + cummulativeNumberOfDefectsFoundUsingAutomationSoFar + " " + weekNumberTempInt + " " + yearNumberTempInt);

											break;

										}
									}
								}
							}
						}
					}
				}


				// Reinitialize all the iterators
				weeklyProjectLevelTestDesignProgressIterable = weeklyProjectLevelTestDesignProgressRepository.findAll();
				weeklyProjectLevelTestDesignProgressRepositoryIterator = weeklyProjectLevelTestDesignProgressIterable.iterator();

				weeklyProjectLevelTestExecutionProgressIterable = weeklyProjectLevelTestExecutionProgressRepository.findAll();
				weeklyProjectLevelTestExecutionProgressRepositoryIterator = weeklyProjectLevelTestExecutionProgressIterable.iterator();

				weeklyModuleLevelTestDesignProgressIterable = weeklyModuleLevelTestDesignProgressRepository.findAll();
				weeklyModuleLevelTestDesignProgressRepositoryIterator = weeklyModuleLevelTestDesignProgressIterable.iterator();

				weeklyModuleLevelTestExecutionProgressIterable = weeklyModuleLevelTestExecutionProgressRepository.findAll();
				weeklyModuleLevelTestExecutionProgressRepositoryIterator = weeklyModuleLevelTestExecutionProgressIterable.iterator();

				int numberOfManualTestCasesDesignedForTheWeek = 0;
				int numberOfRegressionAutomationTestCasesDesignedForTheWeek = 0;
				int numberOfAPIAutomationTestCasesDesignedForTheWeek = 0;
				int numberOfInProjectAutomationTestCasesDesignedForTheWeek = 0;


				// Design Effort
				int manualTestCasesDesignEffortForTheWeek = 0;
				int regressionAutomationTestCasesDesignEffortForTheWeek = 0;
				int apiAutomationTestCasesDesignEffortForTheWeek = 0;
				int inProjectAutomationTestCasesDesignEffortForTheWeek = 0;

				int numberOfResourcesAuthoringManualTestCasesForTheWeek = 0;
				int numberOfResourcesAuthoringAutomationTestCasesForTheWeek = 0;
				int totalNumberOfResourcesAuthoringTestCasesForTheWeek = 0;

				// Number of test scripts executed for the week
				int numberOfManualTestCasesExecutedForTheWeek = 0;
				int numberOfRegressionAutomationTestCasesExecutedForTheWeek = 0;
				int numberOfInProjectAutomationTestScriptsExecutedforTheWeek = 0;
				int numberOfAPIAutomationTestScriptsExecutedForTheWeek = 0;

				int numberOfResourcesExecutingManualTestCasesForTheWeek = 0;
				int numberOfResourcesExecutingRegressionAutomationForTheWeek = 0 ;
				int numberOfResourcesExecutingInProjectAutomationForTheWeek = 0;
				int numberOfResourcesExecutingAPIAutomationForTheWeek = 0;
				int totalNumberOfResourcesExecutingTestCasesForTheWeek = 0;

				int totalNumberOfManualTestCasesNotExecutedForTheWeek = 0;
				int totalNumberOfAutomationTestCasesNotExecutedForTheWeek = 0;



				// Execution Effort
				int manualTestCaseExecutionEffortForTheWeek = 0;
				int regressionAutomationTestCaseExecutionEffortForTheWeek = 0;
				int inProjectAutomationTestCaseExecutionEffortForTheWeek = 0;
				int apiAutomationTestCaseExecutionEffortForTheWeek = 0;


				// Defects
				int totalNumberOfTestDesignReviewDefectsFoundForTheWeek = 0;
				int numberOfDefectsReportedByTestingTeamforTheWeek = 0;
				int numberOfDefectsRejectedForTheWeek = 0;
				int numberOfDefectsFixedForTheWeek = 0;
				int totalNumberOfDefectsReopenedForTheWeek = 0;
				int numberOfTestCasesSuccessfullyExecutedWithoutDefectsForTheWeek = 0;

				int numberOfValidDefectsLoggedInSITForTheWeek = 0;
				int numberOfValidDefectsLoggedInUATForTheWeek = 0;

				int averageTimeTakenToResolveSev1Defects = 0;


				// Downtime
				int environmentDowntimeDuringTestDesignPerPersonInHoursForTheWeek = 0;
				int environmentDowntimeDuringTestExecutionPerPersonInHoursForTheWeek = 0;

				Iterable<ModuleLevelMetrics> moduleLevelMetricsIterable = moduleLevelMetricsRepository.findAll();
				Iterator<ModuleLevelMetrics> moduleLevelMetricsIterator = moduleLevelMetricsIterable.iterator();

				while (weeklyModuleLevelTestDesignProgressRepositoryIterator.hasNext()) {

					WeeklyModuleLevelTestDesignProgress next = weeklyModuleLevelTestDesignProgressRepositoryIterator.next();

					if ((next.getProject().getId() == id)) {

						if (next.getWeekNumber() == weekNumberInt) {
							int tempDiff = next.getYearNumber() - yearNumberInt;
							if (tempDiff == 0) {

								System.out.println("The desired week is " + weekNumberInt);

								numberOfManualTestCasesDesignedForTheWeek += next.getNumberOfManualTestCasesDesignedForTheWeek();
								System.out.println("Number of manual test cases designed for the week " + numberOfManualTestCasesDesignedForTheWeek + " " + weekNumberInt + " " + yearNumberInt);

								numberOfRegressionAutomationTestCasesDesignedForTheWeek += next.getNumberOfRegressionAutomationTestScriptsDesignedForTheWeek();
								System.out.println("Manual test cases design effort for the week " + numberOfRegressionAutomationTestCasesDesignedForTheWeek + " " + weekNumberInt + " " + yearNumberInt);

								boolean moduleLevelMetricFound = false;
								ModuleLevelMetrics nextModuleMetrics = null;

								while (moduleLevelMetricsIterator.hasNext()) {

									nextModuleMetrics = moduleLevelMetricsIterator.next();

									if ((nextModuleMetrics.getProject().getId() == id) &&
											(nextModuleMetrics.getWeekNumber() == next.getWeekNumber()) &&
											(nextModuleMetrics.getModuleName().compareTo(next.getModuleName()) == 0)) {

										int diff = next.getYearNumber() - yearNumberInt;
										if (diff == 0) {

											moduleLevelMetricFound = true;
											nextModuleMetrics.setNumberOfManualTestCasesAuthored(next.getNumberOfManualTestCasesDesignedForTheWeek());
											nextModuleMetrics.setNumberOfRegressionAutomationTestCasesScripted(next.getNumberOfRegressionAutomationTestScriptsDesignedForTheWeek());

											/*

											Double numberOfManualTestCasesDesignedForTheModuleDouble = new Double(next.getNumberOfManualTestCasesDesignedForTheWeek());
											Double manualTestCasesDesignEffortForTheModuleDouble = new Double(next.getManualTestCasesDesignEffortForTheWeek());
											Double tempModuleLevelMetric = (numberOfManualTestCasesDesignedForTheModuleDouble) / (manualTestCasesDesignEffortForTheModuleDouble);

											if (tempModuleLevelMetric == 0) {
												nextModuleMetrics.setManualTestCaseAuthoringProductivity("0.0");
											} else {
												tempModuleLevelMetric = (Math.round(tempModuleLevelMetric * 100.0) / 100.0);
												nextModuleMetrics.setManualTestCaseAuthoringProductivity(tempModuleLevelMetric.toString());
											}


											Double numberOfAutomationTestScriptsDesignedForTheWeekDouble = new Double(next.getNumberOfRegressionAutomationTestScriptsDesignedForTheWeek());
											Double automationTestCasesDesignEffortForTheModuleDouble = new Double(next.getRegressionAutomationDesignEffortForTheWeek());
											tempModuleLevelMetric = ((numberOfAutomationTestScriptsDesignedForTheWeekDouble) / (automationTestCasesDesignEffortForTheModuleDouble));

											if (tempModuleLevelMetric == 0) {
												nextModuleMetrics.setAutomationTestCaseScriptingProductivity("0.0");
											} else {
												tempModuleLevelMetric = (Math.round(tempModuleLevelMetric * 100.0) / 100.0);
												nextModuleMetrics.setAutomationTestCaseScriptingProductivity(tempModuleLevelMetric.toString());
											}

											 */

											moduleLevelMetricsRepository.save(nextModuleMetrics);
											break;
										}
									}
								}


								if (moduleLevelMetricFound == false) {

									nextModuleMetrics = new ModuleLevelMetrics();
									nextModuleMetrics.setWeekNumber(next.getWeekNumber());
									nextModuleMetrics.setYearNumber(next.getYearNumber());
									nextModuleMetrics.setProject(next.getProject());
									nextModuleMetrics.setModuleName(next.getModuleName());
									nextModuleMetrics.setModuleId(next.getModuleId());
									nextModuleMetrics.setNumberOfManualTestCasesAuthored(next.getNumberOfManualTestCasesDesignedForTheWeek());
									nextModuleMetrics.setNumberOfRegressionAutomationTestCasesScripted(next.getNumberOfRegressionAutomationTestScriptsDesignedForTheWeek());

/*
									Double numberOfManualTestCasesDesignedForTheModuleDouble = new Double(next.getNumberOfManualTestCasesDesignedForTheWeek());
									Double manualTestCasesDesignEffortForTheModuleDouble = new Double(next.getManualTestCasesDesignEffortForTheWeek());
									Double tempModuleLevelMetric = ((numberOfManualTestCasesDesignedForTheModuleDouble) / (manualTestCasesDesignEffortForTheModuleDouble));

									if (tempModuleLevelMetric == 0) {
										nextModuleMetrics.setManualTestCaseAuthoringProductivity("0.0");
									} else {
										tempModuleLevelMetric = (Math.round(tempModuleLevelMetric * 100.0) / 100.0);
										nextModuleMetrics.setManualTestCaseAuthoringProductivity(tempModuleLevelMetric.toString());
									}

									Double numberOfAutomationTestScriptsDesignedForTheWeekDouble = new Double(next.getNumberOfRegressionAutomationTestScriptsDesignedForTheWeek());
									Double automationTestCasesDesignEffortForTheModuleDouble = new Double(next.getRegressionAutomationDesignEffortForTheWeek());
									tempModuleLevelMetric = ((numberOfAutomationTestScriptsDesignedForTheWeekDouble) / (automationTestCasesDesignEffortForTheModuleDouble));

									if (tempModuleLevelMetric == 0) {
										nextModuleMetrics.setAutomationTestCaseScriptingProductivity("0.0");
									} else {
										tempModuleLevelMetric = (Math.round(tempModuleLevelMetric * 100.0) / 100.0);
										nextModuleMetrics.setAutomationTestCaseScriptingProductivity(tempModuleLevelMetric.toString());
									}

 */
									moduleLevelMetricsRepository.save(nextModuleMetrics);
								}
							}
						}
					}
				}


				moduleLevelMetricsIterable = moduleLevelMetricsRepository.findAll();
				moduleLevelMetricsIterator = moduleLevelMetricsIterable.iterator();


				while (weeklyModuleLevelTestExecutionProgressRepositoryIterator.hasNext()) {

					WeeklyModuleLevelTestExecutionProgress next = weeklyModuleLevelTestExecutionProgressRepositoryIterator.next();

					if ((next.getProject().getId() == id)) {
						if (next.getWeekNumber() == weekNumberInt) {
							if (next.getYearNumber() == yearNumberInt) {

								System.out.println("The desired week is " + weekNumberInt);
								System.out.println("The desired year is " + yearNumberInt);

								numberOfManualTestCasesExecutedForTheWeek += next.getNumberOfManualTestCasesExecutedForTheWeek();
								System.out.println("Number of manual test cases executed for the week " + numberOfManualTestCasesExecutedForTheWeek + " " + weekNumberInt + " " + yearNumberInt);

								numberOfRegressionAutomationTestCasesExecutedForTheWeek += next.getNumberOfRegressionAutomationTestCasesExecutedForTheWeek();
								System.out.println("Number of regression test cases executed for the week " + numberOfRegressionAutomationTestCasesExecutedForTheWeek + " " + weekNumberInt + " " + yearNumberInt);

								numberOfDefectsReportedByTestingTeamforTheWeek += next.getDefectsForTheWeek();
								System.out.println("Number of defects reported for the week " + numberOfDefectsReportedByTestingTeamforTheWeek + " " + weekNumberInt + " " + yearNumberInt);

								boolean moduleLevelMetricFound = false;
								ModuleLevelMetrics nextModuleMetrics = null;

								while (moduleLevelMetricsIterator.hasNext()) {

									nextModuleMetrics = moduleLevelMetricsIterator.next();

									if (nextModuleMetrics.getProject().getId() == id) {
										if (nextModuleMetrics.getWeekNumber() == next.getWeekNumber()) {
											if (nextModuleMetrics.getYearNumber() == next.getYearNumber()) {
												if (nextModuleMetrics.getModuleName().compareTo(next.getModuleName()) == 0) {

													System.out.println("The desired week is " + next.getWeekNumber());
													System.out.println("The desired year is " + next.getYearNumber());
													System.out.println("The module name is " + next.getModuleName());

													moduleLevelMetricFound = true;
													nextModuleMetrics.setNumberOfManualTestCasesExecuted(next.getNumberOfManualTestCasesExecutedForTheWeek());
													nextModuleMetrics.setNumberOfRegressionAutomationTestCasesExecuted(next.getNumberOfRegressionAutomationTestCasesExecutedForTheWeek());

													Double numberOfManualTestCasesExecutedForTheModuleDouble = new Double(next.getNumberOfManualTestCasesExecutedForTheWeek());
													//Double manualTestCasesExecutionEffortForTheModuleDouble = new Double(next.getManualTestExecutionEffortForTheWeek());
													Double tempModuleLevelMetric = 0.0;

													System.out.println("Calculating Module level ManualTestExecutionProductivity for module " + next.getModuleName() + "for week number " + next.getWeekNumber() + " and year " + next.getYearNumber());

													/*try {
														tempModuleLevelMetric = ((numberOfManualTestCasesExecutedForTheModuleDouble) / (manualTestCasesExecutionEffortForTheModuleDouble));
													}catch(Exception e) {
														System.out.println("Exception occured while calculating ManualTestExecutionProductivity : " + e.getMessage());
													}*/

													if (tempModuleLevelMetric == 0) {
														System.out.println("Inside ManualTestExecutionProductivity - 0.0 scenario");
														nextModuleMetrics.setManualTestExecutionProductivity("0.0");
													} else {
														tempModuleLevelMetric = (Math.round(tempModuleLevelMetric * 100.0) / 100.0);
														nextModuleMetrics.setManualTestExecutionProductivity(tempModuleLevelMetric.toString());
														System.out.println("Inside ManualTestExecutionProductivity valid scenario");
													}



													Double numberOfAutomationTestScriptsExecutedForTheWeekDouble = new Double(next.getNumberOfRegressionAutomationTestCasesExecutedForTheWeek());
													//Double automationTestCasesExecutionEffortForTheModuleDouble = new Double(next.getRegressionAutomationExecutionEffortForTheWeek());
													System.out.println("Calculating Module level AutomationTestExecutionProductivity for module " + next.getModuleName() + "for week number " + next.getWeekNumber() + " and year " + next.getYearNumber());

													tempModuleLevelMetric = 0.0;
													/*try {
														tempModuleLevelMetric = ((numberOfAutomationTestScriptsExecutedForTheWeekDouble) / (automationTestCasesExecutionEffortForTheModuleDouble));
													}catch(Exception e){
														   tempModuleLevelMetric = 0.0;
														System.out.println("Exception occured while calculating AutomationTestExecutionProductivity : " + e.getMessage());
													}*/

													if (tempModuleLevelMetric == 0) {
														System.out.println("Inside AutomationTestExecutionProductivity - 0.0 scenario");
														nextModuleMetrics.setAutomationTestExecutionProductivity("0.0");
													} else {
														tempModuleLevelMetric = (Math.round(tempModuleLevelMetric * 100.0) / 100.0);
														nextModuleMetrics.setAutomationTestExecutionProductivity(tempModuleLevelMetric.toString());
														System.out.println("Inside AutomationTestExecutionProductivity valid scenario");

													}


													nextModuleMetrics.setDefectsForTheWeek(Integer.toString(next.getDefectsForTheWeek()));
													System.out.println("Before moduleLevelMetricsRepository save");
													moduleLevelMetricsRepository.save(nextModuleMetrics);
													System.out.println("Before moduleLevelMetricsRepository successful save");

													break;
												}
											}
										}
									}
								}


								if (moduleLevelMetricFound == false) {

									System.out.println("The desired week is " + next.getWeekNumber());
									System.out.println("The desired year is " + next.getYearNumber());
									System.out.println("The module name is " + next.getModuleName());


									nextModuleMetrics = new ModuleLevelMetrics();
									nextModuleMetrics.setWeekNumber(next.getWeekNumber());
									nextModuleMetrics.setYearNumber(next.getYearNumber());
									nextModuleMetrics.setProject(next.getProject());
									nextModuleMetrics.setModuleName(next.getModuleName());

									nextModuleMetrics.setNumberOfManualTestCasesExecuted(next.getNumberOfManualTestCasesExecutedForTheWeek());
									nextModuleMetrics.setNumberOfRegressionAutomationTestCasesExecuted(next.getNumberOfRegressionAutomationTestCasesExecutedForTheWeek());

									Double numberOfManualTestCasesExecutedForTheModuleDouble = new Double(next.getNumberOfManualTestCasesExecutedForTheWeek());
									//Double manualTestCasesExecutionEffortForTheModuleDouble = new Double(next.getManualTestExecutionEffortForTheWeek());
									Double tempModuleLevelMetric = 0.0;
								    /*try {
										tempModuleLevelMetric = ((numberOfManualTestCasesExecutedForTheModuleDouble) / (manualTestCasesExecutionEffortForTheModuleDouble));
									}catch(Exception e) {
										System.out.println("Exception occured while calculating first time ManualTestExecutionProductivity : " + e.getMessage());
									}

								     */

									if (tempModuleLevelMetric == 0) {
										nextModuleMetrics.setManualTestExecutionProductivity("0.0");
									} else {
										tempModuleLevelMetric = (Math.round(tempModuleLevelMetric * 100.0) / 100.0);
										nextModuleMetrics.setManualTestExecutionProductivity(tempModuleLevelMetric.toString());
									}

									Double numberOfAutomationTestCasesExecutedForTheModuleDouble = new Double(next.getNumberOfRegressionAutomationTestCasesExecutedForTheWeek());
									//Double automationExecutionEffortForTheModuleDouble = new Double(next.getRegressionAutomationExecutionEffortForTheWeek());

									tempModuleLevelMetric = 0.0;
									/*    try {
											tempModuleLevelMetric = ((numberOfAutomationTestCasesExecutedForTheModuleDouble) / (automationExecutionEffortForTheModuleDouble));
										}catch(Exception e){
											System.out.println("Exception occured while calculating first time AutomationTestExecutionProductivity : " + e.getMessage());
										}

									 */

									if (tempModuleLevelMetric == 0) {
										nextModuleMetrics.setAutomationTestExecutionProductivity("0.0");
									} else {
										tempModuleLevelMetric = (Math.round(tempModuleLevelMetric * 100.0) / 100.0);
										nextModuleMetrics.setAutomationTestExecutionProductivity(tempModuleLevelMetric.toString());
									}

									nextModuleMetrics.setDefectsForTheWeek(Integer.toString(next.getDefectsForTheWeek()));
									moduleLevelMetricsRepository.save(nextModuleMetrics);
								}
							}
						}
					}
				}


				while (weeklyProjectLevelTestDesignProgressRepositoryIterator.hasNext()) {

					WeeklyProjectLevelTestDesignProgress next = weeklyProjectLevelTestDesignProgressRepositoryIterator.next();

					if ((next.getProject().getId() == id)) {
						if (next.getWeekNumber() == weekNumberInt) {
							if (next.getYearNumber() == yearNumberInt) {

								System.out.println("The desired week is " + weekNumberInt);
								System.out.println("The desired year is " + yearNumberInt);
								System.out.println("Fetching Project Level Test Design Progress for the week " + weekNumberInt);

								numberOfAPIAutomationTestCasesDesignedForTheWeek = next.getTotalNumberOfAPIAutomationTestCasesAutomated();
								System.out.println("Number of API automation test cases designed for the week " + numberOfAPIAutomationTestCasesDesignedForTheWeek + " " + weekNumberInt + " " + yearNumberInt);

								numberOfInProjectAutomationTestCasesDesignedForTheWeek = next.getTotalNumberOfInProjectAutomationTestCasesAutomated();
								System.out.println("Number of In Sprint automation test cases designed for the week " + numberOfInProjectAutomationTestCasesDesignedForTheWeek + " " + weekNumberInt + " " + yearNumberInt);

								manualTestCasesDesignEffortForTheWeek = next.getManualTestCasesDesignEffort();
								System.out.println("Manual test case design effort for the week " + regressionAutomationTestCasesDesignEffortForTheWeek + " " + weekNumberInt + " " + yearNumberInt);

								regressionAutomationTestCasesDesignEffortForTheWeek = next.getRegressionAutomationDesignEffort();
								System.out.println("Regression automation design effort for the week " + regressionAutomationTestCasesDesignEffortForTheWeek + " " + weekNumberInt + " " + yearNumberInt);

								apiAutomationTestCasesDesignEffortForTheWeek = next.getApiAutomationDesignEffort();
								System.out.println("API automation design effort for the week " + apiAutomationTestCasesDesignEffortForTheWeek + " " + weekNumberInt + " " + yearNumberInt);

								inProjectAutomationTestCasesDesignEffortForTheWeek = next.getInProjectAutomationDesignEffort();
								System.out.println("In Sprint automation design effort for the week " + inProjectAutomationTestCasesDesignEffortForTheWeek + " " + weekNumberInt + " " + yearNumberInt);

								environmentDowntimeDuringTestDesignPerPersonInHoursForTheWeek = next.getEnvironmentDowntimeDuringTestDesignPhase();
								System.out.println("Environment Downtime during test design for the week " + environmentDowntimeDuringTestDesignPerPersonInHoursForTheWeek + " " + weekNumberInt + " " + yearNumberInt);

								totalNumberOfResourcesAuthoringTestCasesForTheWeek = next.getNumberOfResourcesAuthoringManualTestCases();
								numberOfResourcesAuthoringManualTestCasesForTheWeek = next.getNumberOfResourcesAuthoringManualTestCases();
								System.out.println("Number of resources working on manual test case design " + next.getNumberOfResourcesAuthoringManualTestCases() + " " + weekNumberInt + " " + yearNumberInt);

								totalNumberOfResourcesAuthoringTestCasesForTheWeek  += next.getNumberOfResourcesAuthoringAutomationTestCases();
								numberOfResourcesAuthoringAutomationTestCasesForTheWeek = next.getNumberOfResourcesAuthoringAutomationTestCases();

								System.out.println("Number of resources working on automation test case design " + next.getNumberOfResourcesAuthoringAutomationTestCases() + " " + weekNumberInt + " " + yearNumberInt);

								System.out.println("Number of resources working on test case design for the week " + totalNumberOfResourcesAuthoringTestCasesForTheWeek + " " + weekNumberInt + " " + yearNumberInt);

								break;
							}

						}
					}
				}


				while (weeklyProjectLevelTestExecutionProgressRepositoryIterator.hasNext()) {

					WeeklyProjectLevelTestExecutionProgress next = weeklyProjectLevelTestExecutionProgressRepositoryIterator.next();

					if ((next.getProject().getId() == id)) {
						if (next.getWeekNumber() == weekNumberInt) {
							if (next.getYearNumber() == yearNumberInt) {

								System.out.println("The desired week is " + weekNumberInt);
								System.out.println("The desired year is " + yearNumberInt);
								System.out.println("Fetching Project Level Test Execution Progress for the week " + weekNumberInt);

								numberOfInProjectAutomationTestScriptsExecutedforTheWeek = next.getNumberOfInProjectAutomationTestScriptsExecuted();
								System.out.println("Number of In Sprint automation test cases executed for the week " + numberOfInProjectAutomationTestScriptsExecutedforTheWeek + " " + weekNumberInt + " " + yearNumberInt);

								numberOfAPIAutomationTestScriptsExecutedForTheWeek = next.getNumberOfAPIAutomationTestScriptsExecuted();
								System.out.println("Number of API Sprint automation test cases executed for the week " + numberOfAPIAutomationTestScriptsExecutedForTheWeek + " " + weekNumberInt + " " + yearNumberInt);

								manualTestCaseExecutionEffortForTheWeek = next.getManualTestCaseExecutionEffort();
								regressionAutomationTestCaseExecutionEffortForTheWeek = next.getRegressionAutomationTestCaseExecutionEffort();
								inProjectAutomationTestCaseExecutionEffortForTheWeek = next.getInProjectAutomationTestCaseExecutionEffort();
								apiAutomationTestCaseExecutionEffortForTheWeek = next.getAPIAutomationTestCaseExecutionEffort();

								System.out.println("Manual test case execution effort for the week " + manualTestCaseExecutionEffortForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
								System.out.println("Regression test case execution effort for the week " + regressionAutomationTestCaseExecutionEffortForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
								System.out.println("In Sprint test case execution effort for the week " + inProjectAutomationTestCaseExecutionEffortForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
								System.out.println("API automation test case execution effort for the week " + apiAutomationTestCaseExecutionEffortForTheWeek + " " + weekNumberInt + " " + yearNumberInt);

								totalNumberOfManualTestCasesNotExecutedForTheWeek = next.getTotalNumberOfManualTestCasesNotExecuted();
								totalNumberOfAutomationTestCasesNotExecutedForTheWeek = next.getTotalNumberOfAutomationTestCasesNotExecuted();
								numberOfTestCasesSuccessfullyExecutedWithoutDefectsForTheWeek = next.getNumberOfTestCasesSuccessfullyExecutedWithoutDefects();

								System.out.println("Number of manual test cases not executed for the week " + totalNumberOfManualTestCasesNotExecutedForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
								System.out.println("Number of automation test cases not execution effort for the week " + totalNumberOfAutomationTestCasesNotExecutedForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
								System.out.println("Number of test cases successfully executed without defects for the week " + numberOfTestCasesSuccessfullyExecutedWithoutDefectsForTheWeek + " " + weekNumberInt + " " + yearNumberInt);

								environmentDowntimeDuringTestExecutionPerPersonInHoursForTheWeek = next.getEnvironmentDowntimeDuringTestExecutionPerPersonInHours();
								System.out.println("Environment Downtime during test execution for the week " + environmentDowntimeDuringTestExecutionPerPersonInHoursForTheWeek + " " + weekNumberInt + " " + yearNumberInt);

								numberOfResourcesExecutingManualTestCasesForTheWeek = next.getNumberOfResourcesForManualTestCaseExecution();
								System.out.println("Number of resources working on manual test case execution " + next.getNumberOfResourcesForManualTestCaseExecution() + " " + weekNumberInt + " " + yearNumberInt);

								numberOfResourcesExecutingRegressionAutomationForTheWeek += next.getNumberOfResourceForRegressionAutomationExecution();
								System.out.println("Number of resources working on automation test case execution " + next.getNumberOfResourceForRegressionAutomationExecution() + " " + weekNumberInt + " " + yearNumberInt);

								numberOfResourcesExecutingInProjectAutomationForTheWeek += next.getNumberOfResourceForInProjectAutomationExecution();
								System.out.println("Number of resources working on automation test case execution " + next.getNumberOfResourceForInProjectAutomationExecution() + " " + weekNumberInt + " " + yearNumberInt);

								numberOfResourcesExecutingAPIAutomationForTheWeek += next.getNumberOfResourceForAPIAutomationExecution();
								System.out.println("Number of resources working on automation test case execution " + next.getNumberOfResourceForAPIAutomationExecution() + " " + weekNumberInt + " " + yearNumberInt);

								totalNumberOfResourcesExecutingTestCasesForTheWeek = numberOfResourcesExecutingManualTestCasesForTheWeek +
										numberOfResourcesExecutingRegressionAutomationForTheWeek +
										numberOfResourcesExecutingInProjectAutomationForTheWeek +
										numberOfResourcesExecutingAPIAutomationForTheWeek;


								numberOfDefectsReportedByTestingTeamforTheWeek = next.getNumberOfDefectsReportedByTestingTeam();
								System.out.println("Number of defects reported by the testing team for the week " + numberOfDefectsReportedByTestingTeamforTheWeek + " " + weekNumberInt + " " + yearNumberInt);

								numberOfDefectsFixedForTheWeek = next.getTotalNumberOfDefectsFixed();
								System.out.println("Number of defects fixed for the week " + numberOfDefectsFixedForTheWeek + " " + weekNumberInt + " " + yearNumberInt);

								numberOfDefectsRejectedForTheWeek = next.getNumberOfDefectsRejected();
								System.out.println("Number of defects rejected for the week " + numberOfDefectsRejectedForTheWeek + " " + weekNumberInt + " " + yearNumberInt);

								totalNumberOfDefectsReopenedForTheWeek = next.getTotalNumberOfDefectsReopened();
								System.out.println("Number of defects reopened for the week " + totalNumberOfDefectsReopenedForTheWeek + " " + weekNumberInt + " " + yearNumberInt);

								numberOfValidDefectsLoggedInSITForTheWeek = next.getNumberOfValidDefectsLoggedInSIT();
								System.out.println("Number of valid defects in SIT for the week " + numberOfValidDefectsLoggedInSITForTheWeek + " " + weekNumberInt + " " + yearNumberInt);

								numberOfValidDefectsLoggedInUATForTheWeek = next.getNumberOfValidDefectsLoggedInUAT();
								System.out.println("Number of valid defects in UAT for the week " + numberOfValidDefectsLoggedInUATForTheWeek + " " + weekNumberInt + " " + yearNumberInt);

								averageTimeTakenToResolveSev1Defects = next.getAverageTimeTakenToResolveSev1Defects();
								System.out.println("Average time taken to resolve Sev1 defects for the week " + averageTimeTakenToResolveSev1Defects + " " + weekNumberInt + " " + yearNumberInt);

								break;
							}
						}
					}
				}


				Iterable<WeeklyMetrics> weeklyMetricsIterable = weeklyMetricsRepo.findAll();
				Iterator<WeeklyMetrics> weeklyMetricsIterator = weeklyMetricsIterable.iterator();
				boolean metricMatchFound = false;
				WeeklyMetrics weeklyMetric = null;
				String ragStatus = null;

				while (weeklyMetricsIterator.hasNext()) {
					weeklyMetric = weeklyMetricsIterator.next();

					if (weeklyMetric.getProject().getId() == id) {
						if (weeklyMetric.getYearNumber() == yearNumberInt) {
							if (weeklyMetric.getWeekNumber() == weekNumberInt) {
								metricMatchFound = true;
								break;
							}
						}
					}
				}


				if (metricMatchFound == false) {
					weeklyMetric = new WeeklyMetrics();
					weeklyMetric.setWeekNumber(weekNumberInt);
					weeklyMetric.setYearNumber(yearNumberInt);

					Iterable<WeekData> weekDataIterable = weekDataRepository.findAll();
					Iterator<WeekData> weekDataIterator = weekDataIterable.iterator();

					WeekData nextWeek = null;

					// Adding month number for the current week and year combination
					while (weekDataIterator.hasNext()) {
						nextWeek = weekDataIterator.next();
						if ((nextWeek.getWeekNumber() == weekNumberInt) && (nextWeek.getYearNumber() == yearNumberInt)) {
							weeklyMetric.setMonthNumber(nextWeek.getMonthNumber());
							break;
						}
					}


					// Add the project detail to the newly created weekly metric object
					Iterable<Project> allProjects = projectRepository.findAll();
					Iterator<Project> iterator = allProjects.iterator();

					while (iterator.hasNext()) {
						Project nextProject = iterator.next();

						if (nextProject.getId() == id) {
							weeklyMetric.setProject(nextProject);
							weeklyMetricsRepo.save(weeklyMetric);
							break;
						}
					}
				}


				//Automation Utilisation (%  Regression executed through Automation )

				// Regression Automation Test Case Authoring Productivity for the week
				try {
					if (cummulativeNumberOfRegressionAutomationTestCasesDesignedSoFar > 0) {

						// cummulativeNumberOfRegressionAutomationTestCasesDesignedSoFar is the denominator
						// numberOfRegressionAutomationTestCasesExecutedForTheWeek should be in person days
						// Straight forward division
						// If numberOfRegression Automation executed For The Week = 1000 test cases
						// And cummulativeNumberOfRegressionAutomationTestCasesDesignedSoFar is 2000
						// AutomationUtilization is 1000*100/2000 = 50%
						// This is %

						Double numberOfRegressionAutomationTestCasesExecutedForTheWeekDouble = new Double(numberOfRegressionAutomationTestCasesExecutedForTheWeek);
						Double cummulativeNumberOfRegressionAutomationTestCasesDesignedSoFarDouble = new Double(cummulativeNumberOfRegressionAutomationTestCasesDesignedSoFar);
						Double temp = 0.0;

						try {
							temp = (((numberOfRegressionAutomationTestCasesExecutedForTheWeekDouble * 100) / (cummulativeNumberOfRegressionAutomationTestCasesDesignedSoFarDouble)));
						} catch (Exception e) {
						}



						if (temp == 0) {
							weeklyMetric.setAutomationUtilization("0.0");
							weeklyMetric.setAutomationUtilizationStatus("R");
						} else {
							temp = (Math.round(temp * 100.0) / 100.0);
							weeklyMetric.setAutomationUtilization(temp.toString());
							ragStatus = checkRAGStatus(temp, id, weeklyMetric.getRegressionTestAutomationScriptingProductivityName());
							System.out.println("The rag status is : " + ragStatus);
							weeklyMetric.setAutomationUtilizationStatus(ragStatus);

							System.out.println("Number of regression test cases executed for the week " + numberOfRegressionAutomationTestCasesExecutedForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Number of regression test cases design so far " + cummulativeNumberOfRegressionAutomationTestCasesDesignedSoFar + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Automation utilization for the week " + temp.toString() + " " + weekNumberInt + " " + yearNumberInt);

						}

						weeklyMetricsRepo.save(weeklyMetric);
					}
				}catch(Exception e){

				}


				try {

					// Manual Test Case Authoring Productivity for the week
					if ((manualTestCasesDesignEffortForTheWeek > 0) && (numberOfResourcesAuthoringManualTestCasesForTheWeek > 0)) {

						// This is not cummulative
						// manualTestCasesDesignEffortForTheWeek should be in person days
						// Straight forward division
						// If numberOfManualTestCasesDesignedForTheWeek = 100 test cases
						// And manualTestCasesDesignEffortForTheWeek = 10 person days
						// ManualTestCaseAuthoringProductivity is 100/10 = 10
						// This is not %

						Double numberOfManualTestCasesDesignedDouble = new Double(numberOfManualTestCasesDesignedForTheWeek);
						Double manualTestCasesDesignEffortDouble = new Double(manualTestCasesDesignEffortForTheWeek);
						Double numberOfResourcesAuthoringManualTestCasesForTheWeekDouble = new Double(numberOfResourcesAuthoringManualTestCasesForTheWeek);

						Double temp = 0.0;

						try {
							temp = (((numberOfManualTestCasesDesignedDouble) / (manualTestCasesDesignEffortDouble * numberOfResourcesAuthoringManualTestCasesForTheWeekDouble)));
						} catch (Exception e) {

						}


						if (temp == 0) {
							weeklyMetric.setManualTestCaseAuthoringProductivity("0.0");
							weeklyMetric.setManualTestCaseAuthoringProductivityStatus("R");
						} else {
							temp = (Math.round(temp * 100.0) / 100.0);
							weeklyMetric.setManualTestCaseAuthoringProductivity(temp.toString());

							ragStatus = checkRAGStatus(temp, id, weeklyMetric.getManualTestCaseAuthoringProductivityName());
							System.out.println("The rag status is : " + ragStatus);
							weeklyMetric.setManualTestCaseAuthoringProductivityStatus(ragStatus);

							System.out.println("Number of manual test cases designed for the week " + numberOfManualTestCasesDesignedForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("manual test cases effort for the week " + manualTestCasesDesignEffortForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Number of resources authoring manual test cases for the week " + numberOfResourcesAuthoringManualTestCasesForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("manual test cases design productivity for the week " + temp.toString() + " " + weekNumberInt + " " + yearNumberInt);

						}

						weeklyMetricsRepo.save(weeklyMetric);
					}
				}catch(Exception e){
					System.out.println("Exception occured during Manual Test Case Authoring Productivity caluclation for " + " " + weekNumberInt + " " + yearNumberInt);
					System.out.println(e.getMessage());
				}


				// Regression Automation Test Case Authoring Productivity for the week
				try {
					if ((regressionAutomationTestCasesDesignEffortForTheWeek > 0) && (numberOfResourcesAuthoringAutomationTestCasesForTheWeek > 0)) {

						// This is not cummulative
						// regressionAutomationTestCasesDesignEffortForTheWeek should be in person days
						// Straight forward division
						// If numberOfRegression Automation scripted ForTheWeek = 100 test cases
						// And regressionAutomationTestCasesDesignEffortForTheWeek = 10 person days
						// RegressionTestScriptingProductivityStatus is 100/10 = 10
						// This is not %

						Double numberOfRegressionAutomationTestCasesDesignedDouble = new Double(numberOfRegressionAutomationTestCasesDesignedForTheWeek);
						Double regressionAutomationTestCasesDesignEffortDouble = new Double(regressionAutomationTestCasesDesignEffortForTheWeek);
						Double numberOfResourcesAuthoringAutomationTestCasesForTheWeekDouble = new Double(numberOfResourcesAuthoringAutomationTestCasesForTheWeek);

						Double temp = 0.0;

						try {
							temp = (((numberOfRegressionAutomationTestCasesDesignedDouble) / (regressionAutomationTestCasesDesignEffortDouble)));
						} catch (Exception e) {
						}
						;


						if (temp == 0) {
							weeklyMetric.setRegressionTestScriptingProductivity("0.0");
							weeklyMetric.setRegressionAutomationTestExecutionProductivityStatus("R");
						} else {
							temp = (Math.round(temp * 100.0) / 100.0);
							weeklyMetric.setRegressionTestScriptingProductivity(temp.toString());
							ragStatus = checkRAGStatus(temp, id, weeklyMetric.getRegressionTestAutomationScriptingProductivityName());
							System.out.println("The rag status is : " + ragStatus);
							weeklyMetric.setRegressionTestScriptingProductivityStatus(ragStatus);

							System.out.println("Number of regression test cases designed for the week " + numberOfRegressionAutomationTestCasesDesignedForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("regression test cases design effort for the week " + regressionAutomationTestCasesDesignEffortForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Number of resources authoring regression automation for the week " + numberOfResourcesAuthoringAutomationTestCasesForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("regression test cases design productivity for the week " + temp.toString() + " " + weekNumberInt + " " + yearNumberInt);

						}

						weeklyMetricsRepo.save(weeklyMetric);
					}
				}catch(Exception e){

				}


				// In Project Automation Test Case Authoring Productivity for the week
				try {
					if (inProjectAutomationTestCasesDesignEffortForTheWeek > 0) {

						Double numberOfInProjectAutomationTestCasesDesignedDouble = new Double(numberOfInProjectAutomationTestCasesDesignedForTheWeek);
						Double inProjectAutomationTestCasesDesignEffortDouble = new Double(inProjectAutomationTestCasesDesignEffortForTheWeek);
						Double temp = 0.0;

						try {
							temp = (((numberOfInProjectAutomationTestCasesDesignedDouble) / (inProjectAutomationTestCasesDesignEffortDouble)));
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							weeklyMetric.setInProjectTestScriptingProductivity("0.0");
							weeklyMetric.setInProjectTestAutomationScriptingProductivityStatus("R");
						} else {
							temp = (Math.round(temp * 100.0) / 100.0);
							weeklyMetric.setInProjectTestScriptingProductivity(temp.toString());

							ragStatus = checkRAGStatus(temp, id, weeklyMetric.getInProjectAutomationScriptingProductivityName());
							System.out.println("The rag status is : " + ragStatus);
							weeklyMetric.setInProjectTestAutomationScriptingProductivityStatus(ragStatus);

							System.out.println("Number of in sprint automation test cases designed for the week " + numberOfInProjectAutomationTestCasesDesignedForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("in sprint automation test cases design effort for the week " + inProjectAutomationTestCasesDesignEffortForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("in sprint automation test cases design productivity for the week " + temp.toString() + " " + weekNumberInt + " " + yearNumberInt);

						}

						weeklyMetricsRepo.save(weeklyMetric);
					}
				}catch(Exception e){

				}


				// API Automation Test Case Authoring Productivity for the week

				try {
					if (apiAutomationTestCasesDesignEffortForTheWeek > 0) {

						Double numberOfApiAutomationTestCasesDesignedDouble = new Double(numberOfAPIAutomationTestCasesDesignedForTheWeek);
						Double apiAutomationTestCasesDesignEffortDouble = new Double(apiAutomationTestCasesDesignEffortForTheWeek);
						Double temp = 0.0;

						try {
							temp = (((numberOfApiAutomationTestCasesDesignedDouble) / (apiAutomationTestCasesDesignEffortDouble)));
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							weeklyMetric.setApiAutomationTestScriptingProductivity("0.0");
							weeklyMetric.setApiAutomationTestScriptingProductivityStatus("R");
						} else {
							temp = (Math.round(temp * 100.0) / 100.0);
							weeklyMetric.setApiAutomationTestScriptingProductivity(temp.toString());

							ragStatus = checkRAGStatus(temp, id, weeklyMetric.getApiTestAutomationScriptingProductivityName());
							System.out.println("The rag status is : " + ragStatus);
							weeklyMetric.setApiAutomationTestScriptingProductivityStatus(ragStatus);

							System.out.println("Number of api automation test cases designed for the week " + numberOfAPIAutomationTestCasesDesignedForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("api automation test cases design effort for the week " + apiAutomationTestCasesDesignEffortForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("api automation test cases design productivity for the week " + temp.toString() + " " + weekNumberInt + " " + yearNumberInt);
						}

						weeklyMetricsRepo.save(weeklyMetric);
					}
				}catch(Exception e){

				}

				// Manual Test execution productivity for the week

				try {
					if ((manualTestCaseExecutionEffortForTheWeek > 0) && (numberOfResourcesExecutingManualTestCasesForTheWeek > 0)) {

						// manualTestCaseExecutionEffortForTheWeek in person days

						Double numberOfManualTestCasesExecutedDouble = new Double(numberOfManualTestCasesExecutedForTheWeek);
						Double manualTestCaseExecutionEffortDouble = new Double(manualTestCaseExecutionEffortForTheWeek);
						Double numberOfResourcesExecutingManualTestCasesForTheWeekDouble = new Double(numberOfResourcesExecutingManualTestCasesForTheWeek);

						Double temp = 0.0;

						try {
							temp = (((numberOfManualTestCasesExecutedDouble) / (manualTestCaseExecutionEffortDouble * numberOfResourcesExecutingManualTestCasesForTheWeekDouble)));
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							weeklyMetric.setManualTestExecutionProductivity("0.0");
							weeklyMetric.setManualTestExecutionProductivityStatus("R");
						} else {
							temp = (Math.round(temp * 100.0) / 100.0);
							weeklyMetric.setManualTestExecutionProductivity(temp.toString());

							ragStatus = checkRAGStatus(temp, id, weeklyMetric.getManualTestExecutionProductivityName());
							System.out.println("The rag status is : " + ragStatus);
							weeklyMetric.setManualTestExecutionProductivityStatus(ragStatus);

							System.out.println("Number of manual test cases executed for the week " + numberOfManualTestCasesExecutedForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Manual test cases execution effort for the week " + manualTestCaseExecutionEffortForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Number of resources executing manual test cases for the week " + numberOfResourcesExecutingManualTestCasesForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Manual test cases execution productivity for the week " + temp.toString() + " " + weekNumberInt + " " + yearNumberInt);

						}

						weeklyMetricsRepo.save(weeklyMetric);
					}
				}catch(Exception e){

				}



				// Regression Test execution productivity for the week
				try {
					if ((regressionAutomationTestCaseExecutionEffortForTheWeek > 0) && (numberOfResourcesExecutingRegressionAutomationForTheWeek > 0)) {

						// regressionAutomationTestCaseExecutionEffortForTheWeek in person days

						Double numberOfRegressionAutomationTestCasesExecutedDouble = new Double(numberOfRegressionAutomationTestCasesExecutedForTheWeek);
						Double regressionAutomationTestCaseExecutionEffortDouble = new Double(regressionAutomationTestCaseExecutionEffortForTheWeek);
						Double numberOfResourcesExecutingRegressionAutomationForTheWeekDouble = new Double(numberOfResourcesExecutingRegressionAutomationForTheWeek);

						Double temp = 0.0;

						try {
							temp = (((numberOfRegressionAutomationTestCasesExecutedDouble) / (regressionAutomationTestCaseExecutionEffortDouble * numberOfResourcesExecutingRegressionAutomationForTheWeekDouble)));
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							weeklyMetric.setRegressionAutomationTestExecutionProductivity("0.0");
							weeklyMetric.setRegressionAutomationTestExecutionProductivityStatus("R");

						} else {
							temp = (Math.round(temp * 100.0) / 100.0);
							weeklyMetric.setRegressionAutomationTestExecutionProductivity(temp.toString());

							ragStatus = checkRAGStatus(temp, id, weeklyMetric.getRegressionAutomationTestExecutionProductivityName());
							System.out.println("The rag status is : " + ragStatus);
							weeklyMetric.setRegressionAutomationTestExecutionProductivityStatus(ragStatus);

							System.out.println("Number of regression automation test cases executed for the week " + numberOfRegressionAutomationTestCasesExecutedForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("regression test cases execution effort for the week " + regressionAutomationTestCaseExecutionEffortForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Number of resources executing Regression automation for the week " + numberOfResourcesExecutingRegressionAutomationForTheWeekDouble+ " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Regression automation test execution productivity for the week " + temp.toString() + " " + weekNumberInt + " " + yearNumberInt);


						}

						weeklyMetricsRepo.save(weeklyMetric);
					}
				}catch(Exception e){

				}


				// API Automation Test execution productivity for the month
				try {
					if ((apiAutomationTestCaseExecutionEffortForTheWeek > 0) && (numberOfResourcesExecutingAPIAutomationForTheWeek > 0)){

						Double numberOfAPIAutomationTestScriptsExecutedDouble = new Double(numberOfAPIAutomationTestScriptsExecutedForTheWeek);
						Double apiAutomationTestCaseExecutionEffortDouble = new Double(apiAutomationTestCaseExecutionEffortForTheWeek);
						Double numberOfResourcesExecutingAPIAutomationForTheWeekDouble = new Double(numberOfResourcesExecutingAPIAutomationForTheWeek);

						Double temp = 0.0;

						try {
							temp = (((numberOfAPIAutomationTestScriptsExecutedDouble) / (apiAutomationTestCaseExecutionEffortDouble * numberOfResourcesExecutingAPIAutomationForTheWeekDouble)));
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							weeklyMetric.setAPIAutomationTestExecutionProductivity("0.0");
							weeklyMetric.setAPIAutomationTestExecutionProductivityStatus("R");
						} else {
							temp = (Math.round(temp * 100.0) / 100.0);
							weeklyMetric.setAPIAutomationTestExecutionProductivity(temp.toString());

							ragStatus = checkRAGStatus(temp, id, weeklyMetric.getAPIAutomationTestExecutionProductivityName());
							System.out.println("The rag status is : " + ragStatus);
							weeklyMetric.setAPIAutomationTestExecutionProductivityStatus(ragStatus);

							System.out.println("Number of api automation test cases executed for the week " + numberOfAPIAutomationTestScriptsExecutedForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("api automation test cases execution effort for the week " + apiAutomationTestCaseExecutionEffortForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("number of resources executing API automation effort for the week " + numberOfResourcesExecutingAPIAutomationForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("api automation test cases execution productivity for the week " + temp.toString() + " " + weekNumberInt + " " + yearNumberInt);

						}

						weeklyMetricsRepo.save(weeklyMetric);
					}
				}catch(Exception e){

				}



				// InProject Automation Test execution productivity for the month
				try {
					if ((inProjectAutomationTestCaseExecutionEffortForTheWeek > 0) && (numberOfResourcesExecutingInProjectAutomationForTheWeek > 0)) {

						Double numberOfInProjectAutomationTestScriptsExecutedDouble = new Double(numberOfInProjectAutomationTestScriptsExecutedforTheWeek);
						Double inProjectAutomationTestCaseExecutionEffortDouble = new Double(inProjectAutomationTestCaseExecutionEffortForTheWeek);
						Double numberOfResourcesExecutingInProjectAutomationForTheWeekDouble = new Double(numberOfResourcesExecutingInProjectAutomationForTheWeek);

						Double temp = 0.0;

						try {
							temp = (((numberOfInProjectAutomationTestScriptsExecutedDouble) / (inProjectAutomationTestCaseExecutionEffortDouble * numberOfResourcesExecutingInProjectAutomationForTheWeekDouble)));
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							weeklyMetric.setInProjectAutomationTestExecutionProductivity("0.0");
							weeklyMetric.setInProjectAutomationTestExecutionProductivityStatus("R");
						} else {
							temp = (Math.round(temp * 100.0) / 100.0);
							weeklyMetric.setInProjectAutomationTestExecutionProductivity(temp.toString());

							ragStatus = checkRAGStatus(temp, id, weeklyMetric.getInProjectAutomationTestExecutionProductivityName());
							System.out.println("The rag status is : " + ragStatus);
							weeklyMetric.setInProjectAutomationTestExecutionProductivityStatus(ragStatus);

							System.out.println("In Sprint automation test cases executed for the week " + numberOfInProjectAutomationTestScriptsExecutedforTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("In Sprint automation test cases execution effort for the week " + inProjectAutomationTestCaseExecutionEffortForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Number of resources executing In Sprint automation for the week " + numberOfResourcesExecutingInProjectAutomationForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("In Sprint  automation test cases execution productivity for the week " + temp.toString() + " " + weekNumberInt + " " + yearNumberInt);

						}

						weeklyMetricsRepo.save(weeklyMetric);
					}
				}catch(Exception e){

				}


				// Percentage of Manual Test Cases not executed for the week

				try {
					if (cummulativeNumberOfManualTestCasesDesignedSoFar > 0) {
						Double cummulativeNumberOfManualTestCasesDesignedSoFarDouble = new Double(cummulativeNumberOfManualTestCasesDesignedSoFar);
						Double totalNumberOfManualTestCasesNotExecutedDouble = new Double(totalNumberOfManualTestCasesNotExecutedForTheWeek);
						Double temp = 0.0;

						try {
							temp = (((totalNumberOfManualTestCasesNotExecutedDouble * 100) / (cummulativeNumberOfManualTestCasesDesignedSoFarDouble)));
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							weeklyMetric.setTestCaseNotExecutedManual("0.0");
							weeklyMetric.setTestCaseNotExecutedManualStatus("R");
						} else {
							temp = (Math.round(temp * 100.0) / 100.0);
							weeklyMetric.setTestCaseNotExecutedManual(temp.toString());

							ragStatus = checkRAGStatus(temp, id, weeklyMetric.getTestCaseNotExecutedManualName());
							System.out.println("The rag status is : " + ragStatus);
							weeklyMetric.setTestCaseNotExecutedManualStatus(ragStatus);


							System.out.println("Number of manual test cases not executed for the week " + totalNumberOfManualTestCasesNotExecutedForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Cummulative number of manual test cases designed so far " + cummulativeNumberOfManualTestCasesDesignedSoFar + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Percentage of Manual Test Cases not executed so far " + temp.toString() + " " + weekNumberInt + " " + yearNumberInt);

						}

						weeklyMetricsRepo.save(weeklyMetric);
					}
				}catch(Exception e){

				}

				// Percentage of Automation Test Cases not executed for the month

				try {
					if (cummulativeNumberOfAutomationTestCasesDesignedSoFar > 0) {
						Double cummulativeNumberOfAutomationTestCasesDesignedSoFarDouble = new Double(cummulativeNumberOfAutomationTestCasesDesignedSoFar);
						Double totalNumberOfAutomationTestCasesNotExecutedDouble = new Double(totalNumberOfAutomationTestCasesNotExecutedForTheWeek);
						Double temp = 0.0;

						try {
							temp = (((totalNumberOfAutomationTestCasesNotExecutedDouble * 100) / (cummulativeNumberOfAutomationTestCasesDesignedSoFarDouble)));
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							weeklyMetric.setTestCaseNotExecutedAutomation("0.0");
							weeklyMetric.setTestCaseNotExecutedAutomationStatus("R");
						} else {
							temp = (Math.round(temp * 100.0) / 100.0);
							weeklyMetric.setTestCaseNotExecutedAutomation(temp.toString());

							ragStatus = checkRAGStatus(temp, id, weeklyMetric.getTestCaseNotExecutedAutomationName());
							System.out.println("The rag status is : " + ragStatus);
							weeklyMetric.setTestCaseNotExecutedAutomationStatus(ragStatus);


							System.out.println("Number of automation test cases not executed for the week " + totalNumberOfAutomationTestCasesNotExecutedForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Cummulative number of automation test cases designed so far " + cummulativeNumberOfAutomationTestCasesDesignedSoFar + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Percentage of automation Test Cases not executed so far " + temp.toString() + " " + weekNumberInt + " " + yearNumberInt);
						}

						weeklyMetricsRepo.save(weeklyMetric);
					}
				}catch(Exception e){

				}


				// Percentage of test cases executed without defects against total number of test cases executed
				// Quality Ratio
				// This should be percentage
				// The excel use only manual test cases executed so far

				try {
					if (cummulativeNumberOfTestCasesExecutedSoFar > 0) {
						Double numberOfTestCasesSuccessfullyExecutedWithoutDefectsDouble = new Double(numberOfTestCasesSuccessfullyExecutedWithoutDefectsForTheWeek);
						Double cummulativeNumberOfTestCasesExecutedSoFarDouble = new Double(cummulativeNumberOfTestCasesExecutedSoFar);
						Double temp = 0.0;

						try {
							temp = (((numberOfTestCasesSuccessfullyExecutedWithoutDefectsDouble * 100) / (cummulativeNumberOfTestCasesExecutedSoFarDouble)));
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							weeklyMetric.setCurrentQualityRatio("0.0");
							weeklyMetric.setCurrentQualityRatioStatus("R");
						} else {
							temp = (Math.round(temp * 100.0) / 100.0);
							weeklyMetric.setCurrentQualityRatio(temp.toString());

							ragStatus = checkRAGStatus(temp, id, weeklyMetric.getCurrentQualityRatioName());
							System.out.println("The rag status is : " + ragStatus);
							weeklyMetric.setCurrentQualityRatioStatus(ragStatus);

							System.out.println("Number of test cases executed for the week without defects " + numberOfTestCasesSuccessfullyExecutedWithoutDefectsForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Cummulative number of test cases executed so far " + cummulativeNumberOfTestCasesExecutedSoFar + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("The Quality ratio is : " + temp.toString() + " " + weekNumberInt + " " + yearNumberInt);

						}

						weeklyMetricsRepo.save(weeklyMetric);
					}
				}catch(Exception e){

				}

				// Percentage of defects rejected against the number of defects reported for the week
				// defect rejection
				// This should be %

				try {
					weeklyMetric.setNumberOfDefectsReportedByTestingTeamforTheWeek(Integer.toString(numberOfDefectsReportedByTestingTeamforTheWeek));
				}catch(Exception e){
				}

				try {
					weeklyMetric.setNumberOfDefectsRejectedForTheWeek(Integer.toString(numberOfDefectsRejectedForTheWeek));
				}catch(Exception e){

				}

				try {
					weeklyMetric.setTotalNumberOfDefectsReopenedForTheWeek(Integer.toString(totalNumberOfDefectsReopenedForTheWeek));
				}catch(Exception e){

				}

				try {
					if (numberOfDefectsReportedByTestingTeamforTheWeek > 0) {
						Double numberOfDefectsReportedByTestingTeamDouble = new Double(numberOfDefectsReportedByTestingTeamforTheWeek);
						Double numberOfDefectsRejectedDouble = new Double(numberOfDefectsRejectedForTheWeek);
						Double temp = 0.0;

						try {
							temp = (((numberOfDefectsRejectedDouble * 100) / (numberOfDefectsReportedByTestingTeamDouble)));
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							weeklyMetric.setDefectRejection("0.0");
							weeklyMetric.setDefectRejectionStatus("R");
						} else {
							temp = (Math.round(temp * 100.0) / 100.0);
							weeklyMetric.setDefectRejection(temp.toString());

							ragStatus = checkRAGStatus(temp, id, weeklyMetric.getDefectRejectionName());
							System.out.println("The rag status is : " + ragStatus);
							weeklyMetric.setDefectRejectionStatus(ragStatus);

							System.out.println("Number of test cases reported for the week " + numberOfDefectsReportedByTestingTeamforTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Number of defects rejected for the week " + numberOfDefectsRejectedForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("The defect rejection percentage is : " + temp.toString() + " " + weekNumberInt + " " + yearNumberInt);
						}

						weeklyMetricsRepo.save(weeklyMetric);
					}

				}catch(Exception e){

				}


				int totalNoOfTCsTaggedForMigrationExecution = 0;
				int totalNumberOfRequirementsPlanned = 0;
				int numberOfTestCasesDesignedPerHour = 0;
				int numberOfTestCasesExecutedPerHour = 0;


				Iterable<LatestProjectEstimate> allLatestProjectEstimates = latestProjectEstimateRepository.findAll();
				Iterator<LatestProjectEstimate> latestProjectEstimateIterator = allLatestProjectEstimates.iterator();

				while (latestProjectEstimateIterator.hasNext()) {
					LatestProjectEstimate latestProjectEstimate = latestProjectEstimateIterator.next();

					if (latestProjectEstimate.getProject().getId() == id) {
						try {
							totalNoOfTCsTaggedForMigrationExecution = Integer.parseInt(latestProjectEstimate.getTotalNumberOfTestCasesTaggedForMigrationExecution());
							totalNumberOfRequirementsPlanned = Integer.parseInt(latestProjectEstimate.getTotalNumberOfRequirementsPlanned());

							numberOfTestCasesDesignedPerHour = Integer.parseInt(latestProjectEstimate.getNumberOfTestCasesDesignedPerHour());
							numberOfTestCasesExecutedPerHour = Integer.parseInt(latestProjectEstimate.getNumberOfTestCasesExecutedPerHour());

							break;
						} catch (Exception e) {
						}
					}
				}

				//totalNumberOfResourcesAuthoringTestCasesForTheWeek => Number of resources


				weeklyMetric.setDowntimeAndImpactAnalysisDesignAvgProductivityLost(
						Integer.toString(
								environmentDowntimeDuringTestDesignPerPersonInHoursForTheWeek *
										numberOfTestCasesDesignedPerHour *
										totalNumberOfResourcesAuthoringTestCasesForTheWeek));

				Double tempDesignLoss = new Double(Integer.toString(
						environmentDowntimeDuringTestDesignPerPersonInHoursForTheWeek *
								numberOfTestCasesDesignedPerHour *
								totalNumberOfResourcesAuthoringTestCasesForTheWeek));


				System.out.println("Environment Downtime during Test Design Phase For The Week " + environmentDowntimeDuringTestDesignPerPersonInHoursForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
				System.out.println("Number of test cases designed per hour " + numberOfTestCasesDesignedPerHour + " " + weekNumberInt + " " + yearNumberInt);
				System.out.println("Total Number of resources working on test case design for the week : " + totalNumberOfResourcesAuthoringTestCasesForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
				System.out.println("Design loss time : " + tempDesignLoss + " " + weekNumberInt + " " + yearNumberInt);


				ragStatus = checkRAGStatus(tempDesignLoss, id,
						weeklyMetric.getDowntimeAndImpactAnalysisDesignAvgProductivityLostName());
				System.out.println("The rag status is : " + ragStatus);
				weeklyMetric.setDowntimeAndImpactAnalysisDesignAvgProductivityLostStatus(ragStatus);
				weeklyMetricsRepo.save(weeklyMetric);


				weeklyMetric.setDowntimeAndImpactAnalysisExecutionAvgProductivityLost(
						Integer.toString(
								environmentDowntimeDuringTestExecutionPerPersonInHoursForTheWeek *
										numberOfTestCasesExecutedPerHour *
										totalNumberOfResourcesExecutingTestCasesForTheWeek));

				Double tempExecutionLoss = new Double(
						Integer.toString(
								environmentDowntimeDuringTestExecutionPerPersonInHoursForTheWeek *
										numberOfTestCasesExecutedPerHour *
										totalNumberOfResourcesExecutingTestCasesForTheWeek));

				ragStatus = checkRAGStatus(tempExecutionLoss, id,
						weeklyMetric.getDowntimeAndImpactAnalysisExecutionAvgProductivityLostName());
				System.out.println("The rag status is : " + ragStatus);
				weeklyMetric.setDowntimeAndImpactAnalysisExecutionAvgProductivityLostStatus(ragStatus);
				weeklyMetricsRepo.save(weeklyMetric);


				System.out.println("Environment Downtime during Test execution Phase For The Week " + environmentDowntimeDuringTestExecutionPerPersonInHoursForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
				System.out.println("Number of test cases designed per hour " + numberOfTestCasesExecutedPerHour + " " + weekNumberInt + " " + yearNumberInt);
				System.out.println("Total Number of resources working on test case execution for the week : " + totalNumberOfResourcesExecutingTestCasesForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
				System.out.println("Execution loss time : " + tempExecutionLoss + " " + weekNumberInt + " " + yearNumberInt);

				try {
					if (totalNoOfTCsTaggedForMigrationExecution > 0) {

						Double totalNoOfTCsTaggedForMigrationExecutionDouble = new Double(totalNoOfTCsTaggedForMigrationExecution);
						Double cummulativeNumberOfTestCasesExecutedWithMigratedDataDouble = new Double(cummulativeNumberOfTestCasesExecutedWithMigratedDataSoFar);
						Double temp = 0.0;

						try {
							temp = (((cummulativeNumberOfTestCasesExecutedWithMigratedDataDouble * 100) / (totalNoOfTCsTaggedForMigrationExecutionDouble)));
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							weeklyMetric.setMigratedDataExecution("0.0");
							weeklyMetric.setMigratedDataExecutionStatus("R");
						} else {
							temp = (Math.round(temp * 100.0) / 100.0);
							weeklyMetric.setMigratedDataExecution(temp.toString());

							ragStatus = checkRAGStatus(temp, id, weeklyMetric.getMigratedDataExecutionName());
							System.out.println("The rag status is : " + ragStatus);
							weeklyMetric.setMigratedDataExecutionStatus(ragStatus);

							System.out.println("Number of test cases planned for migration data execution " + totalNoOfTCsTaggedForMigrationExecution + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Number of test cases executed with migrated data so far " + cummulativeNumberOfTestCasesExecutedWithMigratedDataSoFar + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Data migration test execution % is : " + temp.toString() + " " + weekNumberInt + " " + yearNumberInt);
						}

						weeklyMetricsRepo.save(weeklyMetric);
					}
				}catch(Exception e){

				}


				try {
					if (totalNumberOfTestDesignReviewDefectsFoundForTheWeek > 0 || numberOfDefectsReportedByTestingTeamforTheWeek > 0) {


						Double cummulativeNumberOfTestDesignReviewDefectsFoundDouble = new Double(cummulativeNumberOfTestDesignReviewDefectsFoundForTheWeek);
						Double cummulativeNumberOfValidDefectsLoggedInSITDouble = new Double(cummulativeNumberOfValidDefectsLoggedInSITSoFar);
						Double cummulativeNumberOfValidDefectsLoggedInUATDouble = new Double(cummulativeNumberOfValidDefectsLoggedInUATSoFar);

						Double temp = (((cummulativeNumberOfTestDesignReviewDefectsFoundDouble * 100) /
								(cummulativeNumberOfTestDesignReviewDefectsFoundDouble +
										cummulativeNumberOfValidDefectsLoggedInSITDouble +
										cummulativeNumberOfValidDefectsLoggedInUATDouble)));

						if (temp == 0) {
							weeklyMetric.setTestReviewEfficiency("0.0");
							weeklyMetric.setTestReviewEfficiencyStatus("R");
						} else {
							temp = (Math.round(temp * 100.0) / 100.0);
							weeklyMetric.setTestReviewEfficiency(temp.toString());

							ragStatus = checkRAGStatus(temp, id, weeklyMetric.getTestReviewEfficiencyName());
							System.out.println("The rag status is : " + ragStatus);
							weeklyMetric.setTestReviewEfficiencyStatus(ragStatus);

							System.out.println("Cummulative number of test design review defects found so far " + cummulativeNumberOfTestDesignReviewDefectsFoundForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Cummulative number of valid defects in SIT so far " + cummulativeNumberOfValidDefectsLoggedInSITSoFar + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Cummulative number of valid defects in UAR so far " + cummulativeNumberOfValidDefectsLoggedInUATSoFar + " " + weekNumberInt + " " + yearNumberInt);

							System.out.println("Test review efficiency % is : " + temp.toString() + " " + weekNumberInt + " " + yearNumberInt);
						}
						weeklyMetricsRepo.save(weeklyMetric);
					}
				}catch(Exception e){

				}


				try {
					if (numberOfDefectsFixedForTheWeek > 0) {

						Double totalNumberOfDefectsReopenedDouble = new Double(totalNumberOfDefectsReopenedForTheWeek);
						Double numberOfDefectsFixedDouble = new Double(numberOfDefectsFixedForTheWeek);
						Double temp = 0.0;

						try {
							temp = (((totalNumberOfDefectsReopenedDouble * 100) / numberOfDefectsFixedDouble));
						} catch (Exception e) {
						}
						;

						temp = 100 - temp;
						temp = (Math.round(temp * 100.0) / 100.0);

						if (temp == 0) {
							weeklyMetric.setQualityOfFixes("0.0");
							weeklyMetric.setQualityOfFixesStatus("R");
						} else {
							weeklyMetric.setQualityOfFixes(temp.toString());

							ragStatus = checkRAGStatus(temp, id, weeklyMetric.getQualityOfFixesName());
							System.out.println("The rag status is : " + ragStatus);
							weeklyMetric.setQualityOfFixesStatus(ragStatus);

							System.out.println("Total number of defects reopened for the week " + totalNumberOfDefectsReopenedForTheWeek + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Total number of defects fixed for the week " + numberOfDefectsFixedForTheWeek + " " + weekNumberInt + " " + yearNumberInt);

							System.out.println("Quality of fixes % is : " + temp.toString() + " " + weekNumberInt + " " + yearNumberInt);
						}
						weeklyMetricsRepo.save(weeklyMetric);

					} else {
						// Need to address this scenario
					}
				}catch(Exception e){

				}


				Iterable<LatestModuleEstimate> allLatestModuleEstimates = latestModuleEstimateRepository.findAll();
				Iterator<LatestModuleEstimate> latestModuleEstimateIterator = allLatestModuleEstimates.iterator();
				int totalEstimatedRegressionAutomationTestCases = 0;
				int totalEstimatedSanityAutomationTestCases = 0;


				while (latestModuleEstimateIterator.hasNext()) {
					LatestModuleEstimate latestModulesEstimate = latestModuleEstimateIterator.next();

					if (latestModulesEstimate.getProject().getId() == id) {
						try {
							totalEstimatedRegressionAutomationTestCases += Integer.parseInt(latestModulesEstimate.getEstimatedRegressionAutomationTestCases());
							totalEstimatedSanityAutomationTestCases += Integer.parseInt(latestModulesEstimate.getEstimatedSanityAutomationTestCases());
						} catch (Exception e) {
						}
					}
				}


				try {
					if (totalEstimatedRegressionAutomationTestCases > 0) {

						Double totalEstimatedRegressionAutomationTestCasesDouble = new Double(totalEstimatedRegressionAutomationTestCases);
						Double temp = 0.0;

						try {
							temp = (((cummulativeNumberOfRegressionAutomationTestCasesExecutedSoFar * 100) / totalEstimatedRegressionAutomationTestCasesDouble));
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							weeklyMetric.setPercentageOfRegressionAutomation("0.0");
							weeklyMetric.setPercentageOfRegressionAutomationStatus("R");
						} else {
							temp = (Math.round(temp * 100.0) / 100.0);
							weeklyMetric.setPercentageOfRegressionAutomation(temp.toString());

							ragStatus = checkRAGStatus(temp, id, weeklyMetric.getPercentageOfRegressionAutomationName());
							System.out.println("The rag status is : " + ragStatus);
							weeklyMetric.setPercentageOfRegressionAutomationStatus(ragStatus);

							System.out.println("Regression automation test cases estimate " + totalEstimatedRegressionAutomationTestCases + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Total number of regression automation test cases executed so far " + cummulativeNumberOfRegressionAutomationTestCasesExecutedSoFar + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Regression automation % is : " + temp.toString() + " " + weekNumberInt + " " + yearNumberInt);
						}

						weeklyMetricsRepo.save(weeklyMetric);
					}
				}catch(Exception e){

				}


				try {
					if (totalEstimatedRegressionAutomationTestCases > 0) {

						Double totalEstimatedRegressionAutomationTestCasesDouble = new Double(totalEstimatedRegressionAutomationTestCases);
						Double totalRegressionTestCasesPendingAutomationDouble = totalEstimatedRegressionAutomationTestCasesDouble - cummulativeNumberOfRegressionAutomationTestCasesExecutedSoFar;
						Double temp = 0.0;

						try {
							temp = (((totalRegressionTestCasesPendingAutomationDouble * 100) / totalEstimatedRegressionAutomationTestCasesDouble));
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							weeklyMetric.setPercentageOfRegressionAutomation("0.0");
						} else {
							temp = (Math.round(temp * 100.0) / 100.0);
							weeklyMetric.setPercentageOfPendingRegressionAutomation(temp.toString());
						}

						System.out.println("Regression automation test cases estimate " + totalEstimatedRegressionAutomationTestCases + " " + weekNumberInt + " " + yearNumberInt);
						System.out.println("Regression automation test cases pending execution " + totalRegressionTestCasesPendingAutomationDouble + " " + weekNumberInt + " " + yearNumberInt);
						System.out.println("Pending Regression automation % is : " + temp.toString() + " " + weekNumberInt + " " + yearNumberInt);

						weeklyMetricsRepo.save(weeklyMetric);
					}
				}catch(Exception e){

				}

				try {
					if (totalEstimatedRegressionAutomationTestCases > 0) {

						Double totalEstimatedRegressionAutomationTestCasesDouble = new Double(totalEstimatedRegressionAutomationTestCases);
						Double temp = 0.0;

						try {
							temp = (((cummulativeNumberOfRegressionAutomationTestCasesExecutedSoFar * 100) / totalEstimatedRegressionAutomationTestCasesDouble));
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							weeklyMetric.setPercentageOfRegressionAutomation("0.0");
							weeklyMetric.setPercentageOfRegressionAutomationStatus("R");
						} else {
							temp = (Math.round(temp * 100.0) / 100.0);
							weeklyMetric.setPercentageOfRegressionAutomation(temp.toString());

							ragStatus = checkRAGStatus(temp, id, weeklyMetric.getPercentageOfRegressionAutomationName());
							System.out.println("The rag status is : " + ragStatus);
							weeklyMetric.setPercentageOfRegressionAutomationStatus(ragStatus);

							System.out.println("Regression automation test cases estimate " + totalEstimatedRegressionAutomationTestCases + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Total number of regression automation test cases executed so far " + cummulativeNumberOfRegressionAutomationTestCasesExecutedSoFar + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Regression automation % is : " + temp.toString() + " " + weekNumberInt + " " + yearNumberInt);
						}

						weeklyMetricsRepo.save(weeklyMetric);
					}
				}catch(Exception e){

				}

				try {
					if (totalEstimatedSanityAutomationTestCases > 0) {

						Double totalEstimatedSanityAutomationTestCasesDouble = new Double(totalEstimatedSanityAutomationTestCases);
						Double cummulativeNumberOfSanityAutomationTestCasesDesignedSoFarDouble = new Double(cummulativeNumberOfSanityAutomationTestCasesDesignedSoFar);
						Double temp = 0.0;

						try {
							temp = (((cummulativeNumberOfSanityAutomationTestCasesDesignedSoFarDouble * 100) / totalEstimatedSanityAutomationTestCasesDouble));
						} catch (Exception e) {
						}


						if (temp == 0) {
							weeklyMetric.setSanityAutomationCoverage("0.0");
							weeklyMetric.setSanityAutomationCoverageStatus("R");

						} else {
							temp = (Math.round(temp * 100.0) / 100.0);
							weeklyMetric.setSanityAutomationCoverage(temp.toString());

							ragStatus = checkRAGStatus(temp, id, weeklyMetric.getSanityAutomationCoverageName());
							System.out.println("The rag status is : " + ragStatus);
							weeklyMetric.setSanityAutomationCoverageStatus(ragStatus);

						}

						System.out.println("Sanity automation test cases estimate " + totalEstimatedRegressionAutomationTestCases + " " + weekNumberInt + " " + yearNumberInt);
						System.out.println("Sanity automation completed so far " + cummulativeNumberOfSanityAutomationTestCasesDesignedSoFarDouble + " " + weekNumberInt + " " + yearNumberInt);
						System.out.println("Sanity Regression automation % is : " + temp.toString() + " " + weekNumberInt + " " + yearNumberInt);

						weeklyMetricsRepo.save(weeklyMetric);
					}
				}catch(Exception e){

				}



				// Need to check as there is confusion

				try {
					if (totalNumberOfRequirementsPlanned > 0) {

						Double cummulativeNumberOfTestableRequirementsDouble = new Double(cummulativeNumberOfTestableRequirementsCoveredSoFar);
						Double totalNumberOfRequirementsPlannedDouble = new Double(totalNumberOfRequirementsPlanned);
						Double temp = 0.0;

						try {
							temp = (((cummulativeNumberOfTestableRequirementsDouble * 100) / totalNumberOfRequirementsPlannedDouble));
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							weeklyMetric.setDesignCoverage("0.0");
							weeklyMetric.setDesignCoverageStatus("R");

						} else {
							temp = (Math.round(temp * 100.0) / 100.0);
							weeklyMetric.setDesignCoverage(temp.toString());

							ragStatus = checkRAGStatus(temp, id, weeklyMetric.getDesignCoverageName());
							System.out.println("The rag status is : " + ragStatus);
							weeklyMetric.setDesignCoverageStatus(ragStatus);

							System.out.println("Total number of requirements planned " + totalNumberOfRequirementsPlanned + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Cummulative Number Of Testable Requirements Covered So Far " + cummulativeNumberOfTestableRequirementsCoveredSoFar + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Design coverage % is : " + temp.toString() + " " + weekNumberInt + " " + yearNumberInt);
						}
						weeklyMetricsRepo.save(weeklyMetric);
					}
				}catch(Exception e){

				}


				try {
					if (totalNumberOfRequirementsPlanned > 0) {

						Double cummulativeNumberOfRequirementsDouble = new Double(cummulativeNumberOfTestableRequirementsCoveredSoFar);
						Double totalNumberOfRequirementsPlannedDouble = new Double(totalNumberOfRequirementsPlanned);
						Double numberOfPendingRequirements = totalNumberOfRequirementsPlannedDouble - cummulativeNumberOfRequirementsDouble;
						Double temp = 0.0;

						try {
							temp = (((numberOfPendingRequirements * 100) / totalNumberOfRequirementsPlannedDouble));
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							weeklyMetric.setPercentageOfRequirementsNotCovered("0.0");

						} else {
							temp = (Math.round(temp * 100.0) / 100.0);
							weeklyMetric.setPercentageOfRequirementsNotCovered(temp.toString());

							System.out.println("Total number of requirements planned " + totalNumberOfRequirementsPlanned + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Cummulative Number Of Testable Requirements not covered yet " + numberOfPendingRequirements + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Pending Design coverage % is : " + temp.toString() + " " + weekNumberInt + " " + yearNumberInt);
						}
						weeklyMetricsRepo.save(weeklyMetric);
					}
				}catch(Exception e){

				}



				Double numberOfValidDefectsLoggedInSITDouble = new Double(cummulativeNumberOfValidDefectsLoggedInSITSoFar);
				Double numberOfValidDefectsLoggedInUATDouble = new Double(cummulativeNumberOfValidDefectsLoggedInUATSoFar);
				Double cummulativeTotalNumberOfDefectsInProductionDouble = new Double(cummulativeTotalNumberOfDefectsInProductionSoFar);

				try {
					if (numberOfValidDefectsLoggedInSITDouble > 0) {
						Double temp = 0.0;

						try {
							temp = (numberOfValidDefectsLoggedInUATDouble * 100) / numberOfValidDefectsLoggedInSITDouble;
						} catch (Exception e) {
						}
						if (temp == 0) {
							weeklyMetric.setDefectLeakageSITToUAT("0.0");
							weeklyMetric.setDefectLeakageSITToUATStatus("R");
						} else {
							temp = 100 - temp;
							temp = (Math.round(temp * 100.0)) / 100.0;
							weeklyMetric.setDefectLeakageSITToUAT(temp.toString());

							ragStatus = checkRAGStatus(temp, id, weeklyMetric.getDefectLeakageSITToUATName());
							System.out.println("The rag status is : " + ragStatus);
							weeklyMetric.setDefectLeakageSITToUATStatus(ragStatus);

							System.out.println("Total number of defect found in SIT so far " + cummulativeNumberOfValidDefectsLoggedInSITSoFar + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Total number of defect found in UAT so far " + cummulativeNumberOfValidDefectsLoggedInUATSoFar + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("SIT to UAT defect leakage % : " + temp.toString() + " " + weekNumberInt + " " + yearNumberInt);
						}
						weeklyMetricsRepo.save(weeklyMetric);
					}
				}catch(Exception e){

				}


				try {
					if ((numberOfValidDefectsLoggedInSITDouble > 0) || (numberOfValidDefectsLoggedInUATDouble > 0)) {

						Double temp = 0.0;
						temp = ((cummulativeTotalNumberOfDefectsInProductionDouble * 100) / (numberOfValidDefectsLoggedInSITDouble + numberOfValidDefectsLoggedInUATDouble));

						if (temp == 0) {
							weeklyMetric.setDefectLeakageToProduction("0.0");
							weeklyMetric.setDefectLeakageToProductionStatus("R");
						} else {
							temp = 100 - temp;
							temp = (Math.round(temp * 100.0)) / 100.0;
							weeklyMetric.setDefectLeakageToProduction(temp.toString());

							ragStatus = checkRAGStatus(temp, id, weeklyMetric.getDefectLeakageToProductionName());
							System.out.println("The rag status is : " + ragStatus);
							weeklyMetric.setDefectLeakageToProductionStatus(ragStatus);

							System.out.println("Total number of defect found in SIT so far " + cummulativeNumberOfValidDefectsLoggedInSITSoFar + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Total number of defect found in UAT so far " + cummulativeNumberOfValidDefectsLoggedInUATSoFar + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Total number of defects found Production so far " + cummulativeTotalNumberOfDefectsInProductionSoFar + " " + weekNumberInt + " " + yearNumberInt);
							System.out.println("Production defect leakage % : " + temp.toString() + " " + weekNumberInt + " " + yearNumberInt);
						}

						weeklyMetricsRepo.save(weeklyMetric);
					}
				}catch(Exception e){

				}

				try {weeklyMetric.setNumberOfTestableRequirementsTillDate(Integer.toString(cummulativeNumberOfTestableRequirementsCoveredSoFar));}catch(Exception e){}
				try {weeklyMetric.setManualTestCaseExecutionEffort(Integer.toString(manualTestCaseExecutionEffortForTheWeek));}catch(Exception e){}
				try {weeklyMetric.setRegressionAutomationTestCaseExecutionEffort(Integer.toString(regressionAutomationTestCaseExecutionEffortForTheWeek));}catch(Exception e){}
				try {weeklyMetric.setAPIAutomationTestCaseExecutionEffort(Integer.toString(apiAutomationTestCaseExecutionEffortForTheWeek));}catch(Exception e){}
				try {weeklyMetric.setInProjectAutomationTestCaseExecutionEffort(Integer.toString(inProjectAutomationTestCaseExecutionEffortForTheWeek));}catch(Exception e){}
				try {weeklyMetric.setNumberOfValidDefectsLoggedInSITForTheWeek(Integer.toString(numberOfValidDefectsLoggedInSITForTheWeek));}catch(Exception e){}
				try {weeklyMetric.setNumberOfValidDefectsLoggedInUATForTheWeek(Integer.toString(numberOfValidDefectsLoggedInUATForTheWeek));}catch(Exception e){}
				try {weeklyMetric.setAverageTimTakenToResolveSev1Defects(Integer.toString(averageTimeTakenToResolveSev1Defects));}catch(Exception e){}
				try {weeklyMetric.setNumberOfDefectsFoundUsingAutomationSoFar(Integer.toString(cummulativeNumberOfDefectsFoundUsingAutomationSoFar));}catch(Exception e){}


				try {
					Double tempDouble = new Double(weeklyMetric.getNumberOfDefectsRejectedForTheWeek());
					ragStatus = checkRAGStatus(tempDouble, id, weeklyMetric.getNumberOfDefectsRejectedForTheWeekName());
					System.out.println("The rag status is : " + ragStatus);
					weeklyMetric.setNumberOfDefectsRejectedForTheWeekStatus(ragStatus);
				}catch(Exception e){

				}

				try {
					Double tempDouble = new Double(weeklyMetric.getTotalNumberOfDefectsReopenedForTheWeek());
					ragStatus = checkRAGStatus(tempDouble, id, weeklyMetric.getTotalNumberOfDefectsReopenedForTheWeekName());
					System.out.println("The rag status is : " + ragStatus);
					weeklyMetric.setTotalNumberOfDefectsReopenedForTheWeekStatus(ragStatus);
				}catch(Exception e){
				}

				try {
					Double tempDouble = new Double(weeklyMetric.getPercentageOfPendingRegressionAutomation());
					ragStatus = checkRAGStatus(tempDouble, id, weeklyMetric.getPercentageOfPendingRegressionAutomationName());
					System.out.println("The rag status is : " + ragStatus);
					weeklyMetric.setPercentageOfPendingRegressionAutomationStatus(ragStatus);
				}catch(Exception e){
				}

				try {
					Double tempDouble = new Double(weeklyMetric.getPercentageOfRequirementsNotCovered());
					ragStatus = checkRAGStatus(tempDouble, id, weeklyMetric.getPercentageOfRequirementsNotCoveredName());
					System.out.println("The rag status is : " + ragStatus);
					weeklyMetric.setPercentageOfRequirementsNotCoveredStatus(ragStatus);
				}catch(Exception e){
				}

				try {
					Double tempDouble = new Double(weeklyMetric.getAverageTimeTakenToResolveSev1Defects());
					ragStatus = checkRAGStatus(tempDouble, id, weeklyMetric.getAverageTimeTakenToResolveSev1DefectsName());
					System.out.println("The rag status for sev1 defects is : " + ragStatus);
					weeklyMetric.setAverageTimeTakenToResolveSev1DefectsStatus(ragStatus);
				}catch(Exception e){
				}


				weeklyMetricsRepo.save(weeklyMetric);
			}


			Iterable<WeeklyMetrics> weeklyMetricsIterable = weeklyMetricsRepo.findAll();
			Iterator<WeeklyMetrics> weeklyMetricsIterator = weeklyMetricsIterable.iterator();

			WeeklyMetrics weeklyMetric = null;


			while (weeklyMetricsIterator.hasNext()) {
				weeklyMetric = weeklyMetricsIterator.next();

				if (weeklyMetric.getProject().getId() == id) {

					String totalString = "{\"Metrics Measurements\":";

					totalString += weeklyMetric.getWeekNumber();
					totalString += ",\"";

					totalString += weeklyMetric.getMonthNumberName();
					totalString += "\":";

					totalString += weeklyMetric.getMonthNumber();
					totalString += ",\"";

					totalString += weeklyMetric.getYearNumberName();
					totalString += "\":";

					totalString += weeklyMetric.getYearNumber();
					totalString += ",\"";

/*
					Iterable<ModuleLevelMetrics> moduleLevelMetricsIterable = moduleLevelMetricsRepository.findAll();
					Iterator<ModuleLevelMetrics> moduleLevelMetricsIterator = moduleLevelMetricsIterable.iterator();

					ModuleLevelMetrics moduleLevelMetric = null;


					while (moduleLevelMetricsIterator.hasNext()) {
						moduleLevelMetric = moduleLevelMetricsIterator.next();

						totalString += moduleLevelMetric.getModuleName() + "-" + moduleLevelMetric.getManualTestCaseAuthoringProductivityName();
						totalString += "\":";

						totalString += moduleLevelMetric.getManualTestCaseAuthoringProductivity();
						totalString += ",\"";


						totalString += moduleLevelMetric.getModuleName() + "-" + moduleLevelMetric.getAutomationTestCaseScriptingProductivityName();
						totalString += "\":";

						totalString += weeklyMetric.getAPIAutomationTestExecutionProductivity();
						totalString += ",\"";


						totalString += moduleLevelMetric.getModuleName() + "-" + moduleLevelMetric.getManualTestExecutionProductivityName();
						totalString += "\":";

						totalString += moduleLevelMetric.getManualTestExecutionProductivity();
						totalString += ",\"";

						totalString += moduleLevelMetric.getModuleName() + "-" + moduleLevelMetric.getAutomationTestExecutionProductivityName();
						totalString += "\":";

						totalString += weeklyMetric.getAPIAutomationTestExecutionProductivity();
						totalString += ",\"";


						totalString += moduleLevelMetric.getModuleName() + "-" + moduleLevelMetric.getDefectsForTheWeekName();
						totalString += "\":";

						totalString += moduleLevelMetric.getDefectsForTheWeek();
						totalString += ",\"";
					}
*/
					totalString += weeklyMetric.getManualTestCaseAuthoringProductivityName();
					totalString += "\":";

					totalString += weeklyMetric.getManualTestCaseAuthoringProductivity();
					totalString += ",\"";

					totalString += weeklyMetric.getRegressionTestAutomationScriptingProductivityName();
					totalString += "\":";

					totalString += weeklyMetric.getRegressionTestAutomationScriptingProductivity();
					totalString += ",\"";

					totalString += weeklyMetric.getDesignCoverageName();
					totalString += "\":";

					totalString += weeklyMetric.getDesignCoverage();
					totalString += ",\"";

					totalString += weeklyMetric.getPercentageOfRegressionAutomationName();
					totalString += "\":";

					totalString += weeklyMetric.getPercentageOfRegressionAutomation();
					totalString += ",\"";

					totalString += weeklyMetric.getTestReviewEfficiencyName();
					totalString += "\":";

					totalString += weeklyMetric.getTestReviewEfficiency();
					totalString += ",\"";

					totalString += weeklyMetric.getDowntimeAndImpactAnalysisDesignAvgProductivityLostName();
					totalString += "\":";

					totalString += weeklyMetric.getDowntimeAndImpactAnalysisDesignAvgProductivityLost();
					totalString += ",\"";

					totalString += weeklyMetric.getManualTestExecutionProductivityName();
					totalString += "\":";

					totalString += weeklyMetric.getManualTestExecutionProductivity();
					totalString += ",\"";

					totalString += weeklyMetric.getRegressionAutomationTestExecutionProductivityName();
					totalString += "\":";

					totalString += weeklyMetric.getRegressionAutomationTestExecutionProductivity();
					totalString += ",\"";

					totalString += weeklyMetric.getInProjectAutomationTestExecutionProductivityName();
					totalString += "\":";

					totalString += weeklyMetric.getInProjectAutomationTestExecutionProductivity();
					totalString += ",\"";

					totalString += weeklyMetric.getAPIAutomationTestExecutionProductivityName();
					totalString += "\":";

					totalString += weeklyMetric.getAPIAutomationTestExecutionProductivity();
					totalString += ",\"";

					totalString += weeklyMetric.getMigratedDataExecutionName();
					totalString += "\":";

					totalString += weeklyMetric.getMigratedDataExecution();
					totalString += ",\"";

					totalString += weeklyMetric.getDefectRejectionName();
					totalString += "\":";

					totalString += weeklyMetric.getDefectRejection();
					totalString += ",\"";

					totalString += weeklyMetric.getTestCaseNotExecutedManualName();
					totalString += "\":";

					totalString += weeklyMetric.getTestCaseNotExecutedManual();
					totalString += ",\"";

					totalString += weeklyMetric.getTestCaseNotExecutedAutomationName();
					totalString += "\":";

					totalString += weeklyMetric.getTestCaseNotExecutedAutomation();
					totalString += ",\"";

					totalString += weeklyMetric.getCurrentQualityRatioName();
					totalString += "\":";

					totalString += weeklyMetric.getCurrentQualityRatio();
					totalString += ",\"";

					totalString += weeklyMetric.getQualityOfFixesName();
					totalString += "\":";

					totalString += weeklyMetric.getQualityOfFixes();
					totalString += ",\"";

					totalString += weeklyMetric.getDowntimeAndImpactAnalysisExecutionAvgProductivityLostName();
					totalString += "\":";

					totalString += weeklyMetric.getDowntimeAndImpactAnalysisExecutionAvgProductivityLost();
					totalString += ",\"";

					totalString += weeklyMetric.getNumberOfTestableRequirementsTillDateName();
					totalString += "\":";

					totalString += weeklyMetric.getNumberOfTestableRequirementsTillDate();
					totalString += ",\"";

					totalString += weeklyMetric.getPercentageOfRequirementsNotCoveredName();
					totalString += "\":";

					totalString += weeklyMetric.getPercentageOfRequirementsNotCovered();
					totalString += ",\"";

					totalString += weeklyMetric.getPercentageOfPendingRegressionAutomationName();
					totalString += "\":";

					totalString += weeklyMetric.getPercentageOfPendingRegressionAutomation();
					totalString += ",\"";

					totalString += weeklyMetric.getNumberOfDefectsReportedByTestingTeamforTheWeekName();
					totalString += "\":";
					totalString += weeklyMetric.getNumberOfDefectsReportedByTestingTeamforTheWeek();
					totalString += ",\"";


					totalString += weeklyMetric.getNumberOfDefectsRejectedForTheWeekName();
					totalString += "\":";

					totalString += weeklyMetric.getNumberOfDefectsRejectedForTheWeek();
					totalString += ",\"";

					totalString += weeklyMetric.getTotalNumberOfDefectsReopenedForTheWeekName();
					totalString += "\":";

					totalString += weeklyMetric.getTotalNumberOfDefectsReopenedForTheWeek();
					totalString += ",\"";

					totalString += weeklyMetric.getNumberOfValidDefectsLoggedInSITForTheWeekName();
					totalString += "\":";

					totalString += weeklyMetric.getNumberOfValidDefectsLoggedInSITForTheWeek();
					totalString += ",\"";

					totalString += weeklyMetric.getNumberOfValidDefectsLoggedInUATForTheWeekName();
					totalString += "\":";

					totalString += weeklyMetric.getNumberOfValidDefectsLoggedInUATForTheWeek();
					totalString += ",\"";

					totalString += weeklyMetric.getManualTestCaseExecutionEffortName();
					totalString += "\":";

					totalString += weeklyMetric.getManualTestCaseExecutionEffort();
					totalString += ",\"";

					totalString += weeklyMetric.getRegressionAutomationTestCaseExecutionEffortName();
					totalString += "\":";

					totalString += weeklyMetric.getRegressionAutomationTestCaseExecutionEffort();
					totalString += ",\"";

					totalString += weeklyMetric.getAPIAutomationTestCaseExecutionEffortName();
					totalString += "\":";

					totalString += weeklyMetric.getAPIAutomationTestCaseExecutionEffort();
					totalString += ",\"";

					totalString += weeklyMetric.getInProjectAutomationTestCaseExecutionEffortName();
					totalString += "\":";

					totalString += weeklyMetric.getInProjectAutomationTestCaseExecutionEffort();
					totalString += ",\"";

					totalString += weeklyMetric.getAverageTimeTakenToResolveSev1DefectsName();
					totalString += "\":";

					totalString += weeklyMetric.getAverageTimeTakenToResolveSev1Defects();
					totalString += ",\"";

					totalString += weeklyMetric.getDefectLeakageSITToUATName();
					totalString += "\":";

					totalString += weeklyMetric.getDefectLeakageSITToUAT();
					totalString += ",\"";


					totalString += weeklyMetric.getDefectLeakageToProductionName();
					totalString += "\":";

					totalString += weeklyMetric.getDefectLeakageToProduction();
					totalString += ",\"";



					totalString = addMetricsLCLUCLGoal(id, totalString);

					Iterable<ModuleLevelMetrics> moduleLevelMetricsIterable = moduleLevelMetricsRepository.findAll();
					Iterator<ModuleLevelMetrics> moduleLevelMetricsIterator = moduleLevelMetricsIterable.iterator();
					ArrayList<String> moduleNames = new ArrayList<String>();

					while (moduleLevelMetricsIterator.hasNext()) {

						ModuleLevelMetrics next = moduleLevelMetricsIterator.next();

						if ((next.getProject().getId() == id)) {

							boolean moduleFound = false;

							for (int index = 0; index < moduleNames.size(); index++) {

								if (moduleNames.get(index).compareTo(next.getModuleName()) == 0) {
									moduleFound = true;
									break;
								}
							}

							if (moduleFound == false)
								moduleNames.add(next.getModuleName());
						}
					}

					Collections.sort(moduleNames);

					for (int i = 0; i < moduleNames.size(); i++) {

						moduleLevelMetricsIterable = moduleLevelMetricsRepository.findAll();
						moduleLevelMetricsIterator = moduleLevelMetricsIterable.iterator();

						int totalNumberOfTestCasesDevelopedTillThisWeek = 0;
						int numberOfRegressionTestCasesDevelopedTillThisWeek = 0;

						int totalNumberOfTestCasesExecutedTillThisWeek = 0;
						int numberOfRegressionTestCasesExecutedTillThisWeek = 0;


						while (moduleLevelMetricsIterator.hasNext()) {


							ModuleLevelMetrics next = moduleLevelMetricsIterator.next();

							// If the current project id from the iterator matched with the user provided project id
							if ((next.getProject().getId() == id)
									&& (next.getWeekNumber() <= weeklyMetric.getWeekNumber())
									&& next.getModuleName().compareTo(moduleNames.get(i)) == 0) {
								totalNumberOfTestCasesDevelopedTillThisWeek += next.getNumberOfManualTestCasesAuthored();
								totalNumberOfTestCasesDevelopedTillThisWeek += next.getNumberOfRegressionAutomationTestCasesScripted();
								numberOfRegressionTestCasesDevelopedTillThisWeek = next.getNumberOfRegressionAutomationTestCasesScripted();

								totalNumberOfTestCasesExecutedTillThisWeek += next.getNumberOfManualTestCasesExecuted();
								totalNumberOfTestCasesExecutedTillThisWeek += next.getNumberOfRegressionAutomationTestCasesExecuted();
								numberOfRegressionTestCasesExecutedTillThisWeek = next.getNumberOfRegressionAutomationTestCasesExecuted();

							}

						}
						Iterable<LatestModuleEstimate> allLatestModuleEstimates = latestModuleEstimateRepository.findAll();
						Iterator<LatestModuleEstimate> latestModuleEstimateIterator = allLatestModuleEstimates.iterator();
						int totalTestCaseEstimateForTheModule = 0;
						int totalRegressionTestCaseEstimateForTheModule = 0;


						while (latestModuleEstimateIterator.hasNext()) {
							LatestModuleEstimate latestModulesEstimate = latestModuleEstimateIterator.next();

							if ((latestModulesEstimate.getProject().getId() == id) && (latestModulesEstimate.getName().compareTo(moduleNames.get(i)) == 0)) {
								try {
									totalTestCaseEstimateForTheModule = Integer.parseInt(latestModulesEstimate.getEstimatedManualTestcases());
									totalTestCaseEstimateForTheModule += Integer.parseInt(latestModulesEstimate.getEstimatedRegressionAutomationTestCases());
									totalRegressionTestCaseEstimateForTheModule = Integer.parseInt(latestModulesEstimate.getEstimatedRegressionAutomationTestCases());
									break;
								} catch (Exception e) {
								}
							}
						}

						totalString += "Number of Test Cases estimated for the module - " + moduleNames.get(i);
						totalString += "\":";

						totalString += totalTestCaseEstimateForTheModule;
						totalString += ",\"";

						totalString += "Number of Test Cases designed for the Week for the module - " + moduleNames.get(i);
						totalString += "\":";

						totalString += totalNumberOfTestCasesDevelopedTillThisWeek;
						totalString += ",\"";


						totalString += "Number of Regression Test Cases estimated for the module - " + moduleNames.get(i);
						totalString += "\":";

						totalString += totalRegressionTestCaseEstimateForTheModule;
						totalString += ",\"";

						totalString += "Number of Regression Test Cases designed for the Week for the module - " + moduleNames.get(i);
						totalString += "\":";

						totalString += numberOfRegressionTestCasesDevelopedTillThisWeek;
						totalString += ",\"";

						totalString += "Number of Test Cases executed for the Week for the module - " + moduleNames.get(i);
						totalString += "\":";

						totalString += totalNumberOfTestCasesExecutedTillThisWeek;
						totalString += ",\"";

						totalString += "Number of Test Cases regression test cases executed for the Week for the module - " + moduleNames.get(i);
						totalString += "\":";

						totalString += numberOfRegressionTestCasesExecutedTillThisWeek;
						totalString += ",\"";


					}


					totalString = totalString.substring(0, totalString.length() - 2);
					totalString += "}";

					System.out.println("The weekly test design, test executions metrics string is : " + totalString);

					try {

						HttpHost host = new HttpHost("localhost", 9200);
						final RestClientBuilder builder = RestClient.builder(host);
						client = new RestHighLevelClient(builder);
						save(Integer.toString(weeklyMetric.getWeekNumber()), totalString, WEEKLY_INDEX);
						client.indices().refresh(new RefreshRequest(WEEKLY_INDEX), RequestOptions.DEFAULT);
						client.close();

					} catch (Exception e) {
						System.out.println(e.getMessage());
					}

				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}


/*
		Iterable<WeeklyProjectGovernance> weeklyProjectGovernanceIterable = weeklyProjectGovernanceRepository.findAll();
		Iterator<WeeklyProjectGovernance> weeklyProjectGovernanceIterator = weeklyProjectGovernanceIterable.iterator();


		WeeklyProjectGovernance weeklyProjectGovernance = null;

		while (weeklyProjectGovernanceIterator.hasNext()) {
			weeklyProjectGovernance = weeklyProjectGovernanceIterator.next();

			if ((weeklyProjectGovernance.getProject().getId() == id)) {

				String totalString = "{\"Metrics Measurements\":";

				totalString += weeklyProjectGovernance.getWeekNumber();
				totalString += ",\"";

				totalString += weeklyProjectGovernance.getMonthNumberName();
				totalString += "\":";

				totalString += weeklyProjectGovernance.getMonthNumber();
				totalString += ",\"";

				totalString += weeklyProjectGovernance.getYearNumberName();
				totalString += "\":";

				totalString += weeklyProjectGovernance.getYearNumber();
				totalString += ",\"";

				totalString += weeklyProjectGovernance.getNumberOfFormalGovernanceMeetingsName();
				totalString += "\":";

				totalString += weeklyProjectGovernance.getNumberOfFormalGovernanceMeetings();
				totalString += ",\"";

				totalString += weeklyProjectGovernance.getRCAPoorRequirementName();
				totalString += "\":";

				totalString += weeklyProjectGovernance.getRCAPoorRequirement();
				totalString += ",\"";

				totalString += weeklyProjectGovernance.getRCALateRequirementName();
				totalString += "\":";

				totalString += weeklyProjectGovernance.getRCALateRequirement();
				totalString += ",\"";

				totalString += weeklyProjectGovernance.getRCADesignErrorName();
				totalString += "\":";

				totalString += weeklyProjectGovernance.getRCADesignError();
				totalString += ",\"";

				totalString += weeklyProjectGovernance.getRCACodingErrorName();
				totalString += "\":";

				totalString += weeklyProjectGovernance.getRCACodingError();
				totalString += ",\"";

				totalString += weeklyProjectGovernance.getRCAInsufficientTestingName();
				totalString += "\":";

				totalString += weeklyProjectGovernance.getRCAInsufficientTesting();
				totalString += ",\"";

				totalString += weeklyProjectGovernance.getRCAProcessRelatedName();
				totalString += "\":";

				totalString += weeklyProjectGovernance.getRCAProcessRelated();
				totalString += ",\"";

				totalString += weeklyProjectGovernance.getRCATestDataName();
				totalString += "\":";

				totalString += weeklyProjectGovernance.getRCATestData();
				totalString += ",\"";

				totalString += weeklyProjectGovernance.getRCATestEnvironmentConfigurationName();
				totalString += "\":";

				totalString += weeklyProjectGovernance.getRCATestEnvironmentConfiguration();
				totalString += ",\"";

				totalString += weeklyProjectGovernance.getRCAOthersName();
				totalString += "\":";

				totalString += weeklyProjectGovernance.getRCAOthers();
				totalString += "}";

				System.out.println("The weekly governance metrics string is : " + totalString);


				try {

					HttpHost host = new HttpHost("localhost", 9200);
					final RestClientBuilder builder = RestClient.builder(host);
					client = new RestHighLevelClient(builder);
					save(Integer.toString(weeklyProjectGovernance.getWeekNumber()), totalString, WEEKLY_INDEX); // Add New Index
					client.indices().refresh(new RefreshRequest(WEEKLY_INDEX), RequestOptions.DEFAULT);
					client.close();

				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
		}

*/


		// Reinitialize all the iterators
		weeklyProjectLevelTestDesignProgressIterable = weeklyProjectLevelTestDesignProgressRepository.findAll();
		weeklyProjectLevelTestDesignProgressRepositoryIterator = weeklyProjectLevelTestDesignProgressIterable.iterator();

		weeklyProjectLevelTestExecutionProgressIterable = weeklyProjectLevelTestExecutionProgressRepository.findAll();
		weeklyProjectLevelTestExecutionProgressRepositoryIterator = weeklyProjectLevelTestExecutionProgressIterable.iterator();

		weeklyModuleLevelTestDesignProgressIterable = weeklyModuleLevelTestDesignProgressRepository.findAll();
		weeklyModuleLevelTestDesignProgressRepositoryIterator = weeklyModuleLevelTestDesignProgressIterable.iterator();

		weeklyModuleLevelTestExecutionProgressIterable = weeklyModuleLevelTestExecutionProgressRepository.findAll();
		weeklyModuleLevelTestExecutionProgressRepositoryIterator = weeklyModuleLevelTestExecutionProgressIterable.iterator();

		Iterable<ModuleLevelMetrics> moduleLevelMetricsIterable = moduleLevelMetricsRepository.findAll();
		Iterator<ModuleLevelMetrics> moduleLevelMetricsIterator = moduleLevelMetricsIterable.iterator();
		ArrayList<String> moduleNames = new ArrayList<String>();

		while (moduleLevelMetricsIterator.hasNext()) {

			ModuleLevelMetrics next = moduleLevelMetricsIterator.next();

			if ((next.getProject().getId() == id)) {

				boolean moduleFound = false;

				for (int index = 0; index < moduleNames.size(); index++) {

					if (moduleNames.get(index).compareTo(next.getModuleName()) == 0) {
						moduleFound = true;
						break;
					}
				}

				if (moduleFound == false)
					moduleNames.add(next.getModuleName());
			}
		}

		Collections.sort(moduleNames);


		for (int i = 0; i < moduleNames.size(); i++) {


			long moduleId = 0l;
			int totalTestCaseEstimateForTheModule = 0;
			int totalManualTestCaseEstimateForTheModule = 0;
			int totalRegressionTestCaseEstimateForTheModule = 0;

			int totalNumberOfManualTestCasesDevelopedForTheModuleSoFar = 0;
			int totalnumberOfRegressionTestCasesDevelopedForTheModuleSoFar = 0;
			int totalNumberOfTestCasesDevelopedForTheModuleSoFar = 0;

			int totalNumberOfManualTestCasesExecutedForTheModuleSoFar = 0;
			int totalNumberOfRegressionTestCasesExecutedForTheModuleSoFar = 0;
			int totalNumberOfTestCasesExecutedForTheModuleSoFar = 0;

			moduleLevelMetricsIterable = moduleLevelMetricsRepository.findAll();
			moduleLevelMetricsIterator = moduleLevelMetricsIterable.iterator();

			while (moduleLevelMetricsIterator.hasNext()) {

				ModuleLevelMetrics next = moduleLevelMetricsIterator.next();

				if ((next.getProject().getId() == id)
						&& next.getModuleName().compareTo(moduleNames.get(i)) == 0) {

					moduleId = next.getModuleId();
					totalNumberOfManualTestCasesDevelopedForTheModuleSoFar += next.getNumberOfManualTestCasesAuthored();
					totalNumberOfTestCasesDevelopedForTheModuleSoFar += next.getNumberOfManualTestCasesAuthored();
					totalnumberOfRegressionTestCasesDevelopedForTheModuleSoFar += next.getNumberOfRegressionAutomationTestCasesScripted();
					totalNumberOfTestCasesDevelopedForTheModuleSoFar += next.getNumberOfRegressionAutomationTestCasesScripted();

					totalNumberOfManualTestCasesExecutedForTheModuleSoFar += next.getNumberOfManualTestCasesExecuted();
					totalNumberOfTestCasesExecutedForTheModuleSoFar += next.getNumberOfRegressionAutomationTestCasesExecuted();
					totalNumberOfRegressionTestCasesExecutedForTheModuleSoFar += next.getNumberOfManualTestCasesExecuted();
					totalNumberOfTestCasesExecutedForTheModuleSoFar += next.getNumberOfRegressionAutomationTestCasesExecuted();

				}
			}

			Iterable<LatestModuleEstimate> allLatestModuleEstimates = latestModuleEstimateRepository.findAll();
			Iterator<LatestModuleEstimate> latestModuleEstimateIterator = allLatestModuleEstimates.iterator();

			while (latestModuleEstimateIterator.hasNext()) {
				LatestModuleEstimate latestModulesEstimate = latestModuleEstimateIterator.next();

				if ((latestModulesEstimate.getProject().getId() == id) && (latestModulesEstimate.getName().compareTo(moduleNames.get(i)) == 0)) {
					try {
						totalTestCaseEstimateForTheModule = Integer.parseInt(latestModulesEstimate.getEstimatedManualTestcases());
						totalTestCaseEstimateForTheModule += Integer.parseInt(latestModulesEstimate.getEstimatedRegressionAutomationTestCases());
						totalManualTestCaseEstimateForTheModule = Integer.parseInt(latestModulesEstimate.getEstimatedManualTestcases());
						totalRegressionTestCaseEstimateForTheModule = Integer.parseInt(latestModulesEstimate.getEstimatedRegressionAutomationTestCases());
						break;
					} catch (Exception e) {
					}
				}
			}

			String totalString = "{\"Metrics Measurements\":";

			totalString += moduleId;
			totalString += ",\"";

			totalString += "Number of Test Cases estimated for the module - " + moduleNames.get(i);
			totalString += "\":";

			totalString += totalTestCaseEstimateForTheModule;
			totalString += ",\"";


			totalString += "Number of Manual Test Cases estimated for the module - " + moduleNames.get(i);
			totalString += "\":";

			totalString += totalManualTestCaseEstimateForTheModule;
			totalString += ",\"";


			totalString += "Number of Regression Automation Test Cases estimated for the module - " + moduleNames.get(i);
			totalString += "\":";

			totalString += totalRegressionTestCaseEstimateForTheModule;
			totalString += ",\"";

			totalString += "Total Number of Manual Test Cases designed so far for the module - " + moduleNames.get(i);
			totalString += "\":";

			totalString += totalNumberOfManualTestCasesDevelopedForTheModuleSoFar;
			totalString += ",\"";

			totalString += "Total Number of Regression Automation Test Cases designed so far for the module - " + moduleNames.get(i);
			totalString += "\":";

			totalString += totalnumberOfRegressionTestCasesDevelopedForTheModuleSoFar;
			totalString += ",\"";


			totalString += "Total Number of Test Cases designed for the Week for the module - " + moduleNames.get(i);
			totalString += "\":";

			totalString += totalNumberOfTestCasesDevelopedForTheModuleSoFar;
			totalString += ",\"";

			totalString += "Total Number of Test Cases executed for the Week for the module - " + moduleNames.get(i);
			totalString += "\":";

			totalString += totalNumberOfTestCasesExecutedForTheModuleSoFar;
			totalString += ",\"";

			totalString += "Number of Test Cases manual test cases executed for the Week for the module - " + moduleNames.get(i);
			totalString += "\":";

			totalString += totalNumberOfManualTestCasesExecutedForTheModuleSoFar;
			totalString += ",\"";

			totalString += "Number of Test Cases regression test cases executed for the Week for the module - " + moduleNames.get(i);
			totalString += "\":";

			totalString += totalNumberOfRegressionTestCasesExecutedForTheModuleSoFar;
			totalString += "}";

			System.out.println("The module level details are : " + totalString);
			// Push the data to server using HTTP Post using a different index
			try {

				HttpHost host = new HttpHost("localhost", 9200);
				final RestClientBuilder builder = RestClient.builder(host);
				client = new RestHighLevelClient(builder);
				save(Long.toString(moduleId), totalString, MODULE_INDEX); // Add New Index
				client.indices().refresh(new RefreshRequest(MODULE_INDEX), RequestOptions.DEFAULT);
				client.close();

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}





		int totalNumberOfManualTestCasesDevelopedSoFar = 0;
		int totalnumberOfRegressionTestCasesDevelopedSoFar = 0;
		int totalNumberOfTestCasesDevelopedSoFar = 0;
		int totalNumberOfAutomationTestCasesDevelopedSoFar = 0;

		while (weeklyModuleLevelTestDesignProgressRepositoryIterator.hasNext()) {

			WeeklyModuleLevelTestDesignProgress next = weeklyModuleLevelTestDesignProgressRepositoryIterator.next();

			if (next.getProject().getId() == id) {
				totalNumberOfManualTestCasesDevelopedSoFar += next.getNumberOfManualTestCasesDesignedForTheWeek();
				totalnumberOfRegressionTestCasesDevelopedSoFar += next.getNumberOfRegressionAutomationTestScriptsDesignedForTheWeek();
				totalNumberOfTestCasesDevelopedSoFar += next.getNumberOfManualTestCasesDesignedForTheWeek();
				totalNumberOfTestCasesDevelopedSoFar += next.getNumberOfRegressionAutomationTestScriptsDesignedForTheWeek();
				totalNumberOfAutomationTestCasesDevelopedSoFar += next.getNumberOfRegressionAutomationTestScriptsDesignedForTheWeek();
			}
		}


		int totalNumberOfInProjectAutomationTestCasesDevelopedSoFar = 0;
		int totalNumberOfAPIAutomationTestCasesDevelopedSoFar = 0;
		int totalNumberOfRequirementsCoveredSoFar = 0;

		while (weeklyProjectLevelTestDesignProgressRepositoryIterator.hasNext()) {

			WeeklyProjectLevelTestDesignProgress next = weeklyProjectLevelTestDesignProgressRepositoryIterator.next();

			if ((next.getProject().getId() == id)) {

				totalNumberOfAutomationTestCasesDevelopedSoFar += next.getTotalNumberOfInProjectAutomationTestCasesAutomated();
				totalNumberOfAutomationTestCasesDevelopedSoFar += next.getTotalNumberOfAPIAutomationTestCasesAutomated();

				totalNumberOfTestCasesDevelopedSoFar += next.getTotalNumberOfInProjectAutomationTestCasesAutomated();
				totalNumberOfTestCasesDevelopedSoFar += next.getTotalNumberOfAPIAutomationTestCasesAutomated();

				totalNumberOfInProjectAutomationTestCasesDevelopedSoFar += next.getTotalNumberOfInProjectAutomationTestCasesAutomated();
				totalNumberOfAPIAutomationTestCasesDevelopedSoFar += next.getTotalNumberOfAPIAutomationTestCasesAutomated();

				totalNumberOfRequirementsCoveredSoFar += next.getNumberOfTestableRequirementsCoveredThisWeek();

			}

		}


		int totalNumberOfManualTestCasesExecutedSoFar = 0;
		int totalnumberOfRegressionTestCasesExecutedSoFar = 0;
		int totalNumberOfTestCasesExecutedSoFar = 0;
		int totalNumberOfAutomationTestCasesExecutedSoFar = 0;


		while (weeklyModuleLevelTestExecutionProgressRepositoryIterator.hasNext()) {

			WeeklyModuleLevelTestExecutionProgress next = weeklyModuleLevelTestExecutionProgressRepositoryIterator.next();

			if (next.getProject().getId() == id) {

				totalNumberOfManualTestCasesExecutedSoFar += next.getNumberOfManualTestCasesExecutedForTheWeek();
				totalnumberOfRegressionTestCasesExecutedSoFar += next.getNumberOfRegressionAutomationTestCasesExecutedForTheWeek();
				totalNumberOfTestCasesExecutedSoFar += next.getNumberOfManualTestCasesExecutedForTheWeek();
				totalNumberOfTestCasesExecutedSoFar += next.getNumberOfRegressionAutomationTestCasesExecutedForTheWeek();
				totalNumberOfAutomationTestCasesExecutedSoFar += next.getNumberOfRegressionAutomationTestCasesExecutedForTheWeek();

			}
		}

		int totalNumberOfInProjectAutomationTestScriptsExecutedSoFar = 0;
		int totalNumberOfAPIAutomationTestScriptsExecutedSoFar = 0;

		int totalNumberOfDefectsReportedByTestingTeamSoFar = 0;
		int totalNumberOfDefectsRejectedSoFar = 0;
		int totalNumberOfDefectsReopenedSoFar = 0;
		int totalNumberOfDefectsFixedSoFar = 0;

		int totalNumberOfValidDefectsLoggedInSITSoFar = 0;
		int totalNumberOfValidDefectsLoggedInUATSoFar = 0;

		int totalNumberOfTestCasesExecutedWithMigratedDataSoFar = 0;


		while (weeklyProjectLevelTestExecutionProgressRepositoryIterator.hasNext()) {

			WeeklyProjectLevelTestExecutionProgress next = weeklyProjectLevelTestExecutionProgressRepositoryIterator.next();

			if ((next.getProject().getId() == id)) {

				totalNumberOfTestCasesExecutedSoFar += next.getNumberOfInProjectAutomationTestScriptsExecuted();
				totalNumberOfTestCasesExecutedSoFar += next.getNumberOfAPIAutomationTestScriptsExecuted();
				totalNumberOfAutomationTestCasesExecutedSoFar += next.getNumberOfInProjectAutomationTestScriptsExecuted();
				totalNumberOfAutomationTestCasesExecutedSoFar += next.getNumberOfAPIAutomationTestScriptsExecuted();

				totalNumberOfInProjectAutomationTestScriptsExecutedSoFar = next.getNumberOfInProjectAutomationTestScriptsExecuted();
				totalNumberOfAPIAutomationTestScriptsExecutedSoFar = next.getNumberOfAPIAutomationTestScriptsExecuted();

				totalNumberOfDefectsReportedByTestingTeamSoFar = next.getNumberOfDefectsReportedByTestingTeam();
				totalNumberOfDefectsRejectedSoFar = next.getNumberOfDefectsRejected();
				totalNumberOfDefectsReopenedSoFar = next.getTotalNumberOfDefectsReopened();

				totalNumberOfDefectsFixedSoFar = next.getTotalNumberOfDefectsFixed();

				totalNumberOfValidDefectsLoggedInSITSoFar = next.getNumberOfValidDefectsLoggedInSIT();
				totalNumberOfValidDefectsLoggedInUATSoFar = next.getNumberOfValidDefectsLoggedInUAT();

				totalNumberOfTestCasesExecutedWithMigratedDataSoFar += next.getNumberOfTestCasesExecutedWithMigratedData();

			}
		}


		int totalNumberOfDefectsLoggedInSITAsPerMonthlyMetricsSoFar = 0;
		int totalNumberOfDefectsLoggedInUATAsPerMonthlyMetricsSoFar = 0;

		Iterable<MonthlyTestExecutionProgress> monthlyTestExecutionProgress = monthlyTestExecutionProgressRepository.findAll();
		Iterator<MonthlyTestExecutionProgress> monthlyTestExecutionProgressIterator = monthlyTestExecutionProgress.iterator();

		MonthlyTestExecutionProgress nextMonthlyTestExecutionProgress = null;

		while (monthlyTestExecutionProgressIterator.hasNext()) {
			nextMonthlyTestExecutionProgress = monthlyTestExecutionProgressIterator.next();

			if (nextMonthlyTestExecutionProgress.getProject().getId() == id) {

				totalNumberOfDefectsLoggedInSITAsPerMonthlyMetricsSoFar += nextMonthlyTestExecutionProgress.getTotalNumberOfDefectsLoggedInSIT();
				totalNumberOfDefectsLoggedInUATAsPerMonthlyMetricsSoFar += nextMonthlyTestExecutionProgress.getTotalNumberOfDefectsLoggedInUAT();
			}
		}

		Iterable<LatestModuleEstimate> allLatestModuleEstimates = latestModuleEstimateRepository.findAll();
		Iterator<LatestModuleEstimate> latestModuleEstimateIterator = allLatestModuleEstimates.iterator();

		int totalEstimatedManualTestCases = 0;
		int totalEstimatedRegressionAutomationTestCases = 0;
		int totalEstimatedTestCases = 0;
		int totalEstimatedAutomationTestCases = 0;


		while (latestModuleEstimateIterator.hasNext()) {
			LatestModuleEstimate latestModulesEstimate = latestModuleEstimateIterator.next();

			if (latestModulesEstimate.getProject().getId() == id) {
				try{
					totalEstimatedManualTestCases += Integer.parseInt(latestModulesEstimate.getEstimatedManualTestcases());
					totalEstimatedRegressionAutomationTestCases += Integer.parseInt(latestModulesEstimate.getEstimatedRegressionAutomationTestCases());
					totalEstimatedAutomationTestCases += Integer.parseInt(latestModulesEstimate.getEstimatedRegressionAutomationTestCases());

					totalEstimatedTestCases += Integer.parseInt(latestModulesEstimate.getEstimatedManualTestcases());
					totalEstimatedTestCases += Integer.parseInt(latestModulesEstimate.getEstimatedRegressionAutomationTestCases());

				}catch(Exception e){
				}
			}
		}

		int inProjectAutomationTestCaseEstimate = 0;
		int apiAutomationTestCaseEstimate = 0;
		int totalNumberOfTestCasesTaggedForMigrationExecution = 0;
		int totalNumberOfRequirementsEstimated = 0;

		Iterable<LatestProjectEstimate> allLatestProjectEstimates = latestProjectEstimateRepository.findAll();
		Iterator<LatestProjectEstimate> latestProjectEstimateIterator = allLatestProjectEstimates.iterator();

		while (latestProjectEstimateIterator.hasNext()) {
			LatestProjectEstimate latestProjectEstimate = latestProjectEstimateIterator.next();

			if (latestProjectEstimate.getProject().getId() == id) {
				try{
					inProjectAutomationTestCaseEstimate = Integer.parseInt(latestProjectEstimate.getInProjectAutomationTestCaseEstimation());
					apiAutomationTestCaseEstimate = Integer.parseInt(latestProjectEstimate.getAPIAutomationTestCaseEstimation());
					totalNumberOfTestCasesTaggedForMigrationExecution = Integer.parseInt(latestProjectEstimate.getTotalNumberOfTestCasesTaggedForMigrationExecution());

					totalEstimatedTestCases += Integer.parseInt(latestProjectEstimate.getInProjectAutomationTestCaseEstimation());
					totalEstimatedTestCases += Integer.parseInt(latestProjectEstimate.getAPIAutomationTestCaseEstimation());

					totalEstimatedAutomationTestCases += Integer.parseInt(latestProjectEstimate.getInProjectAutomationTestCaseEstimation());
					totalEstimatedAutomationTestCases += Integer.parseInt(latestProjectEstimate.getAPIAutomationTestCaseEstimation());

					totalNumberOfRequirementsEstimated = Integer.parseInt(latestProjectEstimate.getTotalNumberOfRequirementsPlanned());


				}catch(Exception e){
				}
			}
		}

		int totalManualTestCasesNotExecuted = 0;
		int totalRegressionAutomationTestCasesNotExecuted = 0;
		int totalAutomationTestCasesNotExecuted = 0;
		int totalTestCasesNotExecuted = 0;
		int totalAPIAutomationTestCasesNotExecuted = 0;
		int totalInProjectTestCasesNotExecuted = 0;

		totalManualTestCasesNotExecuted = totalNumberOfManualTestCasesDevelopedSoFar - totalNumberOfManualTestCasesExecutedSoFar;
		totalRegressionAutomationTestCasesNotExecuted = totalnumberOfRegressionTestCasesDevelopedSoFar - totalnumberOfRegressionTestCasesExecutedSoFar;
		totalAPIAutomationTestCasesNotExecuted = totalNumberOfAPIAutomationTestCasesDevelopedSoFar - totalNumberOfAPIAutomationTestScriptsExecutedSoFar;
		totalInProjectTestCasesNotExecuted =  totalNumberOfInProjectAutomationTestCasesDevelopedSoFar - totalNumberOfInProjectAutomationTestScriptsExecutedSoFar;
		totalTestCasesNotExecuted = totalManualTestCasesNotExecuted +
				totalRegressionAutomationTestCasesNotExecuted +
				totalAPIAutomationTestCasesNotExecuted +
				totalInProjectTestCasesNotExecuted;

		totalAutomationTestCasesNotExecuted = totalRegressionAutomationTestCasesNotExecuted +
				totalAPIAutomationTestCasesNotExecuted +
				totalInProjectTestCasesNotExecuted;


		String defectsAndExecutionDataString = "{\"Metrics Measurements\":";

		defectsAndExecutionDataString += "2222";
		defectsAndExecutionDataString += ",\"";

		defectsAndExecutionDataString += "totalNumberOfDefectsReportedByTestingTeamSoFar";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalNumberOfDefectsReportedByTestingTeamSoFar;
		defectsAndExecutionDataString += ",\"";

		defectsAndExecutionDataString += "totalNumberOfDefectsRejectedSoFar";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalNumberOfDefectsRejectedSoFar;
		defectsAndExecutionDataString += ",\"";

		defectsAndExecutionDataString += "totalNumberOfDefectsReopenedSoFar";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalNumberOfDefectsReopenedSoFar;
		defectsAndExecutionDataString += ",\"";

		defectsAndExecutionDataString += "totalNumberOfDefectsFixedSoFar";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalNumberOfDefectsFixedSoFar;
		defectsAndExecutionDataString += ",\"";

		defectsAndExecutionDataString += "totalNumberOfValidDefectsLoggedInSITSoFar";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalNumberOfValidDefectsLoggedInSITSoFar;
		defectsAndExecutionDataString += ",\"";

		defectsAndExecutionDataString += "totalNumberOfValidDefectsLoggedInUATSoFar";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalNumberOfValidDefectsLoggedInUATSoFar;
		defectsAndExecutionDataString += ",\"";

		defectsAndExecutionDataString += "totalNumberOfDefectsLoggedInSITAsPerMonthlyMetricsSoFar";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalNumberOfDefectsLoggedInSITAsPerMonthlyMetricsSoFar  ;
		defectsAndExecutionDataString += ",\"";


		defectsAndExecutionDataString += "totalNumberOfDefectsLoggedInUATAsPerMonthlyMetricsSoFar";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalNumberOfDefectsLoggedInUATAsPerMonthlyMetricsSoFar  ;
		defectsAndExecutionDataString += ",\"";

		defectsAndExecutionDataString += "totalEstimatedManualTestCases";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalEstimatedManualTestCases;
		defectsAndExecutionDataString += ",\"";

		defectsAndExecutionDataString += "totalEstimatedRegressionAutomationTestCases";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalEstimatedRegressionAutomationTestCases;
		defectsAndExecutionDataString += ",\"";


		defectsAndExecutionDataString += "totalEstimatedTestCases";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalEstimatedTestCases;
		defectsAndExecutionDataString += ",\"";

		defectsAndExecutionDataString += "totalEstimatedAutomationTestCases";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalEstimatedAutomationTestCases;
		defectsAndExecutionDataString += ",\"";

		defectsAndExecutionDataString += "totalNumberOfManualTestCasesDevelopedSoFar";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalNumberOfManualTestCasesDevelopedSoFar;
		defectsAndExecutionDataString += ",\"";


		defectsAndExecutionDataString += "totalNumberOfTestCasesDevelopedSoFar";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalNumberOfTestCasesDevelopedSoFar;
		defectsAndExecutionDataString += ",\"";

		defectsAndExecutionDataString += "totalNumberOfAutomationTestCasesDevelopedSoFar";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalNumberOfAutomationTestCasesDevelopedSoFar;
		defectsAndExecutionDataString += ",\"";


		defectsAndExecutionDataString += "totalNumberOfManualTestCasesExecutedSoFar";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalNumberOfManualTestCasesExecutedSoFar;
		defectsAndExecutionDataString += ",\"";


		defectsAndExecutionDataString += "totalnumberOfRegressionTestCasesExecutedSoFar";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalnumberOfRegressionTestCasesExecutedSoFar;
		defectsAndExecutionDataString += ",\"";


		defectsAndExecutionDataString += "totalNumberOfInProjectAutomationTestScriptsExecutedSoFar";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalNumberOfInProjectAutomationTestScriptsExecutedSoFar;
		defectsAndExecutionDataString += ",\"";


		defectsAndExecutionDataString += "totalNumberOfAPIAutomationTestScriptsExecutedSoFar";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalNumberOfAPIAutomationTestScriptsExecutedSoFar;
		defectsAndExecutionDataString += ",\"";


		defectsAndExecutionDataString += "totalNumberOfTestCasesExecutedSoFar";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalNumberOfTestCasesExecutedSoFar ;
		defectsAndExecutionDataString += ",\"";


		defectsAndExecutionDataString += "totalNumberOfAutomationTestCasesExecutedSoFar";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalNumberOfAutomationTestCasesExecutedSoFar  ;
		defectsAndExecutionDataString += ",\"";


		defectsAndExecutionDataString += "totalManualTestCasesNotExecuted";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalManualTestCasesNotExecuted;
		defectsAndExecutionDataString += ",\"";

		defectsAndExecutionDataString += "totalRegressionAutomationTestCasesNotExecuted";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalRegressionAutomationTestCasesNotExecuted;
		defectsAndExecutionDataString += ",\"";

		defectsAndExecutionDataString += "totalAPIAutomationTestCasesNotExecuted";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalAPIAutomationTestCasesNotExecuted;
		defectsAndExecutionDataString += ",\"";

		defectsAndExecutionDataString += "totalInProjectTestCasesNotExecuted";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalInProjectTestCasesNotExecuted;
		defectsAndExecutionDataString += ",\"";

		defectsAndExecutionDataString += "totalAutomationTestCasesNotExecuted";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalAutomationTestCasesNotExecuted;
		defectsAndExecutionDataString += ",\"";

/*
					if (totalEstimatedRegressionAutomationTestCases > 0) {

						Double totalEstimatedRegressionAutomationTestCasesDouble = new Double(totalEstimatedRegressionAutomationTestCases);
						Double temp = (((totalnumberOfRegressionTestCasesDevelopedSoFar * 100) / totalEstimatedRegressionAutomationTestCasesDouble));

						temp = (Math.round(temp * 100.0) / 100.0);

						defectsAndExecutionDataString += "Regression Automation coverage till date";
						defectsAndExecutionDataString += "\":";

						defectsAndExecutionDataString += temp.toString();
						defectsAndExecutionDataString += ",\"";

					}
*/

		defectsAndExecutionDataString += "totalnumberOfRegressionTestCasesEstimated";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalEstimatedRegressionAutomationTestCases;
		defectsAndExecutionDataString += ",\"";

		defectsAndExecutionDataString += "totalnumberOfRegressionTestCasesDevelopedSoFar";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalnumberOfRegressionTestCasesDevelopedSoFar;
		defectsAndExecutionDataString += ",\"";


/*
		if (apiAutomationTestCaseEstimate > 0) {

						Double apiAutomationTestCaseEstimateDouble = new Double(apiAutomationTestCaseEstimate);
						Double temp = (((totalNumberOfAPIAutomationTestCasesDevelopedSoFar * 100) / apiAutomationTestCaseEstimateDouble));

						temp = (Math.round(temp * 100.0) / 100.0);

						defectsAndExecutionDataString += "API Automation coverage till date";
						defectsAndExecutionDataString += "\":";

						defectsAndExecutionDataString += temp.toString();
						defectsAndExecutionDataString += ",\"";

					}
*/
		defectsAndExecutionDataString += "apiAutomationTestCaseEstimate";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += apiAutomationTestCaseEstimate;
		defectsAndExecutionDataString += ",\"";

		defectsAndExecutionDataString += "totalNumberOfAPIAutomationTestCasesDevelopedSoFar";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalNumberOfAPIAutomationTestCasesDevelopedSoFar;
		defectsAndExecutionDataString += ",\"";


/*
					if (inProjectAutomationTestCaseEstimate > 0) {

						Double inProjectAutomationTestCaseEstimateDouble = new Double(inProjectAutomationTestCaseEstimate);
						Double temp = (((totalNumberOfInProjectAutomationTestCasesDevelopedSoFar * 100) / inProjectAutomationTestCaseEstimateDouble));

						temp = (Math.round(temp * 100.0) / 100.0);

						defectsAndExecutionDataString += "InProject Automation coverage till date";
						defectsAndExecutionDataString += "\":";

						defectsAndExecutionDataString += temp.toString();
						defectsAndExecutionDataString += ",\"";

					}
*/

		defectsAndExecutionDataString += "inProjectAutomationTestCaseEstimate";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += inProjectAutomationTestCaseEstimate;
		defectsAndExecutionDataString += ",\"";

		defectsAndExecutionDataString += "totalNumberOfInProjectAutomationTestCasesDevelopedSoFar";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalNumberOfInProjectAutomationTestCasesDevelopedSoFar;
		defectsAndExecutionDataString += ",\"";

/*
		if (totalNumberOfRequirementsEstimated > 0) {

						Double totalNumberOfRequirementsEstimatedDouble = new Double(totalNumberOfRequirementsEstimated);
						Double temp = (((totalNumberOfRequirementsCoveredSoFar * 100) / totalNumberOfRequirementsEstimatedDouble));

						temp = (Math.round(temp * 100.0) / 100.0);

						defectsAndExecutionDataString += "Requirements coverage till date";
						defectsAndExecutionDataString += "\":";

						defectsAndExecutionDataString += temp.toString();
						defectsAndExecutionDataString += ",\"";

					}
*/

		defectsAndExecutionDataString += "totalNumberOfRequirementsEstimated";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalNumberOfRequirementsEstimated;
		defectsAndExecutionDataString += ",\"";

		defectsAndExecutionDataString += "totalNumberOfRequirementsCoveredSoFar";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalNumberOfRequirementsCoveredSoFar;
		defectsAndExecutionDataString += ",\"";

		defectsAndExecutionDataString += "totalNumberOfTestCasesTaggedForMigrationExecution";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalNumberOfTestCasesTaggedForMigrationExecution;
		defectsAndExecutionDataString += ",\"";

		defectsAndExecutionDataString += "totalNumberOfTestCasesExecutedWithMigratedDataSoFar";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalNumberOfTestCasesExecutedWithMigratedDataSoFar;
		defectsAndExecutionDataString += ",\"";

		defectsAndExecutionDataString += "totalTestCasesNotExecuted";
		defectsAndExecutionDataString += "\":";

		defectsAndExecutionDataString += totalTestCasesNotExecuted;
		defectsAndExecutionDataString += "}";

		System.out.println("The defects And Execution string is : " + defectsAndExecutionDataString);

		// Push the data to server using HTTP Post using a different index
		try {

			HttpHost host = new HttpHost("localhost", 9200);
			final RestClientBuilder builder = RestClient.builder(host);
			client = new RestHighLevelClient(builder);
			save(Integer.toString(2222), defectsAndExecutionDataString, CUMMULATIVE_INDEX); // Add New Index
			client.indices().refresh(new RefreshRequest(CUMMULATIVE_INDEX), RequestOptions.DEFAULT);
			client.close();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}


		try{

/***************************************
 buildDefectAndExecutionString(index++,   "totalNumberOfDefectsReportedByTestingTeamSoFar", totalNumberOfDefectsReportedByTestingTeamSoFar);
 buildDefectAndExecutionString(index++,   "totalNumberOfDefectsRejectedSoFar", totalNumberOfDefectsRejectedSoFar);
 buildDefectAndExecutionString(index++,   "totalNumberOfDefectsReopenedSoFar", totalNumberOfDefectsReopenedSoFar);
 buildDefectAndExecutionString(index++,   "totalNumberOfDefectsFixedSoFar", totalNumberOfDefectsFixedSoFar);
 ****************************************/


			String defectsAndExecutionDataString2 = "{\"Metrics Measurements\":";
			defectsAndExecutionDataString2 += 60000;
			defectsAndExecutionDataString2 += ",";
			defectsAndExecutionDataString2 += "\"type\":\"Defects Reopened: " + totalNumberOfDefectsReopenedSoFar +   "\",";
			defectsAndExecutionDataString2 += "\"value\":";
			defectsAndExecutionDataString2 += totalNumberOfDefectsReopenedSoFar;
			defectsAndExecutionDataString2 += "}";

			System.out.println(">>>>defectsAndExecutionDataString totalNumberOfDefectsReopenedSoFar>>>>>" + defectsAndExecutionDataString2);

			try {

				HttpHost host = new HttpHost("localhost", 9200);
				final RestClientBuilder builder = RestClient.builder(host);
				client = new RestHighLevelClient(builder);
				save(Integer.toString(60000), defectsAndExecutionDataString2, CUMMULATIVE_INDEX_9); // Add New Index
				client.indices().refresh(new RefreshRequest(CUMMULATIVE_INDEX_9), RequestOptions.DEFAULT);
				client.close();

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}


			defectsAndExecutionDataString2 = "{\"Metrics Measurements\":";
			defectsAndExecutionDataString2 += 60001;
			defectsAndExecutionDataString2 += ",";
			defectsAndExecutionDataString2 += "\"type\":\"Defects Rejected: " + totalNumberOfDefectsRejectedSoFar  + "\",";
			defectsAndExecutionDataString2 += "\"value\":";
			defectsAndExecutionDataString2 += totalNumberOfDefectsRejectedSoFar;
			defectsAndExecutionDataString2 += "}";

			System.out.println(">>>>defectsAndExecutionDataString totalNumberOfDefectsRejectedSoFar>>>>>" + defectsAndExecutionDataString2);

			try {

				HttpHost host = new HttpHost("localhost", 9200);
				final RestClientBuilder builder = RestClient.builder(host);
				client = new RestHighLevelClient(builder);
				save(Integer.toString(60001), defectsAndExecutionDataString2, CUMMULATIVE_INDEX_9); // Add New Index
				client.indices().refresh(new RefreshRequest(CUMMULATIVE_INDEX_9), RequestOptions.DEFAULT);
				client.close();

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}


			defectsAndExecutionDataString2 = "{\"Metrics Measurements\":";
			defectsAndExecutionDataString2 += 60002;
			defectsAndExecutionDataString2 += ",";
			defectsAndExecutionDataString2 += "\"type\":\"Defects Fixed: " + totalNumberOfDefectsFixedSoFar + "\",";
			defectsAndExecutionDataString2 += "\"value\":";
			defectsAndExecutionDataString2 += totalNumberOfDefectsFixedSoFar;
			defectsAndExecutionDataString2 += "}";

			System.out.println(">>>>defectsAndExecutionDataString totalNumberOfDefectsFixedSoFar>>>>>" + defectsAndExecutionDataString2);

			try {

				HttpHost host = new HttpHost("localhost", 9200);
				final RestClientBuilder builder = RestClient.builder(host);
				client = new RestHighLevelClient(builder);
				save(Integer.toString(60002), defectsAndExecutionDataString2, CUMMULATIVE_INDEX_9); // Add New Index
				client.indices().refresh(new RefreshRequest(CUMMULATIVE_INDEX_9), RequestOptions.DEFAULT);
				client.close();

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}



			String testExecutionDataString = "{\"Metrics Measurements\":";
			testExecutionDataString += 70000;
			testExecutionDataString += ",";
			testExecutionDataString += "\"type\":\"Executed: " + totalNumberOfTestCasesExecutedSoFar +   "\",";
			testExecutionDataString += "\"value\":";
			testExecutionDataString += totalNumberOfTestCasesExecutedSoFar;
			testExecutionDataString += "}";

			System.out.println(">>>>testExecutionDataString Executed>>>>>" + testExecutionDataString);

			try {

				HttpHost host = new HttpHost("localhost", 9200);
				final RestClientBuilder builder = RestClient.builder(host);
				client = new RestHighLevelClient(builder);
				save(Integer.toString(70000), testExecutionDataString, CUMMULATIVE_INDEX_TE); // Add New Index
				client.indices().refresh(new RefreshRequest(CUMMULATIVE_INDEX_TE), RequestOptions.DEFAULT);
				client.close();

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}


			testExecutionDataString = "{\"Metrics Measurements\":";
			testExecutionDataString += 70001;
			testExecutionDataString += ",";
			testExecutionDataString += "\"type\":\"Not Executed Manual: " + totalManualTestCasesNotExecuted  + "\",";
			testExecutionDataString += "\"value\":";
			testExecutionDataString += totalManualTestCasesNotExecuted;
			testExecutionDataString += "}";

			System.out.println(">>>>testExecutionDataString Not Executed Manual>>>>>" + testExecutionDataString);

			try {

				HttpHost host = new HttpHost("localhost", 9200);
				final RestClientBuilder builder = RestClient.builder(host);
				client = new RestHighLevelClient(builder);
				save(Integer.toString(70001), testExecutionDataString, CUMMULATIVE_INDEX_TE); // Add New Index
				client.indices().refresh(new RefreshRequest(CUMMULATIVE_INDEX_TE), RequestOptions.DEFAULT);
				client.close();

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}


			testExecutionDataString = "{\"Metrics Measurements\":";
			testExecutionDataString += 70002;
			testExecutionDataString += ",";
			testExecutionDataString += "\"type\":\"Not Executed Regression(A): " + totalRegressionAutomationTestCasesNotExecuted + "\",";
			testExecutionDataString += "\"value\":";
			testExecutionDataString += totalRegressionAutomationTestCasesNotExecuted;
			testExecutionDataString += "}";

			System.out.println(">>>>testExecutionDataString Not Executed Regression(A)>>>>>" + testExecutionDataString);

			try {

				HttpHost host = new HttpHost("localhost", 9200);
				final RestClientBuilder builder = RestClient.builder(host);
				client = new RestHighLevelClient(builder);
				save(Integer.toString(70002), testExecutionDataString, CUMMULATIVE_INDEX_TE); // Add New Index
				client.indices().refresh(new RefreshRequest(CUMMULATIVE_INDEX_TE), RequestOptions.DEFAULT);
				client.close();

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			testExecutionDataString = "{\"Metrics Measurements\":";
			testExecutionDataString += 70003;
			testExecutionDataString += ",";
			testExecutionDataString += "\"type\":\"Not Executed API: " + totalAPIAutomationTestCasesNotExecuted + "\",";
			testExecutionDataString += "\"value\":";
			testExecutionDataString += totalAPIAutomationTestCasesNotExecuted;
			testExecutionDataString += "}";

			System.out.println(">>>>testExecutionDataString Not Executed API:>>>>>" + testExecutionDataString);

			try {

				HttpHost host = new HttpHost("localhost", 9200);
				final RestClientBuilder builder = RestClient.builder(host);
				client = new RestHighLevelClient(builder);
				save(Integer.toString(70003), testExecutionDataString, CUMMULATIVE_INDEX_TE); // Add New Index
				client.indices().refresh(new RefreshRequest(CUMMULATIVE_INDEX_TE), RequestOptions.DEFAULT);
				client.close();

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}


			testExecutionDataString = "{\"Metrics Measurements\":";
			testExecutionDataString += 70004;
			testExecutionDataString += ",";
			testExecutionDataString += "\"type\":\"Not Executed In-Project: " + totalInProjectTestCasesNotExecuted + "\",";
			testExecutionDataString += "\"value\":";
			testExecutionDataString += totalInProjectTestCasesNotExecuted;
			testExecutionDataString += "}";

			System.out.println(">>>>testExecutionDataString Not Executed In-Project:>>>>>" + testExecutionDataString);

			try {

				HttpHost host = new HttpHost("localhost", 9200);
				final RestClientBuilder builder = RestClient.builder(host);
				client = new RestHighLevelClient(builder);
				save(Integer.toString(70004), testExecutionDataString, CUMMULATIVE_INDEX_TE); // Add New Index
				client.indices().refresh(new RefreshRequest(CUMMULATIVE_INDEX_TE), RequestOptions.DEFAULT);
				client.close();

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}


			String migrationExecutionString = "{\"Metrics Measurements\":";
			migrationExecutionString += 80000;
			migrationExecutionString += ",";
			int totalNumberOfTestCasesPendingMigration = totalNumberOfTestCasesTaggedForMigrationExecution - totalNumberOfTestCasesExecutedWithMigratedDataSoFar;
			migrationExecutionString += "\"type\":\"Not Executed: " +  totalNumberOfTestCasesPendingMigration +   "\",";
			migrationExecutionString += "\"value\":";
			migrationExecutionString += totalNumberOfTestCasesPendingMigration;
			migrationExecutionString += "}";

			System.out.println(">>>>migrationExecutionString totalNumberOfTestCasesPendingMIgration>>>>>" + migrationExecutionString);

			try {

				HttpHost host = new HttpHost("localhost", 9200);
				final RestClientBuilder builder = RestClient.builder(host);
				client = new RestHighLevelClient(builder);
				save(Integer.toString(80000), migrationExecutionString, CUMMULATIVE_INDEX_MigrationExecution); // Add New Index
				client.indices().refresh(new RefreshRequest(CUMMULATIVE_INDEX_MigrationExecution), RequestOptions.DEFAULT);
				client.close();

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}


			migrationExecutionString = "{\"Metrics Measurements\":";
			migrationExecutionString += 80001;
			migrationExecutionString += ",";
			migrationExecutionString += "\"type\":\"Executed: " + totalNumberOfTestCasesExecutedWithMigratedDataSoFar  + "\",";
			migrationExecutionString += "\"value\":";
			migrationExecutionString += totalNumberOfTestCasesExecutedWithMigratedDataSoFar;
			migrationExecutionString += "}";

			System.out.println(">>>>migrationExecutionString totalNumberOfTestCasesExecutedWithMigratedDataSoFar>>>>>" + migrationExecutionString);

			try {

				HttpHost host = new HttpHost("localhost", 9200);
				final RestClientBuilder builder = RestClient.builder(host);
				client = new RestHighLevelClient(builder);
				save(Integer.toString(80001), migrationExecutionString, CUMMULATIVE_INDEX_MigrationExecution); // Add New Index
				client.indices().refresh(new RefreshRequest(CUMMULATIVE_INDEX_MigrationExecution), RequestOptions.DEFAULT);
				client.close();

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}


			String requirementCoverageString = "{\"Metrics Measurements\":";
			requirementCoverageString += 50000;
			requirementCoverageString += ",";
			requirementCoverageString += "\"type\":\"Covered: " + totalNumberOfRequirementsCoveredSoFar + "\",";
			requirementCoverageString += "\"value\":";
			requirementCoverageString += totalNumberOfRequirementsCoveredSoFar;
			requirementCoverageString += "}";

			System.out.println(">>>>requirementCoverageString Not Executed Regression(A)>>>>>" + requirementCoverageString);

			try {

				HttpHost host = new HttpHost("localhost", 9200);
				final RestClientBuilder builder = RestClient.builder(host);
				client = new RestHighLevelClient(builder);
				save(Integer.toString(50000), requirementCoverageString, CUMMULATIVE_INDEX_RC); // Add New Index
				client.indices().refresh(new RefreshRequest(CUMMULATIVE_INDEX_RC), RequestOptions.DEFAULT);
				client.close();

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			int pendingRequirementsCount = totalNumberOfRequirementsEstimated - totalNumberOfRequirementsCoveredSoFar;

			requirementCoverageString = "{\"Metrics Measurements\":";
			requirementCoverageString += 50001;
			requirementCoverageString += ",";
			requirementCoverageString += "\"type\":\"Not Covered: " + pendingRequirementsCount + "\",";
			requirementCoverageString += "\"value\":";
			requirementCoverageString += pendingRequirementsCount;
			requirementCoverageString += "}";

			System.out.println(">>>>requirementCoverageString Not Executed API:>>>>>" + requirementCoverageString);

			try {

				HttpHost host = new HttpHost("localhost", 9200);
				final RestClientBuilder builder = RestClient.builder(host);
				client = new RestHighLevelClient(builder);
				save(Integer.toString(50001), requirementCoverageString, CUMMULATIVE_INDEX_RC); // Add New Index
				client.indices().refresh(new RefreshRequest(CUMMULATIVE_INDEX_RC), RequestOptions.DEFAULT);
				client.close();

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}



			String inProjectAutomationCoverageString = "{\"Metrics Measurements\":";
			inProjectAutomationCoverageString += 20000;
			inProjectAutomationCoverageString += ",";
			inProjectAutomationCoverageString += "\"type\":\"Covered: " + totalNumberOfInProjectAutomationTestCasesDevelopedSoFar + "\",";
			inProjectAutomationCoverageString += "\"value\":";
			inProjectAutomationCoverageString += totalNumberOfInProjectAutomationTestCasesDevelopedSoFar;
			inProjectAutomationCoverageString += "}";

			System.out.println(">>>>inProjectAutomationCoverageString Not Executed Regression(A)>>>>>" + inProjectAutomationCoverageString);

			try {

				HttpHost host = new HttpHost("localhost", 9200);
				final RestClientBuilder builder = RestClient.builder(host);
				client = new RestHighLevelClient(builder);
				save(Integer.toString(20000), inProjectAutomationCoverageString, CUMMULATIVE_INDEX_IPAC); // Add New Index
				client.indices().refresh(new RefreshRequest(CUMMULATIVE_INDEX_IPAC), RequestOptions.DEFAULT);
				client.close();

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			int pendingInProjectAutomationCount = inProjectAutomationTestCaseEstimate - totalNumberOfInProjectAutomationTestCasesDevelopedSoFar;

			inProjectAutomationCoverageString = "{\"Metrics Measurements\":";
			inProjectAutomationCoverageString += 20001;
			inProjectAutomationCoverageString += ",";
			inProjectAutomationCoverageString += "\"type\":\"Not Covered: " + pendingInProjectAutomationCount + "\",";
			inProjectAutomationCoverageString += "\"value\":";
			inProjectAutomationCoverageString += pendingInProjectAutomationCount;
			inProjectAutomationCoverageString += "}";

			System.out.println(">>>>inProjectAutomationCoverageString Not Executed API:>>>>>" + inProjectAutomationCoverageString);

			try {

				HttpHost host = new HttpHost("localhost", 9200);
				final RestClientBuilder builder = RestClient.builder(host);
				client = new RestHighLevelClient(builder);
				save(Integer.toString(20001), inProjectAutomationCoverageString, CUMMULATIVE_INDEX_IPAC); // Add New Index
				client.indices().refresh(new RefreshRequest(CUMMULATIVE_INDEX_IPAC), RequestOptions.DEFAULT);
				client.close();

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}


			String apiAutomationCoverageString = "{\"Metrics Measurements\":";
			apiAutomationCoverageString += 10000;
			apiAutomationCoverageString += ",";
			apiAutomationCoverageString += "\"type\":\"Covered: " + totalNumberOfAPIAutomationTestCasesDevelopedSoFar + "\",";
			apiAutomationCoverageString += "\"value\":";
			apiAutomationCoverageString += totalNumberOfAPIAutomationTestCasesDevelopedSoFar;
			apiAutomationCoverageString += "}";

			System.out.println(">>>>apiAutomationCoverageString Not Executed Regression(A)>>>>>" + apiAutomationCoverageString);

			try {

				HttpHost host = new HttpHost("localhost", 9200);
				final RestClientBuilder builder = RestClient.builder(host);
				client = new RestHighLevelClient(builder);
				save(Integer.toString(10000), apiAutomationCoverageString, CUMMULATIVE_INDEX_API); // Add New Index
				client.indices().refresh(new RefreshRequest(CUMMULATIVE_INDEX_API), RequestOptions.DEFAULT);
				client.close();

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			int pendingAPIAutomationCount = apiAutomationTestCaseEstimate - totalNumberOfAPIAutomationTestCasesDevelopedSoFar;

			apiAutomationCoverageString = "{\"Metrics Measurements\":";
			apiAutomationCoverageString += 10001;
			apiAutomationCoverageString += ",";
			apiAutomationCoverageString += "\"type\":\"Not Covered: " + pendingAPIAutomationCount + "\",";
			apiAutomationCoverageString += "\"value\":";
			apiAutomationCoverageString += pendingAPIAutomationCount;
			apiAutomationCoverageString += "}";

			System.out.println(">>>>inProjectAutomationCoverageString Not Executed API:>>>>>" + apiAutomationCoverageString);

			try {

				HttpHost host = new HttpHost("localhost", 9200);
				final RestClientBuilder builder = RestClient.builder(host);
				client = new RestHighLevelClient(builder);
				save(Integer.toString(10001), apiAutomationCoverageString, CUMMULATIVE_INDEX_API); // Add New Index
				client.indices().refresh(new RefreshRequest(CUMMULATIVE_INDEX_API), RequestOptions.DEFAULT);
				client.close();

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}


			String regAutomationCoverageString = "{\"Metrics Measurements\":";
			regAutomationCoverageString += 1000;
			regAutomationCoverageString += ",";
			regAutomationCoverageString += "\"type\":\"Covered: " + totalnumberOfRegressionTestCasesDevelopedSoFar + "\",";
			regAutomationCoverageString += "\"value\":";
			regAutomationCoverageString += totalnumberOfRegressionTestCasesDevelopedSoFar;
			regAutomationCoverageString += "}";

			System.out.println(">>>>regAutomationCoverageString Not Executed Regression(A)>>>>>" + regAutomationCoverageString);

			try {

				HttpHost host = new HttpHost("localhost", 9200);
				final RestClientBuilder builder = RestClient.builder(host);
				client = new RestHighLevelClient(builder);
				save(Integer.toString(1000), regAutomationCoverageString, CUMMULATIVE_INDEX_REG); // Add New Index
				client.indices().refresh(new RefreshRequest(CUMMULATIVE_INDEX_REG), RequestOptions.DEFAULT);
				client.close();

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			int pendingRegAutomationDesignCount = totalEstimatedRegressionAutomationTestCases - totalnumberOfRegressionTestCasesDevelopedSoFar;

			regAutomationCoverageString = "{\"Metrics Measurements\":";
			regAutomationCoverageString += 1001;
			regAutomationCoverageString += ",";
			regAutomationCoverageString += "\"type\":\"Not Covered: " + pendingRegAutomationDesignCount + "\",";
			regAutomationCoverageString += "\"value\":";
			regAutomationCoverageString += pendingRegAutomationDesignCount;
			regAutomationCoverageString += "}";

			System.out.println(">>>>regAutomationCoverageString Not Executed API:>>>>>" + regAutomationCoverageString);

			try {

				HttpHost host = new HttpHost("localhost", 9200);
				final RestClientBuilder builder = RestClient.builder(host);
				client = new RestHighLevelClient(builder);
				save(Integer.toString(1001), regAutomationCoverageString, CUMMULATIVE_INDEX_REG); // Add New Index
				client.indices().refresh(new RefreshRequest(CUMMULATIVE_INDEX_REG), RequestOptions.DEFAULT);
				client.close();

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}


/********************************************

 int index = 44444;
 buildDefectAndExecutionString(index++,   "totalNumberOfDefectsReportedByTestingTeamSoFar", totalNumberOfDefectsReportedByTestingTeamSoFar);
 buildDefectAndExecutionString(index++,   "totalNumberOfDefectsRejectedSoFar", totalNumberOfDefectsRejectedSoFar);
 buildDefectAndExecutionString(index++,   "totalNumberOfDefectsReopenedSoFar", totalNumberOfDefectsReopenedSoFar);
 buildDefectAndExecutionString(index++,   "totalNumberOfDefectsFixedSoFar", totalNumberOfDefectsFixedSoFar);
 buildDefectAndExecutionString(index++,   "totalNumberOfValidDefectsLoggedInSITSoFar", totalNumberOfValidDefectsLoggedInSITSoFar);
 buildDefectAndExecutionString(index++,   "totalNumberOfValidDefectsLoggedInUATSoFar", totalNumberOfValidDefectsLoggedInUATSoFar);
 buildDefectAndExecutionString(index++,   "totalNumberOfDefectsLoggedInSITAsPerMonthlyMetricsSoFar", totalNumberOfDefectsLoggedInSITAsPerMonthlyMetricsSoFar);
 buildDefectAndExecutionString(index++,   "totalNumberOfDefectsLoggedInUATAsPerMonthlyMetricsSoFar", totalNumberOfDefectsLoggedInUATAsPerMonthlyMetricsSoFar);
 buildDefectAndExecutionString(index++,   "totalEstimatedManualTestCases", totalEstimatedManualTestCases);
 buildDefectAndExecutionString(index++,   "totalEstimatedRegressionAutomationTestCases", totalEstimatedRegressionAutomationTestCases);
 buildDefectAndExecutionString(index++,   "totalEstimatedTestCases", totalEstimatedTestCases);

 **************************************************/
/*
			defectsAndExecutionDataString = "{\"Metrics Measurements\":";


			defectsAndExecutionDataString += "33333";
			defectsAndExecutionDataString += ",";

			defectsAndExecutionDataString += "\"type\":";
			defectsAndExecutionDataString += "\"totalNumberOfDefectsReportedByTestingTeamSoFar\",";
			defectsAndExecutionDataString += "\"value\":";
			defectsAndExecutionDataString += totalNumberOfDefectsReportedByTestingTeamSoFar;
			defectsAndExecutionDataString += "}";



			defectsAndExecutionDataString += "{\"type\":";
			defectsAndExecutionDataString += "\"totalNumberOfDefectsRejectedSoFar\",";
			defectsAndExecutionDataString += "\"value\":";

			defectsAndExecutionDataString += totalNumberOfDefectsRejectedSoFar;
			defectsAndExecutionDataString += "},";



			defectsAndExecutionDataString += "{\"type\":";
			defectsAndExecutionDataString += "\"totalNumberOfDefectsReopenedSoFar\",";
			defectsAndExecutionDataString += "\"value\":";

			defectsAndExecutionDataString += totalNumberOfDefectsReopenedSoFar;
			defectsAndExecutionDataString += "},";



			defectsAndExecutionDataString += "{\"type\":";
			defectsAndExecutionDataString += "\"totalNumberOfDefectsFixedSoFar\",";
			defectsAndExecutionDataString += "\"value\":";

			defectsAndExecutionDataString += totalNumberOfDefectsFixedSoFar;
			defectsAndExecutionDataString += "},";


			defectsAndExecutionDataString += "{\"type\":";
			defectsAndExecutionDataString += "\"totalNumberOfValidDefectsLoggedInSITSoFar\",";
			defectsAndExecutionDataString += "\"value\":";

			defectsAndExecutionDataString += totalNumberOfValidDefectsLoggedInSITSoFar;
			defectsAndExecutionDataString += "},";


			defectsAndExecutionDataString += "{\"type\":";
			defectsAndExecutionDataString += "\"totalNumberOfValidDefectsLoggedInUATSoFar\",";
			defectsAndExecutionDataString += "\"value\":";

			defectsAndExecutionDataString += totalNumberOfValidDefectsLoggedInUATSoFar;
			defectsAndExecutionDataString += "},";



			defectsAndExecutionDataString += "{\"type\":";
			defectsAndExecutionDataString += "\"totalNumberOfDefectsLoggedInSITAsPerMonthlyMetricsSoFar\",";
			defectsAndExecutionDataString += "\"value\":";

			defectsAndExecutionDataString += totalNumberOfDefectsLoggedInSITAsPerMonthlyMetricsSoFar  ;
			defectsAndExecutionDataString += "},";



			defectsAndExecutionDataString += "{\"type\":";
			defectsAndExecutionDataString += "\"totalNumberOfDefectsLoggedInUATAsPerMonthlyMetricsSoFar\",";
			defectsAndExecutionDataString += "\"value\":";

			defectsAndExecutionDataString += totalNumberOfDefectsLoggedInUATAsPerMonthlyMetricsSoFar  ;
			defectsAndExecutionDataString += "},";



			defectsAndExecutionDataString += "{\"type\":";
			defectsAndExecutionDataString += "\"totalEstimatedManualTestCases\",";
			defectsAndExecutionDataString += "\"value\":";

			defectsAndExecutionDataString += totalEstimatedManualTestCases;
			defectsAndExecutionDataString += "},";



			defectsAndExecutionDataString += "{\"type\":";
			defectsAndExecutionDataString += "\"totalEstimatedRegressionAutomationTestCases\",";
			defectsAndExecutionDataString += "\"value\":";

			defectsAndExecutionDataString += totalEstimatedRegressionAutomationTestCases;
			defectsAndExecutionDataString += "},";



			defectsAndExecutionDataString += "{\"type\":";
			defectsAndExecutionDataString += "\"totalEstimatedTestCases\",";
			defectsAndExecutionDataString += "\"value\":";

			defectsAndExecutionDataString += totalEstimatedTestCases;
			defectsAndExecutionDataString += "},";


			defectsAndExecutionDataString += "{\"type\":";
			defectsAndExecutionDataString += "\"totalEstimatedAutomationTestCases\",";
			defectsAndExecutionDataString += "\"value\":";

			defectsAndExecutionDataString += totalEstimatedAutomationTestCases;
			defectsAndExecutionDataString += "},";

			defectsAndExecutionDataString += "{\"type\":";
			defectsAndExecutionDataString += "\"totalNumberOfManualTestCasesDevelopedSoFar\",";
			defectsAndExecutionDataString += "\"value\":";

			defectsAndExecutionDataString += totalNumberOfManualTestCasesDevelopedSoFar;
			defectsAndExecutionDataString += "},";

			defectsAndExecutionDataString += "{\"type\":";
			defectsAndExecutionDataString += "\"totalNumberOfTestCasesDevelopedSoFar\",";
			defectsAndExecutionDataString += "\"value\":";

			defectsAndExecutionDataString += totalNumberOfTestCasesDevelopedSoFar;
			defectsAndExecutionDataString += "},";

			defectsAndExecutionDataString += "{\"type\":";
			defectsAndExecutionDataString += "\"totalNumberOfAutomationTestCasesDevelopedSoFar\",";
			defectsAndExecutionDataString += "\"value\":";

			defectsAndExecutionDataString += totalNumberOfAutomationTestCasesDevelopedSoFar;
			defectsAndExecutionDataString += "},";

			defectsAndExecutionDataString += "{\"type\":";
			defectsAndExecutionDataString += "\"totalNumberOfManualTestCasesExecutedSoFar\",";
			defectsAndExecutionDataString += "\"value\":";

			defectsAndExecutionDataString += totalNumberOfManualTestCasesExecutedSoFar;
			defectsAndExecutionDataString += "},";

			defectsAndExecutionDataString += "{\"type\":";
			defectsAndExecutionDataString += "\"totalnumberOfRegressionTestCasesExecutedSoFar\",";
			defectsAndExecutionDataString += "\"value\":";

			defectsAndExecutionDataString += totalnumberOfRegressionTestCasesExecutedSoFar;
			defectsAndExecutionDataString += "},";

			defectsAndExecutionDataString += "{\"type\":";
			defectsAndExecutionDataString += "\"totalNumberOfInProjectAutomationTestScriptsExecutedSoFar\",";
			defectsAndExecutionDataString += "\"value\":";

			defectsAndExecutionDataString += totalNumberOfInProjectAutomationTestScriptsExecutedSoFar;
			defectsAndExecutionDataString += "},";

			defectsAndExecutionDataString += "{\"type\":";
			defectsAndExecutionDataString += "\"totalNumberOfAPIAutomationTestScriptsExecutedSoFar\",";
			defectsAndExecutionDataString += "\"value\":";

			defectsAndExecutionDataString += totalNumberOfAPIAutomationTestScriptsExecutedSoFar;
			defectsAndExecutionDataString += "},";

			defectsAndExecutionDataString += "{\"type\":";
			defectsAndExecutionDataString += "\"totalNumberOfTestCasesExecutedSoFar\",";
			defectsAndExecutionDataString += "\"value\":";

			defectsAndExecutionDataString += totalNumberOfTestCasesExecutedSoFar ;
			defectsAndExecutionDataString += "},";

			defectsAndExecutionDataString += "{\"type\":";
			defectsAndExecutionDataString += "\"totalNumberOfAutomationTestCasesExecutedSoFar\",";
			defectsAndExecutionDataString += "\"value\":";

			defectsAndExecutionDataString += totalNumberOfAutomationTestCasesExecutedSoFar  ;
			defectsAndExecutionDataString += "},";

			defectsAndExecutionDataString += "{\"type\":";
			defectsAndExecutionDataString += "\"totalManualTestCasesNotExecuted\",";
			defectsAndExecutionDataString += "\"value\":";

			defectsAndExecutionDataString += totalManualTestCasesNotExecuted;
			defectsAndExecutionDataString += "},";

			defectsAndExecutionDataString += "{\"type\":";
			defectsAndExecutionDataString += "\"totalRegressionAutomationTestCasesNotExecuted\",";
			defectsAndExecutionDataString += "\"value\":";

			defectsAndExecutionDataString += totalRegressionAutomationTestCasesNotExecuted;
			defectsAndExecutionDataString += "},";

			defectsAndExecutionDataString += "{\"type\":";
			defectsAndExecutionDataString += "\"totalAPIAutomationTestCasesNotExecuted\",";
			defectsAndExecutionDataString += "\"value\":";

			defectsAndExecutionDataString += totalAPIAutomationTestCasesNotExecuted;
			defectsAndExecutionDataString += "},";

			defectsAndExecutionDataString += "{\"type\":";
			defectsAndExecutionDataString += "\"totalInProjectTestCasesNotExecuted\",";
			defectsAndExecutionDataString += "\"value\":";

			defectsAndExecutionDataString += totalInProjectTestCasesNotExecuted;
			defectsAndExecutionDataString += "},";

			defectsAndExecutionDataString += "{\"type\":";
			defectsAndExecutionDataString += "\"totalAutomationTestCasesNotExecuted\",";
			defectsAndExecutionDataString += "\"value\":";

			defectsAndExecutionDataString += totalAutomationTestCasesNotExecuted;
			defectsAndExecutionDataString += "},";

		/*
							if (totalEstimatedRegressionAutomationTestCases > 0) {

								Double totalEstimatedRegressionAutomationTestCasesDouble = new Double(totalEstimatedRegressionAutomationTestCases);
								Double temp = (((totalnumberOfRegressionTestCasesDevelopedSoFar * 100) / totalEstimatedRegressionAutomationTestCasesDouble));

								temp = (Math.round(temp * 100.0) / 100.0);

								defectsAndExecutionDataString += "Regression Automation coverage till date";
								defectsAndExecutionDataString += "\":";

								defectsAndExecutionDataString += temp.toString();
								defectsAndExecutionDataString += ",\"";

							}
		*/
			/*********************************************************

			 defectsAndExecutionDataString += "{\"type\":";
			 defectsAndExecutionDataString += "\"totalnumberOfRegressionTestCasesEstimated\",";
			 defectsAndExecutionDataString += "\"value\":";

			 defectsAndExecutionDataString += totalEstimatedRegressionAutomationTestCases;
			 defectsAndExecutionDataString += "},";

			 defectsAndExecutionDataString += "{\"type\":";
			 defectsAndExecutionDataString += "\"totalnumberOfRegressionTestCasesDevelopedSoFar\",";
			 defectsAndExecutionDataString += "\"value\":";

			 defectsAndExecutionDataString += totalnumberOfRegressionTestCasesDevelopedSoFar;
			 defectsAndExecutionDataString += "},";
			 ******************************************************************/

		/*
				if (apiAutomationTestCaseEstimate > 0) {

								Double apiAutomationTestCaseEstimateDouble = new Double(apiAutomationTestCaseEstimate);
								Double temp = (((totalNumberOfAPIAutomationTestCasesDevelopedSoFar * 100) / apiAutomationTestCaseEstimateDouble));

								temp = (Math.round(temp * 100.0) / 100.0);

								defectsAndExecutionDataString += "API Automation coverage till date";
								defectsAndExecutionDataString += "\":";

								defectsAndExecutionDataString += temp.toString();
								defectsAndExecutionDataString += ",\"";

							}
		*/
			/*****************************************************************
			 defectsAndExecutionDataString += "{\"type\":";
			 defectsAndExecutionDataString += "\"apiAutomationTestCaseEstimate\",";
			 defectsAndExecutionDataString += "\"value\":";

			 defectsAndExecutionDataString += apiAutomationTestCaseEstimate;
			 defectsAndExecutionDataString += "},";

			 defectsAndExecutionDataString += "{\"type\":";
			 defectsAndExecutionDataString += "\"totalNumberOfAPIAutomationTestCasesDevelopedSoFar\",";
			 defectsAndExecutionDataString += "\"value\":";

			 defectsAndExecutionDataString += totalNumberOfAPIAutomationTestCasesDevelopedSoFar;
			 defectsAndExecutionDataString += "},";
			 *******************************************************************/


		/*
							if (inProjectAutomationTestCaseEstimate > 0) {

								Double inProjectAutomationTestCaseEstimateDouble = new Double(inProjectAutomationTestCaseEstimate);
								Double temp = (((totalNumberOfInProjectAutomationTestCasesDevelopedSoFar * 100) / inProjectAutomationTestCaseEstimateDouble));

								temp = (Math.round(temp * 100.0) / 100.0);

								defectsAndExecutionDataString += "InProject Automation coverage till date";
								defectsAndExecutionDataString += "\":";

								defectsAndExecutionDataString += temp.toString();
								defectsAndExecutionDataString += ",\"";

							}
		*/

			/****************************************************
			 defectsAndExecutionDataString += "{\"type\":";
			 defectsAndExecutionDataString += "\"inProjectAutomationTestCaseEstimate\",";
			 defectsAndExecutionDataString += "\"value\":";

			 defectsAndExecutionDataString += inProjectAutomationTestCaseEstimate;
			 defectsAndExecutionDataString += "},";

			 defectsAndExecutionDataString += "{\"type\":";
			 defectsAndExecutionDataString += "\"totalNumberOfInProjectAutomationTestCasesDevelopedSoFar\",";
			 defectsAndExecutionDataString += "\"value\":";

			 defectsAndExecutionDataString += totalNumberOfInProjectAutomationTestCasesDevelopedSoFar;
			 defectsAndExecutionDataString += "},";
			 **********************************************/

		/*
				if (totalNumberOfRequirementsEstimated > 0) {

								Double totalNumberOfRequirementsEstimatedDouble = new Double(totalNumberOfRequirementsEstimated);
								Double temp = (((totalNumberOfRequirementsCoveredSoFar * 100) / totalNumberOfRequirementsEstimatedDouble));

								temp = (Math.round(temp * 100.0) / 100.0);

								defectsAndExecutionDataString += "Requirements coverage till date";
								defectsAndExecutionDataString += "\":";

								defectsAndExecutionDataString += temp.toString();
								defectsAndExecutionDataString += ",\"";

							}
		*/

			/*************************************************************************
			 defectsAndExecutionDataString += "{\"type\":";
			 defectsAndExecutionDataString += "\"totalNumberOfRequirementsEstimated\",";
			 defectsAndExecutionDataString += "\"value\":";

			 defectsAndExecutionDataString += totalNumberOfRequirementsEstimated;
			 defectsAndExecutionDataString += "},";

			 defectsAndExecutionDataString += "{\"type\":";
			 defectsAndExecutionDataString += "\"totalNumberOfRequirementsCoveredSoFar\",";
			 defectsAndExecutionDataString += "\"value\":";

			 defectsAndExecutionDataString += totalNumberOfRequirementsCoveredSoFar;
			 defectsAndExecutionDataString += "},";

			 defectsAndExecutionDataString += "{\"type\":";
			 defectsAndExecutionDataString += "\"totalNumberOfTestCasesTaggedForMigrationExecution\",";
			 defectsAndExecutionDataString += "\"value\":";

			 defectsAndExecutionDataString += totalNumberOfTestCasesTaggedForMigrationExecution;
			 defectsAndExecutionDataString += "},";

			 defectsAndExecutionDataString += "{\"type\":";
			 defectsAndExecutionDataString += "\"totalNumberOfTestCasesExecutedWithMigratedDataSoFar\",";
			 defectsAndExecutionDataString += "\"value\":";

			 defectsAndExecutionDataString += totalNumberOfTestCasesExecutedWithMigratedDataSoFar;
			 defectsAndExecutionDataString += "},";

			 defectsAndExecutionDataString += "{\"type\":";
			 defectsAndExecutionDataString += "\"totalTestCasesNotExecuted\",";
			 defectsAndExecutionDataString += "\"value\":";

			 defectsAndExecutionDataString += totalTestCasesNotExecuted;
			 defectsAndExecutionDataString += "}]}";

			 System.out.println("The new cummulative defects And Execution string is : " + defectsAndExecutionDataString);

			 // Push the data to server using HTTP Post using a different index
			 try {

			 HttpHost host = new HttpHost("localhost", 9200);
			 final RestClientBuilder builder = RestClient.builder(host);
			 client = new RestHighLevelClient(builder);
			 save(Integer.toString(3333), defectsAndExecutionDataString, CUMMULATIVE_INDEX_2); // Add New Index
			 client.indices().refresh(new RefreshRequest(CUMMULATIVE_INDEX_2), RequestOptions.DEFAULT);
			 client.close();

			 } catch (Exception e) {
			 System.out.println(e.getMessage());
			 }
			 **************************************************************************/


		}catch(Exception e){

		}


		try {
			Iterable<WeeklyProjectGovernance> weeklyProjectGovernanceIterable = weeklyProjectGovernanceRepository.findAll();
			Iterator<WeeklyProjectGovernance> weeklyProjectGovernanceIterator = weeklyProjectGovernanceIterable.iterator();
			String weeklyGovernancetotalString = null;

			int cummulativeNumberOfFormalGovernanceMeetings = 0;
			int cummulativeRCAPoorRequirement = 0;
			int cummulativeRCALateRequirement = 0;
			int cummulativeRCADesignError = 0;
			int cummulativeRCACodingError = 0;
			int cummulativeRCAInsufficientTesting = 0;
			int cummulativeRCAProcessRelated = 0;
			int cummulativeRCATestData = 0;
			int cummulativeRCATestEnvironmentConfiguration = 0;
			int cummulativeRCAOthers = 0;

			WeeklyProjectGovernance weeklyProjectGovernance = null;

			while (weeklyProjectGovernanceIterator.hasNext()) {
				weeklyProjectGovernance = weeklyProjectGovernanceIterator.next();

				if ((weeklyProjectGovernance.getProject().getId() == id)) {
					cummulativeNumberOfFormalGovernanceMeetings += weeklyProjectGovernance.getNumberOfFormalGovernanceMeetings();
					cummulativeRCAPoorRequirement += weeklyProjectGovernance.getRCAPoorRequirement();
					cummulativeRCALateRequirement += weeklyProjectGovernance.getRCALateRequirement();
					cummulativeRCADesignError += weeklyProjectGovernance.getRCADesignError();
					cummulativeRCACodingError += weeklyProjectGovernance.getRCACodingError();
					cummulativeRCAInsufficientTesting += weeklyProjectGovernance.getRCAInsufficientTesting();
					cummulativeRCAProcessRelated += weeklyProjectGovernance.getRCAProcessRelated();
					cummulativeRCATestData += weeklyProjectGovernance.getRCATestData();
					cummulativeRCATestEnvironmentConfiguration += weeklyProjectGovernance.getRCATestEnvironmentConfiguration();
					cummulativeRCAOthers += weeklyProjectGovernance.getRCAOthers();
				}
			}

			weeklyProjectGovernance = new WeeklyProjectGovernance();

			weeklyGovernancetotalString = "{\"Metrics Measurements\":";

			weeklyGovernancetotalString += "1111";
			weeklyGovernancetotalString += ",\"";

			weeklyGovernancetotalString += weeklyProjectGovernance.getNumberOfFormalGovernanceMeetingsName();
			weeklyGovernancetotalString += "\":";

			weeklyGovernancetotalString += cummulativeNumberOfFormalGovernanceMeetings;
			weeklyGovernancetotalString += ",\"";

			weeklyGovernancetotalString += weeklyProjectGovernance.getRCAPoorRequirementName();
			weeklyGovernancetotalString += "\":";

			weeklyGovernancetotalString += cummulativeRCAPoorRequirement;
			weeklyGovernancetotalString += ",\"";

			weeklyGovernancetotalString += weeklyProjectGovernance.getRCALateRequirementName();
			weeklyGovernancetotalString += "\":";

			weeklyGovernancetotalString += cummulativeRCALateRequirement;
			weeklyGovernancetotalString += ",\"";

			weeklyGovernancetotalString += weeklyProjectGovernance.getRCADesignErrorName();
			weeklyGovernancetotalString += "\":";

			weeklyGovernancetotalString += cummulativeRCADesignError;
			weeklyGovernancetotalString += ",\"";

			weeklyGovernancetotalString += weeklyProjectGovernance.getRCACodingErrorName();
			weeklyGovernancetotalString += "\":";

			weeklyGovernancetotalString += cummulativeRCACodingError;
			weeklyGovernancetotalString += ",\"";

			weeklyGovernancetotalString += weeklyProjectGovernance.getRCAInsufficientTestingName();
			weeklyGovernancetotalString += "\":";

			weeklyGovernancetotalString += cummulativeRCAInsufficientTesting;
			weeklyGovernancetotalString += ",\"";

			weeklyGovernancetotalString += weeklyProjectGovernance.getRCAProcessRelatedName();
			weeklyGovernancetotalString += "\":";

			weeklyGovernancetotalString += cummulativeRCAProcessRelated;
			weeklyGovernancetotalString += ",\"";

			weeklyGovernancetotalString += weeklyProjectGovernance.getRCATestDataName();
			weeklyGovernancetotalString += "\":";

			weeklyGovernancetotalString += cummulativeRCATestData;
			weeklyGovernancetotalString += ",\"";

			weeklyGovernancetotalString += weeklyProjectGovernance.getRCATestEnvironmentConfigurationName();
			weeklyGovernancetotalString += "\":";

			weeklyGovernancetotalString += cummulativeRCATestEnvironmentConfiguration;
			weeklyGovernancetotalString += ",\"";

			weeklyGovernancetotalString += weeklyProjectGovernance.getRCAOthersName();
			weeklyGovernancetotalString += "\":";

			weeklyGovernancetotalString += cummulativeRCAOthers;
			weeklyGovernancetotalString += "}";

			System.out.println("The weekly governance metrics string is : " + weeklyGovernancetotalString);

			try {

				HttpHost host = new HttpHost("localhost", 9200);
				final RestClientBuilder builder = RestClient.builder(host);
				client = new RestHighLevelClient(builder);
				save(Integer.toString(1111), weeklyGovernancetotalString, RCA_INDEX); // Add New Index
				client.indices().refresh(new RefreshRequest(RCA_INDEX), RequestOptions.DEFAULT);
				client.close();

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}catch(Exception e){
			System.out.println("Error occurred during weekly governance metrics calculation :" + e.getMessage());
		}


		Iterable<QuarterlyProgress> quarterlyProgress = quarterlyProgressRepository.findAll();
		Iterator<QuarterlyProgress> quarterlyProgressIterator = quarterlyProgress.iterator();
		ArrayList<Integer> quarterNumbers = new ArrayList<Integer>();

		while (quarterlyProgressIterator.hasNext()) {
			QuarterlyProgress next = quarterlyProgressIterator.next();

			if (next.getProject().getId() == id) {
				quarterNumbers.add(next.getQuarterNumber());
			}

		}


		for (int index = 0; index < quarterNumbers.size(); index++) {
			Iterable<QuarterlyMetrics> quarterlyMetricsIterable = quarterMetricsRepository.findAll();
			Iterator<QuarterlyMetrics> quarterlyMetricsIterator = quarterlyMetricsIterable.iterator();

			boolean metricMatchFound = false;

			QuarterlyMetrics quarterlyMetrics = null;

			while (quarterlyMetricsIterator.hasNext()) {
				quarterlyMetrics = quarterlyMetricsIterator.next();

				if ((quarterlyMetrics.getProject().getId() == id) && quarterlyMetrics.getQuarterNumber() == quarterNumbers.get(index)) {
					metricMatchFound = true;
					break;
				}
			}


			if (metricMatchFound == false) {
				quarterlyMetrics = new QuarterlyMetrics();
				quarterlyMetrics.setQuarterNumber(quarterNumbers.get(index));
				Iterable<Project> allProjects = projectRepository.findAll();
				Iterator<Project> iterator = allProjects.iterator();

				while (iterator.hasNext()) {
					Project nextProject = iterator.next();

					if (nextProject.getId() == id) {
						quarterlyMetrics.setProject(nextProject);
						break;
					}
				}
			}

			quarterlyProgress = quarterlyProgressRepository.findAll();
			quarterlyProgressIterator = quarterlyProgress.iterator();


			while (quarterlyProgressIterator.hasNext()) {
				QuarterlyProgress next = quarterlyProgressIterator.next();

				if ((next.getProject().getId() == id) && (next.getQuarterNumber() == quarterNumbers.get(index))) {

					Integer revenueLeakage = Integer.parseInt(next.getProjectedBillingForCurrentQuarter()) - Integer.parseInt(next.getActualBillingForCurrentQuarter());

					quarterlyMetrics.setRevenueLeakage(revenueLeakage.toString());
					String ragStatus = checkRAGStatus(new Double(Integer.toString(revenueLeakage)),id,quarterlyMetrics.getRevenueLeakageName());
					System.out.println("The revenue leakage rag status is : " + ragStatus);
					quarterlyMetrics.setRevenueLeakageStatus(ragStatus);
					quarterlyMetrics.setNumberOfBusinessMeetings(next.getTotalQuarterlyBusinessReviewMeetings());
					quarterlyMetrics.setYearNumber(next.getYearNumber());
					quarterlyMetrics.setQuarterNumber(next.getQuarterNumber());

					break;

				}
			}
			quarterMetricsRepository.save(quarterlyMetrics);
		}


		Iterable<QuarterlyMetrics> quarterlyMetricsIterable = quarterMetricsRepository.findAll();
		Iterator<QuarterlyMetrics> quarterlyMetricsIterator = quarterlyMetricsIterable.iterator();
		QuarterlyMetrics quarterlyMetrics = null;

		while (quarterlyMetricsIterator.hasNext()) {
			quarterlyMetrics = quarterlyMetricsIterator.next();

			try{
				if ((quarterlyMetrics.getProject().getId() == id)) {

					String totalString = "{\"Metrics Measurements\":";

					totalString += quarterlyMetrics.getQuarterNumber();
					totalString += ",\"";

					totalString += quarterlyMetrics.getYearNumberName();
					totalString += "\":";

					totalString += quarterlyMetrics.getYearNumber();
					totalString += ",\"";

					totalString += quarterlyMetrics.getNumberOfBusinessMeetingsName();
					totalString += "\":";

					totalString += quarterlyMetrics.getNumberOfBusinessMeetings();
					totalString += ",\"";

					totalString = addMetricsLCLUCLGoal(id, totalString);

					totalString += quarterlyMetrics.getRevenueLeakageName();
					totalString += "\":";

					totalString += quarterlyMetrics.getRevenueLeakage();
					totalString += "}";

					System.out.println("The Quarterly  metrics string is : " + totalString);

					try {

						HttpHost host = new HttpHost("localhost", 9200);
						final RestClientBuilder builder = RestClient.builder(host);
						client = new RestHighLevelClient(builder);
						Integer quarterNumberString = quarterlyMetrics.getQuarterNumber();
						save(quarterNumberString.toString(), totalString, QUARTERLY_INDEX);
						client.indices().refresh(new RefreshRequest(QUARTERLY_INDEX), RequestOptions.DEFAULT);
						client.close();

					} catch (Exception e) {
						System.out.println(e.getMessage());
					}

				}
			}catch(Exception e){
				System.out.println("Error occurred during quarterly metric calculation : " + e.getMessage());
			}
		}


	}

	@RequestMapping(value = "/projects/{id}/GenerateMetrics", method = RequestMethod.GET)
	@ResponseBody
	public String generateMetrics(@PathVariable("id") long id) {

		//id = 993;
		WEEKLY_INDEX = "weekly_metric_" + id;
		MONTHLY_INDEX = "monthly_metric_" + id;
		QUARTERLY_INDEX = "quarterly_metric_" + id;
		CUMMULATIVE_INDEX = "cummulative_metric_" + id;
		CUMMULATIVE_INDEX_2 = "cummulative4_metric_" + id;
		CUMMULATIVE_INDEX_9= "cummulative_metric_899" + id;
		CUMMULATIVE_INDEX_TE= "cummulative_metric_tene" + id;
		CUMMULATIVE_INDEX_RC = "cummulative_metric_rc" + id;
		CUMMULATIVE_INDEX_MigrationExecution = "cummulative_metric_migration_execution" + id;
		CUMMULATIVE_INDEX_IPAC = "cummulative_metric_ipac_" + id;
		CUMMULATIVE_INDEX_API = "cummulative_metric_api_" + id;
		CUMMULATIVE_INDEX_REG = "cummulative_metric_reg_" + id;

		MODULE_INDEX = "module_metric_" + id;
		RCA_INDEX = "rca_metric_" + id;

		generateMetricsStandAlone(id);
		return "OK";
	}


	public String generateMonthlyMetrics(long id) {

		try{

			Iterable<MonthlyTestDesignProgress> monthlyTestDesignProgressIterable = monthlyTestDesignProgressRepository.findAll();
			Iterator<MonthlyTestDesignProgress> monthlyTestDesignProgressIterator = monthlyTestDesignProgressIterable.iterator();

			Iterable<MonthlyTestExecutionProgress> monthlyTestExecutionProgress = monthlyTestExecutionProgressRepository.findAll();
			Iterator<MonthlyTestExecutionProgress> monthlyTestExecutionProgressIterator = monthlyTestExecutionProgress.iterator();

			ArrayList<String> monthNumbers = new ArrayList<String>();
			ArrayList<String> monthNumbersTestDesignProgress = new ArrayList<String>();
			ArrayList<String> monthNumbersTestExecutionProgress = new ArrayList<String>();


			monthlyTestDesignProgressIterable = monthlyTestDesignProgressRepository.findAll();
			monthlyTestDesignProgressIterator = monthlyTestDesignProgressIterable.iterator();

			while (monthlyTestDesignProgressIterator.hasNext()) {
				MonthlyTestDesignProgress next = monthlyTestDesignProgressIterator.next();

				if (next.getProject().getId() == id) {

					int tempWeekNumber = next.getMonthNumber();
					if(tempWeekNumber < 10)
						monthNumbersTestDesignProgress.add(next.getYearNumber() + "-" + "0" + next.getMonthNumber());
					else
						monthNumbersTestDesignProgress.add(next.getYearNumber() + "-" + next.getMonthNumber());
				}

			}

			monthlyTestExecutionProgress = monthlyTestExecutionProgressRepository.findAll();
			monthlyTestExecutionProgressIterator = monthlyTestExecutionProgress.iterator();

			while (monthlyTestExecutionProgressIterator.hasNext()) {
				MonthlyTestExecutionProgress next = monthlyTestExecutionProgressIterator.next();

				if (next.getProject().getId() == id) {

					int tempWeekNumber = next.getMonthNumber();
					if(tempWeekNumber < 10)
						monthNumbersTestExecutionProgress.add(next.getYearNumber() + "-" + "0" + next.getMonthNumber());
					else
						monthNumbersTestExecutionProgress.add(next.getYearNumber() + "-" + next.getMonthNumber());

				}
			}


			if(monthNumbersTestDesignProgress.size() > monthNumbersTestExecutionProgress.size())
			{
				for(int i = 0; i < monthNumbersTestDesignProgress.size(); i++)
				{
					for(int j = 0; j < monthNumbersTestExecutionProgress.size(); j++ )
					{
						if(monthNumbersTestDesignProgress.get(i).compareTo(monthNumbersTestExecutionProgress.get(j)) == 0)
						{
							monthNumbers.add(monthNumbersTestDesignProgress.get(i));
						}
					}
				}
			}
			else
			{
				for(int i = 0; i < monthNumbersTestExecutionProgress.size(); i++)
				{
					for(int j = 0; j < monthNumbersTestDesignProgress.size(); j++ )
					{
						if(monthNumbersTestExecutionProgress.get(i).compareTo(monthNumbersTestDesignProgress.get(j))  == 0)
						{
							monthNumbers.add(monthNumbersTestExecutionProgress.get(i));
						}
					}
				}
			}

			Collections.sort(monthNumbers);

			System.out.println("The total number of monthly metrics available is : " + monthNumbers.size());

			for(int index = 0; index < monthNumbers.size(); index++){
				System.out.println("The current month being processed is : " + index);
				System.out.println("This loop should repeat for only once each month");

				Iterable<MonthlyMetrics> monthlyMetricsIterable = monthlyMetricsRepository.findAll();
				Iterator<MonthlyMetrics> monthlyMetricsIterableIterator = monthlyMetricsIterable.iterator();

				boolean metricMatchFound = false;
				MonthlyMetrics monthlyMetric = null;
				int monthNumberInt = 0;
				int yearNumberInt = 0;
				String ragStatus ="";

				StringTokenizer tokenizer = new StringTokenizer(monthNumbers.get(index), "-");

				System.out.println("The current year and month is : " + monthNumbers.get(index));
				String yearNumber = tokenizer.nextToken();
				String monthNumber = tokenizer.nextToken();

				yearNumberInt = Integer.parseInt(yearNumber);
				monthNumberInt = Integer.parseInt(monthNumber);

				System.out.println("The current year is : " + yearNumberInt);
				System.out.println("The current month is : " + monthNumberInt);

				System.out.println("Moving the monthly metric iterator to current month and year: " + monthNumberInt);

				while (monthlyMetricsIterableIterator.hasNext()) {
					monthlyMetric = monthlyMetricsIterableIterator.next();

					if (monthlyMetric.getProject().getId() == id) {

						if (((monthNumberInt - monthlyMetric.getMonthNumber()) == 0) && (yearNumberInt == monthlyMetric.getYearNumber())) {
							metricMatchFound = true;
							break;
						}
					}
				}


				if (metricMatchFound == false) {

					System.out.println("The monthly metric for current month and year was not found ");
					System.out.println("Hence creating the monthly metric for current month and year");

					monthlyMetric = new MonthlyMetrics();
					monthlyMetric.setMonthNumber(monthNumberInt);
					monthlyMetric.setYearNumber(yearNumberInt);

					Iterable<Project> allProjects = projectRepository.findAll();
					Iterator<Project> iterator = allProjects.iterator();

					while (iterator.hasNext()) {
						Project nextProject = iterator.next();

						if (nextProject.getId() == id) {
							monthlyMetric.setProject(nextProject);
							monthlyMetricsRepository.save(monthlyMetric);
							break;
						}
					}
				}


				System.out.println("Moving the monthly test execution progress iterator to point to desired month");
				monthlyTestExecutionProgress = monthlyTestExecutionProgressRepository.findAll();
				monthlyTestExecutionProgressIterator = monthlyTestExecutionProgress.iterator();
				MonthlyTestExecutionProgress nextMonthlyTestExecutionProgress = null;

				while (monthlyTestExecutionProgressIterator.hasNext()) {
					nextMonthlyTestExecutionProgress = monthlyTestExecutionProgressIterator.next();

					if ((nextMonthlyTestExecutionProgress.getProject().getId() == id) &&
							(nextMonthlyTestExecutionProgress.getMonthNumber() == monthNumberInt) &&
							(nextMonthlyTestExecutionProgress.getYearNumber() == yearNumberInt)) {

						System.out.println("Matching monthly test execution progress for desired month and year found");
						break;
					}

				}

				System.out.println("Moving the monthly governance progress iterator to point to desired month");
				Iterable<MonthlyGovernance>	monthlyGovernanceIterable = monthlyGovernanceRepository.findAll();
				Iterator<MonthlyGovernance>	monthlyGovernanceIterator = monthlyGovernanceIterable.iterator();
				MonthlyGovernance monthlyGovernanceNext = null;

				while (monthlyGovernanceIterator.hasNext()) {

					monthlyGovernanceNext = monthlyGovernanceIterator.next();

					if ((monthlyGovernanceNext.getProject().getId() == id) &&
							(monthlyGovernanceNext.getMonthNumber() == monthNumberInt) &&
							(monthlyGovernanceNext.getYearNumber() == yearNumberInt)) {
						System.out.println("Matching monthly test governance progress for desired month and year found");
						break;
					}

				}

				int approvedTestCasesCount = 0;
				approvedTestCasesCount = monthlyGovernanceNext.getNoOfTestCasesForCostApprovedCRs();

				System.out.println("Calculating cost variance for the month........  ");
				try {

					System.out.println("All the necessary inputs are provided by the user for the month");
					System.out.println("The numerator is difference of actual cost and the budgetted cost for the month");
					System.out.println("The denominator is budgetted cost for the month");

					if (monthlyGovernanceNext.getBudgetCostForTheMonth() > 0) {

						Double budgetCostDouble = new Double(monthlyGovernanceNext.getBudgetCostForTheMonth());
						Double actualCostDouble = new Double(monthlyGovernanceNext.getActualCostForTheMonth());
						Double temp = 0.0;
						try {
							temp = ((actualCostDouble - budgetCostDouble) / budgetCostDouble) * 100;
						} catch (Exception e) {
						}

						if (temp == 0) {
							monthlyMetric.setCostVariance("0.0");
							monthlyMetric.setCostVarianceStatus("0.0");
						} else {
							System.out.println("The cost variance is : " + Math.round(temp * 100.0) / 100.0);
							temp = (Math.round(temp * 100.0)) / 100.0;
							monthlyMetric.setCostVariance(temp.toString());
							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getCostVarianceName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setCostVarianceStatus(ragStatus);
						}

					}

					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){
					System.out.println("Exception occurred during cost variance calculation for the month : " + monthNumberInt + " " + yearNumberInt);
					System.out.println(e.getMessage());
				}


				int cummulativeTotalNumberOfInProjectAutomationTestCasesAutomatedSoFar = 0;
				int cummulativeTotalNumberOfAPIAutomationTestCasesAutomatedSoFar = 0;

				int cummulativeNumberOfManualTestCasesExecutedSofar = 0;
				int cummulativeNumberOfRegressionAutomationTestCasesExecutedSofar = 0;
				int cummulativeNumberOfInProjectAutomationTestScriptsExecutedSoFar = 0;
				int cummulativeNumberOfAPIAutomationTestScriptsExecutedSoFar = 0;

				// Defects
				int cummulativeNumberOfValidDefectsLoggedInSITSoFar = 0;
				int cummulativeNumberOfValidDefectsLoggedInUATSoFar = 0;
				int cummulativeTotalNumberOfDefectsInProductionSoFar = 0;
				int cummulativeNumberOfDefectsFoundUsingManualRegression = 0;
				int cummulativeNumberOfDefectsFoundUsingAutomatedRegression = 0;


				int totalEffortConsumedSoFar = 0;


				int cummulativeNumberOfInProjectAutomationTestScriptsExecutedInTheDesiredMonth = 0;
				int cummulativeNumberOfAPIAutomationTestScriptsExecutedInTheDesiredMonth = 0;

				int totalEffortConsumedInTheDesiredMonth = 0;

				int cummulativeNumberOfValidDefectsLoggedInSITInTheDesiredMonth = 0;
				int cummulativeNumberOfValidDefectsLoggedInUATInTheDesiredMonth = 0;
				int cummulativeTotalNumberOfDefectsInProductionInTheDesiredMonth = 0;

				int cummulativeTotalNumberOfInProjectAutomationTestCasesAutomatedInTheDesiredMonth = 0;
				int cummulativeTotalNumberOfAPIAutomationTestCasesAutomatedInTheDesiredMonth = 0;

				int totalEffortConsumedSoFarInTheDesiredMonth = 0;

				int cummulativeNumberOfManualTestCasesExecutedInTheDesiredMonth = 0;
				int cummulativeNumberOfRegressionAutomationTestcasesExecutedInTheDesiredMonth = 0;



				Iterable<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgressIterable = weeklyModuleLevelTestDesignProgressRepository.findAll();
				Iterator<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgressRepositoryIterator = weeklyModuleLevelTestDesignProgressIterable.iterator();

				Iterable<WeeklyProjectLevelTestDesignProgress> weeklyProjectLevelTestDesignProgressIterable = weeklyProjectLevelTestDesignProgressRepository.findAll();
				Iterator<WeeklyProjectLevelTestDesignProgress> weeklyProjectLevelTestDesignProgressRepositoryIterator = weeklyProjectLevelTestDesignProgressIterable.iterator();

				Iterable<WeeklyProjectLevelTestExecutionProgress> weeklyProjectLevelTestExecutionProgressIterable = weeklyProjectLevelTestExecutionProgressRepository.findAll();
				Iterator<WeeklyProjectLevelTestExecutionProgress> weeklyProjectLevelTestExecutionProgressRepositoryIterator = weeklyProjectLevelTestExecutionProgressIterable.iterator();

				Iterable<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgressIterable = weeklyModuleLevelTestExecutionProgressRepository.findAll();
				Iterator<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgressRepositoryIterator = weeklyModuleLevelTestExecutionProgressIterable.iterator();


				System.out.println("Monthly metrics - module level test execution data acummulation in progresss >>>>");
				System.out.println("The current month number is : " + monthNumberInt);
				System.out.println("The current year number is : " + yearNumberInt);
				System.out.println("Data from the weekly test design, test execution progress will be accumulated >>>>");


				while (weeklyModuleLevelTestExecutionProgressRepositoryIterator.hasNext()) {

					WeeklyModuleLevelTestExecutionProgress next = weeklyModuleLevelTestExecutionProgressRepositoryIterator.next();

					if ((next.getProject().getId() == id)) {

						System.out.println("At any point, if the iterated metric month and year is greater than the desired" +
								" month and year then break the loop >>>>");


						if (next.getYearNumber() > yearNumberInt)
							break;

						if (next.getMonthNumber() > monthNumberInt)
							break;

						System.out.println("The following code collates the applicable week numbers for the desired month and year");

						Iterable<WeekMonthData> weekMonthDataIterable = weekMonthDataRepository.findAll();
						Iterator<WeekMonthData> weekMonthDataIterator = weekMonthDataIterable.iterator();

						ArrayList<Integer> weekNumbers = null;

						while (weekMonthDataIterator.hasNext()) {

							WeekMonthData desiredWeeks = weekMonthDataIterator.next();
							int tempYearNumber = desiredWeeks.getYearNumber();
							int tempDiff = tempYearNumber - yearNumberInt;

							if (tempDiff == 0) {

								int tempMonthNumber = desiredWeeks.getMonthNumber();
								tempDiff = tempMonthNumber - monthNumberInt;

								if (tempDiff == 0) {
									weekNumbers = desiredWeeks.getWeekNumbers();
									break;
								}
							}
						}

						System.out.println("The total number of applicable weeks for the desired month and year are is : " + weekNumbers.size());
						System.out.println("These week numbers are : ");
						for(int weekIndex = 0; weekIndex < weekNumbers.size(); weekIndex++) {
							System.out.print(weekIndex + ", ");
						}
						System.out.println();
						System.out.println();



						for (int weekIndex = 0; weekIndex < weekNumbers.size(); weekIndex++) {
							if (weekNumbers.get(weekIndex) == next.getWeekNumber()) {
								cummulativeNumberOfManualTestCasesExecutedInTheDesiredMonth += next.getNumberOfManualTestCasesExecutedForTheWeek();
								cummulativeNumberOfRegressionAutomationTestcasesExecutedInTheDesiredMonth += next.getNumberOfRegressionAutomationTestCasesExecutedForTheWeek();
							}
						}

					}
				}


				while (weeklyProjectLevelTestDesignProgressRepositoryIterator.hasNext()) {

					WeeklyProjectLevelTestDesignProgress next = weeklyProjectLevelTestDesignProgressRepositoryIterator.next();

					if ((next.getProject().getId() == id)) {

						if(next.getYearNumber() > yearNumberInt)
							break;

						if(next.getMonthNumber() > monthNumberInt)
							break;

						Iterable<WeekMonthData> weekMonthDataIterable = weekMonthDataRepository.findAll();
						Iterator<WeekMonthData> weekMonthDataIterator = weekMonthDataIterable.iterator();

						ArrayList<Integer> weekNumbers = null;

						while (weekMonthDataIterator.hasNext()) {

							WeekMonthData desiredWeeks = weekMonthDataIterator.next();
							int tempYearNumber = desiredWeeks.getYearNumber();
							int tempDiff = tempYearNumber - yearNumberInt;

							if (tempDiff == 0) {

								int tempMonthNumber = desiredWeeks.getMonthNumber();
								tempDiff = tempMonthNumber - monthNumberInt;

								if (tempDiff == 0) {
									weekNumbers = desiredWeeks.getWeekNumbers();
									break;
								}
							}
						}

						for(int weekIndex = 0; weekIndex < weekNumbers.size(); weekIndex++)
						{
							if(weekNumbers.get(weekIndex) == next.getWeekNumber())
							{
								cummulativeTotalNumberOfInProjectAutomationTestCasesAutomatedInTheDesiredMonth += next.getTotalNumberOfInProjectAutomationTestCasesAutomated();
								cummulativeTotalNumberOfAPIAutomationTestCasesAutomatedInTheDesiredMonth += next.getTotalNumberOfAPIAutomationTestCasesAutomated();

								totalEffortConsumedSoFarInTheDesiredMonth += next.getTestPlanningEffort();
								totalEffortConsumedSoFarInTheDesiredMonth += next.getManualTestCasesDesignEffort();
								totalEffortConsumedSoFarInTheDesiredMonth += next.getRegressionAutomationDesignEffort();
								totalEffortConsumedSoFarInTheDesiredMonth += next.getApiAutomationDesignEffort();
								totalEffortConsumedSoFarInTheDesiredMonth += next.getInProjectAutomationDesignEffort();
								totalEffortConsumedSoFarInTheDesiredMonth += next.getManualTestCasesMaintenanceEffort();
								totalEffortConsumedSoFarInTheDesiredMonth += next.getAutomationTestCasesMaintenanceEffort();
							}
						}

					}
				}


				while (weeklyProjectLevelTestExecutionProgressRepositoryIterator.hasNext()) {

					WeeklyProjectLevelTestExecutionProgress next = weeklyProjectLevelTestExecutionProgressRepositoryIterator.next();

					if ((next.getProject().getId() == id)) {

						if (next.getYearNumber() > yearNumberInt)
							break;

						if (next.getMonthNumber() > monthNumberInt)
							break;

						Iterable<WeekMonthData> weekMonthDataIterable = weekMonthDataRepository.findAll();
						Iterator<WeekMonthData> weekMonthDataIterator = weekMonthDataIterable.iterator();

						ArrayList<Integer> weekNumbers = null;

						while (weekMonthDataIterator.hasNext()) {

							WeekMonthData desiredWeeks = weekMonthDataIterator.next();
							int tempYearNumber = desiredWeeks.getYearNumber();
							int tempDiff = tempYearNumber - yearNumberInt;

							if (tempDiff == 0) {

								int tempMonthNumber = desiredWeeks.getMonthNumber();
								tempDiff = tempMonthNumber - monthNumberInt;

								if (tempDiff == 0) {
									weekNumbers = desiredWeeks.getWeekNumbers();
									break;
								}
							}
						}

						for (int weekIndex = 0; weekIndex < weekNumbers.size(); weekIndex++) {
							if (weekNumbers.get(weekIndex) == next.getWeekNumber()) {

								cummulativeNumberOfInProjectAutomationTestScriptsExecutedInTheDesiredMonth += next.getNumberOfInProjectAutomationTestScriptsExecuted();
								cummulativeNumberOfAPIAutomationTestScriptsExecutedInTheDesiredMonth += next.getNumberOfAPIAutomationTestScriptsExecuted();

								totalEffortConsumedInTheDesiredMonth += next.getManualTestCaseExecutionEffort();
								totalEffortConsumedInTheDesiredMonth += next.getRegressionAutomationTestCaseExecutionEffort();
								totalEffortConsumedInTheDesiredMonth += next.getInProjectAutomationTestCaseExecutionEffort();
								totalEffortConsumedInTheDesiredMonth += next.getAPIAutomationTestCaseExecutionEffort();

								cummulativeNumberOfValidDefectsLoggedInSITInTheDesiredMonth += next.getNumberOfValidDefectsLoggedInSIT();
								cummulativeNumberOfValidDefectsLoggedInUATInTheDesiredMonth += next.getNumberOfValidDefectsLoggedInUAT();
								cummulativeTotalNumberOfDefectsInProductionInTheDesiredMonth += next.getTotalNumberOfDefectsInProduction();

							}
						}
					}
				}

				weeklyModuleLevelTestDesignProgressIterable = weeklyModuleLevelTestDesignProgressRepository.findAll();
				weeklyModuleLevelTestDesignProgressRepositoryIterator = weeklyModuleLevelTestDesignProgressIterable.iterator();

				weeklyProjectLevelTestDesignProgressIterable = weeklyProjectLevelTestDesignProgressRepository.findAll();
				weeklyProjectLevelTestDesignProgressRepositoryIterator = weeklyProjectLevelTestDesignProgressIterable.iterator();

				weeklyProjectLevelTestExecutionProgressIterable = weeklyProjectLevelTestExecutionProgressRepository.findAll();
				weeklyProjectLevelTestExecutionProgressRepositoryIterator = weeklyProjectLevelTestExecutionProgressIterable.iterator();

				weeklyModuleLevelTestExecutionProgressIterable = weeklyModuleLevelTestExecutionProgressRepository.findAll();
				weeklyModuleLevelTestExecutionProgressRepositoryIterator = weeklyModuleLevelTestExecutionProgressIterable.iterator();


				while (weeklyModuleLevelTestExecutionProgressRepositoryIterator.hasNext()) {

					WeeklyModuleLevelTestExecutionProgress next = weeklyModuleLevelTestExecutionProgressRepositoryIterator.next();

					if ((next.getProject().getId() == id)) {

						if (next.getYearNumber() > yearNumberInt)
							break;

						if (next.getMonthNumber() > monthNumberInt)
							break;

						cummulativeNumberOfManualTestCasesExecutedSofar += next.getNumberOfManualTestCasesExecutedForTheWeek();
						cummulativeNumberOfRegressionAutomationTestCasesExecutedSofar += next.getNumberOfRegressionAutomationTestCasesExecutedForTheWeek();

					}
				}


				while (weeklyProjectLevelTestDesignProgressRepositoryIterator.hasNext()) {

					WeeklyProjectLevelTestDesignProgress next = weeklyProjectLevelTestDesignProgressRepositoryIterator.next();

					if ((next.getProject().getId() == id)) {

						if(next.getYearNumber() > yearNumberInt)
							break;

						if(next.getMonthNumber() > monthNumberInt)
							break;

						cummulativeTotalNumberOfInProjectAutomationTestCasesAutomatedSoFar += next.getTotalNumberOfInProjectAutomationTestCasesAutomated();
						cummulativeTotalNumberOfAPIAutomationTestCasesAutomatedSoFar += next.getTotalNumberOfAPIAutomationTestCasesAutomated();

						totalEffortConsumedSoFar += next.getTestPlanningEffort();
						totalEffortConsumedSoFar += next.getManualTestCasesDesignEffort();
						totalEffortConsumedSoFar += next.getRegressionAutomationDesignEffort();
						totalEffortConsumedSoFar += next.getApiAutomationDesignEffort();
						totalEffortConsumedSoFar += next.getInProjectAutomationDesignEffort();
						totalEffortConsumedSoFar += next.getManualTestCasesMaintenanceEffort();
						totalEffortConsumedSoFar += next.getAutomationTestCasesMaintenanceEffort();

					}
				}


				while (weeklyProjectLevelTestExecutionProgressRepositoryIterator.hasNext()) {

					WeeklyProjectLevelTestExecutionProgress next = weeklyProjectLevelTestExecutionProgressRepositoryIterator.next();

					if ((next.getProject().getId() == id)) {

						if (next.getYearNumber() > yearNumberInt)
							break;

						if (next.getMonthNumber() > monthNumberInt)
							break;

						cummulativeNumberOfInProjectAutomationTestScriptsExecutedSoFar += next.getNumberOfInProjectAutomationTestScriptsExecuted();
						cummulativeNumberOfAPIAutomationTestScriptsExecutedSoFar += next.getNumberOfAPIAutomationTestScriptsExecuted();

						totalEffortConsumedSoFar += next.getManualTestCaseExecutionEffort();
						totalEffortConsumedSoFar += next.getRegressionAutomationTestCaseExecutionEffort();
						totalEffortConsumedSoFar += next.getInProjectAutomationTestCaseExecutionEffort();
						totalEffortConsumedSoFar += next.getAPIAutomationTestCaseExecutionEffort();

						cummulativeNumberOfValidDefectsLoggedInSITSoFar +=  next.getNumberOfValidDefectsLoggedInSIT();
						cummulativeNumberOfValidDefectsLoggedInUATSoFar +=  next.getNumberOfValidDefectsLoggedInUAT();
						cummulativeTotalNumberOfDefectsInProductionSoFar += next.getTotalNumberOfDefectsInProduction();


						cummulativeNumberOfDefectsFoundUsingManualRegression += next.getNumberOfDefectsFoundUsingManualRegression();
						cummulativeNumberOfDefectsFoundUsingAutomatedRegression += next.getNumberOfDefectsFoundUsingAutomatedRegression();

					}
				}

				String plannedStartDate = null;
				String plannedEndDate = null;
				String actualStartDate = null;
				String actualEndDate = null;

				int inProjectAutomationTestCaseEstimate = 0;
				int apiAutomationTestCaseEstimate = 0;
				int totalNumberOfTestCasesTaggedForMigrationExecution = 0;
				int totalPlannedEffort = 0;


				Iterable<LatestProjectEstimate> allLatestProjectEstimates = latestProjectEstimateRepository.findAll();
				Iterator<LatestProjectEstimate> latestProjectEstimateIterator = allLatestProjectEstimates.iterator();

				while (latestProjectEstimateIterator.hasNext()) {
					LatestProjectEstimate latestProjectEstimate = latestProjectEstimateIterator.next();

					if (latestProjectEstimate.getProject().getId() == id) {
						try{
							inProjectAutomationTestCaseEstimate = Integer.parseInt(latestProjectEstimate.getInProjectAutomationTestCaseEstimation());
							apiAutomationTestCaseEstimate = Integer.parseInt(latestProjectEstimate.getAPIAutomationTestCaseEstimation());
							plannedStartDate = latestProjectEstimate.getPlannedStartDate();
							plannedEndDate = latestProjectEstimate.getPlannedEndDate();
							totalNumberOfTestCasesTaggedForMigrationExecution = Integer.parseInt(latestProjectEstimate.getTotalNumberOfTestCasesTaggedForMigrationExecution());

							totalPlannedEffort += Integer.parseInt(latestProjectEstimate.getTestPlanningEffort());

							totalPlannedEffort += Integer.parseInt(latestProjectEstimate.getManualTestCaseDesignEffort());
							totalPlannedEffort += Integer.parseInt(latestProjectEstimate.getRegressionAutomationTestCaseDesignEffort());
							totalPlannedEffort += Integer.parseInt(latestProjectEstimate.getInProjectAutomationTestCaseDesignEffort());
							totalPlannedEffort += Integer.parseInt(latestProjectEstimate.getApiAutomationTestCaseDesignEffort());


							totalPlannedEffort += Integer.parseInt(latestProjectEstimate.getManualTestCaseExecutionEffort());
							totalPlannedEffort += Integer.parseInt(latestProjectEstimate.getRegressionAutomationTestCaseExecutionEffort());
							totalPlannedEffort += Integer.parseInt(latestProjectEstimate.getInProjectAutomationTestCaseExecutionEffort());
							totalPlannedEffort += Integer.parseInt(latestProjectEstimate.getApiAutomationTestCaseExecutionEffort());

							totalPlannedEffort += Integer.parseInt(latestProjectEstimate.getManualTestCaseMaintenanceEffort());
							totalPlannedEffort += Integer.parseInt(latestProjectEstimate.getAutomationTestCaseMaintenanceEffort());


							break;
						}catch(Exception e){
						}
					}
				}

				// ((Actual number of days - Planned number of days) /Planned number of days) * 100

				try {
					actualStartDate = nextMonthlyTestExecutionProgress.getActualStartDateMilestone();
					actualEndDate = nextMonthlyTestExecutionProgress.getActualEndDateMilestone();

					Date d1 = new SimpleDateFormat("dd-MM-yyyy").parse((String) plannedStartDate);
					Date d2 = new SimpleDateFormat("dd-MM-yyyy").parse((String) plannedEndDate);

					Date d3 = new SimpleDateFormat("dd-MM-yyyy").parse((String) actualStartDate);
					Date d4 = new SimpleDateFormat("dd-MM-yyyy").parse((String) actualEndDate);


					long diff1 = d2.getTime() - d1.getTime();            //  20th Jan 2020           10th Jan 2020
					diff1 = diff1 / (1000 * 60 * 60 * 24);   // Actual Start date - Planned Start Date
					//	20th Jan 2021          10th Jan 2021
					long diff2 = d4.getTime() - d3.getTime(); // Actual End date - Planned End Date
					diff2 = diff2 / (1000 * 60 * 60 * 24);


					Double diff1Double = new Double(diff1); // Planned
					Double diff2Double = new Double(diff2); // Actual

					// ((Actual number of days - Planned number of days)/Planned number of days) * 100

					Double temp = 0.0;

					try {
						temp = ((diff2Double - diff1Double) / (diff1Double)) * 100;
					} catch (Exception e) {
					}

					temp = (Math.round(temp * 100.0)) / 100.0;

					if (diff1 > 0) {
						temp = new Double((Math.round(temp * 100.0)) / 100.0);
						monthlyMetric.setScheduleVariance(temp.toString());

						ragStatus = checkRAGStatus(temp, id, monthlyMetric.getScheduleVarianceName());
						System.out.println("The rag status is : " + ragStatus);
						monthlyMetric.setScheduleVarianceStatus(ragStatus);
					} else {
						monthlyMetric.setScheduleVariance("0.0");
						monthlyMetric.setScheduleVarianceStatus("G");
					}
					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){
					System.out.println("Exception occurred during Monthly Metric schedule variance calculation : " + e.getMessage());
				}


				Iterable<LatestModuleEstimate> allLatestModuleEstimates = latestModuleEstimateRepository.findAll();
				Iterator<LatestModuleEstimate> latestModuleEstimateIterator = allLatestModuleEstimates.iterator();

				int totalEstimatedManualTestCases = 0;
				int totalEstimatedRegressionAutomationTestCases = 0;
				int totalEstimatedTestCases = 0;


				while (latestModuleEstimateIterator.hasNext()) {
					LatestModuleEstimate latestModulesEstimate = latestModuleEstimateIterator.next();

					if (latestModulesEstimate.getProject().getId() == id) {
						try{
							totalEstimatedManualTestCases += Integer.parseInt(latestModulesEstimate.getEstimatedManualTestcases());
							totalEstimatedRegressionAutomationTestCases += Integer.parseInt(latestModulesEstimate.getEstimatedRegressionAutomationTestCases());
						}catch(Exception e){
						}
					}
				}


				// Even the Migration test cases should be considered
				totalEstimatedTestCases = totalEstimatedManualTestCases +
						totalEstimatedRegressionAutomationTestCases +
						inProjectAutomationTestCaseEstimate +
						apiAutomationTestCaseEstimate;


				try {
					if (totalEstimatedTestCases > 0) {
						Double numberOfTestCasesTaggedForMigrationDouble = new Double(totalNumberOfTestCasesTaggedForMigrationExecution);
						Double totalEstimatedTestCasesDouble = new Double(totalEstimatedTestCases);

						Double temp = 0.0;

						try {
							temp = ((numberOfTestCasesTaggedForMigrationDouble) / totalEstimatedTestCasesDouble);
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							monthlyMetric.setMigrationExecutionCoverage("0.0");
						} else {
							System.out.println("% of test cases planned for migration testing - " + Math.round(temp * 100.0) / 100.0);
							temp = (Math.round(temp * 100.0)) / 100.0;
							monthlyMetric.setMigrationExecutionCoverage(temp.toString());
						}
					}

					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){

				}



				monthlyTestDesignProgressIterable = monthlyTestDesignProgressRepository.findAll();
				monthlyTestDesignProgressIterator = monthlyTestDesignProgressIterable.iterator();


				String numberOfManualTestCasesAuthoredInTheMonth = "";
				while (monthlyTestDesignProgressIterator.hasNext()) {
					MonthlyTestDesignProgress monthlyTestDesignProgress = monthlyTestDesignProgressIterator.next();

					if ((monthlyTestDesignProgress.getProject().getId() == id) &&
							(monthlyTestDesignProgress.getMonthNumber() == monthNumberInt) &&
							(monthlyTestDesignProgress.getYearNumber() == yearNumberInt))
					{
						numberOfManualTestCasesAuthoredInTheMonth = Integer.toString(monthlyTestDesignProgress.getTotalNumberOfManualTestCasesDesigned());
						break;
					}
				}

				monthlyMetric.setTotalNumberOfManualTestCasesDesigned(numberOfManualTestCasesAuthoredInTheMonth);
				monthlyMetricsRepository.save(monthlyMetric);

				monthlyMetric.setTotalNumberOfResourcesRelieved(Integer.toString(monthlyGovernanceNext.getTotalNumberOfResourcesRelieved()));
				monthlyMetricsRepository.save(monthlyMetric);

				//=IF('Input Sheet'!$F$29=0,"",ROUND(((('Input Sheet'!$F$29+'Input Sheet'!$F$202+'Input Sheet'!F46+'Input Sheet'!F47+'Input Sheet'!F48)-('Input Sheet'!$F$29+'Input Sheet'!F46+'Input Sheet'!F47+'Input Sheet'!F48))/('Input Sheet'!$F$29+'Input Sheet'!F46+'Input Sheet'!F47+'Input Sheet'!F48))*100,2))

				// As per formula - we should consider No of test cases for cost approved CR's (Cumulative Till Date) also
				// This is for scope variance

				try {
					if (totalEstimatedTestCases > 0) {
						Double approvedTestCasesCountDouble = new Double(approvedTestCasesCount);
						Double totalEstimatedTestCasesDouble = new Double(totalEstimatedTestCases);

						Double temp = 0.0;
						try {
							temp = (((approvedTestCasesCountDouble - totalEstimatedTestCasesDouble) / totalEstimatedTestCasesDouble) * 100);
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							monthlyMetric.setScopeVariance("0.0");
							monthlyMetric.setScopeVarianceStatus("0.0");
						} else {
							System.out.println("The scope variance is : " + Math.round(temp * 100.0) / 100.0);
							temp = (Math.round(temp * 100.0)) / 100.0;
							monthlyMetric.setScopeVariance(temp.toString());

							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getScopeVarianceName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setScopeVarianceStatus(ragStatus);
						}
					}

					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){
					System.out.println("Exception occurred during scope variance calculation : " + e.getMessage());
				}

				try {
					if (totalEstimatedTestCases > 0) {

						Double totalPlannedEffortDouble = new Double(totalPlannedEffort);
						Double totalEffortConsumedSoFarDouble = new Double(totalEffortConsumedSoFar);
						Double temp = 0.0;

						try {
							temp = temp = (((totalEffortConsumedSoFarDouble - totalPlannedEffortDouble) / totalPlannedEffortDouble) * 100);
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							monthlyMetric.setEffortVariance("0.0");
							monthlyMetric.setEffortVarianceStatus("0.0");
						} else {
							temp = (Math.round(temp * 100.0)) / 100.0;
							monthlyMetric.setEffortVariance(temp.toString());

							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getEffortVarianceName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setEffortVarianceStatus(ragStatus);
						}
					}
					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){
					System.out.println("Error occurred during effort variance calculation :" + e.getMessage());
				}

				// Cost per defect should consider the actual cost of the month and number of defects found in that month

				try {
					if (nextMonthlyTestExecutionProgress.getNumberOfValidDefectsLoggedInSIT() + nextMonthlyTestExecutionProgress.getNumberOfValidDefectsLoggedInUAT() > 0) {

						Double actualCostDouble = new Double(monthlyGovernanceNext.getActualCostForTheMonth());
						Double numberOfValidDefectsLoggedInSITDouble = new Double(nextMonthlyTestExecutionProgress.getNumberOfValidDefectsLoggedInSIT());
						Double numberOfValidDefectsLoggedInUATDouble = new Double(nextMonthlyTestExecutionProgress.getNumberOfValidDefectsLoggedInUAT());

						Double temp = 0.0;
						try {
							temp = ((actualCostDouble / (numberOfValidDefectsLoggedInSITDouble + numberOfValidDefectsLoggedInUATDouble)));
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							monthlyMetric.setCostPerDefect("0.0");
							monthlyMetric.setCostPerDefectStatus("0.0");

						} else {
							temp = Math.round(temp * 100.0) / 100.0;
							monthlyMetric.setCostPerDefect(temp.toString());
							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getCostPerDefectName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setCostPerDefectStatus(ragStatus);
						}


					}
					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){
					System.out.println("Error occurred suring cost per defect calculation :" + e.getMessage());
				}


				// Migration execution missed out
				// We should consider test cases executed in the month
				// Test cases executed in the previous months should not be considered
				int cummulativeNumberOfTestCasesExecutedSoFar =	cummulativeNumberOfManualTestCasesExecutedSofar +
						cummulativeNumberOfRegressionAutomationTestCasesExecutedSofar +
						cummulativeNumberOfInProjectAutomationTestScriptsExecutedSoFar +
						cummulativeNumberOfAPIAutomationTestScriptsExecutedSoFar;

				try {
					if (cummulativeNumberOfTestCasesExecutedSoFar > 0) {

						Double cummulativeNumberOfTestCasesExecutedSoFarDouble = new Double(cummulativeNumberOfTestCasesExecutedSoFar);
						Double actualCostDouble = new Double(monthlyGovernanceNext.getActualCostForTheMonth());

						Double temp = 0.0;
						try {
							temp = ((actualCostDouble / cummulativeNumberOfTestCasesExecutedSoFarDouble));
						} catch (Exception e) {
						}
						;

						temp = (Math.round(temp * 100.0)) / 100.0;
						if (temp == 0) {
							monthlyMetric.setCostPerTestCase("0.0");
							monthlyMetric.setCostPerTestCaseStatus("0.0");
						} else {
							temp = Math.round(temp * 100.0) / 100.0;
							monthlyMetric.setCostPerTestCase(temp.toString());

							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getCostPerTestCaseName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setCostPerTestCaseStatus(ragStatus);
						}

					}

					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){
					System.out.println("Exception occurred during cost per test case calculation : " + e.getMessage());
				}

				// As per migration coverage - only manual test cases are considered

				try {
					if (totalEstimatedTestCases > 0) {
						Double totalNumberOfTestCasesTaggedForMigrationExecutionDouble = new Double(totalNumberOfTestCasesTaggedForMigrationExecution);
						Double totalEstimatedTestCasesDouble = new Double(totalEstimatedTestCases);

						Double temp = 0.0;
						try {
							temp = (((totalNumberOfTestCasesTaggedForMigrationExecutionDouble * 100) / totalEstimatedTestCasesDouble));
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							monthlyMetric.setPercentageOfTCsIdentifiedForMigration("0.0");
							monthlyMetric.setPercentageOfTCsIdentifiedForMigrationStatus("0.0");
						} else {
							temp = (Math.round(temp * 100.0)) / 100.0;
							monthlyMetric.setPercentageOfTCsIdentifiedForMigration(temp.toString());

							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getPercentageOfTCsIdentifiedForMigrationName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setPercentageOfTCsIdentifiedForMigrationStatus(ragStatus);
						}

					}
					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){
					System.out.println("Error occurred during monthly metric percentage of test cases identified for migration calculation : " + e.getMessage());
				}

				System.out.println("Total number of In Project automation Test cases automated : " + cummulativeTotalNumberOfInProjectAutomationTestCasesAutomatedSoFar);
				System.out.println("In Project automation Test Case estimate are : " + inProjectAutomationTestCaseEstimate );

				try{
					if(inProjectAutomationTestCaseEstimate > 0) {

						Double cummulativeTotalNumberOfInProjectAutomationTestCasesAutomatedDouble = new Double(cummulativeTotalNumberOfInProjectAutomationTestCasesAutomatedSoFar);
						Double inProjectAutomationTestCaseEstimateDouble = new Double(inProjectAutomationTestCaseEstimate);

						Double temp = 0.0;
						try{temp = (((cummulativeTotalNumberOfInProjectAutomationTestCasesAutomatedDouble * 100)/ inProjectAutomationTestCaseEstimateDouble));}catch(Exception e){};

						temp = (Math.round(temp*100.0))/100.0;
						System.out.println("In Project Automation Percentage Extent To Potential : " + Math.round(temp*100.0)/100.0);

						if(temp == 0)
						{
							monthlyMetric.setInProjectAutomationPercentageExtentToPotential("0.0");
							monthlyMetric.setInProjectAutomationPercentageExtentToPotentialStatus("0.0");
						}
						else {
							monthlyMetric.setInProjectAutomationPercentageExtentToPotential(temp.toString());

							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getInProjectAutomationPercentageExtentToPotentialName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setInProjectAutomationPercentageExtentToPotentialStatus(ragStatus);
						}

					}
					monthlyMetricsRepository.save(monthlyMetric);

				}catch(Exception e){
					System.out.println("Error occurred during monthly metric InProject automation percentage extent to potential calculation : " + e.getMessage());
				}

				System.out.println("In Project Automation Percentage Extent To Potential : " + monthlyMetric.getInProjectAutomationPercentageExtentToPotential() );
				System.out.println("Total number of API automation Test cases automated : " + cummulativeTotalNumberOfAPIAutomationTestCasesAutomatedSoFar);
				System.out.println("API automation Test Case estimate are : " + apiAutomationTestCaseEstimate );


				try{
					if(apiAutomationTestCaseEstimate > 0) {

						Double cummulativeTotalNumberOfAPIAutomationTestCasesAutomatedDouble = new Double(cummulativeTotalNumberOfAPIAutomationTestCasesAutomatedSoFar);
						Double apiAutomationTestCaseEstimateDouble = new Double(apiAutomationTestCaseEstimate);

						Double temp = 0.0;
						try{temp = (((cummulativeTotalNumberOfAPIAutomationTestCasesAutomatedDouble * 100)/ apiAutomationTestCaseEstimateDouble));}catch(Exception e){};
						System.out.println("API Automation Percentage Extent To Potential : " + Math.round(temp*100.0)/100.0);


						if(temp == 0)
						{
							monthlyMetric.setApiAutomationPercentageExtentToPotential("0.0");
							monthlyMetric.setAPIAutomationPercentageExtentToPotentialStatus("0.0");
						}
						else {
							temp = (Math.round(temp * 100.0)) / 100.0;
							monthlyMetric.setApiAutomationPercentageExtentToPotential(temp.toString());

							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getApiAutomationPercentageExtentToPotentialName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setAPIAutomationPercentageExtentToPotentialStatus(ragStatus);
						}


					}
					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){
					System.out.println("Error occurred during monthly metric API automation percentage extent to potential calculation : " + e.getMessage());
				}


				System.out.println("API Automation Percentage Extent To Potential : " + monthlyMetric.getApiAutomationPercentageExtentToPotential() );


				// Why only manual test cases are considered
				try{
					if(nextMonthlyTestExecutionProgress.getTotalNumberOfManualSITTestCasesExecuted() > 0) {

						Double totalNumberOfDefectsLoggedInSITDouble = new Double(nextMonthlyTestExecutionProgress.getTotalNumberOfDefectsLoggedInSIT());
						Double totalNumberOfManualSITTestCasesExecutedDouble = new Double(nextMonthlyTestExecutionProgress.getTotalNumberOfManualSITTestCasesExecuted());

						Double temp = 0.0;
						try{temp = (((totalNumberOfDefectsLoggedInSITDouble )/ totalNumberOfManualSITTestCasesExecutedDouble) * 100);}catch(Exception e){};


						if(temp == 0)
						{
							monthlyMetric.setSITDefectDensity("0.0");
							monthlyMetric.setSITDefectDensityStatus(ragStatus);
						}
						else {
							temp = (Math.round(temp * 100.0)) / 100.0;

							monthlyMetric.setSITDefectDensity(temp.toString());
							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getSITDefectDensityName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setSITDefectDensityStatus(ragStatus);
						}

					}
					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){
					System.out.println("Error occurred during monthly metric SIT defect density calculation : " + e.getMessage());
				}


				// Why only manual test cases
				try {
					if (nextMonthlyTestExecutionProgress.getTotalNumberOfManualUATTestCasesExecuted() > 0) {
						Double totalNumberOfDefectsLoggedInUATDouble = new Double(nextMonthlyTestExecutionProgress.getTotalNumberOfDefectsLoggedInUAT());
						Double totalNumberOfManualUATTestCasesExecutedDouble = new Double(nextMonthlyTestExecutionProgress.getTotalNumberOfManualUATTestCasesExecuted());

						Double temp = 0.0;
						try {
							temp = (((totalNumberOfDefectsLoggedInUATDouble) / totalNumberOfManualUATTestCasesExecutedDouble) * 100);
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							monthlyMetric.setUATDefectDensity("0.0");
							monthlyMetric.setUATDefectDensityStatus("0.0");
						} else {
							temp = (Math.round(temp * 100.0)) / 100.0;
							monthlyMetric.setUATDefectDensity(temp.toString());

							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getUATDefectDensityName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setUATDefectDensityStatus(ragStatus);
						}

					}
					monthlyMetricsRepository.save(monthlyMetric);

				}catch(Exception e){
					System.out.println("Error occurred during monthly metric UAT defect density calculation : " + e.getMessage());
				}



				try{
					if(cummulativeNumberOfValidDefectsLoggedInSITSoFar > 0) {

						Double numberOfValidDefectsLoggedInUATDouble = new Double(cummulativeNumberOfValidDefectsLoggedInUATSoFar);
						Double numberOfValidDefectsLoggedInSITDouble = new Double(cummulativeNumberOfValidDefectsLoggedInSITSoFar);

						Double temp = 0.0;
						try{temp = numberOfValidDefectsLoggedInUATDouble / numberOfValidDefectsLoggedInSITDouble;}catch(Exception e){};

						if (temp == 0) {
							monthlyMetric.setDefectLeakageSITToUAT("0.0");
							monthlyMetric.setDefectLeakageSITToUATStatus("0.0");
						} else {
							temp = (Math.round(temp * 100.0)) / 100.0;
							monthlyMetric.setDefectLeakageSITToUAT(temp.toString());

							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getDefectLeakageSITToUATName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setDefectLeakageSITToUATStatus(ragStatus);
						}
					}
					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){
					System.out.println("Error occurred during Monthly metric DefectLeakageSITToUAT calculation " + e.getMessage());
				}

				try {
					if ((cummulativeNumberOfValidDefectsLoggedInSITSoFar > 0) || (cummulativeNumberOfValidDefectsLoggedInSITSoFar > 0)) {

						Double numberOfValidDefectsLoggedInUATDouble = new Double(cummulativeNumberOfValidDefectsLoggedInUATSoFar);
						Double numberOfValidDefectsLoggedInSITDouble = new Double(cummulativeNumberOfValidDefectsLoggedInSITSoFar);
						Double cummulativeTotalNumberOfDefectsInProductionDouble = new Double(cummulativeTotalNumberOfDefectsInProductionSoFar);

						Double temp = 0.0;
						try {
							temp = (cummulativeTotalNumberOfDefectsInProductionDouble / (numberOfValidDefectsLoggedInSITDouble + numberOfValidDefectsLoggedInUATDouble));
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							monthlyMetric.setDefectLeakageToProduction("0.0");
							monthlyMetric.setDefectLeakageToProductionStatus("0.0");

						} else {
							temp = (Math.round(temp * 100.0)) / 100.0;
							monthlyMetric.setDefectLeakageToProduction(temp.toString());

							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getDefectLeakageToProductionName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setDefectLeakageToProductionStatus(ragStatus);
						}
					}

					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){
					System.out.println("Error occurred during Monthly metric DefectLeakageToProduction calculation " + e.getMessage());
				}


				try {
					if ((cummulativeNumberOfDefectsFoundUsingManualRegression  > 0)) {

						Double cummulativeNumberOfDefectsFoundUsingManualRegressionDouble = new Double(cummulativeNumberOfDefectsFoundUsingManualRegression);
						Double cummulativeNumberOfDefectsFoundUsingAutomatedRegressionDouble = new Double(cummulativeNumberOfDefectsFoundUsingAutomatedRegression );

						Double temp = 0.0;
						try {
							temp = ((cummulativeNumberOfDefectsFoundUsingAutomatedRegressionDouble * 100) / (cummulativeNumberOfDefectsFoundUsingManualRegressionDouble));
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							monthlyMetric.setAutomationEffectiveness("0.0");
							monthlyMetric.setAutomationEffectivenessStatus("0.0");

						} else {
							temp = (Math.round(temp * 100.0)) / 100.0;
							monthlyMetric.setAutomationEffectiveness(temp.toString());

							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getAutomationEffectivenessName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setAutomationEffectivenessStatus(ragStatus);
						}
					}

					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){
					System.out.println("Error occurred during Monthly metric DefectLeakageToProduction calculation " + e.getMessage());
				}

				monthlyMetric.setNumberOfStepChangesProposed(Integer.toString(monthlyGovernanceNext.getNumberOfStepChangesProposed()));
				monthlyMetricsRepository.save(monthlyMetric);

				monthlyMetric.setNumberOfStepChangesInProgress(Integer.toString(monthlyGovernanceNext.getNumberOfStepChangesInProgress()));
				monthlyMetricsRepository.save(monthlyMetric);

				monthlyMetric.setNumberOfStepChangesImplemented(Integer.toString(monthlyGovernanceNext.getNumberOfStepChangesImplemented()));
				monthlyMetricsRepository.save(monthlyMetric);

				monthlyMetric.setGrossMargin(Integer.toString(monthlyGovernanceNext.getGrossMarginForTheMonth()));
				monthlyMetricsRepository.save(monthlyMetric);

				monthlyMetric.setTotalNumberOfResignationsSubmitted(Integer.toString(monthlyGovernanceNext.getTotalNumberOfResignationsSubmitted()));
				monthlyMetricsRepository.save(monthlyMetric);

				monthlyMetric.setTotalNumberOfResourcesRelieved(Integer.toString(monthlyGovernanceNext.getTotalNumberOfResourcesRelieved()));
				monthlyMetricsRepository.save(monthlyMetric);

				monthlyMetric.setNumberOfTestEngineers(Integer.toString(monthlyGovernanceNext.getNumberOfTestEngineers()));
				monthlyMetricsRepository.save(monthlyMetric);

				monthlyMetric.setNumberOfLeads(Integer.toString(monthlyGovernanceNext.getNumberOfLeads()));
				monthlyMetricsRepository.save(monthlyMetric);

				monthlyMetric.setNumberOfManagers(Integer.toString(monthlyGovernanceNext.getNumberOfManagers()));
				monthlyMetricsRepository.save(monthlyMetric);

				monthlyMetric.setTotalNumberOfTestCasesTaggedForMigrationExecution(Integer.toString(totalNumberOfTestCasesTaggedForMigrationExecution));
				monthlyMetricsRepository.save(monthlyMetric);

				monthlyMetric.setEstimatedTestCases(Integer.toString(totalEstimatedTestCases));
				monthlyMetricsRepository.save(monthlyMetric);


				cummulativeNumberOfTestCasesExecutedSoFar = 0;
				totalEffortConsumedSoFar = 0;
				totalPlannedEffort = 0;
			}


			Iterable<MonthlyMetrics> monthlyMetricsIterable = monthlyMetricsRepository.findAll();
			Iterator<MonthlyMetrics> monthlyMetricsIterableIterator = monthlyMetricsIterable.iterator();

			MonthlyMetrics monthlyMetric = null;

			while (monthlyMetricsIterableIterator.hasNext()) {
				monthlyMetric = monthlyMetricsIterableIterator.next();

				if ((monthlyMetric.getProject().getId() == id)) {

					String totalString = "{\"Metrics Measurements\":";

					totalString += monthlyMetric.getMonthNumber();
					totalString += ",\"";

					totalString += monthlyMetric.getYearNumberName();
					totalString += "\":";

					totalString += monthlyMetric.getYearNumber();
					totalString += ",\"";

					totalString += monthlyMetric.getCostVarianceName();
					totalString += "\":";

					totalString += monthlyMetric.getCostVariance();
					totalString += ",\"";

					totalString += monthlyMetric.getScheduleVarianceName();
					totalString += "\":";

					totalString += monthlyMetric.getScheduleVariance();
					totalString += ",\"";

					totalString += monthlyMetric.getScopeVarianceName();
					totalString += "\":";

					totalString += monthlyMetric.getScopeVariance();
					totalString += ",\"";

					totalString += monthlyMetric.getEffortVarianceName();
					totalString += "\":";

					totalString += monthlyMetric.getEffortVariance();
					totalString += ",\"";

					totalString += monthlyMetric.getCostPerDefectName();
					totalString += "\":";

					totalString += monthlyMetric.getCostPerDefect();
					totalString += ",\"";

					totalString += monthlyMetric.getCostPerTestCaseName();
					totalString += "\":";

					totalString += monthlyMetric.getCostPerTestCase();
					totalString += ",\"";

					totalString += monthlyMetric.getTotalNumberOfManualTestCasesDesignedName();
					totalString += "\":";

					totalString += monthlyMetric.getTotalNumberOfManualTestCasesDesigned();
					totalString += ",\"";

					totalString += monthlyMetric.getPercentageOfTCsIdentifiedForMigrationName();
					totalString += "\":";

					totalString += monthlyMetric.getPercentageOfTCsIdentifiedForMigration();
					totalString += ",\"";

					totalString += monthlyMetric.getGrossMarginName();
					totalString += "\":";

					totalString += monthlyMetric.getGrossMargin();
					totalString += ",\"";

					totalString += monthlyMetric.getTotalNumberOfResignationsSubmittedName();
					totalString += "\":";

					totalString += monthlyMetric.getTotalNumberOfResignationsSubmitted();
					totalString += ",\"";

					totalString += monthlyMetric.getTotalNumberOfResourcesRelievedName();
					totalString += "\":";

					totalString += monthlyMetric.getTotalNumberOfResourcesRelieved();
					totalString += ",\"";

					totalString += monthlyMetric.getSITDefectDensityName();
					totalString += "\":";

					totalString += monthlyMetric.getSITDefectDensity();
					totalString += ",\"";

					totalString += monthlyMetric.getUATDefectDensityName();
					totalString += "\":";

					totalString += monthlyMetric.getUATDefectDensity();
					totalString += ",\"";


					totalString += monthlyMetric.getNumberOfStepChangesInProgressName();
					totalString += "\":";

					totalString += monthlyMetric.getNumberOfStepChangesInProgress();
					totalString += ",\"";

					totalString += monthlyMetric.getNumberOfTestEngineersName();
					totalString += "\":";

					totalString += monthlyMetric.getNumberOfTestEngineers();
					totalString += ",\"";

					totalString += monthlyMetric.getNumberOfLeadsName();
					totalString += "\":";

					totalString += monthlyMetric.getNumberOfLeads();
					totalString += ",\"";

					totalString += monthlyMetric.getNumberOfManagersName();
					totalString += "\":";

					totalString += monthlyMetric.getNumberOfManagers();
					totalString += ",\"";

					totalString += monthlyMetric.getMigrationExecutionCoverageName();
					totalString += "\":";

					totalString += monthlyMetric.getMigrationExecutionCoverage();
					totalString += ",\"";

					totalString += monthlyMetric.getTotalNumberOfTestCasesTaggedForMigrationExecutionName();
					totalString += "\":";

					totalString += monthlyMetric.getTotalNumberOfTestCasesTaggedForMigrationExecution();
					totalString += ",\"";

					totalString += monthlyMetric.getEstimatedTestCasesName();
					totalString += "\":";

					totalString += monthlyMetric.getEstimatedTestCases();
					totalString += ",\"";

					totalString = addMetricsLCLUCLGoal(id, totalString);

					totalString += monthlyMetric.getNumberOfStepChangesImplementedName();
					totalString += "\":";

					totalString += monthlyMetric.getNumberOfStepChangesImplemented();
					totalString += "}";

					System.out.println("The monthly metrics is : " + totalString);

					BufferedWriter bw = null;
					FileWriter fw = null;

					try {
						fw = new FileWriter("Metrics\\Monthly\\Month_" +  monthlyMetric.getMonthNumber() + "_Metrics.json");
						bw = new BufferedWriter((fw));
						bw.write(totalString);
						bw.close();
						fw.close();
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}

					try {

						HttpHost host = new HttpHost("localhost", 9200);
						final RestClientBuilder builder = RestClient.builder(host);
						client = new RestHighLevelClient(builder);
						save(Integer.toString(monthlyMetric.getMonthNumber()), totalString, MONTHLY_INDEX);
						client.indices().refresh(new RefreshRequest(MONTHLY_INDEX), RequestOptions.DEFAULT);
						client.close();

					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
				}
			}

		} catch(Exception e){
			System.out.println("Exception occured during montly metric generation : " + e.getMessage());
		}

		return "OK";
	}


	public String generateWeekMonthlyMetrics(long id) {

		try{

			Iterable<MonthlyTestDesignProgress> monthlyTestDesignProgressIterable = monthlyTestDesignProgressRepository.findAll();
			Iterator<MonthlyTestDesignProgress> monthlyTestDesignProgressIterator = monthlyTestDesignProgressIterable.iterator();

			Iterable<MonthlyTestExecutionProgress> monthlyTestExecutionProgress = monthlyTestExecutionProgressRepository.findAll();
			Iterator<MonthlyTestExecutionProgress> monthlyTestExecutionProgressIterator = monthlyTestExecutionProgress.iterator();

			ArrayList<String> monthNumbers = new ArrayList<String>();
			ArrayList<String> monthNumbersTestDesignProgress = new ArrayList<String>();
			ArrayList<String> monthNumbersTestExecutionProgress = new ArrayList<String>();


			monthlyTestDesignProgressIterable = monthlyTestDesignProgressRepository.findAll();
			monthlyTestDesignProgressIterator = monthlyTestDesignProgressIterable.iterator();

			while (monthlyTestDesignProgressIterator.hasNext()) {
				MonthlyTestDesignProgress next = monthlyTestDesignProgressIterator.next();

				if (next.getProject().getId() == id) {

					int tempWeekNumber = next.getMonthNumber();
					if(tempWeekNumber < 10)
						monthNumbersTestDesignProgress.add(next.getYearNumber() + "-" + "0" + next.getMonthNumber());
					else
						monthNumbersTestDesignProgress.add(next.getYearNumber() + "-" + next.getMonthNumber());
				}

			}

			monthlyTestExecutionProgress = monthlyTestExecutionProgressRepository.findAll();
			monthlyTestExecutionProgressIterator = monthlyTestExecutionProgress.iterator();

			while (monthlyTestExecutionProgressIterator.hasNext()) {
				MonthlyTestExecutionProgress next = monthlyTestExecutionProgressIterator.next();

				if (next.getProject().getId() == id) {

					int tempWeekNumber = next.getMonthNumber();
					if(tempWeekNumber < 10)
						monthNumbersTestExecutionProgress.add(next.getYearNumber() + "-" + "0" + next.getMonthNumber());
					else
						monthNumbersTestExecutionProgress.add(next.getYearNumber() + "-" + next.getMonthNumber());

				}
			}


			if(monthNumbersTestDesignProgress.size() > monthNumbersTestExecutionProgress.size())
			{
				for(int i = 0; i < monthNumbersTestDesignProgress.size(); i++)
				{
					for(int j = 0; j < monthNumbersTestExecutionProgress.size(); j++ )
					{
						if(monthNumbersTestDesignProgress.get(i).compareTo(monthNumbersTestExecutionProgress.get(j)) == 0)
						{
							monthNumbers.add(monthNumbersTestDesignProgress.get(i));
						}
					}
				}
			}
			else
			{
				for(int i = 0; i < monthNumbersTestExecutionProgress.size(); i++)
				{
					for(int j = 0; j < monthNumbersTestDesignProgress.size(); j++ )
					{
						if(monthNumbersTestExecutionProgress.get(i).compareTo(monthNumbersTestDesignProgress.get(j))  == 0)
						{
							monthNumbers.add(monthNumbersTestExecutionProgress.get(i));
						}
					}
				}
			}

			Collections.sort(monthNumbers);

			System.out.println("The total number of monthly metrics available is : " + monthNumbers.size());

			for(int index = 0; index < monthNumbers.size(); index++){
				System.out.println("The current month being processed is : " + index);
				System.out.println("This loop should repeat for only once each month");

				Iterable<MonthlyMetrics> monthlyMetricsIterable = monthlyMetricsRepository.findAll();
				Iterator<MonthlyMetrics> monthlyMetricsIterableIterator = monthlyMetricsIterable.iterator();

				boolean metricMatchFound = false;
				MonthlyMetrics monthlyMetric = null;
				int monthNumberInt = 0;
				int yearNumberInt = 0;
				String ragStatus ="";

				StringTokenizer tokenizer = new StringTokenizer(monthNumbers.get(index), "-");

				System.out.println("The current year and month is : " + monthNumbers.get(index));
				String yearNumber = tokenizer.nextToken();
				String monthNumber = tokenizer.nextToken();

				yearNumberInt = Integer.parseInt(yearNumber);
				monthNumberInt = Integer.parseInt(monthNumber);

				System.out.println("The current year is : " + yearNumberInt);
				System.out.println("The current month is : " + monthNumberInt);

				System.out.println("Moving the monthly metric iterator to current month and year: " + monthNumberInt);

				while (monthlyMetricsIterableIterator.hasNext()) {
					monthlyMetric = monthlyMetricsIterableIterator.next();

					if (monthlyMetric.getProject().getId() == id) {

						if (((monthNumberInt - monthlyMetric.getMonthNumber()) == 0) && (yearNumberInt == monthlyMetric.getYearNumber())) {
							metricMatchFound = true;
							break;
						}
					}
				}


				if (metricMatchFound == false) {

					System.out.println("The monthly metric for current month and year was not found ");
					System.out.println("Hence creating the monthly metric for current month and year");

					monthlyMetric = new MonthlyMetrics();
					monthlyMetric.setMonthNumber(monthNumberInt);
					monthlyMetric.setYearNumber(yearNumberInt);

					Iterable<Project> allProjects = projectRepository.findAll();
					Iterator<Project> iterator = allProjects.iterator();

					while (iterator.hasNext()) {
						Project nextProject = iterator.next();

						if (nextProject.getId() == id) {
							monthlyMetric.setProject(nextProject);
							monthlyMetricsRepository.save(monthlyMetric);
							break;
						}
					}
				}


				System.out.println("Moving the monthly test execution progress iterator to point to desired month");
				monthlyTestExecutionProgress = monthlyTestExecutionProgressRepository.findAll();
				monthlyTestExecutionProgressIterator = monthlyTestExecutionProgress.iterator();
				MonthlyTestExecutionProgress nextMonthlyTestExecutionProgress = null;

				while (monthlyTestExecutionProgressIterator.hasNext()) {
					nextMonthlyTestExecutionProgress = monthlyTestExecutionProgressIterator.next();

					if ((nextMonthlyTestExecutionProgress.getProject().getId() == id) &&
							(nextMonthlyTestExecutionProgress.getMonthNumber() == monthNumberInt) &&
							(nextMonthlyTestExecutionProgress.getYearNumber() == yearNumberInt)) {

						System.out.println("Matching monthly test execution progress for desired month and year found");
						break;
					}

				}

				System.out.println("Moving the monthly governance progress iterator to point to desired month");
				Iterable<MonthlyGovernance>	monthlyGovernanceIterable = monthlyGovernanceRepository.findAll();
				Iterator<MonthlyGovernance>	monthlyGovernanceIterator = monthlyGovernanceIterable.iterator();
				MonthlyGovernance monthlyGovernanceNext = null;

				while (monthlyGovernanceIterator.hasNext()) {

					monthlyGovernanceNext = monthlyGovernanceIterator.next();

					if ((monthlyGovernanceNext.getProject().getId() == id) &&
							(monthlyGovernanceNext.getMonthNumber() == monthNumberInt) &&
							(monthlyGovernanceNext.getYearNumber() == yearNumberInt)) {
						System.out.println("Matching monthly test governance progress for desired month and year found");
						break;
					}

				}

				int approvedTestCasesCount = 0;
				approvedTestCasesCount = monthlyGovernanceNext.getNoOfTestCasesForCostApprovedCRs();

				System.out.println("Calculating cost variance for the month........  ");
				try {

					System.out.println("All the necessary inputs are provided by the user for the month");
					System.out.println("The numerator is difference of actual cost and the budgetted cost for the month");
					System.out.println("The denominator is budgetted cost for the month");

					if (monthlyGovernanceNext.getBudgetCostForTheMonth() > 0) {

						Double budgetCostDouble = new Double(monthlyGovernanceNext.getBudgetCostForTheMonth());
						Double actualCostDouble = new Double(monthlyGovernanceNext.getActualCostForTheMonth());
						Double temp = 0.0;
						try {
							temp = ((actualCostDouble - budgetCostDouble) / budgetCostDouble) * 100;
						} catch (Exception e) {
						}

						if (temp == 0) {
							monthlyMetric.setCostVariance("0.0");
							monthlyMetric.setCostVarianceStatus("0.0");
						} else {
							System.out.println("The cost variance is : " + Math.round(temp * 100.0) / 100.0);
							temp = (Math.round(temp * 100.0)) / 100.0;
							monthlyMetric.setCostVariance(temp.toString());
							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getCostVarianceName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setCostVarianceStatus(ragStatus);
						}

					}

					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){
					System.out.println("Exception occurred during cost variance calculation for the month : " + monthNumberInt + " " + yearNumberInt);
					System.out.println(e.getMessage());
				}


				int cummulativeTotalNumberOfInProjectAutomationTestCasesAutomatedSoFar = 0;
				int cummulativeTotalNumberOfAPIAutomationTestCasesAutomatedSoFar = 0;

				int cummulativeNumberOfManualTestCasesExecutedSofar = 0;
				int cummulativeNumberOfRegressionAutomationTestCasesExecutedSofar = 0;
				int cummulativeNumberOfInProjectAutomationTestScriptsExecutedSoFar = 0;
				int cummulativeNumberOfAPIAutomationTestScriptsExecutedSoFar = 0;

				// Defects
				int cummulativeNumberOfValidDefectsLoggedInSITSoFar = 0;
				int cummulativeNumberOfValidDefectsLoggedInUATSoFar = 0;
				int cummulativeTotalNumberOfDefectsInProductionSoFar = 0;
				int cummulativeNumberOfDefectsFoundUsingManualRegression = 0;
				int cummulativeNumberOfDefectsFoundUsingAutomatedRegression = 0;


				int totalEffortConsumedSoFar = 0;


				int cummulativeNumberOfInProjectAutomationTestScriptsExecutedInTheDesiredMonth = 0;
				int cummulativeNumberOfAPIAutomationTestScriptsExecutedInTheDesiredMonth = 0;

				int totalEffortConsumedInTheDesiredMonth = 0;

				int cummulativeNumberOfValidDefectsLoggedInSITInTheDesiredMonth = 0;
				int cummulativeNumberOfValidDefectsLoggedInUATInTheDesiredMonth = 0;
				int cummulativeTotalNumberOfDefectsInProductionInTheDesiredMonth = 0;

				int cummulativeTotalNumberOfInProjectAutomationTestCasesAutomatedInTheDesiredMonth = 0;
				int cummulativeTotalNumberOfAPIAutomationTestCasesAutomatedInTheDesiredMonth = 0;

				int totalEffortConsumedSoFarInTheDesiredMonth = 0;

				int cummulativeNumberOfManualTestCasesExecutedInTheDesiredMonth = 0;
				int cummulativeNumberOfRegressionAutomationTestcasesExecutedInTheDesiredMonth = 0;



				Iterable<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgressIterable = weeklyModuleLevelTestDesignProgressRepository.findAll();
				Iterator<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgressRepositoryIterator = weeklyModuleLevelTestDesignProgressIterable.iterator();

				Iterable<WeeklyProjectLevelTestDesignProgress> weeklyProjectLevelTestDesignProgressIterable = weeklyProjectLevelTestDesignProgressRepository.findAll();
				Iterator<WeeklyProjectLevelTestDesignProgress> weeklyProjectLevelTestDesignProgressRepositoryIterator = weeklyProjectLevelTestDesignProgressIterable.iterator();

				Iterable<WeeklyProjectLevelTestExecutionProgress> weeklyProjectLevelTestExecutionProgressIterable = weeklyProjectLevelTestExecutionProgressRepository.findAll();
				Iterator<WeeklyProjectLevelTestExecutionProgress> weeklyProjectLevelTestExecutionProgressRepositoryIterator = weeklyProjectLevelTestExecutionProgressIterable.iterator();

				Iterable<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgressIterable = weeklyModuleLevelTestExecutionProgressRepository.findAll();
				Iterator<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgressRepositoryIterator = weeklyModuleLevelTestExecutionProgressIterable.iterator();


				System.out.println("Monthly metrics - module level test execution data acummulation in progresss >>>>");
				System.out.println("The current month number is : " + monthNumberInt);
				System.out.println("The current year number is : " + yearNumberInt);
				System.out.println("Data from the weekly test design, test execution progress will be accumulated >>>>");


				while (weeklyModuleLevelTestExecutionProgressRepositoryIterator.hasNext()) {

					WeeklyModuleLevelTestExecutionProgress next = weeklyModuleLevelTestExecutionProgressRepositoryIterator.next();

					if ((next.getProject().getId() == id)) {

						System.out.println("At any point, if the iterated metric month and year is greater than the desired" +
								" month and year then break the loop >>>>");


						if (next.getYearNumber() > yearNumberInt)
							break;

						if (next.getMonthNumber() > monthNumberInt)
							break;

						System.out.println("The following code collates the applicable week numbers for the desired month and year");

						Iterable<WeekMonthData> weekMonthDataIterable = weekMonthDataRepository.findAll();
						Iterator<WeekMonthData> weekMonthDataIterator = weekMonthDataIterable.iterator();

						ArrayList<Integer> weekNumbers = null;

						while (weekMonthDataIterator.hasNext()) {

							WeekMonthData desiredWeeks = weekMonthDataIterator.next();
							int tempYearNumber = desiredWeeks.getYearNumber();
							int tempDiff = tempYearNumber - yearNumberInt;

							if (tempDiff == 0) {

								int tempMonthNumber = desiredWeeks.getMonthNumber();
								tempDiff = tempMonthNumber - monthNumberInt;

								if (tempDiff == 0) {
									weekNumbers = desiredWeeks.getWeekNumbers();
									break;
								}
							}
						}

						System.out.println("The total number of applicable weeks for the desired month and year are is : " + weekNumbers.size());
						System.out.println("These week numbers are : ");
						for(int weekIndex = 0; weekIndex < weekNumbers.size(); weekIndex++) {
							System.out.print(weekIndex + ", ");
						}
						System.out.println();
						System.out.println();



						for (int weekIndex = 0; weekIndex < weekNumbers.size(); weekIndex++) {
							if (weekNumbers.get(weekIndex) == next.getWeekNumber()) {
								cummulativeNumberOfManualTestCasesExecutedInTheDesiredMonth += next.getNumberOfManualTestCasesExecutedForTheWeek();
								cummulativeNumberOfRegressionAutomationTestcasesExecutedInTheDesiredMonth += next.getNumberOfRegressionAutomationTestCasesExecutedForTheWeek();
							}
						}

					}
				}


				while (weeklyProjectLevelTestDesignProgressRepositoryIterator.hasNext()) {

					WeeklyProjectLevelTestDesignProgress next = weeklyProjectLevelTestDesignProgressRepositoryIterator.next();

					if ((next.getProject().getId() == id)) {

						if(next.getYearNumber() > yearNumberInt)
							break;

						if(next.getMonthNumber() > monthNumberInt)
							break;

						Iterable<WeekMonthData> weekMonthDataIterable = weekMonthDataRepository.findAll();
						Iterator<WeekMonthData> weekMonthDataIterator = weekMonthDataIterable.iterator();

						ArrayList<Integer> weekNumbers = null;

						while (weekMonthDataIterator.hasNext()) {

							WeekMonthData desiredWeeks = weekMonthDataIterator.next();
							int tempYearNumber = desiredWeeks.getYearNumber();
							int tempDiff = tempYearNumber - yearNumberInt;

							if (tempDiff == 0) {

								int tempMonthNumber = desiredWeeks.getMonthNumber();
								tempDiff = tempMonthNumber - monthNumberInt;

								if (tempDiff == 0) {
									weekNumbers = desiredWeeks.getWeekNumbers();
									break;
								}
							}
						}

						for(int weekIndex = 0; weekIndex < weekNumbers.size(); weekIndex++)
						{
							if(weekNumbers.get(weekIndex) == next.getWeekNumber())
							{
								cummulativeTotalNumberOfInProjectAutomationTestCasesAutomatedInTheDesiredMonth += next.getTotalNumberOfInProjectAutomationTestCasesAutomated();
								cummulativeTotalNumberOfAPIAutomationTestCasesAutomatedInTheDesiredMonth += next.getTotalNumberOfAPIAutomationTestCasesAutomated();

								totalEffortConsumedSoFarInTheDesiredMonth += next.getTestPlanningEffort();
								totalEffortConsumedSoFarInTheDesiredMonth += next.getManualTestCasesDesignEffort();
								totalEffortConsumedSoFarInTheDesiredMonth += next.getRegressionAutomationDesignEffort();
								totalEffortConsumedSoFarInTheDesiredMonth += next.getApiAutomationDesignEffort();
								totalEffortConsumedSoFarInTheDesiredMonth += next.getInProjectAutomationDesignEffort();
								totalEffortConsumedSoFarInTheDesiredMonth += next.getManualTestCasesMaintenanceEffort();
								totalEffortConsumedSoFarInTheDesiredMonth += next.getAutomationTestCasesMaintenanceEffort();
							}
						}

					}
				}


				while (weeklyProjectLevelTestExecutionProgressRepositoryIterator.hasNext()) {

					WeeklyProjectLevelTestExecutionProgress next = weeklyProjectLevelTestExecutionProgressRepositoryIterator.next();

					if ((next.getProject().getId() == id)) {

						if (next.getYearNumber() > yearNumberInt)
							break;

						if (next.getMonthNumber() > monthNumberInt)
							break;

						Iterable<WeekMonthData> weekMonthDataIterable = weekMonthDataRepository.findAll();
						Iterator<WeekMonthData> weekMonthDataIterator = weekMonthDataIterable.iterator();

						ArrayList<Integer> weekNumbers = null;

						while (weekMonthDataIterator.hasNext()) {

							WeekMonthData desiredWeeks = weekMonthDataIterator.next();
							int tempYearNumber = desiredWeeks.getYearNumber();
							int tempDiff = tempYearNumber - yearNumberInt;

							if (tempDiff == 0) {

								int tempMonthNumber = desiredWeeks.getMonthNumber();
								tempDiff = tempMonthNumber - monthNumberInt;

								if (tempDiff == 0) {
									weekNumbers = desiredWeeks.getWeekNumbers();
									break;
								}
							}
						}

						for (int weekIndex = 0; weekIndex < weekNumbers.size(); weekIndex++) {
							if (weekNumbers.get(weekIndex) == next.getWeekNumber()) {

								cummulativeNumberOfInProjectAutomationTestScriptsExecutedInTheDesiredMonth += next.getNumberOfInProjectAutomationTestScriptsExecuted();
								cummulativeNumberOfAPIAutomationTestScriptsExecutedInTheDesiredMonth += next.getNumberOfAPIAutomationTestScriptsExecuted();

								totalEffortConsumedInTheDesiredMonth += next.getManualTestCaseExecutionEffort();
								totalEffortConsumedInTheDesiredMonth += next.getRegressionAutomationTestCaseExecutionEffort();
								totalEffortConsumedInTheDesiredMonth += next.getInProjectAutomationTestCaseExecutionEffort();
								totalEffortConsumedInTheDesiredMonth += next.getAPIAutomationTestCaseExecutionEffort();

								cummulativeNumberOfValidDefectsLoggedInSITInTheDesiredMonth += next.getNumberOfValidDefectsLoggedInSIT();
								cummulativeNumberOfValidDefectsLoggedInUATInTheDesiredMonth += next.getNumberOfValidDefectsLoggedInUAT();
								cummulativeTotalNumberOfDefectsInProductionInTheDesiredMonth += next.getTotalNumberOfDefectsInProduction();

							}
						}
					}
				}

				weeklyModuleLevelTestDesignProgressIterable = weeklyModuleLevelTestDesignProgressRepository.findAll();
				weeklyModuleLevelTestDesignProgressRepositoryIterator = weeklyModuleLevelTestDesignProgressIterable.iterator();

				weeklyProjectLevelTestDesignProgressIterable = weeklyProjectLevelTestDesignProgressRepository.findAll();
				weeklyProjectLevelTestDesignProgressRepositoryIterator = weeklyProjectLevelTestDesignProgressIterable.iterator();

				weeklyProjectLevelTestExecutionProgressIterable = weeklyProjectLevelTestExecutionProgressRepository.findAll();
				weeklyProjectLevelTestExecutionProgressRepositoryIterator = weeklyProjectLevelTestExecutionProgressIterable.iterator();

				weeklyModuleLevelTestExecutionProgressIterable = weeklyModuleLevelTestExecutionProgressRepository.findAll();
				weeklyModuleLevelTestExecutionProgressRepositoryIterator = weeklyModuleLevelTestExecutionProgressIterable.iterator();


				while (weeklyModuleLevelTestExecutionProgressRepositoryIterator.hasNext()) {

					WeeklyModuleLevelTestExecutionProgress next = weeklyModuleLevelTestExecutionProgressRepositoryIterator.next();

					if ((next.getProject().getId() == id)) {

						if (next.getYearNumber() > yearNumberInt)
							break;

						if (next.getMonthNumber() > monthNumberInt)
							break;

						cummulativeNumberOfManualTestCasesExecutedSofar += next.getNumberOfManualTestCasesExecutedForTheWeek();
						cummulativeNumberOfRegressionAutomationTestCasesExecutedSofar += next.getNumberOfRegressionAutomationTestCasesExecutedForTheWeek();

					}
				}


				while (weeklyProjectLevelTestDesignProgressRepositoryIterator.hasNext()) {

					WeeklyProjectLevelTestDesignProgress next = weeklyProjectLevelTestDesignProgressRepositoryIterator.next();

					if ((next.getProject().getId() == id)) {

						if(next.getYearNumber() > yearNumberInt)
							break;

						if(next.getMonthNumber() > monthNumberInt)
							break;

						cummulativeTotalNumberOfInProjectAutomationTestCasesAutomatedSoFar += next.getTotalNumberOfInProjectAutomationTestCasesAutomated();
						cummulativeTotalNumberOfAPIAutomationTestCasesAutomatedSoFar += next.getTotalNumberOfAPIAutomationTestCasesAutomated();

						totalEffortConsumedSoFar += next.getTestPlanningEffort();
						totalEffortConsumedSoFar += next.getManualTestCasesDesignEffort();
						totalEffortConsumedSoFar += next.getRegressionAutomationDesignEffort();
						totalEffortConsumedSoFar += next.getApiAutomationDesignEffort();
						totalEffortConsumedSoFar += next.getInProjectAutomationDesignEffort();
						totalEffortConsumedSoFar += next.getManualTestCasesMaintenanceEffort();
						totalEffortConsumedSoFar += next.getAutomationTestCasesMaintenanceEffort();

					}
				}


				while (weeklyProjectLevelTestExecutionProgressRepositoryIterator.hasNext()) {

					WeeklyProjectLevelTestExecutionProgress next = weeklyProjectLevelTestExecutionProgressRepositoryIterator.next();

					if ((next.getProject().getId() == id)) {

						if (next.getYearNumber() > yearNumberInt)
							break;

						if (next.getMonthNumber() > monthNumberInt)
							break;

						cummulativeNumberOfInProjectAutomationTestScriptsExecutedSoFar += next.getNumberOfInProjectAutomationTestScriptsExecuted();
						cummulativeNumberOfAPIAutomationTestScriptsExecutedSoFar += next.getNumberOfAPIAutomationTestScriptsExecuted();

						totalEffortConsumedSoFar += next.getManualTestCaseExecutionEffort();
						totalEffortConsumedSoFar += next.getRegressionAutomationTestCaseExecutionEffort();
						totalEffortConsumedSoFar += next.getInProjectAutomationTestCaseExecutionEffort();
						totalEffortConsumedSoFar += next.getAPIAutomationTestCaseExecutionEffort();

						cummulativeNumberOfValidDefectsLoggedInSITSoFar +=  next.getNumberOfValidDefectsLoggedInSIT();
						cummulativeNumberOfValidDefectsLoggedInUATSoFar +=  next.getNumberOfValidDefectsLoggedInUAT();
						cummulativeTotalNumberOfDefectsInProductionSoFar += next.getTotalNumberOfDefectsInProduction();


						cummulativeNumberOfDefectsFoundUsingManualRegression += next.getNumberOfDefectsFoundUsingManualRegression();
						cummulativeNumberOfDefectsFoundUsingAutomatedRegression += next.getNumberOfDefectsFoundUsingAutomatedRegression();

					}
				}

				String plannedStartDate = null;
				String plannedEndDate = null;
				String actualStartDate = null;
				String actualEndDate = null;

				int inProjectAutomationTestCaseEstimate = 0;
				int apiAutomationTestCaseEstimate = 0;
				int totalNumberOfTestCasesTaggedForMigrationExecution = 0;
				int totalPlannedEffort = 0;


				Iterable<LatestProjectEstimate> allLatestProjectEstimates = latestProjectEstimateRepository.findAll();
				Iterator<LatestProjectEstimate> latestProjectEstimateIterator = allLatestProjectEstimates.iterator();

				while (latestProjectEstimateIterator.hasNext()) {
					LatestProjectEstimate latestProjectEstimate = latestProjectEstimateIterator.next();

					if (latestProjectEstimate.getProject().getId() == id) {
						try{
							inProjectAutomationTestCaseEstimate = Integer.parseInt(latestProjectEstimate.getInProjectAutomationTestCaseEstimation());
							apiAutomationTestCaseEstimate = Integer.parseInt(latestProjectEstimate.getAPIAutomationTestCaseEstimation());
							plannedStartDate = latestProjectEstimate.getPlannedStartDate();
							plannedEndDate = latestProjectEstimate.getPlannedEndDate();
							totalNumberOfTestCasesTaggedForMigrationExecution = Integer.parseInt(latestProjectEstimate.getTotalNumberOfTestCasesTaggedForMigrationExecution());

							totalPlannedEffort += Integer.parseInt(latestProjectEstimate.getTestPlanningEffort());

							totalPlannedEffort += Integer.parseInt(latestProjectEstimate.getManualTestCaseDesignEffort());
							totalPlannedEffort += Integer.parseInt(latestProjectEstimate.getRegressionAutomationTestCaseDesignEffort());
							totalPlannedEffort += Integer.parseInt(latestProjectEstimate.getInProjectAutomationTestCaseDesignEffort());
							totalPlannedEffort += Integer.parseInt(latestProjectEstimate.getApiAutomationTestCaseDesignEffort());


							totalPlannedEffort += Integer.parseInt(latestProjectEstimate.getManualTestCaseExecutionEffort());
							totalPlannedEffort += Integer.parseInt(latestProjectEstimate.getRegressionAutomationTestCaseExecutionEffort());
							totalPlannedEffort += Integer.parseInt(latestProjectEstimate.getInProjectAutomationTestCaseExecutionEffort());
							totalPlannedEffort += Integer.parseInt(latestProjectEstimate.getApiAutomationTestCaseExecutionEffort());

							totalPlannedEffort += Integer.parseInt(latestProjectEstimate.getManualTestCaseMaintenanceEffort());
							totalPlannedEffort += Integer.parseInt(latestProjectEstimate.getAutomationTestCaseMaintenanceEffort());


							break;
						}catch(Exception e){
						}
					}
				}

				// ((Actual number of days - Planned number of days) /Planned number of days) * 100

				try {
					actualStartDate = nextMonthlyTestExecutionProgress.getActualStartDateMilestone();
					actualEndDate = nextMonthlyTestExecutionProgress.getActualEndDateMilestone();

					Date d1 = new SimpleDateFormat("dd-MM-yyyy").parse((String) plannedStartDate);
					Date d2 = new SimpleDateFormat("dd-MM-yyyy").parse((String) plannedEndDate);

					Date d3 = new SimpleDateFormat("dd-MM-yyyy").parse((String) actualStartDate);
					Date d4 = new SimpleDateFormat("dd-MM-yyyy").parse((String) actualEndDate);


					long diff1 = d2.getTime() - d1.getTime();            //  20th Jan 2020           10th Jan 2020
					diff1 = diff1 / (1000 * 60 * 60 * 24);   // Actual Start date - Planned Start Date
					//	20th Jan 2021          10th Jan 2021
					long diff2 = d4.getTime() - d3.getTime(); // Actual End date - Planned End Date
					diff2 = diff2 / (1000 * 60 * 60 * 24);


					Double diff1Double = new Double(diff1); // Planned
					Double diff2Double = new Double(diff2); // Actual

					// ((Actual number of days - Planned number of days)/Planned number of days) * 100

					Double temp = 0.0;

					try {
						temp = ((diff2Double - diff1Double) / (diff1Double)) * 100;
					} catch (Exception e) {
					}

					temp = (Math.round(temp * 100.0)) / 100.0;

					if (diff1 > 0) {
						temp = new Double((Math.round(temp * 100.0)) / 100.0);
						monthlyMetric.setScheduleVariance(temp.toString());

						ragStatus = checkRAGStatus(temp, id, monthlyMetric.getScheduleVarianceName());
						System.out.println("The rag status is : " + ragStatus);
						monthlyMetric.setScheduleVarianceStatus(ragStatus);
					} else {
						monthlyMetric.setScheduleVariance("0.0");
						monthlyMetric.setScheduleVarianceStatus("G");
					}
					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){
					System.out.println("Exception occurred during Monthly Metric schedule variance calculation : " + e.getMessage());
				}


				Iterable<LatestModuleEstimate> allLatestModuleEstimates = latestModuleEstimateRepository.findAll();
				Iterator<LatestModuleEstimate> latestModuleEstimateIterator = allLatestModuleEstimates.iterator();

				int totalEstimatedManualTestCases = 0;
				int totalEstimatedRegressionAutomationTestCases = 0;
				int totalEstimatedTestCases = 0;


				while (latestModuleEstimateIterator.hasNext()) {
					LatestModuleEstimate latestModulesEstimate = latestModuleEstimateIterator.next();

					if (latestModulesEstimate.getProject().getId() == id) {
						try{
							totalEstimatedManualTestCases += Integer.parseInt(latestModulesEstimate.getEstimatedManualTestcases());
							totalEstimatedRegressionAutomationTestCases += Integer.parseInt(latestModulesEstimate.getEstimatedRegressionAutomationTestCases());
						}catch(Exception e){
						}
					}
				}


				// Even the Migration test cases should be considered
				totalEstimatedTestCases = totalEstimatedManualTestCases +
						totalEstimatedRegressionAutomationTestCases +
						inProjectAutomationTestCaseEstimate +
						apiAutomationTestCaseEstimate;


				try {
					if (totalEstimatedTestCases > 0) {
						Double numberOfTestCasesTaggedForMigrationDouble = new Double(totalNumberOfTestCasesTaggedForMigrationExecution);
						Double totalEstimatedTestCasesDouble = new Double(totalEstimatedTestCases);

						Double temp = 0.0;

						try {
							temp = ((numberOfTestCasesTaggedForMigrationDouble) / totalEstimatedTestCasesDouble);
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							monthlyMetric.setMigrationExecutionCoverage("0.0");
						} else {
							System.out.println("% of test cases planned for migration testing - " + Math.round(temp * 100.0) / 100.0);
							temp = (Math.round(temp * 100.0)) / 100.0;
							monthlyMetric.setMigrationExecutionCoverage(temp.toString());
						}
					}

					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){

				}



				monthlyTestDesignProgressIterable = monthlyTestDesignProgressRepository.findAll();
				monthlyTestDesignProgressIterator = monthlyTestDesignProgressIterable.iterator();


				String numberOfManualTestCasesAuthoredInTheMonth = "";
				while (monthlyTestDesignProgressIterator.hasNext()) {
					MonthlyTestDesignProgress monthlyTestDesignProgress = monthlyTestDesignProgressIterator.next();

					if ((monthlyTestDesignProgress.getProject().getId() == id) &&
							(monthlyTestDesignProgress.getMonthNumber() == monthNumberInt) &&
							(monthlyTestDesignProgress.getYearNumber() == yearNumberInt))
					{
						numberOfManualTestCasesAuthoredInTheMonth = Integer.toString(monthlyTestDesignProgress.getTotalNumberOfManualTestCasesDesigned());
						break;
					}
				}

				monthlyMetric.setTotalNumberOfManualTestCasesDesigned(numberOfManualTestCasesAuthoredInTheMonth);
				monthlyMetricsRepository.save(monthlyMetric);

				monthlyMetric.setTotalNumberOfResourcesRelieved(Integer.toString(monthlyGovernanceNext.getTotalNumberOfResourcesRelieved()));
				monthlyMetricsRepository.save(monthlyMetric);

				//=IF('Input Sheet'!$F$29=0,"",ROUND(((('Input Sheet'!$F$29+'Input Sheet'!$F$202+'Input Sheet'!F46+'Input Sheet'!F47+'Input Sheet'!F48)-('Input Sheet'!$F$29+'Input Sheet'!F46+'Input Sheet'!F47+'Input Sheet'!F48))/('Input Sheet'!$F$29+'Input Sheet'!F46+'Input Sheet'!F47+'Input Sheet'!F48))*100,2))

				// As per formula - we should consider No of test cases for cost approved CR's (Cumulative Till Date) also
				// This is for scope variance

				try {
					if (totalEstimatedTestCases > 0) {
						Double approvedTestCasesCountDouble = new Double(approvedTestCasesCount);
						Double totalEstimatedTestCasesDouble = new Double(totalEstimatedTestCases);

						Double temp = 0.0;
						try {
							temp = (((approvedTestCasesCountDouble - totalEstimatedTestCasesDouble) / totalEstimatedTestCasesDouble) * 100);
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							monthlyMetric.setScopeVariance("0.0");
							monthlyMetric.setScopeVarianceStatus("0.0");
						} else {
							System.out.println("The scope variance is : " + Math.round(temp * 100.0) / 100.0);
							temp = (Math.round(temp * 100.0)) / 100.0;
							monthlyMetric.setScopeVariance(temp.toString());

							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getScopeVarianceName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setScopeVarianceStatus(ragStatus);
						}
					}

					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){
					System.out.println("Exception occurred during scope variance calculation : " + e.getMessage());
				}

				try {
					if (totalEstimatedTestCases > 0) {

						Double totalPlannedEffortDouble = new Double(totalPlannedEffort);
						Double totalEffortConsumedSoFarDouble = new Double(totalEffortConsumedSoFar);
						Double temp = 0.0;

						try {
							temp = temp = (((totalEffortConsumedSoFarDouble - totalPlannedEffortDouble) / totalPlannedEffortDouble) * 100);
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							monthlyMetric.setEffortVariance("0.0");
							monthlyMetric.setEffortVarianceStatus("0.0");
						} else {
							temp = (Math.round(temp * 100.0)) / 100.0;
							monthlyMetric.setEffortVariance(temp.toString());

							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getEffortVarianceName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setEffortVarianceStatus(ragStatus);
						}
					}
					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){
					System.out.println("Error occurred during effort variance calculation :" + e.getMessage());
				}

				// Cost per defect should consider the actual cost of the month and number of defects found in that month

				try {
					if (nextMonthlyTestExecutionProgress.getNumberOfValidDefectsLoggedInSIT() + nextMonthlyTestExecutionProgress.getNumberOfValidDefectsLoggedInUAT() > 0) {

						Double actualCostDouble = new Double(monthlyGovernanceNext.getActualCostForTheMonth());
						Double numberOfValidDefectsLoggedInSITDouble = new Double(nextMonthlyTestExecutionProgress.getNumberOfValidDefectsLoggedInSIT());
						Double numberOfValidDefectsLoggedInUATDouble = new Double(nextMonthlyTestExecutionProgress.getNumberOfValidDefectsLoggedInUAT());

						Double temp = 0.0;
						try {
							temp = ((actualCostDouble / (numberOfValidDefectsLoggedInSITDouble + numberOfValidDefectsLoggedInUATDouble)));
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							monthlyMetric.setCostPerDefect("0.0");
							monthlyMetric.setCostPerDefectStatus("0.0");

						} else {
							temp = Math.round(temp * 100.0) / 100.0;
							monthlyMetric.setCostPerDefect(temp.toString());
							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getCostPerDefectName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setCostPerDefectStatus(ragStatus);
						}


					}
					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){
					System.out.println("Error occurred suring cost per defect calculation :" + e.getMessage());
				}


				// Migration execution missed out
				// We should consider test cases executed in the month
				// Test cases executed in the previous months should not be considered
				int cummulativeNumberOfTestCasesExecutedSoFar =	cummulativeNumberOfManualTestCasesExecutedSofar +
						cummulativeNumberOfRegressionAutomationTestCasesExecutedSofar +
						cummulativeNumberOfInProjectAutomationTestScriptsExecutedSoFar +
						cummulativeNumberOfAPIAutomationTestScriptsExecutedSoFar;

				try {
					if (cummulativeNumberOfTestCasesExecutedSoFar > 0) {

						Double cummulativeNumberOfTestCasesExecutedSoFarDouble = new Double(cummulativeNumberOfTestCasesExecutedSoFar);
						Double actualCostDouble = new Double(monthlyGovernanceNext.getActualCostForTheMonth());

						Double temp = 0.0;
						try {
							temp = ((actualCostDouble / cummulativeNumberOfTestCasesExecutedSoFarDouble));
						} catch (Exception e) {
						}
						;

						temp = (Math.round(temp * 100.0)) / 100.0;
						if (temp == 0) {
							monthlyMetric.setCostPerTestCase("0.0");
							monthlyMetric.setCostPerTestCaseStatus("0.0");
						} else {
							temp = Math.round(temp * 100.0) / 100.0;
							monthlyMetric.setCostPerTestCase(temp.toString());

							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getCostPerTestCaseName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setCostPerTestCaseStatus(ragStatus);
						}

					}

					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){
					System.out.println("Exception occurred during cost per test case calculation : " + e.getMessage());
				}

				// As per migration coverage - only manual test cases are considered

				try {
					if (totalEstimatedTestCases > 0) {
						Double totalNumberOfTestCasesTaggedForMigrationExecutionDouble = new Double(totalNumberOfTestCasesTaggedForMigrationExecution);
						Double totalEstimatedTestCasesDouble = new Double(totalEstimatedTestCases);

						Double temp = 0.0;
						try {
							temp = (((totalNumberOfTestCasesTaggedForMigrationExecutionDouble * 100) / totalEstimatedTestCasesDouble));
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							monthlyMetric.setPercentageOfTCsIdentifiedForMigration("0.0");
							monthlyMetric.setPercentageOfTCsIdentifiedForMigrationStatus("0.0");
						} else {
							temp = (Math.round(temp * 100.0)) / 100.0;
							monthlyMetric.setPercentageOfTCsIdentifiedForMigration(temp.toString());

							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getPercentageOfTCsIdentifiedForMigrationName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setPercentageOfTCsIdentifiedForMigrationStatus(ragStatus);
						}

					}
					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){
					System.out.println("Error occurred during monthly metric percentage of test cases identified for migration calculation : " + e.getMessage());
				}

				System.out.println("Total number of In Project automation Test cases automated : " + cummulativeTotalNumberOfInProjectAutomationTestCasesAutomatedSoFar);
				System.out.println("In Project automation Test Case estimate are : " + inProjectAutomationTestCaseEstimate );

				try{
					if(inProjectAutomationTestCaseEstimate > 0) {

						Double cummulativeTotalNumberOfInProjectAutomationTestCasesAutomatedDouble = new Double(cummulativeTotalNumberOfInProjectAutomationTestCasesAutomatedSoFar);
						Double inProjectAutomationTestCaseEstimateDouble = new Double(inProjectAutomationTestCaseEstimate);

						Double temp = 0.0;
						try{temp = (((cummulativeTotalNumberOfInProjectAutomationTestCasesAutomatedDouble * 100)/ inProjectAutomationTestCaseEstimateDouble));}catch(Exception e){};

						temp = (Math.round(temp*100.0))/100.0;
						System.out.println("In Project Automation Percentage Extent To Potential : " + Math.round(temp*100.0)/100.0);

						if(temp == 0)
						{
							monthlyMetric.setInProjectAutomationPercentageExtentToPotential("0.0");
							monthlyMetric.setInProjectAutomationPercentageExtentToPotentialStatus("0.0");
						}
						else {
							monthlyMetric.setInProjectAutomationPercentageExtentToPotential(temp.toString());

							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getInProjectAutomationPercentageExtentToPotentialName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setInProjectAutomationPercentageExtentToPotentialStatus(ragStatus);
						}

					}
					monthlyMetricsRepository.save(monthlyMetric);

				}catch(Exception e){
					System.out.println("Error occurred during monthly metric InProject automation percentage extent to potential calculation : " + e.getMessage());
				}

				System.out.println("In Project Automation Percentage Extent To Potential : " + monthlyMetric.getInProjectAutomationPercentageExtentToPotential() );
				System.out.println("Total number of API automation Test cases automated : " + cummulativeTotalNumberOfAPIAutomationTestCasesAutomatedSoFar);
				System.out.println("API automation Test Case estimate are : " + apiAutomationTestCaseEstimate );


				try{
					if(apiAutomationTestCaseEstimate > 0) {

						Double cummulativeTotalNumberOfAPIAutomationTestCasesAutomatedDouble = new Double(cummulativeTotalNumberOfAPIAutomationTestCasesAutomatedSoFar);
						Double apiAutomationTestCaseEstimateDouble = new Double(apiAutomationTestCaseEstimate);

						Double temp = 0.0;
						try{temp = (((cummulativeTotalNumberOfAPIAutomationTestCasesAutomatedDouble * 100)/ apiAutomationTestCaseEstimateDouble));}catch(Exception e){};
						System.out.println("API Automation Percentage Extent To Potential : " + Math.round(temp*100.0)/100.0);


						if(temp == 0)
						{
							monthlyMetric.setApiAutomationPercentageExtentToPotential("0.0");
							monthlyMetric.setAPIAutomationPercentageExtentToPotentialStatus("0.0");
						}
						else {
							temp = (Math.round(temp * 100.0)) / 100.0;
							monthlyMetric.setApiAutomationPercentageExtentToPotential(temp.toString());

							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getApiAutomationPercentageExtentToPotentialName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setAPIAutomationPercentageExtentToPotentialStatus(ragStatus);
						}


					}
					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){
					System.out.println("Error occurred during monthly metric API automation percentage extent to potential calculation : " + e.getMessage());
				}


				System.out.println("API Automation Percentage Extent To Potential : " + monthlyMetric.getApiAutomationPercentageExtentToPotential() );


				// Why only manual test cases are considered
				try{
					if(nextMonthlyTestExecutionProgress.getTotalNumberOfManualSITTestCasesExecuted() > 0) {

						Double totalNumberOfDefectsLoggedInSITDouble = new Double(nextMonthlyTestExecutionProgress.getTotalNumberOfDefectsLoggedInSIT());
						Double totalNumberOfManualSITTestCasesExecutedDouble = new Double(nextMonthlyTestExecutionProgress.getTotalNumberOfManualSITTestCasesExecuted());

						Double temp = 0.0;
						try{temp = (((totalNumberOfDefectsLoggedInSITDouble )/ totalNumberOfManualSITTestCasesExecutedDouble) * 100);}catch(Exception e){};


						if(temp == 0)
						{
							monthlyMetric.setSITDefectDensity("0.0");
							monthlyMetric.setSITDefectDensityStatus(ragStatus);
						}
						else {
							temp = (Math.round(temp * 100.0)) / 100.0;

							monthlyMetric.setSITDefectDensity(temp.toString());
							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getSITDefectDensityName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setSITDefectDensityStatus(ragStatus);
						}

					}
					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){
					System.out.println("Error occurred during monthly metric SIT defect density calculation : " + e.getMessage());
				}


				// Why only manual test cases
				try {
					if (nextMonthlyTestExecutionProgress.getTotalNumberOfManualUATTestCasesExecuted() > 0) {
						Double totalNumberOfDefectsLoggedInUATDouble = new Double(nextMonthlyTestExecutionProgress.getTotalNumberOfDefectsLoggedInUAT());
						Double totalNumberOfManualUATTestCasesExecutedDouble = new Double(nextMonthlyTestExecutionProgress.getTotalNumberOfManualUATTestCasesExecuted());

						Double temp = 0.0;
						try {
							temp = (((totalNumberOfDefectsLoggedInUATDouble) / totalNumberOfManualUATTestCasesExecutedDouble) * 100);
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							monthlyMetric.setUATDefectDensity("0.0");
							monthlyMetric.setUATDefectDensityStatus("0.0");
						} else {
							temp = (Math.round(temp * 100.0)) / 100.0;
							monthlyMetric.setUATDefectDensity(temp.toString());

							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getUATDefectDensityName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setUATDefectDensityStatus(ragStatus);
						}

					}
					monthlyMetricsRepository.save(monthlyMetric);

				}catch(Exception e){
					System.out.println("Error occurred during monthly metric UAT defect density calculation : " + e.getMessage());
				}



				try{
					if(cummulativeNumberOfValidDefectsLoggedInSITSoFar > 0) {

						Double numberOfValidDefectsLoggedInUATDouble = new Double(cummulativeNumberOfValidDefectsLoggedInUATSoFar);
						Double numberOfValidDefectsLoggedInSITDouble = new Double(cummulativeNumberOfValidDefectsLoggedInSITSoFar);

						Double temp = 0.0;
						try{temp = numberOfValidDefectsLoggedInUATDouble / numberOfValidDefectsLoggedInSITDouble;}catch(Exception e){};

						if (temp == 0) {
							monthlyMetric.setDefectLeakageSITToUAT("0.0");
							monthlyMetric.setDefectLeakageSITToUATStatus("0.0");
						} else {
							temp = (Math.round(temp * 100.0)) / 100.0;
							monthlyMetric.setDefectLeakageSITToUAT(temp.toString());

							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getDefectLeakageSITToUATName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setDefectLeakageSITToUATStatus(ragStatus);
						}
					}
					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){
					System.out.println("Error occurred during Monthly metric DefectLeakageSITToUAT calculation " + e.getMessage());
				}

				try {
					if ((cummulativeNumberOfValidDefectsLoggedInSITSoFar > 0) || (cummulativeNumberOfValidDefectsLoggedInSITSoFar > 0)) {

						Double numberOfValidDefectsLoggedInUATDouble = new Double(cummulativeNumberOfValidDefectsLoggedInUATSoFar);
						Double numberOfValidDefectsLoggedInSITDouble = new Double(cummulativeNumberOfValidDefectsLoggedInSITSoFar);
						Double cummulativeTotalNumberOfDefectsInProductionDouble = new Double(cummulativeTotalNumberOfDefectsInProductionSoFar);

						Double temp = 0.0;
						try {
							temp = (cummulativeTotalNumberOfDefectsInProductionDouble / (numberOfValidDefectsLoggedInSITDouble + numberOfValidDefectsLoggedInUATDouble));
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							monthlyMetric.setDefectLeakageToProduction("0.0");
							monthlyMetric.setDefectLeakageToProductionStatus("0.0");

						} else {
							temp = (Math.round(temp * 100.0)) / 100.0;
							monthlyMetric.setDefectLeakageToProduction(temp.toString());

							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getDefectLeakageToProductionName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setDefectLeakageToProductionStatus(ragStatus);
						}
					}

					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){
					System.out.println("Error occurred during Monthly metric DefectLeakageToProduction calculation " + e.getMessage());
				}


				try {
					if ((cummulativeNumberOfDefectsFoundUsingManualRegression  > 0)) {

						Double cummulativeNumberOfDefectsFoundUsingManualRegressionDouble = new Double(cummulativeNumberOfDefectsFoundUsingManualRegression);
						Double cummulativeNumberOfDefectsFoundUsingAutomatedRegressionDouble = new Double(cummulativeNumberOfDefectsFoundUsingAutomatedRegression );

						Double temp = 0.0;
						try {
							temp = ((cummulativeNumberOfDefectsFoundUsingAutomatedRegressionDouble * 100) / (cummulativeNumberOfDefectsFoundUsingManualRegressionDouble));
						} catch (Exception e) {
						}
						;

						if (temp == 0) {
							monthlyMetric.setAutomationEffectiveness("0.0");
							monthlyMetric.setAutomationEffectivenessStatus("0.0");

						} else {
							temp = (Math.round(temp * 100.0)) / 100.0;
							monthlyMetric.setAutomationEffectiveness(temp.toString());

							ragStatus = checkRAGStatus(temp, id, monthlyMetric.getAutomationEffectivenessName());
							System.out.println("The rag status is : " + ragStatus);
							monthlyMetric.setAutomationEffectivenessStatus(ragStatus);
						}
					}

					monthlyMetricsRepository.save(monthlyMetric);
				}catch(Exception e){
					System.out.println("Error occurred during Monthly metric DefectLeakageToProduction calculation " + e.getMessage());
				}

				monthlyMetric.setNumberOfStepChangesProposed(Integer.toString(monthlyGovernanceNext.getNumberOfStepChangesProposed()));
				monthlyMetricsRepository.save(monthlyMetric);

				monthlyMetric.setNumberOfStepChangesInProgress(Integer.toString(monthlyGovernanceNext.getNumberOfStepChangesInProgress()));
				monthlyMetricsRepository.save(monthlyMetric);

				monthlyMetric.setNumberOfStepChangesImplemented(Integer.toString(monthlyGovernanceNext.getNumberOfStepChangesImplemented()));
				monthlyMetricsRepository.save(monthlyMetric);

				monthlyMetric.setGrossMargin(Integer.toString(monthlyGovernanceNext.getGrossMarginForTheMonth()));
				monthlyMetricsRepository.save(monthlyMetric);

				monthlyMetric.setTotalNumberOfResignationsSubmitted(Integer.toString(monthlyGovernanceNext.getTotalNumberOfResignationsSubmitted()));
				monthlyMetricsRepository.save(monthlyMetric);

				monthlyMetric.setTotalNumberOfResourcesRelieved(Integer.toString(monthlyGovernanceNext.getTotalNumberOfResourcesRelieved()));
				monthlyMetricsRepository.save(monthlyMetric);

				monthlyMetric.setNumberOfTestEngineers(Integer.toString(monthlyGovernanceNext.getNumberOfTestEngineers()));
				monthlyMetricsRepository.save(monthlyMetric);

				monthlyMetric.setNumberOfLeads(Integer.toString(monthlyGovernanceNext.getNumberOfLeads()));
				monthlyMetricsRepository.save(monthlyMetric);

				monthlyMetric.setNumberOfManagers(Integer.toString(monthlyGovernanceNext.getNumberOfManagers()));
				monthlyMetricsRepository.save(monthlyMetric);

				monthlyMetric.setTotalNumberOfTestCasesTaggedForMigrationExecution(Integer.toString(totalNumberOfTestCasesTaggedForMigrationExecution));
				monthlyMetricsRepository.save(monthlyMetric);

				monthlyMetric.setEstimatedTestCases(Integer.toString(totalEstimatedTestCases));
				monthlyMetricsRepository.save(monthlyMetric);


				cummulativeNumberOfTestCasesExecutedSoFar = 0;
				totalEffortConsumedSoFar = 0;
				totalPlannedEffort = 0;
			}


			Iterable<MonthlyMetrics> monthlyMetricsIterable = monthlyMetricsRepository.findAll();
			Iterator<MonthlyMetrics> monthlyMetricsIterableIterator = monthlyMetricsIterable.iterator();

			MonthlyMetrics monthlyMetric = null;

			while (monthlyMetricsIterableIterator.hasNext()) {
				monthlyMetric = monthlyMetricsIterableIterator.next();

				if ((monthlyMetric.getProject().getId() == id)) {

					String totalString = "{\"Metrics Measurements\":";

					totalString += monthlyMetric.getMonthNumber();
					totalString += ",\"";

					totalString += monthlyMetric.getYearNumberName();
					totalString += "\":";

					totalString += monthlyMetric.getYearNumber();
					totalString += ",\"";

					totalString += monthlyMetric.getCostVarianceName();
					totalString += "\":";

					totalString += monthlyMetric.getCostVariance();
					totalString += ",\"";

					totalString += monthlyMetric.getScheduleVarianceName();
					totalString += "\":";

					totalString += monthlyMetric.getScheduleVariance();
					totalString += ",\"";

					totalString += monthlyMetric.getScopeVarianceName();
					totalString += "\":";

					totalString += monthlyMetric.getScopeVariance();
					totalString += ",\"";

					totalString += monthlyMetric.getEffortVarianceName();
					totalString += "\":";

					totalString += monthlyMetric.getEffortVariance();
					totalString += ",\"";

					totalString += monthlyMetric.getCostPerDefectName();
					totalString += "\":";

					totalString += monthlyMetric.getCostPerDefect();
					totalString += ",\"";

					totalString += monthlyMetric.getCostPerTestCaseName();
					totalString += "\":";

					totalString += monthlyMetric.getCostPerTestCase();
					totalString += ",\"";

					totalString += monthlyMetric.getTotalNumberOfManualTestCasesDesignedName();
					totalString += "\":";

					totalString += monthlyMetric.getTotalNumberOfManualTestCasesDesigned();
					totalString += ",\"";

					totalString += monthlyMetric.getPercentageOfTCsIdentifiedForMigrationName();
					totalString += "\":";

					totalString += monthlyMetric.getPercentageOfTCsIdentifiedForMigration();
					totalString += ",\"";

					totalString += monthlyMetric.getGrossMarginName();
					totalString += "\":";

					totalString += monthlyMetric.getGrossMargin();
					totalString += ",\"";

					totalString += monthlyMetric.getTotalNumberOfResignationsSubmittedName();
					totalString += "\":";

					totalString += monthlyMetric.getTotalNumberOfResignationsSubmitted();
					totalString += ",\"";

					totalString += monthlyMetric.getTotalNumberOfResourcesRelievedName();
					totalString += "\":";

					totalString += monthlyMetric.getTotalNumberOfResourcesRelieved();
					totalString += ",\"";

					totalString += monthlyMetric.getSITDefectDensityName();
					totalString += "\":";

					totalString += monthlyMetric.getSITDefectDensity();
					totalString += ",\"";

					totalString += monthlyMetric.getUATDefectDensityName();
					totalString += "\":";

					totalString += monthlyMetric.getUATDefectDensity();
					totalString += ",\"";


					totalString += monthlyMetric.getNumberOfStepChangesInProgressName();
					totalString += "\":";

					totalString += monthlyMetric.getNumberOfStepChangesInProgress();
					totalString += ",\"";

					totalString += monthlyMetric.getNumberOfTestEngineersName();
					totalString += "\":";

					totalString += monthlyMetric.getNumberOfTestEngineers();
					totalString += ",\"";

					totalString += monthlyMetric.getNumberOfLeadsName();
					totalString += "\":";

					totalString += monthlyMetric.getNumberOfLeads();
					totalString += ",\"";

					totalString += monthlyMetric.getNumberOfManagersName();
					totalString += "\":";

					totalString += monthlyMetric.getNumberOfManagers();
					totalString += ",\"";

					totalString += monthlyMetric.getMigrationExecutionCoverageName();
					totalString += "\":";

					totalString += monthlyMetric.getMigrationExecutionCoverage();
					totalString += ",\"";

					totalString += monthlyMetric.getTotalNumberOfTestCasesTaggedForMigrationExecutionName();
					totalString += "\":";

					totalString += monthlyMetric.getTotalNumberOfTestCasesTaggedForMigrationExecution();
					totalString += ",\"";

					totalString += monthlyMetric.getEstimatedTestCasesName();
					totalString += "\":";

					totalString += monthlyMetric.getEstimatedTestCases();
					totalString += ",\"";

					totalString = addMetricsLCLUCLGoal(id, totalString);

					totalString += monthlyMetric.getNumberOfStepChangesImplementedName();
					totalString += "\":";

					totalString += monthlyMetric.getNumberOfStepChangesImplemented();
					totalString += "}";

					System.out.println("The monthly metrics is : " + totalString);

					BufferedWriter bw = null;
					FileWriter fw = null;

					try {
						fw = new FileWriter("Metrics\\Monthly\\Month_" +  monthlyMetric.getMonthNumber() + "_Metrics.json");
						bw = new BufferedWriter((fw));
						bw.write(totalString);
						bw.close();
						fw.close();
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}

					try {

						HttpHost host = new HttpHost("localhost", 9200);
						final RestClientBuilder builder = RestClient.builder(host);
						client = new RestHighLevelClient(builder);
						save(Integer.toString(monthlyMetric.getMonthNumber()), totalString, MONTHLY_INDEX);
						client.indices().refresh(new RefreshRequest(MONTHLY_INDEX), RequestOptions.DEFAULT);
						client.close();

					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
				}
			}

		} catch(Exception e){
			System.out.println("Exception occured during montly metric generation : " + e.getMessage());
		}

		return "OK";
	}


	@RequestMapping(value = "/projects/{id}/fetchWeeklyMetrics", method = RequestMethod.GET)
	@ResponseBody
	public String fetchWeeklyMetrics(@PathVariable("id") long id) {

		ArrayList<WeeklyMetricsPojo> weeklyMetricsPojosArray = new ArrayList<WeeklyMetricsPojo>();
		Iterable<WeeklyMetrics> weeklyMetricsIterable = weeklyMetricsRepo.findAll();
		Iterator<WeeklyMetrics> weeklyMetricsIterator = weeklyMetricsIterable.iterator();

		WeeklyMetrics weeklyMetric = null;
		String totalresponse = "{\"WeeklyMetrics\":[";
		String nullArrayResponse = "[null]";
		String nullArrayResponse2 = "[]";

		String nullResponse = "{\"WeeklyMetrics\":null}";

		while (weeklyMetricsIterator.hasNext()) {
			weeklyMetric = weeklyMetricsIterator.next();

			if (weeklyMetric.getProject().getId() == id){

				WeeklyMetricsPojo weeklyMetricsPojo = new WeeklyMetricsPojo();
				weeklyMetricsPojo.setWeekNumber(weeklyMetric.getWeekNumber());
				weeklyMetricsPojo.setManualTestCaseAuthoringProductivity(weeklyMetric.getManualTestCaseAuthoringProductivity() + "-" +
						weeklyMetric.getManualTestCaseAuthoringProductivityStatus());
				weeklyMetricsPojo.setRegressionTestScriptingProductivity(weeklyMetric.getRegressionTestAutomationScriptingProductivity()+ "-" +
						weeklyMetric.getRegressionTestAutomationScriptingProductivityStatus());
				weeklyMetricsPojo.setDesignCoverage(weeklyMetric.getDesignCoverage()+ "-" +
						weeklyMetric.getDesignCoverageStatus());
				weeklyMetricsPojo.setPercentageOfRegressionAutomation(weeklyMetric.getPercentageOfRegressionAutomation()+ "-" +
						weeklyMetric.getPercentageOfRegressionAutomationStatus());;
				weeklyMetricsPojo.setTestReviewEfficiency(weeklyMetric.getTestReviewEfficiency()+ "-" +
						weeklyMetric.getTestReviewEfficiencyStatus());
				weeklyMetricsPojo.setDowntimeAndImpactAnalysisDesignAvgProductivityLost(weeklyMetric.getDowntimeAndImpactAnalysisDesignAvgProductivityLost()+ "-" +
						weeklyMetric.getDowntimeAndImpactAnalysisDesignAvgProductivityLostStatus());
				weeklyMetricsPojo.setManualTestExecutionProductivity(weeklyMetric.getManualTestExecutionProductivity()+ "-" +
						weeklyMetric.getManualTestExecutionProductivityStatus());
				weeklyMetricsPojo.setRegressionAutomationTestExecutionProductivity(weeklyMetric.getRegressionAutomationTestExecutionProductivity()+ "-" +
						weeklyMetric.getRegressionAutomationTestExecutionProductivityStatus());
				weeklyMetricsPojo.setInProjectAutomationTestExecutionProductivity(weeklyMetric.getInProjectAutomationTestExecutionProductivity()+ "-" +
						weeklyMetric.getInProjectAutomationTestExecutionProductivityStatus());
				weeklyMetricsPojo.setAPIAutomationTestExecutionProductivity(weeklyMetric.getAPIAutomationTestExecutionProductivity()+ "-" +
						weeklyMetric.getAPIAutomationTestExecutionProductivityStatus());
				weeklyMetricsPojo.setMigratedDataExecution(weeklyMetric.getMigratedDataExecution()+ "-" +
						weeklyMetric.getMigratedDataExecutionStatus());
				weeklyMetricsPojo.setDefectRejection(weeklyMetric.getDefectRejection()+ "-" +
						weeklyMetric.getDefectRejectionStatus());
				weeklyMetricsPojo.setTestCaseNotExecutedManual(weeklyMetric.getTestCaseNotExecutedManual()+ "-" +
						weeklyMetric.getTestCaseNotExecutedManualStatus());
				weeklyMetricsPojo.setTestCaseNotExecutedAutomation(weeklyMetric.getTestCaseNotExecutedAutomation()+ "-" +
						weeklyMetric.getTestCaseNotExecutedAutomationStatus());
				weeklyMetricsPojo.setCurrentQualityRatio(weeklyMetric.getCurrentQualityRatio()+ "-" +
						weeklyMetric.getCurrentQualityRatioStatus());
				weeklyMetricsPojo.setQualityOfFixes(weeklyMetric.getQualityOfFixes()+ "-" +
						weeklyMetric.getQualityOfFixesStatus());
				weeklyMetricsPojo.setDowntimeAndImpactAnalysisExecutionAvgProductivityLost(weeklyMetric.getDowntimeAndImpactAnalysisExecutionAvgProductivityLost()+ "-" +
						weeklyMetric.getDowntimeAndImpactAnalysisExecutionAvgProductivityLostStatus());
				weeklyMetricsPojo.setNumberOfTestableRequirementsTillDate(weeklyMetric.getNumberOfTestableRequirementsTillDate());

				weeklyMetricsPojosArray.add(weeklyMetricsPojo);
			}
		}

		Gson gson = new Gson();
		String response = gson.toJson(weeklyMetricsPojosArray);
		response = "{\"WeeklyMetrics\":" + response + "}";
		System.out.println(response);

		if((response.contains(nullArrayResponse)) || (response.contains(nullArrayResponse2)))
		{
			return nullResponse;
		}

		return response;
	}

	@RequestMapping(value = "/projects/{id}/fetchWeeklyMetricsData", method = RequestMethod.GET)
	@ResponseBody
	public String fetchWeeklyMetrics2(@PathVariable("id") long id) {

		int manualTestCaseAuthoringProductivity = 0;
		int regressionTestScriptingProductivity = 0;
		int designCoverage = 0;
		int percentageOfRegressionAutomation = 0;
		int testReviewEfficiency = 0;
		int downtimeAndImpactAnalysisDesignAvgProductivityLost = 0;
		int manualTestExecutionProductivity = 0;
		int regressionAutomationTestExecutionProductivity = 0;
		int inProjectAutomationTestExecutionProductivity = 0;
		int apiAutomationTestExecutionProductivity = 0;
		int migratedDataExecution = 0;
		int testCaseNotExecutedManual = 0;
		int testCaseNotExecutedAutomation = 0;
		int currentQualityRatio = 0;
		int qualityOfFixes = 0;
		int defectRejection = 0;
		int SIT2UATDefectLeakage = 0;
		int UAT2PRODDefectLeakage = 0;
		int downtimeAndImpactAnalysisExecutionAvgProductivityLost = 0;

		Iterable<HeatMapSettings> heatMapSettingsIterable = heatMapSettingsRepository.findAll();
		Iterator<HeatMapSettings> heatMapSettingsIterator = heatMapSettingsIterable.iterator();
		ArrayList<HeatMapSettingsPojo> heatMapSettingsPojoArray = new ArrayList<HeatMapSettingsPojo>();

		while (heatMapSettingsIterator.hasNext()) {
			HeatMapSettings next = heatMapSettingsIterator.next();

			if (next.getProject().getId() == id) {

				try {
					HeatMapSettingsPojo heatMapSettingsPojo = new HeatMapSettingsPojo();
					heatMapSettingsPojo.setMetricName(next.getMetricName());
					heatMapSettingsPojo.setLCL(next.getLCL());
					heatMapSettingsPojo.setUCL(next.getUCL());
					heatMapSettingsPojo.setGoal(next.getGoal());
					heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				} catch (Exception e) {

				}
			}
		}

		String goal = null;

		Iterable<WeeklyMetrics> weeklyMetricsIterable = weeklyMetricsRepo.findAll();
		Iterator<WeeklyMetrics> weeklyMetricsIterator = weeklyMetricsIterable.iterator();

		WeeklyMetrics weeklyMetric = null;

		String totalresponse = "{\"WeeklyMetrics\":[";
		String nullArrayResponse = "[null]";
		String nullArrayResponse2 = "[]";

		String nullResponse = "{\"WeeklyMetrics\":null}";

		String temptotalResponse = null;
		while (weeklyMetricsIterator.hasNext()) {
			weeklyMetric = weeklyMetricsIterator.next();

			System.out.println("The desired project id is : " + id);
			System.out.println("The current project id is : " + weeklyMetric.getProject().getId());

			if (weeklyMetric.getProject().getId() == id) {
				System.out.println("Inside matching project weekly metrics : " + weeklyMetric.getProject().getId());


				//String response = "{\"weekNumber\":" + weeklyMetric.getWeekNumber() + "," + "\"data\": [{";
				ArrayList<WeeklyMetricsDataPojo> weeklyMetricsPojosArray = new ArrayList<WeeklyMetricsDataPojo>();

				String response = "{\"yearNumber\":" + weeklyMetric.getYearNumber() + "," ;
				response = response + "\"weekNumber\":" + weeklyMetric.getWeekNumber() + "," + "\"data\": ";

				int index = 1;
				WeeklyMetricsDataPojo weeklyMetricsPojo = new WeeklyMetricsDataPojo();
				try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
				try{weeklyMetricsPojo.setName("manualTestCaseAuthoringProductivity");}catch(Exception e){}
				try { goal = getGoal(heatMapSettingsPojoArray,"manualTestCaseAuthoringProductivity"); }catch(Exception e){}
				try{manualTestCaseAuthoringProductivity += Integer.parseInt(weeklyMetric.getManualTestCaseAuthoringProductivity()); }catch(Exception e){}
				try{weeklyMetricsPojo.setValue(weeklyMetric.getManualTestCaseAuthoringProductivity() + "(" + goal + ")");}catch(Exception e){}
				try{weeklyMetricsPojo.setCategory(weeklyMetric.getManualTestCaseAuthoringProductivityStatus());}catch(Exception e){}
				try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

				index += 1;
				try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
				try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
				try{weeklyMetricsPojo.setName("regressionTestScriptingProductivity");}catch(Exception e){}
				try { goal = getGoal(heatMapSettingsPojoArray,"regressionTestScriptingProductivity"); }catch(Exception e){}
				try{regressionTestScriptingProductivity += Integer.parseInt(weeklyMetric.getRegressionTestAutomationScriptingProductivity()); }catch(Exception e){}
				try{weeklyMetricsPojo.setValue(weeklyMetric.getRegressionTestAutomationScriptingProductivity() + "(" + goal + ")");}catch(Exception e){}
				try{weeklyMetricsPojo.setCategory(weeklyMetric.getRegressionTestAutomationScriptingProductivityStatus());}catch(Exception e){}
				try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

				index += 1;
				try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
				try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
				try{weeklyMetricsPojo.setName("designCoverage");}catch(Exception e){}
				try { goal = getGoal(heatMapSettingsPojoArray,"designCoverage"); }catch(Exception e){}
				try{designCoverage += Integer.parseInt(weeklyMetric.getDesignCoverage()); }catch(Exception e){}
				try{weeklyMetricsPojo.setValue(weeklyMetric.getDesignCoverage() + "(" + goal + ")");}catch(Exception e){}
				try{weeklyMetricsPojo.setCategory(weeklyMetric.getDesignCoverageStatus());}catch(Exception e){}
				try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}


				index += 1;
				try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
				try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
				try{weeklyMetricsPojo.setName("percentageOfRegressionAutomation");}catch(Exception e){}
				try { goal = getGoal(heatMapSettingsPojoArray,"percentageOfRegressionAutomation"); }catch(Exception e){}
				try{percentageOfRegressionAutomation += Integer.parseInt(weeklyMetric.getPercentageOfRegressionAutomation()); }catch(Exception e){}
				try{weeklyMetricsPojo.setValue(weeklyMetric.getPercentageOfRegressionAutomation() + "(" + goal + ")");}catch(Exception e){}
				try{weeklyMetricsPojo.setCategory(weeklyMetric.getPercentageOfRegressionAutomationStatus());}catch(Exception e){}
				try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}


				index += 1;
				try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
				try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
				try{weeklyMetricsPojo.setName("testReviewEfficiency");}catch(Exception e){}
				try { goal = getGoal(heatMapSettingsPojoArray,"testReviewEfficiency"); }catch(Exception e){}
				try{testReviewEfficiency += Integer.parseInt(weeklyMetric.getTestReviewEfficiency()); }catch(Exception e){}
				try{weeklyMetricsPojo.setValue(weeklyMetric.getTestReviewEfficiency() + "(" + goal + ")");}catch(Exception e){}
				try{weeklyMetricsPojo.setCategory(weeklyMetric.getTestReviewEfficiencyStatus());}catch(Exception e){}
				try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}


				index += 1;
				try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
				try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
				try{weeklyMetricsPojo.setName("downtimeAndImpactAnalysisDesignAvgProductivityLost");}catch(Exception e){}
				try { goal = getGoal(heatMapSettingsPojoArray,"downtimeAndImpactAnalysisDesignAvgProductivityLost"); }catch(Exception e){}
				try{downtimeAndImpactAnalysisDesignAvgProductivityLost += Integer.parseInt(weeklyMetric.getDowntimeAndImpactAnalysisDesignAvgProductivityLost()); }catch(Exception e){}
				try{weeklyMetricsPojo.setValue(weeklyMetric.getDowntimeAndImpactAnalysisDesignAvgProductivityLost() + "(" + goal + ")");}catch(Exception e){}
				try{weeklyMetricsPojo.setCategory(weeklyMetric.getDowntimeAndImpactAnalysisDesignAvgProductivityLostStatus());}catch(Exception e){}
				try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

				index += 1;
				try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
				try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
				try{weeklyMetricsPojo.setName("manualTestExecutionProductivity");}catch(Exception e){}
				try { goal = getGoal(heatMapSettingsPojoArray,"manualTestExecutionProductivity"); }catch(Exception e){}
				try{manualTestExecutionProductivity += Integer.parseInt(weeklyMetric.getManualTestExecutionProductivity()); }catch(Exception e){}
				System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
				System.out.println("manualTestExecutionProductivity|||||||||||||||||||||||||||||||||||||||||||||||||||");
				System.out.println(weeklyMetric.getManualTestExecutionProductivity());
				System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
				try{weeklyMetricsPojo.setValue(weeklyMetric.getManualTestExecutionProductivity() + "(" + goal + ")");}catch(Exception e){}
				try{weeklyMetricsPojo.setCategory(weeklyMetric.getManualTestExecutionProductivityStatus());}catch(Exception e){}
				try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}


				index += 1;
				try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
				try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
				try{weeklyMetricsPojo.setName("regressionAutomationTestExecutionProductivity");}catch(Exception e){}
				try { goal = getGoal(heatMapSettingsPojoArray,"regressionAutomationTestExecutionProductivity"); }catch(Exception e){}
				try{regressionAutomationTestExecutionProductivity += Integer.parseInt(weeklyMetric.getRegressionAutomationTestExecutionProductivity()); }catch(Exception e){}
				try{weeklyMetricsPojo.setValue(weeklyMetric.getRegressionAutomationTestExecutionProductivity() + "(" + goal + ")");}catch(Exception e){}
				try{weeklyMetricsPojo.setCategory(weeklyMetric.getRegressionAutomationTestExecutionProductivityStatus());}catch(Exception e){}
				try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

				index += 1;
				try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
				try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
				try{weeklyMetricsPojo.setName("inProjectAutomationTestExecutionProductivity");}catch(Exception e){}
				try { goal = getGoal(heatMapSettingsPojoArray,"inProjectAutomationTestExecutionProductivity"); }catch(Exception e){}
				try{inProjectAutomationTestExecutionProductivity += Integer.parseInt(weeklyMetric.getInProjectAutomationTestExecutionProductivity()); }catch(Exception e){}
				try{weeklyMetricsPojo.setValue(weeklyMetric.getInProjectAutomationTestExecutionProductivity() + "(" + goal + ")");}catch(Exception e){}
				try{weeklyMetricsPojo.setCategory(weeklyMetric.getInProjectAutomationTestExecutionProductivityStatus());}catch(Exception e){}
				try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

				index += 1;
				try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
				try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
				try{weeklyMetricsPojo.setName("apiAutomationTestExecutionProductivity");}catch(Exception e){}
				try { goal = getGoal(heatMapSettingsPojoArray,"apiAutomationTestExecutionProductivity"); }catch(Exception e){}
				try{apiAutomationTestExecutionProductivity += Integer.parseInt(weeklyMetric.getAPIAutomationTestExecutionProductivity()); }catch(Exception e){}
				try{weeklyMetricsPojo.setValue(weeklyMetric.getAPIAutomationTestExecutionProductivity() + "(" + goal + ")");}catch(Exception e){}
				try{weeklyMetricsPojo.setCategory(weeklyMetric.getAPIAutomationTestExecutionProductivityStatus());}catch(Exception e){}
				try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

				index += 1;
				try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
				try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
				try{weeklyMetricsPojo.setName("migratedDataExecution");}catch(Exception e){}
				try { goal = getGoal(heatMapSettingsPojoArray,"migratedDataExecution"); }catch(Exception e){}
				try{migratedDataExecution += Integer.parseInt(weeklyMetric.getMigratedDataExecution()); }catch(Exception e){}
				try{weeklyMetricsPojo.setValue(weeklyMetric.getMigratedDataExecution() + "(" + goal + ")");}catch(Exception e){}
				try{weeklyMetricsPojo.setCategory(weeklyMetric.getMigratedDataExecutionStatus());}catch(Exception e){}
				try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}


				index += 1;
				try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
				try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
				try{weeklyMetricsPojo.setName("testCaseNotExecutedManual");}catch(Exception e){}
				try { goal = getGoal(heatMapSettingsPojoArray,"testCaseNotExecutedManual"); }catch(Exception e){}
				try{testCaseNotExecutedManual += Integer.parseInt(weeklyMetric.getTestCaseNotExecutedManual()); }catch(Exception e){}
				try{weeklyMetricsPojo.setValue(weeklyMetric.getTestCaseNotExecutedManual() + "(" + goal + ")");}catch(Exception e){}
				try{weeklyMetricsPojo.setCategory(weeklyMetric.getTestCaseNotExecutedManualStatus());}catch(Exception e){}
				try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

				index += 1;
				try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
				try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
				try{weeklyMetricsPojo.setName("testCaseNotExecutedAutomation");}catch(Exception e){}
				try { goal = getGoal(heatMapSettingsPojoArray,"testCaseNotExecutedAutomation"); }catch(Exception e){}
				try{testCaseNotExecutedAutomation += Integer.parseInt(weeklyMetric.getTestCaseNotExecutedAutomation()); }catch(Exception e){}
				try{weeklyMetricsPojo.setValue(weeklyMetric.getTestCaseNotExecutedAutomation() + "(" + goal + ")");}catch(Exception e){}
				try{weeklyMetricsPojo.setCategory(weeklyMetric.getTestCaseNotExecutedAutomationStatus());}catch(Exception e){}
				try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

				index += 1;
				try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
				try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
				try{weeklyMetricsPojo.setName("currentQualityRatio");}catch(Exception e){}
				try { goal = getGoal(heatMapSettingsPojoArray,"currentQualityRatio"); }catch(Exception e){}
				try{currentQualityRatio += Integer.parseInt(weeklyMetric.getCurrentQualityRatio()); }catch(Exception e){}
				try{weeklyMetricsPojo.setValue(weeklyMetric.getCurrentQualityRatio() + "(" + goal + ")");}catch(Exception e){}
				try{weeklyMetricsPojo.setCategory(weeklyMetric.getCurrentQualityRatioStatus());}catch(Exception e){}
				try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}


				index += 1;
				try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
				try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
				try{weeklyMetricsPojo.setName("qualityOfFixes");}catch(Exception e){}
				try { goal = getGoal(heatMapSettingsPojoArray,"qualityOfFixes"); }catch(Exception e){}
				try{qualityOfFixes += Integer.parseInt(weeklyMetric.getQualityOfFixes()); }catch(Exception e){}
				try{weeklyMetricsPojo.setValue(weeklyMetric.getQualityOfFixes() + "(" + goal + ")");}catch(Exception e){}
				try{weeklyMetricsPojo.setCategory(weeklyMetric.getQualityOfFixesStatus());}catch(Exception e){}
				try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

				index += 1;
				try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
				try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
				try{weeklyMetricsPojo.setName("defectRejection");}catch(Exception e){}
				try { goal = getGoal(heatMapSettingsPojoArray,"defectRejection"); }catch(Exception e){}
				try{defectRejection += Integer.parseInt(weeklyMetric.getDefectRejection()); }catch(Exception e){}
				try{weeklyMetricsPojo.setValue(weeklyMetric.getDefectRejection() + "(" + goal + ")");}catch(Exception e){}
				try{weeklyMetricsPojo.setCategory(weeklyMetric.getDefectRejectionStatus());}catch(Exception e){}
				try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

				index += 1;
				try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
				try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
				try{weeklyMetricsPojo.setName("SIT2UATDefectLeakage");}catch(Exception e){}
				try { goal = getGoal(heatMapSettingsPojoArray,"SIT2UATDefectLeakage"); }catch(Exception e){}
				try{SIT2UATDefectLeakage += Integer.parseInt(weeklyMetric.getDefectLeakageSITToUAT()); }catch(Exception e){}
				try{weeklyMetricsPojo.setValue(weeklyMetric.getDefectLeakageSITToUAT() + "(" + goal + ")");}catch(Exception e){}
				try{weeklyMetricsPojo.setCategory(weeklyMetric.getDefectLeakageSITToUATStatus());}catch(Exception e){}
				try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

				index += 1;
				try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
				try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
				try{weeklyMetricsPojo.setName("UAT2PRODDefectLeakage");}catch(Exception e){}
				try { goal = getGoal(heatMapSettingsPojoArray,"SIT2UATDefectLeakage"); }catch(Exception e){}
				try{UAT2PRODDefectLeakage += Integer.parseInt(weeklyMetric.getDefectLeakageToProduction()); }catch(Exception e){}
				try{weeklyMetricsPojo.setValue(weeklyMetric.getDefectLeakageToProduction() + "(" + goal + ")");}catch(Exception e){}
				try{weeklyMetricsPojo.setCategory(weeklyMetric.getDefectLeakageToProductionStatus());}catch(Exception e){}
				try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

				index += 1;
				try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
				try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
				try{weeklyMetricsPojo.setName("downtimeAndImpactAnalysisExecutionAvgProductivityLost");}catch(Exception e){}
				try { goal = getGoal(heatMapSettingsPojoArray,"downtimeAndImpactAnalysisExecutionAvgProductivityLost"); }catch(Exception e){}
				try{downtimeAndImpactAnalysisExecutionAvgProductivityLost += Integer.parseInt(weeklyMetric.getDowntimeAndImpactAnalysisExecutionAvgProductivityLost()); }catch(Exception e){}
				try{weeklyMetricsPojo.setValue(weeklyMetric.getDowntimeAndImpactAnalysisExecutionAvgProductivityLost());}catch(Exception e){}
				try{weeklyMetricsPojo.setCategory(weeklyMetric.getDowntimeAndImpactAnalysisExecutionAvgProductivityLostStatus());}catch(Exception e){}
				try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

/*				index += 1;
				try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
				try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
				try{weeklyMetricsPojo.setName("numberOfTestableRequirementsTillDate");}catch(Exception e){}
				try{weeklyMetricsPojo.setValue(weeklyMetric.getNumberOfTestableRequirementsTillDate());}catch(Exception e){}
				try{weeklyMetricsPojo.setCategory("G");}catch(Exception e){}
				try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}
*/

				Gson gson = new Gson();

				if(temptotalResponse == null)
				{
					temptotalResponse = response + gson.toJson(weeklyMetricsPojosArray) + "}";
					System.out.println("(1) Weekly Metrics Response : " + temptotalResponse);
				}
				else {
					temptotalResponse =  response + gson.toJson(weeklyMetricsPojosArray) + "}," + temptotalResponse;
					System.out.println("(2) Weekly Metrics Response : " + temptotalResponse);

				}
			}
		}
		//temptotalResponse = temptotalResponse.substring(0,temptotalResponse.length()-1);
		totalresponse = totalresponse + temptotalResponse + "]}";
		System.out.println(totalresponse);

		ArrayList<WeeklyMetricsDataPojo> weeklyMetricsPojosArray = new ArrayList<WeeklyMetricsDataPojo>();

		int index = 1;
		WeeklyMetricsDataPojo weeklyMetricsPojo = new WeeklyMetricsDataPojo();
		try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
		try{weeklyMetricsPojo.setName("manualTestCaseAuthoringProductivity");}catch(Exception e){}
		try { goal = getGoal(heatMapSettingsPojoArray,"manualTestCaseAuthoringProductivity"); }catch(Exception e){}
		try{weeklyMetricsPojo.setValue(manualTestCaseAuthoringProductivity + "(" + goal + ")");}catch(Exception e){}
		try{weeklyMetricsPojo.setCategory(weeklyMetric.getManualTestCaseAuthoringProductivityStatus());}catch(Exception e){}
		try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

		index += 1;
		try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
		try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
		try{weeklyMetricsPojo.setName("regressionTestScriptingProductivity");}catch(Exception e){}
		try { goal = getGoal(heatMapSettingsPojoArray,"regressionTestScriptingProductivity"); }catch(Exception e){}
		try{weeklyMetricsPojo.setValue(regressionTestScriptingProductivity + "(" + goal + ")");}catch(Exception e){}
		try{weeklyMetricsPojo.setCategory(weeklyMetric.getRegressionTestAutomationScriptingProductivityStatus());}catch(Exception e){}
		try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

		index += 1;
		try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
		try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
		try{weeklyMetricsPojo.setName("designCoverage");}catch(Exception e){}
		try { goal = getGoal(heatMapSettingsPojoArray,"designCoverage"); }catch(Exception e){}
		try{weeklyMetricsPojo.setValue(designCoverage + "(" + goal + ")");}catch(Exception e){}
		try{weeklyMetricsPojo.setCategory(weeklyMetric.getDesignCoverageStatus());}catch(Exception e){}
		try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}


		index += 1;
		try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
		try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
		try{weeklyMetricsPojo.setName("percentageOfRegressionAutomation");}catch(Exception e){}
		try { goal = getGoal(heatMapSettingsPojoArray,"percentageOfRegressionAutomation"); }catch(Exception e){}
		try{weeklyMetricsPojo.setValue(percentageOfRegressionAutomation + "(" + goal + ")");}catch(Exception e){}
		try{weeklyMetricsPojo.setCategory(weeklyMetric.getPercentageOfRegressionAutomationStatus());}catch(Exception e){}
		try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}


		index += 1;
		try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
		try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
		try{weeklyMetricsPojo.setName("testReviewEfficiency");}catch(Exception e){}
		try { goal = getGoal(heatMapSettingsPojoArray,"testReviewEfficiency"); }catch(Exception e){}
		try{weeklyMetricsPojo.setValue(testReviewEfficiency + "(" + goal + ")");}catch(Exception e){}
		try{weeklyMetricsPojo.setCategory(weeklyMetric.getTestReviewEfficiencyStatus());}catch(Exception e){}
		try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}


		index += 1;
		try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
		try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
		try{weeklyMetricsPojo.setName("downtimeAndImpactAnalysisDesignAvgProductivityLost");}catch(Exception e){}
		try { goal = getGoal(heatMapSettingsPojoArray,"downtimeAndImpactAnalysisDesignAvgProductivityLost"); }catch(Exception e){}
		try{weeklyMetricsPojo.setValue(downtimeAndImpactAnalysisDesignAvgProductivityLost + "(" + goal + ")");}catch(Exception e){}
		try{weeklyMetricsPojo.setCategory(weeklyMetric.getDowntimeAndImpactAnalysisDesignAvgProductivityLostStatus());}catch(Exception e){}
		try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}


		index += 1;
		try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
		try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
		try{weeklyMetricsPojo.setName("manualTestExecutionProductivity");}catch(Exception e){}
		try { goal = getGoal(heatMapSettingsPojoArray,"manualTestExecutionProductivity"); }catch(Exception e){}
		try{manualTestExecutionProductivity += Integer.parseInt(weeklyMetric.getManualTestExecutionProductivity()); }catch(Exception e){}
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		System.out.println("manualTestExecutionProductivity|||||||||||||||||||||||||||||||||||||||||||||||||||");
		System.out.println(manualTestExecutionProductivity);
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		try{weeklyMetricsPojo.setValue(manualTestExecutionProductivity + "(" + goal + ")");}catch(Exception e){}
		try{weeklyMetricsPojo.setCategory(weeklyMetric.getManualTestExecutionProductivityStatus());}catch(Exception e){}
		try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

		index += 1;
		try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
		try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
		try{weeklyMetricsPojo.setName("regressionAutomationTestExecutionProductivity");}catch(Exception e){}
		try { goal = getGoal(heatMapSettingsPojoArray,"regressionAutomationTestExecutionProductivity"); }catch(Exception e){}
		try{regressionAutomationTestExecutionProductivity += Integer.parseInt(weeklyMetric.getRegressionAutomationTestExecutionProductivity()); }catch(Exception e){}
		try{weeklyMetricsPojo.setValue(regressionAutomationTestExecutionProductivity + "(" + goal + ")");}catch(Exception e){}
		try{weeklyMetricsPojo.setCategory(weeklyMetric.getRegressionAutomationTestExecutionProductivityStatus());}catch(Exception e){}
		try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

		index += 1;
		try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
		try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
		try{weeklyMetricsPojo.setName("inProjectAutomationTestExecutionProductivity");}catch(Exception e){}
		try { goal = getGoal(heatMapSettingsPojoArray,"inProjectAutomationTestExecutionProductivity"); }catch(Exception e){}
		try{inProjectAutomationTestExecutionProductivity += Integer.parseInt(weeklyMetric.getInProjectAutomationTestExecutionProductivity()); }catch(Exception e){}
		try{weeklyMetricsPojo.setValue(inProjectAutomationTestExecutionProductivity + "(" + goal + ")");}catch(Exception e){}
		try{weeklyMetricsPojo.setCategory(weeklyMetric.getInProjectAutomationTestExecutionProductivityStatus());}catch(Exception e){}
		try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

		index += 1;
		try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
		try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
		try{weeklyMetricsPojo.setName("apiAutomationTestExecutionProductivity");}catch(Exception e){}
		try { goal = getGoal(heatMapSettingsPojoArray,"apiAutomationTestExecutionProductivity"); }catch(Exception e){}
		try{apiAutomationTestExecutionProductivity += Integer.parseInt(weeklyMetric.getAPIAutomationTestExecutionProductivity()); }catch(Exception e){}
		try{weeklyMetricsPojo.setValue(apiAutomationTestExecutionProductivity + "(" + goal + ")");}catch(Exception e){}
		try{weeklyMetricsPojo.setCategory(weeklyMetric.getAPIAutomationTestExecutionProductivityStatus());}catch(Exception e){}
		try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

		index += 1;
		try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
		try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
		try{weeklyMetricsPojo.setName("migratedDataExecution");}catch(Exception e){}
		try { goal = getGoal(heatMapSettingsPojoArray,"migratedDataExecution"); }catch(Exception e){}
		try{migratedDataExecution += Integer.parseInt(weeklyMetric.getMigratedDataExecution()); }catch(Exception e){}
		try{weeklyMetricsPojo.setValue(migratedDataExecution + "(" + goal + ")");}catch(Exception e){}
		try{weeklyMetricsPojo.setCategory(weeklyMetric.getMigratedDataExecutionStatus());}catch(Exception e){}
		try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

		index += 1;
		try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
		try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
		try{weeklyMetricsPojo.setName("testCaseNotExecutedManual");}catch(Exception e){}
		try { goal = getGoal(heatMapSettingsPojoArray,"testCaseNotExecutedManual"); }catch(Exception e){}
		try{testCaseNotExecutedManual += Integer.parseInt(weeklyMetric.getTestCaseNotExecutedManual()); }catch(Exception e){}
		try{weeklyMetricsPojo.setValue(testCaseNotExecutedManual + "(" + goal + ")");}catch(Exception e){}
		try{weeklyMetricsPojo.setCategory(weeklyMetric.getTestCaseNotExecutedManualStatus());}catch(Exception e){}
		try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

		index += 1;
		try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
		try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
		try{weeklyMetricsPojo.setName("testCaseNotExecutedAutomation");}catch(Exception e){}
		try { goal = getGoal(heatMapSettingsPojoArray,"testCaseNotExecutedAutomation"); }catch(Exception e){}
		try{testCaseNotExecutedAutomation += Integer.parseInt(weeklyMetric.getTestCaseNotExecutedAutomation()); }catch(Exception e){}
		try{weeklyMetricsPojo.setValue(testCaseNotExecutedAutomation + "(" + goal + ")");}catch(Exception e){}
		try{weeklyMetricsPojo.setCategory(weeklyMetric.getTestCaseNotExecutedAutomationStatus());}catch(Exception e){}
		try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

		index += 1;
		try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
		try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
		try{weeklyMetricsPojo.setName("currentQualityRatio");}catch(Exception e){}
		try { goal = getGoal(heatMapSettingsPojoArray,"currentQualityRatio"); }catch(Exception e){}
		try{currentQualityRatio += Integer.parseInt(weeklyMetric.getCurrentQualityRatio()); }catch(Exception e){}
		try{weeklyMetricsPojo.setValue(currentQualityRatio + "(" + goal + ")");}catch(Exception e){}
		try{weeklyMetricsPojo.setCategory(weeklyMetric.getCurrentQualityRatioStatus());}catch(Exception e){}
		try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

		index += 1;
		try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
		try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
		try{weeklyMetricsPojo.setName("qualityOfFixes");}catch(Exception e){}
		try { goal = getGoal(heatMapSettingsPojoArray,"qualityOfFixes"); }catch(Exception e){}
		try{qualityOfFixes += Integer.parseInt(weeklyMetric.getQualityOfFixes()); }catch(Exception e){}
		try{weeklyMetricsPojo.setValue(qualityOfFixes + "(" + goal + ")");}catch(Exception e){}
		try{weeklyMetricsPojo.setCategory(weeklyMetric.getQualityOfFixesStatus());}catch(Exception e){}
		try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

		index += 1;
		try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
		try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
		try{weeklyMetricsPojo.setName("defectRejection");}catch(Exception e){}
		try { goal = getGoal(heatMapSettingsPojoArray,"defectRejection"); }catch(Exception e){}
		try{defectRejection += Integer.parseInt(weeklyMetric.getDefectRejection()); }catch(Exception e){}
		try{weeklyMetricsPojo.setValue(defectRejection + "(" + goal + ")");}catch(Exception e){}
		try{weeklyMetricsPojo.setCategory(weeklyMetric.getDefectRejectionStatus());}catch(Exception e){}
		try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

		index += 1;
		try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
		try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
		try{weeklyMetricsPojo.setName("SIT2UATDefectLeakage");}catch(Exception e){}
		try { goal = getGoal(heatMapSettingsPojoArray,"SIT2UATDefectLeakage"); }catch(Exception e){}
		try{SIT2UATDefectLeakage += Integer.parseInt(weeklyMetric.getDefectLeakageSITToUAT()); }catch(Exception e){}
		try{weeklyMetricsPojo.setValue(SIT2UATDefectLeakage + "(" + goal + ")");}catch(Exception e){}
		try{weeklyMetricsPojo.setCategory(weeklyMetric.getDefectLeakageSITToUATStatus());}catch(Exception e){}
		try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

		index += 1;
		try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
		try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
		try{weeklyMetricsPojo.setName("UAT2PRODDefectLeakage");}catch(Exception e){}
		try { goal = getGoal(heatMapSettingsPojoArray,"SIT2UATDefectLeakage"); }catch(Exception e){}
		try{UAT2PRODDefectLeakage += Integer.parseInt(weeklyMetric.getDefectLeakageToProduction()); }catch(Exception e){}
		try{weeklyMetricsPojo.setValue(UAT2PRODDefectLeakage + "(" + goal + ")");}catch(Exception e){}
		try{weeklyMetricsPojo.setCategory(weeklyMetric.getDefectLeakageToProductionStatus());}catch(Exception e){}
		try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}

		index += 1;
		try{weeklyMetricsPojo = new WeeklyMetricsDataPojo();}catch(Exception e){}
		try{weeklyMetricsPojo.setId(index);}catch(Exception e){}
		try{weeklyMetricsPojo.setName("downtimeAndImpactAnalysisExecutionAvgProductivityLost");}catch(Exception e){}
		try { goal = getGoal(heatMapSettingsPojoArray,"downtimeAndImpactAnalysisExecutionAvgProductivityLost"); }catch(Exception e){}
		try{downtimeAndImpactAnalysisExecutionAvgProductivityLost += Integer.parseInt(weeklyMetric.getDowntimeAndImpactAnalysisExecutionAvgProductivityLost()); }catch(Exception e){}
		try{weeklyMetricsPojo.setValue(downtimeAndImpactAnalysisExecutionAvgProductivityLost + "(" + goal + ")");}catch(Exception e){}
		try{weeklyMetricsPojo.setCategory(weeklyMetric.getDowntimeAndImpactAnalysisExecutionAvgProductivityLostStatus());}catch(Exception e){}
		try{weeklyMetricsPojosArray.add(weeklyMetricsPojo);}catch(Exception e){}


		System.out.println("(3) Weekly Metrics Response : " + totalresponse);

		if((totalresponse.contains(nullArrayResponse)) || (totalresponse.contains(nullArrayResponse2)))
		{
			return nullResponse;
		}

		return totalresponse;
	}


	String getGoal(ArrayList<HeatMapSettingsPojo> heatMapSettingsPojoArray, String metricName)
	{
			if(metricName.compareTo("manualTestCaseAuthoringProductivity") == 0) {
				for (int i = 0; i < heatMapSettingsPojoArray.size(); i++) {
					if(heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getManualTestCaseAuthoringProductivityName()) == 0) {
						return heatMapSettingsPojoArray.get(i).getGoal();
					}
				}
			}
			else if(metricName.compareTo("regressionTestScriptingProductivity") == 0)
			{
				for (int i = 0; i < heatMapSettingsPojoArray.size(); i++) {
					if(heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getRegressionTestAutomationScriptingProductivityName()) == 0) {
						return heatMapSettingsPojoArray.get(i).getGoal();
					}
				}
			}
			else if(metricName.compareTo("designCoverage") == 0)
			{
				for (int i = 0; i < heatMapSettingsPojoArray.size(); i++) {
					if(heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getDesignCoverageName()) == 0) {
						return heatMapSettingsPojoArray.get(i).getGoal();
					}
				}
			}
			else if(metricName.compareTo("percentageOfRegressionAutomation") == 0)
			{
				for (int i = 0; i < heatMapSettingsPojoArray.size(); i++) {
					if(heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getPercentageOfRegressionAutomationName()) == 0) {
						return heatMapSettingsPojoArray.get(i).getGoal();
					}
				}
			}
			else if(metricName.compareTo("testReviewEfficiency") == 0)
			{
				for (int i = 0; i < heatMapSettingsPojoArray.size(); i++) {
					if(heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getTestReviewEfficiencyName()) == 0) {
						return heatMapSettingsPojoArray.get(i).getGoal();
					}
				}
			}
			else if(metricName.compareTo("downtimeAndImpactAnalysisDesignAvgProductivityLost") == 0)
			{
				for (int i = 0; i < heatMapSettingsPojoArray.size(); i++) {
					if(heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getDowntimeAndImpactAnalysisDesignAvgProductivityLostName()) == 0) {
						return heatMapSettingsPojoArray.get(i).getGoal();
					}
				}
			}
			else if(metricName.compareTo("manualTestExecutionProductivity") == 0)
			{
				for (int i = 0; i < heatMapSettingsPojoArray.size(); i++) {
					if(heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getManualTestExecutionProductivityName()) == 0) {
						return heatMapSettingsPojoArray.get(i).getGoal();
					}
				}
			}
			else if(metricName.compareTo("regressionAutomationTestExecutionProductivity") == 0)
			{
				for (int i = 0; i < heatMapSettingsPojoArray.size(); i++) {
					if(heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getRegressionAutomationTestExecutionProductivityName()) == 0) {
						return heatMapSettingsPojoArray.get(i).getGoal();
					}
				}
			}
			else if(metricName.compareTo("inProjectAutomationTestExecutionProductivity") == 0)
			{
				for (int i = 0; i < heatMapSettingsPojoArray.size(); i++) {
					if(heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getInProjectAutomationTestExecutionProductivityName()) == 0) {
						return heatMapSettingsPojoArray.get(i).getGoal();
					}
				}
			}
			else if(metricName.compareTo("apiAutomationTestExecutionProductivity") == 0)
			{
				for (int i = 0; i < heatMapSettingsPojoArray.size(); i++) {
					if(heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getAPIAutomationTestExecutionProductivityName()) == 0) {
						return heatMapSettingsPojoArray.get(i).getGoal();
					}
				}
			}
			else if(metricName.compareTo("migratedDataExecution") == 0)
			{
				for (int i = 0; i < heatMapSettingsPojoArray.size(); i++) {
					if(heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getMigratedDataExecutionName()) == 0) {
						return heatMapSettingsPojoArray.get(i).getGoal();
					}
				}
			}
			else if(metricName.compareTo("testCaseNotExecutedManual") == 0)
			{
				for (int i = 0; i < heatMapSettingsPojoArray.size(); i++) {
					if(heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getTestCaseNotExecutedManualName()) == 0) {
						return heatMapSettingsPojoArray.get(i).getGoal();
					}
				}
			}
			else if(metricName.compareTo("testCaseNotExecutedAutomation") == 0)
			{
				for (int i = 0; i < heatMapSettingsPojoArray.size(); i++) {
					if(heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getTestCaseNotExecutedAutomationName()) == 0) {
						return heatMapSettingsPojoArray.get(i).getGoal();
					}
				}
			}
			else if(metricName.compareTo("currentQualityRatio") == 0)
			{
				for (int i = 0; i < heatMapSettingsPojoArray.size(); i++) {
					if(heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getCurrentQualityRatioName()) == 0) {
						return heatMapSettingsPojoArray.get(i).getGoal();
					}
				}
			}
			else if(metricName.compareTo("qualityOfFixes") == 0)
			{
				for (int i = 0; i < heatMapSettingsPojoArray.size(); i++) {
					if(heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getQualityOfFixesName()) == 0) {
						return heatMapSettingsPojoArray.get(i).getGoal();
					}
				}
			}
			else if(metricName.compareTo("defectRejection") == 0)
			{
				for (int i = 0; i < heatMapSettingsPojoArray.size(); i++) {
					if(heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getDefectRejectionName()) == 0) {
						return heatMapSettingsPojoArray.get(i).getGoal();
					}
				}
			}
			else if(metricName.compareTo("SIT2UATDefectLeakage") == 0)
			{
				for (int i = 0; i < heatMapSettingsPojoArray.size(); i++) {
					if(heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getDefectLeakageSITToUATName()) == 0) {
						return heatMapSettingsPojoArray.get(i).getGoal();
					}
				}
			}
			else if(metricName.compareTo("UAT2PRODDefectLeakage") == 0)
			{
				for (int i = 0; i < heatMapSettingsPojoArray.size(); i++) {
					if(heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getDefectLeakageToProductionName()) == 0) {
						return heatMapSettingsPojoArray.get(i).getGoal();
					}
				}
			}
			else if(metricName.compareTo("downtimeAndImpactAnalysisExecutionAvgProductivityLost") == 0)
			{
				for (int i = 0; i < heatMapSettingsPojoArray.size(); i++) {
					if(heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getDowntimeAndImpactAnalysisExecutionAvgProductivityLostName()) == 0) {
						return heatMapSettingsPojoArray.get(i).getGoal();
					}
				}
			}



		return null;
	}


	@RequestMapping(value = "/projects/{id}/fetchMonthlyMetrics", method = RequestMethod.GET)
	@ResponseBody
	public String generateMonthlyMetricsData(@PathVariable("id") long id) {

		Iterable<MonthlyMetrics> monthlyMetricsIterable = monthlyMetricsRepository.findAll();
		Iterator<MonthlyMetrics> monthlyMetricsIterableIterator = monthlyMetricsIterable.iterator();

		MonthlyMetrics monthlyMetric = null;
		String totalresponse = "{\"MonthlyMetrics\":[";
		String nullArrayResponse = "[null]";
		String nullResponse = "{\"MonthlyMetrics\":null}";



		String temptotalResponse = null;

		while (monthlyMetricsIterableIterator.hasNext()) {
			monthlyMetric = monthlyMetricsIterableIterator.next();

			if ((monthlyMetric.getProject().getId() == id)) {

				//String response = "{\"weekNumber\":" + weeklyMetric.getWeekNumber() + "," + "\"data\": [{";
				ArrayList<MonthlyMetricsDataPojo> monthlyMetricPojoArray = new ArrayList<MonthlyMetricsDataPojo>();
				String response = "{\"yearNumber\":" + monthlyMetric.getYearNumber() + "," ;
				response = response + "\"monthNumber\":" + monthlyMetric.getMonthNumber() + "," + "\"data\": ";

				int index = 1;
				MonthlyMetricsDataPojo monthlyMetricPojo = new MonthlyMetricsDataPojo();
				monthlyMetricPojo.setId(index);
				monthlyMetricPojo.setName("costVariance");
				monthlyMetricPojo.setValue(monthlyMetric.getCostVariance());
				monthlyMetricPojo.setCategory(monthlyMetric.getCostVarianceStatus());
				monthlyMetricPojoArray.add(monthlyMetricPojo);

				index += 1;
				monthlyMetricPojo = new MonthlyMetricsDataPojo();
				monthlyMetricPojo.setId(index);
				monthlyMetricPojo.setName("scheduleVariance");
				monthlyMetricPojo.setValue(monthlyMetric.getScheduleVariance());
				monthlyMetricPojo.setCategory(monthlyMetric.getScheduleVarianceStatus());
				monthlyMetricPojoArray.add(monthlyMetricPojo);


				index += 1;
				monthlyMetricPojo = new MonthlyMetricsDataPojo();
				monthlyMetricPojo.setId(index);
				monthlyMetricPojo.setName("scopeVariance");
				monthlyMetricPojo.setValue(monthlyMetric.getScopeVariance());
				monthlyMetricPojo.setCategory(monthlyMetric.getScopeVarianceStatus());
				monthlyMetricPojoArray.add(monthlyMetricPojo);

				index += 1;
				monthlyMetricPojo = new MonthlyMetricsDataPojo();
				monthlyMetricPojo.setId(index);
				monthlyMetricPojo.setName("effortVariance");
				monthlyMetricPojo.setValue(monthlyMetric.getEffortVariance());
				monthlyMetricPojo.setCategory(monthlyMetric.getEffortVarianceStatus());
				monthlyMetricPojoArray.add(monthlyMetricPojo);

				index += 1;
				monthlyMetricPojo = new MonthlyMetricsDataPojo();
				monthlyMetricPojo.setId(index);
				monthlyMetricPojo.setName("costPerDefect");
				monthlyMetricPojo.setValue(monthlyMetric.getCostPerDefect());
				monthlyMetricPojo.setCategory(monthlyMetric.getCostPerDefectStatus());
				monthlyMetricPojoArray.add(monthlyMetricPojo);

				index += 1;
				monthlyMetricPojo = new MonthlyMetricsDataPojo();
				monthlyMetricPojo.setId(index);
				monthlyMetricPojo.setName("costPerTestCase");
				monthlyMetricPojo.setValue(monthlyMetric.getCostPerTestCase());
				monthlyMetricPojo.setCategory(monthlyMetric.getCostPerTestCaseStatus());
				monthlyMetricPojoArray.add(monthlyMetricPojo);

				index += 1;
				monthlyMetricPojo = new MonthlyMetricsDataPojo();
				monthlyMetricPojo.setId(index);
				monthlyMetricPojo.setName("numberOfStepChangesProposed");
				monthlyMetricPojo.setValue(monthlyMetric.getNumberOfStepChangesProposed());
				monthlyMetricPojo.setCategory("G");
				monthlyMetricPojoArray.add(monthlyMetricPojo);

				index += 1;
				monthlyMetricPojo = new MonthlyMetricsDataPojo();
				monthlyMetricPojo.setId(index);
				monthlyMetricPojo.setName("numberOfStepChangesInProgress");
				monthlyMetricPojo.setValue(monthlyMetric.getNumberOfStepChangesInProgress());
				monthlyMetricPojo.setCategory("G");
				monthlyMetricPojoArray.add(monthlyMetricPojo);

				index += 1;
				monthlyMetricPojo = new MonthlyMetricsDataPojo();
				monthlyMetricPojo.setId(index);
				monthlyMetricPojo.setName("numberOfStepChangesImplemented");
				monthlyMetricPojo.setValue(monthlyMetric.getNumberOfStepChangesImplemented());
				monthlyMetricPojo.setCategory("G");
				monthlyMetricPojoArray.add(monthlyMetricPojo);


				Gson gson = new Gson();

				if(temptotalResponse == null)
				{
					temptotalResponse = response + gson.toJson(monthlyMetricPojoArray) + "}";
				}
				else {
					temptotalResponse =  response + gson.toJson(monthlyMetricPojoArray) + "}," + temptotalResponse;
				}
			}
		}
		//temptotalResponse = temptotalResponse.substring(0,temptotalResponse.length()-1);
		totalresponse = totalresponse + temptotalResponse + "]}";
		System.out.println(totalresponse);


		if(totalresponse.contains(nullArrayResponse))
		{
			return nullResponse;
		}

		return totalresponse;
	}

	@RequestMapping(value = "/projects/{id}/fetchQuarterlyMetrics", method = RequestMethod.GET)
	@ResponseBody
	public String generateQuarterlyMetricsData(@PathVariable("id") long id) {

		Iterable<QuarterlyMetrics> quarterlyMetricsIterable = quarterMetricsRepository.findAll();
		Iterator<QuarterlyMetrics> quarterlyMetricsIterator = quarterlyMetricsIterable.iterator();

		QuarterlyMetrics quarterlyMetrics = null;
		String totalresponse = "{\"QuarterlyMetrics\":[";
		String nullArrayResponse = "[null]";
		String nullResponse = "{\"QuarterlyMetrics\":null}";



		String temptotalResponse = null;

		while (quarterlyMetricsIterator.hasNext()) {
			quarterlyMetrics = quarterlyMetricsIterator.next();

			if ((quarterlyMetrics.getProject().getId() == id)) {

				//String response = "{\"weekNumber\":" + weeklyMetric.getWeekNumber() + "," + "\"data\": [{";
				ArrayList<QuarterlyMetricsDataPojo> quarterlyMetricsPojoArray = new ArrayList<QuarterlyMetricsDataPojo>();
				String response = "{\"yearNumber\":" + quarterlyMetrics.getYearNumber() + "," ;
				response = response + "\"quarterNumber\":" + quarterlyMetrics.getQuarterNumber() + "," + "\"data\": ";

				int index = 1;
				QuarterlyMetricsDataPojo quarterlyMetricsDataPojo = new QuarterlyMetricsDataPojo();
				quarterlyMetricsDataPojo.setId(index);
				quarterlyMetricsDataPojo.setName("revenueLeakage");
				quarterlyMetricsDataPojo.setValue(quarterlyMetrics.getRevenueLeakage());
				quarterlyMetricsDataPojo.setCategory(quarterlyMetrics.getRevenueLeakageStatus());
				quarterlyMetricsPojoArray.add(quarterlyMetricsDataPojo);

				Gson gson = new Gson();

				if(temptotalResponse == null)
				{
					temptotalResponse = response + gson.toJson(quarterlyMetricsPojoArray) + "}";
				}
				else {
					temptotalResponse =  response + gson.toJson(quarterlyMetricsPojoArray) + "}," + temptotalResponse;
				}
			}
		}
		//temptotalResponse = temptotalResponse.substring(0,temptotalResponse.length()-1);
		totalresponse = totalresponse + temptotalResponse + "]}";
		System.out.println(totalresponse);


		if(totalresponse.contains(nullArrayResponse))
		{
			return nullResponse;
		}

		return totalresponse;
	}

	void buildDefectAndExecutionString(int index, String header, int value){

		String defectsAndExecutionDataString = "{\"Metrics Measurements\":";
		defectsAndExecutionDataString += index;
		defectsAndExecutionDataString += ",";
		defectsAndExecutionDataString += "\"type\":\"";
		defectsAndExecutionDataString += header + "\",";
		defectsAndExecutionDataString += "\"value\":";
		defectsAndExecutionDataString += value;
		defectsAndExecutionDataString += "}";

		System.out.println(">>>>defectsAndExecutionDataString>>>>>" + defectsAndExecutionDataString);

		try {

			HttpHost host = new HttpHost("localhost", 9200);
			final RestClientBuilder builder = RestClient.builder(host);
			client = new RestHighLevelClient(builder);
			save(Integer.toString(index), defectsAndExecutionDataString, CUMMULATIVE_INDEX_2); // Add New Index
			client.indices().refresh(new RefreshRequest(CUMMULATIVE_INDEX_2), RequestOptions.DEFAULT);
			client.close();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}


	}


}