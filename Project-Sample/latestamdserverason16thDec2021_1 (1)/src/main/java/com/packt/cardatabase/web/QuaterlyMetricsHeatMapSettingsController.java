package com.packt.cardatabase.web;

import com.google.gson.Gson;
import com.packt.cardatabase.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@RestController
public class QuaterlyMetricsHeatMapSettingsController {

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private QuarterlyMetricsHeatMapSettingsRepository heatMapSettingsRepository;


	@RequestMapping(value="/projects/{id}/quarterlyMetricsHeatMapSettings", method=RequestMethod.GET)
	@ResponseBody
	public String getMonthlyMetricsHeatMapSettings(@PathVariable("id") long id) {

		try {

    		ArrayList<QuarterlyMetricsHeatMapSettingsPojo> heatMapSettingsPojoArray = new ArrayList<QuarterlyMetricsHeatMapSettingsPojo>();

			Iterable<QuarterlyMetricsHeatMapSettings> heatMapSettingsIterable = heatMapSettingsRepository.findAll();
			Iterator<QuarterlyMetricsHeatMapSettings> heatMapSettingsIterator = heatMapSettingsIterable.iterator();
			String response = null;
			boolean recordFound = false;
			int index = 1;

			while (heatMapSettingsIterator.hasNext()) {
				QuarterlyMetricsHeatMapSettings next = heatMapSettingsIterator.next();

				if (next.getProject().getId() == id) {
					recordFound = true;

					if(next.getCategory().contains("Leakage")) {

						QuarterlyMetricsHeatMapSettingsPojo heatMapSettingsPojo = new QuarterlyMetricsHeatMapSettingsPojo();
						heatMapSettingsPojo.setMetricName(next.getMetricName());
						heatMapSettingsPojo.setId(index);
						index += 1;
						heatMapSettingsPojo.setLCL(next.getLCL());
						heatMapSettingsPojo.setUCL(next.getUCL());
						heatMapSettingsPojo.setGoal(next.getGoal());
						heatMapSettingsPojo.setCategory(next.getCategory());
						heatMapSettingsPojoArray.add(heatMapSettingsPojo);

					}
				}
			}

			if(recordFound == false) {
				QuarterlyMetricsHeatMapSettingsPojo heatMapSettingsPojo = new QuarterlyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setMetricName(QuarterlyMetrics.getRevenueLeakageName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Leakage");
				heatMapSettingsPojo.setId(index);
				index += 1;
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
			}


			//}

			Gson gson = new Gson();
			response = gson.toJson(heatMapSettingsPojoArray);
			response = "{\"MetricsSettings\":" + response + "}";
			System.out.println(response);
			return response;

		} catch (Exception e) {

		}
		return "{\"MetricsSettings\":[] }";
	}



	@PostMapping(value = "/projects/{id}/quarterlyMetricsHeatMapSettings")
	@ResponseBody
	public String updateQuarterlyMetricsHeatMapSettings(@PathVariable("id") long id, @RequestBody List<QuarterlyMetricsHeatMapSettingsPojo> heatMapSettingsPojoArray) {

		try {

			Iterable<QuarterlyMetricsHeatMapSettings> heatMapSettingsIterable = heatMapSettingsRepository.findAll();
			Iterator<QuarterlyMetricsHeatMapSettings> heatMapSettingsIterator = heatMapSettingsIterable.iterator();

			while (heatMapSettingsIterator.hasNext()) {
				QuarterlyMetricsHeatMapSettings next = heatMapSettingsIterator.next();

				if (next.getProject().getId() == id) {

					try {
						heatMapSettingsRepository.delete(next);
					} catch (Exception e) {

					}
				}
			}


			for (int i = 0; i < heatMapSettingsPojoArray.size(); i++) {
				QuarterlyMetricsHeatMapSettings next = new QuarterlyMetricsHeatMapSettings();

				System.out.println(heatMapSettingsPojoArray.get(i).getMetricName());
				System.out.println(heatMapSettingsPojoArray.get(i).getLCL());
				System.out.println(heatMapSettingsPojoArray.get(i).getUCL());
				System.out.println(heatMapSettingsPojoArray.get(i).getGoal());

				if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(QuarterlyMetrics.getRevenueLeakageName()) == 0) {

					next.setMetricName(QuarterlyMetrics.getRevenueLeakageName());
					next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
					next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
					next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
					next.setCategory("Leakage");


					Iterable<Project> allProjects = projectRepository.findAll();
					Iterator<Project> iterator = allProjects.iterator();

					while (iterator.hasNext()) {
						Project nextProject = iterator.next();

						System.out.println("The specified id is : " + id);
						System.out.println("The next project id is : " + nextProject.getId());

						if (nextProject.getId() == id) {
							next.setProject(nextProject);
							break;
						}
					}

					heatMapSettingsRepository.save(next);

				}
			}

			return "OK";

		}catch (Exception e) {
		}
		return "Error";
	}
}