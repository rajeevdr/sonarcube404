package com.packt.cardatabase.domain;

import java.util.ArrayList;

public class AlmModuleDetailsPojo {

	private Integer id;
	private String module;
	private ArrayList<String> testExecutionFolderId = null;
	private ArrayList<String> testDesignFolderId = null;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public ArrayList<String> getTestExecutionFolderId() {
		return testExecutionFolderId;
	}

	public void setTestExecutionFolderId(ArrayList<String> testExecutionFolderId) {
		this.testExecutionFolderId = testExecutionFolderId;
	}

	public ArrayList<String> getTestDesignFolderId() {
		return testDesignFolderId;
	}

	public void setTestDesignFolderId(ArrayList<String> testDesignFolderId) {
		this.testDesignFolderId = testDesignFolderId;
	}

}
