package com.packt.cardatabase.domain;

import javax.persistence.*;

@Entity
public class MonthlyMetrics{


    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private String costVariance;
    private String scopeVariance;
    private String effortVariance;
    private String scheduleVariance;
    private String costPerDefect;
    private String costPerTestCase;
    private String sitDefectDensity;
    private String percentageOfTCsIdentifiedForMigration;
    private String defectLeakageSITToUAT;
    private String defectLeakageToProduction;
    private String inProjectAutomationPercentageExtentToPotential;
    private String apiAutomationPercentageExtentToPotential;
    private String uatDefectDensity;
    private String totalNumberOfResignationsSubmitted;
    private String totalNumberOfResourcesRelieved;
    private String numberOfStepChangesProposed;
    private String numberOfStepChangesInProgress;
    private String numberOfStepChangesImplemented;
    private String totalNumberOfManualTestCasesDesigned;
    private String numberOfTestEngineers;
    private String numberOfLeads;
    private String numberOfManagers;
    private String totalNumberOfTestCasesTaggedForMigrationExecution;
    private String manualEffectiveness;
    private String automationEffectiveness;
    private String migrationExecutionCoverage;
    private String estimatedTestCases;
    private String grossMargin;
    private int monthNumber;
    private int yearNumber;


    private String costVarianceStatus;
    private String scopeVarianceStatus;
    private String effortVarianceStatus;
    private String scheduleVarianceStatus;
    private String costPerDefectStatus;
    private String costPerTestCaseStatus;
    private String sitDefectDensityStatus;
    private String percentageOfTCsIdentifiedForMigrationStatus;
    private String defectLeakageSITToUATStatus;
    private String defectLeakageToProductionStatus;
    private String inProjectAutomationPercentageExtentToPotentialStatus;
    private String apiAutomationPercentageExtentToPotentialStatus;
    private String uatDefectDensityStatus;
    private String manualEffectivenessStatus;
    private String automationEffectivenessStatus;



    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public MonthlyMetrics() {
    }

    MonthlyMetrics(String costVariance,
                   String scopeVariance,
                   String effortVariance,
                   String scheduleVariance,
                   String costPerDefect,
                   String costPerTestCase,
                   String sitDefectDensity,
                   String percentageOfTCsIdentifiedForMigration,
                   String defectLeakageSITToUAT,
                   String defectLeakageToProduction,
                   String inProjectAutomationPercentageExtentToPotential,
                   String apiAutomationPercentageExtentToPotential,
                   String uatDefectDensity,
                   String totalNumberOfResignationsSubmitted,
                   String totalNumberOfResourcesRelieved,
                   String numberOfStepChangesProposed,
                   String numberOfStepChangesInProgress,
                   String numberOfStepChangesImplemented,
                   String grossMargin,
                   String numberOfTestEngineers,
                   String numberOfLeads,
                   String numberOfManagers,
                   String manualEffectiveness,
                   String automationEffectiveness,
                   int monthNumber,
                   int yearNumber,
                   Project project) {

        this.costVariance = costVariance;
        this.scopeVariance = scopeVariance;
        this.effortVariance = effortVariance;
        this.scheduleVariance = scheduleVariance;
        this.costPerDefect = costPerDefect;
        this.costPerTestCase = costPerTestCase;
        this.sitDefectDensity = sitDefectDensity;
        this.percentageOfTCsIdentifiedForMigration = percentageOfTCsIdentifiedForMigration;
        this.defectLeakageSITToUAT = defectLeakageSITToUAT;
        this.defectLeakageToProduction = defectLeakageToProduction;
        this.inProjectAutomationPercentageExtentToPotential = inProjectAutomationPercentageExtentToPotential;
        this.apiAutomationPercentageExtentToPotential = apiAutomationPercentageExtentToPotential;
        this.uatDefectDensity = uatDefectDensity;
        this.totalNumberOfResignationsSubmitted = totalNumberOfResignationsSubmitted;
        this.totalNumberOfResourcesRelieved = totalNumberOfResourcesRelieved;
        this.numberOfStepChangesInProgress = numberOfStepChangesInProgress;
        this.numberOfStepChangesProposed = numberOfStepChangesProposed;
        this.numberOfStepChangesInProgress = numberOfStepChangesInProgress;
        this.numberOfStepChangesImplemented = numberOfStepChangesImplemented;
        this.numberOfTestEngineers = numberOfTestEngineers;
        this.numberOfLeads = numberOfLeads;
        this.numberOfManagers = numberOfManagers;
        this.manualEffectiveness = manualEffectiveness;
        this.automationEffectiveness = automationEffectiveness;
        this.grossMargin = grossMargin;
        this.monthNumber = monthNumber;
        this.yearNumber = yearNumber;
        this.project = project;

    }

    public String getTotalNumberOfTestCasesTaggedForMigrationExecutionName() {
        return "Total Number Of Migration TestCases Planned";
    }


    public String getTotalNumberOfTestCasesTaggedForMigrationExecution() {
        return totalNumberOfTestCasesTaggedForMigrationExecution;
    }

    public void setTotalNumberOfTestCasesTaggedForMigrationExecution(String totalNumberOfTestCasesTaggedForMigrationExecution) {
        this.totalNumberOfTestCasesTaggedForMigrationExecution = totalNumberOfTestCasesTaggedForMigrationExecution;
    }

    public String getMigrationExecutionCoverageName() {
        return "Monthly Migration Execution Coverage";
    }

    public String getMigrationExecutionCoverage() {
        return totalNumberOfTestCasesTaggedForMigrationExecution;
    }

    public void setMigrationExecutionCoverage(String migrationExecutionCoverage) {
        this.migrationExecutionCoverage = migrationExecutionCoverage;
    }


    public String getTotalNumberOfManualTestCasesDesignedName() {
        return "Total Number Of Manual TestCases Designed Monthly";
    }

    public String getTotalNumberOfManualTestCasesDesigned() {
        return totalNumberOfManualTestCasesDesigned;
    }

    public void setTotalNumberOfManualTestCasesDesigned(String totalNumberOfManualTestCasesDesigned) {
        this.totalNumberOfManualTestCasesDesigned = totalNumberOfManualTestCasesDesigned;
    }


    public String getYearNumberName() {
        return "Year";
    }
    public int getYearNumber() {
        return yearNumber;
    }
    public void setYearNumber(int yearNumber) {
        this.yearNumber = yearNumber;
    }


    public static String getNumberOfStepChangesProposedName() {
        return "Number of Step Changes Proposed";
    }
    public String getNumberOfStepChangesProposed() {
        return numberOfStepChangesProposed;
    }
    public void setNumberOfStepChangesProposed(String numberOfStepChangesProposed) {
        this.numberOfStepChangesProposed = numberOfStepChangesProposed;
    }


    public static String getNumberOfStepChangesInProgressName() {
        return "Number of Step Changes in-progress";
    }
    public String getNumberOfStepChangesInProgress() {
        return numberOfStepChangesInProgress;
    }
    public void setNumberOfStepChangesInProgress(String numberOfStepChangesInProgress) {
        this.numberOfStepChangesInProgress = numberOfStepChangesInProgress;
    }



    public static String getNumberOfStepChangesImplementedName() {
        return "Number of Step Changes implemented";
    }
    public String getNumberOfStepChangesImplemented() {
        return numberOfStepChangesImplemented;
    }
    public void setNumberOfStepChangesImplemented(String numberOfStepChangesImplemented) {
        this.numberOfStepChangesImplemented = numberOfStepChangesImplemented;
    }

    public void setCostVariance(String costVariance){
        this.costVariance = costVariance;
    }

    public void setScopeVariance(String scopeVariance){
        this.scopeVariance = scopeVariance;
    }

    public void setEffortVariance(String effortVariance){
        this.effortVariance = effortVariance;
    }

    public void setScheduleVariance(String scheduleVariance){
        this.scheduleVariance = scheduleVariance;
    }

    public void setCostPerDefect(String costPerDefect){
        this.costPerDefect = costPerDefect;
    }

    public void setCostPerTestCase(String costPerTestCase){
        this.costPerTestCase = costPerTestCase;
    }

    public void setSITDefectDensity(String sitDefectDensity){
        this.sitDefectDensity = sitDefectDensity;
    }

    public void setPercentageOfTCsIdentifiedForMigration(String percentageOfTCsIdentifiedForMigration){
        this.percentageOfTCsIdentifiedForMigration = percentageOfTCsIdentifiedForMigration;
    }

    public void setDefectLeakageSITToUAT(String defectLeakageSITToUAT){
        this.defectLeakageSITToUAT = defectLeakageSITToUAT;
    }

    public void setDefectLeakageToProduction(String defectLeakageToProduction){
        this.defectLeakageToProduction = defectLeakageToProduction;
    }

    public void setInProjectAutomationPercentageExtentToPotential(String inProjectAutomationPercentageExtentToPotential){
        this.inProjectAutomationPercentageExtentToPotential = inProjectAutomationPercentageExtentToPotential;
    }

    public void setApiAutomationPercentageExtentToPotential(String apiAutomationPercentageExtentToPotential){
        this.apiAutomationPercentageExtentToPotential = apiAutomationPercentageExtentToPotential;
    }

    public void setUATDefectDensity(String uatDefectDensity){
        this.uatDefectDensity = uatDefectDensity;
    }


    public String getCostVariance(){
        return costVariance;
    }

    public String getScopeVariance(){
        return scopeVariance;
    }

    public String getEffortVariance(){
        return effortVariance;
    }

    public String getScheduleVariance(){
        return scheduleVariance;
    }

    public String getCostPerDefect(){
        return costPerDefect;
    }

    public String getCostPerTestCase(){
        return costPerTestCase;
    }

    public String getSITDefectDensity(){
        return sitDefectDensity;
    }

    public String getPercentageOfTCsIdentifiedForMigration(){
        return percentageOfTCsIdentifiedForMigration;
    }

    public String getDefectLeakageSITToUAT(){
        return defectLeakageSITToUAT;
    }

    public String getDefectLeakageToProduction(){
        return defectLeakageToProduction;
    }

    public String getInProjectAutomationPercentageExtentToPotential(){
        return inProjectAutomationPercentageExtentToPotential;
    }

    public String getApiAutomationPercentageExtentToPotential(){
        return apiAutomationPercentageExtentToPotential;
    }

    public String getUATDefectDensity(){
        return uatDefectDensity;
    }


    public static String getCostVarianceName(){
        return "Cost Variance";
    }

    public static String getScheduleVarianceName(){
        return "Schedule Variance";
    }

    public static String getScopeVarianceName(){
        return "Scope Variance";
    }

    public static String getEffortVarianceName(){
        return "Effort Variance";
    }

    public static String getCostPerDefectName(){
        return "Cost per Defect";
    }

    public static String getCostPerTestCaseName(){
        return "Cost Per Test Case";
    }

    public static String getSITDefectDensityName(){
        return "Monthly SIT Defect Density";
    }

    public static String getUATDefectDensityName(){
        return "Monthly UAT Defect Density";
    }

    public static String getPercentageOfTCsIdentifiedForMigrationName(){
        return "Percentage Of TCs Identified For Migration";
    }

    public static String getDefectLeakageSITToUATName(){
        return "Defect Leakage SIT 2 UAT";
    }

    public static String getDefectLeakageToProductionName(){
        return "Defect Leakage 2 Production";
    }

    public static String getInProjectAutomationPercentageExtentToPotentialName(){
        return "Monthly InProject Automation Percentage Extent To Potential";
    }

    public static String getApiAutomationPercentageExtentToPotentialName(){
        return "Monthly API Automation Percentage Extent To Potential";
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public int getMonthNumber() {
        return monthNumber;
    }
    public String getMonthNumberName() {
        return "Month";
    }
    public void setMonthNumber(int monthNumber) {
        this.monthNumber = monthNumber;
    }

    public String getGrossMarginName(){
        return "Gross Margin for the month";
    }

    public String getGrossMargin(){
        return this.grossMargin;
    }

    public void setGrossMargin(String grossMargin){
        this.grossMargin = grossMargin;
    }


    public String getTotalNumberOfResignationsSubmittedName() {
        return "Total Number Of Resignations Submitted";
    }

    public String getTotalNumberOfResignationsSubmitted() {
        return totalNumberOfResignationsSubmitted;
    }

    public void setTotalNumberOfResignationsSubmitted(String totalNumberOfResignationsSubmitted) {
        this.totalNumberOfResignationsSubmitted = totalNumberOfResignationsSubmitted;
    }

    public String getTotalNumberOfResourcesRelievedName() {
        return "Total Number Of Resources Relieved";
    }

    public String getTotalNumberOfResourcesRelieved() {
        return totalNumberOfResourcesRelieved;
    }

    public void setTotalNumberOfResourcesRelieved(String totalNumberOfResourcesRelieved) {
        this.totalNumberOfResourcesRelieved = totalNumberOfResourcesRelieved;
    }


    public void setCostVarianceStatus(String costVarianceStatus){
        this.costVarianceStatus = costVarianceStatus;
    }

    public void setScopeVarianceStatus(String scopeVarianceStatus){
        this.scopeVarianceStatus = scopeVarianceStatus;
    }

    public void setEffortVarianceStatus(String effortVarianceStatus){
        this.effortVarianceStatus = effortVarianceStatus;
    }

    public void setScheduleVarianceStatus(String scheduleVarianceStatus){
        this.scheduleVarianceStatus = scheduleVarianceStatus;
    }

    public void setCostPerDefectStatus(String costPerDefectStatus){
        this.costPerDefectStatus = costPerDefectStatus;
    }

    public void setCostPerTestCaseStatus(String costPerTestCaseStatus){
        this.costPerTestCaseStatus = costPerTestCaseStatus;
    }

    public void setSITDefectDensityStatus(String sitDefectDensityStatus){
        this.sitDefectDensityStatus = sitDefectDensityStatus;
    }

    public void setPercentageOfTCsIdentifiedForMigrationStatus(String percentageOfTCsIdentifiedForMigrationStatus){
        this.percentageOfTCsIdentifiedForMigrationStatus = percentageOfTCsIdentifiedForMigrationStatus;
    }

    public void setDefectLeakageSITToUATStatus(String defectLeakageSITToUATStatus){
        this.defectLeakageSITToUATStatus = defectLeakageSITToUATStatus;
    }

    public void setDefectLeakageToProductionStatus(String defectLeakageToProductionStatus){
        this.defectLeakageToProductionStatus = defectLeakageToProductionStatus;
    }

    public void setInProjectAutomationPercentageExtentToPotentialStatus(String inProjectAutomationPercentageExtentToPotentialStatus){
        this.inProjectAutomationPercentageExtentToPotentialStatus = inProjectAutomationPercentageExtentToPotentialStatus;
    }

    public void setAPIAutomationPercentageExtentToPotentialStatus(String apiAutomationPercentageExtentToPotentialStatus){
        this.apiAutomationPercentageExtentToPotentialStatus = apiAutomationPercentageExtentToPotentialStatus;
    }

    public void setUATDefectDensityStatus(String uatDefectDensityStatus){
        this.uatDefectDensityStatus = uatDefectDensityStatus;
    }

    public void setManualEffectivenessStatus(String manualEffectivenessStatus){
        this.manualEffectivenessStatus = manualEffectivenessStatus;
    }

    public void setAutomationEffectivenessStatus(String automationEffectivenessStatus){
        this.automationEffectivenessStatus = automationEffectivenessStatus;
    }


    public String getCostVarianceStatus(){
        return this.costVarianceStatus;
    }

    public String getScopeVarianceStatus(){
        return this.scopeVarianceStatus;
    }

    public String getEffortVarianceStatus(){
        return this.effortVarianceStatus;
    }

    public String getScheduleVarianceStatus(){
        return this.scheduleVarianceStatus;
    }

    public String getCostPerDefectStatus(){
        return this.costPerDefectStatus;
    }

    public String getCostPerTestCaseStatus(){
        return this.costPerTestCaseStatus;
    }

    public String getSITDefectDensityStatus(){
        return this.sitDefectDensityStatus;
    }

    public String getPercentageOfTCsIdentifiedForMigrationStatus(){
        return this.percentageOfTCsIdentifiedForMigrationStatus;
    }

    public String getDefectLeakageSITToUATStatus(){
        return this.defectLeakageSITToUATStatus;
    }

    public String getDefectLeakageToProductionStatus(){
        return this.defectLeakageToProductionStatus;
    }

    public String getInProjectAutomationPercentageExtentToPotentialStatus(){
        return this.inProjectAutomationPercentageExtentToPotentialStatus;
    }

    public String getAPIAutomationPercentageExtentToPotentialStatus(){
        return this.apiAutomationPercentageExtentToPotentialStatus;
    }

    public String getUATDefectDensityStatus(){
        return this.uatDefectDensityStatus;
    }

    public String getManualEffectivenessStatus(){
        return this.manualEffectivenessStatus;
    }

    public String getAutomationEffectivenessStatus(){
        return this.automationEffectivenessStatus;
    }


    public String getNumberOfTestEngineersName() {
        return "Number Of Test Engineers";
    }

    public String getNumberOfTestEngineers() {
        return numberOfTestEngineers;
    }

    public void setNumberOfTestEngineers(String numberOfTestEngineers) {
        this.numberOfTestEngineers = numberOfTestEngineers;
    }


    public String getNumberOfLeadsName() {
        return "Number Of Leads";
    }

    public String getNumberOfLeads() {
        return numberOfLeads;
    }

    public void setNumberOfLeads(String numberOfLeads) {
        this.numberOfLeads = numberOfLeads;
    }


    public String getNumberOfManagersName() {
        return "Number Of Managers";
    }

    public String getNumberOfManagers() {
        return numberOfManagers;
    }

    public void setNumberOfManagers(String numberOfManagers) {
        this.numberOfManagers = numberOfManagers;
    }


    public String getEstimatedTestCasesName() {
        return "Estimated TestCases";
    }

    public String getEstimatedTestCases() {
        return estimatedTestCases;
    }

    public void setEstimatedTestCases(String estimatedTestCases) {
        this.estimatedTestCases = estimatedTestCases;
    }


    public static String getManualEffectivenessName() {
        return "Monthly Manual Testing Effectiveness";
    }

    public String getManualEffectiveness() {
        return manualEffectiveness;
    }

    public void setManualEffectiveness(String manualEffectiveness) {
        this.manualEffectiveness = manualEffectiveness;
    }

    public static String getAutomationEffectivenessName() {
        return "Monthly Automation Testing Effectiveness";
    }

    public String getAutomationEffectiveness() {
        return automationEffectiveness;
    }

    public void setAutomationEffectiveness(String automationEffectiveness) {
        this.automationEffectiveness = automationEffectiveness;
    }


}
