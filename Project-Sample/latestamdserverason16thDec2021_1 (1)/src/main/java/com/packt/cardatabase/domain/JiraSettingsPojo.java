package com.packt.cardatabase.domain;

import javax.persistence.*;

public class JiraSettingsPojo {

    private String jiraQueryAPI;
    private String jiraUserId;
    private String jiraUserPassCode;
    private String zephyrBaseUrl;
    private String zapiAccessKey;
    private String secretKey;
    private String accountId;

    public JiraSettingsPojo(){
    }

    public JiraSettingsPojo(String jiraQueryAPI, String jiraUserId, String jiraUserPassCode, String zephyrBaseUrl, String zapiAccessKey, String secretKey, String accountId) {
        this.jiraQueryAPI = jiraQueryAPI;
        this.jiraUserId = jiraUserId;
        this.jiraUserPassCode = jiraUserPassCode;
        this.zephyrBaseUrl = zephyrBaseUrl;
        this.zapiAccessKey = zapiAccessKey;
        this.secretKey = secretKey;
        this.accountId = accountId;
    }

    public JiraSettingsPojo(JiraSettings jiraSettings) {
        this.jiraQueryAPI = jiraSettings.getJiraQueryAPI();
        this.jiraUserId = jiraSettings.getJiraUserId();
        this.jiraUserPassCode = jiraSettings.getJiraUserPassCode();
        this.zephyrBaseUrl = jiraSettings.getZephyrBaseUrl();
        this.zapiAccessKey = jiraSettings.getZapiAccessKey();
        this.secretKey = jiraSettings.getSecretKey();
        this.accountId = jiraSettings.getAccountId();
    }

    public String getJiraQueryAPI() {
        return jiraQueryAPI;
    }

    public void setJiraQueryAPI(String jiraQueryAPI) {
        this.jiraQueryAPI = jiraQueryAPI;
    }

    public String getJiraUserId() {
        return jiraUserId;
    }

    public void setJiraUserId(String jiraUserId) {
        this.jiraUserId = jiraUserId;
    }

    public String getJiraUserPassCode() {
        return jiraUserPassCode;
    }

    public void setJiraUserPassCode(String jiraUserPassCode) {
        this.jiraUserPassCode = jiraUserPassCode;
    }

    public String getZephyrBaseUrl() {
        return zephyrBaseUrl;
    }

    public void setZephyrBaseUrl(String zephyrBaseUrl) {
        this.zephyrBaseUrl = zephyrBaseUrl;
    }

    public String getZapiAccessKey() {
        return zapiAccessKey;
    }

    public void setZapiAccessKey(String zapiAccessKey) {
        this.zapiAccessKey = zapiAccessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

}