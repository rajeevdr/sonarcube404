package com.packt.cardatabase.web;

import com.google.gson.Gson;
import com.packt.cardatabase.domain.*;
import com.packt.cardatabase.domain.Module;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;


@RestController
public class WeeklyTestDesignProgressController {

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private ModuleRepository moduleRepository;

	@Autowired
	private WeeklyProjectLevelTestDesignProgressRepository weeklyProjectLevelTestDesignProgressRepository;

	@Autowired
	private WeeklyModuleLevelTestDesignProgressRepository weeklyModuleLevelTestDesignProgressRepository;

	@Autowired
	private WeekDataRepository weekDataRepository;


	@RequestMapping(value = "/projects/{id}/testDesign/weeklyProgress/week/{week}/year/{year}", method = RequestMethod.GET)
	@ResponseBody
	public String getWeeklyTestDesignProgress(@PathVariable("id") long id, @PathVariable(value = "week") String week, @PathVariable(value = "year") String year) {

		try {

			ArrayList<ProjectProgressPojo> progressPojoArray = new ArrayList<ProjectProgressPojo>();
			StringTokenizer tokenizer = new StringTokenizer(week, "-");
			tokenizer.nextToken();
			int weekNumber = Integer.parseInt(tokenizer.nextToken());
			int yearNumber = Integer.parseInt(year);
			int monthNumber = 0;

			Iterable<WeekData> weekDataIterable = weekDataRepository.findAll();
			Iterator<WeekData> weekDataIterator = weekDataIterable.iterator();

			WeekData nextWeek = null;

			while (weekDataIterator.hasNext()) {
				nextWeek = weekDataIterator.next();
				int tempWeekNumber = nextWeek.getWeekNumber();
				int tempYearNumber = nextWeek.getYearNumber();


				System.out.println("The actual week number is : " + tempWeekNumber);
				System.out.println("The actual year number is : " + tempYearNumber);

				if (((tempWeekNumber - weekNumber) == 0) && (tempYearNumber - yearNumber) == 0) {
					monthNumber = nextWeek.getMonthNumber();
					break;
				}
			}

			System.out.println("The desired week number is : " + weekNumber);
			System.out.println("The desired month number is : " + monthNumber);
			System.out.println("The desired year number is : " + yearNumber);

			boolean recordFound = false;

			Iterable<WeeklyProjectLevelTestDesignProgress> weeklyProjectLevelTestDesignProgress = weeklyProjectLevelTestDesignProgressRepository.findAll();
			Iterator<WeeklyProjectLevelTestDesignProgress> weeklyProjectLevelTestDesignProgressRepositoryIterator = weeklyProjectLevelTestDesignProgress.iterator();

			String response = null;

			try {

				while (weeklyProjectLevelTestDesignProgressRepositoryIterator.hasNext()) {
					WeeklyProjectLevelTestDesignProgress next = weeklyProjectLevelTestDesignProgressRepositoryIterator.next();

					int tempWeekNumber = next.getWeekNumber();
					int tempYearNumber = next.getYearNumber();

					if ((tempWeekNumber - weekNumber) == 0 && (next.getProject().getId() == id) && (tempYearNumber - yearNumber) == 0) {
						recordFound = true;

						ProjectProgressPojo projectProgressPojo = null;
						int index = 0;

						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getNumberOfTestableRequirementsCoveredThisWeekName());
						try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfTestableRequirementsCoveredThisWeek()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						projectProgressPojo.setCategory("Project Level Test Design Progress");
						progressPojoArray.add(projectProgressPojo);


						Iterable<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgress = weeklyModuleLevelTestDesignProgressRepository.findAll();
						Iterator<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgressIterator = weeklyModuleLevelTestDesignProgress.iterator();

						while (weeklyModuleLevelTestDesignProgressIterator.hasNext()) {

							WeeklyModuleLevelTestDesignProgress moduleProgress = weeklyModuleLevelTestDesignProgressIterator.next();

							if ((moduleProgress.getProject().getId() == id) && ( moduleProgress.getWeekNumber() == weekNumber ) && (next.getYearNumber() == yearNumber)) {

								index = index + 1;
								projectProgressPojo = new ProjectProgressPojo();
								projectProgressPojo.setId(index);
								projectProgressPojo.setName(moduleProgress.getModuleName() + "||" + moduleProgress.getNumberOfManualTestCasesDesignedForTheWeekName());
								try{ projectProgressPojo.setValue(Integer.toString(moduleProgress.getNumberOfManualTestCasesDesignedForTheWeek()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation");
								projectProgressPojo.setCategory("Module Level Test Design Progress");

								progressPojoArray.add(projectProgressPojo);
/*
								index = index + 1;
								projectProgressPojo = new ProjectProgressPojo();
								projectProgressPojo.setId(index);
								projectProgressPojo.setName(moduleProgress.getModuleName() + "||" + moduleProgress.getManualTestCasesDesignEffortForTheWeekName());
								try{ projectProgressPojo.setValue(Integer.toString(moduleProgress.getManualTestCasesDesignEffortForTheWeek ()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
								projectProgressPojo.setCategory("ModuleLevelTestDesignProgress");
								progressPojoArray.add(projectProgressPojo);
*/
								index = index + 1;
								projectProgressPojo = new ProjectProgressPojo();
								projectProgressPojo.setId(index);
								projectProgressPojo.setName(moduleProgress.getModuleName() + "||" + moduleProgress.getNumberOfRegressionAutomationTestScriptsDesignedForTheWeekName());
								try{ projectProgressPojo.setValue(Integer.toString(moduleProgress.getNumberOfRegressionAutomationTestScriptsDesignedForTheWeek()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation");
								projectProgressPojo.setCategory("Module Level Test Design Progress");
								progressPojoArray.add(projectProgressPojo);

								index = index + 1;
								projectProgressPojo = new ProjectProgressPojo();
								projectProgressPojo.setId(index);
								projectProgressPojo.setName(moduleProgress.getModuleName() + "||" + moduleProgress.getNumberOfSanityTestsDesignedForTheWeekName());
								try{ projectProgressPojo.setValue(Integer.toString(moduleProgress.getNumberOfSanityTestsDesignedForTheWeek()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation");
								projectProgressPojo.setCategory("Module Level Test Design Progress");
								progressPojoArray.add(projectProgressPojo);

/*
								index = index + 1;
								projectProgressPojo = new ProjectProgressPojo();
								projectProgressPojo.setId(index);
								projectProgressPojo.setName(moduleProgress.getModuleName() + "||" + moduleProgress.getRegressionAutomationDesignEffortForTheWeekName());
								try{ projectProgressPojo.setValue(Integer.toString(moduleProgress.getRegressionAutomationDesignEffortForTheWeek()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
								projectProgressPojo.setCategory("ModuleLevelTestDesignProgress");
								progressPojoArray.add(projectProgressPojo);
*/
							}
						}


						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getTotalNumberOfInProjectAutomationTestCasesAutomatedName());
						try{ projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfInProjectAutomationTestCasesAutomated()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						projectProgressPojo.setCategory("Project Level Test Design Progress");
						progressPojoArray.add(projectProgressPojo);

						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getTotalNumberOfAPIAutomationTestCasesAutomatedName());
						try{ projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfAPIAutomationTestCasesAutomated()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						projectProgressPojo.setCategory("Project Level Test Design Progress");
						progressPojoArray.add(projectProgressPojo);

						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getTotalNumberOfTestDesignReviewDefectsFoundName());
						try{ projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfTestDesignReviewDefectsFound()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						projectProgressPojo.setCategory("Attention Required");
						progressPojoArray.add(projectProgressPojo);

						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getTestPlanningEffortName());
						try{ projectProgressPojo.setValue(Integer.toString(next.getTestPlanningEffort()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						try{ projectProgressPojo.setStartDate(next.getTestPlanningEffortStartDate());}catch(Exception e){projectProgressPojo.setStartDate("");}
						try{ projectProgressPojo.setEndDate(next.getTestPlanningEffortEndDate());}catch(Exception e){projectProgressPojo.setEndDate("");}
						projectProgressPojo.setCategory("Effort");
						projectProgressPojo.setDays(true);

						progressPojoArray.add(projectProgressPojo);

						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getManualTestCasesDesignEffortName());
						try{ projectProgressPojo.setValue(Integer.toString(next.getManualTestCasesDesignEffort()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						try{ projectProgressPojo.setStartDate(next.getManualTestCasesDesignEffortStartDate());}catch(Exception e){projectProgressPojo.setStartDate("");}
						try{ projectProgressPojo.setEndDate(next.getManualTestCasesDesignEffortEndDate());}catch(Exception e){projectProgressPojo.setEndDate("");}
						projectProgressPojo.setCategory("Effort");
						projectProgressPojo.setDays(true);
						progressPojoArray.add(projectProgressPojo);

						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getRegressionAutomationDesignEffortName());
						try{ projectProgressPojo.setValue(Integer.toString(next.getRegressionAutomationDesignEffort()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						try{ projectProgressPojo.setStartDate(next.getRegressionAutomationDesignEffortStartDate());}catch(Exception e){projectProgressPojo.setStartDate("");}
						try{ projectProgressPojo.setEndDate(next.getRegressionAutomationDesignEffortEndDate());}catch(Exception e){projectProgressPojo.setEndDate("");}
						projectProgressPojo.setCategory("Effort");
						projectProgressPojo.setDays(true);
						progressPojoArray.add(projectProgressPojo);


						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getInProjectAutomationDesignEffortName());
						try{ projectProgressPojo.setValue(Integer.toString(next.getInProjectAutomationDesignEffort()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						try{ projectProgressPojo.setStartDate(next.getInProjectAutomationDesignEffortStartDate());}catch(Exception e){projectProgressPojo.setStartDate("");}
						try{ projectProgressPojo.setEndDate(next.getInProjectAutomationDesignEffortEndDate());}catch(Exception e){projectProgressPojo.setEndDate("");}
						projectProgressPojo.setCategory("Effort");
						projectProgressPojo.setDays(true);
						progressPojoArray.add(projectProgressPojo);


						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getApiAutomationDesignEffortName());
						try{ projectProgressPojo.setValue(Integer.toString(next.getApiAutomationDesignEffort()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						try{ projectProgressPojo.setStartDate(next.getApiAutomationDesignEffortStartDate());}catch(Exception e){projectProgressPojo.setStartDate("");}
						try{ projectProgressPojo.setEndDate(next.getApiAutomationDesignEffortEndDate());}catch(Exception e){projectProgressPojo.setEndDate("");}
						projectProgressPojo.setCategory("Effort");
						projectProgressPojo.setDays(true);
						progressPojoArray.add(projectProgressPojo);

						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getManualTestCasesMaintenanceEffortName());
						try{ projectProgressPojo.setValue(Integer.toString(next.getManualTestCasesMaintenanceEffort()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						try{ projectProgressPojo.setStartDate(next.getManualTestCasesMaintenanceEffortStartDate());}catch(Exception e){projectProgressPojo.setStartDate("");}
						try{ projectProgressPojo.setEndDate(next.getManualTestCasesMaintenanceEffortEndDate());}catch(Exception e){projectProgressPojo.setEndDate("");}
						projectProgressPojo.setCategory("Effort");
						projectProgressPojo.setDays(true);
						progressPojoArray.add(projectProgressPojo);


						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getAutomationTestCasesMaintenanceEffortName());
						try{ projectProgressPojo.setValue(Integer.toString(next.getAutomationTestCasesMaintenanceEffort()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						try{ projectProgressPojo.setStartDate(next.getAutomationTestCasesMaintenanceEffortStartDate());}catch(Exception e){projectProgressPojo.setStartDate("");}
						try{ projectProgressPojo.setEndDate(next.getAutomationTestCasesMaintenanceEffortEndDate());}catch(Exception e){projectProgressPojo.setEndDate("");}
						projectProgressPojo.setCategory("Effort");
						projectProgressPojo.setDays(true);
						progressPojoArray.add(projectProgressPojo);


						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getNumberOfResourcesAuthoringManualTestCasesName());
						try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfResourcesAuthoringManualTestCases()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						projectProgressPojo.setCategory("Effort");
						progressPojoArray.add(projectProgressPojo);

						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getNumberOfResourcesAuthoringAutomationTestCasesName());
						try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfResourcesAuthoringAutomationTestCases()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						projectProgressPojo.setCategory("Effort");
						progressPojoArray.add(projectProgressPojo);

						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getEnvironmentDowntimeDuringTestDesignPhaseName());
						try{ projectProgressPojo.setValue(Integer.toString(next.getEnvironmentDowntimeDuringTestDesignPhase()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						projectProgressPojo.setCategory("Attention Required");
						progressPojoArray.add(projectProgressPojo);

						Gson gson = new Gson();
						response = gson.toJson(progressPojoArray);
						response = "{\"progress\":" + response + "}";

						System.out.println(response);

						return response;
					}
				}
			}catch(Exception e){
				System.out.println("Exception occurred during get progress update : " + e.getMessage());
			}

			if(recordFound == false) {
				ProjectProgressPojo projectProgressPojo = null;
				WeeklyProjectLevelTestDesignProgress next = new WeeklyProjectLevelTestDesignProgress();

				int index = 0;

				Iterable<Module> allModules = moduleRepository.findAll();
				Iterator<Module> modulesIterator = allModules.iterator();
				WeeklyModuleLevelTestDesignProgress moduleProgress = new WeeklyModuleLevelTestDesignProgress();


				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getNumberOfTestableRequirementsCoveredThisWeekName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Project Level Test Design Progress");
				progressPojoArray.add(projectProgressPojo);

				while (modulesIterator.hasNext()) {
					Module module = modulesIterator.next();

					if (module.getProject().getId() == id) {
						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(module.getName() + "||" + moduleProgress.getNumberOfManualTestCasesDesignedForTheWeekName());
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation"); projectProgressPojo.setValue("0");
						projectProgressPojo.setCategory("Module Level Test Design Progress");
						progressPojoArray.add(projectProgressPojo);

/*
						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(module.getName() + "||" + moduleProgress.getManualTestCasesDesignEffortForTheWeekName());
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
						progressPojoArray.add(projectProgressPojo);
*/
						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(module.getName() + "||" + moduleProgress.getNumberOfRegressionAutomationTestScriptsDesignedForTheWeekName());
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation"); projectProgressPojo.setValue("0");
						projectProgressPojo.setCategory("Module Level Test Design Progress");
						progressPojoArray.add(projectProgressPojo);

						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(module.getName() + "||" + moduleProgress.getNumberOfSanityTestsDesignedForTheWeekName());
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation"); projectProgressPojo.setValue("0");
						projectProgressPojo.setCategory("Module Level Test Design Progress");
						progressPojoArray.add(projectProgressPojo);

/*
						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(module.getName() + "||" + moduleProgress.getRegressionAutomationDesignEffortForTheWeekName());
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
						progressPojoArray.add(projectProgressPojo);
*/
					}
				}


				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getTotalNumberOfInProjectAutomationTestCasesAutomatedName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Project Level Test Design Progress");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getTotalNumberOfAPIAutomationTestCasesAutomatedName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Project Level Test Design Progress");
				progressPojoArray.add(projectProgressPojo);


				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getTotalNumberOfTestDesignReviewDefectsFoundName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Attention Required");
				progressPojoArray.add(projectProgressPojo);


				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getTestPlanningEffortName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Effort");
				projectProgressPojo.setStartDate("");
				projectProgressPojo.setEndDate("");
				projectProgressPojo.setDays(true);
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getManualTestCasesDesignEffortName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Effort");
				projectProgressPojo.setStartDate("");
				projectProgressPojo.setEndDate("");
				projectProgressPojo.setDays(true);
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getRegressionAutomationDesignEffortName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Effort");
				projectProgressPojo.setStartDate("");
				projectProgressPojo.setEndDate("");
				projectProgressPojo.setDays(true);
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getInProjectAutomationDesignEffortName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Effort");
				projectProgressPojo.setStartDate("");
				projectProgressPojo.setEndDate("");
				projectProgressPojo.setDays(true);
				progressPojoArray.add(projectProgressPojo);


				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getApiAutomationDesignEffortName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Effort");
				projectProgressPojo.setStartDate("");
				projectProgressPojo.setEndDate("");
				projectProgressPojo.setDays(true);
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getManualTestCasesMaintenanceEffortName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Effort");
				projectProgressPojo.setStartDate("");
				projectProgressPojo.setEndDate("");
				projectProgressPojo.setDays(true);
				progressPojoArray.add(projectProgressPojo);


				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getAutomationTestCasesMaintenanceEffortName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Effort");
				projectProgressPojo.setStartDate("");
				projectProgressPojo.setEndDate("");
				projectProgressPojo.setDays(true);
				progressPojoArray.add(projectProgressPojo);




				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getNumberOfResourcesAuthoringManualTestCasesName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Effort");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getNumberOfResourcesAuthoringAutomationTestCasesName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Effort");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getEnvironmentDowntimeDuringTestDesignPhaseName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Attention Required");
				progressPojoArray.add(projectProgressPojo);

				Gson gson = new Gson();
				response = gson.toJson(progressPojoArray);
				response = "{\"progress\":" + response + "}";

				System.out.println(response);

				return response;


			}
		}catch(Exception e){

		}
		return null;
	}


	@RequestMapping(value = "/projects/{id}/testDesign/totalWeeklyProgress", method = RequestMethod.GET)
	@ResponseBody
	public String getTotalWeeklyTestDesignProgress(@PathVariable("id") long id) {

		try {

			ArrayList<ArrayList<ProjectProgressPojo>> weeklyProjectTestDesignProgressArray = new ArrayList<ArrayList<ProjectProgressPojo>>();

			Iterable<WeeklyProjectLevelTestDesignProgress> weeklyProjectLevelTestDesignProgress = weeklyProjectLevelTestDesignProgressRepository.findAll();
			Iterator<WeeklyProjectLevelTestDesignProgress> weeklyProjectLevelTestDesignProgressRepositoryIterator = weeklyProjectLevelTestDesignProgress.iterator();

			String response = null;

			try {

				while (weeklyProjectLevelTestDesignProgressRepositoryIterator.hasNext()) {
					WeeklyProjectLevelTestDesignProgress next = weeklyProjectLevelTestDesignProgressRepositoryIterator.next();
					if (next.getProject().getId() == id) {

						ArrayList<ProjectProgressPojo> progressPojoArray = new ArrayList<ProjectProgressPojo>();
						ProjectProgressPojo projectProgressPojo = null;
						int index = 0;


						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getWeekNumberName());
						try {
							projectProgressPojo.setValue(Integer.toString(next.getWeekNumber()));
						} catch (Exception e) {
							projectProgressPojo.setValue("0");
						}
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						projectProgressPojo.setCategory("ProjectLevelTestDesignProgress");
						progressPojoArray.add(projectProgressPojo);


						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getYearNumberName());
						try {
							projectProgressPojo.setValue(Integer.toString(next.getYearNumber()));
						} catch (Exception e) {
							projectProgressPojo.setValue("0");
						}
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						projectProgressPojo.setCategory("ProjectLevelTestDesignProgress");
						progressPojoArray.add(projectProgressPojo);


						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getNumberOfTestableRequirementsCoveredThisWeekName());
						try {
							projectProgressPojo.setValue(Integer.toString(next.getNumberOfTestableRequirementsCoveredThisWeek()));
						} catch (Exception e) {
							projectProgressPojo.setValue("0");
						}
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						projectProgressPojo.setCategory("ProjectLevelTestDesignProgress");
						progressPojoArray.add(projectProgressPojo);


						Iterable<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgress = weeklyModuleLevelTestDesignProgressRepository.findAll();
						Iterator<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgressIterator = weeklyModuleLevelTestDesignProgress.iterator();

						while (weeklyModuleLevelTestDesignProgressIterator.hasNext()) {

							WeeklyModuleLevelTestDesignProgress moduleProgress = weeklyModuleLevelTestDesignProgressIterator.next();

							if ((moduleProgress.getProject().getId() == id) &&
									(moduleProgress.getWeekNumber() == next.getWeekNumber()) &&
									(moduleProgress.getYearNumber() == next.getYearNumber())) {

								index = index + 1;
								projectProgressPojo = new ProjectProgressPojo();
								projectProgressPojo.setId(index);
								projectProgressPojo.setName(moduleProgress.getModuleName() + "||" + moduleProgress.getNumberOfManualTestCasesDesignedForTheWeekName());
								try {
									projectProgressPojo.setValue(Integer.toString(moduleProgress.getNumberOfManualTestCasesDesignedForTheWeek()));
								} catch (Exception e) {
									projectProgressPojo.setValue("0");
								}
								projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
								projectProgressPojo.setCategory("ModuleLevelTestDesignProgress");
								progressPojoArray.add(projectProgressPojo);
/*
								index = index + 1;
								projectProgressPojo = new ProjectProgressPojo();
								projectProgressPojo.setId(index);
								projectProgressPojo.setName(moduleProgress.getModuleName() + "||" + moduleProgress.getManualTestCasesDesignEffortForTheWeekName());
								try{ projectProgressPojo.setValue(Integer.toString(moduleProgress.getManualTestCasesDesignEffortForTheWeek ()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
								projectProgressPojo.setCategory("ModuleLevelTestDesignProgress");
								progressPojoArray.add(projectProgressPojo);
*/
								index = index + 1;
								projectProgressPojo = new ProjectProgressPojo();
								projectProgressPojo.setId(index);
								projectProgressPojo.setName(moduleProgress.getModuleName() + "||" + moduleProgress.getNumberOfRegressionAutomationTestScriptsDesignedForTheWeekName());
								try {
									projectProgressPojo.setValue(Integer.toString(moduleProgress.getNumberOfRegressionAutomationTestScriptsDesignedForTheWeek()));
								} catch (Exception e) {
									projectProgressPojo.setValue("0");
								}
								projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
								projectProgressPojo.setCategory("ModuleLevelTestDesignProgress");
								progressPojoArray.add(projectProgressPojo);

								index = index + 1;
								projectProgressPojo = new ProjectProgressPojo();
								projectProgressPojo.setId(index);
								projectProgressPojo.setName(moduleProgress.getModuleName() + "||" + moduleProgress.getNumberOfSanityTestsDesignedForTheWeekName());
								try {
									projectProgressPojo.setValue(Integer.toString(moduleProgress.getNumberOfSanityTestsDesignedForTheWeek()));
								} catch (Exception e) {
									projectProgressPojo.setValue("0");
								}
								projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
								projectProgressPojo.setCategory("ModuleLevelTestDesignProgress");
								progressPojoArray.add(projectProgressPojo);

/*
								index = index + 1;
								projectProgressPojo = new ProjectProgressPojo();
								projectProgressPojo.setId(index);
								projectProgressPojo.setName(moduleProgress.getModuleName() + "||" + moduleProgress.getRegressionAutomationDesignEffortForTheWeekName());
								try{ projectProgressPojo.setValue(Integer.toString(moduleProgress.getRegressionAutomationDesignEffortForTheWeek()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
								projectProgressPojo.setCategory("ModuleLevelTestDesignProgress");
								progressPojoArray.add(projectProgressPojo);
*/
							}
						}


						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getTotalNumberOfInProjectAutomationTestCasesAutomatedName());
						try {
							projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfInProjectAutomationTestCasesAutomated()));
						} catch (Exception e) {
							projectProgressPojo.setValue("0");
						}
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						projectProgressPojo.setCategory("ProjectLevelTestDesignProgress");
						progressPojoArray.add(projectProgressPojo);

						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getTotalNumberOfAPIAutomationTestCasesAutomatedName());
						try {
							projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfAPIAutomationTestCasesAutomated()));
						} catch (Exception e) {
							projectProgressPojo.setValue("0");
						}
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						projectProgressPojo.setCategory("ProjectLevelTestDesignProgress");
						progressPojoArray.add(projectProgressPojo);

						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getTotalNumberOfTestDesignReviewDefectsFoundName());
						try {
							projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfTestDesignReviewDefectsFound()));
						} catch (Exception e) {
							projectProgressPojo.setValue("0");
						}
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						projectProgressPojo.setCategory("AttentionRequired");
						progressPojoArray.add(projectProgressPojo);

						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getTestPlanningEffortName());
						try {
							projectProgressPojo.setValue(Integer.toString(next.getTestPlanningEffort()));
						} catch (Exception e) {
							projectProgressPojo.setValue("0");
						}
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						projectProgressPojo.setCategory("Effort");
						progressPojoArray.add(projectProgressPojo);

						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getManualTestCasesDesignEffortName());
						try {
							projectProgressPojo.setValue(Integer.toString(next.getManualTestCasesDesignEffort()));
						} catch (Exception e) {
							projectProgressPojo.setValue("0");
						}
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						projectProgressPojo.setCategory("Effort");
						progressPojoArray.add(projectProgressPojo);

						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getRegressionAutomationDesignEffortName());
						try {
							projectProgressPojo.setValue(Integer.toString(next.getRegressionAutomationDesignEffort()));
						} catch (Exception e) {
							projectProgressPojo.setValue("0");
						}
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						projectProgressPojo.setCategory("Effort");
						progressPojoArray.add(projectProgressPojo);


						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getInProjectAutomationDesignEffortName());
						try {
							projectProgressPojo.setValue(Integer.toString(next.getInProjectAutomationDesignEffort()));
						} catch (Exception e) {
							projectProgressPojo.setValue("0");
						}
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						projectProgressPojo.setCategory("Effort");
						progressPojoArray.add(projectProgressPojo);


						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getApiAutomationDesignEffortName());
						try {
							projectProgressPojo.setValue(Integer.toString(next.getApiAutomationDesignEffort()));
						} catch (Exception e) {
							projectProgressPojo.setValue("0");
						}
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						projectProgressPojo.setCategory("Effort");
						progressPojoArray.add(projectProgressPojo);

						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getManualTestCasesMaintenanceEffortName());
						try {
							projectProgressPojo.setValue(Integer.toString(next.getManualTestCasesMaintenanceEffort()));
						} catch (Exception e) {
							projectProgressPojo.setValue("0");
						}
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						projectProgressPojo.setCategory("Effort");
						progressPojoArray.add(projectProgressPojo);


						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getAutomationTestCasesMaintenanceEffortName());
						try {
							projectProgressPojo.setValue(Integer.toString(next.getAutomationTestCasesMaintenanceEffort()));
						} catch (Exception e) {
							projectProgressPojo.setValue("0");
						}
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						projectProgressPojo.setCategory("Effort");
						progressPojoArray.add(projectProgressPojo);


						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getNumberOfResourcesAuthoringManualTestCasesName());
						try {
							projectProgressPojo.setValue(Integer.toString(next.getNumberOfResourcesAuthoringManualTestCases()));
						} catch (Exception e) {
							projectProgressPojo.setValue("0");
						}
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						projectProgressPojo.setCategory("Effort");
						progressPojoArray.add(projectProgressPojo);

						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getNumberOfResourcesAuthoringAutomationTestCasesName());
						try {
							projectProgressPojo.setValue(Integer.toString(next.getNumberOfResourcesAuthoringAutomationTestCases()));
						} catch (Exception e) {
							projectProgressPojo.setValue("0");
						}
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						projectProgressPojo.setCategory("Effort");
						progressPojoArray.add(projectProgressPojo);

						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(next.getEnvironmentDowntimeDuringTestDesignPhaseName());
						try {
							projectProgressPojo.setValue(Integer.toString(next.getEnvironmentDowntimeDuringTestDesignPhase()));
						} catch (Exception e) {
							projectProgressPojo.setValue("0");
						}
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						projectProgressPojo.setCategory("AttentionRequired");
						progressPojoArray.add(projectProgressPojo);

						weeklyProjectTestDesignProgressArray.add(progressPojoArray);
					}
				}

						Gson gson = new Gson();
						response = gson.toJson(weeklyProjectTestDesignProgressArray);
						response = "{\"progress\":" + response + "}";

						System.out.println(response);

						return response;


			}catch(Exception e){
				System.out.println("Exception occurred during get progress update : " + e.getMessage());
			}

		}catch(Exception e){

		}
		return null;
	}



	@PostMapping(value = "/projects/{id}/testDesign/weeklyProgress/week/{week}/year/{year}")
	@ResponseBody
	public String updateWeeklyTestDesignProgress(@PathVariable("id") long id, @PathVariable(value = "week") String week, @PathVariable(value = "year") String year, @RequestBody List<ProjectProgressPojo> progressPojoArray) {

		try {

			StringTokenizer weekTokenizer = new StringTokenizer(week, "-");
			weekTokenizer.nextToken();
			int weekNumber = Integer.parseInt(weekTokenizer.nextToken());
			int yearNumber = Integer.parseInt(year);
			int monthNumber = 0;

			Iterable<WeekData> weekDataIterable = weekDataRepository.findAll();
			Iterator<WeekData> weekDataIterator = weekDataIterable.iterator();

			WeekData nextWeek = null;

			while (weekDataIterator.hasNext()) {
				nextWeek = weekDataIterator.next();
				if ((nextWeek.getWeekNumber() == weekNumber) && (nextWeek.getYearNumber() == yearNumber)) {
						monthNumber = nextWeek.getMonthNumber();
					break;
				}
			}


			Iterable<WeeklyProjectLevelTestDesignProgress> weeklyProjectLevelTestDesignProgress = weeklyProjectLevelTestDesignProgressRepository.findAll();
			Iterator<WeeklyProjectLevelTestDesignProgress> weeklyProjectLevelTestDesignProgressRepositoryIterator = weeklyProjectLevelTestDesignProgress.iterator();

			boolean matchFound = false;
			while (weeklyProjectLevelTestDesignProgressRepositoryIterator.hasNext()) {
				WeeklyProjectLevelTestDesignProgress next = weeklyProjectLevelTestDesignProgressRepositoryIterator.next();

				if ((next.getWeekNumber() == weekNumber) && (next.getProject().getId() == id) && (next.getYearNumber() == yearNumber))  {
					matchFound = true;

					for (int i = 0; i < progressPojoArray.size(); i++) {

						if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfTestableRequirementsCoveredThisWeekName())) {

							String previousEntry = Integer.toString(next.getNumberOfTestableRequirementsCoveredThisWeek());
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setNumberOfTestableRequirementsCoveredThisWeek(currentUpdatesEntry);
							}

						}
						 if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfInProjectAutomationTestCasesAutomatedName())) {

							String previousEntry = Integer.toString(next.getTotalNumberOfInProjectAutomationTestCasesAutomated());
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setTotalNumberOfInProjectAutomationTestCasesAutomated(currentUpdatesEntry);
								}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfAPIAutomationTestCasesAutomatedName())) {

							String previousEntry = Integer.toString(next.getTotalNumberOfAPIAutomationTestCasesAutomated());
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setTotalNumberOfAPIAutomationTestCasesAutomated(currentUpdatesEntry);
								}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfTestDesignReviewDefectsFoundName())) {

							String previousEntry = Integer.toString(next.getTotalNumberOfTestDesignReviewDefectsFound());
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setTotalNumberOfTestDesignReviewDefectsFound(currentUpdatesEntry);
								}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTestPlanningEffortName())) {
							String previousEntry = Integer.toString(next.getTestPlanningEffort());
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setTestPlanningEffort(currentUpdatesEntry);
								next.setTestPlanningEffortStartDate(progressPojoArray.get(i).getStartDate());
								next.setTestPlanningEffortEndDate(progressPojoArray.get(i).getEndDate());
								}


						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getManualTestCasesDesignEffortName())) {

							String previousEntry = Integer.toString(next.getManualTestCasesDesignEffort());
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setManualTestCasesDesignEffort(currentUpdatesEntry);
								next.setManualTestCasesDesignEffortStartDate(progressPojoArray.get(i).getStartDate());
								next.setManualTestCasesDesignEffortEndDate(progressPojoArray.get(i).getEndDate());
								}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getRegressionAutomationDesignEffortName())) {

							String previousEntry = Integer.toString(next.getRegressionAutomationDesignEffort());
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setRegressionAutomationDesignEffort(currentUpdatesEntry);
								next.setRegressionAutomationDesignEffortStartDate(progressPojoArray.get(i).getStartDate());
								next.setRegressionAutomationDesignEffortEndDate(progressPojoArray.get(i).getEndDate());
							}


						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getApiAutomationDesignEffortName())) {

							String previousEntry = Integer.toString(next.getApiAutomationDesignEffort());
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setApiAutomationDesignEffort(currentUpdatesEntry);
								next.setApiAutomationDesignEffortStartDate(progressPojoArray.get(i).getStartDate());
								next.setApiAutomationDesignEffortEndDate(progressPojoArray.get(i).getEndDate());
							}


						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getInProjectAutomationDesignEffortName())) {

							String previousEntry = Integer.toString(next.getInProjectAutomationDesignEffort());
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setInProjectAutomationDesignEffort(currentUpdatesEntry);
								next.setInProjectAutomationDesignEffortStartDate(progressPojoArray.get(i).getStartDate());
								next.setInProjectAutomationDesignEffortEndDate(progressPojoArray.get(i).getEndDate());
							}


						}   else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getManualTestCasesMaintenanceEffortName())) {
							String previousEntry = Integer.toString(next.getManualTestCasesMaintenanceEffort());
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setManualTestCasesMaintenanceEffort(currentUpdatesEntry);
								next.setManualTestCasesMaintenanceEffortStartDate(progressPojoArray.get(i).getStartDate());
								next.setManualTestCasesMaintenanceEffortEndDate(progressPojoArray.get(i).getEndDate());
								}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getAutomationTestCasesMaintenanceEffortName())) {
							String previousEntry = Integer.toString(next.getAutomationTestCasesMaintenanceEffort());
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setAutomationTestCasesMaintenanceEffort(currentUpdatesEntry);
								next.setAutomationTestCasesMaintenanceEffortStartDate(progressPojoArray.get(i).getStartDate());
								next.setAutomationTestCasesMaintenanceEffortEndDate(progressPojoArray.get(i).getEndDate());
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getEnvironmentDowntimeDuringTestDesignPhaseName())) {

							String previousEntry = Integer.toString(next.getEnvironmentDowntimeDuringTestDesignPhase());
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setEnvironmentDowntimeDuringTestDesignPhase(currentUpdatesEntry);
								}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfResourcesAuthoringManualTestCasesName())) {

							String previousEntry = Integer.toString(next.getNumberOfResourcesAuthoringManualTestCases());
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setNumberOfResourcesAuthoringManualTestCases(currentUpdatesEntry);
								}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfResourcesAuthoringAutomationTestCasesName())) {
							String previousEntry = Integer.toString(next.getNumberOfResourcesAuthoringAutomationTestCases());
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setNumberOfResourcesAuthoringAutomationTestCases(currentUpdatesEntry);
								}

						}
					}

					next.setWeekNumber(weekNumber);
					next.setMonthNumber(monthNumber);
					next.setYearNumber(yearNumber);

					weeklyProjectLevelTestDesignProgressRepository.save(next);
					break;
				}
			}

			if (matchFound == false) {
				WeeklyProjectLevelTestDesignProgress progress = null;

				Iterable<Project> allProjects = projectRepository.findAll();
				Iterator<Project> iterator = allProjects.iterator();

				while (iterator.hasNext()) {
					Project nextProject = iterator.next();

					if (nextProject.getId() == id) {
						progress = new WeeklyProjectLevelTestDesignProgress("0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", weekNumber, monthNumber, yearNumber, nextProject);
						break;
					}
				}

				for (int i = 0; i < progressPojoArray.size(); i++) {

					if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getNumberOfTestableRequirementsCoveredThisWeekName())) {

						String previousEntry = "";
						String currentUpdatesEntry = progressPojoArray.get(i).getValue();

						if (currentUpdatesEntry != previousEntry) {
							progress.setNumberOfTestableRequirementsCoveredThisWeek(currentUpdatesEntry);
						}

					}
					else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getTotalNumberOfInProjectAutomationTestCasesAutomatedName())) {

						String previousEntry = "";
						String currentUpdatesEntry = progressPojoArray.get(i).getValue();

						if (currentUpdatesEntry != previousEntry) {
							progress.setTotalNumberOfInProjectAutomationTestCasesAutomated(currentUpdatesEntry);
							}

					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getTotalNumberOfAPIAutomationTestCasesAutomatedName())) {

						String previousEntry = "";
						String currentUpdatesEntry = progressPojoArray.get(i).getValue();

						if (currentUpdatesEntry != previousEntry) {
							progress.setTotalNumberOfAPIAutomationTestCasesAutomated(currentUpdatesEntry);
							}

					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getTotalNumberOfTestDesignReviewDefectsFoundName())) {

						String previousEntry = "";
						String currentUpdatesEntry = progressPojoArray.get(i).getValue();

						if (currentUpdatesEntry != previousEntry) {
							progress.setTotalNumberOfTestDesignReviewDefectsFound(currentUpdatesEntry);
							}

					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getTestPlanningEffortName())) {
						String previousEntry = "";
						String currentUpdatesEntry = progressPojoArray.get(i).getValue();

						if (currentUpdatesEntry != previousEntry) {
							progress.setTestPlanningEffort(currentUpdatesEntry);
							progress.setTestPlanningEffortStartDate(progressPojoArray.get(i).getStartDate());
							progress.setTestPlanningEffortEndDate(progressPojoArray.get(i).getEndDate());
						}


					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getManualTestCasesDesignEffortName())) {

						String previousEntry = "";
						String currentUpdatesEntry = progressPojoArray.get(i).getValue();

						if (currentUpdatesEntry != previousEntry) {
							progress.setManualTestCasesDesignEffort(currentUpdatesEntry);
							progress.setManualTestCasesDesignEffortStartDate(progressPojoArray.get(i).getStartDate());
							progress.setManualTestCasesDesignEffortEndDate(progressPojoArray.get(i).getEndDate());
						}

					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getRegressionAutomationDesignEffortName())) {

						String previousEntry = "";
						String currentUpdatesEntry = progressPojoArray.get(i).getValue();

						if (currentUpdatesEntry != previousEntry) {
							progress.setRegressionAutomationDesignEffort(currentUpdatesEntry);
							progress.setRegressionAutomationDesignEffortStartDate(progressPojoArray.get(i).getStartDate());
							progress.setRegressionAutomationDesignEffortEndDate(progressPojoArray.get(i).getEndDate());
						}


					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getApiAutomationDesignEffortName())) {
						String previousEntry = "";
						String currentUpdatesEntry = progressPojoArray.get(i).getValue();

						if (currentUpdatesEntry != previousEntry) {
							progress.setApiAutomationDesignEffort(currentUpdatesEntry);
							progress.setApiAutomationDesignEffortStartDate(progressPojoArray.get(i).getStartDate());
							progress.setApiAutomationDesignEffortEndDate(progressPojoArray.get(i).getEndDate());
						}

					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getInProjectAutomationDesignEffortName())) {
						String previousEntry = "";
						String currentUpdatesEntry = progressPojoArray.get(i).getValue();

						if (currentUpdatesEntry != previousEntry) {
							progress.setInProjectAutomationDesignEffort(currentUpdatesEntry);
							progress.setInProjectAutomationDesignEffortStartDate(progressPojoArray.get(i).getStartDate());
							progress.setInProjectAutomationDesignEffortEndDate(progressPojoArray.get(i).getEndDate());
						}

					}    else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getManualTestCasesMaintenanceEffortName())) {
						String previousEntry = "";
						String currentUpdatesEntry = progressPojoArray.get(i).getValue();

						if (currentUpdatesEntry != previousEntry) {
							progress.setManualTestCasesMaintenanceEffort(currentUpdatesEntry);
							progress.setManualTestCasesMaintenanceEffortStartDate(progressPojoArray.get(i).getStartDate());
							progress.setManualTestCasesMaintenanceEffortEndDate(progressPojoArray.get(i).getEndDate());
						}

					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getAutomationTestCasesMaintenanceEffortName())) {
						String previousEntry = "";
						String currentUpdatesEntry = progressPojoArray.get(i).getValue();

						if (currentUpdatesEntry != previousEntry) {
							progress.setAutomationTestCasesMaintenanceEffort(currentUpdatesEntry);
							progress.setAutomationTestCasesMaintenanceEffortStartDate(progressPojoArray.get(i).getStartDate());
							progress.setAutomationTestCasesMaintenanceEffortEndDate(progressPojoArray.get(i).getEndDate());
						}

					}else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getEnvironmentDowntimeDuringTestDesignPhaseName())) {

						String previousEntry = "";
						String currentUpdatesEntry = progressPojoArray.get(i).getValue();

						if (currentUpdatesEntry != previousEntry) {
							progress.setEnvironmentDowntimeDuringTestDesignPhase(currentUpdatesEntry);
							}

					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getNumberOfResourcesAuthoringManualTestCasesName())) {

						String previousEntry = "";
						String currentUpdatesEntry = progressPojoArray.get(i).getValue();

						if (currentUpdatesEntry != previousEntry) {
							progress.setNumberOfResourcesAuthoringManualTestCases(currentUpdatesEntry);
							}

					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getNumberOfResourcesAuthoringAutomationTestCasesName())) {
						String previousEntry = "";
						String currentUpdatesEntry = progressPojoArray.get(i).getValue();

						if (currentUpdatesEntry != previousEntry) {
							progress.setNumberOfResourcesAuthoringAutomationTestCases(currentUpdatesEntry);
							}

					}



				}

				progress.setWeekNumber(weekNumber);
				progress.setMonthNumber(monthNumber);
				progress.setYearNumber(yearNumber);

				weeklyProjectLevelTestDesignProgressRepository.save(progress);
			}


			for (int i = 0; i < progressPojoArray.size(); i++) {

				WeeklyModuleLevelTestDesignProgress tempModuleProgress = new WeeklyModuleLevelTestDesignProgress();

				if (progressPojoArray.get(i).getName().contains(tempModuleProgress.getNumberOfManualTestCasesDesignedForTheWeekName())) {
					WeeklyModuleLevelTestDesignProgress moduleProgress = null;

					StringTokenizer tokenizer = new StringTokenizer(progressPojoArray.get(i).getName(), "||");
					String moduleName = tokenizer.nextToken();

					boolean moduleFound = false;

					Iterable<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgressIterable = weeklyModuleLevelTestDesignProgressRepository.findAll();
					Iterator<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgressIterator = weeklyModuleLevelTestDesignProgressIterable.iterator();

					while (weeklyModuleLevelTestDesignProgressIterator.hasNext()) {

						moduleProgress = weeklyModuleLevelTestDesignProgressIterator.next();

						if ((moduleProgress.getProject().getId() == id) &&
								(moduleProgress.getWeekNumber() == weekNumber) &&
								(moduleProgress.getModuleName().compareTo(moduleName) == 0) &&
								(moduleProgress.getYearNumber() == yearNumber)) {
							moduleFound = true;
							break;
						}

					}

					if (moduleFound == false) {
						moduleProgress = new WeeklyModuleLevelTestDesignProgress();

						moduleProgress.setModuleName(moduleName);
						moduleProgress.setWeekNumber(weekNumber);
						moduleProgress.setMonthNumber(monthNumber);
						moduleProgress.setYearNumber(yearNumber);

						Iterable<Project> allProjects = projectRepository.findAll();
						Iterator<Project> iterator = allProjects.iterator();


						while (iterator.hasNext()) {
							Project nextProject = iterator.next();
							if (nextProject.getId() == id) {
								moduleProgress.setProject(nextProject);
								break;
							}
						}
						moduleProgress.setNumberOfManualTestCasesDesignedForTheWeek(progressPojoArray.get(i).getValue());
						//moduleProgress.setAutomationRegressionCasesDesignedForTheWeek(0);
						weeklyModuleLevelTestDesignProgressRepository.save(moduleProgress);

					} else {
						moduleProgress.setModuleName(moduleName);
						moduleProgress.setWeekNumber(weekNumber);
						moduleProgress.setMonthNumber(monthNumber);
						moduleProgress.setYearNumber(yearNumber);
						moduleProgress.setNumberOfManualTestCasesDesignedForTheWeek(progressPojoArray.get(i).getValue());
						weeklyModuleLevelTestDesignProgressRepository.save(moduleProgress);
					}
				}else if (progressPojoArray.get(i).getName().contains(tempModuleProgress.getNumberOfRegressionAutomationTestScriptsDesignedForTheWeekName())) {
					WeeklyModuleLevelTestDesignProgress moduleProgress = null;

					StringTokenizer tokenizer = new StringTokenizer(progressPojoArray.get(i).getName(), "||");
					String moduleName = tokenizer.nextToken();

					boolean moduleFound = false;

					Iterable<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgressIterable = weeklyModuleLevelTestDesignProgressRepository.findAll();
					Iterator<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgressIterator = weeklyModuleLevelTestDesignProgressIterable.iterator();

					while (weeklyModuleLevelTestDesignProgressIterator.hasNext()) {

						moduleProgress = weeklyModuleLevelTestDesignProgressIterator.next();

						if ((moduleProgress.getProject().getId() == id) &&
								(moduleProgress.getWeekNumber() == weekNumber) &&
								(moduleProgress.getModuleName().compareTo(moduleName) == 0) &&
								(moduleProgress.getYearNumber() == yearNumber)) {
							moduleFound = true;
							break;
						}

					}

					if (moduleFound == false) {
						moduleProgress = new WeeklyModuleLevelTestDesignProgress();

						moduleProgress.setModuleName(moduleName);
						moduleProgress.setWeekNumber(weekNumber);
						moduleProgress.setMonthNumber(monthNumber);
						moduleProgress.setYearNumber(yearNumber);

						Iterable<Project> allProjects = projectRepository.findAll();
						Iterator<Project> iterator = allProjects.iterator();


						while (iterator.hasNext()) {
							Project nextProject = iterator.next();
							if (nextProject.getId() == id) {
								moduleProgress.setProject(nextProject);
								break;
							}
						}

						moduleProgress.setNumberOfRegressionAutomationTestScriptsDesignedForTheWeek(progressPojoArray.get(i).getValue());
						//moduleProgress.setNumberOfManualTestCasesDesignedForTheWeek(0);
						weeklyModuleLevelTestDesignProgressRepository.save(moduleProgress);


					} else {
						moduleProgress.setModuleName(moduleName);
						moduleProgress.setWeekNumber(weekNumber);
						moduleProgress.setMonthNumber(monthNumber);
						moduleProgress.setYearNumber(yearNumber);
						moduleProgress.setNumberOfRegressionAutomationTestScriptsDesignedForTheWeek(progressPojoArray.get(i).getValue());
						weeklyModuleLevelTestDesignProgressRepository.save(moduleProgress);
					}
				}
				else if (progressPojoArray.get(i).getName().contains(tempModuleProgress.getNumberOfSanityTestsDesignedForTheWeekName())) {
					WeeklyModuleLevelTestDesignProgress moduleProgress = null;

					StringTokenizer tokenizer = new StringTokenizer(progressPojoArray.get(i).getName(), "||");
					String moduleName = tokenizer.nextToken();

					boolean moduleFound = false;

					Iterable<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgressIterable = weeklyModuleLevelTestDesignProgressRepository.findAll();
					Iterator<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgressIterator = weeklyModuleLevelTestDesignProgressIterable.iterator();

					while (weeklyModuleLevelTestDesignProgressIterator.hasNext()) {

						moduleProgress = weeklyModuleLevelTestDesignProgressIterator.next();

						if ((moduleProgress.getProject().getId() == id) &&
								(moduleProgress.getWeekNumber() == weekNumber) &&
								(moduleProgress.getModuleName().compareTo(moduleName) == 0) &&
								(moduleProgress.getYearNumber() == yearNumber)) {
							moduleFound = true;
							break;
						}

					}

					if (moduleFound == false) {
						moduleProgress = new WeeklyModuleLevelTestDesignProgress();

						moduleProgress.setModuleName(moduleName);
						moduleProgress.setWeekNumber(weekNumber);
						moduleProgress.setMonthNumber(monthNumber);
						moduleProgress.setYearNumber(yearNumber);

						Iterable<Project> allProjects = projectRepository.findAll();
						Iterator<Project> iterator = allProjects.iterator();


						while (iterator.hasNext()) {
							Project nextProject = iterator.next();
							if (nextProject.getId() == id) {
								moduleProgress.setProject(nextProject);
								break;
							}
						}

						moduleProgress.setNumberOfSanityTestsDesignedForTheWeek(progressPojoArray.get(i).getValue());
						//moduleProgress.setNumberOfManualTestCasesDesignedForTheWeek(0);
						weeklyModuleLevelTestDesignProgressRepository.save(moduleProgress);


					} else {
						moduleProgress.setModuleName(moduleName);
						moduleProgress.setWeekNumber(weekNumber);
						moduleProgress.setMonthNumber(monthNumber);
						moduleProgress.setYearNumber(yearNumber);
						moduleProgress.setNumberOfSanityTestsDesignedForTheWeek(progressPojoArray.get(i).getValue());
						weeklyModuleLevelTestDesignProgressRepository.save(moduleProgress);
					}
				}
            }



		}catch(Exception e){
			System.out.println("Exception occured during module progress update : " + e.getMessage());
		}
		return "OK";
	}

}