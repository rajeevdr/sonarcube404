package com.packt.cardatabase.domain;



public class WeeklyModuleLevelTestExecutionProgressPojo {


    private String numberOfManualTestCasesExecutedForTheWeek;
    private String manualTestExecutionEffortForTheWeek;
    private String numberOfRegAutoTestsExecutedForTheWeek;
    private String regressionAutomationExecutionEffortForTheWeek;
    private String defectsForTheWeek;
    private String moduleName;



    public WeeklyModuleLevelTestExecutionProgressPojo() {
    }

    public WeeklyModuleLevelTestExecutionProgressPojo(String moduleName,
                                                  String numberOfManualTestCasesExecutedForTheWeek,
                                                  String manualTestExecutionEffortForTheWeek,
                                                  String numberOfRegAutoTestsExecutedForTheWeek,
                                                  String regressionAutomationExecutionEffortForTheWeek,
                                                  String defectsForTheWeek) {
        super();
        this.moduleName = moduleName;
        this.numberOfManualTestCasesExecutedForTheWeek = numberOfManualTestCasesExecutedForTheWeek;
        this.manualTestExecutionEffortForTheWeek = manualTestExecutionEffortForTheWeek;
        this.numberOfRegAutoTestsExecutedForTheWeek = numberOfRegAutoTestsExecutedForTheWeek;
        this.regressionAutomationExecutionEffortForTheWeek = regressionAutomationExecutionEffortForTheWeek;
        this.defectsForTheWeek = defectsForTheWeek;

    }



    public String getNumberOfManualTestCasesExecutedForTheWeek() {
        return this.numberOfManualTestCasesExecutedForTheWeek;
    }

    public String getNumberOfManualTestCasesExecutedForTheWeekName() {
        return "NumberOfManualTestCasesExecutedForTheWeek";
    }

    public String getManualTestExecutionEffortForTheWeek() {
        return manualTestExecutionEffortForTheWeek;
    }

    public String getManualTestExecutionEffortForTheWeekName() {
        return "ManualTestExecutionEffortForTheWeek";
    }

    public void setManualTestExecutionEffortForTheWeek(String manualTestExecutionEffortForTheWeek) {
        this.manualTestExecutionEffortForTheWeek = manualTestExecutionEffortForTheWeek;
    }


    public void setNumberOfManualTestCasesExecutedForTheWeek(String numberOfManualTestCasesExecutedForTheWeek) {
        this.numberOfManualTestCasesExecutedForTheWeek = numberOfManualTestCasesExecutedForTheWeek;
    }


    public String getNumberOfRegressionAutomationTestCasesExecutedForTheWeek() {
        return this.numberOfRegAutoTestsExecutedForTheWeek;
    }

    public String getNumberOfRegressionAutomationTestCasesExecutedForTheWeekName() {
        return "NumberOfRegressionAutomationTestCasesExecutedForTheWeek";
    }

    public void setNumberOfRegressionAutomationTestCasesExecutedForTheWeek(String numberOfRegressionAutomationTestCasesExecutedForTheWeek) {
        this.numberOfRegAutoTestsExecutedForTheWeek = numberOfRegressionAutomationTestCasesExecutedForTheWeek;
    }


    public String getRegressionAutomationExecutionEffortForTheWeek() {
        return this.regressionAutomationExecutionEffortForTheWeek;
    }

    public String getRegressionAutomationExecutionEffortForTheWeekName() {
        return "RegressionAutomationExecutionEffortForTheWeek";
    }

    public void setRegressionAutomationExecutionEffortForTheWeek(String regressionAutomationExecutionEffortForTheWeek) {
        this.regressionAutomationExecutionEffortForTheWeek = regressionAutomationExecutionEffortForTheWeek;
    }


    public String getDefectsForTheWeek() {
        return defectsForTheWeek;
    }

    public String getDefectsForTheWeekName() {
        return "DefectsForTheWeek";
    }

    public void setDefectsForTheWeek(String defectsForTheWeek) {
        this.defectsForTheWeek = defectsForTheWeek;
    }

}