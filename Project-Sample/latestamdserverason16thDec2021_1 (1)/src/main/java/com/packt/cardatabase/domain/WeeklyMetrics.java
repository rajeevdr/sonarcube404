package com.packt.cardatabase.domain;

import javax.persistence.*;

@Entity
public class WeeklyMetrics {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private String automationUtilization;
    private String manualTestCaseAuthoringProductivity;
    private String regressionTestAutomationScriptingProductivity;
    private String apiTestAutomationScriptingProductivity;
    private String inProjectAutomationScriptingProductivity;
    private String designCoverage;
    private String sanityAutomationCoverage;
    private String percentageOfRegressionAutomation;
    private String testReviewEfficiency;
    private String downtimeAndImpactAnalysisDesignAvgProductivityLost;
    private String manualTestExecutionProductivity;
    private String regressionAutomationTestExecutionProductivity;
    private String inProjectAutomationTestExecutionProductivity;
    private String apiAutomationTestExecutionProductivity;
    private String migratedDataExecution;
    private String defectRejection;
    private String testCaseNotExecutedManual;
    private String testCaseNotExecutedAutomation;
    private String currentQualityRatio;
    private String qualityOfFixes;
    private String downtimeAndImpactAnalysisExecutionAvgProductivityLost;
    private String defectLeakageSITToUAT;
    private String defectLeakageToProduction;
    private String numberOfTestableRequirementsTillDate;
    private int weekNumber;
    private int monthNumber;
    private int yearNumber;

    private String  numberOfDefectsReportedByTestingTeamforTheWeek;
    private String  numberOfDefectsRejectedForTheWeek;
    private String  totalNumberOfDefectsReopenedForTheWeek;
    private String  numberOfValidDefectsLoggedInSITForTheWeek;
    private String  numberOfValidDefectsLoggedInUATForTheWeek;
    private String manualTestCaseExecutionEffort;
    private String regressionAutomationTestCaseExecutionEffort;
    private String inProjectAutomationTestCaseExecutionEffort;
    private String apiAutomationTestCaseExecutionEffort;
    private String percentageOfPendingRegressionAutomation;
    private String percentageOfRequirementsNotCovered;
    private String averageTimeTakenToResolveSev1Defects;
    private String numberOfDefectsFoundUsingAutomationSoFar;



    private String automationUtilizationStatus;
    private String manualTestCaseAuthoringProductivityStatus;
    private String regressionTestAutomationScriptingProductivityStatus;
    private String apiTestAutomationScriptingProductivityStatus;
    private String inProjectAutomationScriptingProductivityStatus;
    private String designCoverageStatus;
    private String sanityAutomationCoverageStatus;
    private String percentageOfRegressionAutomationStatus;
    private String testReviewEfficiencyStatus;
    private String manualTestExecutionProductivityStatus;
    private String regressionAutomationTestExecutionProductivityStatus;
    private String inProjectAutomationTestExecutionProductivityStatus;
    private String apiAutomationTestExecutionProductivityStatus;
    private String defectRejectionStatus;
    private String currentQualityRatioStatus;
    private String qualityOfFixesStatus;
    private String defectLeakageSITToUATStatus;
    private String defectLeakageToProductionStatus;
    private String designProductivityLostStatus;
    private String migratedDataExecutionStatus;
    private String testCaseNotExecutedManualStatus;
    private String testCaseNotExecutedAutomationStatus;
    private String executionProductivityLostStatus;
    private String percentageOfPendingRegressionAutomationStatus;
    private String percentageOfRequirementsNotCoveredStatus;
    private String averageTimeTakenToResolveSev1DefectsStatus;
    private String numberOfDefectsRejectedForTheWeekStatus;
    private String totalNumberOfDefectsReopenedForTheWeekStatus;





    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public WeeklyMetrics() {}

    public WeeklyMetrics(String automationUtilization,
                         String manualTestCaseAuthoringProductivity,
                         String regressionTestAutomationScriptingProductivity,
                         String apiTestAutomationScriptingProductivity,
                         String inProjectAutomationScriptingProductivity,
                         String designCoverage,
                         String sanityAutomationCoverage,
                         String percentageOfRegressionAutomation,
                         String testReviewEfficiency,
                         String downtimeAndImpactAnalysisDesignAvgProductivityLost,
                         String manualTestExecutionProductivity,
                         String regressionAutomationTestExecutionProductivity,
                         String inProjectAutomationTestExecutionProductivity,
                         String apiAutomationTestExecutionProductivity,
                         String migratedDataExecution,
                         String defectRejection,
                         String testCaseNotExecutedManual,
                         String testCaseNotExecutedAutomation,
                         String currentQualityRatio,
                         String qualityOfFixes,
                         String downtimeAndImpactAnalysisExecutionAvgProductivityLost,
                         String numberOfTestableRequirementsTillDate,
                         String defectLeakageSITToUAT,
                         String defectLeakageToProduction,
                         String  numberOfDefectsReportedByTestingTeamforTheWeek,
                         String  numberOfDefectsRejectedForTheWeek,
                         String  totalNumberOfDefectsReopenedForTheWeek,
                         String  numberOfValidDefectsLoggedInSITForTheWeek,
                         String  numberOfValidDefectsLoggedInUATForTheWeek,
                         String manualTestCaseExecutionEffort,
                         String regressionAutomationTestCaseExecutionEffort,
                         String inProjectAutomationTestCaseExecutionEffort,
                         String apiAutomationTestCaseExecutionEffort,
                         String averageTimeTakenToResolveSev1Defects,
                         int weekNumber,
                         int yearNumber,
                         Project project) {

        super();
        this.automationUtilization = automationUtilization;
        this.manualTestCaseAuthoringProductivity = manualTestCaseAuthoringProductivity;
        this.regressionTestAutomationScriptingProductivity = regressionTestAutomationScriptingProductivity;
        this.apiTestAutomationScriptingProductivity = apiTestAutomationScriptingProductivity;
        this.inProjectAutomationScriptingProductivity = inProjectAutomationScriptingProductivity;
        this.designCoverage =  designCoverage;
        this.sanityAutomationCoverage =  sanityAutomationCoverage;
        this.percentageOfRegressionAutomation =  percentageOfRegressionAutomation;
        this.testReviewEfficiency =  testReviewEfficiency;
        this.downtimeAndImpactAnalysisDesignAvgProductivityLost =  downtimeAndImpactAnalysisDesignAvgProductivityLost;
        this.manualTestExecutionProductivity =  manualTestExecutionProductivity;
        this.regressionAutomationTestExecutionProductivity =  regressionAutomationTestExecutionProductivity;
        this.inProjectAutomationTestExecutionProductivity =  inProjectAutomationTestExecutionProductivity;
        this.apiAutomationTestExecutionProductivity =  apiAutomationTestExecutionProductivity;
        this.migratedDataExecution =  migratedDataExecution;
        this.defectRejection =  defectRejection;
        this.testCaseNotExecutedManual =  testCaseNotExecutedManual;
        this.testCaseNotExecutedAutomation =  testCaseNotExecutedAutomation;
        this.currentQualityRatio =  currentQualityRatio;
        this.qualityOfFixes =  qualityOfFixes;
        this.downtimeAndImpactAnalysisExecutionAvgProductivityLost =  downtimeAndImpactAnalysisExecutionAvgProductivityLost;
        this.defectLeakageSITToUAT = defectLeakageSITToUAT;
        this.defectLeakageToProduction = defectLeakageToProduction;
        this.numberOfTestableRequirementsTillDate = numberOfTestableRequirementsTillDate;

        this.numberOfDefectsReportedByTestingTeamforTheWeek =  numberOfDefectsReportedByTestingTeamforTheWeek;
        this.numberOfDefectsRejectedForTheWeek =  numberOfDefectsRejectedForTheWeek;
        this.totalNumberOfDefectsReopenedForTheWeek = totalNumberOfDefectsReopenedForTheWeek;
        this.numberOfValidDefectsLoggedInSITForTheWeek = numberOfValidDefectsLoggedInSITForTheWeek;
        this.numberOfValidDefectsLoggedInUATForTheWeek = numberOfValidDefectsLoggedInUATForTheWeek;

        this.manualTestCaseExecutionEffort =  manualTestCaseExecutionEffort;
        this.regressionAutomationTestCaseExecutionEffort =  regressionAutomationTestCaseExecutionEffort;
        this.inProjectAutomationTestCaseExecutionEffort = inProjectAutomationTestCaseExecutionEffort;
        this.apiAutomationTestCaseExecutionEffort = apiAutomationTestCaseExecutionEffort;
        this.averageTimeTakenToResolveSev1Defects = averageTimeTakenToResolveSev1Defects;

        this.weekNumber = weekNumber;
        this.yearNumber = yearNumber;
        this.project = project;
    }

    public int getMonthNumber() {
        return monthNumber;
    }
    public String getMonthNumberName() {
        return "Month";
    }
    public void setMonthNumber(int monthNumber) {
        this.monthNumber = monthNumber;
    }

    public String getAutomationUtilization() {
        return automationUtilization;
    }

    public void setAutomationUtilization(String automationUtilization) {
        this.automationUtilization = automationUtilization;
    }

    public String getNumberOfDefectsReportedByTestingTeamforTheWeek(){
        return numberOfDefectsReportedByTestingTeamforTheWeek;
    }

    public void setNumberOfDefectsReportedByTestingTeamforTheWeek(String numberOfDefectsReportedByTestingTeamforTheWeek){
        this.numberOfDefectsReportedByTestingTeamforTheWeek = numberOfDefectsReportedByTestingTeamforTheWeek;
    }

    public static String getNumberOfDefectsReportedByTestingTeamforTheWeekName() {
        return "Total number Of Defects Reported By Testing Team for the Week";
    }


    public String getManualTestCaseExecutionEffort() {
        return manualTestCaseExecutionEffort;
    }

    public static String getManualTestCaseExecutionEffortName() {
        return "Manual TestCase Execution Effort";
    }


    public void setManualTestCaseExecutionEffort(String manualTestCaseExecutionEffort) {
        this.manualTestCaseExecutionEffort = manualTestCaseExecutionEffort;
    }



    public static String getPercentageOfPendingRegressionAutomationName() {
        return "% Pending Regression Automation";
    }


    public String getPercentageOfPendingRegressionAutomation() {
        return percentageOfPendingRegressionAutomation;
    }


    public String getRegressionAutomationTestCaseExecutionEffort() {
        return regressionAutomationTestCaseExecutionEffort;
    }

    public static String getRegressionAutomationTestCaseExecutionEffortName() {
        return "Regression Automation TestCase Execution Effort";
    }

    public void setRegressionAutomationTestCaseExecutionEffort(String regressionAutomationTestCaseExecutionEffort) {
        this.regressionAutomationTestCaseExecutionEffort = regressionAutomationTestCaseExecutionEffort;
    }

    public String getInProjectAutomationTestCaseExecutionEffort() {
        return inProjectAutomationTestCaseExecutionEffort;
    }

    public static String getInProjectAutomationTestCaseExecutionEffortName() {
        return "InProject Automation TestCase Execution Effort";
    }


    public void setInProjectAutomationTestCaseExecutionEffort(String inProjectAutomationTestCaseExecutionEffort) {
        this.inProjectAutomationTestCaseExecutionEffort = inProjectAutomationTestCaseExecutionEffort;
    }

    public String getAPIAutomationTestCaseExecutionEffort() {
        return apiAutomationTestCaseExecutionEffort;
    }

    public static String getAPIAutomationTestCaseExecutionEffortName() {
        return "API Automation TestCase Execution Effort";
    }


    public void setAPIAutomationTestCaseExecutionEffort(String apiAutomationTestCaseExecutionEffort) {
        this.apiAutomationTestCaseExecutionEffort = apiAutomationTestCaseExecutionEffort;
    }


    public String getNumberOfDefectsRejectedForTheWeek(){
        return numberOfDefectsRejectedForTheWeek;
    }

    public void setNumberOfDefectsRejectedForTheWeek(String numberOfDefectsRejectedForTheWeek){
        this.numberOfDefectsRejectedForTheWeek = numberOfDefectsRejectedForTheWeek;
    }

    public static String getNumberOfDefectsRejectedForTheWeekName() {
        return "Total number Of Defects rejected By Testing Team for the Week";
    }

    public String getTotalNumberOfDefectsReopenedForTheWeek(){
        return totalNumberOfDefectsReopenedForTheWeek;
    }

    public void setTotalNumberOfDefectsReopenedForTheWeek(String totalNumberOfDefectsReopenedForTheWeek){
        this.totalNumberOfDefectsReopenedForTheWeek = totalNumberOfDefectsReopenedForTheWeek;
    }

    public static String getTotalNumberOfDefectsReopenedForTheWeekName() {
        return "Total number Of Defects reopened By Testing Team for the Week";
    }


    public String getNumberOfValidDefectsLoggedInSITForTheWeek(){
        return numberOfValidDefectsLoggedInSITForTheWeek;
    }

    public void setNumberOfValidDefectsLoggedInSITForTheWeek(String numberOfValidDefectsLoggedInSITForTheWeek){
        this.numberOfValidDefectsLoggedInSITForTheWeek = numberOfValidDefectsLoggedInSITForTheWeek;
    }

    public static String getNumberOfValidDefectsLoggedInSITForTheWeekName() {
        return "Total number Of Valid Defects Logged in SIT for the Week";
    }

    public String getNumberOfValidDefectsLoggedInUATForTheWeek(){
        return numberOfValidDefectsLoggedInUATForTheWeek;
    }

    public void setNumberOfValidDefectsLoggedInUATForTheWeek(String numberOfValidDefectsLoggedInUATForTheWeek){
        this.numberOfValidDefectsLoggedInUATForTheWeek = numberOfValidDefectsLoggedInUATForTheWeek;
    }

    public static String getNumberOfValidDefectsLoggedInUATForTheWeekName() {
        return "Total number Of Valid Defects Logged in UAT for the Week";
    }


    public int getWeekNumber() {
        return weekNumber;
    }
    public String getWeekNumberName() {
        return "Week";
    }
    public void setWeekNumber(int weekNumber) {
        this.weekNumber = weekNumber;
    }

    public int getYearNumber() {
        return yearNumber;
    }
    public String getYearNumberName() {
        return "Year";
    }
    public void setYearNumber(int yearNumber) {
        this.yearNumber = yearNumber;
    }

    public String getNumberOfTestableRequirementsTillDate(){
        return numberOfTestableRequirementsTillDate;
    }

    public void setNumberOfTestableRequirementsTillDate(String numberOfTestableRequirementsTillDate){
        this.numberOfTestableRequirementsTillDate = numberOfTestableRequirementsTillDate;
    }

    public void setManualTestCaseAuthoringProductivity(String manualTestCaseAuthoringProductivity) {
        this.manualTestCaseAuthoringProductivity = manualTestCaseAuthoringProductivity;
    }

    public String getManualTestCaseAuthoringProductivity() {
        return this.manualTestCaseAuthoringProductivity;
    }



    public String getRegressionTestAutomationScriptingProductivity() {
        return regressionTestAutomationScriptingProductivity;
    }

    public void setRegressionTestScriptingProductivity(String regressionTestAutomationScriptingProductivity) {
        this.regressionTestAutomationScriptingProductivity = regressionTestAutomationScriptingProductivity;
    }

    public String getApiTestAutomationScriptingProductivity() {
        return apiTestAutomationScriptingProductivity;
    }

    public void setApiAutomationTestScriptingProductivity(String apiTestAutomationScriptingProductivity) {
        this.apiTestAutomationScriptingProductivity = apiTestAutomationScriptingProductivity;
    }

    public String getInProjectAutomationScriptingProductivity() {
        return inProjectAutomationScriptingProductivity;
    }

    public void setInProjectTestScriptingProductivity(String inProjectAutomationScriptingProductivity) {
        this.inProjectAutomationScriptingProductivity = inProjectAutomationScriptingProductivity;
    }


    public String getDesignCoverage(){
        return designCoverage;
    }
    public String getSanityAutomationCoverage(){
        return sanityAutomationCoverage;
    }


    public String getPercentageOfRegressionAutomation(){
        return percentageOfRegressionAutomation;
    }

    public String getTestReviewEfficiency(){
        return testReviewEfficiency;
    }

    public String getDowntimeAndImpactAnalysisDesignAvgProductivityLost(){
        return downtimeAndImpactAnalysisDesignAvgProductivityLost;
    }

    public String getManualTestExecutionProductivity(){
        return manualTestExecutionProductivity;
    }

    public String getRegressionAutomationTestExecutionProductivity(){
        return regressionAutomationTestExecutionProductivity;
    }

    public String getInProjectAutomationTestExecutionProductivity(){
        return inProjectAutomationTestExecutionProductivity;
    }

    public String getAPIAutomationTestExecutionProductivity(){
        return apiAutomationTestExecutionProductivity;
    }

    public String getMigratedDataExecution(){
        return migratedDataExecution;
    }

    public String getDefectRejection(){
        return defectRejection;
    }

    public String getTestCaseNotExecutedManual(){
        return testCaseNotExecutedManual;
    }

    public String getTestCaseNotExecutedAutomation(){
        return testCaseNotExecutedAutomation;
    }

    public String getCurrentQualityRatio(){
        return currentQualityRatio;
    }

    public String getQualityOfFixes(){
        return qualityOfFixes;
    }

    public String getDowntimeAndImpactAnalysisExecutionAvgProductivityLost(){
        return downtimeAndImpactAnalysisExecutionAvgProductivityLost;
    }

    public String getDefectLeakageSITToUAT(){
        return defectLeakageSITToUAT;
    }

    public String getDefectLeakageToProduction(){
        return defectLeakageToProduction;
    }

    public void setDesignCoverage(String designCoverage){
        this.designCoverage = designCoverage;
    }

    public void setSanityAutomationCoverage(String sanityAutomationCoverage){
        this.sanityAutomationCoverage = sanityAutomationCoverage;
    }


    public void setPercentageOfRegressionAutomation(String percentageOfRegressionAutomation){
        this.percentageOfRegressionAutomation = percentageOfRegressionAutomation;
    }

    public void setPercentageOfPendingRegressionAutomation(String percentageOfPendingRegressionAutomation){
        this.percentageOfPendingRegressionAutomation = percentageOfPendingRegressionAutomation;
    }
    public void setTestReviewEfficiency(String testReviewEfficiency){
        this.testReviewEfficiency = testReviewEfficiency;
    }

    public void setDowntimeAndImpactAnalysisDesignAvgProductivityLost(String downtimeAndImpactAnalysisDesignAvgProductivityLost){
        this.downtimeAndImpactAnalysisDesignAvgProductivityLost = downtimeAndImpactAnalysisDesignAvgProductivityLost;
    }

    public void setManualTestExecutionProductivity(String manualTestExecutionProductivity){
        this.manualTestExecutionProductivity = manualTestExecutionProductivity;
    }


    public void setRegressionAutomationTestExecutionProductivity(String regressionAutomationTestExecutionProductivity){
        this.regressionAutomationTestExecutionProductivity = regressionAutomationTestExecutionProductivity;
    }

    public void setInProjectAutomationTestExecutionProductivity(String inProjectAutomationTestExecutionProductivity){
        this.inProjectAutomationTestExecutionProductivity = inProjectAutomationTestExecutionProductivity;
    }

    public void setAPIAutomationTestExecutionProductivity(String apiAutomationTestExecutionProductivity){
        this.apiAutomationTestExecutionProductivity = apiAutomationTestExecutionProductivity;
    }

    public void setMigratedDataExecution(String migratedDataExecution){
        this.migratedDataExecution = migratedDataExecution;
    }

    public void setDefectRejection(String defectRejection){
        this.defectRejection = defectRejection;
    }

    public void setTestCaseNotExecutedManual(String testCaseNotExecutedManual){
        this.testCaseNotExecutedManual = testCaseNotExecutedManual;
    }

    public void setTestCaseNotExecutedAutomation(String testCaseNotExecutedAutomation){
        this.testCaseNotExecutedAutomation = testCaseNotExecutedAutomation;
    }

    public void setCurrentQualityRatio(String currentQualityRatio){
        this.currentQualityRatio = currentQualityRatio;
    }

    public void setQualityOfFixes(String qualityOfFixes){
        this.qualityOfFixes = qualityOfFixes;
    }

    public void setDowntimeAndImpactAnalysisExecutionAvgProductivityLost(String downtimeAndImpactAnalysisExecutionAvgProductivityLost){
        this.downtimeAndImpactAnalysisExecutionAvgProductivityLost = downtimeAndImpactAnalysisExecutionAvgProductivityLost;
    }


    public void setDefectLeakageSITToUAT(String defectLeakageSITToUAT){
        this.defectLeakageSITToUAT = defectLeakageSITToUAT;
    }

    public void setDefectLeakageToProduction(String defectLeakageToProduction){
        this.defectLeakageToProduction = defectLeakageToProduction;
    }


    public static String getManualTestCaseAuthoringProductivityName() {
        return "Test Case Authoring Productivity (M)";
    }

    public static String getAutomationTestCaseScriptingProductivityName() {
        return "Test Case Authoring Productivity (A)";
    }


    public static String getRegressionTestAutomationScriptingProductivityName() {
        return "Regression Test Automation Productivity";
    }

    public static String getApiTestAutomationScriptingProductivityName() {
        return "Api Test Automation Scripting Productivity";
    }

    public static String getInProjectAutomationScriptingProductivityName() {
        return "In Project Test Automation Scripting Productivity";
    }

    public static String getDesignCoverageName(){
        return "Design Coverage";
    }

    public static String getSanityAutomationCoverageName(){
        return "Sanity Automation Coverage";
    }

    public static String getPercentageOfRequirementsNotCoveredName(){
        return "% Requirements not covered";
    }


    public static String getPercentageOfRegressionAutomationName(){
        return "% Regression Automation";
    }

    public static String getTestReviewEfficiencyName(){
        return "Test Review Efficiency";
    }


    public static String getDowntimeAndImpactAnalysisDesignAvgProductivityLostName(){
        return "Downtime and Impact analysis (Design) – Avg Productivity Lost";
    }

    public static String getManualTestExecutionProductivityName(){
        return "Test Execution Productivity (M)";
    }

    public static String getAutomationTestExecutionProductivityName(){
        return "Test Execution Productivity (A)";
    }


    public static String getRegressionAutomationTestExecutionProductivityName(){
        return "Test Execution Productivity (A- Regression)";
    }

    public static String getInProjectAutomationTestExecutionProductivityName(){
        return "Test Execution Productivity (A- In-Project)";
    }


    public static String getAPIAutomationTestExecutionProductivityName(){
        return "Test Execution Productivity (A-API)";
    }

    public static String getMigratedDataExecutionName(){
        return "Migrated data Execution % - by function/feature against Test data";
    }

    public static String getDefectRejectionName(){
        return "Defect Rejection";
    }

    public static String getTestCaseNotExecutedManualName(){
        return "Test Case Not Executed(Manual)";
    }

    public static String getTestCaseNotExecutedAutomationName(){
        return "Test Case Not Executed(Automation)";
    }

    public static String getCurrentQualityRatioName(){
        return "Current Quality Ratio";
    }

    public static String getQualityOfFixesName(){
        return "Quality of Fixes";
    }

    public static String getDowntimeAndImpactAnalysisExecutionAvgProductivityLostName(){
        return "Downtime and Impact analysis (Execution)– Avg Productivity Lost";
    }

    public static String getNumberOfTestableRequirementsTillDateName(){
        return "Number of testable requirements Cummulative Till Date";
    }

    public static String getDefectLeakageSITToUATName(){
        return "Defect Leakage SIT To UAT";
    }

    public static String getDefectLeakageToProductionName(){
        return "Defect Leakage To Production";
    }


    public static String getPercentageOfPendingRegressionAutomationStatusName(){
        return "Percentage Of Pending Regression Automation Status";
    }

    public static String getPercentageOfRequirementsNotCoveredStatusName(){
        return "Percentage Of Requirements Not Covered Status";
    }

    public static String getAverageTimeTakenToResolveSev1DefectsStatusName(){
        return "Average Time Taken To Resolve Sev1 Defects Status";
    }


    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }


    public String getAutomationUtilizationStatus() {
        return automationUtilizationStatus;
    }

    public void setAutomationUtilizationStatus(String automationUtilizationStatus) {
        this.automationUtilizationStatus = automationUtilizationStatus;
    }

    public void setManualTestCaseAuthoringProductivityStatus(String manualTestCaseAuthoringProductivityStatus) {
        this.manualTestCaseAuthoringProductivityStatus = manualTestCaseAuthoringProductivityStatus;
    }

    public String getManualTestCaseAuthoringProductivityStatus() {
        return this.manualTestCaseAuthoringProductivityStatus;
    }


    public String getRegressionTestAutomationScriptingProductivityStatus() {
        return regressionTestAutomationScriptingProductivityStatus;
    }

    public void setRegressionTestScriptingProductivityStatus(String regressionTestAutomationScriptingProductivityStatus) {
        this.regressionTestAutomationScriptingProductivityStatus = regressionTestAutomationScriptingProductivityStatus;
    }

    public String getApiTestAutomationScriptingProductivityStatus() {
        return apiTestAutomationScriptingProductivityStatus;
    }

    public void setApiAutomationTestScriptingProductivityStatus(String apiTestAutomationScriptingProductivityStatus) {
        this.apiTestAutomationScriptingProductivityStatus = apiTestAutomationScriptingProductivityStatus;
    }

    public String getInProjectTestAutomationScriptingProductivityStatus() {
        return inProjectAutomationScriptingProductivityStatus;
    }

    public void setInProjectTestAutomationScriptingProductivityStatus(String inProjectAutomationScriptingProductivityStatus) {
        this.inProjectAutomationScriptingProductivityStatus = inProjectAutomationScriptingProductivityStatus;
    }


    public String getDesignCoverageStatus(){
        return designCoverageStatus;
    }

    public String getSanityAutomationCoverageStatus(){
        return sanityAutomationCoverageStatus;
    }

    public String getPercentageOfRegressionAutomationStatus(){
        return percentageOfRegressionAutomationStatus;
    }

    public String getPercentageOfRequirementsNotCovered(){
        return percentageOfRequirementsNotCovered;
    }

    public void setPercentageOfRequirementsNotCovered(String percentageOfRequirementsNotCovered) {
        this.percentageOfRequirementsNotCovered = percentageOfRequirementsNotCovered;
    }


    public String getTestReviewEfficiencyStatus(){
        return testReviewEfficiencyStatus;
    }


    public String getManualTestExecutionProductivityStatus(){
        return manualTestExecutionProductivityStatus;
    }



    public String getRegressionAutomationTestExecutionProductivityStatus(){
        return regressionAutomationTestExecutionProductivityStatus;
    }

    public String getInProjectAutomationTestExecutionProductivityStatus(){
        return inProjectAutomationTestExecutionProductivityStatus;
    }

    public String getAPIAutomationTestExecutionProductivityStatus(){
        return apiAutomationTestExecutionProductivityStatus;
    }



    public String getDefectRejectionStatus(){
        return defectRejectionStatus;
    }



    public String getCurrentQualityRatioStatus(){
        return currentQualityRatioStatus;
    }

    public String getQualityOfFixesStatus(){
        return qualityOfFixesStatus;
    }




    public String getDefectLeakageSITToUATStatus(){
        return defectLeakageSITToUATStatus;
    }

    public String getDefectLeakageToProductionStatus(){
        return defectLeakageToProductionStatus;
    }

    public void setDesignCoverageStatus(String designCoverageStatus){
        this.designCoverageStatus = designCoverageStatus;
    }

    public void setSanityAutomationCoverageStatus(String sanityAutomationCoverageStatus){
        this.sanityAutomationCoverageStatus = sanityAutomationCoverageStatus;
    }


    public void setPercentageOfRegressionAutomationStatus(String percentageOfRegressionAutomationStatus){
        this.percentageOfRegressionAutomationStatus = percentageOfRegressionAutomationStatus;
    }

    public void setTestReviewEfficiencyStatus(String testReviewEfficiencyStatus){
        this.testReviewEfficiencyStatus = testReviewEfficiencyStatus;
    }


    public void setManualTestExecutionProductivityStatus(String manualTestExecutionProductivityStatus){
        this.manualTestExecutionProductivityStatus = manualTestExecutionProductivityStatus;
    }

    public void setRegressionAutomationTestExecutionProductivityStatus(String regressionAutomationTestExecutionProductivityStatus){
        this.regressionAutomationTestExecutionProductivityStatus = regressionAutomationTestExecutionProductivityStatus;
    }

    public void setInProjectAutomationTestExecutionProductivityStatus(String inProjectAutomationTestExecutionProductivityStatus){
        this.inProjectAutomationTestExecutionProductivityStatus = inProjectAutomationTestExecutionProductivityStatus;
    }

    public void setAPIAutomationTestExecutionProductivityStatus(String apiAutomationTestExecutionProductivityStatus){
        this.apiAutomationTestExecutionProductivityStatus = apiAutomationTestExecutionProductivityStatus;
    }


    public void setDefectRejectionStatus(String defectRejectionStatus){
        this.defectRejectionStatus = defectRejectionStatus;
    }


    public void setCurrentQualityRatioStatus(String currentQualityRatioStatus){
        this.currentQualityRatioStatus = currentQualityRatioStatus;
    }

    public void setQualityOfFixesStatus(String qualityOfFixesStatus){
        this.qualityOfFixesStatus = qualityOfFixesStatus;
    }


    public void setDefectLeakageSITToUATStatus(String defectLeakageSITToUATStatus){
        this.defectLeakageSITToUATStatus = defectLeakageSITToUATStatus;
    }

    public void setDefectLeakageToProductionStatus(String defectLeakageToProductionStatus){
        this.defectLeakageToProductionStatus = defectLeakageToProductionStatus;
    }


    public void setMigratedDataExecutionStatus(String migratedDataExecutionStatus){
        this.migratedDataExecutionStatus = migratedDataExecutionStatus;
    }

    public void setTestCaseNotExecutedManualStatus(String testCaseNotExecutedManualStatus){
        this.testCaseNotExecutedManualStatus = testCaseNotExecutedManualStatus;
    }

    public void setTestCaseNotExecutedAutomationStatus(String testCaseNotExecutedAutomationStatus){
        this.testCaseNotExecutedAutomationStatus = testCaseNotExecutedAutomationStatus;
    }


    public String getMigratedDataExecutionStatus(){
        return this.migratedDataExecutionStatus;
    }

    public String getTestCaseNotExecutedManualStatus(){
        return this.testCaseNotExecutedManualStatus;
    }

    public String getTestCaseNotExecutedAutomationStatus(){
        return this.testCaseNotExecutedAutomationStatus;
    }



    public void setDowntimeAndImpactAnalysisDesignAvgProductivityLostStatus(String designProductivityLostStatus){
        this.designProductivityLostStatus = designProductivityLostStatus;
    }

    public String getDowntimeAndImpactAnalysisDesignAvgProductivityLostStatus(){
        return designProductivityLostStatus;
    }

    public void setDowntimeAndImpactAnalysisExecutionAvgProductivityLostStatus(String executionProductivityLostStatus){
        this.executionProductivityLostStatus = executionProductivityLostStatus;
    }

    public String getDowntimeAndImpactAnalysisExecutionAvgProductivityLostStatus(){
        return this.executionProductivityLostStatus;
    }



    public void setPercentageOfPendingRegressionAutomationStatus(String percentageOfPendingRegressionAutomationStatus){
        this.percentageOfPendingRegressionAutomationStatus = percentageOfPendingRegressionAutomationStatus;
    }

    public String getPercentageOfPendingRegressionAutomationStatus(){
        return this.percentageOfPendingRegressionAutomationStatus;
    }

    public void setPercentageOfRequirementsNotCoveredStatus(String percentageOfRequirementsNotCoveredStatus){
        this.percentageOfRequirementsNotCoveredStatus = percentageOfRequirementsNotCoveredStatus;
    }

    public String getPercentageOfRequirementsNotCoveredStatus(){
        return this.percentageOfRequirementsNotCoveredStatus;
    }

    public void setAverageTimeTakenToResolveSev1DefectsStatus(String averageTimeTakenToResolveSev1DefectsStatus){
        this.averageTimeTakenToResolveSev1DefectsStatus = averageTimeTakenToResolveSev1DefectsStatus;
    }

    public String getAverageTimeTakenToResolveSev1DefectsStatus(){
        return this.averageTimeTakenToResolveSev1DefectsStatus;
    }

    public void setNumberOfDefectsRejectedForTheWeekStatus(String numberOfDefectsRejectedForTheWeekStatus){
        this.numberOfDefectsRejectedForTheWeekStatus = numberOfDefectsRejectedForTheWeekStatus;
    }

    public String getNumberOfDefectsRejectedForTheWeekStatus(){
        return this.numberOfDefectsRejectedForTheWeekStatus;
    }

    public void setTotalNumberOfDefectsReopenedForTheWeekStatus(String totalNumberOfDefectsReopenedForTheWeekStatus){
        this.totalNumberOfDefectsReopenedForTheWeekStatus = totalNumberOfDefectsReopenedForTheWeekStatus;
    }

    public String getTotalNumberOfDefectsReopenedForTheWeekStatus(){
        return this.totalNumberOfDefectsReopenedForTheWeekStatus;
    }


    public String getAverageTimeTakenToResolveSev1Defects() {
        return averageTimeTakenToResolveSev1Defects;
    }

    public static String getAverageTimeTakenToResolveSev1DefectsName() {
        return "AverageTimeTakenToResolveSev1Defects";
    }

    public void setAverageTimTakenToResolveSev1Defects(String averageTimeTakenToResolveSev1Defects) {
        this.averageTimeTakenToResolveSev1Defects = averageTimeTakenToResolveSev1Defects;
    }


    public String getNumberOfDefectsFoundUsingAutomationSoFar() {
        return numberOfDefectsFoundUsingAutomationSoFar;
    }

    public static String getNumberOfDefectsFoundUsingAutomationSoFarName() {
        return "Number Of Defects Found Using Automation So Far";
    }

    public void setNumberOfDefectsFoundUsingAutomationSoFar(String numberOfDefectsFoundUsingAutomationSoFar) {
        this.numberOfDefectsFoundUsingAutomationSoFar = numberOfDefectsFoundUsingAutomationSoFar;
    }

}