package com.packt.cardatabase.domain;

import javax.persistence.*;
import java.util.ArrayList;

@Entity
public class AutomationTestExecutionProductivity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private ArrayList<String> numberOfAutomationScriptsExecutedEachWeek;
    private ArrayList<String> moduleNames;
    private String effortSpentOnAutomationTestExecutionEachWeek;
    private String automationTestExecutionProductivity;
    private Integer weekNumber;
    private Integer yearNumber;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public AutomationTestExecutionProductivity() {}

    public AutomationTestExecutionProductivity(ArrayList<String> numberOfAutomationScriptsExecutedEachWeek,
                                               String effortSpentOnAutomationTestExecutionEachWeek,
                                               Integer weekNumber,
                                               Integer yearNumber,
                                               Project project) {

        super();
        this.numberOfAutomationScriptsExecutedEachWeek = numberOfAutomationScriptsExecutedEachWeek;
        this.effortSpentOnAutomationTestExecutionEachWeek = effortSpentOnAutomationTestExecutionEachWeek;
        this.weekNumber = weekNumber;
        this.yearNumber = yearNumber;
        this.project = project;
    }

    public Integer getWeekNumber() {
        return weekNumber;
    }
    public String getWeekNumberName() {
        return "Week";
    }
    public void setWeekNumber(Integer weekNumber) {
        this.weekNumber = weekNumber;
    }

    public Integer getYearNumber() {
        return yearNumber;
    }
    public String getYearNumberName() {
        return "Year";
    }
    public void setYearNumber(Integer yearNumber) {
        this.yearNumber = yearNumber;
    }


    public String getAutomationTestExecutionProductivity() {
        return automationTestExecutionProductivity;
    }
    public String getAutomationTestExecutionProductivityName() {
        return "Automation Test Execution Productivity";
    }
    public void setAutomationTestExecutionProductivity(String automationTestExecutionProductivity) {
        this.automationTestExecutionProductivity = automationTestExecutionProductivity;
    }

    public ArrayList<String> getNumberOfAutomationScriptsExecutedEachWeek(){
        return numberOfAutomationScriptsExecutedEachWeek;
    }
    public String getNumberOfAutomationScriptsExecutedEachWeekName(){
        return "Number Of Automation Scripts Executed";
    }
    public void setNumberOfAutomationScriptsExecutedEachWeek(ArrayList<String> numberOfAutomationScriptsExecutedEachWeek){
        this.numberOfAutomationScriptsExecutedEachWeek = numberOfAutomationScriptsExecutedEachWeek;
    }

    public ArrayList<String> getModuleNames(){
        return moduleNames;
    }
    public String getModuleNamesName(){
        return "Module";
    }
    public void setModuleNames(ArrayList<String> moduleNames){
        this.moduleNames = moduleNames;
    }

    public String  getEffortSpentOnAutomationTestExecutionEachWeek() {
        return effortSpentOnAutomationTestExecutionEachWeek;
    }

    public String getEffortSpentOnAutomationTestExecutionEachWeekName(){
        return "Effort Spent On Automation Test Execution";
    }

    public void setEffortSpentOnAutomationTestExecutionEachWeek(String effortSpentOnAutomationTestExecutionEachWeek) {
        this.effortSpentOnAutomationTestExecutionEachWeek = effortSpentOnAutomationTestExecutionEachWeek;
    }


    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

}