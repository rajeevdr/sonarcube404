package com.packt.cardatabase.domain;


import javax.persistence.*;

@Entity
public class WeekData {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private int weekNumber;
    private int monthNumber;
    private int yearNumber;
    private String weekStartDate;
    private String weekEndDate;
    private String weekDays;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public WeekData() {}


    public WeekData(int weekNumber,int monthNumber, int yearNumber,  String weekStartDate, String weekEndDate, String weekDays) {
        super();
        this.weekNumber = weekNumber;
        this.yearNumber = yearNumber;
        this.weekStartDate = weekStartDate;
        this.weekEndDate = weekEndDate;
        this.monthNumber = monthNumber;
        this.weekDays = weekDays;
    }


    public int getWeekNumber() {
        return weekNumber;
    }

    public void setWeekNumber(int weekNumber) {
        this.weekNumber = weekNumber;
    }

    public int getMonthNumber() {
        return monthNumber;
    }

    public void setMonthNumber(int monthNumber) {
        this.monthNumber = monthNumber;
    }

    public int getYearNumber() {
        return yearNumber;
    }

    public void setYearNumber(int yearNumber) {
        this.yearNumber = yearNumber;
    }

    public String getWeekStartDate() {
        return weekStartDate;
    }

    public void setWeekStartDate(String weekStartDate) {
        this.weekStartDate = weekStartDate;
    }

    public String getWeekEndDate() {
        return weekEndDate;
    }

    public void setWeekEndDate(String weekEndDate) {
        this.weekEndDate = weekEndDate;
    }


    public String getWeekDays() {
        return weekDays;
    }

    public void setWeekDays(String weekDays) {
        this.weekDays = weekDays;
    }


}