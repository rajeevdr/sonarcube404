package com.packt.cardatabase.domain;

import javax.persistence.*;

@Entity
public class ModuleLevelMetrics {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;
    private long moduleId;
    private String moduleName;
    private int numberOfManualTestCasesAuthored;
    private int numberOfRegressionAutomationTestCasesScripted;
    private int numberOfManualTestCasesExecuted;
    private int numberOfRegressionAutomationTestCasesExecuted;
    private String manualTestCaseAuthoringProductivity;
    private String automationTestCaseScriptingProductivity;
    private String manualTestExecutionProductivity;
    private String automationTestExecutionProductivity;
    private String defectsForTheWeek;
    private int weekNumber;
    private int yearNumber;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public ModuleLevelMetrics() {}

    public ModuleLevelMetrics(Long moduleId,
                              String moduleName,
                              String manualTestCaseAuthoringProductivity,
                              String automationTestCaseScriptingProductivity,
                              String manualTestExecutionProductivity,
                              String automationTestExecutionProductivity,
                              String defectsForTheWeek,
                              int weekNumber,
                              int yearNumber,
                              Project project) {


        super();
        this.moduleId =  moduleId;
        this.moduleName =  moduleName;
        this.manualTestCaseAuthoringProductivity = manualTestCaseAuthoringProductivity;
        this.automationTestCaseScriptingProductivity = automationTestCaseScriptingProductivity;
        this.manualTestExecutionProductivity =  manualTestExecutionProductivity;
        this.automationTestExecutionProductivity =  automationTestExecutionProductivity;
        this.defectsForTheWeek =  defectsForTheWeek;
        this.weekNumber = weekNumber;
        this.yearNumber = yearNumber;
        this.project = project;
    }

    public long getModuleId() {
        return moduleId;
    }
    public void setModuleId(long moduleId) {
        this.moduleId = moduleId;
    }

    public int getWeekNumber() {
        return weekNumber;
    }
    public String getWeekNumberName() {
        return "WeekNumber";
    }
    public void setWeekNumber(int weekNumber) {
        this.weekNumber = weekNumber;
    }

    public int getYearNumber() {
        return yearNumber;
    }
    public String getYearNumberName() {
        return "YearNumber";
    }
    public void setYearNumber(int yearNumber) {
        this.yearNumber = yearNumber;
    }

    public String getModuleName() {
        return moduleName;
    }
    public String getModuleNameString() {
        return "ModuleName";
    }
    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getManualTestCaseAuthoringProductivity() {
        return this.manualTestCaseAuthoringProductivity;
    }

    public void setManualTestCaseAuthoringProductivity(String manualTestCaseAuthoringProductivity) {
        this.manualTestCaseAuthoringProductivity = manualTestCaseAuthoringProductivity;
    }


    public int getNumberOfManualTestCasesAuthored() {
        return this.numberOfManualTestCasesAuthored;
    }

    public void setNumberOfManualTestCasesAuthored(int numberOfManualTestCasesAuthored) {
        this.numberOfManualTestCasesAuthored = numberOfManualTestCasesAuthored;
    }

    public int getNumberOfRegressionAutomationTestCasesScripted() {
        return this.numberOfRegressionAutomationTestCasesScripted;
    }

    public void setNumberOfRegressionAutomationTestCasesScripted(int numberOfRegressionAutomationTestCasesScripted) {
        this.numberOfRegressionAutomationTestCasesScripted = numberOfRegressionAutomationTestCasesScripted;
    }

    public int getNumberOfManualTestCasesExecuted() {
        return this.numberOfManualTestCasesExecuted;
    }

    public void setNumberOfManualTestCasesExecuted(int numberOfManualTestCasesExecuted) {
        this.numberOfManualTestCasesExecuted = numberOfManualTestCasesExecuted;
    }

    public int getNumberOfRegressionAutomationTestCasesExecuted() {
        return this.numberOfRegressionAutomationTestCasesExecuted;
    }

    public void setNumberOfRegressionAutomationTestCasesExecuted(int numberOfRegressionAutomationTestCasesExecuted) {
        this.numberOfRegressionAutomationTestCasesExecuted = numberOfRegressionAutomationTestCasesExecuted;
    }


    public String getAutomationTestCaseScriptingProductivity() {
        return automationTestCaseScriptingProductivity;
    }

    public void setAutomationTestCaseScriptingProductivity(String automationTestCaseScriptingProductivity) {
        this.automationTestCaseScriptingProductivity = automationTestCaseScriptingProductivity;
    }

    public String getManualTestExecutionProductivity(){
        return manualTestExecutionProductivity;
    }

    public void setManualTestExecutionProductivity(String manualTestExecutionProductivity){
        this.manualTestExecutionProductivity = manualTestExecutionProductivity;
    }

    public String getAutomationTestExecutionProductivity(){
        return automationTestExecutionProductivity;
    }

    public void setAutomationTestExecutionProductivity(String automationTestExecutionProductivity){
        this.automationTestExecutionProductivity = automationTestExecutionProductivity;
    }

    public String getManualTestCaseAuthoringProductivityName() {
        return "Test Case Authoring Productivity (M)";
    }

    public String getAutomationTestCaseScriptingProductivityName() {
        return "Test Case Authoring Productivity (A)";
    }

    public String getManualTestExecutionProductivityName(){
        return "Test Execution Productivity (M)";
    }

    public String getAutomationTestExecutionProductivityName(){
        return "Test Execution Productivity (A)";
    }

    public String getDefectsForTheWeek() {
        return defectsForTheWeek;
    }

    public String getDefectsForTheWeekName() {
        return "DefectsForTheWeek";
    }

    public void setDefectsForTheWeek(String defectsForTheWeek) {
        this.defectsForTheWeek = defectsForTheWeek;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

}