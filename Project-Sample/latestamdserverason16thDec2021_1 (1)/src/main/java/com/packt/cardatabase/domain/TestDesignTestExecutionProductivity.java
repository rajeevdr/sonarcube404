package com.packt.cardatabase.domain;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class TestDesignTestExecutionProductivity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private String complexity;
    private String complexityName;
    private String manualTestDesignProductivity;
    private String overallAutomationTestDesignProductivity;
    private String regressionTestDesignProductivity;
    private String inProjectTestDesignProductivity;
    private String apiAutomationTestDesignProductivity;
    private String manualTestExecutionProductivity;
    private String overallAutomationTestExecutionProductivity;
    private String regressionTestExecutionProductivity;
    private String inProjectTestExecutionProductivity;
    private String apiAutomationTestExecutionProductivity;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public TestDesignTestExecutionProductivity() {}


    public TestDesignTestExecutionProductivity(String complexity,
                                  String complexityName,
                                  String manualTestDesignProductivity,
                                  String overallAutomationTestDesignProductivity,
                                  String regressionTestDesignProductivity,
                                  String inProjectTestDesignProductivity,
                                  String apiAutomationTestDesignProductivity,
                                  String manualTestExecutionProductivity,
                                  String overallAutomationTestExecutionProductivity,
                                  String regressionTestExecutionProductivity,
                                  String inProjectTestExecutionProductivity,
                                  String apiAutomationTestExecutionProductivity)
    {
        super();
        this.complexity = complexity;
        this.complexityName = complexityName;
        this.manualTestDesignProductivity = manualTestDesignProductivity;
        this.overallAutomationTestDesignProductivity = overallAutomationTestDesignProductivity;
        this.regressionTestDesignProductivity = regressionTestDesignProductivity;
        this.inProjectTestDesignProductivity = inProjectTestDesignProductivity;
        this.apiAutomationTestDesignProductivity = apiAutomationTestDesignProductivity;
        this.manualTestExecutionProductivity = manualTestExecutionProductivity;
        this.overallAutomationTestExecutionProductivity = overallAutomationTestExecutionProductivity;
        this.regressionTestExecutionProductivity = regressionTestExecutionProductivity;
        this.inProjectTestExecutionProductivity = inProjectTestExecutionProductivity;
        this.apiAutomationTestExecutionProductivity = apiAutomationTestExecutionProductivity;
    }


    public String getComplexity() {
        return complexity;
    }

    public void setComplexity(String complexity) {
        this.complexity = complexity;
    }

    public String getComplexityName() {
        return complexityName;
    }

    public void getComplexityName(String complexityName) {
        this.complexityName = complexityName;
    }


    public String getManualTestDesignProductivity() {
        return manualTestDesignProductivity;
    }

    public void setManualTestDesignProductivity(String manualTestDesignProductivity) {
        this.manualTestDesignProductivity = manualTestDesignProductivity;
    }

    public String getOverallAutomationTestDesignProductivity() {
        return overallAutomationTestDesignProductivity;
    }

    public void setOverallAutomationTestDesignProductivity(String overallAutomationTestDesignProductivity) {
        this.overallAutomationTestDesignProductivity = overallAutomationTestDesignProductivity;
    }

    public String getRegressionTestDesignProductivity() {
        return regressionTestDesignProductivity;
    }

    public void setRegressionTestDesignProductivity(String regressionTestDesignProductivity) {
        this.regressionTestDesignProductivity = regressionTestDesignProductivity;
    }


    public String getInProjectTestDesignProductivity() {
        return inProjectTestDesignProductivity;
    }

    public void setInProjectTestDesignProductivity(String inProjectTestDesignProductivity) {
        this.inProjectTestDesignProductivity = inProjectTestDesignProductivity;
    }

    public String getApiAutomationTestDesignProductivity() {
        return apiAutomationTestDesignProductivity;
    }

    public void setApiAutomationTestDesignProductivity(String apiAutomationTestDesignProductivity) {
        this.apiAutomationTestDesignProductivity = apiAutomationTestDesignProductivity;
    }



    public String getManualTestExecutionProductivity() {
        return manualTestExecutionProductivity;
    }

    public void setManualTestExecutionProductivity(String manualTestExecutionProductivity) {
        this.manualTestExecutionProductivity = manualTestExecutionProductivity;
    }


    public String getOverallAutomationTestExecutionProductivity() {
        return overallAutomationTestExecutionProductivity;
    }

    public void setOverallAutomationTestExecutionProductivity(String overallAutomationTestExecutionProductivity) {
        this.overallAutomationTestExecutionProductivity = overallAutomationTestExecutionProductivity;
    }


    public String getRegressionTestExecutionProductivity() {
        return regressionTestExecutionProductivity;
    }

    public void setRegressionTestExecutionProductivity(String regressionTestExecutionProductivity) {
        this.regressionTestDesignProductivity = regressionTestExecutionProductivity;
    }


    public String getInProjectTestExecutionProductivity() {
        return inProjectTestExecutionProductivity;
    }

    public void setInProjectTestExecutionProductivity(String inProjectTestExecutionProductivity) {
        this.inProjectTestExecutionProductivity = inProjectTestExecutionProductivity;
    }


    public String getApiAutomationTestExecutionProductivity() {
        return apiAutomationTestExecutionProductivity;
    }

    public void setApiAutomationTestExecutionProductivity(String apiAutomationTestExecutionProductivity) {
        this.apiAutomationTestExecutionProductivity = apiAutomationTestExecutionProductivity;
    }


}