package com.packt.cardatabase.web;

import com.google.gson.Gson;
import com.packt.cardatabase.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;


@RestController
public class JiraSettingsController {

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private JiraSettingsRepository jiraSettingsRepository;


	@RequestMapping(value="/projects/{id}/jiraSettings", method=RequestMethod.GET)
	@ResponseBody
	public String getJiraSettings(@PathVariable("id") long id) {

		Iterable<JiraSettings> jiraSettingsIterable = jiraSettingsRepository.findAll();
		Iterator<JiraSettings> jiraSettingsIterator = jiraSettingsIterable.iterator();

		String response = null;
		boolean recordFound = false;

		try {

			while (jiraSettingsIterator.hasNext()) {
				JiraSettings next = jiraSettingsIterator.next();

				if (next.getProject().getId() == id) {
					recordFound = true;

					JiraSettingsPojo jiraSettingsPojo = new JiraSettingsPojo(next);

					Gson gson = new Gson();
					response = gson.toJson(jiraSettingsPojo);
					response = "{\"JiraSettings\":" + response + "}";
					System.out.println(response);

					return response;
				}
			}
		}catch(Exception e){
			System.out.println("Exception occurred while fetching ALM settings : " + e.getMessage());
		}

		if(recordFound == false) {

			try {

				JiraSettingsPojo next = new JiraSettingsPojo();

				next.setAccountId("test");
				next.setJiraQueryAPI("test");
				next.setJiraUserId("test");
				next.setJiraUserPassCode("test");
				next.setSecretKey("test");
				next.setZapiAccessKey("test");
				next.setZephyrBaseUrl("test");

				Gson gson = new Gson();
				response = gson.toJson(next);
				response = "{\"JiraSettings\":" + response + "}";
				System.out.println("The JIRA response is : "+ response );
				return response;

			} catch (Exception e) {

			}
		}

        return "Error";

	}

	@PostMapping(value = "/projects/{id}/jiraSettings")
	@ResponseBody
	public String updateAlmSettingsProgress(@PathVariable("id") long id, @RequestBody JiraSettingsPojo jiraSettingsPojo) {

		Gson gson = new Gson();
		System.out.println(gson.toJson(jiraSettingsPojo));

		Iterable<Project> allProjects = projectRepository.findAll();
		Iterator<Project> iterator = allProjects.iterator();
		Project nextProject = null;

		while (iterator.hasNext()) {
			nextProject = iterator.next();
			if (nextProject.getId() == id) {
				break;
			}
		}

		Iterable<JiraSettings> jiraSettingsIterable = jiraSettingsRepository.findAll();
		Iterator<JiraSettings> jiraSettingsIterator = jiraSettingsIterable.iterator();

		try {
			while (jiraSettingsIterator.hasNext()) {
				JiraSettings next = jiraSettingsIterator.next();

				if (next.getProject().getId() == id) {
					jiraSettingsRepository.delete(next);
				}

			}
		}catch(Exception e) {
		}

		JiraSettings jiraSettings = new JiraSettings(jiraSettingsPojo);
		jiraSettings.setProject(nextProject);
		jiraSettingsRepository.save(jiraSettings);

		return "OK";
	}

}