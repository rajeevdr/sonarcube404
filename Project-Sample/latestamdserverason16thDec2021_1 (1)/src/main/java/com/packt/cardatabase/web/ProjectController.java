package com.packt.cardatabase.web;

import com.google.gson.Gson;
import com.packt.cardatabase.domain.*;

import com.packt.cardatabase.domain.Module;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class ProjectController {

	@Autowired
	private CurrentSessionRepository currentSessionRepository;

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private ProjectEstimateRepository projectEstimateRepository;

	@Autowired
	private ModuleRepository moduleRepository;

	@Autowired
	private ModuleEstimateRepository moduleEstimateRepository;

	@Autowired
	private LatestModuleEstimateRepository latestModuleEstimateRepository;

	@Autowired
	private LatestProjectEstimateRepository latestProjectEstimateRepository;

	@Autowired
	private WeeklyModuleLevelTestDesignProgressRepository weeklyModuleLevelTestDesignProgressRepository;

	@Autowired
	private WeeklyModuleLevelTestExecutionProgressRepository weeklyModuleLevelTestExecutionProgressRepository;

	@Autowired
	private ModuleLevelMetricsRepository  moduleLevelMetricsRepository;

	@Autowired
	private FirstProjectEstimateRepository firstProjectEstimateRepository;

	@Autowired
	private FirstModuleEstimateRepository firstModuleEstimateRepository;


	@Autowired
	private HeatMapSettingsRepository heatMapSettingsRepository;

	@Autowired
	private LastUpdateRepository lastUpdateRepository;

	@Autowired
	private WeekDataRepository weekDataRepository;

	@Autowired
	private MonthDataRepository monthDataRepository;

	@Autowired
	private QuarterDataRepository quarterDataRepository;

	@Autowired
	private SyncStartAndEndDateUpdateRepository syncStartAndEndDateUpdateRepository;



	@PostMapping("/currentSession")
	public void updateSession(
			@RequestBody CurrentSessionPojo currentSessionPojo) {

		currentSessionRepository.deleteAll();
		CurrentSession currentSession = new CurrentSession();
		currentSession.setProjectId(currentSessionPojo.getProjectId());
		currentSessionRepository.save(currentSession);
	}

	String getCurrentDate() {
		LocalDateTime myDateObj = LocalDateTime.now();
		DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		String formattedDate = myDateObj.format(myFormatObj);
		return formattedDate;
	}

	String getCurrentDateAndTime() {
		LocalDateTime myDateObj = LocalDateTime.now();
		DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSSSSS");
		String formattedDate = myDateObj.format(myFormatObj);
		return formattedDate;
	}


	@RequestMapping(value = "/currentSession", method = GET)
	@ResponseBody
	public String getCurrentSession() {

		Iterable<CurrentSession> allSessions = currentSessionRepository.findAll();
		Iterator<CurrentSession> iterator = allSessions.iterator();

		while (iterator.hasNext()) {
			CurrentSession next = iterator.next();
			CurrentSessionPojo temp = new CurrentSessionPojo();
			temp.setProjectId(next.getProjectId());
			Gson gson = new Gson();
			return gson.toJson(temp);
		}
		return "";
	}

	@PostMapping("/projects")
	public ResponseEntity createProject(
			@RequestBody ProjectPojo projectPojo) {

	try {
		if( projectPojo.getName().length() == 0)
		{
			return new ResponseEntity<>(
					"Project Name missing",
					HttpStatus.BAD_REQUEST);
		}

		Iterable<Project> allProjects = projectRepository.findAll();
		Iterator<Project> iterator = allProjects.iterator();

		while (iterator.hasNext()) {
			Project nextProject = iterator.next();

			if (nextProject.getName().compareTo(projectPojo.getName()) == 0) {
				return new ResponseEntity<>(
					"Project with same name already exists",
					HttpStatus.BAD_REQUEST);
			}
			}
		}catch(Exception e){
	    }

		Project project = new Project();
		project.setName(projectPojo.getName());
		project.setType(projectPojo.getType());
		project.setCategory(projectPojo.getCategory());
		project.setCluster(projectPojo.getCluster());
		project.setDomain(projectPojo.getDomain());
		project.setDeliveryManager(projectPojo.getDeliveryManager());
		project.setTechnology(projectPojo.getTechnology());
		project.setStartDate(projectPojo.getStartDate());
		project.setEndDate(projectPojo.getEndDate());
		project.setLastUpdateOn(getCurrentDate());



		projectRepository.save(project);




		Iterable<Project> allProjects = projectRepository.findAll();
		Iterator<Project> iterator = allProjects.iterator();
		String response = null;

		long projectId = 0;

		while (iterator.hasNext()) {
			Project latestProject = iterator.next();
			if (latestProject.getName().compareTo(projectPojo.getName()) == 0) {

				try {

					Iterable<LastUpdate> lastUpdateIterable = lastUpdateRepository.findAll();
					Iterator<LastUpdate> lastUpdateIterator = lastUpdateIterable.iterator();

					while (lastUpdateIterator.hasNext()) {
						LastUpdate lastUpdate = lastUpdateIterator.next();

						if (lastUpdate.getProject().getId() == latestProject.getId()) {

							try {
								lastUpdateRepository.delete(lastUpdate);
							} catch (Exception e) {

							}
						}
					}

					String lastUpdateDate = null;

					lastUpdateDate = getCurrentDate();

					if (lastUpdateDate != null) {

						System.out.println("The desired last update date is : " + lastUpdateDate);

						StringTokenizer tokenizer = new StringTokenizer(lastUpdateDate, "-");
						int dateNumber = Integer.parseInt(tokenizer.nextToken());
						int monthNumber = Integer.parseInt(tokenizer.nextToken());
						int yearNumber = Integer.parseInt(tokenizer.nextToken());

						int desiredWeekNumber = 0;
						int desiredMonthNumber = 0;
						int desiredQuarterNumber = 0;
						int desiredYearNumber = 0;


						String dateNumberString = null;
						String monthNumberString = null;


						if (dateNumber < 10)
							dateNumberString = "0" + Integer.toString(dateNumber);
						else
							dateNumberString = Integer.toString(dateNumber);

						if (monthNumber < 10)
							monthNumberString = "0" + Integer.toString(monthNumber);
						else
							monthNumberString = Integer.toString(monthNumber);


						String dateToday = dateNumberString + "-" + monthNumberString + "-" + yearNumber;
						System.out.println("The sync start date is : " + dateToday);


						Iterable<WeekData> weekDataIterable = weekDataRepository.findAll();
						Iterator<WeekData> weekDataIterator = weekDataIterable.iterator();

						while (weekDataIterator.hasNext()) {
							WeekData next = weekDataIterator.next();
							if (next.getWeekDays().contains(dateToday)) {
								desiredWeekNumber = next.getWeekNumber();
								System.out.println("The current week number : " + desiredWeekNumber);
								break;
							}
						}

						Iterable<MonthData> monthDataIterable = monthDataRepository.findAll();
						Iterator<MonthData> monthDataIterator = monthDataIterable.iterator();

						while (monthDataIterator.hasNext()) {
							MonthData next = monthDataIterator.next();

							if ((next.getYearNumber() == yearNumber) &&
									(next.getMonthNumber() == monthNumber)) {
								desiredMonthNumber = next.getMonthNumber();
								desiredYearNumber = next.getYearNumber();
								System.out.println("The desired month number is : " + desiredMonthNumber);
								System.out.println("The desired year number is : " + desiredYearNumber);

								break;
							}

						}

						Iterable<QuarterData> quarterDataIterable = quarterDataRepository.findAll();
						Iterator<QuarterData> quarterDataIterator = quarterDataIterable.iterator();


						while (quarterDataIterator.hasNext()) {
							QuarterData next = quarterDataIterator.next();

							if (next.getYearNumber() == yearNumber) {

								StringTokenizer tokenizer2 = new StringTokenizer(next.getQuarterStartDate(), "-");
								tokenizer2.nextToken();

								int monthNumber2 = Integer.parseInt(tokenizer2.nextToken());

								if (monthNumber2 <= monthNumber) {

									StringTokenizer tokenizer3 = new StringTokenizer(next.getQuarterEndDate(), "-");
									tokenizer3.nextToken();

									int monthNumber3 = Integer.parseInt(tokenizer3.nextToken());

									if (monthNumber3 >= monthNumber) {
										desiredQuarterNumber = next.getQuarterNumber();
										System.out.println("The desired quarter number is : " + desiredQuarterNumber);
										break;
									}

								}
							}
						}


						LastUpdate lastUpdate = new LastUpdate();
						lastUpdate.setLastUpdatedDateNumber(dateNumber);
						lastUpdate.setLastUpdatedWeekNumber(desiredWeekNumber);
						lastUpdate.setLastUpdatedMonthNumber(desiredMonthNumber);
						lastUpdate.setLastUpdatedYearNumber(desiredYearNumber);
						lastUpdate.setLastUpdatedQuarterNumber(desiredQuarterNumber);

						lastUpdate.setProject(latestProject);
						lastUpdateRepository.save(lastUpdate);

					}
					else{

					}
				}catch (Exception e) {

				}


				ProjectPojo temp = new ProjectPojo();
				projectId = latestProject.getId();
				temp.setId(latestProject.getId());
				temp.setName(latestProject.getName());
				temp.setType(latestProject.getType());
				temp.setCategory(latestProject.getCategory());
				temp.setCluster(latestProject.getCluster());
				temp.setDomain(latestProject.getDomain());
				temp.setDeliveryManager(latestProject.getDeliveryManager());
				temp.setTechnology(latestProject.getTechnology());
				temp.setStartDate(latestProject.getStartDate());
				temp.setEndDate(latestProject.getEndDate());
				temp.setLastUpdateOn(latestProject.getLastUpdateOn());




				FirstProjectEstimate firstEstimate = new FirstProjectEstimate();

				firstEstimate.setPlannedStartDate(projectPojo.getStartDate());
				firstEstimate.setPlannedStartDate(projectPojo.getEndDate());
				firstEstimate.setRegressionAutomationTestCaseEstimation("0");
				firstEstimate.setInProjectAutomationTestCaseEstimation("0");
				firstEstimate.setAPIAutomationTestCaseEstimation("0");
				firstEstimate.setTestPlanningEffort("0");
				firstEstimate.setManualTestCaseDesignEffort("0");
				firstEstimate.setAutomationTestCaseDesignEffort("0");
				firstEstimate.setRegressionAutomationTestCaseDesignEffort("0");
				firstEstimate.setInProjectAutomationTestCaseDesignEffort("0");
				firstEstimate.setApiAutomationTestCaseDesignEffort("0");
				firstEstimate.setTotalNumberOfTestCasesTaggedForMigrationExecution("0");
				firstEstimate.setManualTestCaseExecutionEffort("0");
				firstEstimate.setAutomationTestCaseExecutionEffort("0");
				firstEstimate.setRegressionAutomationTestCaseExecutionEffort("0");
				firstEstimate.setInProjectAutomationTestCaseExecutionEffort("0");
				firstEstimate.setApiAutomationTestCaseExecutionEffort("0");
				firstEstimate.setManualTestCaseMaintenanceEffort("0");
				firstEstimate.setAutomationTestCaseMaintenanceEffort("0");
				firstEstimate.setTotalEffortInPersonDays("0");
				firstEstimate.setPlannedResourcesForManualTestCaseExecution("0");
				firstEstimate.setPlannedResourcesForManualTestCaseAuthoring("0");
				firstEstimate.setPlannedResourcesForAutomationDesign("0");
				firstEstimate.setPlannedResourcesForAutomationExecution("0");
				firstEstimate.setProjectedBilling("0");
				firstEstimate.setTotalNumberOfRequirementsPlanned("0");
				firstEstimate.setNumberOfTestCasesDesignedPerHour("0");
				firstEstimate.setNumberOfTestCasesExecutedPerHour("0");
				firstEstimate.setProject(latestProject);

				firstProjectEstimateRepository.save(firstEstimate);




				boolean exceptionFound = false;
				boolean matchFound = false;

				try {
					Iterable<LastUpdate> lastUpdateIterable = lastUpdateRepository.findAll();
					Iterator<LastUpdate> lastUpdateIterator = lastUpdateIterable.iterator();

					while (lastUpdateIterator.hasNext()) {
						LastUpdate lastUpdate = lastUpdateIterator.next();

						if (lastUpdate.getProject().getId() == latestProject.getId()) {

							int dateNumber = lastUpdate.getLastUpdatedDateNumber();
							int monthNumber = lastUpdate.getLastUpdatedMonthNumber();
							int yearNumber = lastUpdate.getLastUpdatedYearNumber();

							String dateNumberString = null;
							String monthNumberString = null;

							if (dateNumber < 10)
								dateNumberString = "0" + dateNumber;
							else
								dateNumberString = Integer.toString(dateNumber);

							if (monthNumber < 10)
								monthNumberString = "0" + monthNumber;
							else
								monthNumberString = Integer.toString(monthNumber);


							String lastUpdatedDate = dateNumberString + "-" + monthNumberString + "-" + Integer.toString(yearNumber);
							System.out.println(lastUpdatedDate);
							temp.setLastSyncUpdateDate(lastUpdatedDate);
							matchFound = true;

						}
					}
				}catch(Exception e){
					exceptionFound = true;
				}

				if((exceptionFound == true) || matchFound == false)
					temp.setLastSyncUpdateDate("");


				Gson gson = new Gson();
				response = gson.toJson(temp);
				break;
			}
		}

		setDefaultValues(projectId, WeeklyMetrics.getManualTestCaseAuthoringProductivityName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getRegressionTestAutomationScriptingProductivityName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getApiTestAutomationScriptingProductivityName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getInProjectAutomationScriptingProductivityName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getManualTestExecutionProductivityName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getRegressionAutomationTestExecutionProductivityName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getInProjectAutomationTestExecutionProductivityName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getAPIAutomationTestExecutionProductivityName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getDesignCoverageName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getSanityAutomationCoverageName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getPercentageOfRegressionAutomationName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getMigratedDataExecutionName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getDefectLeakageSITToUATName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getDefectLeakageToProductionName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getNumberOfDefectsRejectedForTheWeekName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getTotalNumberOfDefectsReopenedForTheWeekName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getDefectRejectionName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getTestCaseNotExecutedManualName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getTestCaseNotExecutedAutomationName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getPercentageOfPendingRegressionAutomationName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getPercentageOfRequirementsNotCoveredName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getAverageTimeTakenToResolveSev1DefectsName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getDowntimeAndImpactAnalysisDesignAvgProductivityLostName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getDowntimeAndImpactAnalysisExecutionAvgProductivityLostName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getTestReviewEfficiencyName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getCurrentQualityRatioName(), "");
		setDefaultValues(projectId, WeeklyMetrics.getQualityOfFixesName(), "");

		setDefaultValues(projectId, MonthlyMetrics.getCostVarianceName(), "");
		setDefaultValues(projectId, MonthlyMetrics.getEffortVarianceName(), "");
		setDefaultValues(projectId, MonthlyMetrics.getScopeVarianceName(), "");
		setDefaultValues(projectId, MonthlyMetrics.getScheduleVarianceName(), "");
		setDefaultValues(projectId, MonthlyMetrics.getCostPerDefectName(), "");
		setDefaultValues(projectId, MonthlyMetrics.getCostPerTestCaseName(), "");
		setDefaultValues(projectId, MonthlyMetrics.getSITDefectDensityName(), "");
		setDefaultValues(projectId, MonthlyMetrics.getUATDefectDensityName(), "");
		setDefaultValues(projectId, MonthlyMetrics.getDefectLeakageSITToUATName(), "");
		setDefaultValues(projectId, MonthlyMetrics.getDefectLeakageToProductionName(), "");
		setDefaultValues(projectId, MonthlyMetrics.getInProjectAutomationPercentageExtentToPotentialName(), "");
		setDefaultValues(projectId, MonthlyMetrics.getApiAutomationPercentageExtentToPotentialName(), "");
		setDefaultValues(projectId, MonthlyMetrics.getPercentageOfTCsIdentifiedForMigrationName(), "");
		setDefaultValues(projectId, MonthlyMetrics.getManualEffectivenessName(), "");
		setDefaultValues(projectId, MonthlyMetrics.getAutomationEffectivenessName(), "");

		setDefaultValues(projectId, QuarterlyMetrics.getRevenueLeakageName(), "");

		return ResponseEntity.ok(HttpStatus.OK + "\r\n" + response);
	}


	public void setDefaultValues(long id, String metricName, String category) {

		HeatMapSettings heatMapSettings = new HeatMapSettings();
		heatMapSettings.setLCL("0");
		heatMapSettings.setUCL("0");
		heatMapSettings.setGoal("0");
		heatMapSettings.setCategory(category);
		heatMapSettings.setMetricName(metricName);

		Iterable<Project> allProjects = projectRepository.findAll();
		Iterator<Project> iterator = allProjects.iterator();

		while (iterator.hasNext()) {
			Project nextProject = iterator.next();

			if (nextProject.getId() == id) {
				heatMapSettings.setProject(nextProject);
				break;
			}
		}

		heatMapSettingsRepository.save(heatMapSettings);
	}

	@RequestMapping(value = "/projects", method = GET)
	@ResponseBody
	public String getProjects() {

		List<ProjectPojo> projectsPojo = new ArrayList<ProjectPojo>();
		Iterable<Project> allProjects = projectRepository.findAll();
		Iterator<Project> iterator = allProjects.iterator();

		while (iterator.hasNext()) {
			Project next = iterator.next();
			ProjectPojo temp = new ProjectPojo();
			temp.setId(next.getId());
			temp.setName(next.getName());
			temp.setType(next.getType());
			temp.setCategory(next.getCategory());
			temp.setCluster(next.getCluster());
			temp.setDomain(next.getDomain());
			temp.setDeliveryManager(next.getDeliveryManager());
			temp.setTechnology(next.getTechnology());
			temp.setStartDate(next.getStartDate());
			temp.setEndDate(next.getEndDate());
			temp.setLastUpdateOn(next.getLastUpdateOn());

			boolean exceptionFound = false;
			boolean matchFound = false;

			try {
				Iterable<LastUpdate> lastUpdateIterable = lastUpdateRepository.findAll();
				Iterator<LastUpdate> lastUpdateIterator = lastUpdateIterable.iterator();

				while (lastUpdateIterator.hasNext()) {
					LastUpdate lastUpdate = lastUpdateIterator.next();

					if (lastUpdate.getProject().getId() == next.getId()) {

						int dateNumber = lastUpdate.getLastUpdatedDateNumber();
						int monthNumber = lastUpdate.getLastUpdatedMonthNumber();
						int yearNumber = lastUpdate.getLastUpdatedYearNumber();

						String dateNumberString = null;
						String monthNumberString = null;

						if (dateNumber < 10)
							dateNumberString = "0" + dateNumber;
						else
							dateNumberString = Integer.toString(dateNumber);

						if (monthNumber < 10)
							monthNumberString = "0" + monthNumber;
						else
							monthNumberString = Integer.toString(monthNumber);


						String lastUpdatedDate = dateNumberString + "-" + monthNumberString + "-" + Integer.toString(yearNumber);
						String response = "{\"lastUpdateDate\":{\"" + lastUpdatedDate + "\"}}";
						System.out.println(response);
						temp.setLastSyncUpdateDate(lastUpdatedDate);
						matchFound = true;

					}
				}
			}catch(Exception e){
				exceptionFound = true;
			}

			if((exceptionFound == true) || matchFound == false)
				temp.setLastSyncUpdateDate("");

			projectsPojo.add(temp);
		}
		Gson gson = new Gson();
		return "{\"projects\":" + gson.toJson(projectsPojo) + "}";
	}

	@RequestMapping(value = "/projects/{id}", method = GET)
	@ResponseBody
	public String getProject(@PathVariable("id") long id) {
		Iterable<Project> allProjects = projectRepository.findAll();
		Iterator<Project> iterator = allProjects.iterator();
		String response = null;

		while (iterator.hasNext()) {
			Project next = iterator.next();
			if (next.getId() == id) {
				ProjectPojo temp = new ProjectPojo();
				temp.setId(next.getId());
				temp.setName(next.getName());
				temp.setType(next.getType());
				temp.setCategory(next.getCategory());
				temp.setCluster(next.getCluster());
				temp.setDomain(next.getDomain());
				temp.setDeliveryManager(next.getDeliveryManager());
				temp.setTechnology(next.getTechnology());
				temp.setStartDate(next.getStartDate());
				temp.setEndDate(next.getEndDate());
				temp.setLastUpdateOn(next.getLastUpdateOn());


				boolean exceptionFound = false;
				boolean matchFound = false;

				try {
					Iterable<LastUpdate> lastUpdateIterable = lastUpdateRepository.findAll();
					Iterator<LastUpdate> lastUpdateIterator = lastUpdateIterable.iterator();

					while (lastUpdateIterator.hasNext()) {
						LastUpdate lastUpdate = lastUpdateIterator.next();

						if (lastUpdate.getProject().getId() == id) {

							int dateNumber = lastUpdate.getLastUpdatedDateNumber();
							int monthNumber = lastUpdate.getLastUpdatedMonthNumber();
							int yearNumber = lastUpdate.getLastUpdatedYearNumber();

							String dateNumberString = null;
							String monthNumberString = null;

							if (dateNumber < 10)
								dateNumberString = "0" + dateNumber;
							else
								dateNumberString = Integer.toString(dateNumber);

							if (monthNumber < 10)
								monthNumberString = "0" + monthNumber;
							else
								monthNumberString = Integer.toString(monthNumber);


							String lastUpdatedDate = dateNumberString + "-" + monthNumberString + "-" + Integer.toString(yearNumber);
							response = "{\"lastUpdateDate\":{\"" + lastUpdatedDate + "\"}}";
							System.out.println(response);
							temp.setLastSyncUpdateDate(lastUpdatedDate);
							matchFound = true;


						}
					}
				}catch(Exception e){
					exceptionFound = true;
				}

				if((exceptionFound == true) || matchFound == false)
					temp.setLastSyncUpdateDate("");


				Gson gson = new Gson();
				response = gson.toJson(temp);
				break;
			}
		}

		if (response == null)
			response = "{\"error\":\"Project not found\"}";

		return response;
	}

	@PostMapping(value = "/projects/{id}")
	@ResponseBody
	public String updateProject(
			@RequestBody ProjectPojo projectPojo, @PathVariable("id") long id) {
		Iterable<Project> allProjects = projectRepository.findAll();
		Iterator<Project> iterator = allProjects.iterator();

		if( projectPojo.getName().length() == 0)
		{
			return "Project Name missing";
		}

		while (iterator.hasNext()) {
			Project nextProject = iterator.next();

			if (nextProject.getId() == id) {

				nextProject.setName(projectPojo.getName());
				nextProject.setType(projectPojo.getType());
				nextProject.setCategory(projectPojo.getCategory());
				nextProject.setCluster(projectPojo.getCluster());
				nextProject.setDomain(projectPojo.getDomain());
				nextProject.setDeliveryManager(projectPojo.getDeliveryManager());
				nextProject.setTechnology(projectPojo.getTechnology());
				nextProject.setStartDate(projectPojo.getStartDate());
				nextProject.setEndDate(projectPojo.getEndDate());

				nextProject.setLastUpdateOn(getCurrentDate());



				try {

					Iterable<LastUpdate> lastUpdateIterable = lastUpdateRepository.findAll();
					Iterator<LastUpdate> lastUpdateIterator = lastUpdateIterable.iterator();

					while (lastUpdateIterator.hasNext()) {
						LastUpdate next = lastUpdateIterator.next();

						if (next.getProject().getId() == id) {

							try {
								lastUpdateRepository.delete(next);
							} catch (Exception e) {

							}
						}
					}

					String lastUpdateDate = null;

					lastUpdateDate = projectPojo.getLastSyncUpdateDate();

					if (lastUpdateDate != null) {

						System.out.println("The desired last update date is : " + lastUpdateDate);

						StringTokenizer tokenizer = new StringTokenizer(lastUpdateDate, "-");
						int dateNumber = Integer.parseInt(tokenizer.nextToken());
						int monthNumber = Integer.parseInt(tokenizer.nextToken());
						int yearNumber = Integer.parseInt(tokenizer.nextToken());

						int desiredWeekNumber = 0;
						int desiredMonthNumber = 0;
						int desiredQuarterNumber = 0;
						int desiredYearNumber = 0;


						String dateNumberString = null;
						String monthNumberString = null;


						if (dateNumber < 10)
							dateNumberString = "0" + Integer.toString(dateNumber);
						else
							dateNumberString = Integer.toString(dateNumber);

						if (monthNumber < 10)
							monthNumberString = "0" + Integer.toString(monthNumber);
						else
							monthNumberString = Integer.toString(monthNumber);


						String dateToday = dateNumberString + "-" + monthNumberString + "-" + yearNumber;
						System.out.println("The sync start date is : " + dateToday);


						Iterable<WeekData> weekDataIterable = weekDataRepository.findAll();
						Iterator<WeekData> weekDataIterator = weekDataIterable.iterator();

						while (weekDataIterator.hasNext()) {
							WeekData next = weekDataIterator.next();
							if (next.getWeekDays().contains(dateToday)) {
								desiredWeekNumber = next.getWeekNumber();
								System.out.println("The current week number : " + desiredWeekNumber);
								break;
							}
						}

						Iterable<MonthData> monthDataIterable = monthDataRepository.findAll();
						Iterator<MonthData> monthDataIterator = monthDataIterable.iterator();

						while (monthDataIterator.hasNext()) {
							MonthData next = monthDataIterator.next();

							if ((next.getYearNumber() == yearNumber) &&
									(next.getMonthNumber() == monthNumber)) {
								desiredMonthNumber = next.getMonthNumber();
								desiredYearNumber = next.getYearNumber();
								System.out.println("The desired month number is : " + desiredMonthNumber);
								System.out.println("The desired year number is : " + desiredYearNumber);

								break;
							}

						}

						Iterable<QuarterData> quarterDataIterable = quarterDataRepository.findAll();
						Iterator<QuarterData> quarterDataIterator = quarterDataIterable.iterator();


						while (quarterDataIterator.hasNext()) {
							QuarterData next = quarterDataIterator.next();

							if (next.getYearNumber() == yearNumber) {

								StringTokenizer tokenizer2 = new StringTokenizer(next.getQuarterStartDate(), "-");
								tokenizer2.nextToken();

								int monthNumber2 = Integer.parseInt(tokenizer2.nextToken());

								if (monthNumber2 <= monthNumber) {

									StringTokenizer tokenizer3 = new StringTokenizer(next.getQuarterEndDate(), "-");
									tokenizer3.nextToken();

									int monthNumber3 = Integer.parseInt(tokenizer3.nextToken());

									if (monthNumber3 >= monthNumber) {
										desiredQuarterNumber = next.getQuarterNumber();
										System.out.println("The desired quarter number is : " + desiredQuarterNumber);
										break;
									}

								}
							}
						}


						LastUpdate lastUpdate = new LastUpdate();
						lastUpdate.setLastUpdatedDateNumber(dateNumber);
						lastUpdate.setLastUpdatedWeekNumber(desiredWeekNumber);
						lastUpdate.setLastUpdatedMonthNumber(desiredMonthNumber);
						lastUpdate.setLastUpdatedYearNumber(desiredYearNumber);
						lastUpdate.setLastUpdatedQuarterNumber(desiredQuarterNumber);

						lastUpdate.setProject(nextProject);
						lastUpdateRepository.save(lastUpdate);

					}
				}catch (Exception e) {

				}

				projectRepository.save(nextProject);
				break;
			}
		}
		return "OK";

	}

	@RequestMapping(value = "/projects", method = DELETE)
	@ResponseBody
	public void deleteAllProjects() {

		moduleEstimateRepository.deleteAll();
		projectEstimateRepository.deleteAll();
		latestModuleEstimateRepository.deleteAll();
		latestProjectEstimateRepository.deleteAll();
		moduleRepository.deleteAll();
		projectRepository.deleteAll();

	}

	@RequestMapping(value = "/projects/{id}", method = DELETE)
	@ResponseBody
	public void deleteProject(@PathVariable("id") long id) {

		Iterable<LatestProjectEstimate> allLatestProjectEstimates = latestProjectEstimateRepository.findAll();
		Iterator<LatestProjectEstimate> latestProjectEstimateIterator = allLatestProjectEstimates.iterator();

		while (latestProjectEstimateIterator.hasNext()) {
			LatestProjectEstimate latestProjectEstimate = latestProjectEstimateIterator.next();

			if (latestProjectEstimate.getProject().getId() == id) {
				try{
					latestProjectEstimateRepository.delete(latestProjectEstimate);
				}catch(Exception e){
				}
			}
		}

		Iterable<ProjectEstimate> allProjectEstimates = projectEstimateRepository.findAll();
		Iterator<ProjectEstimate> projectEstimateIterator = allProjectEstimates.iterator();

		while (projectEstimateIterator.hasNext()) {
			ProjectEstimate projectEstimate = projectEstimateIterator.next();

			if (projectEstimate.getProject().getId() == id) {
				try{
					projectEstimateRepository.delete(projectEstimate);
				}catch(Exception e){
				}
			}
		}


		Iterable<LatestModuleEstimate> allLatestModuleEstimates = latestModuleEstimateRepository.findAll();
		Iterator<LatestModuleEstimate> latestModuleEstimateIterator = allLatestModuleEstimates.iterator();

		while (latestModuleEstimateIterator.hasNext()) {
			LatestModuleEstimate latestModulesEstimate = latestModuleEstimateIterator.next();

			if (latestModulesEstimate.getProject().getId() == id) {
				try{
					latestModuleEstimateRepository.delete(latestModulesEstimate);
				}catch(Exception e){
				}
			}
		}


		Iterable<ModuleEstimate> allModuleEstimates = moduleEstimateRepository.findAll();
		Iterator<ModuleEstimate> moduleEstimateIterator = allModuleEstimates.iterator();

		while (moduleEstimateIterator.hasNext()) {
			ModuleEstimate modulesEstimate = moduleEstimateIterator.next();

			if (modulesEstimate.getProject().getId() == id) {
				try{
					moduleEstimateRepository.delete(modulesEstimate);
				}catch(Exception e){
				}
			}
		}


		Iterable<Module> allModules = moduleRepository.findAll();
		Iterator<Module> modulesIterator = allModules.iterator();


		while (modulesIterator.hasNext()) {
			Module module = modulesIterator.next();

			if (module.getProject().getId() == id) {
				try{
					moduleRepository.delete(module);
				}catch(Exception e){
				}
			}
		}

		Iterable<Project> allProjects = projectRepository.findAll();
		Iterator<Project> projectIterator = allProjects.iterator();


		while (projectIterator.hasNext()) {
			Project project = projectIterator.next();

			if (project.getId() == id) {
				try{
					projectRepository.delete(project);
				}catch(Exception e){
				}
			}
		}


	}


	@PostMapping(value = "/projects/{id}/addModule")
	@ResponseBody
	public String addModule(@PathVariable("id") long id, @RequestBody ModulePojo modulePojo) {

		try {

			Iterable<Module> allModules = moduleRepository.findAll();
			Iterator<Module> modulesIterator = allModules.iterator();

			if( modulePojo.getName().length() == 0)
			{
				return "Module Name missing";
			}


			Iterable<Project> allProjects = projectRepository.findAll();
			Iterator<Project> iterator = allProjects.iterator();

			while (iterator.hasNext()) {
				Project next = iterator.next();
				if (next.getId() == id) {

					while (modulesIterator.hasNext()) {
						Module desiredModule = modulesIterator.next();
						if ((desiredModule.getName().compareTo(modulePojo.getName()) == 0) && (desiredModule.getProject().getId() == id)) {
							return "Module with same name already exists";
						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Exception occured in Add Module API" + e.getMessage());
		}

		Iterable<Project> allProjects = projectRepository.findAll();
		Iterator<Project> iterator = allProjects.iterator();

		while (iterator.hasNext()) {
			Project next = iterator.next();
			if (next.getId() == id) {

				Module tempModule = new Module();
				tempModule.setName(modulePojo.getName());
				tempModule.setEstimatedManualTestCases(modulePojo.getEstimatedManualTestCases());
				tempModule.setEstimatedRegressionAutomationTestCases(modulePojo.getEstimatedRegressionAutomationTestCases());
				tempModule.setEstimatedSanityAutomationTestCases(modulePojo.getEstimatedSanityAutomationTestCases());
				tempModule.setComplexity(modulePojo.getComplexity());
				tempModule.setProject(next);
				moduleRepository.save(tempModule);

				FirstModuleEstimate firstModuleEstimate = new FirstModuleEstimate(tempModule);
				firstModuleEstimateRepository.save(firstModuleEstimate);

				Iterable<Module> allModules = moduleRepository.findAll();
				Iterator<Module> allModulesIterator = allModules.iterator();

				while (allModulesIterator.hasNext()) {
					Module nextModule = allModulesIterator.next();
					if ((nextModule.getName().compareTo(modulePojo.getName()) == 0) && (nextModule.getProject().getId() == id)) {

						ModuleEstimate tempModuleEstimate = new ModuleEstimate();
						tempModuleEstimate.setModuleTimeStamp(getCurrentDateAndTime());
						tempModuleEstimate.setOriginalModuleId(nextModule.getId());
						tempModuleEstimate.setName(modulePojo.getName());
						tempModuleEstimate.setEstimatedManualTestCases(modulePojo.getEstimatedManualTestCases());
						tempModuleEstimate.setEstimatedRegressionAutomationTestCases(modulePojo.getEstimatedRegressionAutomationTestCases());
						tempModuleEstimate.setEstimatedSanityAutomationTestCases(modulePojo.getEstimatedSanityAutomationTestCases());
						tempModuleEstimate.setProject(next);
						moduleEstimateRepository.save(tempModuleEstimate);

						LatestModuleEstimate latestModuleEstimate = new LatestModuleEstimate(tempModuleEstimate);
						latestModuleEstimateRepository.save(latestModuleEstimate);
						break;
					}
				}
			}
		}

			Iterable<Module> allModules = moduleRepository.findAll();
			Iterator<Module> modulesIterator = allModules.iterator();

			while (modulesIterator.hasNext()) {
				Module module = modulesIterator.next();
				if (module.getName() == modulePojo.getName()) {
					ModulePojo temp = new ModulePojo();
					temp.setId(module.getId());
					temp.setName(module.getName());
					temp.setEstimatedManualTestCases(module.getEstimatedManualTestCases());
					temp.setEstimatedRegressionAutomationTestCases(module.getEstimatedRegressionAutomationTestCases());
					temp.setEstimatedSanityAutomationTestCases(module.getEstimatedSanityAutomationTestCases());
					temp.setComplexity(module.getComplexity());
					Gson gson = new Gson();
					return gson.toJson(temp);
				}
			}
		return "";
	}


	@PostMapping(value = "/projects/{id}/updateModule/{moduleId}")
	@ResponseBody
	public String updateModule(@PathVariable("id") long id, @PathVariable("moduleId") long moduleId, @RequestBody ModulePojo modulePojo) {

		Iterable<Module> allModules = moduleRepository.findAll();

		try {

			if( modulePojo.getName().length() == 0)
			{
				return "Module Name missing";
			}


		try {

			Iterable<Project> allProjects = projectRepository.findAll();
			Iterator<Project> iterator = allProjects.iterator();
			boolean jobDone = false;

			while (iterator.hasNext()) {
				Project next = iterator.next();

				Iterable<Module> modules = moduleRepository.findAll();
				Iterator<Module> moduleIterator = modules.iterator();

				if (next.getId() == id) {

					while (moduleIterator.hasNext()) {
						Module desiredModule = moduleIterator.next();

						if (desiredModule.getId() == moduleId) {

							//if(desiredModule.getName().compareTo(modulePojo.getName()) != 0)
							//{
								// Module Estimate
								// Latest Module Estimate => not required
								// Weekly Module test design progress
								// Weekly Module test execution progress
								// Update the names

								// Project Should be same
								// Old name should match
								// update the new name


							try {

								Iterable<ModuleEstimate> allModuleEstimates = moduleEstimateRepository.findAll();
								Iterator<ModuleEstimate> moduleEstimateIterator = allModuleEstimates.iterator();

								while (moduleEstimateIterator.hasNext()) {
									ModuleEstimate modulesEstimate = moduleEstimateIterator.next();

									if ((modulesEstimate.getProject().getId() == id) &&
											(modulesEstimate.getName().compareTo(desiredModule.getName()) == 0)) {

										modulesEstimate.setName(modulePojo.getName());
										moduleEstimateRepository.save(modulesEstimate);

									}
								}
							}catch(Exception e){
								System.out.println("Exception occured during updatemodule:moduleEstimateRepository " +
										e.getMessage());
							}

							try {
								Iterable<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgressIterable = weeklyModuleLevelTestDesignProgressRepository.findAll();
								Iterator<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgressIterator = weeklyModuleLevelTestDesignProgressIterable.iterator();

								while (weeklyModuleLevelTestDesignProgressIterator.hasNext()) {

									WeeklyModuleLevelTestDesignProgress moduleProgress = weeklyModuleLevelTestDesignProgressIterator.next();

									if ((moduleProgress.getProject().getId() == id) &&
											(moduleProgress.getModuleName().compareTo(desiredModule.getName()) == 0)) {

										moduleProgress.setModuleName(modulePojo.getName());
										weeklyModuleLevelTestDesignProgressRepository.save(moduleProgress);
									}

								}
							}catch(Exception e){
								System.out.println("Exception occured during updatemodule:weeklyModuleLevelTestDesignProgressRepository " +
										e.getMessage());
							}

							try{
								Iterable<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgress = weeklyModuleLevelTestExecutionProgressRepository.findAll();
								Iterator<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgressIterator = weeklyModuleLevelTestExecutionProgress.iterator();

								while (weeklyModuleLevelTestExecutionProgressIterator.hasNext()) {

									WeeklyModuleLevelTestExecutionProgress moduleProgress = weeklyModuleLevelTestExecutionProgressIterator.next();

									if ((moduleProgress.getProject().getId() == id) &&
											(moduleProgress.getModuleName().compareTo(desiredModule.getName()) == 0)) {

										moduleProgress.setModuleName(modulePojo.getName());
										weeklyModuleLevelTestExecutionProgressRepository.save(moduleProgress);
									}

								}
							}catch(Exception e){
								System.out.println("Exception occured during updatemodule:weeklyModuleLevelTestExecutionProgressRepository " +
										e.getMessage());
							}



							try{
							Iterable<ModuleLevelMetrics> moduleLevelMetricsIterable = moduleLevelMetricsRepository.findAll();
							Iterator<ModuleLevelMetrics> moduleLevelMetricsIterator = moduleLevelMetricsIterable.iterator();

							ModuleLevelMetrics nextModuleMetrics = null;

							while (moduleLevelMetricsIterator.hasNext()) {

								nextModuleMetrics = moduleLevelMetricsIterator.next();

								if ((nextModuleMetrics.getProject().getId() == id) &&
										(nextModuleMetrics.getModuleName().compareTo(desiredModule.getName()) == 0)) {
									nextModuleMetrics.setModuleName(modulePojo.getName());
									moduleLevelMetricsRepository.save(nextModuleMetrics);

								}
							}
							}catch(Exception e){
								System.out.println("Exception occured during updatemodule:moduleLevelMetricsRepository " +
										e.getMessage());
							}


							ModuleEstimate tempModuleEstimate = null;

							try{
							tempModuleEstimate = new ModuleEstimate();
							tempModuleEstimate.setModuleTimeStamp(getCurrentDateAndTime());
							tempModuleEstimate.setOriginalModuleId(modulePojo.getId());
							tempModuleEstimate.setName(modulePojo.getName());
							tempModuleEstimate.setEstimatedManualTestCases(modulePojo.getEstimatedManualTestCases());
							tempModuleEstimate.setEstimatedRegressionAutomationTestCases(modulePojo.getEstimatedRegressionAutomationTestCases());
							tempModuleEstimate.setEstimatedSanityAutomationTestCases(modulePojo.getEstimatedSanityAutomationTestCases());
							tempModuleEstimate.setProject(next);
							moduleEstimateRepository.save(tempModuleEstimate);
							}catch(Exception e){
								System.out.println("Exception occured during updatemodule:moduleEstimateRepository " +
										e.getMessage());
							}

							try{
							Iterable<LatestModuleEstimate> latestModuleEstimates = latestModuleEstimateRepository.findAll();
							Iterator<LatestModuleEstimate> latestModuleEstimatesIterator = latestModuleEstimates.iterator();

							while (latestModuleEstimatesIterator.hasNext()) {
								LatestModuleEstimate latestModuleEstimate = latestModuleEstimatesIterator.next();
								if ((latestModuleEstimate.getProject().getId() == id) &&
										(latestModuleEstimate.getName().compareTo(desiredModule.getName()) == 0)) {
									latestModuleEstimateRepository.delete(latestModuleEstimate);
								}
							}

							LatestModuleEstimate latestModuleEstimate = new LatestModuleEstimate(tempModuleEstimate);
							latestModuleEstimateRepository.save(latestModuleEstimate);

							}catch(Exception e){
								System.out.println("Exception occured during updatemodule:latestModuleEstimateRepository " +
										e.getMessage());
							}


							try {
									Iterable<FirstModuleEstimate> firstModuleEstimates = firstModuleEstimateRepository.findAll();
									Iterator<FirstModuleEstimate> firstModuleEstimatesIterator = firstModuleEstimates.iterator();

									while (firstModuleEstimatesIterator.hasNext()) {
										FirstModuleEstimate firstModuleEstimate = firstModuleEstimatesIterator.next();

										if ((firstModuleEstimate.getProject().getId() == id) &&
												(firstModuleEstimate.getName().compareTo(desiredModule.getName()) == 0)) {

											firstModuleEstimate.setName(modulePojo.getName());
											firstModuleEstimateRepository.save(firstModuleEstimate);

										}
									}
								}catch(Exception e){
									System.out.println("Exception occured during FirstModuleEstimate : " +
											e.getMessage());
								}


								try{
									desiredModule.setName(modulePojo.getName());
									desiredModule.setEstimatedManualTestCases(modulePojo.getEstimatedManualTestCases());
									desiredModule.setEstimatedRegressionAutomationTestCases(modulePojo.getEstimatedRegressionAutomationTestCases());
									desiredModule.setEstimatedSanityAutomationTestCases(modulePojo.getEstimatedSanityAutomationTestCases());
									desiredModule.setComplexity(modulePojo.getComplexity());
									desiredModule.setProject(next);
									moduleRepository.save(desiredModule);
								}catch(Exception e){
									System.out.println("Exception occured during updatemodule:moduleRepository " +
											e.getMessage());
								}

							jobDone = true;
							break;
						}
					}

					if (jobDone == true) {
						break;
					}
				}
			}
		}catch (Exception e) {
		}

		}catch (Exception e) {
		}
		return "OK";
	}


	@RequestMapping(value = "/projects/{id}/modules", method = GET)
	@ResponseBody
	public String getModules(@PathVariable("id") long id) {

		List<ModulePojo> modulesPojo = new ArrayList<ModulePojo>();
		Iterable<Project> allProjects = projectRepository.findAll();
		Iterator<Project> iterator = allProjects.iterator();
		List<Module> modules = null;

		while (iterator.hasNext()) {
			Project next = iterator.next();
			if (next.getId() == id) {
				modules = next.getModules();
				for (int i = 0; i < modules.size(); i++) {
					ModulePojo temp = new ModulePojo();
					temp.setId(modules.get(i).getId());
					temp.setName(modules.get(i).getName());
					temp.setEstimatedManualTestCases(modules.get(i).getEstimatedManualTestCases());
					temp.setEstimatedRegressionAutomationTestCases(modules.get(i).getEstimatedRegressionAutomationTestCases());
					temp.setEstimatedSanityAutomationTestCases(modules.get(i).getEstimatedSanityAutomationTestCases());
					temp.setComplexity(modules.get(i).getComplexity());
					modulesPojo.add(temp);
				}
				break;
			}
		}

		Gson gson = new Gson();
		System.out.println("{\"modules\":" + gson.toJson(modulesPojo) + "}");
		return "{\"modules\":" + gson.toJson(modulesPojo) + "}";
	}


	@RequestMapping(value = "/projects/{id}/modules", method = DELETE)
	@ResponseBody
	public void deleteModules(@PathVariable("id") long id) {

		Iterable<LatestModuleEstimate> allLatestModuleEstimates = latestModuleEstimateRepository.findAll();
		Iterator<LatestModuleEstimate> latestModuleEstimateIterator = allLatestModuleEstimates.iterator();

		while (latestModuleEstimateIterator.hasNext()) {
			LatestModuleEstimate latestModulesEstimate = latestModuleEstimateIterator.next();

			if (latestModulesEstimate.getProject().getId() == id) {
				try{
					latestModuleEstimateRepository.delete(latestModulesEstimate);
				}catch(Exception e){
				}
			}
		}


		Iterable<ModuleEstimate> allModuleEstimates = moduleEstimateRepository.findAll();
		Iterator<ModuleEstimate> moduleEstimateIterator = allModuleEstimates.iterator();

		while (moduleEstimateIterator.hasNext()) {
			ModuleEstimate modulesEstimate = moduleEstimateIterator.next();

			if (modulesEstimate.getProject().getId() == id) {
				try{
					moduleEstimateRepository.delete(modulesEstimate);
				}catch(Exception e){
				}
			}
		}


		Iterable<Module> allModules = moduleRepository.findAll();
		Iterator<Module> modulesIterator = allModules.iterator();


		while (modulesIterator.hasNext()) {
			Module module = modulesIterator.next();

			if (module.getProject().getId() == id) {
				try{
					moduleRepository.delete(module);
				}catch(Exception e){
				}
			}
		}

	}

	@RequestMapping(value = "/projects/{id}/modules/{moduleId}", method = DELETE)
	@ResponseBody
	public void deleteModule(@PathVariable("id") long id, @PathVariable("moduleId") long moduleId) {

		Iterable<Module> allModules = moduleRepository.findAll();
		Iterator<Module> modulesIterator = allModules.iterator();

		String moduleName = "";

		while (modulesIterator.hasNext()) {
			Module module = modulesIterator.next();
			if((module.getProject().getId() == id) && (module.getId() == moduleId))
			{
				moduleName = module.getName();
				moduleRepository.delete(module);
			}
		}


		Iterable<ModuleEstimate> allModuleEstimates = moduleEstimateRepository.findAll();
		Iterator<ModuleEstimate> allModuleEstimatesIterator = allModuleEstimates.iterator();

		while (allModuleEstimatesIterator.hasNext()) {
			ModuleEstimate moduleEstimate = allModuleEstimatesIterator.next();
			if((moduleEstimate.getProject().getId() == id) && (moduleEstimate.getName().compareTo(moduleName) == 0))
			{
				moduleEstimateRepository.delete(moduleEstimate);
			}
		}


		Iterable<LatestModuleEstimate> allLatestModuleEstimates = latestModuleEstimateRepository.findAll();
		Iterator<LatestModuleEstimate> allLatestModuleEstimatesIterator = allLatestModuleEstimates.iterator();

		while (allLatestModuleEstimatesIterator.hasNext()) {
			LatestModuleEstimate latestModuleEstimate = allLatestModuleEstimatesIterator.next();
			if((latestModuleEstimate.getProject().getId() == id) && (latestModuleEstimate.getName().compareTo(moduleName) == 0))
			{
				latestModuleEstimateRepository.delete(latestModuleEstimate);
			}
		}

	}



	@RequestMapping(value = "/projects/{id}/estimates", method = GET)
	@ResponseBody
	public String getProjectEstimates(@PathVariable("id") long id) {

		ArrayList<ProjectEstimatePojo> projectEstimatePojoArray = new ArrayList<ProjectEstimatePojo>();

		Iterable<LatestProjectEstimate> latestProjectEstimates = latestProjectEstimateRepository.findAll();
		Iterator<LatestProjectEstimate> latestProjectEstimatesIterator = latestProjectEstimates.iterator();

		Iterable<LatestModuleEstimate> latestModuleEstimates = latestModuleEstimateRepository.findAll();
		Iterator<LatestModuleEstimate> latestModuleEstimateIterator = latestModuleEstimates.iterator();

		ArrayList<LatestProjectEstimate> currentProjectEstimates = new ArrayList<LatestProjectEstimate>();
		ArrayList<LatestModuleEstimate> currentProjectModuleEstimates = new ArrayList<LatestModuleEstimate>();

		// First if there are any modules,
		// if no modules then send error message that modules should be added before project estimation
		// If modules exist for the current project, load them in currentProjectModuleEstimates
		boolean moduleFound = false;

		while (latestModuleEstimateIterator.hasNext()) {
			LatestModuleEstimate latestModuleEstimate = latestModuleEstimateIterator.next();

			if (latestModuleEstimate.getProject().getId() == id) {
				currentProjectModuleEstimates.add(latestModuleEstimate);
				moduleFound = true;
			}

		}

		if(moduleFound == false)
		{
			return "{\"estimates\":[]}";
			//return "Modules missing. Please add modules before you estimate the project";
		}

		// Load the existing project estimates for the current project in currentProjectEstimates

		String finalJson = null;
		boolean projectFound = false;

		while (latestProjectEstimatesIterator.hasNext()) {
			LatestProjectEstimate latestProjectEstimate = latestProjectEstimatesIterator.next();
			if (latestProjectEstimate.getProject().getId() == id) {
				projectFound = true;
				currentProjectEstimates.add(latestProjectEstimate);
				break;
			}
		}


		// If there are no existing estimates for the current project, then create on dummy estimate for the project
		// This will help to initiaze the estimates window
		// load the dummy estimate in currentProjectEstimates

		String plannedStartDate = null;
		String plannedEndDate = null;

		if(projectFound == false) {
			Iterable<Project> allProjects = projectRepository.findAll();
			Iterator<Project> projectIterator = allProjects.iterator();

			while (projectIterator.hasNext()) {
				Project nextProject = projectIterator.next();
				if (nextProject.getId() == id) {
					plannedStartDate = nextProject.getStartDate();
					plannedEndDate = nextProject.getEndDate();
					ProjectEstimate estimate = new ProjectEstimate();
					estimate.setProject(nextProject);
					estimate.setProjectTimeStamp(getCurrentDateAndTime());
					projectEstimateRepository.save(estimate);
					LatestProjectEstimate latestProjectEstimate = new LatestProjectEstimate(estimate);
					latestProjectEstimateRepository.save(latestProjectEstimate);
					currentProjectEstimates.add(latestProjectEstimate);
				}
			}
		}


		ProjectEstimatePojo projectEstimatePojo = new ProjectEstimatePojo();
		projectEstimatePojoArray.add(projectEstimatePojo);
		projectEstimatePojo.setId(currentProjectEstimates.get(0).getId());

		int index = 1;

		finalJson = "{\"estimates\":[{" + "\"id\": " + index++ + "," + "\"category\":\"TestCase Estimate\",\"type\":\"number\",\"name\":\"Total Number Of Requirements Planned\",\"value\":\"";
		finalJson = finalJson + currentProjectEstimates.get(0).getTotalNumberOfRequirementsPlanned() + "\"},";


		for (int i = 0; i < currentProjectModuleEstimates.size(); i++) {
			finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"category\":\"TestCase Estimate\",\"type\":\"number\",\"name\":\"" + "Module||" + currentProjectModuleEstimates.get(i).getName() + "||" + "Estimated Manual TestCases\",\"value\":\"";
			finalJson = finalJson + currentProjectModuleEstimates.get(i).getEstimatedManualTestcases() + "\"},";
			finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"category\":\"TestCase Estimate\",\"type\":\"number\",\"name\":\"" + "Module||" + currentProjectModuleEstimates.get(i).getName() + "||" + "Estimated Regression Automation TestCases\",\"value\":\"";
			finalJson = finalJson + currentProjectModuleEstimates.get(i).getEstimatedRegressionAutomationTestCases() + "\"},";
			finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"category\":\"TestCase Estimate\",\"type\":\"number\",\"name\":\"" + "Module||" + currentProjectModuleEstimates.get(i).getName() + "||" + "Estimated Sanity Automation TestCases\",\"value\":\"";
			finalJson = finalJson + currentProjectModuleEstimates.get(i).getEstimatedSanityAutomationTestCases() + "\"},";
		}

		try {
			finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"category\":\"Date Estimate\",\"type\":\"date\",\"name\":\"Planned Start Date\",\"value\":\"";

			if(currentProjectEstimates.get(0).getPlannedStartDate() == null) {
				finalJson = finalJson + plannedStartDate + "\"},";
			}
			else {
				finalJson = finalJson + currentProjectEstimates.get(0).getPlannedStartDate() + "\"},";
			}

		}catch(Exception e){

		}


		try {

			finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"category\":\"Date Estimate\",\"type\":\"date\",\"name\":\"Planned End Date\",\"value\":\"";

			if(currentProjectEstimates.get(0).getPlannedEndDate() == null) {
				finalJson = finalJson + plannedEndDate + "\"},";
			}
			else {
				finalJson = finalJson + currentProjectEstimates.get(0).getPlannedEndDate() + "\"},";
			}

		}catch(Exception e){

		}

		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"category\":\"TestCase Estimate\",\"type\":\"number\",\"name\":\"Total Number Of TestCases Tagged For Migration Execution\",\"value\":\"";
		finalJson = finalJson + currentProjectEstimates.get(0).getTotalNumberOfTestCasesTaggedForMigrationExecution() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"category\":\"TestCase Estimate\",\"type\":\"number\",\"name\":\"API Automation TestCases Estimate\",\"value\":\"";
		finalJson = finalJson + currentProjectEstimates.get(0).getAPIAutomationTestCaseEstimation() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"category\":\"TestCase Estimate\",\"type\":\"number\",\"name\":\"InProject Automation TestCases Estimate\",\"value\":\"";
		finalJson = finalJson + currentProjectEstimates.get(0).getInProjectAutomationTestCaseEstimation() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"category\":\"Effort Estimate\",\"type\":\"number\",\"name\":\"Test Planning Effort\",\"value\":\"";
		finalJson = finalJson + currentProjectEstimates.get(0).getTestPlanningEffort() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"category\":\"Effort Estimate\",\"type\":\"number\",\"name\":\"Manual TestCases Design Effort\",\"value\":\"";
		finalJson = finalJson + currentProjectEstimates.get(0).getManualTestCaseDesignEffort() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"category\":\"Effort Estimate\",\"type\":\"number\",\"name\":\"Regression Automation Test Design Effort\",\"value\":\"";
		finalJson = finalJson + currentProjectEstimates.get(0).getRegressionAutomationTestCaseDesignEffort() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"category\":\"Effort Estimate\",\"type\":\"number\",\"name\":\"InProject Automation Test Design Effort\",\"value\":\"";
		finalJson = finalJson + currentProjectEstimates.get(0).getInProjectAutomationTestCaseDesignEffort() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"category\":\"Effort Estimate\",\"type\":\"number\",\"name\":\"API Automation Test Design Effort\",\"value\":\"";
		finalJson = finalJson + currentProjectEstimates.get(0).getApiAutomationTestCaseDesignEffort() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"category\":\"Effort Estimate\",\"type\":\"number\",\"name\":\"Manual TestCases Execution Effort\",\"value\":\"";
		finalJson = finalJson + currentProjectEstimates.get(0).getManualTestCaseExecutionEffort() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"category\":\"Effort Estimate\",\"type\":\"number\",\"name\":\"Regression Automation Execution Effort\",\"value\":\"";
		finalJson = finalJson + currentProjectEstimates.get(0).getRegressionAutomationTestCaseExecutionEffort() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"category\":\"Effort Estimate\",\"type\":\"number\",\"name\":\"InProject Automation Execution Effort\",\"value\":\"";
		finalJson = finalJson + currentProjectEstimates.get(0).getInProjectAutomationTestCaseExecutionEffort() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"category\":\"Effort Estimate\",\"type\":\"number\",\"name\":\"API Automation Execution Effort\",\"value\":\"";
		finalJson = finalJson + currentProjectEstimates.get(0).getApiAutomationTestCaseExecutionEffort() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"category\":\"Effort Estimate\",\"type\":\"number\",\"name\":\"Manual TestCases Maintenance Effort\",\"value\":\"";
		finalJson = finalJson + currentProjectEstimates.get(0).getManualTestCaseMaintenanceEffort() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"category\":\"Effort Estimate\",\"type\":\"number\",\"name\":\"Automation TestCases Maintenance Effort\",\"value\":\"";
		finalJson = finalJson + currentProjectEstimates.get(0).getAutomationTestCaseMaintenanceEffort() + "\"},";
    	finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"category\":\"Resource Estimate\",\"type\":\"number\",\"name\":\"Planned Resources For TestCase Authoring\",\"value\":\"";
		finalJson = finalJson + currentProjectEstimates.get(0).getPlannedResourcesForManualTestCaseAuthoring() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"category\":\"Resource Estimate\",\"type\":\"number\",\"name\":\"Planned Resources For Automation Design\",\"value\":\"";
		finalJson = finalJson + currentProjectEstimates.get(0).getPlannedResourcesForAutomationDesign() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"category\":\"Productivity Estimate\",\"type\":\"number\",\"name\":\"Number Of TestCases Designed Per Hour\",\"value\":\"";
		finalJson = finalJson + currentProjectEstimates.get(0).getNumberOfTestCasesDesignedPerHour() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"category\":\"Productivity Estimate\",\"type\":\"number\",\"name\":\"Number Of TestCases Executed Per Hour\",\"value\":\"";
		finalJson = finalJson + currentProjectEstimates.get(0).getNumberOfTestCasesExecutedPerHour() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"category\":\"Resource Estimate\",\"type\":\"number\",\"name\":\"Planned Resources For Manual TestCase Execution\",\"value\":\"";
		finalJson = finalJson + currentProjectEstimates.get(0).getPlannedResourcesForManualTestCaseExecution() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"category\":\"Resource Estimate\",\"type\":\"number\",\"name\":\"Planned Resources For Automation Execution\",\"value\":\"";
		finalJson = finalJson + currentProjectEstimates.get(0).getPlannedResourcesForAutomationExecution() + "\"}]}";

		System.out.println(finalJson);

		return finalJson;

    }


	@RequestMapping(value = "/projects/{id}/baselineEstimates", method = GET)
	@ResponseBody
	public String getBaselineProjectEstimates(@PathVariable("id") long id) {

		Iterable<FirstProjectEstimate> firstProjectEstimates = firstProjectEstimateRepository.findAll();
		Iterator<FirstProjectEstimate> firstProjectEstimatesIterator = firstProjectEstimates.iterator();

		Iterable<FirstModuleEstimate> firstModuleEstimates = firstModuleEstimateRepository.findAll();
		Iterator<FirstModuleEstimate> firstModuleEstimateIterator = firstModuleEstimates.iterator();

		String finalJson = null;
		FirstProjectEstimate firstProjectEstimate = null;
		while (firstProjectEstimatesIterator.hasNext()) {
			firstProjectEstimate = firstProjectEstimatesIterator.next();
			if (firstProjectEstimate.getProject().getId() == id) {
				break;
			}
		}


		ArrayList<FirstModuleEstimate> firstModuleEstimateArray = new ArrayList<FirstModuleEstimate>();
		while (firstModuleEstimateIterator.hasNext()) {
			FirstModuleEstimate firstModuleEstimate = firstModuleEstimateIterator.next();
			if (firstModuleEstimate.getProject().getId() == id) {
				firstModuleEstimateArray.add(firstModuleEstimate);
			}
		}

		// If there are no existing estimates for the current project, then create on dummy estimate for the project
		// This will help to initiaze the estimates window
		// load the dummy estimate in currentProjectEstimates


		String plannedStartDate = null;
		String plannedEndDate = null;
		Project nextProject = null;

			Iterable<Project> allProjects = projectRepository.findAll();
			Iterator<Project> projectIterator = allProjects.iterator();

			while (projectIterator.hasNext()) {
				nextProject = projectIterator.next();
				if (nextProject.getId() == id) {
					plannedStartDate = nextProject.getStartDate();
					plannedEndDate = nextProject.getEndDate();
					break;
				}
			}


		int index = 1;

		finalJson = "{\"estimates\":[{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"TestCase Estimate\",\"type\":\"number\",\"name\":\"Total Number Of Requirements Planned\",\"value\":\"";
		finalJson = finalJson + firstProjectEstimate.getTotalNumberOfRequirementsPlanned() + "\"},";


		for (int i = 0; i < firstModuleEstimateArray.size(); i++) {
			finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"TestCase Estimate\",\"type\":\"number\",\"name\":\"" + "Module||" + firstModuleEstimateArray.get(i).getName() + "||" + "Estimated Manual TestCases\",\"value\":\"";
			finalJson = finalJson + firstModuleEstimateArray.get(i).getEstimatedManualTestcases() + "\"},";
			finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"TestCase Estimate\",\"type\":\"number\",\"name\":\"" + "Module||" + firstModuleEstimateArray.get(i).getName() + "||" + "Estimated Regression Automation TestCases\",\"value\":\"";
			finalJson = finalJson + firstModuleEstimateArray.get(i).getEstimatedRegressionAutomationTestCases() + "\"},";
			finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"TestCase Estimate\",\"type\":\"number\",\"name\":\"" + "Module||" + firstModuleEstimateArray.get(i).getName() + "||" + "Estimated Sanity Automation TestCases\",\"value\":\"";
			finalJson = finalJson + firstModuleEstimateArray.get(i).getEstimatedSanityAutomationTestCases() + "\"},";
		}


		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"Date Estimate\",\"type\":\"date\",\"name\":\"Planned Start Date\",\"value\":\"";
		finalJson = finalJson + plannedStartDate + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"Date Estimate\",\"type\":\"date\",\"name\":\"Planned End Date\",\"value\":\"";
		finalJson = finalJson + plannedEndDate + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"TestCase Estimate\",\"type\":\"number\",\"name\":\"Total Number Of TestCases Tagged For Migration Execution\",\"value\":\"";
		finalJson = finalJson + firstProjectEstimate.getTotalNumberOfTestCasesTaggedForMigrationExecution() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"TestCase Estimate\",\"type\":\"number\",\"name\":\"API Automation TestCases Estimate\",\"value\":\"";
		finalJson = finalJson + firstProjectEstimate.getAPIAutomationTestCaseEstimation() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"TestCase Estimate\",\"type\":\"number\",\"name\":\"InProject Automation TestCases Estimate\",\"value\":\"";
		finalJson = finalJson + firstProjectEstimate.getInProjectAutomationTestCaseEstimation() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"Effort Estimate\",\"type\":\"number\",\"name\":\"Test Planning Effort\",\"value\":\"";
		finalJson = finalJson + firstProjectEstimate.getTestPlanningEffort() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"Effort Estimate\",\"type\":\"number\",\"name\":\"Manual TestCases Design Effort\",\"value\":\"";
		finalJson = finalJson + firstProjectEstimate.getManualTestCaseDesignEffort() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"Effort Estimate\",\"type\":\"number\",\"name\":\"Regression Automation Test Design Effort\",\"value\":\"";
		finalJson = finalJson + firstProjectEstimate.getRegressionAutomationTestCaseDesignEffort() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"Effort Estimate\",\"type\":\"number\",\"name\":\"InProject Automation Test Design Effort\",\"value\":\"";
		finalJson = finalJson + firstProjectEstimate.getInProjectAutomationTestCaseDesignEffort() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"Effort Estimate\",\"type\":\"number\",\"name\":\"API Automation Test Design Effort\",\"value\":\"";
		finalJson = finalJson + firstProjectEstimate.getApiAutomationTestCaseDesignEffort() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"Effort Estimate\",\"type\":\"number\",\"name\":\"Manual TestCases Execution Effort\",\"value\":\"";
		finalJson = finalJson + firstProjectEstimate.getManualTestCaseExecutionEffort() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"Effort Estimate\",\"type\":\"number\",\"name\":\"Regression Automation Execution Effort\",\"value\":\"";
		finalJson = finalJson + firstProjectEstimate.getRegressionAutomationTestCaseExecutionEffort() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"Effort Estimate\",\"type\":\"number\",\"name\":\"InProject Automation Execution Effort\",\"value\":\"";
		finalJson = finalJson + firstProjectEstimate.getInProjectAutomationTestCaseExecutionEffort() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"Effort Estimate\",\"type\":\"number\",\"name\":\"API Automation Execution Effort\",\"value\":\"";
		finalJson = finalJson + firstProjectEstimate.getApiAutomationTestCaseExecutionEffort() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"Effort Estimate\",\"type\":\"number\",\"name\":\"Manual TestCases Maintenance Effort\",\"value\":\"";
		finalJson = finalJson + firstProjectEstimate.getManualTestCaseMaintenanceEffort() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"Effort Estimate\",\"type\":\"number\",\"name\":\"Automation TestCases Maintenance Effort\",\"value\":\"";
		finalJson = finalJson + firstProjectEstimate.getAutomationTestCaseMaintenanceEffort() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"Resource Estimate\",\"type\":\"number\",\"name\":\"Planned Resources For TestCase Authoring\",\"value\":\"";
		finalJson = finalJson + firstProjectEstimate.getPlannedResourcesForManualTestCaseAuthoring() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"Resource Estimate\",\"type\":\"number\",\"name\":\"Planned Resources For Automation Design\",\"value\":\"";
		finalJson = finalJson + firstProjectEstimate.getPlannedResourcesForAutomationDesign() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"Productivity Estimate\",\"type\":\"number\",\"name\":\"Number Of TestCases Designed Per Hour\",\"value\":\"";
		finalJson = finalJson + firstProjectEstimate.getNumberOfTestCasesDesignedPerHour() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"Productivity Estimate\",\"type\":\"number\",\"name\":\"Number Of TestCases Executed Per Hour\",\"value\":\"";
		finalJson = finalJson + firstProjectEstimate.getNumberOfTestCasesExecutedPerHour() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"Resource Estimate\",\"type\":\"number\",\"name\":\"Planned Resources For Manual TestCase Execution\",\"value\":\"";
		finalJson = finalJson + firstProjectEstimate.getPlannedResourcesForManualTestCaseExecution() + "\"},";
		finalJson = finalJson + "{" + "\"id\": " + index++ + "," + "\"edit\":\"false\"," + "\"category\":\"Resource Estimate\",\"type\":\"number\",\"name\":\"Planned Resources For Automation Execution\",\"value\":\"";
		finalJson = finalJson + firstProjectEstimate.getPlannedResourcesForAutomationExecution() + "\"}]}";

		System.out.println(finalJson);

		return finalJson;

	}


	@PostMapping(value = "/projects/{id}/estimates")
	@ResponseBody
	public String updateProjectEstimates(@PathVariable("id") long id, @RequestBody List<EstimatePojo> estimates) {

		Iterable<Project> allProjects = projectRepository.findAll();
		Iterator<Project> projectIterator = allProjects.iterator();

		while (projectIterator.hasNext()) {
			Project nextProject = projectIterator.next();

			if (nextProject.getId() == id) {

				ProjectEstimate temp = new ProjectEstimate();
				temp.setProjectTimeStamp(getCurrentDateAndTime());
				temp.setProject(nextProject);

				for (int i = 0; i < estimates.size(); i++) {

					System.out.println(estimates.get(i).getName() + " : " +  estimates.get(i).getValue());

			        if (estimates.get(i).getName().compareTo("Total Number Of Requirements Planned") == 0) {
						temp.setTotalNumberOfRequirementsPlanned(estimates.get(i).getValue());
					} else if (estimates.get(i).getName().compareTo("Planned Start Date") == 0) {
						temp.setPlannedStartDate(estimates.get(i).getValue());
					} else if (estimates.get(i).getName().compareTo("Planned End Date") == 0) {
						temp.setPlannedEndDate(estimates.get(i).getValue());
					} else if (estimates.get(i).getName().compareTo("API Automation TestCases Estimate") == 0) {
						temp.setAPIAutomationTestCaseEstimation(estimates.get(i).getValue());
					} else if (estimates.get(i).getName().compareTo("InProject Automation TestCases Estimate") == 0) {
						temp.setInProjectAutomationTestCaseEstimation(estimates.get(i).getValue());
					} else if (estimates.get(i).getName().compareTo("Total Number Of TestCases Tagged For Migration Execution") == 0) {
						temp.setTotalNumberOfTestCasesTaggedForMigrationExecution(estimates.get(i).getValue());
					}  else if (estimates.get(i).getName().compareTo("Test Planning Effort") == 0) {
						temp.setTestPlanningEffort(estimates.get(i).getValue());
					} else if (estimates.get(i).getName().compareTo("Manual TestCases Design Effort") == 0) {
						temp.setManualTestCaseDesignEffort(estimates.get(i).getValue());
					} else if (estimates.get(i).getName().compareTo("Regression Automation Test Design Effort") == 0) {
						temp.setRegressionAutomationTestCaseDesignEffort(estimates.get(i).getValue());
					}  else if (estimates.get(i).getName().compareTo("InProject Automation Test Design Effort") == 0) {
						temp.setInProjectAutomationTestCaseDesignEffort(estimates.get(i).getValue());
					} else if (estimates.get(i).getName().compareTo("API Automation Test Design Effort") == 0) {
						temp.setApiAutomationTestCaseDesignEffort(estimates.get(i).getValue());
					} else if (estimates.get(i).getName().compareTo("Manual TestCases Execution Effort") == 0) {
						temp.setManualTestCaseExecutionEffort(estimates.get(i).getValue());
					} else if (estimates.get(i).getName().compareTo("Regression Automation Execution Effort") == 0) {
						temp.setRegressionAutomationTestCaseExecutionEffort(estimates.get(i).getValue());
					} else if (estimates.get(i).getName().compareTo("InProject Automation Execution Effort") == 0) {
						temp.setInProjectAutomationTestCaseExecutionEffort(estimates.get(i).getValue());
					} else if (estimates.get(i).getName().compareTo("API Automation Execution Effort") == 0) {
						temp.setApiAutomationTestCaseExecutionEffort(estimates.get(i).getValue());
					} else if (estimates.get(i).getName().compareTo("Manual TestCases Maintenance Effort") == 0) {
						temp.setManualTestCaseMaintenanceEffort(estimates.get(i).getValue());
					} else if (estimates.get(i).getName().compareTo("Automation TestCases Maintenance Effort") == 0) {
						temp.setAutomationTestCaseMaintenanceEffort(estimates.get(i).getValue());
					} else if (estimates.get(i).getName().compareTo("Total Effort In Person Days") == 0) {
						temp.setTotalEffortInPersonDays(estimates.get(i).getValue());
					} else if (estimates.get(i).getName().compareTo("Planned Resources For Manual TestCase Execution") == 0){
						temp.setPlannedResourcesForManualTestCaseExecution(estimates.get(i).getValue());
					} else if (estimates.get(i).getName().compareTo("Planned Resources For TestCase Authoring") == 0) {
						temp.setPlannedResourcesForManualTestCaseAuthoring(estimates.get(i).getValue());
					} else if (estimates.get(i).getName().compareTo("Planned Resources For Automation Design") == 0) {
						temp.setPlannedResourcesForAutomationDesign(estimates.get(i).getValue());
					} else if (estimates.get(i).getName().compareTo("Planned Resources For Automation Execution") == 0) {
						temp.setPlannedResourcesForAutomationExecution(estimates.get(i).getValue());
					}  else if (estimates.get(i).getName().compareTo("Number Of TestCases Designed Per Hour") == 0) {
						temp.setNumberOfTestCasesDesignedPerHour(estimates.get(i).getValue());
					}  else if (estimates.get(i).getName().compareTo("Number Of TestCases Executed Per Hour") == 0) {
						temp.setNumberOfTestCasesExecutedPerHour(estimates.get(i).getValue());
					} else if (estimates.get(i).getName().contains("Estimated Manual TestCases")) {

						StringTokenizer tokenizer = new StringTokenizer(estimates.get(i).getName(), "||");
						String token = tokenizer.nextToken();
						token = tokenizer.nextToken();

						List<Module> currentModules = nextProject.getModules();
						List<ModuleEstimate> currentModuleEstimates = nextProject.getModuleEstimates();
						List<LatestModuleEstimate> latestModuleEstimates = nextProject.getLatestModuleEstimates();


						for (int moduleIndex = 0; moduleIndex < currentModules.size(); moduleIndex++) {
							if (currentModules.get(moduleIndex).getName().compareTo(token) == 0) {
								currentModules.get(moduleIndex).setEstimatedManualTestCases(estimates.get(i).getValue());
								break;
							}
						}


						for (int moduleIndex = 0; moduleIndex < currentModuleEstimates.size(); moduleIndex++) {
							if (currentModuleEstimates.get(moduleIndex).getName().compareTo(token) == 0) {
								currentModuleEstimates.get(moduleIndex).setEstimatedManualTestCases(estimates.get(i).getValue());
								break;
							}
						}

						for (int moduleIndex = 0; moduleIndex < latestModuleEstimates.size(); moduleIndex++) {
							if (latestModuleEstimates.get(moduleIndex).getName().compareTo(token) == 0) {
								latestModuleEstimates.get(moduleIndex).setEstimatedManualTestCases(estimates.get(i).getValue());
								break;
							}
						}

					}

					else if (estimates.get(i).getName().contains("Estimated Regression Automation TestCases")) {

						StringTokenizer tokenizer = new StringTokenizer(estimates.get(i).getName(), "||");
						String token = tokenizer.nextToken();
						token = tokenizer.nextToken();


						List<Module> currentModules = nextProject.getModules();
						List<ModuleEstimate> currentModuleEstimates = nextProject.getModuleEstimates();
						List<LatestModuleEstimate> latestModuleEstimates = nextProject.getLatestModuleEstimates();


						for(int moduleIndex = 0; moduleIndex < currentModules.size(); moduleIndex++)
						{
							if(currentModules.get(moduleIndex).getName().compareTo(token) == 0)
							{
								currentModules.get(moduleIndex).setEstimatedRegressionAutomationTestCases(estimates.get(i).getValue());
								break;
							}
						}


						for(int moduleIndex = 0; moduleIndex < currentModuleEstimates.size(); moduleIndex++)
						{
							if(currentModuleEstimates.get(moduleIndex).getName().compareTo(token) == 0)
							{
								currentModuleEstimates.get(moduleIndex).setEstimatedRegressionAutomationTestCases(estimates.get(i).getValue());
								break;
							}
						}

						for(int moduleIndex = 0; moduleIndex < latestModuleEstimates.size(); moduleIndex++)
						{
							if(latestModuleEstimates.get(moduleIndex).getName().compareTo(token) == 0)
							{
								latestModuleEstimates.get(moduleIndex).setEstimatedRegressionAutomationTestCases(estimates.get(i).getValue());
								break;
							}
						}

					}



					else if (estimates.get(i).getName().contains("Estimated Sanity Automation TestCases")) {

						StringTokenizer tokenizer = new StringTokenizer(estimates.get(i).getName(), "||");
						String token = tokenizer.nextToken();
						token = tokenizer.nextToken();


						List<Module> currentModules = nextProject.getModules();
						List<ModuleEstimate> currentModuleEstimates = nextProject.getModuleEstimates();
						List<LatestModuleEstimate> latestModuleEstimates = nextProject.getLatestModuleEstimates();


						for(int moduleIndex = 0; moduleIndex < currentModules.size(); moduleIndex++)
						{
							if(currentModules.get(moduleIndex).getName().compareTo(token) == 0)
							{
								currentModules.get(moduleIndex).setEstimatedSanityAutomationTestCases(estimates.get(i).getValue());
								break;
							}
						}


						for(int moduleIndex = 0; moduleIndex < currentModuleEstimates.size(); moduleIndex++)
						{
							if(currentModuleEstimates.get(moduleIndex).getName().compareTo(token) == 0)
							{
								currentModuleEstimates.get(moduleIndex).setEstimatedSanityAutomationTestCases(estimates.get(i).getValue());
								break;
							}
						}

						for(int moduleIndex = 0; moduleIndex < latestModuleEstimates.size(); moduleIndex++)
						{
							if(latestModuleEstimates.get(moduleIndex).getName().compareTo(token) == 0)
							{
								latestModuleEstimates.get(moduleIndex).setEstimatedSanityAutomationTestCases(estimates.get(i).getValue());
								break;
							}
						}

					}

				}
					projectEstimateRepository.save(temp);

					Iterable<LatestProjectEstimate> latestProjectEstimates = latestProjectEstimateRepository.findAll();
					Iterator<LatestProjectEstimate> latestProjectsEstimatesIterator = latestProjectEstimates.iterator();


					while (latestProjectsEstimatesIterator.hasNext()) {
						LatestProjectEstimate latestProjectEstimate = latestProjectsEstimatesIterator.next();
						if (latestProjectEstimate.getProject().getName().compareTo(nextProject.getName()) == 0) {
							latestProjectEstimateRepository.delete(latestProjectEstimate);
						}
					}


					LatestProjectEstimate latestProjectEstimate = new LatestProjectEstimate(temp);
					latestProjectEstimateRepository.save(latestProjectEstimate);


				Iterable<FirstProjectEstimate> firstProjectEstimateIterable = firstProjectEstimateRepository.findAll();
				Iterator<FirstProjectEstimate> firstProjectEstimateIterator = firstProjectEstimateIterable.iterator();

				while(firstProjectEstimateIterator.hasNext()) {
					FirstProjectEstimate firstEstimate = firstProjectEstimateIterator.next();

					if (firstEstimate.getProject().getId() == id) {

						try {
							if ((latestProjectEstimate.getRegressionAutomationTestCaseEstimation()) != null) {

								if ((latestProjectEstimate.getRegressionAutomationTestCaseEstimation().contains("null"))) {

								} else {
									try {
										if (Integer.parseInt(firstEstimate.getRegressionAutomationTestCaseEstimation()) == 0) {
											firstEstimate.setRegressionAutomationTestCaseEstimation(latestProjectEstimate.getRegressionAutomationTestCaseEstimation());
										}
									} catch (Exception e) {
										firstEstimate.setRegressionAutomationTestCaseEstimation(latestProjectEstimate.getRegressionAutomationTestCaseEstimation());
									}
								}
							}
						}catch(Exception e){
						System.out.println("Exception occurred : "+ e.getMessage());
					}


					try {

							if((latestProjectEstimate.getInProjectAutomationTestCaseEstimation())!=null){

							if((latestProjectEstimate.getInProjectAutomationTestCaseEstimation().contains("null"))){

							}
							else {
								try {
									if (Integer.parseInt(firstEstimate.getInProjectAutomationTestCaseEstimation()) == 0) {
										firstEstimate.setInProjectAutomationTestCaseEstimation(latestProjectEstimate.getInProjectAutomationTestCaseEstimation());
									}
								}catch(Exception e){
									firstEstimate.setInProjectAutomationTestCaseEstimation(latestProjectEstimate.getInProjectAutomationTestCaseEstimation());
								}
							}

						}

					}catch(Exception e){
						System.out.println("Exception occurred : "+ e.getMessage());
					}


							try {

								if((latestProjectEstimate.getAPIAutomationTestCaseEstimation())!=null){
							if((latestProjectEstimate.getAPIAutomationTestCaseEstimation().contains("null"))){

							}
							else {
								try {
									if (Integer.parseInt(firstEstimate.getAPIAutomationTestCaseEstimation()) == 0) {
										firstEstimate.setAPIAutomationTestCaseEstimation(latestProjectEstimate.getAPIAutomationTestCaseEstimation());
									}
								}catch(Exception e){
									firstEstimate.setAPIAutomationTestCaseEstimation(latestProjectEstimate.getAPIAutomationTestCaseEstimation());
								}
							}
						}

							}catch(Exception e){
								System.out.println("Exception occurred : "+ e.getMessage());
							}

						try {

						if((latestProjectEstimate.getTestPlanningEffort())!=null){
							if((latestProjectEstimate.getTestPlanningEffort().contains("null"))){

							}
							else {
								try {
									if (Integer.parseInt(firstEstimate.getTestPlanningEffort()) == 0) {
										firstEstimate.setTestPlanningEffort(latestProjectEstimate.getTestPlanningEffort());
									}
								}catch(Exception e){
									firstEstimate.setTestPlanningEffort(latestProjectEstimate.getTestPlanningEffort());
								}
							}
						}
						}catch(Exception e){
							System.out.println("Exception occurred : "+ e.getMessage());
						}

									try {

						if((latestProjectEstimate.getManualTestCaseDesignEffort())!=null){

							if((latestProjectEstimate.getManualTestCaseDesignEffort().contains("null"))){

							}
							else {
								try {
									if (Integer.parseInt(firstEstimate.getManualTestCaseDesignEffort()) == 0) {
										firstEstimate.setManualTestCaseDesignEffort(latestProjectEstimate.getManualTestCaseDesignEffort());
									}
								}catch(Exception e){
									firstEstimate.setManualTestCaseDesignEffort(latestProjectEstimate.getManualTestCaseDesignEffort());
								}
							}

						}
									}catch(Exception e){
										System.out.println("Exception occurred : "+ e.getMessage());
									}

										try {

						if((latestProjectEstimate.getAutomationTestCaseDesignEffort())!=null){
							if((latestProjectEstimate.getAutomationTestCaseDesignEffort().contains("null"))){}
							else {
								try {
									if (Integer.parseInt(firstEstimate.getAutomationTestCaseDesignEffort()) == 0) {
										firstEstimate.setAutomationTestCaseDesignEffort(latestProjectEstimate.getAutomationTestCaseDesignEffort());
									}
								}catch(Exception e){
									firstEstimate.setAutomationTestCaseDesignEffort(latestProjectEstimate.getAutomationTestCaseDesignEffort());
								}
							}
						}
										}catch(Exception e){
											System.out.println("Exception occurred : "+ e.getMessage());
										}

											try {

												if((latestProjectEstimate.getRegressionAutomationTestCaseDesignEffort())!=null){
							if((latestProjectEstimate.getRegressionAutomationTestCaseDesignEffort().contains("null"))){}
							else {
								try {
									if (Integer.parseInt(firstEstimate.getRegressionAutomationTestCaseDesignEffort()) == 0) {
										firstEstimate.setRegressionAutomationTestCaseDesignEffort(latestProjectEstimate.getRegressionAutomationTestCaseDesignEffort());
									}
								}catch(Exception e){
									firstEstimate.setRegressionAutomationTestCaseDesignEffort(latestProjectEstimate.getRegressionAutomationTestCaseDesignEffort());
								}
							}
						}
											}catch(Exception e){
												System.out.println("Exception occurred : "+ e.getMessage());
											}


												try {

													if((latestProjectEstimate.getInProjectAutomationTestCaseDesignEffort())!=null){

							if((latestProjectEstimate.getInProjectAutomationTestCaseDesignEffort().contains("null"))){

							}
							else {
								try {
									if (Integer.parseInt(firstEstimate.getInProjectAutomationTestCaseDesignEffort()) == 0) {
										firstEstimate.setInProjectAutomationTestCaseDesignEffort(latestProjectEstimate.getInProjectAutomationTestCaseDesignEffort());
									}
								}catch(Exception e){
									firstEstimate.setInProjectAutomationTestCaseDesignEffort(latestProjectEstimate.getInProjectAutomationTestCaseDesignEffort());
								}
							}
						}
												}catch(Exception e){
													System.out.println("Exception occurred : "+ e.getMessage());
												}

													try {

														if((latestProjectEstimate.getApiAutomationTestCaseDesignEffort())!=null){
							if((latestProjectEstimate.getApiAutomationTestCaseDesignEffort().contains("null"))){}
							else {
								try {
									if (Integer.parseInt(firstEstimate.getApiAutomationTestCaseDesignEffort()) == 0) {
										firstEstimate.setApiAutomationTestCaseDesignEffort(latestProjectEstimate.getApiAutomationTestCaseDesignEffort());
									}
								}catch(Exception e){
									firstEstimate.setApiAutomationTestCaseDesignEffort(latestProjectEstimate.getApiAutomationTestCaseDesignEffort());
								}
							}
						}
													}catch(Exception e){
														System.out.println("Exception occurred : "+ e.getMessage());
													}

														try {

						if((latestProjectEstimate.getTotalNumberOfTestCasesTaggedForMigrationExecution())!=null){
							if((latestProjectEstimate.getTotalNumberOfTestCasesTaggedForMigrationExecution().contains("null"))){}
							else {
								try {
									if (Integer.parseInt(firstEstimate.getTotalNumberOfTestCasesTaggedForMigrationExecution()) == 0) {
										firstEstimate.setTotalNumberOfTestCasesTaggedForMigrationExecution(latestProjectEstimate.getTotalNumberOfTestCasesTaggedForMigrationExecution());
									}
								}catch(Exception e){
									firstEstimate.setTotalNumberOfTestCasesTaggedForMigrationExecution(latestProjectEstimate.getTotalNumberOfTestCasesTaggedForMigrationExecution());
								}
							}
						}
														}catch(Exception e){
															System.out.println("Exception occurred : "+ e.getMessage());
														}

															try {

						if((latestProjectEstimate.getManualTestCaseExecutionEffort())!=null){
							if((latestProjectEstimate.getManualTestCaseExecutionEffort().contains("null"))){}
							else {
								try {
									if (Integer.parseInt(firstEstimate.getManualTestCaseExecutionEffort()) == 0) {
										firstEstimate.setManualTestCaseExecutionEffort(latestProjectEstimate.getManualTestCaseExecutionEffort());
									}
								}catch(Exception e){
									firstEstimate.setManualTestCaseExecutionEffort(latestProjectEstimate.getManualTestCaseExecutionEffort());
								}
							}
						}
															}catch(Exception e){
																System.out.println("Exception occurred : "+ e.getMessage());
															}

																try {


																	if((latestProjectEstimate.getAutomationTestCaseExecutionEffort())!=null){
							if((latestProjectEstimate.getAutomationTestCaseExecutionEffort().contains("null"))){

							}
							else {
								try {
									if (Integer.parseInt(firstEstimate.getAutomationTestCaseExecutionEffort()) == 0) {
										firstEstimate.setAutomationTestCaseExecutionEffort(latestProjectEstimate.getAutomationTestCaseExecutionEffort());
									}
								}catch(Exception e){
									firstEstimate.setAutomationTestCaseExecutionEffort(latestProjectEstimate.getAutomationTestCaseExecutionEffort());
								}
							}
						}
																}catch(Exception e){
																	System.out.println("Exception occurred : "+ e.getMessage());
																}

																	try {

																		if((latestProjectEstimate.getRegressionAutomationTestCaseExecutionEffort())!=null){
							if((latestProjectEstimate.getRegressionAutomationTestCaseExecutionEffort().contains("null"))){

							}
							else {
								try {
									if (Integer.parseInt(firstEstimate.getRegressionAutomationTestCaseExecutionEffort()) == 0) {
										firstEstimate.setRegressionAutomationTestCaseExecutionEffort(latestProjectEstimate.getRegressionAutomationTestCaseExecutionEffort());
									}
								}catch(Exception e){
									firstEstimate.setRegressionAutomationTestCaseExecutionEffort(latestProjectEstimate.getRegressionAutomationTestCaseExecutionEffort());
								}
							}
						}
																	}catch(Exception e){
																		System.out.println("Exception occurred : "+ e.getMessage());
																	}


						try {

																			if((latestProjectEstimate.getInProjectAutomationTestCaseExecutionEffort())!=null){
							if((latestProjectEstimate.getInProjectAutomationTestCaseExecutionEffort().contains("null"))){

							}
							else {
								try {
									if (Integer.parseInt(firstEstimate.getInProjectAutomationTestCaseExecutionEffort()) == 0) {
										firstEstimate.setInProjectAutomationTestCaseExecutionEffort(latestProjectEstimate.getInProjectAutomationTestCaseExecutionEffort());
									}
								}catch(Exception e){
									firstEstimate.setInProjectAutomationTestCaseExecutionEffort(latestProjectEstimate.getInProjectAutomationTestCaseExecutionEffort());
								}
							}
						}
						}catch(Exception e){
							System.out.println("Exception occurred : "+ e.getMessage());
						}


						try {

																				if((latestProjectEstimate.getApiAutomationTestCaseExecutionEffort())!=null){
							if((latestProjectEstimate.getApiAutomationTestCaseExecutionEffort().contains("null"))){

							}
							else {
								try {
									if (Integer.parseInt(firstEstimate.getApiAutomationTestCaseExecutionEffort()) == 0) {
										firstEstimate.setApiAutomationTestCaseExecutionEffort(latestProjectEstimate.getApiAutomationTestCaseExecutionEffort());
									}
								}catch(Exception e){
									firstEstimate.setApiAutomationTestCaseExecutionEffort(latestProjectEstimate.getApiAutomationTestCaseExecutionEffort());
								}
							}
						}
						}catch(Exception e){
							System.out.println("Exception occurred : "+ e.getMessage());
						}


						//if((latestProjectEstimate.getManualTestCaseMaintenanceEffort())!=null){if((latestProjectEstimate.getManualTestCaseMaintenanceEffort().contains("null"))){}else if(Integer.parseInt(firstEstimate.getManualTestCaseMaintenanceEffort())==0){firstEstimate.setManualTestCaseMaintenanceEffort(latestProjectEstimate.getManualTestCaseMaintenanceEffort());}}

						System.out.println("Stage (1)");
																				try {


																					if((latestProjectEstimate.getManualTestCaseMaintenanceEffort())!=null){

							System.out.println("Stage (2)");

							if((latestProjectEstimate.getManualTestCaseMaintenanceEffort().contains("null"))){

								System.out.println("Stage (3)");

							}
							else {
								try {
									if (Integer.parseInt(firstEstimate.getManualTestCaseMaintenanceEffort()) == 0) {

										System.out.println("Stage (4)");

										firstEstimate.setManualTestCaseMaintenanceEffort(latestProjectEstimate.getManualTestCaseMaintenanceEffort());

									}
								}catch(Exception e){
									firstEstimate.setManualTestCaseMaintenanceEffort(latestProjectEstimate.getManualTestCaseMaintenanceEffort());
								}
							}
						}
																				}catch(Exception e){
																					System.out.println("Exception occurred : "+ e.getMessage());
																				}

						try {


																						if((latestProjectEstimate.getAutomationTestCaseMaintenanceEffort())!=null){
							if((latestProjectEstimate.getAutomationTestCaseMaintenanceEffort().contains("null"))){

							}
							else
								try {
									if (Integer.parseInt(firstEstimate.getAutomationTestCaseMaintenanceEffort()) == 0) {
										firstEstimate.setAutomationTestCaseMaintenanceEffort(latestProjectEstimate.getAutomationTestCaseMaintenanceEffort());
									}
								}catch(Exception e){
									firstEstimate.setAutomationTestCaseMaintenanceEffort(latestProjectEstimate.getAutomationTestCaseMaintenanceEffort());
								}
							}

					}catch(Exception e){
						System.out.println("Exception occurred : "+ e.getMessage());
					}
					try {


						if ((latestProjectEstimate.getTotalEffortInPersonDays()) != null) {
							if ((latestProjectEstimate.getTotalEffortInPersonDays().contains("null"))) {

							} else {
								try {
									if (Integer.parseInt(firstEstimate.getTotalEffortInPersonDays()) == 0) {
										firstEstimate.setTotalEffortInPersonDays(latestProjectEstimate.getTotalEffortInPersonDays());
									}
								} catch (Exception e) {
									firstEstimate.setTotalEffortInPersonDays(latestProjectEstimate.getTotalEffortInPersonDays());
								}
							}

						}
					}catch(Exception e){
							System.out.println("Exception occurred : "+ e.getMessage());
						}


						try {

																								if((latestProjectEstimate.getPlannedResourcesForManualTestCaseExecution())!=null){
							if((latestProjectEstimate.getPlannedResourcesForManualTestCaseExecution().contains("null"))){

							}
							else {
								try {
									if (Integer.parseInt(firstEstimate.getPlannedResourcesForManualTestCaseExecution()) == 0) {
										firstEstimate.setPlannedResourcesForManualTestCaseExecution(latestProjectEstimate.getPlannedResourcesForManualTestCaseExecution());
									}
								}catch(Exception e){
									firstEstimate.setPlannedResourcesForManualTestCaseExecution(latestProjectEstimate.getPlannedResourcesForManualTestCaseExecution());
								}
							}
						}
						}catch(Exception e){
							System.out.println("Exception occurred : "+ e.getMessage());
						}



						try {

																									if((latestProjectEstimate.getPlannedResourcesForManualTestCaseAuthoring())!=null){
							if((latestProjectEstimate.getPlannedResourcesForManualTestCaseAuthoring().contains("null"))){

							}
							else {
								try {
									if (Integer.parseInt(firstEstimate.getPlannedResourcesForManualTestCaseAuthoring()) == 0) {
										firstEstimate.setPlannedResourcesForManualTestCaseAuthoring(latestProjectEstimate.getPlannedResourcesForManualTestCaseAuthoring());
									}
								}catch(Exception e){
									firstEstimate.setPlannedResourcesForManualTestCaseAuthoring(latestProjectEstimate.getPlannedResourcesForManualTestCaseAuthoring());
								}
							}
						}
						}catch(Exception e){
							System.out.println("Exception occurred : "+ e.getMessage());
						}


																									try {

																										if((latestProjectEstimate.getPlannedResourcesForAutomationDesign())!=null){
							if((latestProjectEstimate.getPlannedResourcesForAutomationDesign().contains("null"))){}
							else {
								try {
									if (Integer.parseInt(firstEstimate.getPlannedResourcesForAutomationDesign()) == 0) {
										firstEstimate.setPlannedResourcesForAutomationDesign(latestProjectEstimate.getPlannedResourcesForAutomationDesign());
									}
								}catch(Exception e){
									firstEstimate.setPlannedResourcesForAutomationDesign(latestProjectEstimate.getPlannedResourcesForAutomationDesign());
								}
							}
						}
																									}catch(Exception e){
																										System.out.println("Exception occurred : "+ e.getMessage());
																									}


						System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Planned Resources For Automation Execution>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
						try{
							System.out.println(latestProjectEstimate.getPlannedResourcesForAutomationExecution());
						}catch(Exception e){
							System.out.println("Exception occurred : "+ e.getMessage());
						}


																										try {

																											if((latestProjectEstimate.getPlannedResourcesForAutomationExecution())!=null){
							if((latestProjectEstimate.getPlannedResourcesForAutomationExecution().contains("null"))){

							}
							else {
								try {
									if (Integer.parseInt(firstEstimate.getPlannedResourcesForAutomationExecution()) == 0) {
										firstEstimate.setPlannedResourcesForAutomationExecution(latestProjectEstimate.getPlannedResourcesForAutomationExecution());
									}
								}catch(Exception e){
									firstEstimate.setPlannedResourcesForAutomationExecution(latestProjectEstimate.getPlannedResourcesForAutomationExecution());
								}
							}
						}
																										}catch(Exception e){
																											System.out.println("Exception occurred : "+ e.getMessage());
																										}


						try {

																												if((latestProjectEstimate.getProjectedBilling())!=null){
							if((latestProjectEstimate.getProjectedBilling().contains("null"))){

							}
							else {
								try {
									if (Integer.parseInt(firstEstimate.getProjectedBilling()) == 0) {
										firstEstimate.setProjectedBilling(latestProjectEstimate.getProjectedBilling());
									}
								}catch(Exception e){
									firstEstimate.setProjectedBilling(latestProjectEstimate.getProjectedBilling());
								}
							}
						}
						}catch(Exception e){
							System.out.println("Exception occurred : "+ e.getMessage());
						}


						try {


																												if((latestProjectEstimate.getTotalNumberOfRequirementsPlanned())!=null){
							if((latestProjectEstimate.getTotalNumberOfRequirementsPlanned().contains("null"))){

							}
							else {
								try {
									if (Integer.parseInt(firstEstimate.getTotalNumberOfRequirementsPlanned()) == 0) {
										firstEstimate.setTotalNumberOfRequirementsPlanned(latestProjectEstimate.getTotalNumberOfRequirementsPlanned());
									}
								}catch(Exception e){
									firstEstimate.setTotalNumberOfRequirementsPlanned(latestProjectEstimate.getTotalNumberOfRequirementsPlanned());
								}
							}
						}

						}catch(Exception e){
							System.out.println("Exception occurred : "+ e.getMessage());
						}

																											try {


																												if((latestProjectEstimate.getNumberOfTestCasesDesignedPerHour())!=null){
							if((latestProjectEstimate.getNumberOfTestCasesDesignedPerHour().contains("null"))){

							}
							else {
								try {
									if (Integer.parseInt(firstEstimate.getNumberOfTestCasesDesignedPerHour()) == 0) {
										firstEstimate.setNumberOfTestCasesDesignedPerHour(latestProjectEstimate.getNumberOfTestCasesDesignedPerHour());
									}
								}catch(Exception e){
									firstEstimate.setNumberOfTestCasesDesignedPerHour(latestProjectEstimate.getNumberOfTestCasesDesignedPerHour());
								}
							}
						}

																											}catch(Exception e){
																												System.out.println("Exception occurred : "+ e.getMessage());
																											}

						try {


																												if((latestProjectEstimate.getNumberOfTestCasesExecutedPerHour())!=null){
							if((latestProjectEstimate.getNumberOfTestCasesExecutedPerHour().contains("null"))){

							}
							else {
								try {
									if (Integer.parseInt(firstEstimate.getNumberOfTestCasesExecutedPerHour()) == 0) {
										firstEstimate.setNumberOfTestCasesExecutedPerHour(latestProjectEstimate.getNumberOfTestCasesExecutedPerHour());
									}
								}catch(Exception e){
									firstEstimate.setNumberOfTestCasesExecutedPerHour(latestProjectEstimate.getNumberOfTestCasesExecutedPerHour());
								}
							}
						}
						}catch(Exception e){
							System.out.println("Exception occurred : "+ e.getMessage());
						}


						firstProjectEstimateRepository.save(firstEstimate);
						break;

					}
				}
			}
		}

		return "OK";
	}


	@RequestMapping(value = "/projects/{id}/estimates", method = DELETE)
	@ResponseBody
	public void deleteEstimates(@PathVariable("id") long id) {


			Iterable<LatestProjectEstimate> allLatestProjectEstimates = latestProjectEstimateRepository.findAll();
			Iterator<LatestProjectEstimate> latestProjectEstimateIterator = allLatestProjectEstimates.iterator();

			while (latestProjectEstimateIterator.hasNext()) {
				LatestProjectEstimate latestProjectEstimate = latestProjectEstimateIterator.next();

				if (latestProjectEstimate.getProject().getId() == id) {
					try{
						latestProjectEstimateRepository.delete(latestProjectEstimate);
					}catch(Exception e){
					}
				}
			}

			Iterable<ProjectEstimate> allProjectEstimates = projectEstimateRepository.findAll();
			Iterator<ProjectEstimate> projectEstimateIterator = allProjectEstimates.iterator();

			while (projectEstimateIterator.hasNext()) {
				ProjectEstimate projectEstimate = projectEstimateIterator.next();

				if (projectEstimate.getProject().getId() == id) {
					try{
						projectEstimateRepository.delete(projectEstimate);
					}catch(Exception e){
					}
				}
			}


			Iterable<LatestModuleEstimate> allLatestModuleEstimates = latestModuleEstimateRepository.findAll();
			Iterator<LatestModuleEstimate> latestModuleEstimateIterator = allLatestModuleEstimates.iterator();

			while (latestModuleEstimateIterator.hasNext()) {
				LatestModuleEstimate latestModulesEstimate = latestModuleEstimateIterator.next();

				if (latestModulesEstimate.getProject().getId() == id) {
					try{
						latestModuleEstimateRepository.delete(latestModulesEstimate);
					}catch(Exception e){
					}
				}
			}


			Iterable<ModuleEstimate> allModuleEstimates = moduleEstimateRepository.findAll();
			Iterator<ModuleEstimate> moduleEstimateIterator = allModuleEstimates.iterator();

			while (moduleEstimateIterator.hasNext()) {
				ModuleEstimate modulesEstimate = moduleEstimateIterator.next();

				if (modulesEstimate.getProject().getId() == id) {
					try{
						moduleEstimateRepository.delete(modulesEstimate);
					}catch(Exception e){
					}
				}
			}
		}
}