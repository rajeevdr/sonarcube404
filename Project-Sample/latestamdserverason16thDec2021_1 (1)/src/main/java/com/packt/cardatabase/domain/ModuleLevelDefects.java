package com.packt.cardatabase.domain;

import java.util.ArrayList;
import java.util.List;


public class ModuleLevelDefects {

	private long id;

	private String projectFieldName;
	private ArrayList<String> projectName;
	private String moduleName;
	private String defectModuleFieldName;
	private ArrayList<String> tags;

	public Long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getProjectFieldName() {
		return projectFieldName;
	}

	public void setProjectFieldName(String projectFieldName) {
		this.projectFieldName = projectFieldName;
	}

	public ArrayList<String> getProjectName() {
		return projectName;
	}

	public void setProjectName(ArrayList<String> projectName) {
		this.projectName = projectName;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getDefectModuleFieldName() {
		return defectModuleFieldName;
	}

	public void setDefectModuleFieldName(String defectModuleFieldName) {
		this.defectModuleFieldName = defectModuleFieldName;
	}

	public ArrayList<String> getTags() {
		return tags;
	}

	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}

}