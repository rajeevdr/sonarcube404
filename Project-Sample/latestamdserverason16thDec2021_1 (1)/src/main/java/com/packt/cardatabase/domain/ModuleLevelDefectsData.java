package com.packt.cardatabase.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class ModuleLevelDefectsData {
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private long id;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "project")
	private Project project;

	private String projectFieldName;
	private ArrayList<String> projectName;
	private String moduleName;
	private String defectModuleFieldName;
	private ArrayList<String> tags;


	public String getProjectFieldName() {
		return projectFieldName;
	}

	public void setProjectFieldName(String projectFieldName) {
		this.projectFieldName = projectFieldName;
	}

	public ArrayList<String> getProjectName() {
		return projectName;
	}

	public void setProjectName(ArrayList<String> projectName) {
		this.projectName = projectName;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getDefectModuleFieldName() {
		return defectModuleFieldName;
	}

	public void setDefectModuleFieldName(String defectModuleFieldName) {
		this.defectModuleFieldName = defectModuleFieldName;
	}

	public ArrayList<String> getTags() {
		return tags;
	}

	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

}