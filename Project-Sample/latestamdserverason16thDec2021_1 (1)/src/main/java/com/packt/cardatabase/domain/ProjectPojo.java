package com.packt.cardatabase.domain;


public class ProjectPojo {

    private long id;
    private String name; // Free Text
    private String type; // Fix Bid Transformation, MTM Transformation, MTM BAU, T&M Staffing Transformation, T&M BAU
    private String cluster; // Free Text
    private String domain; // Retail Banking, Corporate Banking, Wealth MGMT, Telecom, Others
    private String deliveryManager; // Free Text
    private String technology; // Free text for e.g. T24, CRM, finnacle
    private String category; // Simple, Medium, Complex
    private String lastUpdateOn; // dd-mm-yyyy auto generated
    private String startDate;// dd-mm-yyyy (Cannot be changed after the first time)
    private String endDate;// dd-mm-yyyy
    private String lastSyncUpdateDate;



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ProjectPojo() {}

    public ProjectPojo(String name, String type, String cluster, String domain, String deliveryManager, String technology, String category, String lastUpdateOn, String startDate, String endDate) {
        this.name = name;
        this.type = type;
        this.cluster = cluster;
        this.domain = domain;
        this.deliveryManager = deliveryManager;
        this.technology = technology;
        this.category = category;
        this.lastUpdateOn = lastUpdateOn;
        this.startDate = startDate;
        this.endDate = endDate;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCluster() {
        return cluster;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getDeliveryManager() {
        return deliveryManager;
    }

    public void setDeliveryManager(String deliveryManager) {
        this.deliveryManager = deliveryManager;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLastUpdateOn() {
        return lastUpdateOn;
    }

    public void setLastUpdateOn(String lastUpdateOn) {
        this.lastUpdateOn = lastUpdateOn;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }


    public String getLastSyncUpdateDate() {
        return lastSyncUpdateDate;
    }

    public void setLastSyncUpdateDate(String lastSyncUpdateDate) {
        this.lastSyncUpdateDate = lastSyncUpdateDate;
    }


}