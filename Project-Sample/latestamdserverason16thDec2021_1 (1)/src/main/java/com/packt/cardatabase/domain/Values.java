package com.packt.cardatabase.domain;

import java.util.ArrayList;

public class Values {


	public Values(){
		super();
	}


	private ArrayList<String> sit;
	private ArrayList<String> uat;
	private ArrayList<String> production;
	private ArrayList<String> automation;
	private ArrayList<String> manual;

	public ArrayList<String> getSit() {
		return sit;
	}

	public void setSit(ArrayList<String> sit) {
		this.sit = sit;
	}

	public ArrayList<String> getUat() {
		return uat;
	}

	public void setUat(ArrayList<String> uat) {
		this.uat = uat;
	}

	public ArrayList<String> getProduction() {
		return production;
	}

	public void setProduction(ArrayList<String> production) {
		this.production = production;
	}

	public ArrayList<String> getAutomation() {
		return automation;
	}

	public void setAutomation(ArrayList<String> automation) {
		this.automation = automation;
	}

	public ArrayList<String> getManual() {
		return manual;
	}

	public void setManual(ArrayList<String> manual) {
		this.manual = manual;
	}


}