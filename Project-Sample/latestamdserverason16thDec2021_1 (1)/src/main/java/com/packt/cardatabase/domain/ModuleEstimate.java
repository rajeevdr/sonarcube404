package com.packt.cardatabase.domain;


import javax.persistence.*;


@Entity
public class ModuleEstimate {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private long originalModuleId;
    private String name;
    private String estimatedManualTestCases;
    private String estimatedRegressionAutomationTestCases;
    private String estimatedSanityAutomationTestCases;
    private String moduleTimeStamp;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;

    public ModuleEstimate() {}

    public ModuleEstimate(Long originalModuleId,
                          String moduleTimeStamp,
                          String name,
                          String estimatedManualTestCases,
                          String estimatedRegressionAutomationTestCases,
                          String estimatedSanityAutomationTestCases,
                          Project project) {
        super();
        this.originalModuleId = originalModuleId;
        this.moduleTimeStamp = moduleTimeStamp;
        this.name = name;
        this.estimatedManualTestCases = estimatedManualTestCases;
        this.estimatedRegressionAutomationTestCases = estimatedRegressionAutomationTestCases;
        this.estimatedSanityAutomationTestCases = estimatedSanityAutomationTestCases;
        this.project = project;
    }

    public String getModuleTimeStamp() {
        return moduleTimeStamp;
    }

    public void setModuleTimeStamp(String moduleTimeStamp) {
        this.moduleTimeStamp = moduleTimeStamp;
    }

    public Long getOriginalModuleId() {
        return originalModuleId;
    }

    public void setOriginalModuleId(long originalModuleId) {
        this.originalModuleId = originalModuleId;
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEstimatedManualTestCases() {
        return estimatedManualTestCases;
    }

    public void setEstimatedManualTestCases(String estimatedManualTestCases) {
        this.estimatedManualTestCases = estimatedManualTestCases;
    }

    public String getEstimatedRegressionAutomationTestCases() {
        return estimatedRegressionAutomationTestCases;
    }

    public void setEstimatedRegressionAutomationTestCases(String estimatedRegressionAutomationTestCases) {
        this.estimatedRegressionAutomationTestCases = estimatedRegressionAutomationTestCases;
    }

    public String getEstimatedSanityAutomationTestCases() {
        return estimatedSanityAutomationTestCases;
    }

    public void setEstimatedSanityAutomationTestCases(String estimatedSanityAutomationTestCases) {
        this.estimatedSanityAutomationTestCases = estimatedSanityAutomationTestCases;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

}