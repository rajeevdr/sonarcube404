package com.packt.cardatabase.domain;

import javax.persistence.*;

@Entity
public class MonthlyGovernance {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private String budgetCostForTheMonth;
    private String actualCostForTheMonth;
    private String actualBillingForTheMonth;
    private String grossMarginForTheMonth;
    private String numberOfTestEngineers;
    private String numberOfLeads;
    private String numberOfManagers;
    private String noOfTestCasesForCostApprovedCRs;
    private String totalNumberOfResignationsSubmitted;
    private String totalNumberOfResourcesRelieved;
    private String numberOfFormalGovernanceMeetings;
    private String numberOfStepChangesProposed;
    private String numberOfStepChangesInProgress;
    private String numberOfStepChangesImplemented;
    private String numbeOfCIsProposed;
    private String numberOfCIsInProgress;
    private String numberOfCIsImplemented;
    private int monthNumber;
    private int yearNumber;




    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public MonthlyGovernance() {}

    public MonthlyGovernance(String budgetCostForTheMonth,
                             String actualCostForTheMonth,
                             String actualBillingForTheMonth,
                                     String grossMarginForTheMonth,
                                     String numberOfTestEngineers,
                                     String numberOfLeads,
                                     String numberOfManagers,
                                     String noOfTestCasesForCostApprovedCRs,
                                     String totalNumberOfResignationsSubmitted,
                                     String totalNumberOfResourcesRelieved,
                                     String numberOfFormalGovernanceMeetings,
                                     String numberOfStepChangesProposed,
                                     String numberOfStepChangesInProgress,
                                     String numberOfStepChangesImplemented,
                                     String numbeOfCIsProposed,
                                     String numberOfCIsInProgress,
                                     String numberOfCIsImplemented,
                                     int monthNumber,
                                     int yearNumber,
                                     Project project)  {
        super();
        this.budgetCostForTheMonth = budgetCostForTheMonth;
        this.actualCostForTheMonth = actualCostForTheMonth;
        this.actualBillingForTheMonth = actualBillingForTheMonth;
        this.grossMarginForTheMonth = grossMarginForTheMonth;
        this.numberOfTestEngineers = numberOfTestEngineers;
        this.numberOfLeads = numberOfLeads;
        this.numberOfManagers = numberOfManagers;
        this.noOfTestCasesForCostApprovedCRs = noOfTestCasesForCostApprovedCRs;
        this.totalNumberOfResignationsSubmitted = totalNumberOfResignationsSubmitted;
        this.totalNumberOfResourcesRelieved = totalNumberOfResourcesRelieved;
        this.numberOfFormalGovernanceMeetings = numberOfFormalGovernanceMeetings;
        this.numberOfStepChangesProposed = numberOfStepChangesProposed;
        this.numberOfStepChangesInProgress = numberOfStepChangesInProgress;
        this.numberOfStepChangesImplemented = numberOfStepChangesImplemented;
        this.numbeOfCIsProposed = numbeOfCIsProposed;
        this.numberOfCIsInProgress = numberOfCIsInProgress;
        this.numberOfCIsImplemented = numberOfCIsImplemented;
        this.monthNumber = monthNumber;
        this.yearNumber = yearNumber;
        this.project = project;
    }

    public int getMonthNumber() {
        return monthNumber;
    }
    public String getMonthNumberName() {
        return "Month";
    }
    public void setMonthNumber(int monthNumber) {
        this.monthNumber = monthNumber;
    }

    public int getYearNumber() {
        return yearNumber;
    }
    public String getYearNumberName() {
        return "Year";
    }
    public void setYearNumber(int yearNumber) {
        this.yearNumber = yearNumber;
    }


    public int getBudgetCostForTheMonth() {

        if(budgetCostForTheMonth == null)
            return 0;
        return Integer.parseInt(budgetCostForTheMonth);

    }

    public String getBudgetCostForTheMonthName() {
        return "Budget Cost For The Month";
    }

    public void setBudgetCostForTheMonth(String budgetCostForTheMonth) {
        this.budgetCostForTheMonth = budgetCostForTheMonth;
    }

    public int getActualCostForTheMonth() {

        if(actualCostForTheMonth == null)
            return 0;
        return Integer.parseInt(actualCostForTheMonth);

    }

    public String getActualCostForTheMonthName() {
        return "Actual Cost For The Month";
    }

    public void setActualCostForTheMonth(String actualCostForTheMonth) {
        this.actualCostForTheMonth = actualCostForTheMonth;
    }

    public int getGrossMarginForTheMonth() {

        if(grossMarginForTheMonth == null)
            return 0;
        return Integer.parseInt(grossMarginForTheMonth);

    }

    public String getGrossMarginForTheMonthName() {
        return "Gross Margin For The Month";
    }

    public void setGrossMarginForTheMonth(String grossMarginForTheMonth) {
        this.grossMarginForTheMonth = grossMarginForTheMonth;
    }

    public int getActualBillingForTheMonth() {

        if(actualBillingForTheMonth == null)
            return 0;
        return Integer.parseInt(actualBillingForTheMonth);
    }

    public String getActualBillingForTheMonthName() {
        return "Actual Billing For The Month";
    }

    public void setActualBillingForTheMonth(String actualBillingForTheMonth) {
        this.actualBillingForTheMonth = actualBillingForTheMonth;
    }

    public int getNumberOfTestEngineers() {

        if(numberOfTestEngineers == null)
            return 0;
        return Integer.parseInt(numberOfTestEngineers);

    }


    public String getNumberOfTestEngineersName() {
        return "Number Of Test Engineers";
    }

    public void setNumberOfTestEngineers(String numberOfTestEngineers) {
        this.numberOfTestEngineers = numberOfTestEngineers;
    }

    public int getNumberOfLeads() {

        if(numberOfLeads == null)
            return 0;
        return Integer.parseInt(numberOfLeads);

    }

    public String getNumberOfLeadsName() {
        return "Number Of Leads";
    }


    public void setNumberOfLeads(String numberOfLeads) {
        this.numberOfLeads = numberOfLeads;
    }

    public int getNumberOfManagers() {

        if(numberOfManagers == null)
            return 0;
        return Integer.parseInt(numberOfManagers);

    }

    public String getNumberOfManagersName() {
        return "Number Of Managers";
    }


    public void setNumberOfManagers(String numberOfManagers) {
        this.numberOfManagers = numberOfManagers;
    }

    public int getNoOfTestCasesForCostApprovedCRs() {

        if(noOfTestCasesForCostApprovedCRs == null)
            return 0;
        return Integer.parseInt(noOfTestCasesForCostApprovedCRs);

    }

    public String getNoOfTestCasesForCostApprovedCRsName() {
        return "Number Of TestCases For Cost Approved CRs";
    }

    public void setNoOfTestCasesForCostApprovedCRs(String noOfTestCasesForCostApprovedCRs) {
        this.noOfTestCasesForCostApprovedCRs = noOfTestCasesForCostApprovedCRs;
    }

    public int getTotalNumberOfResignationsSubmitted() {

        if(totalNumberOfResignationsSubmitted == null)
            return 0;
        return Integer.parseInt(totalNumberOfResignationsSubmitted);

    }

    public String getTotalNumberOfResignationsSubmittedName() {
        return "Total Number Of Resignations Submitted";
    }

    public void setTotalNumberOfResignationsSubmitted(String totalNumberOfResignationsSubmitted) {
        this.totalNumberOfResignationsSubmitted = totalNumberOfResignationsSubmitted;
    }

    public int getTotalNumberOfResourcesRelieved() {

        if(totalNumberOfResourcesRelieved == null)
            return 0;
        return Integer.parseInt(totalNumberOfResourcesRelieved);
    }

    public String getTotalNumberOfResourcesRelievedName() {
        return "Total Number Of Resources Relieved";
    }


    public void setTotalNumberOfResourcesRelieved(String totalNumberOfResourcesRelieved) {
        this.totalNumberOfResourcesRelieved = totalNumberOfResourcesRelieved;
    }

    public int getNumberOfFormalGovernanceMeetings() {

        if(numberOfFormalGovernanceMeetings == null)
            return 0;
        return Integer.parseInt(numberOfFormalGovernanceMeetings);
    }


    public String getNumberOfFormalGovernanceMeetingsName() {
        return "Number Of Formal Governance Meetings";
    }


    public void setNumberOfFormalGovernanceMeetings(String numberOfFormalGovernanceMeetings) {
        this.numberOfFormalGovernanceMeetings = numberOfFormalGovernanceMeetings;
    }

    public int getNumberOfStepChangesProposed() {

        if(numberOfStepChangesProposed == null)
            return 0;
        return Integer.parseInt(numberOfStepChangesProposed);
    }


    public String getNumberOfStepChangesProposedName() {
        return "Number Of Step Changes Proposed";
    }


    public void setNumberOfStepChangesProposed(String numberOfStepChangesProposed) {
        this.numberOfStepChangesProposed = numberOfStepChangesProposed;
    }

    public int getNumberOfStepChangesInProgress() {

        if(numberOfStepChangesInProgress == null)
            return 0;
        return Integer.parseInt(numberOfStepChangesInProgress);
    }


    public String getNumberOfStepChangesInProgressName() {
        return "Number Of Step Changes In Progress";
    }



    public void setNumberOfStepChangesInProgress(String numberOfStepChangesInProgress) {
        this.numberOfStepChangesInProgress = numberOfStepChangesInProgress;
    }

    public int getNumberOfStepChangesImplemented() {

        if(numberOfStepChangesImplemented == null)
            return 0;
        return Integer.parseInt(numberOfStepChangesImplemented);
    }


    public String getNumberOfStepChangesImplementedName() {
        return "Number Of Step Changes Implemented";
    }


    public void setNumberOfStepChangesImplemented(String numberOfStepChangesImplemented) {
        this.numberOfStepChangesImplemented = numberOfStepChangesImplemented;
    }

    public int getNumberOfCIsProposed() {

        if(numbeOfCIsProposed == null)
            return 0;
        return Integer.parseInt(numbeOfCIsProposed);
    }

    public String getNumberOfCIsProposedName() {
        return "Number Of CIs Proposed";
    }

    public void setNumberOfCIsProposed(String numbeOfCIsProposed) {
        this.numbeOfCIsProposed = numbeOfCIsProposed;
    }

    public int getNumberOfCIsInProgress() {

        if(numberOfCIsInProgress == null)
            return 0;
        return Integer.parseInt(numberOfCIsInProgress);
    }

    public String getNumberOfCIsInProgressName() {
        return "Number Of CIs In Progress";
    }

    public void setNumberOfCIsInProgress(String numberOfCIsInProgress) {
        this.numberOfCIsInProgress = numberOfCIsInProgress;
    }

    public int getNumberOfCIsImplemented() {

        if(numberOfCIsImplemented == null)
            return 0;
        return Integer.parseInt(numberOfCIsImplemented);
    }


    public String getNumberOfCIsImplementedName() {
        return "Number Of CIs Implemented";
    }

    public void setNumberOfCIsImplemented(String numberOfCIsImplemented) {
        this.numberOfCIsImplemented = numberOfCIsImplemented;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

}