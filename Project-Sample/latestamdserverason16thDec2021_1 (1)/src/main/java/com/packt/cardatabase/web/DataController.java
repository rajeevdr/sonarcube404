package com.packt.cardatabase.web;

import com.google.gson.Gson;
import com.packt.cardatabase.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;


@RestController
public class DataController {

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private ModuleRepository moduleRepository;

	@Autowired
	private WeeklyProjectLevelTestExecutionProgressRepository weeklyProjectLevelTestExecutionProgressRepository;

	@Autowired
	private WeeklyModuleLevelTestExecutionProgressRepository weeklyModuleLevelTestExecutionProgressRepository;

	@Autowired
	private WeeklyModuleLevelTestDesignProgressRepository weeklyModuleLevelTestDesignProgressRepository;

	@Autowired
	private WeeklyProjectLevelTestDesignProgressRepository weeklyProjectLevelTestDesignProgressRepository;

	@Autowired
	private WeekDataRepository weekDataRepository;

	@Autowired
	private WeeklyProjectGovernanceRepository weeklyProjectGovernanceRepository;

	@Autowired
	private WeeklyMetricsRepo weeklyMetricsRepository;



	@RequestMapping(value = "/projects/{id}/testExecution/weeklyProgress/week/{week}/year/{year}", method = RequestMethod.DELETE)
	@ResponseBody
	public String deleteProgress(@PathVariable("id") long id, @PathVariable(value = "week") String week, @PathVariable(value = "year") String year) {

		try {

			StringTokenizer tokenizer = new StringTokenizer(week, "-");
			tokenizer.nextToken();
			int weekNumber = Integer.parseInt(tokenizer.nextToken());
			int yearNumber = Integer.parseInt(year);

			System.out.println("The desired week number is : " + weekNumber);
			System.out.println("The desired year number is : " + yearNumber);


			try {
				Iterable<WeeklyProjectLevelTestDesignProgress> weeklyProjectLevelTestDesignProgress = weeklyProjectLevelTestDesignProgressRepository.findAll();
				Iterator<WeeklyProjectLevelTestDesignProgress> weeklyProjectLevelTestDesignProgressIterator = weeklyProjectLevelTestDesignProgress.iterator();

				while (weeklyProjectLevelTestDesignProgressIterator.hasNext()) {

					WeeklyProjectLevelTestDesignProgress projectProgress = weeklyProjectLevelTestDesignProgressIterator.next();

					if ((projectProgress.getProject().getId() == id) &&
							(projectProgress.getWeekNumber() == weekNumber) &&
							(projectProgress.getYearNumber() == yearNumber)) {
						try {
							weeklyProjectLevelTestDesignProgressRepository.delete(projectProgress);
						} catch (Exception e) {
						}
					}
				}

			} catch (Exception e) {

			}


			try {
				Iterable<WeeklyProjectLevelTestExecutionProgress> weeklyProjectLevelTestExecutionProgress = weeklyProjectLevelTestExecutionProgressRepository.findAll();
				Iterator<WeeklyProjectLevelTestExecutionProgress> weeklyProjectLevelTestExecutionProgressRepositoryIterator = weeklyProjectLevelTestExecutionProgress.iterator();

				while (weeklyProjectLevelTestExecutionProgressRepositoryIterator.hasNext()) {
					WeeklyProjectLevelTestExecutionProgress next = weeklyProjectLevelTestExecutionProgressRepositoryIterator.next();
					if ((next.getWeekNumber() == weekNumber) && (next.getProject().getId() == id) && (next.getYearNumber() == yearNumber)) {

						try {
							weeklyProjectLevelTestExecutionProgressRepository.delete(next);
						} catch (Exception e) {
						}

					}
				}
			}catch(Exception e){
			}

			try {
				Iterable<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgress = weeklyModuleLevelTestDesignProgressRepository.findAll();
				Iterator<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgressIterator = weeklyModuleLevelTestDesignProgress.iterator();

				while (weeklyModuleLevelTestDesignProgressIterator.hasNext()) {

					WeeklyModuleLevelTestDesignProgress moduleProgress = weeklyModuleLevelTestDesignProgressIterator.next();

					if ((moduleProgress.getProject().getId() == id) &&
							(moduleProgress.getWeekNumber() == weekNumber) &&
							(moduleProgress.getYearNumber() == yearNumber)) {

						try {
							weeklyModuleLevelTestDesignProgressRepository.delete(moduleProgress);
						} catch (Exception e) {
						}

					}
				}

			} catch (Exception e) {
			}

			try {
				Iterable<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgress = weeklyModuleLevelTestExecutionProgressRepository.findAll();
				Iterator<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgressIterator = weeklyModuleLevelTestExecutionProgress.iterator();

				while (weeklyModuleLevelTestExecutionProgressIterator.hasNext()) {

					WeeklyModuleLevelTestExecutionProgress moduleProgress = weeklyModuleLevelTestExecutionProgressIterator.next();

					if ((moduleProgress.getProject().getId() == id) &&
							(moduleProgress.getWeekNumber() == weekNumber) &&
							(moduleProgress.getYearNumber() == yearNumber)) {

						try {
							weeklyModuleLevelTestExecutionProgressRepository.delete(moduleProgress);
						} catch (Exception e) {
						}

					}
				}

			} catch (Exception e) {
			}


			try {
				Iterable<WeeklyProjectGovernance> weeklyProjectGovernanceIterable = weeklyProjectGovernanceRepository.findAll();
				Iterator<WeeklyProjectGovernance> weeklyProjectGovernanceIterator = weeklyProjectGovernanceIterable.iterator();

				while (weeklyProjectGovernanceIterator.hasNext()) {

					WeeklyProjectGovernance projectGovernance = weeklyProjectGovernanceIterator.next();

					if ((projectGovernance.getProject().getId() == id) &&
							(projectGovernance.getWeekNumber() == weekNumber) &&
							(projectGovernance.getYearNumber() == yearNumber)) {

						try {
							weeklyProjectGovernanceRepository.delete(projectGovernance);
						} catch (Exception e) {
						}

					}
				}

			} catch (Exception e) {
			}

			try {
				Iterable<WeeklyMetrics> weeklyMetricsIterable = weeklyMetricsRepository.findAll();
				Iterator<WeeklyMetrics> weeklyMetricsIterator = weeklyMetricsIterable.iterator();

				while (weeklyMetricsIterator.hasNext()) {

					WeeklyMetrics weeklyMetrics = weeklyMetricsIterator.next();

					if ((weeklyMetrics.getProject().getId() == id) &&
							(weeklyMetrics.getWeekNumber() == weekNumber) &&
							(weeklyMetrics.getYearNumber() == yearNumber)) {

						try {
							weeklyMetricsRepository.delete(weeklyMetrics);
						} catch (Exception e) {
						}

					}
				}

			} catch (Exception e) {
			}


		} catch (Exception e) {
		}

		return "SUCCESS";
	}
}