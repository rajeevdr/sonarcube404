package com.packt.cardatabase.domain;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class QuarterData {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private int quarterNumber;
    private int yearNumber;
    private String quarterStartDate;
    private String quarterEndDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public QuarterData() {}


    public QuarterData(int quarterNumber, int yearNumber, String quarterStartDate, String quarterEndDate) {
        super();
        this.quarterNumber = quarterNumber;
        this.yearNumber = yearNumber;
        this.quarterStartDate = quarterStartDate;
        this.quarterEndDate = quarterEndDate;
    }


    public int getQuarterNumber() {
        return quarterNumber;
    }

    public void setQuarterNumber(int quarterNumber) {
        this.quarterNumber = quarterNumber;
    }

    public int getYearNumber() {
        return yearNumber;
    }

    public void setYearNumber(int yearNumber) {
        this.yearNumber = yearNumber;
    }

    public String getQuarterStartDate() {
        return quarterStartDate;
    }

    public void setQuarterStartDate(String quarterStartDate) {
        this.quarterStartDate = quarterStartDate;
    }

    public String getQuarterEndDate() {
        return quarterEndDate;
    }

    public void setQuarterEndDate(String quarterEndDate) {
        this.quarterEndDate = quarterEndDate;
    }


}