package com.packt.cardatabase.domain;


import javax.persistence.*;


@Entity
public class FirstModuleEstimate {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private String name;
    private String estimatedManualTestCases;
    private String estimatedRegressionAutomationTestCases;
    private String estimatedSanityAutomationTestCases;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;

    public FirstModuleEstimate() {}

    public FirstModuleEstimate(String name,
                               String estimatedManualTestCases,
                               String estimatedRegressionAutomationTestCases,
                               String estimatedSanityAutomationTestCases,
                               Project project) {
        super();
        this.name = name;
        this.estimatedManualTestCases = estimatedManualTestCases;
        this.estimatedRegressionAutomationTestCases = estimatedRegressionAutomationTestCases;
        this.estimatedSanityAutomationTestCases = estimatedSanityAutomationTestCases;
        this.project = project;
    }

    public FirstModuleEstimate(Module module) {
        super();
        this.name = module.getName();
        this.estimatedManualTestCases = module.getEstimatedManualTestCases();
        this.estimatedRegressionAutomationTestCases = module.getEstimatedRegressionAutomationTestCases();
        this.estimatedSanityAutomationTestCases = module.getEstimatedSanityAutomationTestCases();
        this.project = module.getProject();
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEstimatedManualTestcases() {
        return estimatedManualTestCases;
    }

    public void setEstimatedManualTestCases(String estimatedManualTestcases) {
        this.estimatedManualTestCases = estimatedManualTestcases;
    }


    public String getEstimatedRegressionAutomationTestCases() {
        return estimatedRegressionAutomationTestCases;
    }

    public void setEstimatedRegressionAutomationTestCases(String estimatedRegressionAutomationTestCases) {
        this.estimatedRegressionAutomationTestCases = estimatedRegressionAutomationTestCases;
    }

    public String getEstimatedSanityAutomationTestCases() {
        return estimatedSanityAutomationTestCases;
    }

    public void setEstimatedSanityAutomationTestCases(String estimatedSanityAutomationTestCases) {
        this.estimatedSanityAutomationTestCases = estimatedSanityAutomationTestCases;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

}