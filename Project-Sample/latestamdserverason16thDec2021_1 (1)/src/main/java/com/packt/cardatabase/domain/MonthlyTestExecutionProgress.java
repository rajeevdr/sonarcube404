package com.packt.cardatabase.domain;


import javax.persistence.*;

@Entity
public class MonthlyTestExecutionProgress {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private String totalNumberOfManualUATTestCasesExecuted;
    private String sITServicesInscope;
    private String sitServicesNotAvailableForTesting;
    private String totalNumberOfManualSITTestCasesExecuted;
    private String totalNumberOfDefectsLoggedInSIT;
    private String numberOfValidDefectsLoggedInSIT;
    private String totalNumberOfDefectsLoggedInUAT;
    private String numberOfValidDefectsLoggedInUAT;
    private String totalNumberOfDefectsInProduction;
    private String totalNumberOfValidAutomationDefects;
    private String totalNumberOfDefects;
    private String actualStartDateMilestone;
    private String actualEndDateMilestone;
    private int monthNumber;
    private int yearNumber;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public MonthlyTestExecutionProgress() {
    }


    public MonthlyTestExecutionProgress(String totalNumberOfManualUATTestCasesExecuted,
                                        String sITServicesInscope,
                                        String sitServicesNotAvailableForTesting,
                                        String totalNumberOfManualSITTestCasesExecuted,
                                        String totalNumberOfDefectsLoggedInSIT,
                                        String numberOfValidDefectsLoggedInSIT,
                                        String totalNumberOfDefectsLoggedInUAT,
                                        String numberOfValidDefectsLoggedInUAT,
                                        String totalNumberOfDefectsInProduction,
                                        String totalNumberOfValidAutomationDefects,
                                        String totalNumberOfDefects,
                                        int monthNumber,
                                        int yearNumber,
                                        String actualStartDateMilestone,
                                        String actualEndDateMilestone,
                                        Project project) {

        super();
        this.sitServicesNotAvailableForTesting = sitServicesNotAvailableForTesting ;
        this.totalNumberOfManualUATTestCasesExecuted = totalNumberOfManualUATTestCasesExecuted ;
        this.totalNumberOfManualSITTestCasesExecuted = totalNumberOfManualSITTestCasesExecuted ;
        this.sITServicesInscope = sITServicesInscope;
        this.totalNumberOfDefectsLoggedInSIT = totalNumberOfDefectsLoggedInSIT ;
        this.numberOfValidDefectsLoggedInSIT = numberOfValidDefectsLoggedInSIT ;
        this.totalNumberOfDefectsLoggedInUAT = totalNumberOfDefectsLoggedInUAT ;
        this.numberOfValidDefectsLoggedInUAT = numberOfValidDefectsLoggedInUAT ;
        this.totalNumberOfDefectsInProduction = totalNumberOfDefectsInProduction;
        this.totalNumberOfValidAutomationDefects = totalNumberOfValidAutomationDefects ;
        this.totalNumberOfDefects = totalNumberOfDefects ;
        this.monthNumber = monthNumber;
        this.yearNumber = yearNumber;
        this.actualStartDateMilestone = actualStartDateMilestone;
        this.actualEndDateMilestone = actualEndDateMilestone;
        this.project = project;
    }

    public int getTotalNumberOfDefectsInProduction() {

        if(totalNumberOfDefectsInProduction == null)
            return 0;
        return Integer.parseInt(totalNumberOfDefectsInProduction);

    }

    public String getTotalNumberOfDefectsInProductionName() {
        return "Total Number Of Defects In Production";
    }

    public void setTotalNumberOfDefectsInProduction(String totalNumberOfDefectsInProduction) {
        this.totalNumberOfDefectsInProduction = totalNumberOfDefectsInProduction;
    }

    public int getSITServicesInscope() {

        if(sITServicesInscope == null)
            return 0;
        return Integer.parseInt(sITServicesInscope);

    }

    public String getSITServicesInscopeName() {
        return "SIT Services InScope";
    }


    public void setSITServicesInscope(String sITServicesInscope) {
        this.sITServicesInscope = sITServicesInscope;
    }


    public int getMonthNumber() {
        return monthNumber;
    }
    public String getMonthNumberName() {
        return "Month";
    }
    public void setMonthNumber(int monthNumber) {
        this.monthNumber = monthNumber;
    }

    public int getYearNumber() {
        return yearNumber;
    }
    public String getYearNumberName() {
        return "Year";
    }
    public void setYearNumber(int yearNumber) {
        this.yearNumber = yearNumber;
    }


    public int getTotalNumberOfManualUATTestCasesExecuted() {

        if(totalNumberOfManualUATTestCasesExecuted == null)
            return 0;
        return Integer.parseInt(totalNumberOfManualUATTestCasesExecuted);

    }

    public String getTotalNumberOfManualUATTestCasesExecutedName() {
        return "Total Number Of Manual UAT TestCases Executed";
    }

    public void setTotalNumberOfManualUATTestCasesExecuted(String totalNumberOfManualUATTestCasesExecuted) {
        this.totalNumberOfManualUATTestCasesExecuted = totalNumberOfManualUATTestCasesExecuted;
    }

    public int getSITServicesNotAvailableForTesting() {

        if(sitServicesNotAvailableForTesting == null)
            return 0;
        return Integer.parseInt(sitServicesNotAvailableForTesting);

    }


    public String getSITServicesNotAvailableForTestingName() {
        return "SIT Services Not Available For Testing";
    }


    public void setSITServicesNotAvailableForTesting(String sitServicesNotAvailableForTesting) {
        this.sitServicesNotAvailableForTesting = sitServicesNotAvailableForTesting;
    }

    public int getTotalNumberOfManualSITTestCasesExecuted() {

        if(totalNumberOfManualSITTestCasesExecuted == null)
            return 0;
        return Integer.parseInt(totalNumberOfManualSITTestCasesExecuted);

    }

    public String getTotalNumberOfManualSITTestCasesExecutedName() {
        return "Total Number Of Manual SIT TestCases Executed";
    }

    public void setTotalNumberOfManualSITTestCasesExecuted(String totalNumberOfManualSITTestCasesExecuted) {
        this.totalNumberOfManualSITTestCasesExecuted = totalNumberOfManualSITTestCasesExecuted;
    }

    public int getTotalNumberOfDefectsLoggedInSIT() {

        if(totalNumberOfDefectsLoggedInSIT == null)
            return 0;
        return Integer.parseInt(totalNumberOfDefectsLoggedInSIT);

    }

    public String getTotalNumberOfDefectsLoggedInSITName() {
        return "Total Number Of Defects Logged In SIT";
    }

    public void setTotalNumberOfDefectsLoggedInSIT(String totalNumberOfDefectsLoggedInSIT) {
        this.totalNumberOfDefectsLoggedInSIT = totalNumberOfDefectsLoggedInSIT;
    }

    public int getNumberOfValidDefectsLoggedInSIT() {

        if(numberOfValidDefectsLoggedInSIT == null)
            return 0;
        return Integer.parseInt(numberOfValidDefectsLoggedInSIT);

    }

    public String getNumberOfValidDefectsLoggedInSITName() {
        return "Number Of Valid Defects Logged In SIT";
    }

    public void setNumberOfValidDefectsLoggedInSIT(String numberOfValidDefectsLoggedInSIT) {
        this.numberOfValidDefectsLoggedInSIT = numberOfValidDefectsLoggedInSIT;
    }

    public int getTotalNumberOfDefectsLoggedInUAT() {

        if(totalNumberOfDefectsLoggedInUAT == null)
            return 0;
        return Integer.parseInt(totalNumberOfDefectsLoggedInUAT);

    }

    public String getTotalNumberOfDefectsLoggedInUATName() {
        return "Total Number Of Defects Logged In UAT";
    }

    public void setTotalNumberOfDefectsLoggedInUAT(String totalNumberOfDefectsLoggedInUAT) {
        this.totalNumberOfDefectsLoggedInUAT = totalNumberOfDefectsLoggedInUAT;
    }

    public int getNumberOfValidDefectsLoggedInUAT() {

        if(numberOfValidDefectsLoggedInUAT == null)
            return 0;
        return Integer.parseInt(numberOfValidDefectsLoggedInUAT);

    }

    public String getNumberOfValidDefectsLoggedInUATName() {
        return "Number Of Valid Defects Logged In UAT";
    }

    public void setNumberOfValidDefectsLoggedInUAT(String numberOfValidDefectsLoggedInUAT) {
        this.numberOfValidDefectsLoggedInUAT = numberOfValidDefectsLoggedInUAT;
    }

    public int getTotalNumberOfValidAutomationDefects() {

        if(totalNumberOfValidAutomationDefects == null)
            return 0;
        return Integer.parseInt(totalNumberOfValidAutomationDefects);

    }

    public String getTotalNumberOfValidAutomationDefectsName() {
        return "Total Number Of Valid Automation Defects";
    }

    public void setTotalNumberOfValidAutomationDefects(String totalNumberOfValidAutomationDefects) {
        this.totalNumberOfValidAutomationDefects = totalNumberOfValidAutomationDefects;
    }

    public int getTotalNumberOfDefects() {

        if(totalNumberOfDefects == null)
            return 0;
        return Integer.parseInt(totalNumberOfDefects);

    }

    public String getTotalNumberOfDefectsName() {
        return "Total Number Of Defects";
    }

    public void setTotalNumberOfDefects(String totalNumberOfDefects) {
        this.totalNumberOfDefects = totalNumberOfDefects;
    }

    public String getActualStartDateMilestone() {
        return actualStartDateMilestone;
    }

    public String getActualStartDateMilestoneName() {
        return "Actual Start Date Milestone";
    }


    public void setActualStartDateMilestone(String actualStartDateMilestone) {
        this.actualStartDateMilestone = actualStartDateMilestone;
    }

    public String getActualEndDateMilestone() {
        return actualEndDateMilestone;
    }

    public String getActualEndDateMilestoneName() {
        return "Actual End Date Milestone";
    }

    public void setActualEndDateMilestone(String actualEndDateMilestone) {
        this.actualEndDateMilestone = actualEndDateMilestone;
    }
    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

}