package com.packt.cardatabase.web;

import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;
import com.packt.cardatabase.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

@RestController
public class TestdesignTestexecutionProgressController {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private ModuleRepository moduleRepository;

    @Autowired
    private WeeklyProjectLevelTestDesignProgressRepository weeklyProjectLevelTestDesignProgressRepository;

    @Autowired
    private WeeklyModuleLevelTestDesignProgressRepository weeklyModuleLevelTestDesignProgressRepository;

    @Autowired
    private WeekDataRepository weekDataRepository;

    @Autowired
    private WeeklyProjectLevelTestExecutionProgressRepository weeklyProjectLevelTestExecutionProgressRepository;

    @Autowired
    private WeeklyModuleLevelTestExecutionProgressRepository weeklyModuleLevelTestExecutionProgressRepository;


    @PostMapping(value = "/projects/{id}/progress")
    @ResponseBody
    public String progress(@PathVariable("id") long id) {

        try {

            Fillo fillo = new Fillo();
            ArrayList<ArrayList<String>> dataArray = new ArrayList<ArrayList<String>>();
            Connection connection = fillo.getConnection("D://uploads//testdesigntestexecution.xlsx");


            try {

                String allEnabledTestCases = "Select * from weeklytestdesignprogress";
                Recordset allEnabledTestCasesRecordset = connection.executeQuery(allEnabledTestCases);

                System.out.println("The total number of teststeps covering all the test cases are : " + allEnabledTestCasesRecordset.getCount());

                int cursor = -1;
                while (allEnabledTestCasesRecordset.next()) { // get one testcase at a time

                    try {
                        ArrayList<String> data = new ArrayList<String>();
                        dataArray.add(data);

                        while (1 == 1) {
                            cursor = cursor + 1;
                            data.add(allEnabledTestCasesRecordset.getField(cursor + 1).value());
                            System.out.println(allEnabledTestCasesRecordset.getField(cursor + 1).value() + " ");
                        }

                    } catch (Exception e) {
                        cursor = -1;
                    }


                }

                System.out.println("Data size : " + dataArray.size());

                for (int index2 = 1; index2 < dataArray.get(0).size(); index2++) {

                    int weekNumber = 0;
                    int yearNumber = 0;
                    int monthNumber = 0;

                    ArrayList<ProjectProgressPojo> progressPojoArray = new ArrayList<ProjectProgressPojo>();

                    for (int index = 0; index < dataArray.size(); index++) {
                        ArrayList<String> data = dataArray.get(index);

                        if (data.get(0).compareTo("Week") == 0) {
                            System.out.println("New Week Found : " + data.get(index2));
                            System.out.println(data.get(0) + " : " + data.get(index2));

                            weekNumber = Integer.parseInt(data.get(index2));

                        } else if (data.get(0).compareTo("YearNumber") == 0) {
                            System.out.println("New Year Found : " + data.get(index2));
                            System.out.println(data.get(0) + " : " + data.get(index2));

                            yearNumber = Integer.parseInt(data.get(index2));

                            Iterable<WeekData> weekDataIterable = weekDataRepository.findAll();
                            Iterator<WeekData> weekDataIterator = weekDataIterable.iterator();

                            WeekData nextWeek = null;

                            while (weekDataIterator.hasNext()) {
                                nextWeek = weekDataIterator.next();
                                if ((nextWeek.getWeekNumber() == weekNumber) && (nextWeek.getYearNumber() == yearNumber)) {
                                    monthNumber = nextWeek.getMonthNumber();
                                    break;
                                }
                            }


                        } else {
                            ProjectProgressPojo projectProgressPojo = new ProjectProgressPojo();

                            projectProgressPojo.setName(data.get(0));
                            projectProgressPojo.setValue(data.get(index2));
                            progressPojoArray.add(projectProgressPojo);

                        }
                    }

                    System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");


                    Iterable<WeeklyProjectLevelTestDesignProgress> weeklyProjectLevelTestDesignProgress = weeklyProjectLevelTestDesignProgressRepository.findAll();
                    Iterator<WeeklyProjectLevelTestDesignProgress> weeklyProjectLevelTestDesignProgressRepositoryIterator = weeklyProjectLevelTestDesignProgress.iterator();

                    boolean matchFound = false;
                    while (weeklyProjectLevelTestDesignProgressRepositoryIterator.hasNext()) {
                        WeeklyProjectLevelTestDesignProgress next = weeklyProjectLevelTestDesignProgressRepositoryIterator.next();

                        if ((next.getWeekNumber() == weekNumber) && (next.getProject().getId() == id) && (next.getYearNumber() == yearNumber)) {
                            matchFound = true;

                            for (int i = 0; i < progressPojoArray.size(); i++) {

                                if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfTestableRequirementsCoveredThisWeekName())) {

                                    String previousEntry = Integer.toString(next.getNumberOfTestableRequirementsCoveredThisWeek());
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setNumberOfTestableRequirementsCoveredThisWeek(currentUpdatesEntry);
                                    }

                                }
                                if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfInProjectAutomationTestCasesAutomatedName())) {

                                    String previousEntry = Integer.toString(next.getTotalNumberOfInProjectAutomationTestCasesAutomated());
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setTotalNumberOfInProjectAutomationTestCasesAutomated(currentUpdatesEntry);
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfAPIAutomationTestCasesAutomatedName())) {

                                    String previousEntry = Integer.toString(next.getTotalNumberOfAPIAutomationTestCasesAutomated());
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setTotalNumberOfAPIAutomationTestCasesAutomated(currentUpdatesEntry);
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfTestDesignReviewDefectsFoundName())) {

                                    String previousEntry = Integer.toString(next.getTotalNumberOfTestDesignReviewDefectsFound());
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setTotalNumberOfTestDesignReviewDefectsFound(currentUpdatesEntry);
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTestPlanningEffortName())) {
                                    String previousEntry = Integer.toString(next.getTestPlanningEffort());
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setTestPlanningEffort(currentUpdatesEntry);
                                        next.setTestPlanningEffortStartDate(progressPojoArray.get(i).getStartDate());
                                        next.setTestPlanningEffortEndDate(progressPojoArray.get(i).getEndDate());
                                    }


                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getManualTestCasesDesignEffortName())) {

                                    String previousEntry = Integer.toString(next.getManualTestCasesDesignEffort());
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setManualTestCasesDesignEffort(currentUpdatesEntry);
                                        next.setManualTestCasesDesignEffortStartDate(progressPojoArray.get(i).getStartDate());
                                        next.setManualTestCasesDesignEffortEndDate(progressPojoArray.get(i).getEndDate());
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getRegressionAutomationDesignEffortName())) {

                                    String previousEntry = Integer.toString(next.getRegressionAutomationDesignEffort());
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setRegressionAutomationDesignEffort(currentUpdatesEntry);
                                        next.setRegressionAutomationDesignEffortStartDate(progressPojoArray.get(i).getStartDate());
                                        next.setRegressionAutomationDesignEffortEndDate(progressPojoArray.get(i).getEndDate());
                                    }


                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getApiAutomationDesignEffortName())) {

                                    String previousEntry = Integer.toString(next.getApiAutomationDesignEffort());
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setApiAutomationDesignEffort(currentUpdatesEntry);
                                        next.setApiAutomationDesignEffortStartDate(progressPojoArray.get(i).getStartDate());
                                        next.setApiAutomationDesignEffortEndDate(progressPojoArray.get(i).getEndDate());
                                    }


                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getInProjectAutomationDesignEffortName())) {

                                    String previousEntry = Integer.toString(next.getInProjectAutomationDesignEffort());
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setInProjectAutomationDesignEffort(currentUpdatesEntry);
                                        next.setInProjectAutomationDesignEffortStartDate(progressPojoArray.get(i).getStartDate());
                                        next.setInProjectAutomationDesignEffortEndDate(progressPojoArray.get(i).getEndDate());
                                    }


                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getManualTestCasesMaintenanceEffortName())) {
                                    String previousEntry = Integer.toString(next.getManualTestCasesMaintenanceEffort());
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setManualTestCasesMaintenanceEffort(currentUpdatesEntry);
                                        next.setManualTestCasesMaintenanceEffortStartDate(progressPojoArray.get(i).getStartDate());
                                        next.setManualTestCasesMaintenanceEffortEndDate(progressPojoArray.get(i).getEndDate());
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getAutomationTestCasesMaintenanceEffortName())) {
                                    String previousEntry = Integer.toString(next.getAutomationTestCasesMaintenanceEffort());
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setAutomationTestCasesMaintenanceEffort(currentUpdatesEntry);
                                        next.setAutomationTestCasesMaintenanceEffortStartDate(progressPojoArray.get(i).getStartDate());
                                        next.setAutomationTestCasesMaintenanceEffortEndDate(progressPojoArray.get(i).getEndDate());
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getEnvironmentDowntimeDuringTestDesignPhaseName())) {

                                    String previousEntry = Integer.toString(next.getEnvironmentDowntimeDuringTestDesignPhase());
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setEnvironmentDowntimeDuringTestDesignPhase(currentUpdatesEntry);
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfResourcesAuthoringManualTestCasesName())) {

                                    String previousEntry = Integer.toString(next.getNumberOfResourcesAuthoringManualTestCases());
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setNumberOfResourcesAuthoringManualTestCases(currentUpdatesEntry);
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfResourcesAuthoringAutomationTestCasesName())) {
                                    String previousEntry = Integer.toString(next.getNumberOfResourcesAuthoringAutomationTestCases());
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setNumberOfResourcesAuthoringAutomationTestCases(currentUpdatesEntry);
                                    }

                                }
                            }

                            next.setWeekNumber(weekNumber);
                            next.setMonthNumber(monthNumber);
                            next.setYearNumber(yearNumber);

                            weeklyProjectLevelTestDesignProgressRepository.save(next);
                            break;
                        }
                    }

                    if (matchFound == false) {
                        WeeklyProjectLevelTestDesignProgress progress = null;

                        Iterable<Project> allProjects = projectRepository.findAll();
                        Iterator<Project> iterator = allProjects.iterator();

                        while (iterator.hasNext()) {
                            Project nextProject = iterator.next();

                            if (nextProject.getId() == id) {
                                progress = new WeeklyProjectLevelTestDesignProgress("0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", weekNumber, monthNumber, yearNumber, nextProject);
                                break;
                            }
                        }

                        for (int i = 0; i < progressPojoArray.size(); i++) {

                            if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getNumberOfTestableRequirementsCoveredThisWeekName())) {

                                String previousEntry = "";
                                String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                if (currentUpdatesEntry != previousEntry) {
                                    progress.setNumberOfTestableRequirementsCoveredThisWeek(currentUpdatesEntry);
                                }

                            } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getTotalNumberOfInProjectAutomationTestCasesAutomatedName())) {

                                String previousEntry = "";
                                String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                if (currentUpdatesEntry != previousEntry) {
                                    progress.setTotalNumberOfInProjectAutomationTestCasesAutomated(currentUpdatesEntry);
                                }

                            } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getTotalNumberOfAPIAutomationTestCasesAutomatedName())) {

                                String previousEntry = "";
                                String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                if (currentUpdatesEntry != previousEntry) {
                                    progress.setTotalNumberOfAPIAutomationTestCasesAutomated(currentUpdatesEntry);
                                }

                            } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getTotalNumberOfTestDesignReviewDefectsFoundName())) {

                                String previousEntry = "";
                                String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                if (currentUpdatesEntry != previousEntry) {
                                    progress.setTotalNumberOfTestDesignReviewDefectsFound(currentUpdatesEntry);
                                }

                            } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getTestPlanningEffortName())) {
                                String previousEntry = "";
                                String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                if (currentUpdatesEntry != previousEntry) {
                                    progress.setTestPlanningEffort(currentUpdatesEntry);
                                    progress.setTestPlanningEffortStartDate(progressPojoArray.get(i).getStartDate());
                                    progress.setTestPlanningEffortEndDate(progressPojoArray.get(i).getEndDate());
                                }


                            } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getManualTestCasesDesignEffortName())) {

                                String previousEntry = "";
                                String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                if (currentUpdatesEntry != previousEntry) {
                                    progress.setManualTestCasesDesignEffort(currentUpdatesEntry);
                                    progress.setManualTestCasesDesignEffortStartDate(progressPojoArray.get(i).getStartDate());
                                    progress.setManualTestCasesDesignEffortEndDate(progressPojoArray.get(i).getEndDate());
                                }

                            } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getRegressionAutomationDesignEffortName())) {

                                String previousEntry = "";
                                String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                if (currentUpdatesEntry != previousEntry) {
                                    progress.setRegressionAutomationDesignEffort(currentUpdatesEntry);
                                    progress.setRegressionAutomationDesignEffortStartDate(progressPojoArray.get(i).getStartDate());
                                    progress.setRegressionAutomationDesignEffortEndDate(progressPojoArray.get(i).getEndDate());
                                }


                            } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getApiAutomationDesignEffortName())) {
                                String previousEntry = "";
                                String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                if (currentUpdatesEntry != previousEntry) {
                                    progress.setApiAutomationDesignEffort(currentUpdatesEntry);
                                    progress.setApiAutomationDesignEffortStartDate(progressPojoArray.get(i).getStartDate());
                                    progress.setApiAutomationDesignEffortEndDate(progressPojoArray.get(i).getEndDate());
                                }

                            } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getInProjectAutomationDesignEffortName())) {
                                String previousEntry = "";
                                String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                if (currentUpdatesEntry != previousEntry) {
                                    progress.setInProjectAutomationDesignEffort(currentUpdatesEntry);
                                    progress.setInProjectAutomationDesignEffortStartDate(progressPojoArray.get(i).getStartDate());
                                    progress.setInProjectAutomationDesignEffortEndDate(progressPojoArray.get(i).getEndDate());
                                }

                            } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getManualTestCasesMaintenanceEffortName())) {
                                String previousEntry = "";
                                String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                if (currentUpdatesEntry != previousEntry) {
                                    progress.setManualTestCasesMaintenanceEffort(currentUpdatesEntry);
                                    progress.setManualTestCasesMaintenanceEffortStartDate(progressPojoArray.get(i).getStartDate());
                                    progress.setManualTestCasesMaintenanceEffortEndDate(progressPojoArray.get(i).getEndDate());
                                }

                            } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getAutomationTestCasesMaintenanceEffortName())) {
                                String previousEntry = "";
                                String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                if (currentUpdatesEntry != previousEntry) {
                                    progress.setAutomationTestCasesMaintenanceEffort(currentUpdatesEntry);
                                    progress.setAutomationTestCasesMaintenanceEffortStartDate(progressPojoArray.get(i).getStartDate());
                                    progress.setAutomationTestCasesMaintenanceEffortEndDate(progressPojoArray.get(i).getEndDate());
                                }

                            } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getEnvironmentDowntimeDuringTestDesignPhaseName())) {

                                String previousEntry = "";
                                String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                if (currentUpdatesEntry != previousEntry) {
                                    progress.setEnvironmentDowntimeDuringTestDesignPhase(currentUpdatesEntry);
                                }

                            } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getNumberOfResourcesAuthoringManualTestCasesName())) {

                                String previousEntry = "";
                                String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                if (currentUpdatesEntry != previousEntry) {
                                    progress.setNumberOfResourcesAuthoringManualTestCases(currentUpdatesEntry);
                                }

                            } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getNumberOfResourcesAuthoringAutomationTestCasesName())) {
                                String previousEntry = "";
                                String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                if (currentUpdatesEntry != previousEntry) {
                                    progress.setNumberOfResourcesAuthoringAutomationTestCases(currentUpdatesEntry);
                                }

                            }


                        }

                        progress.setWeekNumber(weekNumber);
                        progress.setMonthNumber(monthNumber);
                        progress.setYearNumber(yearNumber);

                        weeklyProjectLevelTestDesignProgressRepository.save(progress);
                    }


                    for (int i = 0; i < progressPojoArray.size(); i++) {

                        WeeklyModuleLevelTestDesignProgress tempModuleProgress = new WeeklyModuleLevelTestDesignProgress();

                        if (progressPojoArray.get(i).getName().contains(tempModuleProgress.getNumberOfManualTestCasesDesignedForTheWeekName())) {
                            WeeklyModuleLevelTestDesignProgress moduleProgress = null;

                            StringTokenizer tokenizer = new StringTokenizer(progressPojoArray.get(i).getName(), "||");
                            String moduleName = tokenizer.nextToken();

                            boolean moduleFound = false;

                            Iterable<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgressIterable = weeklyModuleLevelTestDesignProgressRepository.findAll();
                            Iterator<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgressIterator = weeklyModuleLevelTestDesignProgressIterable.iterator();

                            while (weeklyModuleLevelTestDesignProgressIterator.hasNext()) {

                                moduleProgress = weeklyModuleLevelTestDesignProgressIterator.next();

                                if ((moduleProgress.getProject().getId() == id) &&
                                        (moduleProgress.getWeekNumber() == weekNumber) &&
                                        (moduleProgress.getModuleName().compareTo(moduleName) == 0) &&
                                        (moduleProgress.getYearNumber() == yearNumber)) {
                                    moduleFound = true;
                                    break;
                                }

                            }

                            if (moduleFound == false) {
                                moduleProgress = new WeeklyModuleLevelTestDesignProgress();

                                moduleProgress.setModuleName(moduleName);
                                moduleProgress.setWeekNumber(weekNumber);
                                moduleProgress.setMonthNumber(monthNumber);
                                moduleProgress.setYearNumber(yearNumber);

                                Iterable<Project> allProjects = projectRepository.findAll();
                                Iterator<Project> iterator = allProjects.iterator();


                                while (iterator.hasNext()) {
                                    Project nextProject = iterator.next();
                                    if (nextProject.getId() == id) {
                                        moduleProgress.setProject(nextProject);
                                        break;
                                    }
                                }
                                moduleProgress.setNumberOfManualTestCasesDesignedForTheWeek(progressPojoArray.get(i).getValue());
                                //moduleProgress.setAutomationRegressionCasesDesignedForTheWeek(0);
                                weeklyModuleLevelTestDesignProgressRepository.save(moduleProgress);

                            } else {
                                moduleProgress.setModuleName(moduleName);
                                moduleProgress.setWeekNumber(weekNumber);
                                moduleProgress.setMonthNumber(monthNumber);
                                moduleProgress.setYearNumber(yearNumber);
                                moduleProgress.setNumberOfManualTestCasesDesignedForTheWeek(progressPojoArray.get(i).getValue());
                                weeklyModuleLevelTestDesignProgressRepository.save(moduleProgress);
                            }
                        } else if (progressPojoArray.get(i).getName().contains(tempModuleProgress.getNumberOfRegressionAutomationTestScriptsDesignedForTheWeekName())) {
                            WeeklyModuleLevelTestDesignProgress moduleProgress = null;

                            StringTokenizer tokenizer = new StringTokenizer(progressPojoArray.get(i).getName(), "||");
                            String moduleName = tokenizer.nextToken();

                            boolean moduleFound = false;

                            Iterable<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgressIterable = weeklyModuleLevelTestDesignProgressRepository.findAll();
                            Iterator<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgressIterator = weeklyModuleLevelTestDesignProgressIterable.iterator();

                            while (weeklyModuleLevelTestDesignProgressIterator.hasNext()) {

                                moduleProgress = weeklyModuleLevelTestDesignProgressIterator.next();

                                if ((moduleProgress.getProject().getId() == id) &&
                                        (moduleProgress.getWeekNumber() == weekNumber) &&
                                        (moduleProgress.getModuleName().compareTo(moduleName) == 0) &&
                                        (moduleProgress.getYearNumber() == yearNumber)) {
                                    moduleFound = true;
                                    break;
                                }

                            }

                            if (moduleFound == false) {
                                moduleProgress = new WeeklyModuleLevelTestDesignProgress();

                                moduleProgress.setModuleName(moduleName);
                                moduleProgress.setWeekNumber(weekNumber);
                                moduleProgress.setMonthNumber(monthNumber);
                                moduleProgress.setYearNumber(yearNumber);

                                Iterable<Project> allProjects = projectRepository.findAll();
                                Iterator<Project> iterator = allProjects.iterator();


                                while (iterator.hasNext()) {
                                    Project nextProject = iterator.next();
                                    if (nextProject.getId() == id) {
                                        moduleProgress.setProject(nextProject);
                                        break;
                                    }
                                }

                                moduleProgress.setNumberOfRegressionAutomationTestScriptsDesignedForTheWeek(progressPojoArray.get(i).getValue());
                                //moduleProgress.setNumberOfManualTestCasesDesignedForTheWeek(0);
                                weeklyModuleLevelTestDesignProgressRepository.save(moduleProgress);


                            } else {
                                moduleProgress.setModuleName(moduleName);
                                moduleProgress.setWeekNumber(weekNumber);
                                moduleProgress.setMonthNumber(monthNumber);
                                moduleProgress.setYearNumber(yearNumber);
                                moduleProgress.setNumberOfRegressionAutomationTestScriptsDesignedForTheWeek(progressPojoArray.get(i).getValue());
                                weeklyModuleLevelTestDesignProgressRepository.save(moduleProgress);
                            }
                        } else if (progressPojoArray.get(i).getName().contains(tempModuleProgress.getNumberOfSanityTestsDesignedForTheWeekName())) {
                            WeeklyModuleLevelTestDesignProgress moduleProgress = null;

                            StringTokenizer tokenizer = new StringTokenizer(progressPojoArray.get(i).getName(), "||");
                            String moduleName = tokenizer.nextToken();

                            boolean moduleFound = false;

                            Iterable<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgressIterable = weeklyModuleLevelTestDesignProgressRepository.findAll();
                            Iterator<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgressIterator = weeklyModuleLevelTestDesignProgressIterable.iterator();

                            while (weeklyModuleLevelTestDesignProgressIterator.hasNext()) {

                                moduleProgress = weeklyModuleLevelTestDesignProgressIterator.next();

                                if ((moduleProgress.getProject().getId() == id) &&
                                        (moduleProgress.getWeekNumber() == weekNumber) &&
                                        (moduleProgress.getModuleName().compareTo(moduleName) == 0) &&
                                        (moduleProgress.getYearNumber() == yearNumber)) {
                                    moduleFound = true;
                                    break;
                                }

                            }

                            if (moduleFound == false) {
                                moduleProgress = new WeeklyModuleLevelTestDesignProgress();

                                moduleProgress.setModuleName(moduleName);
                                moduleProgress.setWeekNumber(weekNumber);
                                moduleProgress.setMonthNumber(monthNumber);
                                moduleProgress.setYearNumber(yearNumber);

                                Iterable<Project> allProjects = projectRepository.findAll();
                                Iterator<Project> iterator = allProjects.iterator();


                                while (iterator.hasNext()) {
                                    Project nextProject = iterator.next();
                                    if (nextProject.getId() == id) {
                                        moduleProgress.setProject(nextProject);
                                        break;
                                    }
                                }

                                moduleProgress.setNumberOfSanityTestsDesignedForTheWeek(progressPojoArray.get(i).getValue());
                                //moduleProgress.setNumberOfManualTestCasesDesignedForTheWeek(0);
                                weeklyModuleLevelTestDesignProgressRepository.save(moduleProgress);


                            } else {
                                moduleProgress.setModuleName(moduleName);
                                moduleProgress.setWeekNumber(weekNumber);
                                moduleProgress.setMonthNumber(monthNumber);
                                moduleProgress.setYearNumber(yearNumber);
                                moduleProgress.setNumberOfSanityTestsDesignedForTheWeek(progressPojoArray.get(i).getValue());
                                weeklyModuleLevelTestDesignProgressRepository.save(moduleProgress);
                            }
                        }
                    }


                }

            } catch (Exception e) {

            }


            try {

                String allEnabledTestCases = "Select * from weeklytestexecutionprogress";
                Recordset allEnabledTestCasesRecordset = connection.executeQuery(allEnabledTestCases);

                int cursor = -1;
                while (allEnabledTestCasesRecordset.next()) { // get one testcase at a time

                    try {
                        ArrayList<String> data = new ArrayList<String>();
                        dataArray.add(data);

                        while (1 == 1) {
                            cursor = cursor + 1;
                            data.add(allEnabledTestCasesRecordset.getField(cursor + 1).value());
                            System.out.println(allEnabledTestCasesRecordset.getField(cursor + 1).value() + " ");
                        }

                    } catch (Exception e) {
                        cursor = -1;
                    }


                }

                System.out.println("Data size : " + dataArray.size());

                for (int index2 = 1; index2 < dataArray.get(0).size(); index2++) {

                    int weekNumber = 0;
                    int yearNumber = 0;
                    int monthNumber = 0;

                    ArrayList<ProjectProgressPojo> progressPojoArray = new ArrayList<ProjectProgressPojo>();

                    for (int index = 0; index < dataArray.size(); index++) {
                        ArrayList<String> data = dataArray.get(index);

                        if (data.get(0).compareTo("Week") == 0) {
                            System.out.println("New Week Found : " + data.get(index2));
                            System.out.println(data.get(0) + " : " + data.get(index2));

                            weekNumber = Integer.parseInt(data.get(index2));

                        } else if (data.get(0).compareTo("YearNumber") == 0) {
                            System.out.println("New Year Found : " + data.get(index2));
                            System.out.println(data.get(0) + " : " + data.get(index2));

                            yearNumber = Integer.parseInt(data.get(index2));

                            Iterable<WeekData> weekDataIterable = weekDataRepository.findAll();
                            Iterator<WeekData> weekDataIterator = weekDataIterable.iterator();

                            WeekData nextWeek = null;

                            while (weekDataIterator.hasNext()) {
                                nextWeek = weekDataIterator.next();
                                if ((nextWeek.getWeekNumber() == weekNumber) && (nextWeek.getYearNumber() == yearNumber)) {
                                    monthNumber = nextWeek.getMonthNumber();
                                    break;
                                }
                            }


                        } else {
                            ProjectProgressPojo projectProgressPojo = new ProjectProgressPojo();

                            projectProgressPojo.setName(data.get(0));
                            projectProgressPojo.setValue(data.get(index2));
                            progressPojoArray.add(projectProgressPojo);

                        }
                    }

                    System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

                    Iterable<WeeklyProjectLevelTestExecutionProgress> weeklyProjectLevelTestExecutionProgress = weeklyProjectLevelTestExecutionProgressRepository.findAll();
                    Iterator<WeeklyProjectLevelTestExecutionProgress> weeklyProjectLevelTestExecutionProgressRepositoryIterator = weeklyProjectLevelTestExecutionProgress.iterator();
                    boolean matchFound = false;
                    while (weeklyProjectLevelTestExecutionProgressRepositoryIterator.hasNext()) {

                        WeeklyProjectLevelTestExecutionProgress next = weeklyProjectLevelTestExecutionProgressRepositoryIterator.next();
                        if ((next.getWeekNumber() == weekNumber) && (next.getProject().getId() == id) && (next.getYearNumber() == yearNumber)) {
                            matchFound = true;

                            for (int i = 0; i < progressPojoArray.size(); i++) {

                                try {

                                    if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfInProjectAutomationTestScriptsExecutedName())) {

                                        String previousEntry = Integer.toString(next.getNumberOfInProjectAutomationTestScriptsExecuted());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setNumberOfInProjectAutomationTestScriptsExecuted(currentUpdatesEntry);
                                        }


                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfAPIAutomationTestScriptsExecutedName())) {

                                        String previousEntry = Integer.toString(next.getNumberOfAPIAutomationTestScriptsExecuted());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setNumberOfAPIAutomationTestScriptsExecuted(currentUpdatesEntry);
                                        }


                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfTestCasesExecutedWithMigratedDataName())) {

                                        String previousEntry = Integer.toString(next.getNumberOfTestCasesExecutedWithMigratedData());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setNumberOfTestCasesExecutedWithMigratedData(currentUpdatesEntry);
                                        }


                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfModulesNotDeliveredName())) {

                                        String previousEntry = Integer.toString(next.getNumberOfModulesNotDelivered());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setNumberOfModulesNotDelivered(currentUpdatesEntry);
                                        }

                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfManualTestCasesNotExecutedName())) {

                                        String previousEntry = Integer.toString(next.getTotalNumberOfManualTestCasesNotExecuted());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setTotalNumberOfManualTestCasesNotExecuted(currentUpdatesEntry);
                                        }

                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfAutomationTestCasesNotExecutedName())) {

                                        String previousEntry = Integer.toString(next.getTotalNumberOfAutomationTestCasesNotExecuted());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setTotalNumberOfAutomationTestCasesNotExecuted(currentUpdatesEntry);
                                        }

                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfTestCasesSuccessfullyExecutedWithoutDefectsName())) {

                                        String previousEntry = Integer.toString(next.getNumberOfTestCasesSuccessfullyExecutedWithoutDefects());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setNumberOfTestCasesSuccessfullyExecutedWithoutDefects(currentUpdatesEntry);
                                        }

                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfDefectsReportedByTestingTeamName())) {

                                        String previousEntry = Integer.toString(next.getNumberOfDefectsReportedByTestingTeam());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setNumberOfDefectsReportedByTestingTeam(currentUpdatesEntry);
                                        }

                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfDefectsRejectedName())) {

                                        String previousEntry = Integer.toString(next.getNumberOfDefectsRejected());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setNumberOfDefectsRejected(currentUpdatesEntry);
                                        }

                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfValidDefectsLoggedInSITName())) {

                                        String previousEntry = Integer.toString(next.getNumberOfValidDefectsLoggedInSIT());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setNumberOfValidDefectsLoggedInSIT(currentUpdatesEntry);
                                        }

                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfValidDefectsLoggedInUATName())) {

                                        String previousEntry = Integer.toString(next.getNumberOfValidDefectsLoggedInUAT());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setNumberOfValidDefectsLoggedInUAT(currentUpdatesEntry);
                                        }

                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfDefectsFixedName())) {

                                        String previousEntry = Integer.toString(next.getTotalNumberOfDefectsFixed());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setTotalNumberOfDefectsFixed(currentUpdatesEntry);
                                        }

                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfDefectsReopenedName())) {

                                        String previousEntry = Integer.toString(next.getTotalNumberOfDefectsReopened());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setTotalNumberOfDefectsReopened(currentUpdatesEntry);
                                        }

                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getAverageTimeTakenToResolveSev1DefectsName())) {

                                        String previousEntry = Integer.toString(next.getAverageTimeTakenToResolveSev1Defects());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setAverageTimTakenToResolveSev1Defects(currentUpdatesEntry);
                                        }

                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getEnvironmentDowntimeDuringTestExecutionPerPersonInHoursName())) {

                                        String previousEntry = Integer.toString(next.getEnvironmentDowntimeDuringTestExecutionPerPersonInHours());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setEnvironmentDowntimeDuringTestExecutionPerPersonInHours(currentUpdatesEntry);
                                        }

                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getManualTestCaseExecutionEffortName())) {

                                        String previousEntry = Integer.toString(next.getManualTestCaseExecutionEffort());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setManualTestCaseExecutionEffort(currentUpdatesEntry);
                                            next.setManualTestCaseExecutionEffortStartDate(progressPojoArray.get(i).getStartDate());
                                            next.setManualTestCaseExecutionEffortEndDate(progressPojoArray.get(i).getEndDate());
                                        }

                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getRegressionAutomationTestCaseExecutionEffortName())) {

                                        String previousEntry = Integer.toString(next.getRegressionAutomationTestCaseExecutionEffort());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setRegressionAutomationTestCaseExecutionEffort(currentUpdatesEntry);
                                            next.setRegressionAutomationTestCaseExecutionEffortStartDate(progressPojoArray.get(i).getStartDate());
                                            next.setRegressionAutomationTestCaseExecutionEffortEndDate(progressPojoArray.get(i).getEndDate());
                                        }

                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getInProjectAutomationTestCaseExecutionEffortName())) {

                                        String previousEntry = Integer.toString(next.getInProjectAutomationTestCaseExecutionEffort());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setInProjectAutomationTestCaseExecutionEffort(currentUpdatesEntry);
                                            next.setInProjectAutomationTestCaseExecutionEffortStartDate(progressPojoArray.get(i).getStartDate());
                                            next.setInProjectAutomationTestCaseExecutionEffortEndDate(progressPojoArray.get(i).getEndDate());
                                        }

                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getAPIAutomationTestCaseExecutionEffortName())) {

                                        String previousEntry = Integer.toString(next.getAPIAutomationTestCaseExecutionEffort());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setAPIAutomationTestCaseExecutionEffort(currentUpdatesEntry);
                                            next.setApiAutomationTestCaseExecutionEffortStartDate(progressPojoArray.get(i).getStartDate());
                                            next.setApiAutomationTestCaseExecutionEffortEndDate(progressPojoArray.get(i).getEndDate());
                                        }

                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfResourcesForManualTestCaseExecutionName())) {

                                        String previousEntry = Integer.toString(next.getNumberOfResourcesForManualTestCaseExecution());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setNumberOfResourcesForManualTestCaseExecution(currentUpdatesEntry);
                                        }

                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfResourcesForManualTestCaseExecutionName())) {

                                        String previousEntry = Integer.toString(next.getNumberOfResourcesForManualTestCaseExecution());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setNumberOfResourcesForManualTestCaseExecution(currentUpdatesEntry);
                                        }

                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfResourceForRegressionAutomationExecutionName())) {

                                        String previousEntry = Integer.toString(next.getNumberOfResourceForRegressionAutomationExecution());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setNumberOfResourceForRegressionAutomationExecution(currentUpdatesEntry);
                                        }

                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfResourceForInProjectAutomationExecutionName())) {

                                        String previousEntry = Integer.toString(next.getNumberOfResourceForInProjectAutomationExecution());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setNumberOfResourceForInProjectAutomationExecution(currentUpdatesEntry);
                                        }

                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfResourceForAPIAutomationExecutionName())) {

                                        String previousEntry = Integer.toString(next.getNumberOfResourceForAPIAutomationExecution());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setNumberOfResourceForAPIAutomationExecution(currentUpdatesEntry);
                                        }

                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfDefectsInProductionName())) {

                                        String previousEntry = Integer.toString(next.getTotalNumberOfDefectsInProduction());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setTotalNumberOfDefectsInProduction(currentUpdatesEntry);
                                        }

                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfDefectsFoundUsingManualRegressionName())) {

                                        String previousEntry = Integer.toString(next.getNumberOfDefectsFoundUsingManualRegression());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setNumberOfDefectsFoundUsingManualRegression(currentUpdatesEntry);
                                        }

                                    } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfDefectsFoundUsingAutomatedRegressionName())) {

                                        String previousEntry = Integer.toString(next.getNumberOfDefectsFoundUsingAutomatedRegression());
                                        String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                        if (currentUpdatesEntry != previousEntry) {
                                            next.setNumberOfDefectsFoundUsingAutomatedRegression(currentUpdatesEntry);
                                        }

                                    }
                                } catch (Exception e) {
                                    System.out.println("The exception message for Test execution update :" + e.getMessage());
                                }
                            }

                            next.setWeekNumber(weekNumber);
                            next.setMonthNumber(monthNumber);
                            next.setYearNumber(yearNumber);

                            weeklyProjectLevelTestExecutionProgressRepository.save(next);
                            break;
                        }
                    }


                    if (matchFound == false) {


                        WeeklyProjectLevelTestExecutionProgress next = null;

                        Iterable<Project> allProjects = projectRepository.findAll();
                        Iterator<Project> iterator = allProjects.iterator();

                        while (iterator.hasNext()) {
                            Project nextProject = iterator.next();

                            if (nextProject.getId() == id) {
                                next = new WeeklyProjectLevelTestExecutionProgress("0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", weekNumber, monthNumber, yearNumber, nextProject);
                                break;
                            }
                        }

                        for (int i = 0; i < progressPojoArray.size(); i++) {

                            try {
                                if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfInProjectAutomationTestScriptsExecutedName())) {

                                    String previousEntry = "";
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setNumberOfInProjectAutomationTestScriptsExecuted(currentUpdatesEntry);
                                    }


                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfAPIAutomationTestScriptsExecutedName())) {

                                    String previousEntry = "";
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setNumberOfAPIAutomationTestScriptsExecuted(currentUpdatesEntry);
                                    }


                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfTestCasesExecutedWithMigratedDataName())) {

                                    String previousEntry = "";
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setNumberOfTestCasesExecutedWithMigratedData(currentUpdatesEntry);
                                    }


                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfModulesNotDeliveredName())) {

                                    String previousEntry = "";
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setNumberOfModulesNotDelivered(currentUpdatesEntry);
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfManualTestCasesNotExecutedName())) {

                                    String previousEntry = "";
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setTotalNumberOfManualTestCasesNotExecuted(currentUpdatesEntry);
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfAutomationTestCasesNotExecutedName())) {

                                    String previousEntry = "";
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setTotalNumberOfAutomationTestCasesNotExecuted(currentUpdatesEntry);
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfTestCasesSuccessfullyExecutedWithoutDefectsName())) {

                                    String previousEntry = "";
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setNumberOfTestCasesSuccessfullyExecutedWithoutDefects(currentUpdatesEntry);
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfDefectsReportedByTestingTeamName())) {

                                    String previousEntry = "";
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setNumberOfDefectsReportedByTestingTeam(currentUpdatesEntry);
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfDefectsRejectedName())) {

                                    String previousEntry = "";
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setNumberOfDefectsRejected(currentUpdatesEntry);
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfValidDefectsLoggedInSITName())) {

                                    String previousEntry = "";
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setNumberOfValidDefectsLoggedInSIT(currentUpdatesEntry);
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfValidDefectsLoggedInUATName())) {

                                    String previousEntry = "";
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setNumberOfValidDefectsLoggedInUAT(currentUpdatesEntry);
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfDefectsFixedName())) {

                                    String previousEntry = "";
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setTotalNumberOfDefectsFixed(currentUpdatesEntry);
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfDefectsReopenedName())) {

                                    String previousEntry = "";
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setTotalNumberOfDefectsReopened(currentUpdatesEntry);
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getAverageTimeTakenToResolveSev1DefectsName())) {

                                    String previousEntry = "";
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setAverageTimTakenToResolveSev1Defects(currentUpdatesEntry);
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getEnvironmentDowntimeDuringTestExecutionPerPersonInHoursName())) {

                                    String previousEntry = "";
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setEnvironmentDowntimeDuringTestExecutionPerPersonInHours(currentUpdatesEntry);
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getManualTestCaseExecutionEffortName())) {

                                    String previousEntry = "";
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setManualTestCaseExecutionEffort(currentUpdatesEntry);
                                        next.setManualTestCaseExecutionEffortStartDate(progressPojoArray.get(i).getStartDate());
                                        next.setManualTestCaseExecutionEffortEndDate(progressPojoArray.get(i).getEndDate());
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getRegressionAutomationTestCaseExecutionEffortName())) {

                                    String previousEntry = "";
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setRegressionAutomationTestCaseExecutionEffort(currentUpdatesEntry);
                                        next.setRegressionAutomationTestCaseExecutionEffortStartDate(progressPojoArray.get(i).getStartDate());
                                        next.setRegressionAutomationTestCaseExecutionEffortEndDate(progressPojoArray.get(i).getEndDate());
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getInProjectAutomationTestCaseExecutionEffortName())) {

                                    String previousEntry = "";
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setInProjectAutomationTestCaseExecutionEffort(currentUpdatesEntry);
                                        next.setInProjectAutomationTestCaseExecutionEffortStartDate(progressPojoArray.get(i).getStartDate());
                                        next.setInProjectAutomationTestCaseExecutionEffortEndDate(progressPojoArray.get(i).getEndDate());
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getAPIAutomationTestCaseExecutionEffortName())) {

                                    String previousEntry = "";
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setAPIAutomationTestCaseExecutionEffort(currentUpdatesEntry);
                                        next.setApiAutomationTestCaseExecutionEffortStartDate(progressPojoArray.get(i).getStartDate());
                                        next.setApiAutomationTestCaseExecutionEffortEndDate(progressPojoArray.get(i).getEndDate());

                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfResourcesForManualTestCaseExecutionName())) {

                                    String previousEntry = "";
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setNumberOfResourcesForManualTestCaseExecution(currentUpdatesEntry);
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfResourcesForManualTestCaseExecutionName())) {

                                    String previousEntry = "";
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setNumberOfResourcesForManualTestCaseExecution(currentUpdatesEntry);
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfResourceForRegressionAutomationExecutionName())) {

                                    String previousEntry = Integer.toString(next.getNumberOfResourceForRegressionAutomationExecution());
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setNumberOfResourceForRegressionAutomationExecution(currentUpdatesEntry);
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfResourceForAPIAutomationExecutionName())) {

                                    String previousEntry = Integer.toString(next.getNumberOfResourceForAPIAutomationExecution());
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setNumberOfResourceForAPIAutomationExecution(currentUpdatesEntry);
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfResourceForInProjectAutomationExecutionName())) {

                                    String previousEntry = Integer.toString(next.getNumberOfResourceForInProjectAutomationExecution());
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setNumberOfResourceForInProjectAutomationExecution(currentUpdatesEntry);
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfDefectsInProductionName())) {

                                    String previousEntry = "";
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setTotalNumberOfDefectsInProduction(currentUpdatesEntry);
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfDefectsFoundUsingManualRegressionName())) {

                                    String previousEntry = "";
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setNumberOfDefectsFoundUsingManualRegression(currentUpdatesEntry);
                                    }

                                } else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfDefectsFoundUsingAutomatedRegressionName())) {

                                    String previousEntry = "";
                                    String currentUpdatesEntry = progressPojoArray.get(i).getValue();

                                    if (currentUpdatesEntry != previousEntry) {
                                        next.setNumberOfDefectsFoundUsingAutomatedRegression(currentUpdatesEntry);
                                    }

                                }
                            } catch (Exception e) {
                                System.out.println("The exception message for Test execution update :" + e.getMessage());
                            }
                        }
                        next.setWeekNumber(weekNumber);
                        next.setMonthNumber(monthNumber);
                        next.setYearNumber(yearNumber);

                        weeklyProjectLevelTestExecutionProgressRepository.save(next);
                    }


                    for (int i = 0; i < progressPojoArray.size(); i++) {
                        WeeklyModuleLevelTestExecutionProgress moduleProgress = new WeeklyModuleLevelTestExecutionProgress();


                        if (progressPojoArray.get(i).getName().contains(moduleProgress.getNumberOfManualTestCasesExecutedForTheWeekName())) {
                            StringTokenizer tokenizer = new StringTokenizer(progressPojoArray.get(i).getName(), "||");
                            String moduleName = tokenizer.nextToken();

                            boolean moduleFound = false;

                            Iterable<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgressIterable = weeklyModuleLevelTestExecutionProgressRepository.findAll();
                            Iterator<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgressIterator = weeklyModuleLevelTestExecutionProgressIterable.iterator();

                            while (weeklyModuleLevelTestExecutionProgressIterator.hasNext()) {

                                moduleProgress = weeklyModuleLevelTestExecutionProgressIterator.next();

                                if ((moduleProgress.getProject().getId() == id) &&
                                        (moduleProgress.getWeekNumber() == weekNumber) &&
                                        (moduleProgress.getModuleName().compareTo(moduleName) == 0) &&
                                        (moduleProgress.getYearNumber() == yearNumber)) {
                                    moduleFound = true;
                                    break;
                                }

                            }

                            if (moduleFound == false) {
                                moduleProgress = new WeeklyModuleLevelTestExecutionProgress();
                                moduleProgress.setModuleName(moduleName);
                                moduleProgress.setWeekNumber(weekNumber);
                                moduleProgress.setMonthNumber(monthNumber);
                                moduleProgress.setYearNumber(yearNumber);

                                Iterable<Project> allProjects = projectRepository.findAll();
                                Iterator<Project> iterator = allProjects.iterator();


                                while (iterator.hasNext()) {
                                    Project nextProject = iterator.next();
                                    if (nextProject.getId() == id) {
                                        moduleProgress.setProject(nextProject);
                                        break;
                                    }
                                }
                            }


                            moduleProgress.setNumberOfManualTestCasesExecutedForTheWeek(progressPojoArray.get(i).getValue());
                            weeklyModuleLevelTestExecutionProgressRepository.save(moduleProgress);


                        } else if (progressPojoArray.get(i).getName().contains(moduleProgress.getNumberOfRegressionAutomationTestCasesExecutedForTheWeekName())) {
                            StringTokenizer tokenizer = new StringTokenizer(progressPojoArray.get(i).getName(), "||");
                            String moduleName = tokenizer.nextToken();

                            boolean moduleFound = false;

                            Iterable<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgressIterable = weeklyModuleLevelTestExecutionProgressRepository.findAll();
                            Iterator<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgressIterator = weeklyModuleLevelTestExecutionProgressIterable.iterator();

                            while (weeklyModuleLevelTestExecutionProgressIterator.hasNext()) {

                                moduleProgress = weeklyModuleLevelTestExecutionProgressIterator.next();

                                if ((moduleProgress.getProject().getId() == id) &&
                                        (moduleProgress.getWeekNumber() == weekNumber) &&
                                        (moduleProgress.getModuleName().compareTo(moduleName) == 0) &&
                                        (moduleProgress.getYearNumber() == yearNumber)) {
                                    moduleFound = true;
                                    break;
                                }

                            }

                            if (moduleFound == false) {
                                moduleProgress = new WeeklyModuleLevelTestExecutionProgress();
                                moduleProgress.setModuleName(moduleName);
                                moduleProgress.setWeekNumber(weekNumber);
                                moduleProgress.setMonthNumber(monthNumber);
                                moduleProgress.setYearNumber(yearNumber);

                                Iterable<Project> allProjects = projectRepository.findAll();
                                Iterator<Project> iterator = allProjects.iterator();


                                while (iterator.hasNext()) {
                                    Project nextProject = iterator.next();
                                    if (nextProject.getId() == id) {
                                        moduleProgress.setProject(nextProject);
                                        break;
                                    }
                                }
                            }


                            moduleProgress.setNumberOfRegressionAutomationTestCasesExecutedForTheWeek(progressPojoArray.get(i).getValue());
                            weeklyModuleLevelTestExecutionProgressRepository.save(moduleProgress);


                        } else if (progressPojoArray.get(i).getName().contains(moduleProgress.getDefectsForTheWeekName())) {
                            StringTokenizer tokenizer = new StringTokenizer(progressPojoArray.get(i).getName(), "||");
                            String moduleName = tokenizer.nextToken();

                            boolean moduleFound = false;

                            Iterable<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgressIterable = weeklyModuleLevelTestExecutionProgressRepository.findAll();
                            Iterator<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgressIterator = weeklyModuleLevelTestExecutionProgressIterable.iterator();

                            while (weeklyModuleLevelTestExecutionProgressIterator.hasNext()) {

                                moduleProgress = weeklyModuleLevelTestExecutionProgressIterator.next();

                                if ((moduleProgress.getProject().getId() == id) &&
                                        (moduleProgress.getWeekNumber() == weekNumber) &&
                                        (moduleProgress.getModuleName().compareTo(moduleName) == 0) &&
                                        (moduleProgress.getYearNumber() == yearNumber)) {
                                    moduleFound = true;
                                    break;
                                }

                            }

                            if (moduleFound == false) {
                                moduleProgress = new WeeklyModuleLevelTestExecutionProgress();
                                moduleProgress.setModuleName(moduleName);
                                moduleProgress.setWeekNumber(weekNumber);
                                moduleProgress.setMonthNumber(monthNumber);
                                moduleProgress.setYearNumber(yearNumber);


                                Iterable<Project> allProjects = projectRepository.findAll();
                                Iterator<Project> iterator = allProjects.iterator();


                                while (iterator.hasNext()) {
                                    Project nextProject = iterator.next();
                                    if (nextProject.getId() == id) {
                                        moduleProgress.setProject(nextProject);
                                        break;
                                    }
                                }
                            }


                            moduleProgress.setDefectsForTheWeek(progressPojoArray.get(i).getValue());
                            weeklyModuleLevelTestExecutionProgressRepository.save(moduleProgress);
                        }
                    }


                }

            } catch (Exception e) {

            }

        }catch(Exception e){

        }
        return "SUCCESS";
    }

}