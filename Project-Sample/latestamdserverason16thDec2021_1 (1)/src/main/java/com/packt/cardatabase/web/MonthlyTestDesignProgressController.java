package com.packt.cardatabase.web;

import com.google.gson.Gson;
import com.packt.cardatabase.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.*;


@RestController
public class MonthlyTestDesignProgressController {

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private MonthlyTestDesignProgressRepository monthlyTestDesignProgressRepository;


//	http://localhost:8082/springmvc/tutorial?topic=requestParam&author=daniel&site=jcg

	@RequestMapping(value="/projects/{id}/testDesign/monthlyProgress/month/{month}/year/{year}", method=RequestMethod.GET)
	@ResponseBody
	public String getMonthlyTestDesignProgress(@PathVariable("id") long id, @PathVariable(value="month") String month, @PathVariable(value="year") int year) {

		try {
			
			ArrayList<ProjectProgressPojo> progressPojoArray = new ArrayList<ProjectProgressPojo>();
			int monthNumber = 0;

			if(month.contains("Jan"))
			{
				monthNumber = 1;
			}
			else if(month.contains("Feb"))
			{
				monthNumber = 2;
			}
			else if(month.contains("Mar"))
			{
				monthNumber = 3;
			}
			else if(month.contains("Apr"))
			{
				monthNumber = 4;
			}
			else if(month.contains("May"))
			{
				monthNumber = 5;
			}
			else if(month.contains("Jun"))
			{
				monthNumber = 6;
			}
			else if(month.contains("Jul"))
			{
				monthNumber = 7;
			}
			else if(month.contains("Aug"))
			{
				monthNumber = 8;
			}
			else if(month.contains("Sep"))
			{
				monthNumber = 9;
			}
			else if(month.contains("Oct"))
			{
				monthNumber = 10;
			}
			else if(month.contains("Nov"))
			{
				monthNumber = 11;
			}
			else if(month.contains("Dec"))
			{
				monthNumber = 12;
			}
			else{
				monthNumber = Integer.parseInt(month);
			}

			int yearNumber = year;


			Iterable<MonthlyTestDesignProgress> monthlyDesignTestDesignProgressIterable = monthlyTestDesignProgressRepository.findAll();
			Iterator<MonthlyTestDesignProgress> monthlyDesignTestDesignProgressIterator = monthlyDesignTestDesignProgressIterable.iterator();
			String response = null;
			boolean recordFound = false;

			while (monthlyDesignTestDesignProgressIterator.hasNext()) {
				MonthlyTestDesignProgress next = monthlyDesignTestDesignProgressIterator.next();

				System.out.println("The current project is : " + next.getProject().getId());
				System.out.println("Current Month number is : " + next.getMonthNumber());


				if ((next.getMonthNumber() == monthNumber) && (next.getProject().getId() == id) && (next.getYearNumber() == yearNumber)) {
					System.out.println("The current project is : " + next.getProject().getId());
					System.out.println("Current Month number is : " + next.getMonthNumber());
					recordFound = true;

					int index = 1;
					ProjectProgressPojo projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("date");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getActualStartDateMilestoneName());
					try{ projectProgressPojo.setValue(next.getActualStartDateMilestone().toString());}catch(Exception e){projectProgressPojo.setValue("");}
					projectProgressPojo.setCategory("Basic Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("date");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getActualEndDateMilestoneName());
					try{ projectProgressPojo.setValue(next.getActualEndDateMilestone().toString());}catch(Exception e){projectProgressPojo.setValue("");}
					projectProgressPojo.setCategory("Basic Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNoOfTestCasesReusedForMultiCountryRolloutsName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getTotalNoOfTestCasesReusedForMultiCountryRollouts()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Test Design Progress");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfManualTestCasesDesignedName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfManualTestCasesDesigned()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Test Design Progress");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfTestCasesTaggedForMigrationExecutionName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfTestCasesTaggedForMigrationExecution()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Basic Information");
					progressPojoArray.add(projectProgressPojo);


					Gson gson = new Gson();
					response = gson.toJson(progressPojoArray);
					response = "{\"progress\":" + response + "}";

					System.out.println("The response message is : " + response);

					return response;
				}
			}

				if(recordFound == false){
					MonthlyTestDesignProgress next = new MonthlyTestDesignProgress();

					int index = 1;
					ProjectProgressPojo projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("date");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getActualStartDateMilestoneName());
					projectProgressPojo.setValue("");
					projectProgressPojo.setCategory("Basic Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("date");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getActualEndDateMilestoneName());
					projectProgressPojo.setValue("");
					projectProgressPojo.setCategory("Basic Information");
					progressPojoArray.add(projectProgressPojo);


					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNoOfTestCasesReusedForMultiCountryRolloutsName());
					projectProgressPojo.setValue("0");
					projectProgressPojo.setCategory("Test Design Progress");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfManualTestCasesDesignedName());
					projectProgressPojo.setValue("0");
					projectProgressPojo.setCategory("Test Design Progress");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfTestCasesTaggedForMigrationExecutionName());
					projectProgressPojo.setValue("0");
					projectProgressPojo.setCategory("Basic Information");
					progressPojoArray.add(projectProgressPojo);


					Gson gson = new Gson();
					response = gson.toJson(progressPojoArray);

					response = "{\"progress\":" + response + "}";

					System.out.println("The response message is : " + response);

					return response;

				}

		}catch(Exception e){
          System.out.println("The exception message is : " + e.getMessage());
		}
		return null;
	}


	@RequestMapping(value="/projects/{id}/testDesign/totalMonthlyProgress", method=RequestMethod.GET)
	@ResponseBody
	public String getTotalMonthlyTestDesignProgress(@PathVariable("id") long id) {

		try {
			ArrayList<ArrayList<ProjectProgressPojo>> monthlyDesignTestDesignProgressArray = new ArrayList<ArrayList<ProjectProgressPojo>>();

			Iterable<MonthlyTestDesignProgress> monthlyDesignTestDesignProgressIterable = monthlyTestDesignProgressRepository.findAll();
			Iterator<MonthlyTestDesignProgress> monthlyDesignTestDesignProgressIterator = monthlyDesignTestDesignProgressIterable.iterator();
			String response = null;

			while (monthlyDesignTestDesignProgressIterator.hasNext()) {
				MonthlyTestDesignProgress next = monthlyDesignTestDesignProgressIterator.next();

				if ((next.getProject().getId() == id)) {

					ArrayList<ProjectProgressPojo> progressPojoArray = new ArrayList<ProjectProgressPojo>();
					int index = 0;
					ProjectProgressPojo projectProgressPojo = null;

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("Index");
					projectProgressPojo.setName(next.getMonthNumberName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getMonthNumber()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("Index");
					projectProgressPojo.setName(next.getYearNumberName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getYearNumber()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("date");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getActualStartDateMilestoneName());
					try {
						projectProgressPojo.setValue(next.getActualStartDateMilestone());
					} catch (Exception e) {
						projectProgressPojo.setValue("");
					}
					projectProgressPojo.setCategory("Basic Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("date");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getActualEndDateMilestoneName());
					try {
						projectProgressPojo.setValue(next.getActualEndDateMilestone().toString());
					} catch (Exception e) {
						projectProgressPojo.setValue("");
					}
					projectProgressPojo.setCategory("BasicInformation");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNoOfTestCasesReusedForMultiCountryRolloutsName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getTotalNoOfTestCasesReusedForMultiCountryRollouts()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("TestDesignProgress");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfManualTestCasesDesignedName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfManualTestCasesDesigned()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("TestDesignProgress");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfTestCasesTaggedForMigrationExecutionName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfTestCasesTaggedForMigrationExecution()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("BasicInformation");
					progressPojoArray.add(projectProgressPojo);
					monthlyDesignTestDesignProgressArray.add(progressPojoArray);
				}
			}

			Gson gson = new Gson();
			response = gson.toJson(monthlyDesignTestDesignProgressArray);
			response = "{\"progress\":" + response + "}";
			System.out.println("The response message is : " + response);
			return response;


		}catch(Exception e){
			System.out.println("The exception message is : " + e.getMessage());
		}
		return null;
	}

//	http://localhost:8082/springmvc/tutorial?topic=requestParam&author=daniel&site=jcg
	@PostMapping(value = "/projects/{id}/testDesign/monthlyProgress/month/{month}/year/{year}")
	@ResponseBody
    public String updateMonthlyTestDesignProgress(@PathVariable("id") long id, @PathVariable(value="month") String month, @PathVariable(value="year") int year, @RequestBody List<ProjectProgressPojo> progressPojoArray) {

		try {

			int monthNumber = 0;

			if(month.contains("Jan"))
			{
				monthNumber = 1;
			}
			else if(month.contains("Feb"))
			{
				monthNumber = 2;
			}
			else if(month.contains("Mar"))
			{
				monthNumber = 3;
			}
			else if(month.contains("Apr"))
			{
				monthNumber = 4;
			}
			else if(month.contains("May"))
			{
				monthNumber = 5;
			}
			else if(month.contains("Jun"))
			{
				monthNumber = 6;
			}
			else if(month.contains("Jul"))
			{
				monthNumber = 7;
			}
			else if(month.contains("Aug"))
			{
				monthNumber = 8;
			}
			else if(month.contains("Sep"))
			{
				monthNumber = 9;
			}
			else if(month.contains("Oct"))
			{
				monthNumber = 10;
			}
			else if(month.contains("Nov"))
			{
				monthNumber = 11;
			}
			else if(month.contains("Dec"))
			{
				monthNumber = 12;
			}
			else{
				monthNumber = Integer.parseInt(month);
			}

			int yearNumber = year;


			Iterable<MonthlyTestDesignProgress> monthlyDesignTestDesignProgressIterable = monthlyTestDesignProgressRepository.findAll();
			Iterator<MonthlyTestDesignProgress> monthlyDesignTestDesignProgressIterator = monthlyDesignTestDesignProgressIterable.iterator();

			boolean matchFound = false;

			while (monthlyDesignTestDesignProgressIterator.hasNext()) {
				MonthlyTestDesignProgress next = monthlyDesignTestDesignProgressIterator.next();

				System.out.println("Desired project is : " + id);
				System.out.println("The current project is : " + next.getProject().getId());
				System.out.println("Desired Month number is : " + monthNumber);
				System.out.println("Current Month number is : " + next.getMonthNumber());


				if ((next.getMonthNumber() == monthNumber) && (next.getProject().getId() == id) && (next.getYearNumber() == yearNumber)) {

					matchFound = true;
					for (int i = 0; i < progressPojoArray.size(); i++) {

						if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNoOfTestCasesReusedForMultiCountryRolloutsName())) {

							String previousEntry = Integer.toString(next.getTotalNoOfTestCasesReusedForMultiCountryRollouts());
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if(currentUpdatesEntry != previousEntry) {
								next.setTotalNoOfTestCasesReusedForMultiCountryRollouts(currentUpdatesEntry);
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfManualTestCasesDesignedName())) {

							String previousEntry = Integer.toString(next.getTotalNumberOfManualTestCasesDesigned());
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if(currentUpdatesEntry != previousEntry) {
								next.setTotalNumberOfManualTestCasesDesigned(currentUpdatesEntry);
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfTestCasesTaggedForMigrationExecutionName())) {


							String previousEntry = Integer.toString(next.getTotalNumberOfTestCasesTaggedForMigrationExecution());
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if(currentUpdatesEntry != previousEntry) {
								next.setTotalNumberOfTestCasesTaggedForMigrationExecution(currentUpdatesEntry);
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getActualStartDateMilestoneName())) {


							String previousEntry = next.getActualStartDateMilestone();
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if(currentUpdatesEntry.compareTo(previousEntry) != 0) {
								next.setActualStartDateMilestone(currentUpdatesEntry);
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getActualEndDateMilestoneName())) {


							String previousEntry = next.getActualEndDateMilestone();
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if(currentUpdatesEntry.compareTo(previousEntry) != 0) {
								next.setActualEndDateMilestone(currentUpdatesEntry);
							}

						}
					}

					next.setMonthNumber(monthNumber);
					next.setYearNumber(yearNumber);

					monthlyTestDesignProgressRepository.save(next);
					break;
				}
			}
			if (matchFound == true)
				return "OK";
			else {

				MonthlyTestDesignProgress progress = new MonthlyTestDesignProgress();

				for (int i = 0; i < progressPojoArray.size(); i++) {

                    if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getTotalNoOfTestCasesReusedForMultiCountryRolloutsName())) {
						progress.setTotalNoOfTestCasesReusedForMultiCountryRollouts(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getTotalNumberOfManualTestCasesDesignedName())) {
						progress.setTotalNumberOfManualTestCasesDesigned(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getTotalNumberOfTestCasesTaggedForMigrationExecutionName())) {
						progress.setTotalNumberOfTestCasesTaggedForMigrationExecution(progressPojoArray.get(i).getValue());
					}else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getActualStartDateMilestoneName())) {
						progress.setActualStartDateMilestone(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getActualEndDateMilestoneName())) {
						progress.setActualEndDateMilestone(progressPojoArray.get(i).getValue());
					}
				}

				Iterable<Project> allProjects = projectRepository.findAll();
				Iterator<Project> iterator = allProjects.iterator();

				while (iterator.hasNext()) {
					Project nextProject = iterator.next();

					if (nextProject.getId() == id) {
						progress.setProject(nextProject);
						break;
					}
				}

				progress.setMonthNumber(monthNumber);
				progress.setYearNumber(yearNumber);

				monthlyTestDesignProgressRepository.save(progress);
				return "OK";

				}
		} catch (Exception e) {
		}
		return "Error";
	}
}