package com.packt.cardatabase.domain;

import javax.persistence.*;
import java.util.ArrayList;

@Entity
public class DefectsEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    private String projectFieldName;
    private ArrayList<String> projectName = null;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ProjectLevelDefects")
    private ProjectLevelDefectsEntity projectLevelDefects;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "moduleLevelDefects")
    private ModuleLevelDefectsEntity moduleLevelDefects;

    public String getProjectFieldName() {
        return projectFieldName;
    }

    public void setProjectFieldName(String projectFieldName) {
        this.projectFieldName = projectFieldName;
    }

    public ArrayList<String> getProjectName() {
        return projectName;
    }

    public void setProjectName(ArrayList<String> projectName) {
        this.projectName = projectName;
    }


    public ModuleLevelDefectsEntity getModuleLevelDefects() {
        return moduleLevelDefects;
    }

    public void setModuleLevelDefects(ModuleLevelDefectsEntity moduleLevelDefects) {
        this.moduleLevelDefects = moduleLevelDefects;
    }


    public ProjectLevelDefectsEntity getProjectLevelDefects() {
        return projectLevelDefects;
    }

    public void setProjectLevelDefects(ProjectLevelDefectsEntity projectLevelDefects) {
        this.projectLevelDefects = projectLevelDefects;
    }


    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

}
