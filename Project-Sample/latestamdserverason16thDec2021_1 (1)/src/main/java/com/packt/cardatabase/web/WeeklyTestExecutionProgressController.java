package com.packt.cardatabase.web;

import com.google.gson.Gson;
import com.packt.cardatabase.domain.*;
import com.packt.cardatabase.domain.Module;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;


@RestController
public class WeeklyTestExecutionProgressController {

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private ModuleRepository moduleRepository;

	@Autowired
	private WeeklyProjectLevelTestExecutionProgressRepository weeklyProjectLevelTestExecutionProgressRepository;

	@Autowired
	private WeeklyModuleLevelTestExecutionProgressRepository weeklyModuleLevelTestExecutionProgressRepository;

	@Autowired
	private WeekDataRepository weekDataRepository;

	@RequestMapping(value = "/projects/{id}/testExecution/weeklyProgress/week/{week}/year/{year}", method = RequestMethod.GET)
	@ResponseBody
	public String getWeeklyTestExecutionProgress(@PathVariable("id") long id, @PathVariable(value = "week") String week, @PathVariable(value = "year") String year) {

		try {

			ArrayList<ProjectProgressPojo> progressPojoArray = new ArrayList<ProjectProgressPojo>();
			StringTokenizer tokenizer = new StringTokenizer(week, "-");
			tokenizer.nextToken();
			int weekNumber = Integer.parseInt(tokenizer.nextToken());
			int yearNumber = Integer.parseInt(year);
			int monthNumber = 0;

			Iterable<WeekData> weekDataIterable = weekDataRepository.findAll();
			Iterator<WeekData> weekDataIterator = weekDataIterable.iterator();

			WeekData nextWeek = null;

			while (weekDataIterator.hasNext()) {
				nextWeek = weekDataIterator.next();
				if ((nextWeek.getWeekNumber() == weekNumber) && (nextWeek.getYearNumber() == yearNumber)) {
					monthNumber = nextWeek.getMonthNumber();
					break;
				}
			}

			System.out.println("The desired week number is : " + weekNumber);
			System.out.println("The desired month number is : " + monthNumber);
			System.out.println("The desired year number is : " + yearNumber);


			Iterable<WeeklyProjectLevelTestExecutionProgress> weeklyProjectLevelTestExecutionProgress = weeklyProjectLevelTestExecutionProgressRepository.findAll();
			Iterator<WeeklyProjectLevelTestExecutionProgress> weeklyProjectLevelTestExecutionProgressRepositoryIterator = weeklyProjectLevelTestExecutionProgress.iterator();
			String response = null;

			boolean recordFound = false;

			while (weeklyProjectLevelTestExecutionProgressRepositoryIterator.hasNext()) {
				WeeklyProjectLevelTestExecutionProgress next = weeklyProjectLevelTestExecutionProgressRepositoryIterator.next();
				if ((next.getWeekNumber() == weekNumber) && (next.getProject().getId() == id) && (next.getYearNumber() == yearNumber)) {
					recordFound = true;

					ProjectProgressPojo projectProgressPojo = null;
					int index = 0;

					Iterable<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgress = weeklyModuleLevelTestExecutionProgressRepository.findAll();
					Iterator<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgressIterator = weeklyModuleLevelTestExecutionProgress.iterator();

					while (weeklyModuleLevelTestExecutionProgressIterator.hasNext()) {

						WeeklyModuleLevelTestExecutionProgress moduleProgress = weeklyModuleLevelTestExecutionProgressIterator.next();


						if ((moduleProgress.getProject().getId() == id) && ( moduleProgress.getWeekNumber() == weekNumber ) && (moduleProgress.getYearNumber() == yearNumber)) {

							index = index + 1;
							projectProgressPojo = new ProjectProgressPojo();
							projectProgressPojo.setId(index);
							projectProgressPojo.setName(moduleProgress.getModuleName() + "||" + moduleProgress.getNumberOfManualTestCasesExecutedForTheWeekName());
							projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation");
							try{projectProgressPojo.setValue(Integer.toString(moduleProgress.getNumberOfManualTestCasesExecutedForTheWeek()));}catch(Exception e){projectProgressPojo.setValue("0");}
							projectProgressPojo.setCategory("Module Level Test Execution");
							progressPojoArray.add(projectProgressPojo);

/*
							index = index + 1;
							projectProgressPojo = new ProjectProgressPojo();
							projectProgressPojo.setId(index);
							projectProgressPojo.setName(moduleProgress.getModuleName() + "||" + moduleProgress.getManualTestExecutionEffortForTheWeekName());
							projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
							try{ projectProgressPojo.setValue(Integer.toString(moduleProgress.getManualTestExecutionEffortForTheWeek()));}catch(Exception e){projectProgressPojo.setValue("0");}
							projectProgressPojo.setCategory("ModuleLevelTestExecution");
							progressPojoArray.add(projectProgressPojo);
*/


							index = index + 1;
							projectProgressPojo = new ProjectProgressPojo();
							projectProgressPojo.setId(index);
							projectProgressPojo.setName(moduleProgress.getModuleName() + "||" + moduleProgress.getNumberOfRegressionAutomationTestCasesExecutedForTheWeekName());
							projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation");
							try{ projectProgressPojo.setValue(Integer.toString(moduleProgress.getNumberOfRegressionAutomationTestCasesExecutedForTheWeek()));}catch(Exception e){projectProgressPojo.setValue("0");}
							projectProgressPojo.setCategory("Module Level Test Execution");
							progressPojoArray.add(projectProgressPojo);

/*
							index = index + 1;
							projectProgressPojo = new ProjectProgressPojo();
							projectProgressPojo.setId(index);
							projectProgressPojo.setName(moduleProgress.getModuleName() + "||" + moduleProgress.getRegressionAutomationExecutionEffortForTheWeekName());
							projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
							try{ projectProgressPojo.setValue(Integer.toString(moduleProgress.getRegressionAutomationExecutionEffortForTheWeek()));}catch(Exception e){projectProgressPojo.setValue("0");}
							projectProgressPojo.setCategory("ModuleLevelTestExecution");
							progressPojoArray.add(projectProgressPojo);
*/
							index = index + 1;
							projectProgressPojo = new ProjectProgressPojo();
							projectProgressPojo.setId(index);
							projectProgressPojo.setName(moduleProgress.getModuleName() + "||" + moduleProgress.getDefectsForTheWeekName());
							projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation");
							try{ projectProgressPojo.setValue(Integer.toString(moduleProgress.getDefectsForTheWeek()));}catch(Exception e){projectProgressPojo.setValue("0");}
							projectProgressPojo.setCategory("Module Level Test Execution");
							progressPojoArray.add(projectProgressPojo);

						}
					}


					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfInProjectAutomationTestScriptsExecutedName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfInProjectAutomationTestScriptsExecuted()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Project Level Test Execution");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfAPIAutomationTestScriptsExecutedName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfAPIAutomationTestScriptsExecuted()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Project Level Test Execution");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfTestCasesExecutedWithMigratedDataName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfTestCasesExecutedWithMigratedData()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Project Level Test Execution");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfModulesNotDeliveredName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfModulesNotDelivered()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Need Attention");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getTotalNumberOfManualTestCasesNotExecutedName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try{ projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfManualTestCasesNotExecuted()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Need Attention");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getTotalNumberOfAutomationTestCasesNotExecutedName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try{ projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfAutomationTestCasesNotExecuted()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Need Attention");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfTestCasesSuccessfullyExecutedWithoutDefectsName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfTestCasesSuccessfullyExecutedWithoutDefects()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Need Attention");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfDefectsReportedByTestingTeamName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation");
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfDefectsReportedByTestingTeam()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfDefectsRejectedName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation");
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfDefectsRejected()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfValidDefectsLoggedInSITName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation");
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfValidDefectsLoggedInSIT()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfValidDefectsLoggedInUATName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation");
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfValidDefectsLoggedInUAT()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getTotalNumberOfDefectsFixedName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation");
					try{ projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfDefectsFixed()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getTotalNumberOfDefectsReopenedName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation");
					try{ projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfDefectsReopened()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getAverageTimeTakenToResolveSev1DefectsName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try{ projectProgressPojo.setValue(Integer.toString(next.getAverageTimeTakenToResolveSev1Defects()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getEnvironmentDowntimeDuringTestExecutionPerPersonInHoursName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try{ projectProgressPojo.setValue(Integer.toString(next.getEnvironmentDowntimeDuringTestExecutionPerPersonInHours()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getTotalNumberOfDefectsInProductionName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try{ projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfDefectsInProduction()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getManualTestCaseExecutionEffortName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try{ projectProgressPojo.setValue(Integer.toString(next.getManualTestCaseExecutionEffort()));}catch(Exception e){projectProgressPojo.setValue("0");}
					try{ projectProgressPojo.setStartDate(next.getManualTestCaseExecutionEffortStartDate());}catch(Exception e){projectProgressPojo.setStartDate("");}
					try{ projectProgressPojo.setEndDate(next.getManualTestCaseExecutionEffortEndDate());}catch(Exception e){projectProgressPojo.setEndDate("");}
					projectProgressPojo.setCategory("Effort");
					projectProgressPojo.setDays(true);
					progressPojoArray.add(projectProgressPojo);


					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getRegressionAutomationTestCaseExecutionEffortName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try{ projectProgressPojo.setValue(Integer.toString(next.getRegressionAutomationTestCaseExecutionEffort()));}catch(Exception e){projectProgressPojo.setValue("0");}
					try{ projectProgressPojo.setStartDate(next.getRegressionAutomationTestCaseExecutionEffortStartDate());}catch(Exception e){projectProgressPojo.setStartDate("");}
					try{ projectProgressPojo.setEndDate(next.getRegressionAutomationTestCaseExecutionEffortEndDate());}catch(Exception e){projectProgressPojo.setEndDate("");}
					projectProgressPojo.setCategory("Effort");
					projectProgressPojo.setDays(true);
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getInProjectAutomationTestCaseExecutionEffortName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try{ projectProgressPojo.setValue(Integer.toString(next.getInProjectAutomationTestCaseExecutionEffort()));}catch(Exception e){projectProgressPojo.setValue("0");}
					try{ projectProgressPojo.setStartDate(next.getInProjectAutomationTestCaseExecutionEffortStartDate());}catch(Exception e){projectProgressPojo.setStartDate("");}
					try{ projectProgressPojo.setEndDate(next.getInProjectAutomationTestCaseExecutionEffortEndDate());}catch(Exception e){projectProgressPojo.setEndDate("");}
					projectProgressPojo.setCategory("Effort");
					projectProgressPojo.setDays(true);
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getAPIAutomationTestCaseExecutionEffortName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try{ projectProgressPojo.setValue(Integer.toString(next.getAPIAutomationTestCaseExecutionEffort()));}catch(Exception e){projectProgressPojo.setValue("0");}
					try{ projectProgressPojo.setStartDate(next.getApiAutomationTestCaseExecutionEffortStartDate());}catch(Exception e){projectProgressPojo.setStartDate("");}
					try{ projectProgressPojo.setEndDate(next.getApiAutomationTestCaseExecutionEffortEndDate());}catch(Exception e){projectProgressPojo.setEndDate("");}
					projectProgressPojo.setCategory("Effort");
					projectProgressPojo.setDays(true);
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfResourcesForManualTestCaseExecutionName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfResourcesForManualTestCaseExecution()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Effort");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfResourceForRegressionAutomationExecutionName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfResourceForRegressionAutomationExecution()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Effort");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfResourceForInProjectAutomationExecutionName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfResourceForInProjectAutomationExecution()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Effort");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfResourceForAPIAutomationExecutionName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfResourceForAPIAutomationExecution()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Effort");
					progressPojoArray.add(projectProgressPojo);


					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfDefectsFoundUsingManualRegressionName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation");
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfDefectsFoundUsingManualRegression()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfDefectsFoundUsingAutomatedRegressionName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation");
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfDefectsFoundUsingAutomatedRegression()));}catch(Exception e){projectProgressPojo.setValue("0");}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);


					Gson gson = new Gson();
					response = gson.toJson(progressPojoArray);
					response = "{\"progress\":" + response + "}";

					System.out.println(response);

					return response;
				}
			}

			if (recordFound == false) {
				ProjectProgressPojo projectProgressPojo = null;
				WeeklyProjectLevelTestExecutionProgress next = new WeeklyProjectLevelTestExecutionProgress();
				WeeklyModuleLevelTestExecutionProgress nextModule = new WeeklyModuleLevelTestExecutionProgress();

				int index = 0;

				Iterable<Module> allModules = moduleRepository.findAll();
				Iterator<Module> modulesIterator = allModules.iterator();


				while (modulesIterator.hasNext()) {
					Module module = modulesIterator.next();

					if (module.getProject().getId() == id) {
						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(module.getName() + "||" + nextModule.getNumberOfManualTestCasesExecutedForTheWeekName());
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation"); projectProgressPojo.setValue("0");
						projectProgressPojo.setCategory("Module Level Test Execution");
						progressPojoArray.add(projectProgressPojo);
/*
						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(module.getName() + "||" + nextModule.getManualTestExecutionEffortForTheWeekName());
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation"); projectProgressPojo.setValue("0");
						projectProgressPojo.setCategory("ModuleLevelTestExecution");
						progressPojoArray.add(projectProgressPojo);
*/
						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(module.getName() + "||" + nextModule.getNumberOfRegressionAutomationTestCasesExecutedForTheWeekName());
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation"); projectProgressPojo.setValue("0");
						projectProgressPojo.setCategory("Module Level Test Execution");
						progressPojoArray.add(projectProgressPojo);
/*
						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(module.getName() + "||" + nextModule.getRegressionAutomationExecutionEffortForTheWeekName());
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
						projectProgressPojo.setCategory("ModuleLevelTestExecution");
						progressPojoArray.add(projectProgressPojo);
*/
						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setName(module.getName() + "||" + nextModule.getDefectsForTheWeekName());
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation"); projectProgressPojo.setValue("0");
						projectProgressPojo.setCategory("Module Level Test Execution");
						progressPojoArray.add(projectProgressPojo);
					}
				}


				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getNumberOfInProjectAutomationTestScriptsExecutedName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Project Level Test Execution");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getNumberOfAPIAutomationTestScriptsExecutedName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Project Level Test Execution");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getNumberOfTestCasesExecutedWithMigratedDataName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Project Level Test Execution");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getNumberOfModulesNotDeliveredName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Need Attention");
				progressPojoArray.add(projectProgressPojo);


				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getTotalNumberOfManualTestCasesNotExecutedName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Need Attention");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getTotalNumberOfAutomationTestCasesNotExecutedName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Need Attention");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getEnvironmentDowntimeDuringTestExecutionPerPersonInHoursName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Need Attention");
				progressPojoArray.add(projectProgressPojo);


				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getNumberOfTestCasesSuccessfullyExecutedWithoutDefectsName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Need Attention");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getNumberOfDefectsRejectedName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Defects");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getNumberOfValidDefectsLoggedInSITName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Defects");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getNumberOfValidDefectsLoggedInUATName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Defects");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getTotalNumberOfDefectsFixedName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Defects");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getTotalNumberOfDefectsReopenedName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Defects");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getAverageTimeTakenToResolveSev1DefectsName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Defects");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getNumberOfDefectsReportedByTestingTeamName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Defects");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getTotalNumberOfDefectsInProductionName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Defects");
				progressPojoArray.add(projectProgressPojo);


				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getManualTestCaseExecutionEffortName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setStartDate("");
				projectProgressPojo.setEndDate("");
				projectProgressPojo.setCategory("Effort");
				projectProgressPojo.setDays(true);
				progressPojoArray.add(projectProgressPojo);


				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getRegressionAutomationTestCaseExecutionEffortName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setStartDate("");
				projectProgressPojo.setEndDate("");
				projectProgressPojo.setCategory("Effort");
				projectProgressPojo.setDays(true);
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getInProjectAutomationTestCaseExecutionEffortName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setStartDate("");
				projectProgressPojo.setEndDate("");
				projectProgressPojo.setCategory("Effort");
				projectProgressPojo.setDays(true);
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getAPIAutomationTestCaseExecutionEffortName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setStartDate("");
				projectProgressPojo.setEndDate("");
				projectProgressPojo.setCategory("Effort");
				projectProgressPojo.setDays(true);
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getNumberOfResourcesForManualTestCaseExecutionName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Effort");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getNumberOfResourceForRegressionAutomationExecutionName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Effort");
				progressPojoArray.add(projectProgressPojo);


				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getNumberOfResourceForInProjectAutomationExecutionName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Effort");
				progressPojoArray.add(projectProgressPojo);


				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getNumberOfResourceForAPIAutomationExecutionName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Effort");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getNumberOfDefectsFoundUsingManualRegressionName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Defects");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);
				projectProgressPojo.setName(next.getNumberOfDefectsFoundUsingAutomatedRegressionName());
				projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation"); projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Defects");
				progressPojoArray.add(projectProgressPojo);


				Gson gson = new Gson();
				response = gson.toJson(progressPojoArray);

				System.out.println(response);
				response = "{\"progress\":" + response + "}";

				return response;


			}
		} catch (Exception e) {

		}
		return null;
	}


	@RequestMapping(value = "/projects/{id}/testExecution/totalWeeklyProgress", method = RequestMethod.GET)
	@ResponseBody
	public String getTotalWeeklyTestExecutionProgress(@PathVariable("id") long id) {

		try {

			ArrayList<ArrayList<ProjectProgressPojo>> weeklyTestExecutionProgressArray = new ArrayList<ArrayList<ProjectProgressPojo>>();
			Iterable<WeeklyProjectLevelTestExecutionProgress> weeklyProjectLevelTestExecutionProgress = weeklyProjectLevelTestExecutionProgressRepository.findAll();
			Iterator<WeeklyProjectLevelTestExecutionProgress> weeklyProjectLevelTestExecutionProgressRepositoryIterator = weeklyProjectLevelTestExecutionProgress.iterator();

			String response = null;

			while (weeklyProjectLevelTestExecutionProgressRepositoryIterator.hasNext()) {

				WeeklyProjectLevelTestExecutionProgress next = weeklyProjectLevelTestExecutionProgressRepositoryIterator.next();

				if (next.getProject().getId() == id) {

					ArrayList<ProjectProgressPojo> progressPojoArray = new ArrayList<ProjectProgressPojo>();
					ProjectProgressPojo projectProgressPojo = null;
					int index = 0;


					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("Index");
					projectProgressPojo.setName(next.getWeekNumberName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getWeekNumber()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("Index");
					projectProgressPojo.setName(next.getYearNumberName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getYearNumber()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);


					Iterable<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgress = weeklyModuleLevelTestExecutionProgressRepository.findAll();
					Iterator<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgressIterator = weeklyModuleLevelTestExecutionProgress.iterator();

					while (weeklyModuleLevelTestExecutionProgressIterator.hasNext()) {

						WeeklyModuleLevelTestExecutionProgress moduleProgress = weeklyModuleLevelTestExecutionProgressIterator.next();


						if ((moduleProgress.getProject().getId() == id) &&
								(moduleProgress.getWeekNumber() == next.getWeekNumber()) &&
								(moduleProgress.getYearNumber() == next.getYearNumber())) {

							index = index + 1;
							projectProgressPojo = new ProjectProgressPojo();
							projectProgressPojo.setId(index);
							projectProgressPojo.setName(moduleProgress.getModuleName() + "||" + moduleProgress.getNumberOfManualTestCasesExecutedForTheWeekName());
							projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation");
							try {
								projectProgressPojo.setValue(Integer.toString(moduleProgress.getNumberOfManualTestCasesExecutedForTheWeek()));
							} catch (Exception e) {
								projectProgressPojo.setValue("0");
							}
							projectProgressPojo.setCategory("ModuleLevelTestExecution");
							progressPojoArray.add(projectProgressPojo);

/*
							index = index + 1;
							projectProgressPojo = new ProjectProgressPojo();
							projectProgressPojo.setId(index);
							projectProgressPojo.setName(moduleProgress.getModuleName() + "||" + moduleProgress.getManualTestExecutionEffortForTheWeekName());
							projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
							try {
								projectProgressPojo.setValue(Integer.toString(moduleProgress.getManualTestExecutionEffortForTheWeek()));
							} catch (Exception e) {
								projectProgressPojo.setValue("0");
							}
							projectProgressPojo.setCategory("ModuleLevelTestExecution");
							progressPojoArray.add(projectProgressPojo);
*/

							index = index + 1;
							projectProgressPojo = new ProjectProgressPojo();
							projectProgressPojo.setId(index);
							projectProgressPojo.setName(moduleProgress.getModuleName() + "||" + moduleProgress.getNumberOfRegressionAutomationTestCasesExecutedForTheWeekName());
							projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation");
							try {
								projectProgressPojo.setValue(Integer.toString(moduleProgress.getNumberOfRegressionAutomationTestCasesExecutedForTheWeek()));
							} catch (Exception e) {
								projectProgressPojo.setValue("0");
							}
							projectProgressPojo.setCategory("ModuleLevelTestExecution");
							progressPojoArray.add(projectProgressPojo);

/*
							index = index + 1;
							projectProgressPojo = new ProjectProgressPojo();
							projectProgressPojo.setId(index);
							projectProgressPojo.setName(moduleProgress.getModuleName() + "||" + moduleProgress.getRegressionAutomationExecutionEffortForTheWeekName());
							projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
							try {
								projectProgressPojo.setValue(Integer.toString(moduleProgress.getRegressionAutomationExecutionEffortForTheWeek()));
							} catch (Exception e) {
								projectProgressPojo.setValue("0");
							}
							projectProgressPojo.setCategory("ModuleLevelTestExecution");
							progressPojoArray.add(projectProgressPojo);
*/
							index = index + 1;
							projectProgressPojo = new ProjectProgressPojo();
							projectProgressPojo.setId(index);
							projectProgressPojo.setName(moduleProgress.getModuleName() + "||" + moduleProgress.getDefectsForTheWeekName());
							projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation");
							try {
								projectProgressPojo.setValue(Integer.toString(moduleProgress.getDefectsForTheWeek()));
							} catch (Exception e) {
								projectProgressPojo.setValue("0");
							}
							projectProgressPojo.setCategory("ModuleLevelTestExecution");
							progressPojoArray.add(projectProgressPojo);

						}
					}


					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfInProjectAutomationTestScriptsExecutedName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfInProjectAutomationTestScriptsExecuted()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("ProjectLevelTestExecution");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfAPIAutomationTestScriptsExecutedName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfAPIAutomationTestScriptsExecuted()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("ProjectLevelTestExecution");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfTestCasesExecutedWithMigratedDataName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfTestCasesExecutedWithMigratedData()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("ProjectLevelTestExecution");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfModulesNotDeliveredName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfModulesNotDelivered()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("NeedAttention");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getTotalNumberOfManualTestCasesNotExecutedName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfManualTestCasesNotExecuted()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("NeedAttention");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getTotalNumberOfAutomationTestCasesNotExecutedName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfAutomationTestCasesNotExecuted()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("NeedAttention");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfTestCasesSuccessfullyExecutedWithoutDefectsName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfTestCasesSuccessfullyExecutedWithoutDefects()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("NeedAttention");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfDefectsReportedByTestingTeamName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfDefectsReportedByTestingTeam()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfDefectsRejectedName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfDefectsRejected()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfValidDefectsLoggedInSITName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfValidDefectsLoggedInSIT()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfValidDefectsLoggedInUATName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfValidDefectsLoggedInUAT()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getTotalNumberOfDefectsFixedName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfDefectsFixed()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getTotalNumberOfDefectsReopenedName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfDefectsReopened()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getAverageTimeTakenToResolveSev1DefectsName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getAverageTimeTakenToResolveSev1Defects()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getEnvironmentDowntimeDuringTestExecutionPerPersonInHoursName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getEnvironmentDowntimeDuringTestExecutionPerPersonInHours()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getTotalNumberOfDefectsInProductionName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfDefectsInProduction()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getManualTestCaseExecutionEffortName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getManualTestCaseExecutionEffort()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("Effort");
					progressPojoArray.add(projectProgressPojo);


					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getRegressionAutomationTestCaseExecutionEffortName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getRegressionAutomationTestCaseExecutionEffort()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("Effort");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getInProjectAutomationTestCaseExecutionEffortName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getInProjectAutomationTestCaseExecutionEffort()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("Effort");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getAPIAutomationTestCaseExecutionEffortName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getAPIAutomationTestCaseExecutionEffort()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("Effort");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfResourcesForManualTestCaseExecutionName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfResourcesForManualTestCaseExecution()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("Effort");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfResourceForRegressionAutomationExecutionName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfResourceForRegressionAutomationExecution()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("Effort");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfResourceForInProjectAutomationExecutionName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfResourceForInProjectAutomationExecution()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("Effort");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfResourceForAPIAutomationExecutionName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfResourceForAPIAutomationExecution()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("Effort");
					progressPojoArray.add(projectProgressPojo);


					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfDefectsFoundUsingManualRegressionName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfDefectsFoundUsingManualRegression()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setName(next.getNumberOfDefectsFoundUsingAutomatedRegressionName());
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Automation");
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfDefectsFoundUsingAutomatedRegression()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					weeklyTestExecutionProgressArray.add(progressPojoArray);
				}
			}

					Gson gson = new Gson();
					response = gson.toJson(weeklyTestExecutionProgressArray);
					response = "{\"progress\":" + response + "}";

					System.out.println(response);

					return response;



		} catch (Exception e) {

		}
		return null;
	}

	//	http://localhost:8082/springmvc/tutorial?topic=requestParam&author=daniel&site=jcg
	@PostMapping(value = "/projects/{id}/testExecution/weeklyProgress/week/{week}/year/{year}")
	@ResponseBody
	public String updateWeeklyTestExecutionProgress(@PathVariable("id") long id, @PathVariable(value = "week") String week, @PathVariable(value = "year") String year, @RequestBody List<ProjectProgressPojo> progressPojoArray) {

		try {

			StringTokenizer weekTokenizer = new StringTokenizer(week, "-");
			weekTokenizer.nextToken();
			int weekNumber = Integer.parseInt(weekTokenizer.nextToken());
			int yearNumber = Integer.parseInt(year);
			int monthNumber = 0;

			Iterable<WeekData> weekDataIterable = weekDataRepository.findAll();
			Iterator<WeekData> weekDataIterator = weekDataIterable.iterator();

			WeekData nextWeek = null;

			while (weekDataIterator.hasNext()) {
				nextWeek = weekDataIterator.next();
				if ((nextWeek.getWeekNumber() == weekNumber) && (nextWeek.getYearNumber() == yearNumber)) {
					monthNumber = nextWeek.getMonthNumber();
					break;
				}
			}

			Iterable<WeeklyProjectLevelTestExecutionProgress> weeklyProjectLevelTestExecutionProgressIterable = weeklyProjectLevelTestExecutionProgressRepository.findAll();
			Iterator<WeeklyProjectLevelTestExecutionProgress> weeklyProjectLevelTestExecutionProgressRepositoryIterator = weeklyProjectLevelTestExecutionProgressIterable.iterator();

			boolean matchFound = false;

			while (weeklyProjectLevelTestExecutionProgressRepositoryIterator.hasNext()) {

				WeeklyProjectLevelTestExecutionProgress next = weeklyProjectLevelTestExecutionProgressRepositoryIterator.next();
				if ((next.getWeekNumber() == weekNumber) && (next.getProject().getId() == id) && (next.getYearNumber() == yearNumber)) {
					matchFound = true;

					for (int i = 0; i < progressPojoArray.size(); i++) {

						try {

							if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfInProjectAutomationTestScriptsExecutedName())) {

								String previousEntry = Integer.toString(next.getNumberOfInProjectAutomationTestScriptsExecuted());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setNumberOfInProjectAutomationTestScriptsExecuted(currentUpdatesEntry);
								}


							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfAPIAutomationTestScriptsExecutedName())) {

								String previousEntry = Integer.toString(next.getNumberOfAPIAutomationTestScriptsExecuted());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setNumberOfAPIAutomationTestScriptsExecuted(currentUpdatesEntry);
								}


							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfTestCasesExecutedWithMigratedDataName())) {

								String previousEntry = Integer.toString(next.getNumberOfTestCasesExecutedWithMigratedData());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setNumberOfTestCasesExecutedWithMigratedData(currentUpdatesEntry);
								}


							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfModulesNotDeliveredName())) {

								String previousEntry = Integer.toString(next.getNumberOfModulesNotDelivered());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setNumberOfModulesNotDelivered(currentUpdatesEntry);
								}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfManualTestCasesNotExecutedName())) {

								String previousEntry = Integer.toString(next.getTotalNumberOfManualTestCasesNotExecuted());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setTotalNumberOfManualTestCasesNotExecuted(currentUpdatesEntry);
								}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfAutomationTestCasesNotExecutedName())) {

								String previousEntry = Integer.toString(next.getTotalNumberOfAutomationTestCasesNotExecuted());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setTotalNumberOfAutomationTestCasesNotExecuted(currentUpdatesEntry);
								}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfTestCasesSuccessfullyExecutedWithoutDefectsName())) {

								String previousEntry = Integer.toString(next.getNumberOfTestCasesSuccessfullyExecutedWithoutDefects());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setNumberOfTestCasesSuccessfullyExecutedWithoutDefects(currentUpdatesEntry);
								}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfDefectsReportedByTestingTeamName())) {

								String previousEntry = Integer.toString(next.getNumberOfDefectsReportedByTestingTeam());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setNumberOfDefectsReportedByTestingTeam(currentUpdatesEntry);
								}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfDefectsRejectedName())) {

								String previousEntry = Integer.toString(next.getNumberOfDefectsRejected());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setNumberOfDefectsRejected(currentUpdatesEntry);
								}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfValidDefectsLoggedInSITName())) {

								String previousEntry = Integer.toString(next.getNumberOfValidDefectsLoggedInSIT());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setNumberOfValidDefectsLoggedInSIT(currentUpdatesEntry);
								}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfValidDefectsLoggedInUATName())) {

								String previousEntry = Integer.toString(next.getNumberOfValidDefectsLoggedInUAT());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setNumberOfValidDefectsLoggedInUAT(currentUpdatesEntry);
								}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfDefectsFixedName())) {

								String previousEntry = Integer.toString(next.getTotalNumberOfDefectsFixed());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setTotalNumberOfDefectsFixed(currentUpdatesEntry);
								}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfDefectsReopenedName())) {

								String previousEntry = Integer.toString(next.getTotalNumberOfDefectsReopened());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setTotalNumberOfDefectsReopened(currentUpdatesEntry);
								}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getAverageTimeTakenToResolveSev1DefectsName())) {

								String previousEntry = Integer.toString(next.getAverageTimeTakenToResolveSev1Defects());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setAverageTimTakenToResolveSev1Defects(currentUpdatesEntry);
								}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getEnvironmentDowntimeDuringTestExecutionPerPersonInHoursName())) {

								String previousEntry = Integer.toString(next.getEnvironmentDowntimeDuringTestExecutionPerPersonInHours());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setEnvironmentDowntimeDuringTestExecutionPerPersonInHours(currentUpdatesEntry);
								}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getManualTestCaseExecutionEffortName())) {

								String previousEntry = Integer.toString(next.getManualTestCaseExecutionEffort());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setManualTestCaseExecutionEffort(currentUpdatesEntry);
									next.setManualTestCaseExecutionEffortStartDate(progressPojoArray.get(i).getStartDate());
									next.setManualTestCaseExecutionEffortEndDate(progressPojoArray.get(i).getEndDate());
								}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getRegressionAutomationTestCaseExecutionEffortName())) {

								String previousEntry = Integer.toString(next.getRegressionAutomationTestCaseExecutionEffort());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setRegressionAutomationTestCaseExecutionEffort(currentUpdatesEntry);
									next.setRegressionAutomationTestCaseExecutionEffortStartDate(progressPojoArray.get(i).getStartDate());
									next.setRegressionAutomationTestCaseExecutionEffortEndDate(progressPojoArray.get(i).getEndDate());
								}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getInProjectAutomationTestCaseExecutionEffortName())) {

								String previousEntry = Integer.toString(next.getInProjectAutomationTestCaseExecutionEffort());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setInProjectAutomationTestCaseExecutionEffort(currentUpdatesEntry);
									next.setInProjectAutomationTestCaseExecutionEffortStartDate(progressPojoArray.get(i).getStartDate());
									next.setInProjectAutomationTestCaseExecutionEffortEndDate(progressPojoArray.get(i).getEndDate());
								}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getAPIAutomationTestCaseExecutionEffortName())) {

								String previousEntry = Integer.toString(next.getAPIAutomationTestCaseExecutionEffort());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setAPIAutomationTestCaseExecutionEffort(currentUpdatesEntry);
									next.setApiAutomationTestCaseExecutionEffortStartDate(progressPojoArray.get(i).getStartDate());
									next.setApiAutomationTestCaseExecutionEffortEndDate(progressPojoArray.get(i).getEndDate());
								}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfResourcesForManualTestCaseExecutionName())) {

								String previousEntry = Integer.toString(next.getNumberOfResourcesForManualTestCaseExecution());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setNumberOfResourcesForManualTestCaseExecution(currentUpdatesEntry);
								}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfResourcesForManualTestCaseExecutionName())) {

								String previousEntry = Integer.toString(next.getNumberOfResourcesForManualTestCaseExecution());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setNumberOfResourcesForManualTestCaseExecution(currentUpdatesEntry);
								}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfResourceForRegressionAutomationExecutionName())) {

								String previousEntry = Integer.toString(next.getNumberOfResourceForRegressionAutomationExecution());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setNumberOfResourceForRegressionAutomationExecution(currentUpdatesEntry);
								}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfResourceForInProjectAutomationExecutionName())) {

								String previousEntry = Integer.toString(next.getNumberOfResourceForInProjectAutomationExecution());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setNumberOfResourceForInProjectAutomationExecution(currentUpdatesEntry);
								}

							}  else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfResourceForAPIAutomationExecutionName())) {

								String previousEntry = Integer.toString(next.getNumberOfResourceForAPIAutomationExecution());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setNumberOfResourceForAPIAutomationExecution(currentUpdatesEntry);
								}

							}  else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfDefectsInProductionName())) {

								String previousEntry = Integer.toString(next.getTotalNumberOfDefectsInProduction());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setTotalNumberOfDefectsInProduction(currentUpdatesEntry);
								}

							}  else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfDefectsFoundUsingManualRegressionName())) {

								String previousEntry = Integer.toString(next.getNumberOfDefectsFoundUsingManualRegression());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setNumberOfDefectsFoundUsingManualRegression(currentUpdatesEntry);
								}

							}  else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfDefectsFoundUsingAutomatedRegressionName())) {

								String previousEntry = Integer.toString(next.getNumberOfDefectsFoundUsingAutomatedRegression());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setNumberOfDefectsFoundUsingAutomatedRegression(currentUpdatesEntry);
								}

							}
						} catch (Exception e) {
							System.out.println("The exception message for Test execution update :" + e.getMessage());
						}
					}

					next.setWeekNumber(weekNumber);
					next.setMonthNumber(monthNumber);
					next.setYearNumber(yearNumber);

					weeklyProjectLevelTestExecutionProgressRepository.save(next);
					break;
				}
			}



			if (matchFound == false) {


				WeeklyProjectLevelTestExecutionProgress next = null;

				Iterable<Project> allProjects = projectRepository.findAll();
				Iterator<Project> iterator = allProjects.iterator();

				while (iterator.hasNext()) {
					Project nextProject = iterator.next();

					if (nextProject.getId() == id) {
						next = new WeeklyProjectLevelTestExecutionProgress("0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", weekNumber, monthNumber, yearNumber, nextProject);
						break;
					}
				}

				for (int i = 0; i < progressPojoArray.size(); i++) {

					try {
						if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfInProjectAutomationTestScriptsExecutedName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setNumberOfInProjectAutomationTestScriptsExecuted(currentUpdatesEntry);
							}


						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfAPIAutomationTestScriptsExecutedName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setNumberOfAPIAutomationTestScriptsExecuted(currentUpdatesEntry);
							}


						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfTestCasesExecutedWithMigratedDataName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setNumberOfTestCasesExecutedWithMigratedData(currentUpdatesEntry);
							}


						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfModulesNotDeliveredName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setNumberOfModulesNotDelivered(currentUpdatesEntry);
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfManualTestCasesNotExecutedName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setTotalNumberOfManualTestCasesNotExecuted(currentUpdatesEntry);
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfAutomationTestCasesNotExecutedName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setTotalNumberOfAutomationTestCasesNotExecuted(currentUpdatesEntry);
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfTestCasesSuccessfullyExecutedWithoutDefectsName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setNumberOfTestCasesSuccessfullyExecutedWithoutDefects(currentUpdatesEntry);
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfDefectsReportedByTestingTeamName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setNumberOfDefectsReportedByTestingTeam(currentUpdatesEntry);
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfDefectsRejectedName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setNumberOfDefectsRejected(currentUpdatesEntry);
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfValidDefectsLoggedInSITName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setNumberOfValidDefectsLoggedInSIT(currentUpdatesEntry);
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfValidDefectsLoggedInUATName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setNumberOfValidDefectsLoggedInUAT(currentUpdatesEntry);
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfDefectsFixedName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setTotalNumberOfDefectsFixed(currentUpdatesEntry);
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfDefectsReopenedName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setTotalNumberOfDefectsReopened(currentUpdatesEntry);
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getAverageTimeTakenToResolveSev1DefectsName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setAverageTimTakenToResolveSev1Defects(currentUpdatesEntry);
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getEnvironmentDowntimeDuringTestExecutionPerPersonInHoursName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setEnvironmentDowntimeDuringTestExecutionPerPersonInHours(currentUpdatesEntry);
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getManualTestCaseExecutionEffortName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setManualTestCaseExecutionEffort(currentUpdatesEntry);
								next.setManualTestCaseExecutionEffortStartDate(progressPojoArray.get(i).getStartDate());
								next.setManualTestCaseExecutionEffortEndDate(progressPojoArray.get(i).getEndDate());
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getRegressionAutomationTestCaseExecutionEffortName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setRegressionAutomationTestCaseExecutionEffort(currentUpdatesEntry);
								next.setRegressionAutomationTestCaseExecutionEffortStartDate(progressPojoArray.get(i).getStartDate());
								next.setRegressionAutomationTestCaseExecutionEffortEndDate(progressPojoArray.get(i).getEndDate());
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getInProjectAutomationTestCaseExecutionEffortName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setInProjectAutomationTestCaseExecutionEffort(currentUpdatesEntry);
								next.setInProjectAutomationTestCaseExecutionEffortStartDate(progressPojoArray.get(i).getStartDate());
								next.setInProjectAutomationTestCaseExecutionEffortEndDate(progressPojoArray.get(i).getEndDate());
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getAPIAutomationTestCaseExecutionEffortName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setAPIAutomationTestCaseExecutionEffort(currentUpdatesEntry);
								next.setApiAutomationTestCaseExecutionEffortStartDate(progressPojoArray.get(i).getStartDate());
								next.setApiAutomationTestCaseExecutionEffortEndDate(progressPojoArray.get(i).getEndDate());

							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfResourcesForManualTestCaseExecutionName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setNumberOfResourcesForManualTestCaseExecution(currentUpdatesEntry);
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfResourcesForManualTestCaseExecutionName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setNumberOfResourcesForManualTestCaseExecution(currentUpdatesEntry);
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfResourceForRegressionAutomationExecutionName())) {

							String previousEntry = Integer.toString(next.getNumberOfResourceForRegressionAutomationExecution());
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setNumberOfResourceForRegressionAutomationExecution(currentUpdatesEntry);
							}

						}  else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfResourceForAPIAutomationExecutionName())) {

							String previousEntry = Integer.toString(next.getNumberOfResourceForAPIAutomationExecution());
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setNumberOfResourceForAPIAutomationExecution(currentUpdatesEntry);
							}

						}  else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfResourceForInProjectAutomationExecutionName())) {

							String previousEntry = Integer.toString(next.getNumberOfResourceForInProjectAutomationExecution());
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setNumberOfResourceForInProjectAutomationExecution(currentUpdatesEntry);
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfDefectsInProductionName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setTotalNumberOfDefectsInProduction(currentUpdatesEntry);
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfDefectsFoundUsingManualRegressionName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setNumberOfDefectsFoundUsingManualRegression(currentUpdatesEntry);
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfDefectsFoundUsingAutomatedRegressionName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setNumberOfDefectsFoundUsingAutomatedRegression(currentUpdatesEntry);
							}

						}
					} catch (Exception e) {
						System.out.println("The exception message for Test execution update :" + e.getMessage());
					}
				}
				next.setWeekNumber(weekNumber);
				next.setMonthNumber(monthNumber);
				next.setYearNumber(yearNumber);

				weeklyProjectLevelTestExecutionProgressRepository.save(next);
			}



			for (int i = 0; i < progressPojoArray.size(); i++) {
				WeeklyModuleLevelTestExecutionProgress moduleProgress = new WeeklyModuleLevelTestExecutionProgress();


				if (progressPojoArray.get(i).getName().contains(moduleProgress.getNumberOfManualTestCasesExecutedForTheWeekName())) {
					StringTokenizer tokenizer = new StringTokenizer(progressPojoArray.get(i).getName(), "||");
					String moduleName = tokenizer.nextToken();

					boolean moduleFound = false;

					Iterable<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgressIterable = weeklyModuleLevelTestExecutionProgressRepository.findAll();
					Iterator<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgressIterator = weeklyModuleLevelTestExecutionProgressIterable.iterator();

					while (weeklyModuleLevelTestExecutionProgressIterator.hasNext()) {

						moduleProgress = weeklyModuleLevelTestExecutionProgressIterator.next();

						if ((moduleProgress.getProject().getId() == id) &&
								(moduleProgress.getWeekNumber() == weekNumber) &&
								(moduleProgress.getModuleName().compareTo(moduleName) == 0) &&
								(moduleProgress.getYearNumber() == yearNumber)) {
							moduleFound = true;
							break;
						}

					}

					if (moduleFound == false) {
						moduleProgress = new WeeklyModuleLevelTestExecutionProgress();
						moduleProgress.setModuleName(moduleName);
						moduleProgress.setWeekNumber(weekNumber);
						moduleProgress.setMonthNumber(monthNumber);
						moduleProgress.setYearNumber(yearNumber);

						Iterable<Project> allProjects = projectRepository.findAll();
						Iterator<Project> iterator = allProjects.iterator();


						while (iterator.hasNext()) {
							Project nextProject = iterator.next();
							if (nextProject.getId() == id) {
								moduleProgress.setProject(nextProject);
								break;
							}
						}
					}



					moduleProgress.setNumberOfManualTestCasesExecutedForTheWeek(progressPojoArray.get(i).getValue());
					weeklyModuleLevelTestExecutionProgressRepository.save(moduleProgress);



				} else if (progressPojoArray.get(i).getName().contains(moduleProgress.getNumberOfRegressionAutomationTestCasesExecutedForTheWeekName())) {
					StringTokenizer tokenizer = new StringTokenizer(progressPojoArray.get(i).getName(), "||");
					String moduleName = tokenizer.nextToken();

					boolean moduleFound = false;

					Iterable<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgressIterable = weeklyModuleLevelTestExecutionProgressRepository.findAll();
					Iterator<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgressIterator = weeklyModuleLevelTestExecutionProgressIterable.iterator();

					while (weeklyModuleLevelTestExecutionProgressIterator.hasNext()) {

						moduleProgress = weeklyModuleLevelTestExecutionProgressIterator.next();

						if ((moduleProgress.getProject().getId() == id) &&
								(moduleProgress.getWeekNumber() == weekNumber) &&
								(moduleProgress.getModuleName().compareTo(moduleName) == 0) &&
								(moduleProgress.getYearNumber() == yearNumber)) {
							moduleFound = true;
							break;
						}

					}

					if (moduleFound == false) {
						moduleProgress = new WeeklyModuleLevelTestExecutionProgress();
						moduleProgress.setModuleName(moduleName);
						moduleProgress.setWeekNumber(weekNumber);
						moduleProgress.setMonthNumber(monthNumber);
						moduleProgress.setYearNumber(yearNumber);

						Iterable<Project> allProjects = projectRepository.findAll();
						Iterator<Project> iterator = allProjects.iterator();


						while (iterator.hasNext()) {
							Project nextProject = iterator.next();
							if (nextProject.getId() == id) {
								moduleProgress.setProject(nextProject);
								break;
							}
						}
					}



					moduleProgress.setNumberOfRegressionAutomationTestCasesExecutedForTheWeek(progressPojoArray.get(i).getValue());
					weeklyModuleLevelTestExecutionProgressRepository.save(moduleProgress);



				} else if (progressPojoArray.get(i).getName().contains(moduleProgress.getDefectsForTheWeekName())) {
					StringTokenizer tokenizer = new StringTokenizer(progressPojoArray.get(i).getName(), "||");
					String moduleName = tokenizer.nextToken();

					boolean moduleFound = false;

					Iterable<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgressIterable = weeklyModuleLevelTestExecutionProgressRepository.findAll();
					Iterator<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgressIterator = weeklyModuleLevelTestExecutionProgressIterable.iterator();

					while (weeklyModuleLevelTestExecutionProgressIterator.hasNext()) {

						moduleProgress = weeklyModuleLevelTestExecutionProgressIterator.next();

						if ((moduleProgress.getProject().getId() == id) &&
								(moduleProgress.getWeekNumber() == weekNumber) &&
								(moduleProgress.getModuleName().compareTo(moduleName) == 0) &&
								(moduleProgress.getYearNumber() == yearNumber)) {
							moduleFound = true;
							break;
						}

					}

					if (moduleFound == false) {
						moduleProgress = new WeeklyModuleLevelTestExecutionProgress();
						moduleProgress.setModuleName(moduleName);
						moduleProgress.setWeekNumber(weekNumber);
						moduleProgress.setMonthNumber(monthNumber);
						moduleProgress.setYearNumber(yearNumber);


						Iterable<Project> allProjects = projectRepository.findAll();
						Iterator<Project> iterator = allProjects.iterator();


						while (iterator.hasNext()) {
							Project nextProject = iterator.next();
							if (nextProject.getId() == id) {
								moduleProgress.setProject(nextProject);
								break;
							}
						}
					}


					moduleProgress.setDefectsForTheWeek(progressPojoArray.get(i).getValue());
					weeklyModuleLevelTestExecutionProgressRepository.save(moduleProgress);
				}
			}

		} catch (Exception e) {
			System.out.println("Exception occured during module progress update : " + e.getMessage());
		}
		return "OK";
	}
}

