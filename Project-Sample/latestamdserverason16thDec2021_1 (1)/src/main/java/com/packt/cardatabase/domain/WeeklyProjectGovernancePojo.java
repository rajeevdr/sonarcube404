package com.packt.cardatabase.domain;

public class WeeklyProjectGovernancePojo {


    private long id;

    private String numberOfFormalGovernanceMeetings;
    private String rCAPoorRequirement;
    private String rCALateRequirement;
    private String rCADesignError;
    private String rCACodingError;
    private String rCATestData;
    private String rCATestEnvironmentConfiguration;
    private String rCAProcessRelated;
    private String rCAInsufficientTesting;
    private String rCAOthers;



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }



    public WeeklyProjectGovernancePojo() {
    }

    public String getnumberOfFormalGovernanceMeetings() {
        return numberOfFormalGovernanceMeetings;
    }

    public void setnumberOfFormalGovernanceMeetings(String numberOfFormalGovernanceMeetings) {
        this.numberOfFormalGovernanceMeetings = numberOfFormalGovernanceMeetings;
    }

    public String getRCAPoorRequirement() {
        return rCAPoorRequirement;
    }

    public void setRCAPoorRequirement(String rCAPoorRequirement) {
        this.rCAPoorRequirement = rCAPoorRequirement;
    }

    public String getRCALateRequirement() {
        return rCALateRequirement;
    }

    public void setRCALateRequirement(String rCALateRequirement) {
        this.rCALateRequirement = rCALateRequirement;
    }

    public String getRCADesignError() {
        return rCADesignError;
    }

    public void setRCADesignError(String rCADesignError) {
        this.rCADesignError = rCADesignError;
    }

    public String getRCACodingError() {
        return rCACodingError;
    }

    public void setRCACodingError(String rCACodingError) {
        this.rCACodingError = rCACodingError;
    }

    public String getRCATestData() {
        return rCATestData;
    }

    public void setRCATestData(String rCATestData) {
        this.rCATestData = rCATestData;
    }

    public String getRCATestEnvironmentConfiguration() {
        return rCATestEnvironmentConfiguration;
    }

    public void setRCATestEnvironmentConfiguration(String rCATestEnvironmentConfiguration) {
        this.rCATestEnvironmentConfiguration = rCATestEnvironmentConfiguration;
    }

    public String getRCAProcessRelated() {
        return rCAProcessRelated;
    }

    public void setRCAProcessRelated(String rCAProcessRelated) {
        this.rCAProcessRelated = rCAProcessRelated;
    }

    public String getRCAInsufficientTesting() {
        return rCAInsufficientTesting;
    }

    public void setRCAInsufficientTesting(String rCAInsufficientTesting) {
        this.rCAInsufficientTesting = rCAInsufficientTesting;
    }

    public String getRCAOthers() {
        return rCAOthers;
    }

    public void setRCAOthers(String rCAOthers) {
        this.rCAOthers = rCAOthers;
    }

}