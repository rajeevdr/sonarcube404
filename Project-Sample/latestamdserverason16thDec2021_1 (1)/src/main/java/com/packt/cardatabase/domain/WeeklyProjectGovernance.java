package com.packt.cardatabase.domain;

import javax.persistence.*;

@Entity
public class WeeklyProjectGovernance {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private String numberOfFormalGovernanceMeetings;
    private String rCAPoorRequirement;
    private String rCALateRequirement;
    private String rCADesignError;
    private String rCACodingError;
    private String rCATestData;
    private String rCATestEnvironmentConfiguration;
    private String rCAProcessRelated;
    private String rCAInsufficientTesting;
    private String rCAOthers;
    private int weekNumber;
    private int monthNumber;
    private int yearNumber;



    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;


    public WeeklyProjectGovernance() {
    }

    public WeeklyProjectGovernance(String numberOfFormalGovernanceMeetings,
                                   String rCAPoorRequirement,
                                   String rCALateRequirement,
                                   String rCADesignError,
                                   String rCACodingError,
                                   String rCATestData,
                                   String rCATestEnvironmentConfiguration,
                                   String rCAProcessRelated,
                                   String rCAInsufficientTesting,
                                   String rCAOthers,
                                   int weekNumber,
                                   int monthNumber,
                                   int yearNumber,
                                   Project project) {

        super();
        this.numberOfFormalGovernanceMeetings = numberOfFormalGovernanceMeetings;
        this.rCAPoorRequirement = rCAPoorRequirement;
        this.rCALateRequirement = rCALateRequirement;
        this.rCADesignError = rCADesignError;
        this.rCACodingError = rCACodingError;
        this.rCATestData = rCATestData;
        this.rCATestEnvironmentConfiguration = rCATestEnvironmentConfiguration;
        this.rCAProcessRelated = rCAProcessRelated;
        this.rCAInsufficientTesting = rCAInsufficientTesting;
        this.rCAOthers = rCAOthers;
        this.weekNumber = weekNumber;
        this.monthNumber = monthNumber;
        this.yearNumber = yearNumber;
        this.project = project;
    }

    public int getWeekNumber() {
        return weekNumber;
    }
    public String getWeekNumberName() {
        return "Week";
    }
    public void setWeekNumber(int weekNumber) {
        this.weekNumber = weekNumber;
    }

    public int getMonthNumber() {
        return monthNumber;
    }
    public String getMonthNumberName() {
        return "Month";
    }
    public void setMonthNumber(int monthNumber) {
        this.monthNumber = monthNumber;
    }

    public int getYearNumber() {
        return yearNumber;
    }
    public String getYearNumberName() {
        return "Year";
    }
    public void setYearNumber(int yearNumber) {
        this.yearNumber = yearNumber;
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getNumberOfFormalGovernanceMeetingsName() {
        return "Number Of Formal Governance Meetings";
    }

    public int getNumberOfFormalGovernanceMeetings() {

        if(numberOfFormalGovernanceMeetings == null)
            return 0;
        return Integer.parseInt(numberOfFormalGovernanceMeetings);
    }

    public void setNumberOfFormalGovernanceMeetings(String numberOfFormalGovernanceMeetings) {
        this.numberOfFormalGovernanceMeetings = numberOfFormalGovernanceMeetings;
    }

    public int getRCAPoorRequirement() {
        if(rCAPoorRequirement == null)
            return 0;
        return Integer.parseInt(rCAPoorRequirement);
    }

    public String getRCAPoorRequirementName() {
        return "RCA Poor Requirement";
    }

    public void setRCAPoorRequirement(String rCAPoorRequirement) {
        this.rCAPoorRequirement = rCAPoorRequirement;
    }

    public int getRCALateRequirement() {

        if(rCALateRequirement == null)
            return 0;
        return Integer.parseInt(rCALateRequirement);

    }

    public String getRCALateRequirementName() {
        return "RCA Late Requirement";
    }

    public void setRCALateRequirement(String rCALateRequirement) {
        this.rCALateRequirement = rCALateRequirement;
    }

    public int getRCADesignError() {

        if(rCADesignError == null)
            return 0;
        return Integer.parseInt(rCADesignError);

    }

    public String getRCADesignErrorName() {
        return "RCA Design Error";
    }

    public void setRCADesignError(String rCADesignError) {
        this.rCADesignError = rCADesignError;
    }

    public int getRCACodingError() {

        if(rCACodingError == null)
            return 0;
        return Integer.parseInt(rCACodingError);

    }

    public String getRCACodingErrorName() {
        return "RCA Coding Error";
    }

    public void setRCACodingError(String rCACodingError) {
        this.rCACodingError = rCACodingError;
    }

    public int getRCATestData() {

        if(rCATestData == null)
            return 0;
        return Integer.parseInt(rCATestData);

    }

    public String getRCATestDataName() {
        return "RCA Test Data";
    }

    public void setRCATestData(String rCATestData) {
        this.rCATestData = rCATestData;
    }

    public int getRCATestEnvironmentConfiguration() {

        if(rCATestEnvironmentConfiguration == null)
            return 0;
        return Integer.parseInt(rCATestEnvironmentConfiguration);

    }

    public String getRCATestEnvironmentConfigurationName() {
        return "RCA Test Environment Configuration";
    }

    public void setRCATestEnvironmentConfiguration(String rCATestEnvironmentConfiguration) {
        this.rCATestEnvironmentConfiguration = rCATestEnvironmentConfiguration;
    }

    public int getRCAProcessRelated() {
        if(rCAProcessRelated == null)
            return 0;
        return Integer.parseInt(rCAProcessRelated);

    }

    public String getRCAProcessRelatedName() {
        return "RCA Process Related";
    }

    public void setRCAProcessRelated(String rCAProcessRelated) {
        this.rCAProcessRelated = rCAProcessRelated;
    }

    public int getRCAInsufficientTesting() {

        if(rCAInsufficientTesting == null)
            return 0;
        return Integer.parseInt(rCAInsufficientTesting);

    }

    public String getRCAInsufficientTestingName() {
        return "RCA Insufficient Testing";
    }

    public void setRCAInsufficientTesting(String rCAInsufficientTesting) {
        this.rCAInsufficientTesting = rCAInsufficientTesting;
    }

    public int getRCAOthers() {

        if(rCAOthers == null)
            return 0;
        return Integer.parseInt(rCAOthers);

    }

    public String getRCAOthersName() {
        return "RCA Others";
    }

    public void setRCAOthers(String rCAOthers) {
        this.rCAOthers = rCAOthers;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

}