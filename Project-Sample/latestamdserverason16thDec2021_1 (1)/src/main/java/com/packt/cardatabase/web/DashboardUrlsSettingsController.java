package com.packt.cardatabase.web;

import com.google.gson.Gson;
import com.packt.cardatabase.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;


@RestController
public class DashboardUrlsSettingsController {

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private DashboardUrlsRepository dashboardUrlsRepository;


	@RequestMapping(value="/projects/{id}/dashboardUrls", method=RequestMethod.GET)
	@ResponseBody
	public String getDashboardUrls(@PathVariable("id") long id) {

		Iterable<DashboardUrls> dashboardUrlsIterable = dashboardUrlsRepository.findAll();
		Iterator<DashboardUrls> dashboardUrlsIterator = dashboardUrlsIterable.iterator();

		String response = null;

		ArrayList<DashboardUrlsPojo> dashboardUrls = new ArrayList<DashboardUrlsPojo>();

		int i = 1;

		try {

			while (dashboardUrlsIterator.hasNext()) {

				DashboardUrls next = dashboardUrlsIterator.next();
				if (next.getProject().getId() == id) {

					DashboardUrlsPojo temp = new DashboardUrlsPojo(next);
					temp.setId(i++);
					dashboardUrls.add(temp);
				}

			}


			Gson gson = new Gson();
			response = gson.toJson(dashboardUrls);
			response = "{\"DashboardUrls\":" + response + "}";
			System.out.println(response);

			return response;


		}catch(Exception e){
			System.out.println("Exception occurred while fetching Dashboard URLs : " + e.getMessage());
		}


        return "Error";

	}

	@PostMapping(value = "/projects/{id}/dashboardUrls")
	@ResponseBody
	public String updateDashboardUrls(@PathVariable("id") long id, @RequestBody ArrayList<DashboardUrlsPojo> dashboardUrlsList) {

		Gson gson = new Gson();
		System.out.println(gson.toJson(dashboardUrlsList));

		Iterable<Project> allProjects = projectRepository.findAll();
		Iterator<Project> iterator = allProjects.iterator();
		Project nextProject = null;

		while (iterator.hasNext()) {
			nextProject = iterator.next();
			if (nextProject.getId() == id) {
				break;
			}
		}

		Iterable<DashboardUrls> dashboardUrlsIterable = dashboardUrlsRepository.findAll();
		Iterator<DashboardUrls> dashboardUrlsIterator = dashboardUrlsIterable.iterator();

		try {
			while (dashboardUrlsIterator.hasNext()) {
				DashboardUrls next = dashboardUrlsIterator.next();

				if (next.getProject().getId() == id) {
					dashboardUrlsRepository.delete(next);
				}

			}
		}catch(Exception e) {
		}




		for(int i = 0; i < dashboardUrlsList.size(); i++) {
			DashboardUrls dashboardUrls = new DashboardUrls(dashboardUrlsList.get(i));
			dashboardUrls.setProject(nextProject);
			dashboardUrlsRepository.save(dashboardUrls);
		}

		return "OK";
	}

}