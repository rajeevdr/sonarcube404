package com.packt.cardatabase.domain;

import java.util.ArrayList;

public class AlmInProjectDetailsPojo {

	private long id;


	private ArrayList<String> testExecutionFolderId = null;
	private ArrayList<String> testDesignFolderId = null;

	public Long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ArrayList<String> getTestExecutionFolderId() {
		return testExecutionFolderId;
	}

	public void setTestExecutionFolderId(ArrayList<String> testExecutionFolderId) {
		this.testExecutionFolderId = testExecutionFolderId;
	}

	public ArrayList<String> getTestDesignFolderId() {
		return testDesignFolderId;
	}

	public void setTestDesignFolderId(ArrayList<String> testDesignFolderId) {
		this.testDesignFolderId = testDesignFolderId;
	}



}