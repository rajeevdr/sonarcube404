package com.packt.cardatabase.domain;

import java.util.ArrayList;

public class RequirementsProjectFolders {


	private String projectFieldName;
	private ArrayList<String> projectName = null;

	public String getProjectFieldName() {
		return projectFieldName;
	}

	public void setProjectFieldName(String projectFieldName) {
		this.projectFieldName = projectFieldName;
	}

	public ArrayList<String> getProjectName() {
		return projectName;
	}

	public void setProjectName(ArrayList<String> projectName) {
		this.projectName = projectName;
	}



}