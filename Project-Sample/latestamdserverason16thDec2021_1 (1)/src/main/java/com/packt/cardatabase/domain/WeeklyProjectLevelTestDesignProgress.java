package com.packt.cardatabase.domain;


import javax.persistence.*;

@Entity
public class WeeklyProjectLevelTestDesignProgress {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private String numberOfTestableRequirementsCoveredThisWeek;
    private String totalNumberOfInProjectAutomationTestCasesAutomated;
    private String totalNumberOfAPIAutomationTestCasesAutomated;
    private String totalNumberOfTestDesignReviewDefectsFound;
    private String testPlanningEffort;
    private String manualTestCasesDesignEffort;
    private String regressionAutomationDesignEffort;
    private String apiAutomationDesignEffort;
    private String inProjectAutomationDesignEffort;
    private String manualTestCasesMaintenanceEffort;
    private String automationTestCasesMaintenanceEffort;
    private String environmentDowntimeDuringTestDesignPhase;
    private String numberOfResourcesAuthoringManualTestCases;
    private String numberOfResourcesAuthoringAutomationTestCases;
    private String testPlanningEffortStartDate;
    private String manualTestCasesDesignEffortStartDate;
    private String regressionAutomationDesignEffortStartDate;
    private String apiAutomationDesignEffortStartDate;
    private String inProjectAutomationDesignEffortStartDate;
    private String manualTestCasesMaintenanceEffortStartDate;
    private String automationTestCasesMaintenanceEffortStartDate;
    private String testPlanningEffortEndDate;
    private String manualTestCasesDesignEffortEndDate;
    private String regressionAutomationDesignEffortEndDate;
    private String apiAutomationDesignEffortEndDate;
    private String inProjectAutomationDesignEffortEndDate;
    private String manualTestCasesMaintenanceEffortEndDate;
    private String automationTestCasesMaintenanceEffortEndDate;

    private int weekNumber;
    private int monthNumber;
    private int yearNumber;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public WeeklyProjectLevelTestDesignProgress() {}


    public WeeklyProjectLevelTestDesignProgress(String numberOfTestableRequirementsCoveredThisWeek,
                                                String totalNumberOfInProjectAutomationTestCasesAutomated,
                                                String totalNumberOfAPIAutomationTestCasesAutomated,
                                                String totalNumberOfTestDesignReviewDefectsFound,
                                                String testPlanningEffort,
                                                String manualTestCasesDesignEffort,
                                                String regressionAutomationDesignEffort,
                                                String inProjectAutomationDesignEffort,
                                                String apiAutomationDesignEffort,
                                                String manualTestCasesMaintenanceEffort,
                                                String automationTestCasesMaintenanceEffort,
                                                String environmentDowntimeDuringTestDesignPhase,
                                                String numberOfResourcesAuthoringManualTestCases,
                                                String numberOfResourcesAuthoringAutomationTestCases,
                                                int weekNumber,
                                                int monthNumber,
                                                int yearNumber,
                                                Project project) {


        super();
        this.numberOfTestableRequirementsCoveredThisWeek = numberOfTestableRequirementsCoveredThisWeek;
        this.totalNumberOfInProjectAutomationTestCasesAutomated = totalNumberOfInProjectAutomationTestCasesAutomated;
        this.totalNumberOfAPIAutomationTestCasesAutomated = totalNumberOfAPIAutomationTestCasesAutomated;
        this.totalNumberOfTestDesignReviewDefectsFound = totalNumberOfTestDesignReviewDefectsFound;
        this.testPlanningEffort = testPlanningEffort;
        this.manualTestCasesDesignEffort = manualTestCasesDesignEffort;
        this.regressionAutomationDesignEffort = regressionAutomationDesignEffort;
        this.inProjectAutomationDesignEffort = inProjectAutomationDesignEffort;
        this.apiAutomationDesignEffort = apiAutomationDesignEffort;
        this.manualTestCasesMaintenanceEffort = manualTestCasesMaintenanceEffort;
        this.automationTestCasesMaintenanceEffort = automationTestCasesMaintenanceEffort;
        this.environmentDowntimeDuringTestDesignPhase = environmentDowntimeDuringTestDesignPhase;
        this.numberOfResourcesAuthoringManualTestCases = numberOfResourcesAuthoringManualTestCases;
        this.numberOfResourcesAuthoringAutomationTestCases = numberOfResourcesAuthoringAutomationTestCases;
        this.weekNumber = weekNumber;
        this.monthNumber = monthNumber;
        this.yearNumber = yearNumber;
        this.project = project;

    }


    public int getNumberOfTestableRequirementsCoveredThisWeek(){
        if(numberOfTestableRequirementsCoveredThisWeek == null)
        {
            return 0;
        }

        return Integer.parseInt(numberOfTestableRequirementsCoveredThisWeek);

    }
    public void setNumberOfTestableRequirementsCoveredThisWeek(String numberOfTestableRequirementsCoveredThisWeek){
        this.numberOfTestableRequirementsCoveredThisWeek =  numberOfTestableRequirementsCoveredThisWeek;
    }


    public int getMonthNumber() {
        return monthNumber;
    }
    public String getMonthNumberName() {
        return "Month";
    }
    public void setMonthNumber(int monthNumber) {
        this.monthNumber = monthNumber;
    }

    public int getYearNumber() {
        return yearNumber;
    }
    public String getYearNumberName() {
        return "YearNumber";
    }
    public void setYearNumber(int yearNumber) {
        this.yearNumber = yearNumber;
    }


    public int getTotalNumberOfTestDesignReviewDefectsFound(){

        if(totalNumberOfTestDesignReviewDefectsFound == null)
        {
            return 0;
        }

        return Integer.parseInt(totalNumberOfTestDesignReviewDefectsFound);

    }


    public int getTotalNumberOfInProjectAutomationTestCasesAutomated(){

        if(totalNumberOfInProjectAutomationTestCasesAutomated == null)
        {
            return 0;
        }

        return Integer.parseInt(totalNumberOfInProjectAutomationTestCasesAutomated);

    }

    public int getTotalNumberOfAPIAutomationTestCasesAutomated(){

        if(totalNumberOfAPIAutomationTestCasesAutomated == null)
        {
            return 0;
        }

        return Integer.parseInt(totalNumberOfAPIAutomationTestCasesAutomated);

    }

    public int getTestPlanningEffort(){

        if(testPlanningEffort == null)
        {
            return 0;
        }

        return Integer.parseInt(testPlanningEffort);

    }

    public int getManualTestCasesDesignEffort(){

        if(manualTestCasesDesignEffort == null)
        {
            return 0;
        }

        return Integer.parseInt(manualTestCasesDesignEffort);

    }

    public int getRegressionAutomationDesignEffort(){

        if(regressionAutomationDesignEffort == null)
        {
            return 0;
        }

        return Integer.parseInt(regressionAutomationDesignEffort);

    }


    public int getApiAutomationDesignEffort(){

        if(apiAutomationDesignEffort == null)
        {
            return 0;
        }

        return Integer.parseInt(apiAutomationDesignEffort);

    }

    public int getInProjectAutomationDesignEffort(){

        if(inProjectAutomationDesignEffort == null)
        {
            return 0;
        }

        return Integer.parseInt(inProjectAutomationDesignEffort);

    }

    public int getManualTestCasesMaintenanceEffort(){

        if(manualTestCasesMaintenanceEffort == null)
        {
            return 0;
        }

        return Integer.parseInt(manualTestCasesMaintenanceEffort);

    }

    public int getAutomationTestCasesMaintenanceEffort(){

        if(automationTestCasesMaintenanceEffort == null)
        {
            return 0;
        }

        return Integer.parseInt(automationTestCasesMaintenanceEffort);

    }

    public int getEnvironmentDowntimeDuringTestDesignPhase(){

        if(environmentDowntimeDuringTestDesignPhase == null)
        {
            return 0;
        }

        return Integer.parseInt(environmentDowntimeDuringTestDesignPhase);

    }

    public int getNumberOfResourcesAuthoringManualTestCases(){

        if(numberOfResourcesAuthoringManualTestCases == null)
        {
            return 0;
        }

        return Integer.parseInt(numberOfResourcesAuthoringManualTestCases);

    }

    public int getNumberOfResourcesAuthoringAutomationTestCases(){

        if(numberOfResourcesAuthoringAutomationTestCases == null)
        {
            return 0;
        }

        return Integer.parseInt(numberOfResourcesAuthoringAutomationTestCases);

    }

    public int getWeekNumber(){
        return this.weekNumber =  weekNumber;
    }


    public void setTotalNumberOfInProjectAutomationTestCasesAutomated(String totalNumberOfInProjectAutomationTestCasesAutomated){
        this.totalNumberOfInProjectAutomationTestCasesAutomated =  totalNumberOfInProjectAutomationTestCasesAutomated;
    }

    public void setTotalNumberOfAPIAutomationTestCasesAutomated(String totalNumberOfAPIAutomationTestCasesAutomated){
        this.totalNumberOfAPIAutomationTestCasesAutomated =  totalNumberOfAPIAutomationTestCasesAutomated;
    }


    public void setTotalNumberOfTestDesignReviewDefectsFound(String totalNumberOfTestDesignReviewDefectsFound){
        this.totalNumberOfTestDesignReviewDefectsFound =  totalNumberOfTestDesignReviewDefectsFound;
    }

    public void setTestPlanningEffort(String testPlanningEffort){
        this.testPlanningEffort =  testPlanningEffort;
    }

    public void setManualTestCasesDesignEffort(String manualTestCasesDesignEffort){
        this.manualTestCasesDesignEffort =  manualTestCasesDesignEffort;
    }


    public void setRegressionAutomationDesignEffort(String regressionAutomationDesignEffort){
        this.regressionAutomationDesignEffort =  regressionAutomationDesignEffort;
    }
    
    public void setApiAutomationDesignEffort(String apiAutomationDesignEffort){
        this.apiAutomationDesignEffort =  apiAutomationDesignEffort;
    }

    public void setInProjectAutomationDesignEffort(String inProjectAutomationDesignEffort){
        this.inProjectAutomationDesignEffort =  inProjectAutomationDesignEffort;
    }


    public void setManualTestCasesMaintenanceEffort(String manualTestCasesMaintenanceEffort){
        this.manualTestCasesMaintenanceEffort =  manualTestCasesMaintenanceEffort;
    }

    public void setAutomationTestCasesMaintenanceEffort(String automationTestCasesMaintenanceEffort){
        this.automationTestCasesMaintenanceEffort =  automationTestCasesMaintenanceEffort;
    }

    public void setEnvironmentDowntimeDuringTestDesignPhase(String environmentDowntimeDuringTestDesignPhase){
        this.environmentDowntimeDuringTestDesignPhase =  environmentDowntimeDuringTestDesignPhase;
    }

    public void setNumberOfResourcesAuthoringManualTestCases(String numberOfResourcesAuthoringManualTestCases){
        this.numberOfResourcesAuthoringManualTestCases =  numberOfResourcesAuthoringManualTestCases;
    }

    public void setNumberOfResourcesAuthoringAutomationTestCases(String numberOfResourcesAuthoringAutomationTestCases){
        this.numberOfResourcesAuthoringAutomationTestCases =  numberOfResourcesAuthoringAutomationTestCases;
    }


    public void setWeekNumber(int weekNumber){
        this.weekNumber =  weekNumber;
    }



    public String getNumberOfTestableRequirementsCoveredThisWeekName(){
        return "Number Of Testable Requirements Covered This Week";
    }

    public String getTotalNumberOfInProjectAutomationTestCasesAutomatedName(){
        return "Total Number Of InProject Automation TestCases Automated";
    }

    public String getTotalNumberOfAPIAutomationTestCasesAutomatedName(){
        return "Total Number Of API Automation TestCases Automated";
    }


    public String getTotalNumberOfTestDesignReviewDefectsFoundName(){
        return "Total Number Of Test Design Review Defects Found";
    }

    public String getTestPlanningEffortName(){
        return "Test Planning Effort";
    }

    public String getManualTestCasesDesignEffortName(){
        return "Manual TestCases Design Effort In Person Days";
    }


    public String getRegressionAutomationDesignEffortName(){
        return "Regression Automation Design Effort In Person Days";
    }


    public String getApiAutomationDesignEffortName(){
        return "Api Automation Design Effort In Person Days";
    }

    public String getInProjectAutomationDesignEffortName(){
        return "InProject Automation Design Effort In Person days";
    }


    public String getManualTestCasesMaintenanceEffortName(){
        return "Manual TestCases Maintenance Effort";
    }

    public String getAutomationTestCasesMaintenanceEffortName(){
        return "Automation TestCases Maintenance Effort";
    }

    public String getEnvironmentDowntimeDuringTestDesignPhaseName(){
        return "Environment Downtime During TestDesign Phase";
    }

    public String getNumberOfResourcesAuthoringManualTestCasesName(){
        return "Number Of Resources Authoring Manual TestCases";
    }

    public String getNumberOfResourcesAuthoringAutomationTestCasesName(){
        return "Number Of Resources Authoring Automation TestCases";
    }



    public String getTestPlanningEffortStartDate() {
        if(testPlanningEffortStartDate == null)
            return "";
        else
            return testPlanningEffortStartDate;
    }

    public void setTestPlanningEffortStartDate(String testPlanningEffortStartDate) {
        this.testPlanningEffortStartDate = testPlanningEffortStartDate;
    }

    public String getManualTestCasesDesignEffortStartDate() {
        if(manualTestCasesDesignEffortStartDate == null)
            return "";
        else
            return manualTestCasesDesignEffortStartDate;
    }

    public void setManualTestCasesDesignEffortStartDate(String manualTestCasesDesignEffortStartDate) {
        this.manualTestCasesDesignEffortStartDate = manualTestCasesDesignEffortStartDate;
    }

    public String getRegressionAutomationDesignEffortStartDate() {
        if(regressionAutomationDesignEffortStartDate == null)
            return "";
        else
            return regressionAutomationDesignEffortStartDate;
    }

    public void setRegressionAutomationDesignEffortStartDate(String regressionAutomationDesignEffortStartDate) {
        this.regressionAutomationDesignEffortStartDate = regressionAutomationDesignEffortStartDate;
    }

    public String getApiAutomationDesignEffortStartDate() {
        if(apiAutomationDesignEffortStartDate == null)
            return "";
        else
            return apiAutomationDesignEffortStartDate;
    }

    public void setApiAutomationDesignEffortStartDate(String apiAutomationDesignEffortStartDate) {
        this.apiAutomationDesignEffortStartDate = apiAutomationDesignEffortStartDate;
    }

    public String getInProjectAutomationDesignEffortStartDate() {
        if(inProjectAutomationDesignEffortStartDate == null)
            return "";
        else
            return inProjectAutomationDesignEffortStartDate;
    }

    public void setInProjectAutomationDesignEffortStartDate(String inProjectAutomationDesignEffortStartDate) {
        this.inProjectAutomationDesignEffortStartDate = inProjectAutomationDesignEffortStartDate;
    }

    public String getManualTestCasesMaintenanceEffortStartDate() {
        if(manualTestCasesMaintenanceEffortStartDate == null)
            return "";
        else
            return manualTestCasesMaintenanceEffortStartDate;
    }

    public void setManualTestCasesMaintenanceEffortStartDate(String manualTestCasesMaintenanceEffortStartDate) {
        this.manualTestCasesMaintenanceEffortStartDate = manualTestCasesMaintenanceEffortStartDate;
    }

    public String getAutomationTestCasesMaintenanceEffortStartDate() {
        if(automationTestCasesMaintenanceEffortStartDate == null)
            return "";
        else
            return automationTestCasesMaintenanceEffortStartDate;
    }

    public void setAutomationTestCasesMaintenanceEffortStartDate(String automationTestCasesMaintenanceEffortStartDate) {
        this.automationTestCasesMaintenanceEffortStartDate = automationTestCasesMaintenanceEffortStartDate;
    }

    public String getTestPlanningEffortEndDate() {
        if(testPlanningEffortEndDate == null)
            return "";
        else
            return testPlanningEffortEndDate;
    }

    public void setTestPlanningEffortEndDate(String testPlanningEffortEndDate) {
        this.testPlanningEffortEndDate = testPlanningEffortEndDate;
    }

    public String getManualTestCasesDesignEffortEndDate() {
        if(manualTestCasesDesignEffortEndDate == null)
            return "";
        else
            return manualTestCasesDesignEffortEndDate;
    }

    public void setManualTestCasesDesignEffortEndDate(String manualTestCasesDesignEffortEndDate) {
        this.manualTestCasesDesignEffortEndDate = manualTestCasesDesignEffortEndDate;
    }

    public String getRegressionAutomationDesignEffortEndDate() {
        if(regressionAutomationDesignEffortEndDate == null)
            return "";
        else
            return regressionAutomationDesignEffortEndDate;
    }

    public void setRegressionAutomationDesignEffortEndDate(String regressionAutomationDesignEffortEndDate) {
        this.regressionAutomationDesignEffortEndDate = regressionAutomationDesignEffortEndDate;
    }

    public String getApiAutomationDesignEffortEndDate() {

        if(apiAutomationDesignEffortEndDate == null)
            return "";
        else
            return apiAutomationDesignEffortEndDate;

    }

    public void setApiAutomationDesignEffortEndDate(String apiAutomationDesignEffortEndDate) {
        this.apiAutomationDesignEffortEndDate = apiAutomationDesignEffortEndDate;
    }

    public String getInProjectAutomationDesignEffortEndDate() {

        if(inProjectAutomationDesignEffortEndDate == null)
            return "";
        else
            return inProjectAutomationDesignEffortEndDate;

    }

    public void setInProjectAutomationDesignEffortEndDate(String inProjectAutomationDesignEffortEndDate) {
        this.inProjectAutomationDesignEffortEndDate = inProjectAutomationDesignEffortEndDate;
    }

    public String getManualTestCasesMaintenanceEffortEndDate() {

        if(manualTestCasesMaintenanceEffortEndDate == null)
            return "";
        else
            return manualTestCasesMaintenanceEffortEndDate;

    }

    public void setManualTestCasesMaintenanceEffortEndDate(String manualTestCasesMaintenanceEffortEndDate) {
        this.manualTestCasesMaintenanceEffortEndDate = manualTestCasesMaintenanceEffortEndDate;
    }

    public String getAutomationTestCasesMaintenanceEffortEndDate() {
        if(automationTestCasesMaintenanceEffortEndDate == null)
            return "";
        else
            return automationTestCasesMaintenanceEffortEndDate;
    }

    public void setAutomationTestCasesMaintenanceEffortEndDate(String automationTestCasesMaintenanceEffortEndDate) {
        this.automationTestCasesMaintenanceEffortEndDate = automationTestCasesMaintenanceEffortEndDate;
    }

    public String getWeekNumberName(){
        return "Week";
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

}