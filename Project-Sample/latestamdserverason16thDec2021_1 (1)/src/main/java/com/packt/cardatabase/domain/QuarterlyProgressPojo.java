package com.packt.cardatabase.domain;


public class QuarterlyProgressPojo {


    private long id;

    private String totalQuarterlyBusinessReviewMeetings;
    private String projectedBillingForCurrentQuarter;
    private String actualBillingForCurrentQuarter;
    private String totalNumberOfCRsRaised;
    private String totalNumberOfCRsAccepted;
    private String totalNumberOfCRsInvoiced;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public QuarterlyProgressPojo() {}


    public String getTotalQuarterlyBusinessReviewMeetings() {
        return totalQuarterlyBusinessReviewMeetings;
    }

    public void setTotalQuarterlyBusinessReviewMeetings(String totalQuarterlyBusinessReviewMeetings) {
        this.totalQuarterlyBusinessReviewMeetings = totalQuarterlyBusinessReviewMeetings;
    }

    public String getProjectedBillingForCurrentQuarter() {
        return projectedBillingForCurrentQuarter;
    }

    public void setProjectedBillingForCurrentQuarter(String projectedBillingForCurrentQuarter) {
        this.projectedBillingForCurrentQuarter = projectedBillingForCurrentQuarter;
    }

    public String getActualBillingForCurrentQuarter() {
        return actualBillingForCurrentQuarter;
    }

    public void setActualBillingForCurrentQuarter(String actualBillingForCurrentQuarter) {
        this.actualBillingForCurrentQuarter = actualBillingForCurrentQuarter;
    }

    public String getTotalNumberOfCRsRaised() {
        return totalNumberOfCRsRaised;
    }

    public void setTotalNumberOfCRsRaised(String totalNumberOfCRsRaised) {
        this.totalNumberOfCRsRaised = totalNumberOfCRsRaised;
    }

    public String getTotalNumberOfCRsAccepted() {
        return totalNumberOfCRsAccepted;
    }

    public void setTotalNumberOfCRsAccepted(String totalNumberOfCRsAccepted) {
        this.totalNumberOfCRsAccepted = totalNumberOfCRsAccepted;
    }

    public String getTotalNumberOfCRsInvoiced() {
        return totalNumberOfCRsInvoiced;
    }

    public void setTotalNumberOfCRsInvoiced(String totalNumberOfCRsInvoiced) {
        this.totalNumberOfCRsInvoiced = totalNumberOfCRsInvoiced;
    }

}