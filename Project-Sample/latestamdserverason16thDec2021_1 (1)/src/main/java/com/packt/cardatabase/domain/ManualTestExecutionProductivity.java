package com.packt.cardatabase.domain;

import javax.persistence.*;
import java.util.ArrayList;

@Entity
public class ManualTestExecutionProductivity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private ArrayList<String> numberOfManualTestcasesExecutedEachWeek;
    private ArrayList<String> moduleNames;
    private String effortSpentOnManualTestcaseExecutionEachWeek;
    private String manualTestCaseExecutedProductivity;
    private Integer weekNumber;
    private Integer yearNumber;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public ManualTestExecutionProductivity() {}

    public ManualTestExecutionProductivity(ArrayList<String> numberOfManualTestcasesExecutedEachWeek,
                                           String effortSpentOnManualTestcaseExecutionEachWeek,
                                           int weekNumber,
                                           int yearNumber,
                                           Project project) {

        super();
        this.numberOfManualTestcasesExecutedEachWeek = numberOfManualTestcasesExecutedEachWeek;
        this.effortSpentOnManualTestcaseExecutionEachWeek = effortSpentOnManualTestcaseExecutionEachWeek;
        this.weekNumber = weekNumber;
        this.yearNumber = yearNumber;
        this.project = project;
    }

    public Integer getWeekNumber() {
        return weekNumber;
    }
    public String getWeekNumberName() {
        return "WeekNumber";
    }
    public void setWeekNumber(Integer weekNumber) {
        this.weekNumber = weekNumber;
    }

    public Integer getYearNumber() {
        return yearNumber;
    }
    public String getYearNumberName() {
        return "YearNumber";
    }
    public void setYearNumber(Integer yearNumber) {
        this.yearNumber = yearNumber;
    }


    public String getManualTestCaseExecutedProductivity() {
        return manualTestCaseExecutedProductivity;
    }
    public String getManualTestCaseExecutedProductivityName() {
        return "ManualTestCaseExecutedProductivity";
    }
    public void setManualTestCaseExecutedProductivity(String manualTestCaseExecutedProductivity) {
        this.manualTestCaseExecutedProductivity = manualTestCaseExecutedProductivity;
    }

    public ArrayList<String> getNumberOfManualTestcasesExecutedEachWeek(){
        return numberOfManualTestcasesExecutedEachWeek;
    }
    public String getNumberOfManualTestcasesExecutedEachWeekName(){
        return "NumberOfManualTestcasesExecutedEachWeek";
    }
    public void setNumberOfManualTestcasesExecutedEachWeek(ArrayList<String> numberOfManualTestcasesExecutedEachWeek){
        this.numberOfManualTestcasesExecutedEachWeek = numberOfManualTestcasesExecutedEachWeek;
    }

    public ArrayList<String> getModuleNames(){
        return moduleNames;
    }
    public String getModuleNamesName(){
        return "ModuleNames";
    }
    public void setModuleNames(ArrayList<String> moduleNames){
        this.moduleNames = moduleNames;
    }

    public String  getEffortSpentOnManualTestcaseExecutedEachWeek() {
        return effortSpentOnManualTestcaseExecutionEachWeek;
    }

    public String getEffortSpentOnManualTestcaseExecutionEachWeek(){
        return "EffortSpentOnManualTestcaseExecutionEachWeek";
    }

    public void setEffortSpentOnManualTestcaseExecutedEachWeek(String effortSpentOnManualTestcaseExecutionEachWeek) {
        this.effortSpentOnManualTestcaseExecutionEachWeek = effortSpentOnManualTestcaseExecutionEachWeek;
    }



    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

}