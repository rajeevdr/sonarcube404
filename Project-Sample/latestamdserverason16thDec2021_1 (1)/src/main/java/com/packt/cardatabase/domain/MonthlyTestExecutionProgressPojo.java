package com.packt.cardatabase.domain;


public class MonthlyTestExecutionProgressPojo {

    private long id;

    private String totalNumberOfManualUATTestCasesExecuted;
    private String sITServicesNotAvailableForTesting;
    private String totalNumberOfManualSITTestCasesExecuted;
    private String totalNumberOfDefectsLoggedInSIT;
    private String numberOfValidDefectsLoggedInSIT;
    private String totalNumberOfDefectsLoggedInUAT;
    private String numberOfValidDefectsLoggedInUAT;
    private String totalNumberOfValidAutomationDefects;
    private String totalNumberOfDefects;
    private String actualStartDateMilestone;
    private String actualEndDateMilestone;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public MonthlyTestExecutionProgressPojo() {
    }


    public String getTotalNumberOfManualUATTestCasesExecuted() {
        return totalNumberOfManualUATTestCasesExecuted;
    }

    public void setTotalNumberOfManualUATTestCasesExecuted(String totalNumberOfManualUATTestCasesExecuted) {
        this.totalNumberOfManualUATTestCasesExecuted = totalNumberOfManualUATTestCasesExecuted;
    }

    public String getSITServicesNotAvailableForTesting() {
        return sITServicesNotAvailableForTesting;
    }

    public void setSITServicesNotAvailableForTesting(String sITServicesNotAvailableForTesting) {
        this.sITServicesNotAvailableForTesting = sITServicesNotAvailableForTesting;
    }

    public String getTotalNumberOfManualSITTestCasesExecuted() {
        return totalNumberOfManualSITTestCasesExecuted;
    }

    public void setTotalNumberOfManualSITTestCasesExecuted(String totalNumberOfManualSITTestCasesExecuted) {
        this.totalNumberOfManualSITTestCasesExecuted = totalNumberOfManualSITTestCasesExecuted;
    }

    public String getTotalNumberOfDefectsLoggedInSIT() {
        return totalNumberOfDefectsLoggedInSIT;
    }

    public void setTotalNumberOfDefectsLoggedInSIT(String totalNumberOfDefectsLoggedInSIT) {
        this.totalNumberOfDefectsLoggedInSIT = totalNumberOfDefectsLoggedInSIT;
    }

    public String getNumberOfValidDefectsLoggedInSIT() {
        return numberOfValidDefectsLoggedInSIT;
    }

    public void setNumberOfValidDefectsLoggedInSIT(String numberOfValidDefectsLoggedInSIT) {
        this.numberOfValidDefectsLoggedInSIT = numberOfValidDefectsLoggedInSIT;
    }

    public String getTotalNumberOfDefectsLoggedInUAT() {
        return totalNumberOfDefectsLoggedInUAT;
    }

    public void setTotalNumberOfDefectsLoggedInUAT(String totalNumberOfDefectsLoggedInUAT) {
        this.totalNumberOfDefectsLoggedInUAT = totalNumberOfDefectsLoggedInUAT;
    }

    public String getNumberOfValidDefectsLoggedInUAT() {
        return numberOfValidDefectsLoggedInUAT;
    }

    public void setNumberOfValidDefectsLoggedInUAT(String numberOfValidDefectsLoggedInUAT) {
        this.numberOfValidDefectsLoggedInUAT = numberOfValidDefectsLoggedInUAT;
    }

    public String getTotalNumberOfValidAutomationDefects() {
        return totalNumberOfValidAutomationDefects;
    }

    public void setTotalNumberOfValidAutomationDefects(String totalNumberOfValidAutomationDefects) {
        this.totalNumberOfValidAutomationDefects = totalNumberOfValidAutomationDefects;
    }

    public String getTotalNumberOfDefects() {
        return totalNumberOfDefects;
    }

    public void setTotalNumberOfDefects(String totalNumberOfDefects) {
        this.totalNumberOfDefects = totalNumberOfDefects;
    }

    public String getActualStartDateMilestone() {
        return actualStartDateMilestone;
    }

    public String getActualStartDateMilestoneName() {
        return "ActualStartDateMilestone";
    }


    public void setActualStartDateMilestone(String actualStartDateMilestone) {
        this.actualStartDateMilestone = actualStartDateMilestone;
    }

    public String getActualEndDateMilestone() {
        return actualEndDateMilestone;
    }

    public String getActualEndDateMilestoneName() {
        return "ActualEndDateMilestone";
    }

    public void setActualEndDateMilestone(String actualEndDateMilestone) {
        this.actualEndDateMilestone = actualEndDateMilestone;
    }

}