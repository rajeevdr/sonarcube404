package com.packt.cardatabase.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.packt.cardatabase.service.UserService;

@RestController
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Bean
	BCryptPasswordEncoder passwordEncoder()
	{
		return new BCryptPasswordEncoder();
	}


	@PostMapping("/forgot-password")
	public ResponseEntity<String> forgotPassword(@RequestParam String username) {

		String token = userService.forgotPassword(username);
		System.out.println("the newly created token is : " + token);

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("Access-Control-Expose-Headers", "*");
		responseHeaders.set("X-token", token);

		return ResponseEntity.ok()
				.headers(responseHeaders)
				.body("check the token header for password reset token");

	}

	@PostMapping("/reset-password")
	public String resetPassword(@RequestParam String token,
								@RequestParam String password) {

		return userService.resetPassword(token, passwordEncoder.encode(password));
	}
}
