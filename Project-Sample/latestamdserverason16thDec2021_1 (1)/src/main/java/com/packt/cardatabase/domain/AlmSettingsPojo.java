package com.packt.cardatabase.domain;

import java.util.ArrayList;

public class AlmSettingsPojo {
	private String host;
	private String almUsername;
	private String almPassword;
	private String domain;
	private String almProject;

	private RequirementsProjectFolders requirementsProjectFolders;
	private Defects defects;

	private ArrayList<AlmApiDetailsPojo> almApiDetailsPojo = null;
	private ArrayList<AlmInProjectDetailsPojo> almInProjectDetailsPojo = null;
	private ArrayList<AlmSitDetailsPojo> almSitDetailsPojo = null;
	private ArrayList<AlmUatDetailsPojo> almUatDetailsPojo = null;
	private ArrayList<AlmModuleDetailsPojo> almModuleDetailsPojo = null;
	private ArrayList<MigrationModuleDetailsPojo> migrationModuleDetailsPojo = null;

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getAlmUsername() {
		return almUsername;
	}

	public void setAlmUsername(String almUsername) {
		this.almUsername = almUsername;
	}

	public String getAlmPassword() {
		return almPassword;
	}

	public void setAlmPassword(String almPassword) {
		this.almPassword = almPassword;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getAlmProject() {
		return almProject;
	}

	public void setAlmProject(String almProject) {
		this.almProject = almProject;
	}

	public RequirementsProjectFolders getRequirementsProjectFolders() {
		return requirementsProjectFolders;
	}

	public void setRequirementsProjectFolders(RequirementsProjectFolders requirementsProjectFolders) {
		this.requirementsProjectFolders = requirementsProjectFolders;
	}

	public Defects getDefects() {
		return defects;
	}

	public void setDefects(Defects defects) {
		this.defects = defects;
	}

	public ArrayList<AlmApiDetailsPojo> getAlmApiDetailsPojo() {
		return almApiDetailsPojo;
	}

	public void setAlmApiDetailsPojo(ArrayList<AlmApiDetailsPojo> almApiDetailsPojo) {
		this.almApiDetailsPojo = almApiDetailsPojo;
	}

	public ArrayList<AlmInProjectDetailsPojo> getAlmInProjectDetailsPojo() {
		return almInProjectDetailsPojo;
	}

	public void setAlmInProjectDetailsPojo(ArrayList<AlmInProjectDetailsPojo> almInProjectDetailsPojo) {
		this.almInProjectDetailsPojo = almInProjectDetailsPojo;
	}

	public ArrayList<AlmSitDetailsPojo> getAlmSitDetailsPojo() {
		return almSitDetailsPojo;
	}

	public void setAlmSitDetailsPojo(ArrayList<AlmSitDetailsPojo> almSitDetailsPojo) {
		this.almSitDetailsPojo = almSitDetailsPojo;
	}

	public ArrayList<AlmUatDetailsPojo> getAlmUatDetailsPojo() {
		return almUatDetailsPojo;
	}

	public void setAlmUatDetailsPojo(ArrayList<AlmUatDetailsPojo> almUatDetailsPojo) {
		this.almUatDetailsPojo = almUatDetailsPojo;
	}

	public ArrayList<AlmModuleDetailsPojo> getAlmModuleDetailsPojo() {
		return almModuleDetailsPojo;
	}

	public void setAlmModuleDetailsPojo(ArrayList<AlmModuleDetailsPojo> almModuleDetailsPojo) {
		this.almModuleDetailsPojo = almModuleDetailsPojo;
	}

	public ArrayList<MigrationModuleDetailsPojo> getMigrationModuleDetailsPojo() {
		return migrationModuleDetailsPojo;
	}

	public void setMigrationModuleDetailsPojo(ArrayList<MigrationModuleDetailsPojo> migrationModuleDetailsPojo) {
		this.migrationModuleDetailsPojo = migrationModuleDetailsPojo;
	}

}
