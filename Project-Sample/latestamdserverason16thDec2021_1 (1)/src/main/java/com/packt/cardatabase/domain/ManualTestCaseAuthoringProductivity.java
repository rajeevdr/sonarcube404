package com.packt.cardatabase.domain;

import javax.persistence.*;
import java.util.ArrayList;

@Entity
public class ManualTestCaseAuthoringProductivity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private ArrayList<String> numberOfManualTestcasesAddedEachWeek;
    private ArrayList<String> moduleNames;
    private String effortSpentOnManualTestcaseDesignEachWeek;
    private String manualTestCaseAuthoringProductivity;
    private Integer weekNumber;
    private Integer yearNumber;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public ManualTestCaseAuthoringProductivity() {}

    public ManualTestCaseAuthoringProductivity(ArrayList<String> numberOfManualTestcasesAddedEachWeek,
                                               String effortSpentOnManualTestcaseDesignEachWeek,
                                               int weekNumber,
                                               int yearNumber,
                                               Project project) {

        super();
        this.numberOfManualTestcasesAddedEachWeek = numberOfManualTestcasesAddedEachWeek;
        this.effortSpentOnManualTestcaseDesignEachWeek = effortSpentOnManualTestcaseDesignEachWeek;
        this.weekNumber = weekNumber;
        this.yearNumber = yearNumber;
        this.project = project;
    }

    public Integer getWeekNumber() {
        return weekNumber;
    }
    public String getWeekNumberName() {
        return "WeekNumber";
    }
    public void setWeekNumber(Integer weekNumber) {
        this.weekNumber = weekNumber;
    }

    public Integer getYearNumber() {
        return yearNumber;
    }
    public String getYearNumberName() {
        return "YearNumber";
    }
    public void setYearNumber(Integer yearNumber) {
        this.yearNumber = yearNumber;
    }


    public String getManualTestCaseAuthoringProductivity() {
        return manualTestCaseAuthoringProductivity;
    }
    public String getManualTestCaseAuthoringProductivityName() {
        return "ManualTestCaseAuthoringProductivity";
    }
    public void setManualTestCaseAuthoringProductivity(String manualTestCaseAuthoringProductivity) {
        this.manualTestCaseAuthoringProductivity = manualTestCaseAuthoringProductivity;
    }

    public ArrayList<String> getNumberOfManualTestcasesAddedEachWeek(){
        return numberOfManualTestcasesAddedEachWeek;
    }
    public String getNumberOfManualTestcasesAddedEachWeekName(){
        return "NumberOfManualTestcasesAddedEachWeek";
    }
    public void setNumberOfManualTestcasesAddedEachWeek(ArrayList<String> numberOfManualTestcasesAddedEachWeek){
        this.numberOfManualTestcasesAddedEachWeek = numberOfManualTestcasesAddedEachWeek;
    }

    public ArrayList<String> getModuleNames(){
        return moduleNames;
    }
    public String getModuleNamesName(){
        return "ModuleNames";
    }
    public void setModuleNames(ArrayList<String> moduleNames){
        this.moduleNames = moduleNames;
    }

    public String  getEffortSpentOnManualTestcaseDesignEachWeek() {
        return effortSpentOnManualTestcaseDesignEachWeek;
    }

    public String getEffortSpentOnManualTestcaseDesignEachWeekName(){
        return "EffortSpentOnManualTestcaseDesignEachWeek";
    }

    public void setEffortSpentOnManualTestcaseDesignEachWeek(String effortSpentOnManualTestcaseDesignEachWeek) {
        this.effortSpentOnManualTestcaseDesignEachWeek = effortSpentOnManualTestcaseDesignEachWeek;
    }



    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

}