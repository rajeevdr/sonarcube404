package com.packt.cardatabase.domain;

import javax.persistence.*;

public class WeeklyMetricsPojo {

    private String manualTestCaseAuthoringProductivity;
    private String regressionTestAutomationScriptingProductivity;
    private String apiTestAutomationScriptingProductivity;
    private String inProjectAutomationScriptingProductivity;
    private String designCoverage;
    private String percentageOfRegressionAutomation;
    private String testReviewEfficiency;
    private String downtimeAndImpactAnalysisDesignAvgProductivityLost;
    private String manualTestExecutionProductivity;
    private String regressionAutomationTestExecutionProductivity;
    private String inProjectAutomationTestExecutionProductivity;
    private String apiAutomationTestExecutionProductivity;
    private String migratedDataExecution;
    private String defectRejection;
    private String testCaseNotExecutedManual;
    private String testCaseNotExecutedAutomation;
    private String currentQualityRatio;
    private String qualityOfFixes;
    private String downtimeAndImpactAnalysisExecutionAvgProductivityLost;
    private int weekNumber;
    private String defectLeakageSITToUAT;
    private String defectLeakageToProduction;
    private String numberOfTestableRequirementsTillDate;


    public WeeklyMetricsPojo() {}

    public WeeklyMetricsPojo(String manualTestCaseAuthoringProductivity,
                         String regressionTestAutomationScriptingProductivity,
                         String apiTestAutomationScriptingProductivity,
                         String inProjectAutomationScriptingProductivity,
                         String designCoverage,
                         String percentageOfRegressionAutomation,
                         String testReviewEfficiency,
                             String downtimeAndImpactAnalysisDesignAvgProductivityLost,
                         String manualTestExecutionProductivity,
                         String automationTestExecutionProductivity,
                         String regressionAutomationTestExecutionProductivity,
                         String inProjectAutomationTestExecutionProductivity,
                         String apiAutomationTestExecutionProductivity,
                         String migratedDataExecution,
                         String defectRejection,
                         String testCaseNotExecutedManual,
                         String testCaseNotExecutedAutomation,
                         String currentQualityRatio,
                         String qualityOfFixes,
                             String downtimeAndImpactAnalysisExecutionAvgProductivityLost,
                             String numberOfTestableRequirementsTillDate,
                         String defectLeakageSITToUAT,
                         String defectLeakageToProduction,
                             int weekNumber) {

        super();
        this.manualTestCaseAuthoringProductivity = manualTestCaseAuthoringProductivity;
        this.regressionTestAutomationScriptingProductivity = regressionTestAutomationScriptingProductivity;
        this.apiTestAutomationScriptingProductivity = apiTestAutomationScriptingProductivity;
        this.inProjectAutomationScriptingProductivity = inProjectAutomationScriptingProductivity;
        this.designCoverage =  designCoverage;
        this.percentageOfRegressionAutomation =  percentageOfRegressionAutomation;
        this.testReviewEfficiency =  testReviewEfficiency;
        this.downtimeAndImpactAnalysisDesignAvgProductivityLost =  downtimeAndImpactAnalysisDesignAvgProductivityLost;
        this.manualTestExecutionProductivity =  manualTestExecutionProductivity;
        this.regressionAutomationTestExecutionProductivity =  regressionAutomationTestExecutionProductivity;
        this.inProjectAutomationTestExecutionProductivity =  inProjectAutomationTestExecutionProductivity;
        this.apiAutomationTestExecutionProductivity =  apiAutomationTestExecutionProductivity;
        this.migratedDataExecution =  migratedDataExecution;
        this.defectRejection =  defectRejection;
        this.testCaseNotExecutedManual =  testCaseNotExecutedManual;
        this.testCaseNotExecutedAutomation =  testCaseNotExecutedAutomation;
        this.currentQualityRatio =  currentQualityRatio;
        this.qualityOfFixes =  qualityOfFixes;
        this.downtimeAndImpactAnalysisExecutionAvgProductivityLost =  downtimeAndImpactAnalysisExecutionAvgProductivityLost;
        this.defectLeakageSITToUAT = defectLeakageSITToUAT;
        this.defectLeakageToProduction = defectLeakageToProduction;
        this.numberOfTestableRequirementsTillDate = numberOfTestableRequirementsTillDate;

        this.weekNumber = weekNumber;
    }

    public int getWeekNumber() {
        return weekNumber;
    }
    public String getWeekNumberName() {
        return "WeekNumber";
    }
    public void setWeekNumber(int weekNumber) {
        this.weekNumber = weekNumber;
    }


    public String getNumberOfTestableRequirementsTillDate(){
        return numberOfTestableRequirementsTillDate;
    }

    public void setNumberOfTestableRequirementsTillDate(String numberOfTestableRequirementsTillDate){
        this.numberOfTestableRequirementsTillDate = numberOfTestableRequirementsTillDate;
    }

    public void setManualTestCaseAuthoringProductivity(String manualTestCaseAuthoringProductivity) {
        this.manualTestCaseAuthoringProductivity = manualTestCaseAuthoringProductivity;
    }

    public String getManualTestCaseAuthoringProductivity() {
        return this.manualTestCaseAuthoringProductivity;
    }



    public String getRegressionTestAutomationScriptingProductivity() {
        return regressionTestAutomationScriptingProductivity;
    }

    public void setRegressionTestScriptingProductivity(String regressionTestAutomationScriptingProductivity) {
        this.regressionTestAutomationScriptingProductivity = regressionTestAutomationScriptingProductivity;
    }

    public String getApiTestAutomationScriptingProductivity() {
        return apiTestAutomationScriptingProductivity;
    }

    public void setApiAutomationTestScriptingProductivity(String apiTestAutomationScriptingProductivity) {
        this.apiTestAutomationScriptingProductivity = apiTestAutomationScriptingProductivity;
    }

    public String getInProjectAutomationScriptingProductivity() {
        return inProjectAutomationScriptingProductivity;
    }

    public void setInProjectTestScriptingProductivity(String inProjectAutomationScriptingProductivity) {
        this.inProjectAutomationScriptingProductivity = inProjectAutomationScriptingProductivity;
    }


    public String getDesignCoverage(){
        return designCoverage;
    }

    public String getPercentageOfRegressionAutomation(){
        return percentageOfRegressionAutomation;
    }

    public String getTestReviewEfficiency(){
        return testReviewEfficiency;
    }

    public String getDowntimeAndImpactAnalysisDesignAvgProductivityLost(){
        return downtimeAndImpactAnalysisDesignAvgProductivityLost;
    }

    public String getManualTestExecutionProductivity(){
        return manualTestCaseAuthoringProductivity;
    }


    public String getAutomationTestExecutionProductivity(){
        return regressionAutomationTestExecutionProductivity;
    }

    public String getRegressionAutomationTestExecutionProductivity(){
        return regressionAutomationTestExecutionProductivity;
    }

    public String getInProjectAutomationTestExecutionProductivity(){
        return inProjectAutomationTestExecutionProductivity;
    }

    public String getAPIAutomationTestExecutionProductivity(){
        return apiAutomationTestExecutionProductivity;
    }

    public String getMigratedDataExecution(){
        return migratedDataExecution;
    }

    public String getDefectRejection(){
        return defectRejection;
    }

    public String getTestCaseNotExecutedManual(){
        return testCaseNotExecutedManual;
    }

    public String getTestCaseNotExecutedAutomation(){
        return testCaseNotExecutedAutomation;
    }

    public String getCurrentQualityRatio(){
        return currentQualityRatio;
    }

    public String getQualityOfFixes(){
        return qualityOfFixes;
    }

    public String getDowntimeAndImpactAnalysisExecutionAvgProductivityLost(){
        return downtimeAndImpactAnalysisExecutionAvgProductivityLost;
    }


    public String getDefectLeakageSITToUAT(){
        return defectLeakageSITToUAT;
    }

    public String getDefectLeakageToProduction(){
        return defectLeakageToProduction;
    }

    public void setDesignCoverage(String designCoverage){
        this.designCoverage = designCoverage;
    }

    public void setPercentageOfRegressionAutomation(String percentageOfRegressionAutomation){
        this.percentageOfRegressionAutomation = percentageOfRegressionAutomation;
    }

    public void setTestReviewEfficiency(String testReviewEfficiency){
        this.testReviewEfficiency = testReviewEfficiency;
    }

    public void setDowntimeAndImpactAnalysisDesignAvgProductivityLost(String downtimeAndImpactAnalysisDesignAvgProductivityLost){
        this.downtimeAndImpactAnalysisDesignAvgProductivityLost = downtimeAndImpactAnalysisDesignAvgProductivityLost;
    }

    public void setManualTestExecutionProductivity(String manualTestExecutionProductivity){
        this.manualTestExecutionProductivity = manualTestExecutionProductivity;
    }

    public void setAutomationTestExecutionProductivity(String regressionAutomationTestExecutionProductivity){
        this.regressionAutomationTestExecutionProductivity = regressionAutomationTestExecutionProductivity;
    }

    public void setRegressionAutomationTestExecutionProductivity(String regressionAutomationTestExecutionProductivity){
        this.regressionAutomationTestExecutionProductivity = regressionAutomationTestExecutionProductivity;
    }

    public void setInProjectAutomationTestExecutionProductivity(String inProjectAutomationTestExecutionProductivity){
        this.inProjectAutomationTestExecutionProductivity = inProjectAutomationTestExecutionProductivity;
    }

    public void setAPIAutomationTestExecutionProductivity(String apiAutomationTestExecutionProductivity){
        this.apiAutomationTestExecutionProductivity = apiAutomationTestExecutionProductivity;
    }

    public void setMigratedDataExecution(String migratedDataExecution){
        this.migratedDataExecution = migratedDataExecution;
    }

    public void setDefectRejection(String defectRejection){
        this.defectRejection = defectRejection;
    }

    public void setTestCaseNotExecutedManual(String testCaseNotExecutedManual){
        this.testCaseNotExecutedManual = testCaseNotExecutedManual;
    }

    public void setTestCaseNotExecutedAutomation(String testCaseNotExecutedAutomation){
        this.testCaseNotExecutedAutomation = testCaseNotExecutedAutomation;
    }

    public void setCurrentQualityRatio(String currentQualityRatio){
        this.currentQualityRatio = currentQualityRatio;
    }

    public void setQualityOfFixes(String qualityOfFixes){
        this.qualityOfFixes = qualityOfFixes;
    }

    public void setDowntimeAndImpactAnalysisExecutionAvgProductivityLost(String downtimeAndImpactAnalysisExecutionAvgProductivityLost){
        this.downtimeAndImpactAnalysisExecutionAvgProductivityLost = downtimeAndImpactAnalysisExecutionAvgProductivityLost;
    }

    public void setDefectLeakageSITToUAT(String defectLeakageSITToUAT){
        this.defectLeakageSITToUAT = defectLeakageSITToUAT;
    }

    public void setDefectLeakageToProduction(String defectLeakageToProduction){
        this.defectLeakageToProduction = defectLeakageToProduction;
    }

}