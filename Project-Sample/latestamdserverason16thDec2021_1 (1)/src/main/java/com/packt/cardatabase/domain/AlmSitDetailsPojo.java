package com.packt.cardatabase.domain;

import java.util.ArrayList;

public class AlmSitDetailsPojo {

	private Integer id;
	private ArrayList<String> testExecutionFolderId = null;
	private ArrayList<String> testDesignFolderId = null;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ArrayList<String> getTestExecutionFolderId() {
		return testExecutionFolderId;
	}

	public void setTestExecutionFolderId(ArrayList<String> testExecutionFolderId) {
		this.testExecutionFolderId = testExecutionFolderId;
	}

	public ArrayList<String> getTestDesignFolderId() {
		return testDesignFolderId;
	}

	public void setTestDesignFolderId(ArrayList<String> testDesignFolderId) {
		this.testDesignFolderId = testDesignFolderId;
	}

}
