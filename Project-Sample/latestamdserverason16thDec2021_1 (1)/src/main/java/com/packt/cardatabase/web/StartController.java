package com.packt.cardatabase.web;

import com.google.gson.Gson;
import com.packt.cardatabase.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.*;


@RestController
public class StartController {

	@Autowired
	private WeekDataRepository weekDataRepository;

	@Autowired
	private MonthDataRepository monthDataRepository;

	@Autowired
	private QuarterDataRepository quarterDataRepository;

	@Autowired
	private LastUpdateRepository lastUpdateRepository;

	@Autowired
	private SyncStartAndEndDateUpdateRepository syncStartAndEndDateUpdateRepository;

	@Autowired
	private ProjectRepository projectRepository;


	@RequestMapping(value = "/currentWeekMonthQuarter", method = RequestMethod.GET)
	@ResponseBody
	public String getCurrentWeekMonthQuarterDates() {

		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		Date date = new Date();
		String dateToday = formatter.format(date);
		System.out.println(dateToday);

		StringTokenizer tokenizer = new StringTokenizer(formatter.format(date), "-");
		int dateNumber = Integer.parseInt(tokenizer.nextToken());
		int monthNumber = Integer.parseInt(tokenizer.nextToken());
		int yearNumber = Integer.parseInt(tokenizer.nextToken());

		int currentWeekNumber = 0;
		int currentMonthNumber = 0;
		int currentQuarterNumber = 0;

		String currentWeekStartDate = "";
		String currentWeekEndDate = "";
		String currentMonthStartDate = "";
		String currentMonthEndDate = "";
		String currentQuarterStartDate = "";
		String currentQuarterEndDate = "";

		Iterable<WeekData> weekDataIterable = weekDataRepository.findAll();
		Iterator<WeekData> weekDataIterator = weekDataIterable.iterator();

		while (weekDataIterator.hasNext()) {
			WeekData next = weekDataIterator.next();
			if (next.getWeekDays().contains(dateToday)) {
				currentWeekNumber = next.getWeekNumber();
				System.out.println("The current week number : " + currentWeekNumber);
				currentWeekStartDate = next.getWeekStartDate();
				currentWeekEndDate = next.getWeekEndDate();
				break;
			}
		}


		Iterable<MonthData> monthDataIterable = monthDataRepository.findAll();
		Iterator<MonthData> monthDataIterator = monthDataIterable.iterator();


		while (monthDataIterator.hasNext()) {
			MonthData next = monthDataIterator.next();

			if ((next.getYearNumber() == yearNumber) &&
					(next.getMonthNumber() == monthNumber)) {
				currentMonthNumber = next.getMonthNumber();
				currentMonthStartDate = next.getMonthStartDate();
				currentMonthEndDate = next.getMonthEndDate();
				break;
			}

		}


		Iterable<QuarterData> quarterDataIterable = quarterDataRepository.findAll();
		Iterator<QuarterData> quarterDataIterator = quarterDataIterable.iterator();


		while (quarterDataIterator.hasNext()) {
			QuarterData next = quarterDataIterator.next();

			if (next.getYearNumber() == yearNumber) {

				StringTokenizer tokenizer2 = new StringTokenizer(next.getQuarterStartDate(), "-");
				tokenizer2.nextToken();

				int monthNumber2 = Integer.parseInt(tokenizer2.nextToken());

				if (monthNumber2 <= monthNumber) {

					StringTokenizer tokenizer3 = new StringTokenizer(next.getQuarterEndDate(), "-");
					tokenizer3.nextToken();

					int monthNumber3 = Integer.parseInt(tokenizer3.nextToken());

					if (monthNumber3 >= monthNumber) {
						currentQuarterNumber = next.getQuarterNumber();
						System.out.println("The current quarter number is : " + currentQuarterNumber);
						currentQuarterStartDate = next.getQuarterStartDate();
						currentQuarterEndDate = next.getQuarterEndDate();
						break;
					}

				}
			}
		}


		String response = "{" + "\"currentWeekStartDate\":\"" + currentWeekStartDate + "\",";
		response += "\"currentWeekEndDate\":\"" + currentWeekEndDate + "\",";
		response += "\"currentMonthStartDate\":\"" + currentMonthStartDate + "\",";
		response += "\"currentMonthEndDate\":\"" + currentMonthEndDate + "\",";
		response += "\"currentQuarterStartDate\":\"" + currentQuarterStartDate + "\",";
		response += "\"currentQuarterEndDate\":\"" + currentQuarterEndDate + "\",";
		response += "\"currentWeek\":\"" + currentWeekNumber + "\",";
		response += "\"currentMonth\":\"" + monthNumber + "\",";
		response += "\"currentQuarter\":\"" + currentQuarterNumber + "\",";
		response += "\"currentYear\":\"" + yearNumber + "\"}";


		System.out.println("The current week, month and quarter details are : " + response);


		return response;
	}


	@PostMapping(value = "/projects/{id}/setLastSyncUpdateDate")
	@ResponseBody
	public String setLastSyncUpdateDate(@PathVariable("id") long id, @RequestParam String lastUpdateDate) {

		try {

			Iterable<LastUpdate> lastUpdateIterable = lastUpdateRepository.findAll();
			Iterator<LastUpdate> lastUpdateIterator = lastUpdateIterable.iterator();

			while (lastUpdateIterator.hasNext()) {
				LastUpdate next = lastUpdateIterator.next();

				if (next.getProject().getId() == id) {

					try {
						lastUpdateRepository.delete(next);
					} catch (Exception e) {

					}
				}
			}

			System.out.println("The desired last update date is : " + lastUpdateDate);
			//Date date = new SimpleDateFormat("dd-MM-yyyy").parse(lastUpdateDate);


			StringTokenizer tokenizer = new StringTokenizer(lastUpdateDate, "-");
			int dateNumber = Integer.parseInt(tokenizer.nextToken());
			int monthNumber = Integer.parseInt(tokenizer.nextToken());
			int yearNumber = Integer.parseInt(tokenizer.nextToken());

			int desiredWeekNumber = 0;
			int desiredMonthNumber = 0;
			int desiredQuarterNumber = 0;
			int desiredYearNumber = 0;


			String dateNumberString = null;
			String monthNumberString = null;


			if (dateNumber < 10)
				dateNumberString = "0" + Integer.toString(dateNumber);
			else
				dateNumberString = Integer.toString(dateNumber);

			if (monthNumber < 10)
				monthNumberString = "0" + Integer.toString(monthNumber);
			else
				monthNumberString = Integer.toString(monthNumber);


			String dateToday = dateNumberString + "-" + monthNumberString + "-" + yearNumber;
			System.out.println("The sync start date is : " + dateToday);


			Iterable<WeekData> weekDataIterable = weekDataRepository.findAll();
			Iterator<WeekData> weekDataIterator = weekDataIterable.iterator();

			while (weekDataIterator.hasNext()) {
				WeekData next = weekDataIterator.next();
				if (next.getWeekDays().contains(dateToday)) {
					desiredWeekNumber = next.getWeekNumber();
					System.out.println("The current week number : " + desiredWeekNumber);
					break;
				}
			}

			Iterable<MonthData> monthDataIterable = monthDataRepository.findAll();
			Iterator<MonthData> monthDataIterator = monthDataIterable.iterator();

			while (monthDataIterator.hasNext()) {
				MonthData next = monthDataIterator.next();

				if ((next.getYearNumber() == yearNumber) &&
						(next.getMonthNumber() == monthNumber)) {
					desiredMonthNumber = next.getMonthNumber();
					desiredYearNumber = next.getYearNumber();
					System.out.println("The desired month number is : " + desiredMonthNumber);
					System.out.println("The desired year number is : " + desiredYearNumber);

					break;
				}

			}

			Iterable<QuarterData> quarterDataIterable = quarterDataRepository.findAll();
			Iterator<QuarterData> quarterDataIterator = quarterDataIterable.iterator();


			while (quarterDataIterator.hasNext()) {
				QuarterData next = quarterDataIterator.next();

				if (next.getYearNumber() == yearNumber) {

					StringTokenizer tokenizer2 = new StringTokenizer(next.getQuarterStartDate(), "-");
					tokenizer2.nextToken();

					int monthNumber2 = Integer.parseInt(tokenizer2.nextToken());

					if (monthNumber2 <= monthNumber) {

						StringTokenizer tokenizer3 = new StringTokenizer(next.getQuarterEndDate(), "-");
						tokenizer3.nextToken();

						int monthNumber3 = Integer.parseInt(tokenizer3.nextToken());

						if (monthNumber3 >= monthNumber) {
							desiredQuarterNumber = next.getQuarterNumber();
							System.out.println("The desired quarter number is : " + desiredQuarterNumber);
							break;
						}

					}
				}
			}


			LastUpdate lastUpdate = new LastUpdate();
			lastUpdate.setLastUpdatedDateNumber(dateNumber);
			lastUpdate.setLastUpdatedWeekNumber(desiredWeekNumber);
			lastUpdate.setLastUpdatedMonthNumber(desiredMonthNumber);
			lastUpdate.setLastUpdatedYearNumber(desiredYearNumber);
			lastUpdate.setLastUpdatedQuarterNumber(desiredQuarterNumber);

			Iterable<Project> allProjects = projectRepository.findAll();
			Iterator<Project> iterator = allProjects.iterator();

			while (iterator.hasNext()) {
				Project nextProject = iterator.next();

				if (nextProject.getId() == id) {
					lastUpdateRepository.delete(lastUpdate);
				}
			}

			allProjects = projectRepository.findAll();
			iterator = allProjects.iterator();

			while (iterator.hasNext()) {
				Project nextProject = iterator.next();

				if (nextProject.getId() == id) {
					lastUpdate.setProject(nextProject);
					lastUpdateRepository.save(lastUpdate);
					break;
				}
			}


			return "OK";
		} catch (Exception e) {

		}
		return "Error";
	}

	@RequestMapping(value = "/getLastSyncUpdateDate/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String getLastSyncUpdateDate(@PathVariable("id") long id) {

		Iterable<LastUpdate> lastUpdateIterable = lastUpdateRepository.findAll();
		Iterator<LastUpdate> lastUpdateIterator = lastUpdateIterable.iterator();

		while (lastUpdateIterator.hasNext()) {
			LastUpdate next = lastUpdateIterator.next();

			if (next.getProject().getId() == id) {

				try {
					int dateNumber = next.getLastUpdatedDateNumber();
					int monthNumber = next.getLastUpdatedMonthNumber();
					int yearNumber = next.getLastUpdatedYearNumber();

					String dateNumberString = null;
					String monthNumberString = null;

					if (dateNumber < 10)
						dateNumberString = "0" + dateNumber;
					else
						dateNumberString = Integer.toString(dateNumber);

					if (monthNumber < 10)
						monthNumberString = "0" + monthNumber;
					else
						monthNumberString = Integer.toString(monthNumber);


					String lastUpdatedDate = dateNumberString + "-" + monthNumberString + "-" + Integer.toString(yearNumber);

					String response = "{\"lastUpdateDate\":{\"" + lastUpdatedDate + "\"}}";
					System.out.println(response);
					return response;

				} catch (Exception e) {

				}
			}
		}
		return "Error";
	}


	@RequestMapping(value = "/getSyncWeekMonthQuarterDatesSinceLastUpdate/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String getSyncWeekMonthQuarterDatesSinceLastUpdate(@PathVariable("id") long id) {

		int syncStartDateNumber = 0;
		int syncStartWeekNumber = 0;
		int syncStartMonthNumber = 0;
		int syncStartQuarterNumber = 0;
		int syncStartYearNumber = 0;

		int syncEndDateNumber = 0;
		int syncEndWeekNumber = 0;
		int syncEndMonthNumber = 0;
		int syncEndQuarterNumber = 0;
		int syncEndYearNumber = 0;

		int syncEndYearNumber2 = 0;

		try {

			Iterable<LastUpdate> lastUpdateIterable = lastUpdateRepository.findAll();
			Iterator<LastUpdate> lastUpdateIterator = lastUpdateIterable.iterator();

			while (lastUpdateIterator.hasNext()) {
				LastUpdate next = lastUpdateIterator.next();

				if (next.getProject().getId() == id) {
					syncStartDateNumber = next.getLastUpdatedDateNumber();
					syncStartWeekNumber = next.getLastUpdatedWeekNumber();
					syncStartMonthNumber = next.getLastUpdatedMonthNumber();
					syncStartYearNumber = next.getLastUpdatedYearNumber();
					syncStartQuarterNumber = next.getLastUpdatedQuarterNumber();
					break;
				}
			}


			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			Date date = new Date();
			String dateToday = formatter.format(date);
			System.out.println(dateToday);
			StringTokenizer tokenizer = new StringTokenizer(formatter.format(date), "-");

			syncEndDateNumber  = Integer.parseInt(tokenizer.nextToken());
			syncEndMonthNumber = Integer.parseInt(tokenizer.nextToken());
			syncEndYearNumber  = Integer.parseInt(tokenizer.nextToken());

		} catch (Exception e) {
		}

		int dateNumber  = syncEndDateNumber;
		int monthNumber = syncEndMonthNumber;
		int yearNumber  = syncEndYearNumber;

		String dateNumberString = null;
		String monthNumberString = null;


		if (dateNumber < 10)
			dateNumberString = "0" + Integer.toString(dateNumber);
		else
			dateNumberString = Integer.toString(dateNumber);

		if (monthNumber < 10)
			monthNumberString = "0" + Integer.toString(monthNumber);
		else
			monthNumberString = Integer.toString(monthNumber);


		String dateToday = dateNumberString + "-" + monthNumberString + "-" + yearNumber;
		System.out.println("The sync end date is : " + dateToday);


		Iterable<WeekData> weekDataIterable = weekDataRepository.findAll();
		Iterator<WeekData> weekDataIterator = weekDataIterable.iterator();

		while (weekDataIterator.hasNext()) {
			WeekData next = weekDataIterator.next();
			if (next.getWeekDays().contains(dateToday)) {
				syncEndWeekNumber = next.getWeekNumber();
				System.out.println("The sync end week number : " + syncEndWeekNumber);
				break;
			}
		}


		Iterable<MonthData> monthDataIterable = monthDataRepository.findAll();
		Iterator<MonthData> monthDataIterator = monthDataIterable.iterator();

		while (monthDataIterator.hasNext()) {
			MonthData next = monthDataIterator.next();

			if ((next.getYearNumber() == yearNumber) &&
					(next.getMonthNumber() == monthNumber)) {
				syncEndMonthNumber = next.getMonthNumber();
				System.out.println("The sync end month number : " + syncEndMonthNumber);
				break;
			}

		}

		Iterable<QuarterData> quarterDataIterable = quarterDataRepository.findAll();
		Iterator<QuarterData> quarterDataIterator = quarterDataIterable.iterator();

		while (quarterDataIterator.hasNext()) {
			QuarterData next = quarterDataIterator.next();

			if (next.getYearNumber() == yearNumber) {

				StringTokenizer tokenizer2 = new StringTokenizer(next.getQuarterStartDate(), "-");
				tokenizer2.nextToken();

				int monthNumber2 = Integer.parseInt(tokenizer2.nextToken());

				if (monthNumber2 <= monthNumber) {

					StringTokenizer tokenizer3 = new StringTokenizer(next.getQuarterEndDate(), "-");
					tokenizer3.nextToken();

					int monthNumber3 = Integer.parseInt(tokenizer3.nextToken());

					if (monthNumber3 >= monthNumber) {
						syncEndQuarterNumber = next.getQuarterNumber();
						System.out.println("The sync end quarter number is : " + syncEndQuarterNumber);
						break;
					}

				}
			}
		}

		syncEndYearNumber2 = syncEndYearNumber;

		if(syncEndMonthNumber == 12){
			if(syncEndWeekNumber == 1){
				syncEndYearNumber2 = syncEndYearNumber2 + 1;
			}
		}

		boolean yearMatched = false;
		boolean monthMatched = false;
		boolean weekMatched = false;
		boolean quarterMatched = false;

		if (syncStartYearNumber == syncEndYearNumber) {
			weekDataIterable = weekDataRepository.findAll();
			weekDataIterator = weekDataIterable.iterator();
			yearMatched = false;
			monthMatched = false;
			weekMatched = false;
			quarterMatched = false;


			String misseddates = "{\"Dates\":{";

			String missedWeeks = "\"Weeks\":[";
			while (weekDataIterator.hasNext()) {
				WeekData next = weekDataIterator.next();

				if(yearMatched == false) {
					if (next.getYearNumber() != syncStartYearNumber) {
						continue;
					}
				}

				yearMatched = true;

				if(monthMatched == false) {
					if (next.getMonthNumber() != syncStartMonthNumber)
						continue;
				}

				monthMatched = true;

				if(weekMatched == false) {
					if (next.getWeekNumber() != syncStartWeekNumber)
						continue;
				}
				weekMatched = true;


				missedWeeks = missedWeeks + "{";
				missedWeeks += "\"weekNumber\":\"" + next.getWeekNumber() + "\",";
				missedWeeks += "\"weekStartDate\":\"" + next.getWeekStartDate() + "\",";
				missedWeeks += "\"weekEndDate\":\"" + next.getWeekEndDate() + "\",";
				missedWeeks += "\"year\":\"" + next.getYearNumber() + "\"},";

				if ((next.getYearNumber() == syncEndYearNumber2) &&
						(next.getWeekNumber() == syncEndWeekNumber)) {
					break;
				}

			}

			missedWeeks = missedWeeks.substring(0, missedWeeks.length() - 1);
			missedWeeks = missedWeeks + "],";

			monthDataIterable = monthDataRepository.findAll();
			monthDataIterator = monthDataIterable.iterator();
			String missedmonths = "\"Months\":[";

			yearMatched = false;
			monthMatched = false;
			weekMatched = false;
			quarterMatched = false;

			while (monthDataIterator.hasNext()) {
				MonthData next = monthDataIterator.next();


				if(yearMatched == false) {
					if (next.getYearNumber() != syncStartYearNumber)
						continue;
				}

				yearMatched = true;

				if(monthMatched == false) {
					if (next.getMonthNumber() < syncStartMonthNumber)
						continue;
				}
				monthMatched = true;


				missedmonths = missedmonths + "{";
				missedmonths += "\"monthNumber\":\"" + next.getMonthNumber() + "\",";
				missedmonths += "\"monthStartDate\":\"" + next.getMonthStartDate() + "\",";
				missedmonths += "\"monthEndDate\":\"" + next.getMonthEndDate() + "\",";
				missedmonths += "\"year\":\"" + next.getYearNumber() + "\"},";

				if ((next.getYearNumber() == syncEndYearNumber) &&
						(next.getMonthNumber() == syncEndMonthNumber)) {
					break;
				}

			}

			missedmonths = missedmonths.substring(0, missedmonths.length() - 1);
			missedmonths = missedmonths + "]},";

			quarterDataIterable = quarterDataRepository.findAll();
			quarterDataIterator = quarterDataIterable.iterator();
			String missedQuarters = "\"Quarters\":[";

			yearMatched = false;
			monthMatched = false;
			weekMatched = false;
			quarterMatched = false;


			while (quarterDataIterator.hasNext()) {
				QuarterData next = quarterDataIterator.next();

				if(yearMatched == false) {
					if (next.getYearNumber() != syncStartYearNumber)
						continue;
				}
				yearMatched = true;

				if(quarterMatched == false) {
					if (next.getQuarterNumber() < syncStartQuarterNumber)
						continue;
				}

				quarterMatched = true;



				System.out.println("The sync end year number is : " + syncEndYearNumber);
				System.out.println("The sync end quarter number is : " + syncEndQuarterNumber);
				System.out.println("The current year number is : " + next.getYearNumber());
				System.out.println("The current quarter number is : " + next.getQuarterNumber());

				missedQuarters = missedQuarters + "{";
				missedQuarters += "\"quarterNumber\":\"" + next.getQuarterNumber() + "\",";
				missedQuarters += "\"quarterStartDate\":\"" + next.getQuarterStartDate() + "\",";
				missedQuarters += "\"quarterEndDate\":\"" + next.getQuarterEndDate() + "\",";
				missedQuarters += "\"year\":\"" + next.getYearNumber() + "\"},";

				if ((next.getYearNumber() == syncEndYearNumber) &&
						(next.getQuarterNumber() == syncEndQuarterNumber)) {
					break;
				}

			}

			missedQuarters = missedQuarters.substring(0, missedQuarters.length() - 1);
			missedQuarters = missedQuarters + "]}";

			System.out.println(misseddates + missedWeeks + missedmonths + missedQuarters);
			return misseddates + missedWeeks + missedmonths + missedQuarters;

		} else {
			weekDataIterable = weekDataRepository.findAll();
			weekDataIterator = weekDataIterable.iterator();

			String misseddates = "{\"Dates\":{";

			String missedWeeks = "\"Weeks\":[";

			yearMatched = false;
			monthMatched = false;
			weekMatched = false;
			quarterMatched = false;

			boolean startDateReached = false;
			while (weekDataIterator.hasNext()) {
				WeekData next = weekDataIterator.next();


				if(yearMatched == false) {
					if (next.getYearNumber() != syncStartYearNumber) {
						continue;
					}
				}

				yearMatched = true;

				if(monthMatched == false) {
					if (next.getMonthNumber() != syncStartMonthNumber)
						continue;
				}

				monthMatched = true;

				if(weekMatched == false) {
					if (next.getWeekNumber() != syncStartWeekNumber)
						continue;
				}
				weekMatched = true;

				missedWeeks = missedWeeks + "{";
				missedWeeks += "\"weekNumber\":\"" + next.getWeekNumber() + "\",";
				missedWeeks += "\"weekStartDate\":\"" + next.getWeekStartDate() + "\",";
				missedWeeks += "\"weekEndDate\":\"" + next.getWeekEndDate() + "\",";
				missedWeeks += "\"year\":\"" + next.getYearNumber() + "\"},";

				if ((next.getYearNumber() == syncEndYearNumber2) &&
						(next.getWeekNumber() == syncEndWeekNumber)) {
					break;
				}

			}

			missedWeeks = missedWeeks.substring(0, missedWeeks.length() - 1);
			missedWeeks = missedWeeks + "],";

			monthDataIterable = monthDataRepository.findAll();
			monthDataIterator = monthDataIterable.iterator();
			String missedmonths = "\"Months\":[";
			yearMatched = false;
			monthMatched = false;
			weekMatched = false;
			quarterMatched = false;

			while (monthDataIterator.hasNext()) {
				MonthData next = monthDataIterator.next();


				if(yearMatched == false) {
					if (next.getYearNumber() != syncStartYearNumber)
						continue;
				}

				yearMatched = true;

				if(monthMatched == false) {
					if (next.getMonthNumber() < syncStartMonthNumber)
						continue;
				}
				monthMatched = true;


				missedmonths = missedmonths + "{";
				missedmonths += "\"monthNumber\":\"" + next.getMonthNumber() + "\",";
				missedmonths += "\"monthStartDate\":\"" + next.getMonthStartDate() + "\",";
				missedmonths += "\"monthEndDate\":\"" + next.getMonthEndDate() + "\",";
				missedmonths += "\"year\":\"" + next.getYearNumber() + "\"},";

				if ((next.getYearNumber() == syncEndYearNumber) &&
						(next.getMonthNumber() == syncEndMonthNumber)) {
					break;
				}

			}

			missedmonths = missedmonths.substring(0, missedmonths.length() - 1);
			missedmonths = missedmonths + "]},";

			quarterDataIterable = quarterDataRepository.findAll();
			quarterDataIterator = quarterDataIterable.iterator();
			String missedQuarters = "\"Quarters\":[";
			yearMatched = false;
			monthMatched = false;
			weekMatched = false;
			quarterMatched = false;


			while (quarterDataIterator.hasNext()) {
				QuarterData next = quarterDataIterator.next();

				if(yearMatched == false) {
					if (next.getYearNumber() != syncStartYearNumber)
						continue;
				}
				yearMatched = true;

				if(quarterMatched == false) {
					if (next.getQuarterNumber() < syncStartQuarterNumber)
						continue;
				}

				quarterMatched = true;



				System.out.println("The sync end year number is : " + syncEndYearNumber);
				System.out.println("The sync end quarter number is : " + syncEndQuarterNumber);
				System.out.println("The current year number is : " + next.getYearNumber());
				System.out.println("The current quarter number is : " + next.getQuarterNumber());


				missedQuarters = missedQuarters + "{";
				missedQuarters += "\"quarterNumber\":\"" + next.getQuarterNumber() + "\",";
				missedQuarters += "\"quarterStartDate\":\"" + next.getQuarterStartDate() + "\",";
				missedQuarters += "\"quarterEndDate\":\"" + next.getQuarterEndDate() + "\",";
				missedQuarters += "\"year\":\"" + next.getYearNumber() + "\"},";


				if ((next.getYearNumber() == syncEndYearNumber) &&
						(next.getQuarterNumber() == syncEndQuarterNumber)) {
					break;
				}

			}

			missedQuarters = missedQuarters.substring(0, missedQuarters.length() - 1);
			missedQuarters = missedQuarters + "]}";

			System.out.println(misseddates + missedWeeks + missedmonths + missedQuarters);
			return misseddates + missedWeeks + missedmonths + missedQuarters;

		}

	}


	@PostMapping(value = "/projects/{id}/setAdhocSyncStartAndEndDate")
	@ResponseBody
	public String setAdhocSyncWeekMonthQuarterStartAndEndDates(@PathVariable("id") long id,
															   @RequestParam String startSyncDate,
															   @RequestParam String endSyncDate) {

		try {
			System.out.println("The desired sync start date is : " + startSyncDate);
			System.out.println("The desired sync end date is : " + endSyncDate);

			//Date date = new SimpleDateFormat("dd-MM-yyyy").parse(lastUpdateDate);


			Iterable<SyncStartAndEndDateUpdate> syncStartAndEndDateUpdateIterable = syncStartAndEndDateUpdateRepository.findAll();
			Iterator<SyncStartAndEndDateUpdate> syncStartAndEndDateUpdateIterator = syncStartAndEndDateUpdateIterable.iterator();

			try {
				while (syncStartAndEndDateUpdateIterator.hasNext()) {
					SyncStartAndEndDateUpdate next = syncStartAndEndDateUpdateIterator.next();

					if (next.getProject().getId() == id) {
						syncStartAndEndDateUpdateRepository.delete(next);
					}
				}
			} catch (Exception e) {
			}


			StringTokenizer tokenizer = new StringTokenizer(startSyncDate, "-");
			int startDateNumber = Integer.parseInt(tokenizer.nextToken());
			int monthNumber = Integer.parseInt(tokenizer.nextToken());
			int yearNumber = Integer.parseInt(tokenizer.nextToken());


			String dateNumberString = null;
			String monthNumberString = null;


			if (startDateNumber < 10)
				dateNumberString = "0" + Integer.toString(startDateNumber);
			else
				dateNumberString = Integer.toString(startDateNumber);

			if (monthNumber < 10)
				monthNumberString = "0" + Integer.toString(monthNumber);
			else
				monthNumberString = Integer.toString(monthNumber);


			startSyncDate = dateNumberString + "-" + monthNumberString + "-" + yearNumber;
			System.out.println("The adhoc sync start date is : " + startSyncDate);


			int desiredSyncStartWeekNumber = 0;
			int desiredSyncStartMonthNumber = 0;
			int desiredSyncStartQuarterNumber = 0;
			int desiredSyncStartYearNumber = 0;


			Iterable<WeekData> weekDataIterable = weekDataRepository.findAll();
			Iterator<WeekData> weekDataIterator = weekDataIterable.iterator();

			while (weekDataIterator.hasNext()) {
				WeekData next = weekDataIterator.next();
				if (next.getWeekDays().contains(startSyncDate)) {
					desiredSyncStartWeekNumber = next.getWeekNumber();
					System.out.println("The desired sync start week number : " + desiredSyncStartWeekNumber);
					break;
				}
			}

			Iterable<MonthData> monthDataIterable = monthDataRepository.findAll();
			Iterator<MonthData> monthDataIterator = monthDataIterable.iterator();

			while (monthDataIterator.hasNext()) {
				MonthData next = monthDataIterator.next();

				if ((next.getYearNumber() == yearNumber) &&
						(next.getMonthNumber() == monthNumber)) {
					desiredSyncStartMonthNumber = next.getMonthNumber();
					desiredSyncStartYearNumber = next.getYearNumber();
					System.out.println("The desired sync start month number is : " + desiredSyncStartMonthNumber);
					System.out.println("The desired sync start year number is : " + desiredSyncStartYearNumber);

					break;
				}

			}

			Iterable<QuarterData> quarterDataIterable = quarterDataRepository.findAll();
			Iterator<QuarterData> quarterDataIterator = quarterDataIterable.iterator();


			while (quarterDataIterator.hasNext()) {
				QuarterData next = quarterDataIterator.next();

				if (next.getYearNumber() == yearNumber) {

					StringTokenizer tokenizer2 = new StringTokenizer(next.getQuarterStartDate(), "-");
					tokenizer2.nextToken();

					int monthNumber2 = Integer.parseInt(tokenizer2.nextToken());

					if (monthNumber2 <= monthNumber) {

						StringTokenizer tokenizer3 = new StringTokenizer(next.getQuarterEndDate(), "-");
						tokenizer3.nextToken();

						int monthNumber3 = Integer.parseInt(tokenizer3.nextToken());

						if (monthNumber3 >= monthNumber) {
							desiredSyncStartQuarterNumber = next.getQuarterNumber();
							System.out.println("The desired quarter number is : " + desiredSyncStartQuarterNumber);
							break;
						}

					}
				}
			}

			tokenizer = new StringTokenizer(endSyncDate, "-");
			int endDateNumber = Integer.parseInt(tokenizer.nextToken());
			monthNumber = Integer.parseInt(tokenizer.nextToken());
			yearNumber = Integer.parseInt(tokenizer.nextToken());


			dateNumberString = null;
			monthNumberString = null;

			if (endDateNumber < 10)
				dateNumberString = "0" + Integer.toString(endDateNumber);
			else
				dateNumberString = Integer.toString(endDateNumber);

			if (monthNumber < 10)
				monthNumberString = "0" + Integer.toString(monthNumber);
			else
				monthNumberString = Integer.toString(monthNumber);


			endSyncDate = dateNumberString + "-" + monthNumberString + "-" + yearNumber;
			System.out.println("The adhoc sync end date is : " + endSyncDate);


			int desiredSyncEndWeekNumber = 0;
			int desiredSyncEndMonthNumber = 0;
			int desiredSyncEndQuarterNumber = 0;
			int desiredSyncEndYearNumber = 0;


			weekDataIterable = weekDataRepository.findAll();
			weekDataIterator = weekDataIterable.iterator();

			while (weekDataIterator.hasNext()) {
				WeekData next = weekDataIterator.next();
				if (next.getWeekDays().contains(endSyncDate)) {
					desiredSyncEndWeekNumber = next.getWeekNumber();
					System.out.println("The desired sync end week number is : " + desiredSyncEndWeekNumber);
					break;
				}
			}

			monthDataIterable = monthDataRepository.findAll();
			monthDataIterator = monthDataIterable.iterator();

			while (monthDataIterator.hasNext()) {
				MonthData next = monthDataIterator.next();

				if ((next.getYearNumber() == yearNumber) &&
						(next.getMonthNumber() == monthNumber)) {
					desiredSyncEndMonthNumber = next.getMonthNumber();
					desiredSyncEndYearNumber = next.getYearNumber();
					System.out.println("The desired sync end month number is : " + desiredSyncEndMonthNumber);
					System.out.println("The desired sync end year number is : " + desiredSyncEndYearNumber);

					break;
				}

			}

			quarterDataIterable = quarterDataRepository.findAll();
			quarterDataIterator = quarterDataIterable.iterator();


			while (quarterDataIterator.hasNext()) {
				QuarterData next = quarterDataIterator.next();

				if (next.getYearNumber() == yearNumber) {

					StringTokenizer tokenizer2 = new StringTokenizer(next.getQuarterStartDate(), "-");
					tokenizer2.nextToken();

					int monthNumber2 = Integer.parseInt(tokenizer2.nextToken());

					if (monthNumber2 <= monthNumber) {

						StringTokenizer tokenizer3 = new StringTokenizer(next.getQuarterEndDate(), "-");
						tokenizer3.nextToken();

						int monthNumber3 = Integer.parseInt(tokenizer3.nextToken());

						if (monthNumber3 >= monthNumber) {
							desiredSyncEndQuarterNumber = next.getQuarterNumber();
							System.out.println("The desired sync end quarter number is : " + desiredSyncEndQuarterNumber);
							break;
						}

					}
				}
			}

			SyncStartAndEndDateUpdate desiredSyncStartAndEndDate = new SyncStartAndEndDateUpdate();
			desiredSyncStartAndEndDate.setDesiredSyncStartDateNumber(startDateNumber);
			desiredSyncStartAndEndDate.setDesiredSyncStartDateWeekNumber(desiredSyncStartWeekNumber);
			desiredSyncStartAndEndDate.setDesiredSyncStartDateMonthNumber(desiredSyncStartMonthNumber);
			desiredSyncStartAndEndDate.setDesiredSyncStartDateYearNumber(desiredSyncStartYearNumber);
			desiredSyncStartAndEndDate.setDesiredSyncStartDateQuarterNumber(desiredSyncStartQuarterNumber);

			desiredSyncStartAndEndDate.setDesiredSyncEndDateNumber(endDateNumber);
			desiredSyncStartAndEndDate.setDesiredSyncEndDateWeekNumber(desiredSyncEndWeekNumber);
			desiredSyncStartAndEndDate.setDesiredSyncEndDateMonthNumber(desiredSyncEndMonthNumber);
			desiredSyncStartAndEndDate.setDesiredSyncEndDateYearNumber(desiredSyncEndYearNumber);
			desiredSyncStartAndEndDate.setDesiredSyncEndDateQuarterNumber(desiredSyncEndQuarterNumber);

			Iterable<Project> allProjects = projectRepository.findAll();
			Iterator<Project> iterator = allProjects.iterator();

			while (iterator.hasNext()) {
				Project nextProject = iterator.next();

				if (nextProject.getId() == id) {
					desiredSyncStartAndEndDate.setProject(nextProject);
					syncStartAndEndDateUpdateRepository.save(desiredSyncStartAndEndDate);
					break;
				}
			}

			return "OK";
		} catch (Exception e) {

		}
		return "Error";
	}

	@RequestMapping(value = "/getAdhocSyncStartAndEndDate/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String getAdhocSyncStartAndEndDate(@PathVariable("id") long id) {

		Iterable<SyncStartAndEndDateUpdate> syncStartAndEndDateUpdateIterable = syncStartAndEndDateUpdateRepository.findAll();
		Iterator<SyncStartAndEndDateUpdate> syncStartAndEndDateUpdateIterator = syncStartAndEndDateUpdateIterable.iterator();

		while (syncStartAndEndDateUpdateIterator.hasNext()) {
			SyncStartAndEndDateUpdate next = syncStartAndEndDateUpdateIterator.next();

			if (next.getProject().getId() == id) {

				try {

					String startDate = next.getDesiredSyncStartDateNumber() + "-" +
							next.getDesiredSyncStartDateMonthNumber() + "-" +
							next.getDesiredSyncStartDateYearNumber();

					String endDate = next.getDesiredSyncEndDateNumber() + "-" +
							next.getDesiredSyncEndDateMonthNumber() + "-" +
							next.getDesiredSyncEndDateYearNumber();

					String response = "{\"lastUpdate\":{" +
							"\"startDate\":\"" +
							startDate + "\"," +
							"\"endDate\":\"" +
							endDate + "\"}}";

					System.out.println(response);
					return response;

				} catch (Exception e) {

				}
			}
		}

		return "No data found";

	}


	@RequestMapping(value = "/getAdhocSyncWeekMonthQuarterDates/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String getAdhocSyncWeekMonthQuarterDates(@PathVariable("id") long id) {

		Iterable<SyncStartAndEndDateUpdate> syncStartAndEndDateUpdateIterable = syncStartAndEndDateUpdateRepository.findAll();
		Iterator<SyncStartAndEndDateUpdate> syncStartAndEndDateUpdateIterator = syncStartAndEndDateUpdateIterable.iterator();

		while (syncStartAndEndDateUpdateIterator.hasNext()) {
			SyncStartAndEndDateUpdate syncStartAndEndDateUpdate = syncStartAndEndDateUpdateIterator.next();

			int syncStartDateNumber = 0;
			int syncStartWeekNumber = 0;
			int syncStartMonthNumber = 0;
			int syncStartQuarterNumber = 0;
			int syncStartYearNumber = 0;

			int syncEndDateNumber = 0;
			int syncEndWeekNumber = 0;
			int syncEndMonthNumber = 0;
			int syncEndQuarterNumber = 0;
			int syncEndYearNumber = 0;

			int syncEndYearNumber2 = 0;


			if (syncStartAndEndDateUpdate.getProject().getId() == id) {

				try {
					syncStartDateNumber = syncStartAndEndDateUpdate.getDesiredSyncStartDateNumber();
					syncStartMonthNumber = syncStartAndEndDateUpdate.getDesiredSyncStartDateMonthNumber();
					syncStartYearNumber = syncStartAndEndDateUpdate.getDesiredSyncStartDateYearNumber();
					syncEndDateNumber = syncStartAndEndDateUpdate.getDesiredSyncEndDateNumber();
					syncEndMonthNumber = syncStartAndEndDateUpdate.getDesiredSyncEndDateMonthNumber();
					syncEndYearNumber = syncStartAndEndDateUpdate.getDesiredSyncEndDateYearNumber();
				} catch (Exception e) {
				}


				int dateNumber = syncStartDateNumber;
				int monthNumber = syncStartMonthNumber;
				int yearNumber = syncStartYearNumber;

				String dateNumberString = null;
				String monthNumberString = null;


				if (dateNumber < 10)
					dateNumberString = "0" + Integer.toString(dateNumber);
				else
					dateNumberString = Integer.toString(dateNumber);

				if (monthNumber < 10)
					monthNumberString = "0" + Integer.toString(monthNumber);
				else
					monthNumberString = Integer.toString(monthNumber);


				String dateToday = dateNumberString + "-" + monthNumberString + "-" + yearNumber;
				System.out.println("The sync start date is : " + dateToday);

				Iterable<WeekData> weekDataIterable = weekDataRepository.findAll();
				Iterator<WeekData> weekDataIterator = weekDataIterable.iterator();

				while (weekDataIterator.hasNext()) {
					WeekData next = weekDataIterator.next();
					if (next.getWeekDays().contains(dateToday)) {
						syncStartWeekNumber = next.getWeekNumber();
						System.out.println("The sync start week number : " + syncStartWeekNumber);
						break;
					}
				}


				Iterable<MonthData> monthDataIterable = monthDataRepository.findAll();
				Iterator<MonthData> monthDataIterator = monthDataIterable.iterator();

				while (monthDataIterator.hasNext()) {
					MonthData next = monthDataIterator.next();

					if ((next.getYearNumber() == yearNumber) &&
							(next.getMonthNumber() == monthNumber)) {
						syncStartMonthNumber = next.getMonthNumber();
						System.out.println("The sync start month number : " + syncStartMonthNumber);
						break;
					}

				}

				Iterable<QuarterData> quarterDataIterable = quarterDataRepository.findAll();
				Iterator<QuarterData> quarterDataIterator = quarterDataIterable.iterator();

				while (quarterDataIterator.hasNext()) {
					QuarterData next = quarterDataIterator.next();

					if (next.getYearNumber() == yearNumber) {

						StringTokenizer tokenizer2 = new StringTokenizer(next.getQuarterStartDate(), "-");
						tokenizer2.nextToken();

						int monthNumber2 = Integer.parseInt(tokenizer2.nextToken());

						if (monthNumber2 <= monthNumber) {

							StringTokenizer tokenizer3 = new StringTokenizer(next.getQuarterEndDate(), "-");
							tokenizer3.nextToken();

							int monthNumber3 = Integer.parseInt(tokenizer3.nextToken());

							if (monthNumber3 >= monthNumber) {
								syncStartQuarterNumber = next.getQuarterNumber();
								System.out.println("The sync start quarter number is : " + syncStartQuarterNumber);
								break;
							}

						}
					}
				}

				dateNumber = syncEndDateNumber;
				monthNumber = syncEndMonthNumber;
				yearNumber = syncEndYearNumber;
				syncEndYearNumber2 = syncEndYearNumber;


				dateNumberString = null;
				monthNumberString = null;
				dateToday = null;

				if (dateNumber < 10)
					dateNumberString = "0" + Integer.toString(dateNumber);
				else
					dateNumberString = Integer.toString(dateNumber);

				if (monthNumber < 10)
					monthNumberString = "0" + Integer.toString(monthNumber);
				else
					monthNumberString = Integer.toString(monthNumber);


				dateToday = dateNumberString + "-" + monthNumberString + "-" + yearNumber;
				System.out.println("The sync end date is : " + dateToday);


				weekDataIterable = weekDataRepository.findAll();
				weekDataIterator = weekDataIterable.iterator();

				while (weekDataIterator.hasNext()) {
					WeekData next = weekDataIterator.next();
					if (next.getWeekDays().contains(dateToday)) {
						syncEndWeekNumber = next.getWeekNumber();
						System.out.println("The sync end week number : " + syncEndWeekNumber);
						break;
					}
				}


				monthDataIterable = monthDataRepository.findAll();
				monthDataIterator = monthDataIterable.iterator();

				while (monthDataIterator.hasNext()) {
					MonthData next = monthDataIterator.next();

					if ((next.getYearNumber() == yearNumber) &&
							(next.getMonthNumber() == monthNumber)) {
						syncEndMonthNumber = next.getMonthNumber();
						System.out.println("The sync end month number : " + syncEndMonthNumber);
						break;
					}

				}

				if(syncEndMonthNumber == 12)
				{
					if(syncEndWeekNumber == 1) {
						syncEndYearNumber2 = syncEndYearNumber + 1;
					}
				}

				quarterDataIterable = quarterDataRepository.findAll();
				quarterDataIterator = quarterDataIterable.iterator();

				while (quarterDataIterator.hasNext()) {
					QuarterData next = quarterDataIterator.next();

					if (next.getYearNumber() == yearNumber) {

						StringTokenizer tokenizer2 = new StringTokenizer(next.getQuarterStartDate(), "-");
						tokenizer2.nextToken();

						int monthNumber2 = Integer.parseInt(tokenizer2.nextToken());

						if (monthNumber2 <= monthNumber) {

							StringTokenizer tokenizer3 = new StringTokenizer(next.getQuarterEndDate(), "-");
							tokenizer3.nextToken();

							int monthNumber3 = Integer.parseInt(tokenizer3.nextToken());

							if (monthNumber3 >= monthNumber) {
								syncEndQuarterNumber = next.getQuarterNumber();
								System.out.println("The sync end quarter number is : " + syncEndQuarterNumber);
								break;
							}

						}
					}
				}

				boolean yearMatched = false;
				boolean monthMatched = false;
				boolean weekMatched = false;
				boolean quarterMatched = false;


				if (syncStartYearNumber == syncEndYearNumber) {
					weekDataIterable = weekDataRepository.findAll();
					weekDataIterator = weekDataIterable.iterator();

					String misseddates = "{\"Dates\":{";

					String missedWeeks = "\"Weeks\":[";
					while (weekDataIterator.hasNext()) {
						WeekData next = weekDataIterator.next();

						if(yearMatched == false) {
							if (next.getYearNumber() != syncStartYearNumber) {
								continue;
							}
						}

						yearMatched = true;

						if(monthMatched == false) {
							if (next.getMonthNumber() != syncStartMonthNumber)
								continue;
						}

						monthMatched = true;

						if(weekMatched == false) {
							if (next.getWeekNumber() != syncStartWeekNumber)
								continue;
						}
						weekMatched = true;


						missedWeeks = missedWeeks + "{";
						missedWeeks += "\"weekNumber\":\"" + next.getWeekNumber() + "\",";
						missedWeeks += "\"weekStartDate\":\"" + next.getWeekStartDate() + "\",";
						missedWeeks += "\"weekEndDate\":\"" + next.getWeekEndDate() + "\",";
						missedWeeks += "\"year\":\"" + next.getYearNumber() + "\"},";

						String tempmissedWeeks = "{" + "\"weekNumber\":\"" + next.getWeekNumber() + "\",";
						tempmissedWeeks += "\"weekStartDate\":\"" + next.getWeekStartDate() + "\",";
						tempmissedWeeks += "\"weekEndDate\":\"" + next.getWeekEndDate() + "\",";
						tempmissedWeeks += "\"year\":\"" + next.getYearNumber() + "\"}";

						System.out.println(tempmissedWeeks);
						System.out.println();

						if ((next.getYearNumber() == syncEndYearNumber2) &&
								(next.getWeekNumber() == syncEndWeekNumber)) {
							break;
						}

					}

					missedWeeks = missedWeeks.substring(0, missedWeeks.length() - 1);
					missedWeeks = missedWeeks + "],";

					monthDataIterable = monthDataRepository.findAll();
					monthDataIterator = monthDataIterable.iterator();
					String missedmonths = "\"Months\":[";

					yearMatched = false;
					monthMatched = false;

					while (monthDataIterator.hasNext()) {
						MonthData next = monthDataIterator.next();

						if(yearMatched == false) {
							if (next.getYearNumber() != syncStartYearNumber)
								continue;
						}

						yearMatched = true;

						if(monthMatched == false) {
							if (next.getMonthNumber() < syncStartMonthNumber)
								continue;
						}
						monthMatched = true;


						missedmonths = missedmonths + "{";
						missedmonths += "\"monthNumber\":\"" + next.getMonthNumber() + "\",";
						missedmonths += "\"monthStartDate\":\"" + next.getMonthStartDate() + "\",";
						missedmonths += "\"monthEndDate\":\"" + next.getMonthEndDate() + "\",";
						missedmonths += "\"year\":\"" + next.getYearNumber() + "\"},";

						if ((next.getYearNumber() == syncEndYearNumber) &&
								(next.getMonthNumber() == syncEndMonthNumber)) {
							break;
						}

					}

					missedmonths = missedmonths.substring(0, missedmonths.length() - 1);
					missedmonths = missedmonths + "]},";

					quarterDataIterable = quarterDataRepository.findAll();
					quarterDataIterator = quarterDataIterable.iterator();
					String missedQuarters = "\"Quarters\":[";

					yearMatched = false;
					quarterMatched = false;

					while (quarterDataIterator.hasNext()) {
						QuarterData next = quarterDataIterator.next();

						if(yearMatched == false) {
							if (next.getYearNumber() != syncStartYearNumber)
								continue;
						}
						yearMatched = true;

						if(quarterMatched == false) {
							if (next.getQuarterNumber() < syncStartQuarterNumber)
								continue;
						}

						quarterMatched = true;



						System.out.println("The sync end year number is : " + syncEndYearNumber);
						System.out.println("The sync end quarter number is : " + syncEndQuarterNumber);
						System.out.println("The current year number is : " + next.getYearNumber());
						System.out.println("The current quarter number is : " + next.getQuarterNumber());

						missedQuarters = missedQuarters + "{";
						missedQuarters += "\"quarterNumber\":\"" + next.getQuarterNumber() + "\",";
						missedQuarters += "\"quarterStartDate\":\"" + next.getQuarterStartDate() + "\",";
						missedQuarters += "\"quarterEndDate\":\"" + next.getQuarterEndDate() + "\",";
						missedQuarters += "\"year\":\"" + next.getYearNumber() + "\"},";

						if ((next.getYearNumber() == syncEndYearNumber) &&
								(next.getQuarterNumber() == syncEndQuarterNumber)) {
							break;
						}

					}

					missedQuarters = missedQuarters.substring(0, missedQuarters.length() - 1);
					missedQuarters = missedQuarters + "]}";

					System.out.println(misseddates + missedWeeks + missedmonths + missedQuarters);
					return misseddates + missedWeeks + missedmonths + missedQuarters;

				}
				else {

					yearMatched = false;
					monthMatched = false;
					weekMatched = false;
					quarterMatched = false;

					weekDataIterable = weekDataRepository.findAll();
					weekDataIterator = weekDataIterable.iterator();

					String misseddates = "{\"Dates\":{";

					String missedWeeks = "\"Weeks\":[";

					boolean startDateReached = false;
					while (weekDataIterator.hasNext()) {
						WeekData next = weekDataIterator.next();

						if(yearMatched == false) {
							if (next.getYearNumber() != syncStartYearNumber) {
								continue;
							}
						}

						yearMatched = true;

						if(monthMatched == false) {
							if (next.getMonthNumber() != syncStartMonthNumber)
								continue;
						}

						monthMatched = true;

						if(weekMatched == false) {
							if (next.getWeekNumber() != syncStartWeekNumber)
								continue;
						}
						weekMatched = true;


						missedWeeks = missedWeeks + "{";
						missedWeeks += "\"weekNumber\":\"" + next.getWeekNumber() + "\",";
						missedWeeks += "\"weekStartDate\":\"" + next.getWeekStartDate() + "\",";
						missedWeeks += "\"weekEndDate\":\"" + next.getWeekEndDate() + "\",";
						missedWeeks += "\"year\":\"" + next.getYearNumber() + "\"},";

						System.out.println(missedWeeks);

						if ((next.getYearNumber() == syncEndYearNumber2) &&
								(next.getWeekNumber() == syncEndWeekNumber)) {
							break;
						}

					}

					missedWeeks = missedWeeks.substring(0, missedWeeks.length() - 1);
					missedWeeks = missedWeeks + "],";

					monthDataIterable = monthDataRepository.findAll();
					monthDataIterator = monthDataIterable.iterator();
					String missedmonths = "\"Months\":[";
					startDateReached = false;

					yearMatched = false;
					monthMatched = false;
					weekMatched = false;
					quarterMatched = false;


					while (monthDataIterator.hasNext()) {
						MonthData next = monthDataIterator.next();

						if(yearMatched == false) {
							if (next.getYearNumber() != syncStartYearNumber) {
								continue;
							}
						}

						yearMatched = true;

						if(monthMatched == false) {
							if (next.getMonthNumber() != syncStartMonthNumber)
								continue;
						}

						monthMatched = true;


						missedmonths = missedmonths + "{";
						missedmonths += "\"monthNumber\":\"" + next.getMonthNumber() + "\",";
						missedmonths += "\"monthStartDate\":\"" + next.getMonthStartDate() + "\",";
						missedmonths += "\"monthEndDate\":\"" + next.getMonthEndDate() + "\",";
						missedmonths += "\"year\":\"" + next.getYearNumber() + "\"},";

						if ((next.getYearNumber() == syncEndYearNumber) &&
								(next.getMonthNumber() == syncEndMonthNumber)) {
							break;
						}

					}

					missedmonths = missedmonths.substring(0, missedmonths.length() - 1);
					missedmonths = missedmonths + "]},";

					quarterDataIterable = quarterDataRepository.findAll();
					quarterDataIterator = quarterDataIterable.iterator();
					String missedQuarters = "\"Quarters\":[";
					startDateReached = false;

					yearMatched = false;
					monthMatched = false;
					weekMatched = false;
					quarterMatched = false;


					while (quarterDataIterator.hasNext()) {
						QuarterData next = quarterDataIterator.next();

						if(yearMatched == false) {
							if (next.getYearNumber() != syncStartYearNumber)
								continue;
						}
						yearMatched = true;

						if(quarterMatched == false) {
							if (next.getQuarterNumber() < syncStartQuarterNumber)
								continue;
						}

						quarterMatched = true;

						System.out.println("The sync end year number is : " + syncEndYearNumber);
						System.out.println("The sync end quarter number is : " + syncEndQuarterNumber);
						System.out.println("The current year number is : " + next.getYearNumber());
						System.out.println("The current quarter number is : " + next.getQuarterNumber());

						if (next.getYearNumber() > syncEndYearNumber)
							break;

						missedQuarters = missedQuarters + "{";
						missedQuarters += "\"quarterNumber\":\"" + next.getQuarterNumber() + "\",";
						missedQuarters += "\"quarterStartDate\":\"" + next.getQuarterStartDate() + "\",";
						missedQuarters += "\"quarterEndDate\":\"" + next.getQuarterEndDate() + "\",";
						missedQuarters += "\"year\":\"" + next.getYearNumber() + "\"},";


						if ((next.getYearNumber() == syncEndYearNumber) &&
								(next.getQuarterNumber() == syncEndQuarterNumber)) {
							break;
						}

					}

					missedQuarters = missedQuarters.substring(0, missedQuarters.length() - 1);
					missedQuarters = missedQuarters + "]}";

					System.out.println(misseddates + missedWeeks + missedmonths + missedQuarters);
					return misseddates + missedWeeks + missedmonths + missedQuarters;

				}
			}
		}
		return "Error";
	}


	@PostMapping(value = "/projects/{id}/removeAdhocSyncStartAndEndDate")
	@ResponseBody
	public String removeSyncStartAndEndDates(@PathVariable("id") long id) {

		try {

			Iterable<SyncStartAndEndDateUpdate> syncStartAndEndDateUpdateIterable = syncStartAndEndDateUpdateRepository.findAll();
			Iterator<SyncStartAndEndDateUpdate> syncStartAndEndDateUpdateIterator = syncStartAndEndDateUpdateIterable.iterator();

			try {
				while (syncStartAndEndDateUpdateIterator.hasNext()) {
					SyncStartAndEndDateUpdate next = syncStartAndEndDateUpdateIterator.next();

					if (next.getProject().getId() == id) {
						syncStartAndEndDateUpdateRepository.delete(next);
					}
				}
				return "OK";
			} catch (Exception e) {
			}

		} catch (Exception e) {
		}
		return "Error";
	}



}