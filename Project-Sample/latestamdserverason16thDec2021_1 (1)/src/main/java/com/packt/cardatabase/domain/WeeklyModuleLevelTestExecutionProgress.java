package com.packt.cardatabase.domain;


import javax.persistence.*;

@Entity
public class WeeklyModuleLevelTestExecutionProgress {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private String numberOfManualTestCasesExecutedForTheWeek;
    private String numberOfRegAutoTestsExecutedForTheWeek;
    private String defectsForTheWeek;


    private int weekNumber;
    private int monthNumber;
    private int yearNumber;
    private String moduleName;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public WeeklyModuleLevelTestExecutionProgress() {
    }

    public WeeklyModuleLevelTestExecutionProgress(String moduleName,
                                                  String numberOfManualTestCasesExecutedForTheWeek,
                                                  String numberOfRegAutoTestsExecutedForTheWeek,
                                                  String defectsForTheWeek,
                                                  int weekNumber,
                                                  int monthNumber,
                                                  int yearNumber,
                                           Project project) {
        super();
        this.moduleName = moduleName;
        this.numberOfManualTestCasesExecutedForTheWeek = numberOfManualTestCasesExecutedForTheWeek;
        this.numberOfRegAutoTestsExecutedForTheWeek = numberOfRegAutoTestsExecutedForTheWeek;
        this.defectsForTheWeek = defectsForTheWeek;
        this.weekNumber = weekNumber;
        this.monthNumber = monthNumber;
        this.yearNumber = yearNumber;
        this.project = project;

    }


    public int getYearNumber() {
        return yearNumber;
    }
    public String getYearNumberName() {
        return "Year";
    }
    public void setYearNumber(int yearNumber) {
        this.yearNumber = yearNumber;
    }

    public int getMonthNumber() {
        return monthNumber;
    }
    public String getMonthNumberName() {
        return "Month";
    }
    public void setMonthNumber(int monthNumber) {
        this.monthNumber = monthNumber;
    }


    public String getModuleName() {
        return moduleName;
    }
    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public int getWeekNumber() {
        return weekNumber;
    }

    public String getWeekNumberName() {
        return "Week";
    }
    public void setWeekNumber(int weekNumber) {
        this.weekNumber = weekNumber;
    }

    public int getNumberOfManualTestCasesExecutedForTheWeek() {

        if(numberOfManualTestCasesExecutedForTheWeek == null)
            return 0;
        return Integer.parseInt(numberOfManualTestCasesExecutedForTheWeek);
    }

    public String getNumberOfManualTestCasesExecutedForTheWeekName() {
        return "Number Of Manual TestCases Executed For The Week";
    }


    public void setNumberOfManualTestCasesExecutedForTheWeek(String numberOfManualTestCasesExecutedForTheWeek) {
        this.numberOfManualTestCasesExecutedForTheWeek = numberOfManualTestCasesExecutedForTheWeek;
    }


    public int getNumberOfRegressionAutomationTestCasesExecutedForTheWeek() {
        if(numberOfRegAutoTestsExecutedForTheWeek == null)
            return 0;
        return Integer.parseInt(numberOfRegAutoTestsExecutedForTheWeek);
    }

    public String getNumberOfRegressionAutomationTestCasesExecutedForTheWeekName() {
        return "Number Of Regression Automation TestCases Executed For The Week";
    }

    public void setNumberOfRegressionAutomationTestCasesExecutedForTheWeek(String numberOfRegressionAutomationTestCasesExecutedForTheWeek) {
        this.numberOfRegAutoTestsExecutedForTheWeek = numberOfRegressionAutomationTestCasesExecutedForTheWeek;
    }


    public int getDefectsForTheWeek() {

        if(defectsForTheWeek == null)
            return 0;
        return Integer.parseInt(defectsForTheWeek);
    }

    public String getDefectsForTheWeekName() {
        return "Defects For The Week";
    }

    public void setDefectsForTheWeek(String defectsForTheWeek) {
        this.defectsForTheWeek = defectsForTheWeek;
    }


    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

}