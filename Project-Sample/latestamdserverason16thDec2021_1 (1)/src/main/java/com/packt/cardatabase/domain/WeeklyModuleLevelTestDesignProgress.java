package com.packt.cardatabase.domain;

import javax.persistence.*;

@Entity
public class WeeklyModuleLevelTestDesignProgress {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;
    private long moduleId;
    private String numberOfManualTestCasesDesignedForTheWeek;
    private String numberOfRegAutoTestsDesignedForTheWeek;
    private String numberOfSanityTestsDesignedForTheWeek;
    private int weekNumber;
    private int monthNumber;
    private int yearNumber;
    private String moduleName;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public WeeklyModuleLevelTestDesignProgress() {}

    public WeeklyModuleLevelTestDesignProgress(String moduleName,
                                               long moduleId,
                                               String numberOfManualTestCasesDesignedForTheWeek,
                                               String numberOfRegAutoTestsDesignedForTheWeek,
                                               String numberOfSanityTestsDesignedForTheWeek,
                                               int weekNumber,
                                               int monthNumber,
                                               int yearNumber,
                                               Project project) {

        super();
        this.moduleName = moduleName;
        this.moduleId =  moduleId;
        this.numberOfManualTestCasesDesignedForTheWeek = numberOfManualTestCasesDesignedForTheWeek;
        this.numberOfRegAutoTestsDesignedForTheWeek = numberOfRegAutoTestsDesignedForTheWeek;
        this.numberOfSanityTestsDesignedForTheWeek = numberOfSanityTestsDesignedForTheWeek;
        this.weekNumber = weekNumber;
        this.monthNumber = monthNumber;
        this.yearNumber = yearNumber;
        this.project = project;
    }


    public int getWeekNumber() {
        return weekNumber;
    }
    public String getWeekNumberName() {
        return "Week";
    }
    public void setWeekNumber(int weekNumber) {
        this.weekNumber = weekNumber;
    }

    public int getMonthNumber() {
        return monthNumber;
    }
    public String getMonthNumberName() {
        return "Month";
    }
    public void setMonthNumber(int monthNumber) {
        this.monthNumber = monthNumber;
    }

    public int getYearNumber() {
        return yearNumber;
    }
    public String getYearNumberName() {
        return "Year";
    }
    public void setYearNumber(int yearNumber) {
        this.yearNumber = yearNumber;
    }

    public String getModuleName() {
        return moduleName;
    }
    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public long getModuleId() {
        return moduleId;
    }
    public void setModuleId(long moduleId) {
        this.moduleId = moduleId;
    }

    public int getNumberOfManualTestCasesDesignedForTheWeek() {
        if(numberOfManualTestCasesDesignedForTheWeek == null)
        {
            return 0;
        }
        return Integer.parseInt(numberOfManualTestCasesDesignedForTheWeek);
    }

    public String getNumberOfManualTestCasesDesignedForTheWeekName() {
        return "Number Of Manual TestCases Designed For The Week";
    }

    public void setNumberOfManualTestCasesDesignedForTheWeek(String numberOfManualTestCasesDesignedForTheWeek) {
        this.numberOfManualTestCasesDesignedForTheWeek = numberOfManualTestCasesDesignedForTheWeek;
    }


    public int getNumberOfRegressionAutomationTestScriptsDesignedForTheWeek() {

        if(numberOfRegAutoTestsDesignedForTheWeek == null)
        {
            return 0;
        }
        return Integer.parseInt(numberOfRegAutoTestsDesignedForTheWeek);

    }

    public String getNumberOfRegressionAutomationTestScriptsDesignedForTheWeekName() {
        return "Number Of Regression Automation Test Scripts Designed For The Week";
    }
    public void setNumberOfRegressionAutomationTestScriptsDesignedForTheWeek(String numberOfRegAutoTestsDesignedForTheWeek) {
        this.numberOfRegAutoTestsDesignedForTheWeek = numberOfRegAutoTestsDesignedForTheWeek;
    }


    public int getNumberOfSanityTestsDesignedForTheWeek() {

        if(numberOfSanityTestsDesignedForTheWeek == null)
        {
            return 0;
        }
        return Integer.parseInt(numberOfSanityTestsDesignedForTheWeek);

    }

    public void setNumberOfSanityTestsDesignedForTheWeek(String numberOfSanityTestsDesignedForTheWeek) {
        this.numberOfSanityTestsDesignedForTheWeek = numberOfSanityTestsDesignedForTheWeek;
    }

    public String getNumberOfSanityTestsDesignedForTheWeekName() {
        return "Number Of Sanity Tests Designed For The Week";
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

}