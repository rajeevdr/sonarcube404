package com.packt.cardatabase.domain;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.Iterator;

@Entity
public class MonthlyTestDesignProgress {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private String totalNoOfTestCasesReusedForMultiCountryRollouts;
    private String totalNumberOfManualTestCasesDesigned;
    private String totalNumberOfTestCasesTaggedForMigrationExecution;
    private int monthNumber;
    private int yearNumber;
    private String actualStartDateMilestone;
    private String actualEndDateMilestone;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public MonthlyTestDesignProgress() {}

    public MonthlyTestDesignProgress(
                                      String totalNoOfTestCasesReusedForMultiCountryRollouts,
                                      String totalNumberOfManualTestCasesDesigned,
                                      String totalNumberOfTestCasesTaggedForMigrationExecution,
                                      int monthNumber,
                                      int yearNumber,
                                      String actualStartDateMilestone,
                                      String actualEndDateMilestone,
                                      Project project)  {
        super();
        this.totalNoOfTestCasesReusedForMultiCountryRollouts = totalNoOfTestCasesReusedForMultiCountryRollouts;
        this.totalNumberOfManualTestCasesDesigned = totalNumberOfManualTestCasesDesigned;
        this.totalNumberOfTestCasesTaggedForMigrationExecution = totalNumberOfTestCasesTaggedForMigrationExecution;
        this.monthNumber = monthNumber;
        this.yearNumber = yearNumber;
        this.actualStartDateMilestone = actualStartDateMilestone;
        this.actualEndDateMilestone = actualEndDateMilestone;
        this.project = project;
    }

    public int getMonthNumber() {
        return monthNumber;
    }
    public String getMonthNumberName() {
        return "Month";
    }
    public void setMonthNumber(int monthNumber) {
        this.monthNumber = monthNumber;
    }

    public int getYearNumber() {
        return yearNumber;
    }
    public String getYearNumberName() {
        return "Year";
    }
    public void setYearNumber(int yearNumber) {
        this.yearNumber = yearNumber;
    }


    public int getTotalNoOfTestCasesReusedForMultiCountryRollouts() {

        if(totalNoOfTestCasesReusedForMultiCountryRollouts == null)
            return 0;
        return Integer.parseInt(totalNoOfTestCasesReusedForMultiCountryRollouts);
    }

    public String getTotalNoOfTestCasesReusedForMultiCountryRolloutsName() {
        return "Total Number Of TestCases Reused For MultiCountry Rollouts";
    }

    public void setTotalNoOfTestCasesReusedForMultiCountryRollouts(String totalNoOfTestCasesReusedForMultiCountryRollouts) {
        this.totalNoOfTestCasesReusedForMultiCountryRollouts = totalNoOfTestCasesReusedForMultiCountryRollouts;
    }

    public int getTotalNumberOfManualTestCasesDesigned() {

        if(totalNumberOfManualTestCasesDesigned == null)
            return 0;
        return Integer.parseInt(totalNumberOfManualTestCasesDesigned);

    }


    public String getTotalNumberOfManualTestCasesDesignedName() {
        return "Total Number Of Manual TestCases Designed";
    }

    public void setTotalNumberOfManualTestCasesDesigned(String totalNumberOfManualTestCasesDesigned) {
        this.totalNumberOfManualTestCasesDesigned = totalNumberOfManualTestCasesDesigned;
    }

    public int getTotalNumberOfTestCasesTaggedForMigrationExecution() {

        if(totalNumberOfTestCasesTaggedForMigrationExecution == null)
            return 0;
        return Integer.parseInt(totalNumberOfTestCasesTaggedForMigrationExecution);

    }

    public String getTotalNumberOfTestCasesTaggedForMigrationExecutionName() {
        return "Total Number Of TestCases Tagged For Migration Execution";
    }

    public void setTotalNumberOfTestCasesTaggedForMigrationExecution(String totalNumberOfTestCasesTaggedForMigrationExecution) {
        this.totalNumberOfTestCasesTaggedForMigrationExecution = totalNumberOfTestCasesTaggedForMigrationExecution;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String getActualStartDateMilestone() {
        return actualStartDateMilestone;
    }

    public String getActualStartDateMilestoneName() {
        return "Actual Start Date Milestone";
    }


    public void setActualStartDateMilestone(String actualStartDateMilestone) {
        this.actualStartDateMilestone = actualStartDateMilestone;
    }

    public String getActualEndDateMilestone() {
        return actualEndDateMilestone;
    }

    public String getActualEndDateMilestoneName() {
        return "Actual End Date Milestone";
    }

    public void setActualEndDateMilestone(String actualEndDateMilestone) {
        this.actualEndDateMilestone = actualEndDateMilestone;
    }


}