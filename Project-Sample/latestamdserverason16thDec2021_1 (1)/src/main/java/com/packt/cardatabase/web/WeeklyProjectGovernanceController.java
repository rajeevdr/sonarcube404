package com.packt.cardatabase.web;

import com.google.gson.Gson;
import com.packt.cardatabase.domain.Module;
import com.packt.cardatabase.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;


@RestController
public class WeeklyProjectGovernanceController {

	@Autowired
	private ProjectRepository projectRepository;
	
	@Autowired
	private WeeklyProjectGovernanceRepository WeeklyProjectGovernanceRepository;

	@Autowired
	private WeekDataRepository weekDataRepository;


	@RequestMapping(value = "/projects/{id}/governance/weeklyProgress/week/{week}/year/{year}", method = RequestMethod.GET)
	@ResponseBody
	public String getWeeklyGovernanceProgress(@PathVariable("id") long id, @PathVariable(value = "week") String week, @PathVariable(value = "year") String year) {

		try {

			ArrayList<ProjectProgressPojo> progressPojoArray = new ArrayList<ProjectProgressPojo>();
			StringTokenizer tokenizer = new StringTokenizer(week, "-");
			tokenizer.nextToken();
			int weekNumber = Integer.parseInt(tokenizer.nextToken());
			int yearNumber = Integer.parseInt(year);
			int monthNumber = 0;

			Iterable<WeekData> weekDataIterable = weekDataRepository.findAll();
			Iterator<WeekData> weekDataIterator = weekDataIterable.iterator();

			WeekData nextWeek = null;

			while (weekDataIterator.hasNext()) {
				nextWeek = weekDataIterator.next();
				if ((nextWeek.getWeekNumber() == weekNumber) && (nextWeek.getYearNumber() == yearNumber)) {
					monthNumber = nextWeek.getMonthNumber();
					break;
				}
			}

			System.out.println("The desired week number is : " + weekNumber);
			System.out.println("The desired month number is : " + monthNumber);
			System.out.println("The desired year number is : " + yearNumber);


			Iterable<WeeklyProjectGovernance> WeeklyProjectGovernance = WeeklyProjectGovernanceRepository.findAll();
			Iterator<WeeklyProjectGovernance> WeeklyProjectGovernanceRepositoryIterator = WeeklyProjectGovernance.iterator();
			String response = null;

			boolean recordFound = false;

			while (WeeklyProjectGovernanceRepositoryIterator.hasNext()) {
				WeeklyProjectGovernance next = WeeklyProjectGovernanceRepositoryIterator.next();
				if ((next.getWeekNumber() == weekNumber) && (next.getProject().getId() == id) && (next.getYearNumber() == yearNumber)) {
					recordFound = true;

					ProjectProgressPojo projectProgressPojo = null;
					int index = 0;

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setCategory("RCA");
					projectProgressPojo.setName(next.getNumberOfFormalGovernanceMeetingsName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfFormalGovernanceMeetings()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setCategory("RCA");
					projectProgressPojo.setName(next.getRCAPoorRequirementName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getRCAPoorRequirement()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setCategory("RCA");
					projectProgressPojo.setName(next.getRCALateRequirementName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getRCALateRequirement()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setCategory("RCA");
					projectProgressPojo.setName(next.getRCADesignErrorName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getRCADesignError()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setCategory("RCA");
					projectProgressPojo.setName(next.getRCACodingErrorName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getRCACodingError()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setCategory("RCA");
					projectProgressPojo.setName(next.getRCATestDataName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getRCATestData()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setCategory("RCA");
					projectProgressPojo.setName(next.getRCATestEnvironmentConfigurationName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getRCATestEnvironmentConfiguration()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setCategory("RCA");
					projectProgressPojo.setName(next.getRCAProcessRelatedName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getRCAProcessRelated()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setCategory("RCA");
					projectProgressPojo.setName(next.getRCAInsufficientTestingName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getRCAInsufficientTesting()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setCategory("RCA");
					projectProgressPojo.setName(next.getRCAOthersName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getRCAOthers()));}catch(Exception e){projectProgressPojo.setValue("0");} projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);


					Gson gson = new Gson();
					response = gson.toJson(progressPojoArray);
					response = "{\"progress\":" + response + "}";

					System.out.println(response);

					return response;
				}
			}

			if (recordFound == false) {
				ProjectProgressPojo projectProgressPojo = null;
				WeeklyProjectGovernance next = new WeeklyProjectGovernance();

				int index = 0;
				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);projectProgressPojo.setCategory("RCA");projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
				projectProgressPojo.setName(next.getNumberOfFormalGovernanceMeetingsName());
				projectProgressPojo.setValue("0");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);projectProgressPojo.setCategory("RCA");projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
				projectProgressPojo.setName(next.getRCAPoorRequirementName());
				projectProgressPojo.setValue("0");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);projectProgressPojo.setCategory("RCA");projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
				projectProgressPojo.setName(next.getRCALateRequirementName());
				projectProgressPojo.setValue("0");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);projectProgressPojo.setCategory("RCA");projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
				projectProgressPojo.setName(next.getRCADesignErrorName());
				projectProgressPojo.setValue("0");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);projectProgressPojo.setCategory("RCA");projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
				projectProgressPojo.setName(next.getRCACodingErrorName());
				projectProgressPojo.setValue("0");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);projectProgressPojo.setCategory("RCA");projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
				projectProgressPojo.setName(next.getRCATestDataName());
				projectProgressPojo.setValue("0");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);projectProgressPojo.setCategory("RCA");projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
				projectProgressPojo.setName(next.getRCATestEnvironmentConfigurationName());
				projectProgressPojo.setValue("0");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);projectProgressPojo.setCategory("RCA");projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
				projectProgressPojo.setName(next.getRCAProcessRelatedName());
				projectProgressPojo.setValue("0");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);projectProgressPojo.setCategory("RCA");projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
				projectProgressPojo.setName(next.getRCAInsufficientTestingName());
				projectProgressPojo.setValue("0");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);projectProgressPojo.setCategory("RCA");projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
				projectProgressPojo.setName(next.getRCAOthersName());
				projectProgressPojo.setValue("0");
				progressPojoArray.add(projectProgressPojo);


				Gson gson = new Gson();
				response = gson.toJson(progressPojoArray);

				System.out.println(response);
				response = "{\"progress\":" + response + "}";

				return response;


			}
		} catch (Exception e) {

		}
		return null;
	}



	@RequestMapping(value = "/projects/{id}/governance/totalWeeklyGovernanceProgress", method = RequestMethod.GET)
	@ResponseBody
	public String getCompleteWeeklyGovernanceProgress(@PathVariable("id") long id) {

		try {

			Iterable<WeeklyProjectGovernance> WeeklyProjectGovernance = WeeklyProjectGovernanceRepository.findAll();
			Iterator<WeeklyProjectGovernance> WeeklyProjectGovernanceRepositoryIterator = WeeklyProjectGovernance.iterator();
			String response = null;

			ArrayList<ArrayList<ProjectProgressPojo>> weeklyProjectGovernanceArray = new ArrayList<ArrayList<ProjectProgressPojo>>();

			while (WeeklyProjectGovernanceRepositoryIterator.hasNext()) {

				WeeklyProjectGovernance next = WeeklyProjectGovernanceRepositoryIterator.next();
				if (next.getProject().getId() == id) {

					ArrayList<ProjectProgressPojo> progressPojoArray = new ArrayList<ProjectProgressPojo>();
					ProjectProgressPojo projectProgressPojo = null;
					int index = 0;

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("Index");
					projectProgressPojo.setName(next.getWeekNumberName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getWeekNumber()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("Index");
					projectProgressPojo.setName(next.getYearNumberName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getYearNumber()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);


					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setCategory("RCA");
					projectProgressPojo.setName(next.getNumberOfFormalGovernanceMeetingsName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfFormalGovernanceMeetings()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setCategory("RCA");
					projectProgressPojo.setName(next.getRCAPoorRequirementName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getRCAPoorRequirement()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setCategory("RCA");
					projectProgressPojo.setName(next.getRCALateRequirementName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getRCALateRequirement()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setCategory("RCA");
					projectProgressPojo.setName(next.getRCADesignErrorName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getRCADesignError()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setCategory("RCA");
					projectProgressPojo.setName(next.getRCACodingErrorName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getRCACodingError()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setCategory("RCA");
					projectProgressPojo.setName(next.getRCATestDataName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getRCATestData()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setCategory("RCA");
					projectProgressPojo.setName(next.getRCATestEnvironmentConfigurationName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getRCATestEnvironmentConfiguration()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setCategory("RCA");
					projectProgressPojo.setName(next.getRCAProcessRelatedName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getRCAProcessRelated()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setCategory("RCA");
					projectProgressPojo.setName(next.getRCAInsufficientTestingName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getRCAInsufficientTesting()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setCategory("RCA");
					projectProgressPojo.setName(next.getRCAOthersName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getRCAOthers()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					weeklyProjectGovernanceArray.add(progressPojoArray);
				}
			}


					Gson gson = new Gson();
					response = gson.toJson(weeklyProjectGovernanceArray);
					response = "{\"progress\":" + response + "}";

					System.out.println(response);

					return response;


		} catch (Exception e) {

		}
		return null;
	}


	//	http://localhost:8082/springmvc/tutorial?topic=requestParam&author=daniel&site=jcg
	@PostMapping(value = "/projects/{id}/governance/weeklyProgress/week/{week}/year/{year}")
	@ResponseBody
	public String updateWeeklyGovernanceProgress(@PathVariable("id") long id, @PathVariable(value = "week") String week, @PathVariable(value = "year") String year, @RequestBody List<ProjectProgressPojo> progressPojoArray) {

		try {

			StringTokenizer weekTokenizer = new StringTokenizer(week, "-");
			weekTokenizer.nextToken();
			int weekNumber = Integer.parseInt(weekTokenizer.nextToken());
			int yearNumber = Integer.parseInt(year);
			int monthNumber = 0;

			Iterable<WeekData> weekDataIterable = weekDataRepository.findAll();
			Iterator<WeekData> weekDataIterator = weekDataIterable.iterator();

			WeekData nextWeek = null;

			while (weekDataIterator.hasNext()) {
				nextWeek = weekDataIterator.next();
				if ((nextWeek.getWeekNumber() == weekNumber) && (nextWeek.getYearNumber() == yearNumber)) {
					monthNumber = nextWeek.getMonthNumber();
					break;
				}
			}

			Iterable<WeeklyProjectGovernance> WeeklyProjectGovernanceIterable = WeeklyProjectGovernanceRepository.findAll();
			Iterator<WeeklyProjectGovernance> WeeklyProjectGovernanceRepositoryIterator = WeeklyProjectGovernanceIterable.iterator();

			boolean matchFound = false;

			while (WeeklyProjectGovernanceRepositoryIterator.hasNext()) {

				WeeklyProjectGovernance next = WeeklyProjectGovernanceRepositoryIterator.next();
				if ((next.getWeekNumber() == weekNumber) && (next.getProject().getId() == id) && (next.getYearNumber() == yearNumber)) {
					matchFound = true;

					for (int i = 0; i < progressPojoArray.size(); i++) {

						try {

                            if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfFormalGovernanceMeetingsName())) {

								String previousEntry = Integer.toString(next.getNumberOfFormalGovernanceMeetings());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setNumberOfFormalGovernanceMeetings(currentUpdatesEntry);
									}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getRCAPoorRequirementName())) {

								String previousEntry = Integer.toString(next.getRCAPoorRequirement());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setRCAPoorRequirement(currentUpdatesEntry);
									}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getRCALateRequirementName())) {

								String previousEntry = Integer.toString(next.getRCALateRequirement());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setRCALateRequirement(currentUpdatesEntry);
									}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getRCADesignErrorName())) {

								String previousEntry = Integer.toString(next.getRCADesignError());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setRCADesignError(currentUpdatesEntry);
									}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getRCACodingErrorName())) {

								String previousEntry = Integer.toString(next.getRCACodingError());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setRCACodingError(currentUpdatesEntry);
									}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getRCATestDataName())) {

								String previousEntry = Integer.toString(next.getRCATestData());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setRCATestData(currentUpdatesEntry);
								}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getRCATestEnvironmentConfigurationName())) {

								String previousEntry = Integer.toString(next.getRCATestEnvironmentConfiguration());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setRCATestEnvironmentConfiguration(currentUpdatesEntry);
								}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getRCAProcessRelatedName())) {

								String previousEntry = Integer.toString(next.getRCAProcessRelated());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setRCAProcessRelated(currentUpdatesEntry);
									}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getRCAInsufficientTestingName())) {

								String previousEntry = Integer.toString(next.getRCAInsufficientTesting());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setRCAInsufficientTesting(currentUpdatesEntry);
									}

							} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getRCAOthersName())) {

								String previousEntry = Integer.toString(next.getRCAOthers());
								String currentUpdatesEntry = progressPojoArray.get(i).getValue();

								if (currentUpdatesEntry != previousEntry) {
									next.setRCAOthers(currentUpdatesEntry);
									}

							}
						} catch (Exception e) {
							System.out.println("The exception message for Test execution update :" + e.getMessage());
						}
					}

					next.setWeekNumber(weekNumber);
					next.setMonthNumber(monthNumber);
					next.setYearNumber(yearNumber);

					WeeklyProjectGovernanceRepository.save(next);
					break;
				}
			}

			if (matchFound == false) {


				WeeklyProjectGovernance next = null;

				Iterable<Project> allProjects = projectRepository.findAll();
				Iterator<Project> iterator = allProjects.iterator();

				while (iterator.hasNext()) {
					Project nextProject = iterator.next();

					if (nextProject.getId() == id) {
						next = new WeeklyProjectGovernance("0", "0", "0", "0", "0", "0", "0", "0", "0", "0", weekNumber, monthNumber, yearNumber, nextProject);
						break;
					}
				}

				for (int i = 0; i < progressPojoArray.size(); i++) {

					try {
						if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfFormalGovernanceMeetingsName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setNumberOfFormalGovernanceMeetings(currentUpdatesEntry);
								}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getRCAPoorRequirementName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setRCAPoorRequirement(currentUpdatesEntry);
								}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getRCALateRequirementName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setRCALateRequirement(currentUpdatesEntry);
								}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getRCADesignErrorName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setRCADesignError(currentUpdatesEntry);
								}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getRCACodingErrorName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setRCACodingError(currentUpdatesEntry);
								}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getRCATestDataName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setRCATestData(currentUpdatesEntry);
								}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getRCATestEnvironmentConfigurationName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setRCATestEnvironmentConfiguration(currentUpdatesEntry);
								}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getRCAProcessRelatedName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setRCAProcessRelated(currentUpdatesEntry);
								}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getRCAInsufficientTestingName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setRCAInsufficientTesting(currentUpdatesEntry);
								}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getRCAOthersName())) {

							String previousEntry = "";
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if (currentUpdatesEntry != previousEntry) {
								next.setRCAOthers(currentUpdatesEntry);
								}

						}
					} catch (Exception e) {
						System.out.println("The exception message for Test execution update :" + e.getMessage());
					}
				}
				next.setWeekNumber(weekNumber);
				next.setMonthNumber(monthNumber);
				next.setYearNumber(yearNumber);
				WeeklyProjectGovernanceRepository.save(next);
			}

		} catch (Exception e) {
			System.out.println("Exception occured during module progress update : " + e.getMessage());
		}
		return "OK";
	}
}
			
