package com.packt.cardatabase.domain;


import javax.persistence.*;

@Entity
public class FirstProjectEstimate {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private String projectTimeStamp;
    private String plannedStartDate;
    private String plannedEndDate;
    private String regressionAutomationTestCaseEstimation;
    private String aPIAutomationTestCaseEstimation;
    private String inProjectAutomationTestCaseEstimation;
    private String testPlanningEffort;
    private String manualTestCaseDesignEffort;
    private String automationTestCaseDesignEffort;
    private String regressionAutomationTestCaseDesignEffort;
    private String inProjectAutomationTestCaseDesignEffort;
    private String apiAutomationTestCaseDesignEffort;
    private String manualTestCaseExecutionEffort;
    private String automationTestCaseExecutionEffort;
    private String regressionAutomationTestCaseExecutionEffort;
    private String inProjectAutomationTestCaseExecutionEffort;
    private String apiAutomationTestCaseExecutionEffort;
    private String totalNumberOfTestCasesTaggedForMigrationExecution;
    private String manualTestCaseMaintenanceEffort;
    private String automationTestCaseMaintenanceEffort;
    private String totalEffortInPersonDays;
    private String plannedResourcesForManualTestCaseExecution;
    private String plannedResourcesForManualTestCaseAuthoring;
    private String plannedResourcesForAutomationDesign;
    private String plannedResourcesForAutomationExecution;
    private String totalNumberOfRequirementsPlanned;
    private String numberOfTestCasesDesignedPerHour;
    private String numberOfTestCasesExecutedPerHour;


    private String projectedBilling;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public FirstProjectEstimate() {}


    public FirstProjectEstimate(String projectTimeStamp,
                                String plannedStartDate,
                                String plannedEndDate,
                                String regressionAutomationTestCaseEstimation,
                                String aPIAutomationTestCaseEstimation,
                                String inProjectAutomationTestCaseEstimation,
                                String testPlanningEffort,
                                String manualTestCaseDesignEffort,
                                String automationTestCaseDesignEffort,
                                String regressionAutomationTestCaseDesignEffort,
                                String inProjectAutomationTestCaseDesignEffort,
                                String apiAutomationTestCaseDesignEffort,
                                String manualTestCaseExecutionEffort,
                                String automationTestCaseExecutionEffort,
                                String regressionAutomationTestCaseExecutionEffort,
                                String inProjectAutomationTestCaseExecutionEffort,
                                String apiAutomationTestCaseExecutionEffort,
                                String totalNumberOfTestCasesTaggedForMigrationExecution,
                                String manualTestCaseMaintenanceEffort,
                                String automationTestCaseMaintenanceEffort,
                                String totalEffortInPersonDays,
                                String plannedResourcesForManualTestCaseExecution,
                                String plannedResourcesForManualTestCaseAuthoring,
                                String plannedResourcesForAutomationDesign,
                                String plannedResourcesForAutomationExecution,
                                String totalNumberOfRequirementsPlanned,
                                String projectedBilling,
                                String numberOfTestCasesDesignedPerHour,
                                String numberOfTestCasesExecutedPerHour,
                                Project project) {
        super();
        this.projectTimeStamp = projectTimeStamp;
        this.plannedStartDate = plannedStartDate;
        this.plannedEndDate = plannedEndDate;
        this.regressionAutomationTestCaseEstimation = regressionAutomationTestCaseEstimation;
        this.aPIAutomationTestCaseEstimation = aPIAutomationTestCaseEstimation;
        this.inProjectAutomationTestCaseEstimation = inProjectAutomationTestCaseEstimation;
        this.testPlanningEffort = testPlanningEffort;
        this.manualTestCaseDesignEffort = manualTestCaseDesignEffort;
        this.automationTestCaseDesignEffort = automationTestCaseDesignEffort;
        this.regressionAutomationTestCaseDesignEffort = regressionAutomationTestCaseDesignEffort;
        this.inProjectAutomationTestCaseDesignEffort = inProjectAutomationTestCaseDesignEffort;
        this.apiAutomationTestCaseDesignEffort = apiAutomationTestCaseDesignEffort;
        this.manualTestCaseExecutionEffort = manualTestCaseExecutionEffort;
        this.automationTestCaseExecutionEffort = automationTestCaseExecutionEffort;
        this.regressionAutomationTestCaseExecutionEffort = regressionAutomationTestCaseExecutionEffort;
        this.inProjectAutomationTestCaseExecutionEffort = inProjectAutomationTestCaseExecutionEffort;
        this.apiAutomationTestCaseExecutionEffort = apiAutomationTestCaseExecutionEffort;
        this.totalNumberOfTestCasesTaggedForMigrationExecution = totalNumberOfTestCasesTaggedForMigrationExecution;
        this.manualTestCaseMaintenanceEffort = manualTestCaseMaintenanceEffort;
        this.automationTestCaseMaintenanceEffort = automationTestCaseMaintenanceEffort;
        this.totalEffortInPersonDays = totalEffortInPersonDays;
        this.plannedResourcesForManualTestCaseExecution = plannedResourcesForManualTestCaseExecution;
        this.plannedResourcesForManualTestCaseAuthoring = plannedResourcesForManualTestCaseAuthoring;
        this.plannedResourcesForAutomationDesign = plannedResourcesForAutomationDesign;
        this.plannedResourcesForAutomationExecution = plannedResourcesForAutomationExecution;
        this.totalNumberOfRequirementsPlanned = totalNumberOfRequirementsPlanned;
        this.numberOfTestCasesDesignedPerHour = numberOfTestCasesDesignedPerHour;
        this.numberOfTestCasesExecutedPerHour = numberOfTestCasesExecutedPerHour;
        this.projectedBilling = projectedBilling;
        this.project = project;
    }

    public FirstProjectEstimate(ProjectEstimate projectEstimate) {
        super();
        this.projectTimeStamp = projectEstimate.getProjectTimeStamp();
        this.plannedStartDate = projectEstimate.getPlannedStartDate();
        this.plannedEndDate = projectEstimate.getPlannedEndDate();
        this.regressionAutomationTestCaseEstimation = projectEstimate.getRegressionAutomationTestCaseEstimation();
        this.aPIAutomationTestCaseEstimation = projectEstimate.getAPIAutomationTestCaseEstimation();
        this.inProjectAutomationTestCaseEstimation = projectEstimate.getInProjectAutomationTestCaseEstimation();
        this.testPlanningEffort = projectEstimate.getTestPlanningEffort();
        this.manualTestCaseDesignEffort = projectEstimate.getManualTestCaseDesignEffort();
        this.automationTestCaseDesignEffort = projectEstimate.getAutomationTestCaseDesignEffort();
        this.regressionAutomationTestCaseDesignEffort = projectEstimate.getRegressionAutomationTestCaseDesignEffort();
        this.inProjectAutomationTestCaseDesignEffort = projectEstimate.getInProjectAutomationTestCaseDesignEffort();
        this.apiAutomationTestCaseDesignEffort = projectEstimate.getApiAutomationTestCaseDesignEffort();
        this.manualTestCaseExecutionEffort = projectEstimate.getManualTestCaseExecutionEffort();
        this.automationTestCaseExecutionEffort = projectEstimate.getAutomationTestCaseExecutionEffort();
        this.regressionAutomationTestCaseExecutionEffort = projectEstimate.getRegressionAutomationTestCaseExecutionEffort();
        this.inProjectAutomationTestCaseExecutionEffort = projectEstimate.getInProjectAutomationTestCaseExecutionEffort();
        this.apiAutomationTestCaseExecutionEffort = projectEstimate.getApiAutomationTestCaseExecutionEffort();
        this.totalNumberOfTestCasesTaggedForMigrationExecution = projectEstimate.getTotalNumberOfTestCasesTaggedForMigrationExecution();
        this.manualTestCaseMaintenanceEffort = projectEstimate.getManualTestCaseMaintenanceEffort();
        this.automationTestCaseMaintenanceEffort = projectEstimate.getAutomationTestCaseMaintenanceEffort();
        this.totalEffortInPersonDays = projectEstimate.getTotalEffortInPersonDays();
        this.plannedResourcesForManualTestCaseExecution = projectEstimate.getPlannedResourcesForManualTestCaseExecution();
        this.plannedResourcesForManualTestCaseAuthoring = projectEstimate.getPlannedResourcesForManualTestCaseAuthoring();
        this.plannedResourcesForAutomationDesign = projectEstimate.getPlannedResourcesForAutomationDesign();
        this.plannedResourcesForAutomationExecution = projectEstimate.getPlannedResourcesForAutomationExecution();
        this.projectedBilling = projectEstimate.getProjectedBilling();
        this.totalNumberOfRequirementsPlanned = projectEstimate.getTotalNumberOfRequirementsPlanned();
        this.numberOfTestCasesDesignedPerHour = projectEstimate.getNumberOfTestCasesDesignedPerHour();
        this.numberOfTestCasesExecutedPerHour = projectEstimate.getNumberOfTestCasesExecutedPerHour();

        this.project = projectEstimate.getProject();
    }

    public String getProjectTimeStamp() {
        return projectTimeStamp;
    }

    public void setProjectTimeStamp(String projectTimeStamp) {
        this.projectTimeStamp = projectTimeStamp;
    }

    public String getPlannedStartDate() {
        return plannedStartDate;
    }

    public void setPlannedStartDate(String plannedStartDate) {
        this.plannedStartDate = plannedStartDate;
    }

    public String getPlannedEndDate() {
        return plannedEndDate;
    }

    public void setPlannedEndDate(String plannedEndDate) {
        this.plannedEndDate = plannedEndDate;
    }

    public String getRegressionAutomationTestCaseEstimation() {
        return regressionAutomationTestCaseEstimation;
    }

    public void setRegressionAutomationTestCaseEstimation(String regressionAutomationTestCaseEstimation) {
        this.regressionAutomationTestCaseEstimation = regressionAutomationTestCaseEstimation;
    }

    public String getInProjectAutomationTestCaseEstimation() {
        return inProjectAutomationTestCaseEstimation;
    }

    public void setInProjectAutomationTestCaseEstimation(String inProjectAutomationTestCaseEstimation) {
        this.inProjectAutomationTestCaseEstimation = inProjectAutomationTestCaseEstimation;
    }

    public String getAPIAutomationTestCaseEstimation() {
        return aPIAutomationTestCaseEstimation;
    }

    public void setAPIAutomationTestCaseEstimation(String aPIAutomationTestCaseEstimation) {
        this.aPIAutomationTestCaseEstimation = aPIAutomationTestCaseEstimation;
    }


    public String getTestPlanningEffort() {
        return testPlanningEffort;
    }

    public void setTestPlanningEffort(String testPlanningEffort) {
        this.testPlanningEffort = testPlanningEffort;
    }

    public String getManualTestCaseDesignEffort() {
        return manualTestCaseDesignEffort;
    }

    public void setManualTestCaseDesignEffort(String manualTestCaseDesignEffort) {
        this.manualTestCaseDesignEffort = manualTestCaseDesignEffort;
    }

    public String getAutomationTestCaseDesignEffort() {
        return automationTestCaseDesignEffort;
    }

    public void setAutomationTestCaseDesignEffort(String automationTestCaseDesignEffort) {
        this.automationTestCaseDesignEffort = automationTestCaseDesignEffort;
    }

    public String getRegressionAutomationTestCaseDesignEffort() {
        return regressionAutomationTestCaseDesignEffort;
    }

    public void setRegressionAutomationTestCaseDesignEffort(String regressionAutomationTestCaseDesignEffort) {
        this.regressionAutomationTestCaseDesignEffort = regressionAutomationTestCaseDesignEffort;
    }

    public String getInProjectAutomationTestCaseDesignEffort() {
        return inProjectAutomationTestCaseDesignEffort;
    }

    public void setInProjectAutomationTestCaseDesignEffort(String inProjectAutomationTestCaseDesignEffort) {
        this.inProjectAutomationTestCaseDesignEffort = inProjectAutomationTestCaseDesignEffort;
    }

    public String getApiAutomationTestCaseDesignEffort() {
        return apiAutomationTestCaseDesignEffort;
    }

    public void setApiAutomationTestCaseDesignEffort(String apiAutomationTestCaseDesignEffort) {
        this.apiAutomationTestCaseDesignEffort = apiAutomationTestCaseDesignEffort;
    }


    public String getTotalNumberOfTestCasesTaggedForMigrationExecution() {
        return totalNumberOfTestCasesTaggedForMigrationExecution;
    }

    public void setTotalNumberOfTestCasesTaggedForMigrationExecution(String totalNumberOfTestCasesTaggedForMigrationExecution) {
        this.totalNumberOfTestCasesTaggedForMigrationExecution = totalNumberOfTestCasesTaggedForMigrationExecution;
    }


    public String getManualTestCaseExecutionEffort() {
        return manualTestCaseExecutionEffort;
    }

    public void setManualTestCaseExecutionEffort(String manualTestCaseExecutionEffort) {
        this.manualTestCaseExecutionEffort = manualTestCaseExecutionEffort;

    }


    public String getAutomationTestCaseExecutionEffort() {
        return automationTestCaseExecutionEffort;
    }

    public void setAutomationTestCaseExecutionEffort(String automationTestCaseExecutionEffort) {
        this.automationTestCaseExecutionEffort = automationTestCaseExecutionEffort;
    }


    public String getRegressionAutomationTestCaseExecutionEffort() {
        return regressionAutomationTestCaseExecutionEffort;
    }

    public void setRegressionAutomationTestCaseExecutionEffort(String regressionAutomationTestCaseExecutionEffort) {
        this.regressionAutomationTestCaseExecutionEffort = regressionAutomationTestCaseExecutionEffort;
    }

    public String getInProjectAutomationTestCaseExecutionEffort() {
        return inProjectAutomationTestCaseExecutionEffort;
    }

    public void setInProjectAutomationTestCaseExecutionEffort(String inProjectAutomationTestCaseExecutionEffort) {
        this.inProjectAutomationTestCaseExecutionEffort = inProjectAutomationTestCaseExecutionEffort;
    }

    public String getApiAutomationTestCaseExecutionEffort() {
        return apiAutomationTestCaseExecutionEffort;
    }

    public void setApiAutomationTestCaseExecutionEffort(String apiAutomationTestCaseExecutionEffort) {
        this.apiAutomationTestCaseExecutionEffort = apiAutomationTestCaseExecutionEffort;
    }


    public String getManualTestCaseMaintenanceEffort() {
        return manualTestCaseMaintenanceEffort;
    }

    public void setManualTestCaseMaintenanceEffort(String manualTestCaseMaintenanceEffort) {
        this.manualTestCaseMaintenanceEffort = manualTestCaseMaintenanceEffort;
    }

    public String getAutomationTestCaseMaintenanceEffort() {
        return automationTestCaseMaintenanceEffort;
    }

    public void setAutomationTestCaseMaintenanceEffort(String automationTestCaseMaintenanceEffort) {
        this.automationTestCaseMaintenanceEffort = automationTestCaseMaintenanceEffort;
    }

    public String getTotalEffortInPersonDays() {
        return totalEffortInPersonDays;
    }

    public void setTotalEffortInPersonDays(String totalEffortInPersonDays) {
        this.totalEffortInPersonDays = totalEffortInPersonDays;
    }

    public String getPlannedResourcesForManualTestCaseExecution() {
        return plannedResourcesForManualTestCaseExecution;
    }

    public void setPlannedResourcesForManualTestCaseExecution(String plannedResourcesForManualTestCaseExecution) {
        this.plannedResourcesForManualTestCaseExecution = plannedResourcesForManualTestCaseExecution;
    }

    public String getPlannedResourcesForManualTestCaseAuthoring() {
        return plannedResourcesForManualTestCaseAuthoring;
    }

    public void setPlannedResourcesForManualTestCaseAuthoring(String plannedResourcesForManualTestCaseAuthoring) {
        this.plannedResourcesForManualTestCaseAuthoring = plannedResourcesForManualTestCaseAuthoring;
    }

    public String getPlannedResourcesForAutomationDesign() {
        return plannedResourcesForAutomationDesign;
    }

    public void setPlannedResourcesForAutomationDesign(String plannedResourcesForAutomationDesign) {
        this.plannedResourcesForAutomationDesign = plannedResourcesForAutomationDesign;
    }

    public String getPlannedResourcesForAutomationExecution() {
        return plannedResourcesForAutomationExecution;
    }

    public void setPlannedResourcesForAutomationExecution(String plannedResourcesForAutomationExecution) {
        this.plannedResourcesForAutomationExecution = plannedResourcesForAutomationExecution;
    }

    public String getProjectedBilling() {
        return projectedBilling;
    }

    public void setProjectedBilling(String projectedBilling) {
        this.projectedBilling = projectedBilling;
    }

    public String getTotalNumberOfRequirementsPlanned() {
        return totalNumberOfRequirementsPlanned;
    }

    public void setTotalNumberOfRequirementsPlanned(String totalNumberOfRequirementsPlanned) {
        this.totalNumberOfRequirementsPlanned = totalNumberOfRequirementsPlanned;
    }


    public String getNumberOfTestCasesDesignedPerHour() {
        return numberOfTestCasesDesignedPerHour;
    }

    public void setNumberOfTestCasesDesignedPerHour(String numberOfTestCasesDesignedPerHour) {
        this.numberOfTestCasesDesignedPerHour = numberOfTestCasesDesignedPerHour;
    }


    public String getNumberOfTestCasesExecutedPerHour() {
        return numberOfTestCasesExecutedPerHour;
    }

    public void setNumberOfTestCasesExecutedPerHour(String numberOfTestCasesExecutedPerHour) {
        this.numberOfTestCasesExecutedPerHour = numberOfTestCasesExecutedPerHour;
    }


    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

}