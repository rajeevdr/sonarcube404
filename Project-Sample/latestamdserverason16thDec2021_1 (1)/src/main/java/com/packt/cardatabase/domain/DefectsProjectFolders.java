package com.packt.cardatabase.domain;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated("jsonschema2pojo")
public class DefectsProjectFolders {

private String projectFieldName;
private ArrayList<String> projectName = null;

public String getProjectFieldName() {
return projectFieldName;
}

public void setProjectFieldName(String projectFieldName) {
this.projectFieldName = projectFieldName;
}

public List<String> getProjectName() {
return projectName;
}

public void setProjectName(ArrayList<String> projectName) {
this.projectName = projectName;
}

}
