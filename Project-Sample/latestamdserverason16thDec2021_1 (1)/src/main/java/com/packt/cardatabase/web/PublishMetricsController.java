package com.packt.cardatabase.web;

import com.google.gson.Gson;
import com.packt.cardatabase.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@RestController
public class PublishMetricsController {

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private ManualTestCaseAuthoringProductivityRepository  manualTestCaseAuthoringProductivityRepository;

	@Autowired
	private AutomationTestDesignProductivityRepository  automationTestDesignProductivityRepository;

	@Autowired
	private ManualTestExecutionProductivityRepository  manualTestExecutionProductivityRepository;

	@Autowired
	private AutomationTestExecutionProductivityRepository  automationTestExecutionProductivityRepository;


	@RequestMapping(value = "/projects/{id}/printmetrics", method = RequestMethod.GET)
	@ResponseBody
	public String printMetrics(@PathVariable("id") long id) {

		try {

			System.out.println("Start printing Manual Test Design productivity >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			Iterable<ManualTestCaseAuthoringProductivity> manualTestCaseAuthoringProductivityIterable = manualTestCaseAuthoringProductivityRepository.findAll();
			Iterator<ManualTestCaseAuthoringProductivity> manualTestCaseAuthoringProductivityIterator = manualTestCaseAuthoringProductivityIterable.iterator();

			while (manualTestCaseAuthoringProductivityIterator.hasNext()) {
				System.out.println();
				System.out.println();

				ManualTestCaseAuthoringProductivity manualTestCaseAuthoringProductivity = manualTestCaseAuthoringProductivityIterator.next();

				if(manualTestCaseAuthoringProductivity.getProject().getId() == id) {

					System.out.println("Week Number : " + manualTestCaseAuthoringProductivity.getWeekNumber());
					System.out.println();

					int cummulativeManualTestCasesAdded = 0;

					for (int i = 0; i < manualTestCaseAuthoringProductivity.getNumberOfManualTestcasesAddedEachWeek().size(); i++) {
						System.out.println("Manual test cases added in the week number : " + manualTestCaseAuthoringProductivity.getWeekNumber() +
								" for the module : " +
								manualTestCaseAuthoringProductivity.getModuleNames().get(i) + " " +
								" = " +
								manualTestCaseAuthoringProductivity.getNumberOfManualTestcasesAddedEachWeek().get(i));

						cummulativeManualTestCasesAdded += Integer.parseInt(manualTestCaseAuthoringProductivity.getNumberOfManualTestcasesAddedEachWeek().get(i));
					}

					System.out.println("Total number of manual test cases added in the week : " + cummulativeManualTestCasesAdded);

					System.out.println("Effort spent on manual test cases design in the week : " + manualTestCaseAuthoringProductivity.getEffortSpentOnManualTestcaseDesignEachWeek());

					System.out.println("The manual test case authoring productivity : " +
							cummulativeManualTestCasesAdded + "/" + manualTestCaseAuthoringProductivity.getEffortSpentOnManualTestcaseDesignEachWeek() + " = "
							+ manualTestCaseAuthoringProductivity.getManualTestCaseAuthoringProductivity());

					System.out.println();
					System.out.println();
				}
			}

		}catch(Exception e){

		}



		try {
			System.out.println("Inside Automation Test Design Productivity >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

			Iterable<AutomationTestDesignProductivity> automationTestDesignProductivityIterable = automationTestDesignProductivityRepository.findAll();
			Iterator<AutomationTestDesignProductivity> automationTestDesignProductivityIterator = automationTestDesignProductivityIterable.iterator();

			while (automationTestDesignProductivityIterator.hasNext()) {
				System.out.println();
				System.out.println();

				AutomationTestDesignProductivity automationTestDesignProductivity = automationTestDesignProductivityIterator.next();

				if(automationTestDesignProductivity.getProject().getId() == id) {

					System.out.println("Week Number : " + automationTestDesignProductivity.getWeekNumber());
					System.out.println();

					int cummulativeAutomationScriptsAdded = 0;

					for (int i = 0; i < automationTestDesignProductivity.getNumberOfAutomationScriptsAddedEachWeek().size(); i++) {
						System.out.println("Automation test scripts added in the week number : " + automationTestDesignProductivity.getWeekNumber() +
								" for the module : " +
								automationTestDesignProductivity.getModuleNames().get(i) + " " +
								" = " +
								automationTestDesignProductivity.getNumberOfAutomationScriptsAddedEachWeek().get(i));

						cummulativeAutomationScriptsAdded += Integer.parseInt(automationTestDesignProductivity.getNumberOfAutomationScriptsAddedEachWeek().get(i));
					}

					System.out.println("Total number of automation test cases added in the week : " + cummulativeAutomationScriptsAdded);

					System.out.println("Effort spent on Automation test design in the week : " + automationTestDesignProductivity.getEffortSpentOnAutomationTestDesignEachWeek());

					System.out.println("The Automation test design productivity : " +
							cummulativeAutomationScriptsAdded + "/" + automationTestDesignProductivity.getEffortSpentOnAutomationTestDesignEachWeek() + " = "
							+ automationTestDesignProductivity.getAutomationTestDesignProductivity());

					System.out.println();
					System.out.println();
				}

			}

		}catch(Exception e){

		}


		try {

			System.out.println("Start printing Manual Test Execution productivity >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			Iterable<ManualTestExecutionProductivity> manualTestExecutionProductivityIterable = manualTestExecutionProductivityRepository.findAll();
			Iterator<ManualTestExecutionProductivity> manualTestExecutionProductivityIterator = manualTestExecutionProductivityIterable.iterator();

			while (manualTestExecutionProductivityIterator.hasNext()) {
				System.out.println();
				System.out.println();

				ManualTestExecutionProductivity manualTestExecutionProductivity = manualTestExecutionProductivityIterator.next();

				if(manualTestExecutionProductivity.getProject().getId() == id) {

					System.out.println("Week Number : " + manualTestExecutionProductivity.getWeekNumber());
					System.out.println();

					int cummulativeManualTestCasesExecuted = 0;

					for (int i = 0; i < manualTestExecutionProductivity.getNumberOfManualTestcasesExecutedEachWeek().size(); i++) {
						System.out.println("Manual test cases executed in the week number : " + manualTestExecutionProductivity.getWeekNumber() +
								" for the module : " +
								manualTestExecutionProductivity.getModuleNames().get(i) + " " +
								" = " +
								manualTestExecutionProductivity.getNumberOfManualTestcasesExecutedEachWeek().get(i));

						cummulativeManualTestCasesExecuted += Integer.parseInt(manualTestExecutionProductivity.getNumberOfManualTestcasesExecutedEachWeek().get(i));
					}

					System.out.println("Total number of manual test cases executed in the week : " + cummulativeManualTestCasesExecuted);

					System.out.println("Effort spent on manual test cases execution in the week : " + manualTestExecutionProductivity.getEffortSpentOnManualTestcaseExecutedEachWeek());

					System.out.println("The manual test case execution productivity : " +
							cummulativeManualTestCasesExecuted + "/" + manualTestExecutionProductivity.getEffortSpentOnManualTestcaseExecutedEachWeek() + " = "
							+ manualTestExecutionProductivity.getManualTestCaseExecutedProductivity());

					System.out.println();
					System.out.println();
				}
			}

		}catch(Exception e){

		}


		try {

			System.out.println("Start printing Automation Test Execution productivity >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			Iterable<AutomationTestExecutionProductivity> automationTestExecutionProductivityIterable = automationTestExecutionProductivityRepository.findAll();
			Iterator<AutomationTestExecutionProductivity> automationTestExecutionProductivityIterator = automationTestExecutionProductivityIterable.iterator();

			while (automationTestExecutionProductivityIterator.hasNext()) {
				System.out.println();
				System.out.println();

				AutomationTestExecutionProductivity automationTestExecutionProductivity = automationTestExecutionProductivityIterator.next();

				if(automationTestExecutionProductivity.getProject().getId() == id) {

					System.out.println("Week Number : " + automationTestExecutionProductivity.getWeekNumber());
					System.out.println();

					int cummulativeAutomationTestCasesExecuted = 0;

					for (int i = 0; i < automationTestExecutionProductivity.getNumberOfAutomationScriptsExecutedEachWeek().size(); i++) {
						System.out.println("Automation test cases executed in the week number : " + automationTestExecutionProductivity.getWeekNumber() +
								" for the module : " +
								automationTestExecutionProductivity.getModuleNames().get(i) + " " +
								" = " +
								automationTestExecutionProductivity.getNumberOfAutomationScriptsExecutedEachWeek().get(i));

						cummulativeAutomationTestCasesExecuted += Integer.parseInt(automationTestExecutionProductivity.getNumberOfAutomationScriptsExecutedEachWeek().get(i));
					}

					System.out.println("Total number of test cases executed in the week : " + cummulativeAutomationTestCasesExecuted);

					System.out.println("Effort spent on automation test cases execution in the week : " + automationTestExecutionProductivity.getEffortSpentOnAutomationTestExecutionEachWeek());

					System.out.println("The automation test case execution productivity : " +
							cummulativeAutomationTestCasesExecuted + "/" + automationTestExecutionProductivity.getEffortSpentOnAutomationTestExecutionEachWeek() + " = "
							+ automationTestExecutionProductivity.getAutomationTestExecutionProductivity());

					System.out.println();
					System.out.println();
				}
			}

		}catch(Exception e){

		}

		return "OK";
	}
}