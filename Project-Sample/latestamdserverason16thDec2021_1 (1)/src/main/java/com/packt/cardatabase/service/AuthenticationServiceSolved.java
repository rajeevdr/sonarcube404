package com.packt.cardatabase.service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

import static java.util.Collections.emptyList;

public class AuthenticationServiceSolved {
    private AuthenticationServiceSolved() {
       throw new IllegalStateException("Utility class");
    }
    static final long EXPIRATION_TIME = 864_000_00; // 1 day in milliseconds
    static final String SIGNING_KEY = "SecretKey";
    static final String PREFIX = "Bearer";

    public static  void addToken(HttpServletResponse res, String username) {
        String jwt_token = Jwts.builder().setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SIGNING_KEY)
                .compact();
        res.addHeader("test1", PREFIX + " " + jwt_token);
        res.addHeader("Access-Control-Expose-Headers", "test2");
    }

    public static  Authentication getAuthentication(HttpServletRequest request) {
        String token = request.getHeader("test3");
        if (token != null) {
            String user = Jwts.parser()
                    .setSigningKey(SIGNING_KEY)
                    .parseClaimsJws(token.replace(PREFIX, ""))
                    .getBody()
                    .getSubject();

            if (user != null)
                return new UsernamePasswordAuthenticationToken(user, null, emptyList());
        }
        return null;
    }
}