package com.packt.cardatabase.domain;


import javax.persistence.*;


@Entity
public class SyncStartAndEndDateUpdate {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;

    private int desiredSyncStartDateNumber;
    private int desiredSyncStartDateWeekNumber;
    private int desiredSyncStartDateMonthNumber;
    private int desiredSyncStartDateYearNumber;
    private int desiredSyncStartDateQuarterNumber;

    private int desiredSyncEndDateNumber;
    private int desiredSyncEndDateWeekNumber;
    private int desiredSyncEndDateMonthNumber;
    private int desiredSyncEndDateYearNumber;
    private int desiredSyncEndDateQuarterNumber;


    public SyncStartAndEndDateUpdate() {}

    public SyncStartAndEndDateUpdate(int desiredSyncStartDateNumber,
                                     int desiredSyncStartDateWeekNumber,
                                     int desiredSyncStartDateMonthNumber,
                                     int desiredSyncStartDateYearNumber,
                                     int desiredSyncStartDateQuarterNumber,
                                     int desiredSyncEndDateNumber,
                                     int desiredSyncEndDateWeekNumber,
                                     int desiredSyncEndDateMonthNumber,
                                     int desiredSyncEndDateYearNumber,
                                     int desiredSyncEndDateQuarterNumber,
                                     Project project) {
        super();
        this.desiredSyncStartDateNumber = desiredSyncStartDateNumber;
        this.desiredSyncStartDateWeekNumber = desiredSyncStartDateWeekNumber;
        this.desiredSyncStartDateMonthNumber = desiredSyncStartDateMonthNumber;
        this.desiredSyncStartDateYearNumber = desiredSyncStartDateYearNumber;
        this.desiredSyncStartDateQuarterNumber = desiredSyncStartDateQuarterNumber;
        this.desiredSyncEndDateNumber = desiredSyncEndDateNumber;
        this.desiredSyncEndDateWeekNumber = desiredSyncEndDateWeekNumber;
        this.desiredSyncEndDateMonthNumber = desiredSyncEndDateMonthNumber;
        this.desiredSyncEndDateYearNumber = desiredSyncEndDateYearNumber;
        this.desiredSyncEndDateQuarterNumber = desiredSyncEndDateQuarterNumber;

        this.project = project;
    }


    public int getDesiredSyncEndDateNumber() {
        return desiredSyncEndDateNumber;
    }

    public void setDesiredSyncEndDateNumber(int desiredSyncEndDateNumber) {
        this.desiredSyncEndDateNumber = desiredSyncEndDateNumber;
    }

    public int getDesiredSyncStartDateNumber() {
        return desiredSyncStartDateNumber;
    }

    public void setDesiredSyncStartDateNumber(int desiredSyncStartDateNumber) {
        this.desiredSyncStartDateNumber = desiredSyncStartDateNumber;
    }

    public int getDesiredSyncStartDateWeekNumber() {
        return desiredSyncStartDateWeekNumber;
    }

    public void setDesiredSyncStartDateWeekNumber(int desiredSyncStartDateWeekNumber) {
        this.desiredSyncStartDateWeekNumber = desiredSyncStartDateWeekNumber;
    }

    public int getDesiredSyncStartDateMonthNumber() {
        return desiredSyncStartDateMonthNumber;
    }

    public void setDesiredSyncStartDateMonthNumber(int desiredSyncStartDateMonthNumber) {
        this.desiredSyncStartDateMonthNumber = desiredSyncStartDateMonthNumber;
    }

    public int getDesiredSyncStartDateYearNumber() {
        return desiredSyncStartDateYearNumber;
    }

    public void setDesiredSyncStartDateYearNumber(int desiredSyncStartDateYearNumber) {
        this.desiredSyncStartDateYearNumber = desiredSyncStartDateYearNumber;
    }

    public int getDesiredSyncStartDateQuarterNumber() {
        return desiredSyncStartDateQuarterNumber;
    }

    public void setDesiredSyncStartDateQuarterNumber(int desiredSyncStartDateQuarterNumber) {
        this.desiredSyncStartDateQuarterNumber = desiredSyncStartDateQuarterNumber;
    }

    public int getDesiredSyncEndDateWeekNumber() {
        return desiredSyncEndDateWeekNumber;
    }

    public void setDesiredSyncEndDateWeekNumber(int desiredSyncEndDateWeekNumber) {
        this.desiredSyncEndDateWeekNumber = desiredSyncEndDateWeekNumber;
    }

    public int getDesiredSyncEndDateMonthNumber() {
        return desiredSyncEndDateMonthNumber;
    }

    public void setDesiredSyncEndDateMonthNumber(int desiredSyncEndDateMonthNumber) {
        this.desiredSyncEndDateMonthNumber = desiredSyncEndDateMonthNumber;
    }

    public int getDesiredSyncEndDateYearNumber() {
        return desiredSyncEndDateYearNumber;
    }

    public void setDesiredSyncEndDateYearNumber(int desiredSyncEndDateYearNumber) {
        this.desiredSyncEndDateYearNumber = desiredSyncEndDateYearNumber;
    }

    public int getDesiredSyncEndDateQuarterNumber() {
        return desiredSyncEndDateQuarterNumber;
    }

    public void setDesiredSyncEndDateQuarterNumber(int desiredSyncEndDateQuarterNumber) {
        this.desiredSyncEndDateQuarterNumber = desiredSyncEndDateQuarterNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

}