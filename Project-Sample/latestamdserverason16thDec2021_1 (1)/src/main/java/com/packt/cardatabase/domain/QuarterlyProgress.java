package com.packt.cardatabase.domain;

import javax.persistence.*;

@Entity
public class QuarterlyProgress {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private String totalQuarterlyBusinessReviewMeetings;
    private String projectedBillingForCurrentQuarter;
    private String actualBillingForCurrentQuarter;
    private String totalNumberOfCRsRaised;
    private String totalNumberOfCRsAccepted;
    private String totalNumberOfCRsInvoiced;
    private int quarterNumber;
    private int yearNumber;



    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public QuarterlyProgress() {}

    public QuarterlyProgress(String totalQuarterlyBusinessReviewMeetings,
                             String projectedBillingForCurrentQuarter,
                             String actualBillingForCurrentQuarter,
                             String totalNumberOfCRsRaised,
                             String totalNumberOfCRsAccepted,
                             String totalNumberOfCRsInvoiced,
                             int quarterNumber,
                             int yearNumber,
                             Project project) {
        super();
        this.totalQuarterlyBusinessReviewMeetings = totalQuarterlyBusinessReviewMeetings;
        this.projectedBillingForCurrentQuarter = projectedBillingForCurrentQuarter;
        this.actualBillingForCurrentQuarter = actualBillingForCurrentQuarter;
        this.totalNumberOfCRsRaised = totalNumberOfCRsRaised;
        this.totalNumberOfCRsAccepted = totalNumberOfCRsAccepted;
        this.totalNumberOfCRsInvoiced = totalNumberOfCRsInvoiced;
        this.quarterNumber = quarterNumber;
        this.yearNumber = yearNumber;
        this.project = project;
    }

    public int getYearNumber() {
        return yearNumber;
    }
    public String getYearNumberName() {
        return "YearNumber";
    }
    public void setYearNumber(int yearNumber) {
        this.yearNumber = yearNumber;
    }


    public int getQuarterNumber() {
        return quarterNumber;
    }
    public String getQuarterNumberName() {
        return "Quarter";
    }
    public void setQuarterNumber(int quarterNumber) {
        this.quarterNumber = quarterNumber;
    }

    public String getTotalQuarterlyBusinessReviewMeetings() {
        return totalQuarterlyBusinessReviewMeetings;
    }

    public String getTotalQuarterlyBusinessReviewMeetingsName() {
        return "Total Quarterly Business Review Meetings";
    }

    public void setTotalQuarterlyBusinessReviewMeetings(String totalQuarterlyBusinessReviewMeetings) {
        this.totalQuarterlyBusinessReviewMeetings = totalQuarterlyBusinessReviewMeetings;
    }

    public String getProjectedBillingForCurrentQuarter() {
        return projectedBillingForCurrentQuarter;
    }

    public String getProjectedBillingForCurrentQuarterName() {
        return "Projected Billing For Current Quarter";
    }

    public void setProjectedBillingForCurrentQuarter(String projectedBillingForCurrentQuarter) {
        this.projectedBillingForCurrentQuarter = projectedBillingForCurrentQuarter;
    }

    public String getActualBillingForCurrentQuarter() {
        return actualBillingForCurrentQuarter;
    }

    public String getActualBillingForCurrentQuarterName() {
        return "Actual Billing For Current Quarter";
    }

    public void setActualBillingForCurrentQuarter(String actualBillingForCurrentQuarter) {
        this.actualBillingForCurrentQuarter = actualBillingForCurrentQuarter;
    }

    public String getTotalNumberOfCRsRaised() {
        return totalNumberOfCRsRaised;
    }

    public String getTotalNumberOfCRsRaisedName() {
        return "Total Number Of CRs Raised";
    }

    public void setTotalNumberOfCRsRaised(String totalNumberOfCRsRaised) {
        this.totalNumberOfCRsRaised = totalNumberOfCRsRaised;
    }

    public String getTotalNumberOfCRsAccepted() {
        return totalNumberOfCRsAccepted;
    }

    public String getTotalNumberOfCRsAcceptedName() {
        return "Total Number Of CRs Accepted";
    }

    public void setTotalNumberOfCRsAccepted(String totalNumberOfCRsAccepted) {
        this.totalNumberOfCRsAccepted = totalNumberOfCRsAccepted;
    }

    public String getTotalNumberOfCRsInvoiced() {
        return totalNumberOfCRsInvoiced;
    }

    public String getTotalNumberOfCRsInvoicedName() {
        return "Total Number Of CRs Invoiced";
    }

    public void setTotalNumberOfCRsInvoiced(String totalNumberOfCRsInvoiced) {
        this.totalNumberOfCRsInvoiced = totalNumberOfCRsInvoiced;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

}