package com.packt.cardatabase.domain;


import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Entity
public class WeekMonthData {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;
    private int monthNumber;
    private int yearNumber;
    private ArrayList<Integer> weekNumbers;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public WeekMonthData() {}


    public WeekMonthData(int monthNumber, int yearNumber, ArrayList<Integer> weekNumbers) {
        super();
        this.monthNumber = monthNumber;
        this.yearNumber = yearNumber;
        this.weekNumbers = weekNumbers;
    }


    public ArrayList<Integer> getWeekNumbers() {
        return weekNumbers;
    }

    public void setWeekNumbers(ArrayList<String> weekNumber) {
        this.weekNumbers = weekNumbers;
    }

    public int getMonthNumber() {
        return monthNumber;
    }

    public void setMonthNumber(int monthNumber) {
        this.monthNumber = monthNumber;
    }

    public int getYearNumber() {
        return yearNumber;
    }

    public void setYearNumber(int yearNumber) {
        this.yearNumber = yearNumber;
    }

}