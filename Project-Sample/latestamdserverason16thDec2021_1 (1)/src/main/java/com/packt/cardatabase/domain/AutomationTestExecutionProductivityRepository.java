package com.packt.cardatabase.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


@RepositoryRestResource
public interface AutomationTestExecutionProductivityRepository extends CrudRepository <AutomationTestExecutionProductivity, Long> {

}
