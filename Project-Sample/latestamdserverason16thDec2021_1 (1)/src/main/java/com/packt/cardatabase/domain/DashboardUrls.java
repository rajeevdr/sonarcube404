package com.packt.cardatabase.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import javax.persistence.*;

@Entity
public class DashboardUrls {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private String name;
    private String type;
    private String value;
    private String category;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;

    public DashboardUrls() {

    }

    public DashboardUrls(String name, String type, String value, String category) {
        this.name = name;
        this.type = type;
        this.value = value;
        this.category = category;
    }

    public DashboardUrls(DashboardUrlsPojo dashboardUrlsPojo) {
        this.name = dashboardUrlsPojo.getName();
        this.type = dashboardUrlsPojo.getType();
        this.value = dashboardUrlsPojo.getValue();
        this.category = dashboardUrlsPojo.getCategory();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }


}
