package com.packt.cardatabase.web;

import com.google.gson.Gson;
import com.packt.cardatabase.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.StringTokenizer;

import com.packt.cardatabase.domain.Module;


@RestController
public class ALMSettingsController {

	@Autowired
	private ModuleRepository moduleRepository;

	@Autowired
	private AlmSettingsRepository almSettingsRepository;

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private AlmModuleDetailsRepository almModuleDetailsRepository;

	@Autowired
	private AlmApiDetailsPojoRepository almApiDetailsPojoRepository;

	@Autowired
	private AlmInProjectDetailsPojoRepository almInProjectDetailsPojoRepository;

	@Autowired
	private AlmSITDetailsRepository almSITDetailsRepository;

	@Autowired
	private AlmUATDetailsRepository almUATDetailsRepository;

	@Autowired
	private MigrationModuleDetailsRepository migrationModuleDetailsRepository;

	@Autowired
	private ModuleLevelDefectsRepository moduleLevelDefectsRepository;

	@Autowired
	private ProjectLevelDefectsRepository projectLevelDefectsRepository;

	@Autowired
	private DefectsRepository defectsRepository;

	@Autowired
	private RequirementsProjectFoldersRepository requirementsProjectFoldersRepository;

	@Autowired
	private ModuleLevelDefectsDataRepository moduleLevelDefectsDataRepository;


	@RequestMapping(value="/projects/{id}/almDefectSettings", method=RequestMethod.GET)
	@ResponseBody
	public String getAlmDefectSettings(@PathVariable("id") long id) {

		BufferedReader br = null;
		FileReader fr = null;
		String sCurrentLine;
		String filename = "DefectSettingLatest4.json";
		String totalBuffer = null;

		try {
			fr = new FileReader(filename);
			br = new BufferedReader(fr);
		} catch (Exception e) {

		}

		try {
			while ((sCurrentLine = br.readLine()) != null) {
				if (totalBuffer == null)
					totalBuffer = sCurrentLine;
				else
					totalBuffer = totalBuffer + sCurrentLine;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return totalBuffer;
	}

	@RequestMapping(value="/projects/{id}/almsettings", method=RequestMethod.GET)
	@ResponseBody
	public String getAlmSettings(@PathVariable("id") long id) {

		Iterable<AlmSettings> almSettingsIterable = almSettingsRepository.findAll();
		Iterator<AlmSettings> almSettingsIterator = almSettingsIterable.iterator();

		String response = null;
		boolean recordFound = false;

		try {

			while (almSettingsIterator.hasNext()) {
				AlmSettings next = almSettingsIterator.next();

				if (next.getProject().getId() == id) {
					recordFound = true;

					AlmSettingsPojo almpojo = new AlmSettingsPojo();

					almpojo.setHost(next.getHost());
					almpojo.setDomain(next.getDomain());
					almpojo.setAlmUsername(next.getAlmUsername());
					almpojo.setAlmPassword(next.getAlmPassword());
					almpojo.setAlmProject(next.getAlmProject());


					boolean almApiDetailsFound = false;
					Iterable<AlmApiDetails> almApiDetailsIterable = almApiDetailsPojoRepository.findAll();
					Iterator<AlmApiDetails> almApiDetailsIterator = almApiDetailsIterable.iterator();

					ArrayList<AlmApiDetailsPojo> almApiDetailsArray = new ArrayList<AlmApiDetailsPojo>();
					int almApiDetailsModuleIndex = 1;


					try {
						while (almApiDetailsIterator.hasNext()) {
							AlmApiDetails nextAlmApiDetails = almApiDetailsIterator.next();

							if (nextAlmApiDetails.getProject().getId() == id) {

								System.out.println("The Project is : " + nextAlmApiDetails.getProject().getId());
								System.out.println("The Test Design Folder ID is : " + nextAlmApiDetails.getTestDesignFolderId());
								System.out.println("The Test Execution Folder ID is : " + nextAlmApiDetails.getTestExecutionFolderId());

								AlmApiDetailsPojo nextAlmApiDetailsPojo = new AlmApiDetailsPojo();
								nextAlmApiDetailsPojo.setId(almApiDetailsModuleIndex);
								almApiDetailsModuleIndex += 1;
								//nextAlmApiDetailsPojo.setTestDesignFolderId(nextAlmApiDetails.getTestDesignFolderId());
								//nextAlmApiDetailsPojo.setTestExecutionFolderId(nextAlmApiDetails.getTestExecutionFolderId());



								ArrayList<String> testDesignFolders = new ArrayList<String>();
								StringTokenizer tokenizer = new StringTokenizer(nextAlmApiDetails.getTestDesignFolderIdString(), ",");
								while(tokenizer.hasMoreTokens()){
									testDesignFolders.add(tokenizer.nextToken());
								}
								nextAlmApiDetailsPojo.setTestDesignFolderId(testDesignFolders);

								ArrayList<String> testExecutionFolders = new ArrayList<String>();
								tokenizer = new StringTokenizer(nextAlmApiDetails.getTestExecutionFolderIdString(), ",");
								while(tokenizer.hasMoreTokens()){
									testExecutionFolders.add(tokenizer.nextToken());
								}
								nextAlmApiDetailsPojo.setTestExecutionFolderId(testExecutionFolders);

								almApiDetailsArray.add(nextAlmApiDetailsPojo);
								almApiDetailsFound = true;

							}

						}
					}catch(Exception e) {
					}

					if(almApiDetailsFound == true)
						almpojo.setAlmApiDetailsPojo(almApiDetailsArray);
					else{
						ArrayList<AlmApiDetailsPojo> almApiDetailsPojoArrayList = new ArrayList<AlmApiDetailsPojo>();

						AlmApiDetailsPojo almApiDetails = new AlmApiDetailsPojo();
						almApiDetails.setId(1);

						ArrayList<String> tempAlmApiDetailsArray = new ArrayList<String>();
						tempAlmApiDetailsArray.add("");
						almApiDetails.setTestDesignFolderId(tempAlmApiDetailsArray);

						tempAlmApiDetailsArray = new ArrayList<String>();
						tempAlmApiDetailsArray.add("");
						almApiDetails.setTestExecutionFolderId(tempAlmApiDetailsArray);
						almApiDetailsPojoArrayList.add(almApiDetails);

						almpojo.setAlmApiDetailsPojo(almApiDetailsPojoArrayList);

					}



					boolean almInProjectDetailsFound = false;
					Iterable<AlmInProjectDetails> almInProjectDetailsIterable = almInProjectDetailsPojoRepository.findAll();
					Iterator<AlmInProjectDetails> almInProjectDetailsIterator = almInProjectDetailsIterable.iterator();

					ArrayList<AlmInProjectDetailsPojo> almInProjectDetailsArray = new ArrayList<AlmInProjectDetailsPojo>();
					int almInProjectDetailsModuleIndex = 1;


					try {
						while (almInProjectDetailsIterator.hasNext()) {
							AlmInProjectDetails nextAlmInProjectDetails = almInProjectDetailsIterator.next();

							if (nextAlmInProjectDetails.getProject().getId() == id) {

								System.out.println("The Project is : " + nextAlmInProjectDetails.getProject().getId());
								System.out.println("The Test Design Folder ID is : " + nextAlmInProjectDetails.getTestDesignFolderId());
								System.out.println("The Test Execution Folder ID is : " + nextAlmInProjectDetails.getTestExecutionFolderId());

								AlmInProjectDetailsPojo nextAlmInProjectDetailsPojo = new AlmInProjectDetailsPojo();
								nextAlmInProjectDetailsPojo.setId(almInProjectDetailsModuleIndex);
								almInProjectDetailsModuleIndex += 1;
								//nextAlmInProjectDetailsPojo.setTestDesignFolderId(nextAlmInProjectDetails.getTestDesignFolderId());
								//nextAlmInProjectDetailsPojo.setTestExecutionFolderId(nextAlmInProjectDetails.getTestExecutionFolderId());

								ArrayList<String> testDesignFolders = new ArrayList<String>();
								StringTokenizer tokenizer = new StringTokenizer(nextAlmInProjectDetails.getTestDesignFolderIdString(), ",");
								while(tokenizer.hasMoreTokens()){
									testDesignFolders.add(tokenizer.nextToken());
								}
								nextAlmInProjectDetailsPojo.setTestDesignFolderId(testDesignFolders);

								ArrayList<String> testExecutionFolders = new ArrayList<String>();
								tokenizer = new StringTokenizer(nextAlmInProjectDetails.getTestExecutionFolderIdString(), ",");
								while(tokenizer.hasMoreTokens()){
									testExecutionFolders.add(tokenizer.nextToken());
								}
								nextAlmInProjectDetailsPojo.setTestExecutionFolderId(testExecutionFolders);

								almInProjectDetailsArray.add(nextAlmInProjectDetailsPojo);
								almInProjectDetailsFound = true;

							}

						}
					}catch(Exception e) {
					}

					if(almInProjectDetailsFound == true)
						almpojo.setAlmInProjectDetailsPojo(almInProjectDetailsArray);
					else{
						ArrayList<AlmInProjectDetailsPojo> almInProjectDetailsPojoArrayList = new ArrayList<AlmInProjectDetailsPojo>();

						AlmInProjectDetailsPojo almInProjectDetails = new AlmInProjectDetailsPojo();
						almInProjectDetails.setId(1);

						ArrayList<String> tempAlmInProjectDetailsArray = new ArrayList<String>();
						tempAlmInProjectDetailsArray.add("");
						almInProjectDetails.setTestDesignFolderId(tempAlmInProjectDetailsArray);

						tempAlmInProjectDetailsArray = new ArrayList<String>();
						tempAlmInProjectDetailsArray.add("");
						almInProjectDetails.setTestExecutionFolderId(tempAlmInProjectDetailsArray);
						almInProjectDetailsPojoArrayList.add(almInProjectDetails);

						almpojo.setAlmInProjectDetailsPojo(almInProjectDetailsPojoArrayList);

					}




					Iterable<AlmModuleDetails> almModuleDetailsIterable = almModuleDetailsRepository.findAll();
					Iterator<AlmModuleDetails> almModuleDetailsIterator = almModuleDetailsIterable.iterator();

					ArrayList<AlmModuleDetailsPojo> almModuleDetailsArray = new ArrayList<AlmModuleDetailsPojo>();
					int almModuleDetailsModuleIndex = 1;
					boolean almModuleFound = false;


					try {
						while (almModuleDetailsIterator.hasNext()) {
							AlmModuleDetails nextAlmModuleDetails = almModuleDetailsIterator.next();

							if (nextAlmModuleDetails.getProject().getId() == id) {

								System.out.println("The Project is : " + nextAlmModuleDetails.getProject().getId());
								System.out.println("The Module Name is : " + nextAlmModuleDetails.getModuleName());
								System.out.println("The Test Design Folder ID is : " + nextAlmModuleDetails.getTestDesignFolderId());
								System.out.println("The Test Execution Folder ID is : " + nextAlmModuleDetails.getTestExecutionFolderId());

								AlmModuleDetailsPojo nextAlmModuleDetailsPojo = new AlmModuleDetailsPojo();
								nextAlmModuleDetailsPojo.setId(almModuleDetailsModuleIndex);
								almModuleDetailsModuleIndex += 1;
								nextAlmModuleDetailsPojo.setModule(nextAlmModuleDetails.getModule());

								ArrayList<String> testDesignFolders = new ArrayList<String>();
								StringTokenizer tokenizer = new StringTokenizer(nextAlmModuleDetails.getTestDesignFolderIdString(), ",");
								while(tokenizer.hasMoreTokens()){
									testDesignFolders.add(tokenizer.nextToken());
								}
								nextAlmModuleDetailsPojo.setTestDesignFolderId(testDesignFolders);

								ArrayList<String> testExecutionFolders = new ArrayList<String>();
								tokenizer = new StringTokenizer(nextAlmModuleDetails.getTestExecutionFolderIdString(), ",");
								while(tokenizer.hasMoreTokens()){
									testExecutionFolders.add(tokenizer.nextToken());
								}
								nextAlmModuleDetailsPojo.setTestExecutionFolderId(testExecutionFolders);

								almModuleDetailsArray.add(nextAlmModuleDetailsPojo);
								almModuleFound = true;

							}

						}
					}catch(Exception e) {
					}

					if(almModuleFound == true)
						almpojo.setAlmModuleDetailsPojo(almModuleDetailsArray);
					else
					{
						ArrayList<AlmModuleDetailsPojo> almModuleDetailsList = new ArrayList<AlmModuleDetailsPojo>();

						Iterable<Module> allModules = moduleRepository.findAll();
						Iterator<Module> modulesIterator = allModules.iterator();

						int almModuleIndex = 1;

						while (modulesIterator.hasNext()) {
							Module module = modulesIterator.next();

							if (module.getProject().getId() == id) {
								try{
									AlmModuleDetailsPojo almModuleDetails = new AlmModuleDetailsPojo();
									almModuleDetails.setId(almModuleIndex);
									almModuleIndex += 1;
									almModuleDetails.setModule(module.getName());

									ArrayList<String> almTestDesignFolders = new ArrayList<String>();
									almTestDesignFolders.add("");
									almModuleDetails.setTestDesignFolderId(almTestDesignFolders);

									ArrayList<String> almTestExecutionFolders = new ArrayList<String>();
									almTestExecutionFolders.add("");
									almModuleDetails.setTestExecutionFolderId(almTestExecutionFolders);

									almModuleDetailsList.add(almModuleDetails);

								}catch(Exception e){
								}
							}
						}

						Gson gson = new Gson();
						System.out.println(gson.toJson(almModuleDetailsList));

						almpojo.setAlmModuleDetailsPojo(almModuleDetailsList);
					}


					boolean almSITDetailsFound = false;
					Iterable<AlmSITDetails> almSITDetailsIterable = almSITDetailsRepository.findAll();
					Iterator<AlmSITDetails> almSITDetailsIterator = almSITDetailsIterable.iterator();

					ArrayList<AlmSitDetailsPojo> almSITDetailsArray = new ArrayList<AlmSitDetailsPojo>();
					int almSITDetailsModuleIndex = 1;


					try {
						while (almSITDetailsIterator.hasNext()) {
							AlmSITDetails nextAlmSITDetails = almSITDetailsIterator.next();

							if (nextAlmSITDetails.getProject().getId() == id) {

								System.out.println("The Project is : " + nextAlmSITDetails.getProject().getId());
								System.out.println("The Test Design Folder ID is : " + nextAlmSITDetails.getTestDesignFolderId());
								System.out.println("The Test Execution Folder ID is : " + nextAlmSITDetails.getTestExecutionFolderId());

								AlmSitDetailsPojo nextAlmSITDetailsPojo = new AlmSitDetailsPojo();
								nextAlmSITDetailsPojo.setId(almSITDetailsModuleIndex);
								almSITDetailsModuleIndex += 1;

								ArrayList<String> testDesignFolders = new ArrayList<String>();
								StringTokenizer tokenizer = new StringTokenizer(nextAlmSITDetails.getTestDesignFolderIdString(), ",");
								while(tokenizer.hasMoreTokens()){
									testDesignFolders.add(tokenizer.nextToken());
								}
								nextAlmSITDetailsPojo.setTestDesignFolderId(testDesignFolders);

								ArrayList<String> testExecutionFolders = new ArrayList<String>();
								tokenizer = new StringTokenizer(nextAlmSITDetails.getTestExecutionFolderIdString(), ",");
								while(tokenizer.hasMoreTokens()){
									testExecutionFolders.add(tokenizer.nextToken());
								}
								nextAlmSITDetailsPojo.setTestExecutionFolderId(testExecutionFolders);


								//nextAlmSITDetailsPojo.setTestDesignFolderId(nextAlmSITDetails.getTestDesignFolderId());
								//nextAlmSITDetailsPojo.setTestExecutionFolderId(nextAlmSITDetails.getTestExecutionFolderId());
								almSITDetailsArray.add(nextAlmSITDetailsPojo);
								almSITDetailsFound = true;

							}

						}
					}catch(Exception e) {
					}

					if(almSITDetailsFound == true)
						almpojo.setAlmSitDetailsPojo(almSITDetailsArray);
					else{
						ArrayList<AlmSitDetailsPojo> almSITDetailsPojoArrayList = new ArrayList<AlmSitDetailsPojo>();

						AlmSitDetailsPojo almSITDetails = new AlmSitDetailsPojo();
						almSITDetails.setId(1);

						ArrayList<String> tempAlmSITDetailsArray = new ArrayList<String>();
						tempAlmSITDetailsArray.add("");
						almSITDetails.setTestDesignFolderId(tempAlmSITDetailsArray);

						tempAlmSITDetailsArray = new ArrayList<String>();
						tempAlmSITDetailsArray.add("");
						almSITDetails.setTestExecutionFolderId(tempAlmSITDetailsArray);
						almSITDetailsPojoArrayList.add(almSITDetails);

						almpojo.setAlmSitDetailsPojo(almSITDetailsPojoArrayList);

					}


					boolean almUATDetailsFound = false;

					Iterable<AlmUATDetails> almUATDetailsIterable = almUATDetailsRepository.findAll();
					Iterator<AlmUATDetails> almUATDetailsIterator = almUATDetailsIterable.iterator();

					ArrayList<AlmUatDetailsPojo> almUATDetailsArray = new ArrayList<AlmUatDetailsPojo>();
					int almUATDetailsModuleIndex = 1;


					try {
						while (almUATDetailsIterator.hasNext()) {
							AlmUATDetails nextAlmUATDetails = almUATDetailsIterator.next();

							if (nextAlmUATDetails.getProject().getId() == id) {

								System.out.println("The Project is : " + nextAlmUATDetails.getProject().getId());
								System.out.println("The Test Design Folder ID is : " + nextAlmUATDetails.getTestDesignFolderId());
								System.out.println("The Test Execution Folder ID is : " + nextAlmUATDetails.getTestExecutionFolderId());

								AlmUatDetailsPojo nextAlmUATDetailsPojo = new AlmUatDetailsPojo();
								nextAlmUATDetailsPojo.setId(almUATDetailsModuleIndex);
								almUATDetailsModuleIndex += 1;


								AlmSitDetailsPojo nextAlmSITDetailsPojo = new AlmSitDetailsPojo();
								nextAlmSITDetailsPojo.setId(almSITDetailsModuleIndex);
								almSITDetailsModuleIndex += 1;

								ArrayList<String> testDesignFolders = new ArrayList<String>();
								StringTokenizer tokenizer = new StringTokenizer(nextAlmUATDetails.getTestDesignFolderIdString(), ",");
								while(tokenizer.hasMoreTokens()){
									testDesignFolders.add(tokenizer.nextToken());
								}
								nextAlmUATDetailsPojo.setTestDesignFolderId(testDesignFolders);

								ArrayList<String> testExecutionFolders = new ArrayList<String>();
								tokenizer = new StringTokenizer(nextAlmUATDetails.getTestExecutionFolderIdString(), ",");
								while(tokenizer.hasMoreTokens()){
									testExecutionFolders.add(tokenizer.nextToken());
								}
								nextAlmUATDetailsPojo.setTestExecutionFolderId(testExecutionFolders);


								//nextAlmUATDetailsPojo.setTestDesignFolderId(nextAlmUATDetails.getTestDesignFolderId());
								//nextAlmUATDetailsPojo.setTestExecutionFolderId(nextAlmUATDetails.getTestExecutionFolderId());
								almUATDetailsArray.add(nextAlmUATDetailsPojo);

								almUATDetailsFound = true;

							}

						}
					}catch(Exception e) {
					}

					if(almUATDetailsFound == true)
						almpojo.setAlmUatDetailsPojo(almUATDetailsArray);
					else{

						ArrayList<AlmUatDetailsPojo> almUATDetailsPojoArrayList = new ArrayList<AlmUatDetailsPojo>();

						AlmUatDetailsPojo almUATDetails = new AlmUatDetailsPojo();
						almUATDetails.setId(1);

						ArrayList<String> tempAlmUATDetailsArray = new ArrayList<String>();
						tempAlmUATDetailsArray.add("");
						almUATDetails.setTestDesignFolderId(tempAlmUATDetailsArray);

						tempAlmUATDetailsArray = new ArrayList<String>();
						tempAlmUATDetailsArray.add("");
						almUATDetails.setTestExecutionFolderId(tempAlmUATDetailsArray);

						almUATDetailsPojoArrayList.add(almUATDetails);

						almpojo.setAlmUatDetailsPojo(almUATDetailsPojoArrayList);
					}


					boolean defectDetailsFound = false;
					Iterable<DefectsEntity> defectsIterable = defectsRepository.findAll();
					Iterator<DefectsEntity> defectsIterator = defectsIterable.iterator();

					try {
						while (defectsIterator.hasNext()) {
							DefectsEntity defects = defectsIterator.next();

							if (defects.getProject().getId() == id) {
								defectDetailsFound = true;
								System.out.println(defects.getProjectFieldName());
								System.out.println(defects.getProjectName());
								Defects tempDefects = new Defects();
								tempDefects.setProjectFieldName(defects.getProjectFieldName());
								tempDefects.setProjectName(defects.getProjectName());
								almpojo.setDefects(tempDefects);
								break;
							}

						}
					}catch(Exception e){
							System.out.println("Exception occurred : " + e.getMessage());
					}

					if(defectDetailsFound == false)
					{
						Defects tempDefects = new Defects();
						tempDefects.setProjectFieldName("");

						ArrayList<String> defectFolders = new ArrayList<String>();
						defectFolders.add("");
						tempDefects.setProjectFieldName("");

						almpojo.setDefects(tempDefects);
					}


					boolean projectLevelDefectsFound = false;
					Iterable<ProjectLevelDefectsEntity> projectLevelDefectIterable = projectLevelDefectsRepository.findAll();
					Iterator<ProjectLevelDefectsEntity> projectLevelDefectIterator = projectLevelDefectIterable.iterator();

					try {
						while (projectLevelDefectIterator.hasNext()) {
							ProjectLevelDefectsEntity projectLevelDefects = projectLevelDefectIterator.next();

							if (projectLevelDefects.getProject().getId() == id) {
								projectLevelDefectsFound = true;
								ProjectLevelDefects tempProjectLevelDefects = new ProjectLevelDefects();
								tempProjectLevelDefects.setName(projectLevelDefects.getName());
								tempProjectLevelDefects.setSit(projectLevelDefects.getSit());
								tempProjectLevelDefects.setUat(projectLevelDefects.getUat());
								tempProjectLevelDefects.setProduction(projectLevelDefects.getProduction());
								tempProjectLevelDefects.setManual(projectLevelDefects.getManual());
								tempProjectLevelDefects.setAutomation(projectLevelDefects.getAutomation());
								almpojo.getDefects().setProjectLevelDefects(tempProjectLevelDefects);
								break;
							}

						}
					}catch(Exception e){

					}

					if(projectLevelDefectsFound == false)
					{
						ArrayList<String> emptyList = new ArrayList<String>();
						emptyList.add("");
						ProjectLevelDefects tempProjectLevelDefects = new ProjectLevelDefects();
						tempProjectLevelDefects.setName("");
						tempProjectLevelDefects.setSit(emptyList);
						tempProjectLevelDefects.setUat(emptyList);
						tempProjectLevelDefects.setSit(emptyList);
						tempProjectLevelDefects.setSit(emptyList);
						tempProjectLevelDefects.setSit(emptyList);
						almpojo.getDefects().setProjectLevelDefects(tempProjectLevelDefects);
					}


					boolean moduleLevelDefectsFound = false;
					Iterable<ModuleLevelDefectsData> moduleLevelDefectsDataIterable = moduleLevelDefectsDataRepository.findAll();
					Iterator<ModuleLevelDefectsData> moduleLevelDefectsDataIterator = moduleLevelDefectsDataIterable.iterator();
					ArrayList<ModuleLevelDefects> moduleLevelDefectsList = new ArrayList<ModuleLevelDefects>();
					int moduleLevelDefectsIndex = 1;

					try {
						while (moduleLevelDefectsDataIterator.hasNext()) {
							ModuleLevelDefectsData moduleLevelDefectsData = moduleLevelDefectsDataIterator.next();

							if (moduleLevelDefectsData.getProject().getId() == id) {
								moduleLevelDefectsFound = true;
								ModuleLevelDefects tempModuleLevelDefects = new ModuleLevelDefects();

								tempModuleLevelDefects.setProjectFieldName(moduleLevelDefectsData.getProjectFieldName());
								tempModuleLevelDefects.setProjectName(moduleLevelDefectsData.getProjectName());
								tempModuleLevelDefects.setId(moduleLevelDefectsIndex);
								moduleLevelDefectsIndex++;

								tempModuleLevelDefects.setModuleName(moduleLevelDefectsData.getModuleName());
								tempModuleLevelDefects.setDefectModuleFieldName(moduleLevelDefectsData.getDefectModuleFieldName());
								tempModuleLevelDefects.setTags(moduleLevelDefectsData.getTags());
								moduleLevelDefectsList.add(tempModuleLevelDefects);
							}

						}
					}catch(Exception e){

					}

					if(moduleLevelDefectsFound == false)
					{
						ModuleLevelDefects tempModuleLevelDefects = new ModuleLevelDefects();

						tempModuleLevelDefects.setProjectFieldName("");
						ArrayList<String> projectFieldNames = new ArrayList<String>();
						tempModuleLevelDefects.setProjectName(projectFieldNames);
						tempModuleLevelDefects.setId(1);

						tempModuleLevelDefects.setModuleName("");
						tempModuleLevelDefects.setDefectModuleFieldName("");
						ArrayList<String> tags = new ArrayList<String>();
						tempModuleLevelDefects.setTags(tags);
						moduleLevelDefectsList.add(tempModuleLevelDefects);
					}

					almpojo.getDefects().setModuleLevelDefects(moduleLevelDefectsList);


					boolean requirementsProjectFoldersFound = false;
					Iterable<RequirementsProjectFoldersEntity> requirementsProjectFoldersIterable = requirementsProjectFoldersRepository.findAll();
					Iterator<RequirementsProjectFoldersEntity> requirementsProjectFoldersIterator = requirementsProjectFoldersIterable.iterator();


					try {
						while (requirementsProjectFoldersIterator.hasNext()) {
							RequirementsProjectFoldersEntity requirementsProjectFolders = requirementsProjectFoldersIterator.next();

							if (requirementsProjectFolders.getProject().getId() == id) {
								requirementsProjectFoldersFound = true;
								RequirementsProjectFolders tempRequirementsProjectFolders = new RequirementsProjectFolders();
								tempRequirementsProjectFolders.setProjectFieldName(requirementsProjectFolders.getProjectFieldName());
								tempRequirementsProjectFolders.setProjectName(requirementsProjectFolders.getProjectName());
								almpojo.setRequirementsProjectFolders(tempRequirementsProjectFolders);
								break;
							}

						}
					}catch(Exception e){

					}

					if(requirementsProjectFoldersFound == false)
					{
						RequirementsProjectFolders tempRequirementsProjectFolders = new RequirementsProjectFolders();

						ArrayList<String> tempProjectNames = new ArrayList<String>();
						tempProjectNames.add("");
						tempRequirementsProjectFolders.setProjectFieldName("");
						tempRequirementsProjectFolders.setProjectName(tempProjectNames);
						almpojo.setRequirementsProjectFolders(tempRequirementsProjectFolders);
					}


					boolean migrationModuleDetailsFound = false;
					Iterable<MigrationModuleDetails> migrationModuleDetailsIterable = migrationModuleDetailsRepository.findAll();
					Iterator<MigrationModuleDetails> migrationModuleDetailsIterator = migrationModuleDetailsIterable.iterator();

					ArrayList<MigrationModuleDetailsPojo> migrationModuleDetailsArray = new ArrayList<MigrationModuleDetailsPojo>();
					int migrationModuleIndex = 1;
					try {
						while (migrationModuleDetailsIterator.hasNext()) {
							MigrationModuleDetails nextMigrationModuleDetails = migrationModuleDetailsIterator.next();

							if (nextMigrationModuleDetails.getProject().getId() == id) {

								MigrationModuleDetailsPojo nextMigrationModuleDetailsPojo = new MigrationModuleDetailsPojo();
								nextMigrationModuleDetailsPojo.setId(migrationModuleIndex);
								migrationModuleIndex += 1;
								nextMigrationModuleDetailsPojo.setModule(nextMigrationModuleDetails.getModule());

								ArrayList<String> testDesignFolders = new ArrayList<String>();
								StringTokenizer tokenizer = new StringTokenizer(nextMigrationModuleDetails.getTestDesignFolderIdString(), ",");
								while(tokenizer.hasMoreTokens()){
									testDesignFolders.add(tokenizer.nextToken());
								}
								nextMigrationModuleDetailsPojo.setTestDesignFolderId(testDesignFolders);

								ArrayList<String> testExecutionFolders = new ArrayList<String>();
								tokenizer = new StringTokenizer(nextMigrationModuleDetails.getTestExecutionFolderIdString(), ",");
								while(tokenizer.hasMoreTokens()){
									testExecutionFolders.add(tokenizer.nextToken());
								}
								nextMigrationModuleDetailsPojo.setTestExecutionFolderId(testExecutionFolders);


								//nextMigrationModuleDetailsPojo.setTestDesignFolderId(nextMigrationModuleDetails.getTestDesignFolderId());
								//nextMigrationModuleDetailsPojo.setTestExecutionFolderId(nextMigrationModuleDetails.getTestExecutionFolderId());
								migrationModuleDetailsArray.add(nextMigrationModuleDetailsPojo);
								migrationModuleDetailsFound = true;

							}
						}
					}catch(Exception e){
					}

					if(migrationModuleDetailsFound == true)
						almpojo.setMigrationModuleDetailsPojo(migrationModuleDetailsArray);
					else
					{
						ArrayList<MigrationModuleDetailsPojo> migrationModuleDetailsList = new ArrayList<MigrationModuleDetailsPojo>();

						MigrationModuleDetailsPojo migrationModuleDetails = new MigrationModuleDetailsPojo();
						migrationModuleDetails.setId(1);
						migrationModuleDetails.setModule("");

						ArrayList<String> migrationModuleTestDesignFolders = new ArrayList<String>();
						migrationModuleTestDesignFolders.add("");
						migrationModuleDetails.setTestDesignFolderId(migrationModuleTestDesignFolders);

						ArrayList<String> migrationModuleTestExecutionFolders = new ArrayList<String>();
						migrationModuleTestExecutionFolders.add("");
						migrationModuleDetails.setTestExecutionFolderId(migrationModuleTestExecutionFolders);

						migrationModuleDetailsList.add(migrationModuleDetails);

						almpojo.setMigrationModuleDetailsPojo(migrationModuleDetailsList);

					}



					Gson gson = new Gson();
					response = gson.toJson(almpojo);
					//response = "{\"AlmSettings\":" + response + "}";
					System.out.println(response);

					return response;
				}
			}
		}catch(Exception e){
			System.out.println("Exception occurred while fetching ALM settings : " + e.getMessage());
		}

		if(recordFound == false) {
			response = "{\n" +
					"\t\"host\": \"\",\n" +
					"\t\"almUsername\": \"\",\n" +
					"\t\"almPassword\": \"\",\n" +
					"\t\"domain\": \"\",\n" +
					"\t\"almProject\": \"\",\n" +
					"\t\"requirementsProjectFolders\": {\n" +
					"\t\t\"projectFieldName\": \"\",\n" +
					"\t\t\"projectName\": [\"\"]\n" +
					"\t},\n" +
					"\t\"defects\": {\n" +
					"\t\t\"projectFieldName\": \"\",\n" +
					"\t\t\"projectName\": [\"\"],\n" +
					"\t\t\"projectLevelDefects\": {\n" +
					"\t\t\t\"name\": \"\",\n" +
					"\t\t\t\"sit\": [\"\"],\n" +
					"\t\t\t\"uat\": [\"\"],\n" +
					"\t\t\t\"production\": [\"\"],\n" +
					"\t\t\t\"automation\": [\"\"],\n" +
					"\t\t\t\"manual\": [\"\"]\n" +
					"\t\t},\n" +
					"\t\t\t\"moduleLevelDefects\": [{\n" +
					"\t\t\"id\": 1,\n" +
					"\t\t\"projectFieldName\": \"\",\n" +
					"\t\t\"projectName\": [\"\"],\n" +
					"\t\t\t\t\"moduleName\": \"\",\n" +
					"\t\t\t\t\"defectModuleFieldName\": \"\",\n" +
					"\t\t\t\t\"tags\": [\"\"]\n" +
					"\t\t\t}]\n}," +
					"\t\"almApiDetailsPojo\": [{\n" +
					"\t\t\"id\": 1,\n" +
					"\t\t\"testExecutionFolderId\": [\"\"],\n" +
					"\t\t\"testDesignFolderId\": [\"\"]\n" +
					"\t}],\n" +
					"\t\"almInProjectDetailsPojo\": [{\n" +
					"\t\t\"id\": 1,\n" +
					"\t\t\"testExecutionFolderId\": [\"\"],\n" +
					"\t\t\"testDesignFolderId\": [\"\"]\n" +
					"\t}],\n" +
					"\t\"almSitDetailsPojo\": [{\n" +
					"\t\t\"id\": 1,\n" +
					"\t\t\"testExecutionFolderId\": [\"\"],\n" +
					"\t\t\"testDesignFolderId\": [\"\"]\n" +
					"\t}],\n" +
					"\t\"almUatDetailsPojo\": [{\n" +
					"\t\t\"id\": 1,\n" +
					"\t\t\"testExecutionFolderId\": [\"\"],\n" +
					"\t\t\"testDesignFolderId\": [\"\"]\n" +
					"\t}],\n" +

					"\t\"almModuleDetailsPojo\":";



			ArrayList<AlmModuleDetailsPojo> almModuleDetailsList = new ArrayList<AlmModuleDetailsPojo>();

			Iterable<Module> allModules = moduleRepository.findAll();
			Iterator<Module> modulesIterator = allModules.iterator();

			int almModuleIndex = 1;

			while (modulesIterator.hasNext()) {
				Module module = modulesIterator.next();

				if (module.getProject().getId() == id) {
					try{
						AlmModuleDetailsPojo almModuleDetails = new AlmModuleDetailsPojo();
						almModuleDetails.setId(almModuleIndex);
						almModuleIndex += 1;
						almModuleDetails.setModule(module.getName());

						ArrayList<String> almTestDesignFolders = new ArrayList<String>();
						almTestDesignFolders.add("");
						almModuleDetails.setTestDesignFolderId(almTestDesignFolders);

						ArrayList<String> almTestExecutionFolders = new ArrayList<String>();
						almTestExecutionFolders.add("");
						almModuleDetails.setTestExecutionFolderId(almTestExecutionFolders);

						almModuleDetailsList.add(almModuleDetails);

					}catch(Exception e){
					}
				}
			}

			Gson gson = new Gson();
			System.out.println(gson.toJson(almModuleDetailsList));

			response = response + gson.toJson(almModuleDetailsList);

			response = response +	",\t\"migrationModuleDetailsPojo\": [{\n" +
					"\t\t\"id\": 1,\n" +
					"\t\t\"module\": \"\",\n" +
					"\t\t\"testExecutionFolderId\": [\"\"],\n" +
					"\t\t\"testDesignFolderId\": [\"\"]\n" +
					"\t}]\n" +
					"}";

			System.out.println(response);
			return response;
		}
        return "Error";

	}

	@PostMapping(value = "/projects/{id}/almsettings")
	@ResponseBody
	public String updateAlmSettingsProgress(@PathVariable("id") long id, @RequestBody AlmSettingsPojo almSettingsPojo) {

		Gson gson = new Gson();
		System.out.println(gson.toJson(almSettingsPojo));

		Iterable<Project> allProjects = projectRepository.findAll();
		Iterator<Project> iterator = allProjects.iterator();
		Project nextProject = null;

		while (iterator.hasNext()) {
			nextProject = iterator.next();
			if (nextProject.getId() == id) {
				break;
			}
		}

		Iterable<AlmSettings> almASettingsIterable = almSettingsRepository.findAll();
		Iterator<AlmSettings> almASettingsIterator = almASettingsIterable.iterator();

		try {
			while (almASettingsIterator.hasNext()) {
				AlmSettings next = almASettingsIterator.next();

				if (next.getProject().getId() == id) {
					almSettingsRepository.delete(next);
				}
			}
		}catch(Exception e) {
		}



		AlmSettings tempAlmSettings = new AlmSettings();
		try{tempAlmSettings.setHost(almSettingsPojo.getHost());	almSettingsRepository.save(tempAlmSettings);}catch(Exception e){}
		try{tempAlmSettings.setAlmUsername(almSettingsPojo.getAlmUsername()); almSettingsRepository.save(tempAlmSettings);}catch(Exception e){}
		try{tempAlmSettings.setAlmPassword(almSettingsPojo.getAlmPassword()); almSettingsRepository.save(tempAlmSettings);}catch(Exception e){}
		try{tempAlmSettings.setDomain(almSettingsPojo.getDomain()); almSettingsRepository.save(tempAlmSettings);}catch(Exception e){}
		try{tempAlmSettings.setAlmProject(almSettingsPojo.getAlmProject()); almSettingsRepository.save(tempAlmSettings);}catch(Exception e){}
		try{tempAlmSettings.setProject(nextProject);almSettingsRepository.save(tempAlmSettings);}catch(Exception e){}

		Iterable<RequirementsProjectFoldersEntity> requirementsProjectFoldersIterable = requirementsProjectFoldersRepository.findAll();
		Iterator<RequirementsProjectFoldersEntity> requirementsProjectFoldersIterator = requirementsProjectFoldersIterable.iterator();

		try {
			while (requirementsProjectFoldersIterator.hasNext()) {
				RequirementsProjectFoldersEntity next = requirementsProjectFoldersIterator.next();

				if (next.getProject().getId() == id) {
					requirementsProjectFoldersRepository.delete(next);
				}
			}
		}catch(Exception e) {
		}


		try {
			RequirementsProjectFoldersEntity requirementsProjectFolders = new RequirementsProjectFoldersEntity();
			requirementsProjectFolders.setProjectFieldName(almSettingsPojo.getRequirementsProjectFolders().getProjectFieldName());
			requirementsProjectFolders.setProjectName(almSettingsPojo.getRequirementsProjectFolders().getProjectName());
			requirementsProjectFolders.setProject(nextProject);
			requirementsProjectFoldersRepository.save(requirementsProjectFolders);
		}catch(Exception e){
		}


		Iterable<AlmApiDetails> almApiDetailsPojoIterable = almApiDetailsPojoRepository.findAll();
		Iterator<AlmApiDetails> almApiDetailsPojoIterator = almApiDetailsPojoIterable.iterator();

		try {
			while (almApiDetailsPojoIterator.hasNext()) {
				AlmApiDetails next = almApiDetailsPojoIterator.next();

				if (next.getProject().getId() == id) {
					almApiDetailsPojoRepository.delete(next);
				}
			}
		}catch(Exception e) {
		}


		try {
			for (int i = 0; i < almSettingsPojo.getAlmApiDetailsPojo().size(); i++) {
				AlmApiDetails tempAlmApiDetailsPojo = new AlmApiDetails();
				tempAlmApiDetailsPojo.setProject(nextProject);
				//tempAlmApiDetailsPojo.setTestDesignFolderId(almSettingsPojo.getAlmApiDetailsPojo().get(i).getTestDesignFolderId());
				//tempAlmApiDetailsPojo.setTestExecutionFolderId(almSettingsPojo.getAlmApiDetailsPojo().get(i).getTestExecutionFolderId());



				String testDesignFolderString = null;
				for(int j = 0; j < almSettingsPojo.getAlmApiDetailsPojo().get(i).getTestDesignFolderId().size(); j++)
				{
					if(testDesignFolderString == null)
						testDesignFolderString = almSettingsPojo.getAlmApiDetailsPojo().get(i).getTestDesignFolderId().get(j);
					else
						testDesignFolderString = testDesignFolderString+ "," + almSettingsPojo.getAlmApiDetailsPojo().get(i).getTestDesignFolderId().get(j);
				}

				tempAlmApiDetailsPojo.setTestDesignFolderIdString(testDesignFolderString);


				String testExecutionFolderString = null;
				for(int j = 0; j < almSettingsPojo.getAlmApiDetailsPojo().get(i).getTestExecutionFolderId().size(); j++)
				{
					if(testExecutionFolderString == null)
						testExecutionFolderString = almSettingsPojo.getAlmApiDetailsPojo().get(i).getTestExecutionFolderId().get(j);
					else
						testExecutionFolderString = testExecutionFolderString+ "," + almSettingsPojo.getAlmApiDetailsPojo().get(i).getTestExecutionFolderId().get(j);
				}

				tempAlmApiDetailsPojo.setTestExecutionFolderIdString(testExecutionFolderString);

				almApiDetailsPojoRepository.save(tempAlmApiDetailsPojo);
			}
		}catch(Exception e){
		}

		Iterable<AlmInProjectDetails> almInProjectDetailsPojoIterable = almInProjectDetailsPojoRepository.findAll();
		Iterator<AlmInProjectDetails> almInProjectDetailsPojoIterator = almInProjectDetailsPojoIterable.iterator();

		try {
			while (almInProjectDetailsPojoIterator.hasNext()) {
				AlmInProjectDetails next = almInProjectDetailsPojoIterator.next();

				if (next.getProject().getId() == id) {
					almInProjectDetailsPojoRepository.delete(next);
				}

			}
		}catch(Exception e) {
		}

		try {
			for (int i = 0; i < almSettingsPojo.getAlmInProjectDetailsPojo().size(); i++) {
				AlmInProjectDetails almInProjectDetailsPojo = new AlmInProjectDetails();
				almInProjectDetailsPojo.setProject(nextProject);
				//almInProjectDetailsPojo.setTestDesignFolderId(almSettingsPojo.getAlmInProjectDetailsPojo().get(i).getTestDesignFolderId());
				//almInProjectDetailsPojo.setTestExecutionFolderId(almSettingsPojo.getAlmInProjectDetailsPojo().get(i).getTestExecutionFolderId());


				String testDesignFolderString = null;
				for(int j = 0; j < almSettingsPojo.getAlmInProjectDetailsPojo().get(i).getTestDesignFolderId().size(); j++)
				{
					if(testDesignFolderString == null)
						testDesignFolderString = almSettingsPojo.getAlmInProjectDetailsPojo().get(i).getTestDesignFolderId().get(j);
					else
						testDesignFolderString = testDesignFolderString+ "," + almSettingsPojo.getAlmInProjectDetailsPojo().get(i).getTestDesignFolderId().get(j);
				}

				almInProjectDetailsPojo.setTestDesignFolderIdString(testDesignFolderString);


				String testExecutionFolderString = null;
				for(int j = 0; j < almSettingsPojo.getAlmInProjectDetailsPojo().get(i).getTestExecutionFolderId().size(); j++)
				{
					if(testExecutionFolderString == null)
						testExecutionFolderString = almSettingsPojo.getAlmInProjectDetailsPojo().get(i).getTestExecutionFolderId().get(j);
					else
						testExecutionFolderString = testExecutionFolderString+ "," + almSettingsPojo.getAlmInProjectDetailsPojo().get(i).getTestExecutionFolderId().get(j);
				}

				almInProjectDetailsPojo.setTestExecutionFolderIdString(testExecutionFolderString);


				almInProjectDetailsPojoRepository.save(almInProjectDetailsPojo);
			}
		}catch(Exception e){
		}



		Iterable<AlmModuleDetails> almModuleDetailsIterable = almModuleDetailsRepository.findAll();
		Iterator<AlmModuleDetails> almModuleDetailsIterator = almModuleDetailsIterable.iterator();

		try {
			while (almModuleDetailsIterator.hasNext()) {
				AlmModuleDetails next = almModuleDetailsIterator.next();

				if (next.getProject().getId() == id) {
					almModuleDetailsRepository.delete(next);
				}
			}
		}catch(Exception e) {
		}


		try {

			ArrayList<AlmModuleDetailsPojo> almModuleDetails = almSettingsPojo.getAlmModuleDetailsPojo();
			for (int i = 0; i < almModuleDetails.size(); i++) {
				AlmModuleDetails tempAlmModuleDetails = new AlmModuleDetails();
				tempAlmModuleDetails.setProject(nextProject);
				tempAlmModuleDetails.setModule(almModuleDetails.get(i).getModule());

			//	System.out.println(">>>>>>" + almModuleDetails.get(i).getTestDesignFolderId());
			//	System.out.println("!!!!!!" + almModuleDetails.get(i).getTestExecutionFolderId());

				String testDesignFolderString = null;

				for(int j = 0; j < almModuleDetails.get(i).getTestDesignFolderId().size(); j++)
				{
					if(testDesignFolderString == null)
						testDesignFolderString = almModuleDetails.get(i).getTestDesignFolderId().get(j);
					else
						testDesignFolderString = testDesignFolderString+ "," + almModuleDetails.get(i).getTestDesignFolderId().get(j);
				}

				tempAlmModuleDetails.setTestDesignFolderIdString(testDesignFolderString);


				String testExecutionFolderString = null;

				for(int j = 0; j < almModuleDetails.get(i).getTestExecutionFolderId().size(); j++)
				{
					if(testExecutionFolderString == null)
						testExecutionFolderString = almModuleDetails.get(i).getTestExecutionFolderId().get(j);
					else
						testExecutionFolderString = testExecutionFolderString+ "," + almModuleDetails.get(i).getTestExecutionFolderId().get(j);
				}

				tempAlmModuleDetails.setTestExecutionFolderIdString(testExecutionFolderString);

				almModuleDetailsRepository.save(tempAlmModuleDetails);
			}

		}catch(Exception e){

		}

		Iterable<AlmSITDetails> almSITDetailsIterable = almSITDetailsRepository.findAll();
		Iterator<AlmSITDetails> almSITDetailsIterator = almSITDetailsIterable.iterator();

		try {
			while (almSITDetailsIterator.hasNext()) {
				AlmSITDetails next = almSITDetailsIterator.next();

				if (next.getProject().getId() == id) {
					almSITDetailsRepository.delete(next);
				}
			}
		}catch(Exception e) {
		}



		try {
			ArrayList<AlmSitDetailsPojo> almSITDetailsPojo = almSettingsPojo.getAlmSitDetailsPojo();
			for (int i = 0; i < almSITDetailsPojo.size(); i++) {
				AlmSITDetails tempAlmSITDetails = new AlmSITDetails();
				tempAlmSITDetails.setProject(nextProject);

				String testDesignFolderString = null;

				for(int j = 0; j < almSITDetailsPojo.get(i).getTestDesignFolderId().size(); j++)
				{
					if(testDesignFolderString == null)
						testDesignFolderString = almSITDetailsPojo.get(i).getTestDesignFolderId().get(j);
					else
						testDesignFolderString = testDesignFolderString+ "," + almSITDetailsPojo.get(i).getTestDesignFolderId().get(j);
				}

				tempAlmSITDetails.setTestDesignFolderIdString(testDesignFolderString);


				String testExecutionFolderString = null;

				for(int j = 0; j < almSITDetailsPojo.get(i).getTestExecutionFolderId().size(); j++)
				{
					if(testExecutionFolderString == null)
						testExecutionFolderString = almSITDetailsPojo.get(i).getTestExecutionFolderId().get(j);
					else
						testExecutionFolderString = testExecutionFolderString+ "," + almSITDetailsPojo.get(i).getTestExecutionFolderId().get(j);
				}

				tempAlmSITDetails.setTestExecutionFolderIdString(testExecutionFolderString);

				//tempAlmSITDetails.setTestDesignFolderID(almSITDetailsPojo.get(i).getTestDesignFolderId());
				//tempAlmSITDetails.setTestExecutionFolderID(almSITDetailsPojo.get(i).getTestExecutionFolderId());
				almSITDetailsRepository.save(tempAlmSITDetails);
			}
		}catch(Exception e){

		}



		Iterable<AlmUATDetails> almUATDetailsIterable = almUATDetailsRepository.findAll();
		Iterator<AlmUATDetails> almUATDetailsIterator = almUATDetailsIterable.iterator();

		try {
			while (almUATDetailsIterator.hasNext()) {
				AlmUATDetails next = almUATDetailsIterator.next();

				if (next.getProject().getId() == id) {
					almUATDetailsRepository.delete(next);
				}
			}
		}catch(Exception e) {
		}



		try {
			ArrayList<AlmUatDetailsPojo> almUATDetailsPojo = almSettingsPojo.getAlmUatDetailsPojo();
			for (int i = 0; i < almUATDetailsPojo.size(); i++) {
				AlmUATDetails tempAlmUATDetails = new AlmUATDetails();
				tempAlmUATDetails.setProject(nextProject);

				String testDesignFolderString = null;
				for(int j = 0; j < almUATDetailsPojo.get(i).getTestDesignFolderId().size(); j++)
				{
					if(testDesignFolderString == null)
						testDesignFolderString = almUATDetailsPojo.get(i).getTestDesignFolderId().get(j);
					else
						testDesignFolderString = testDesignFolderString+ "," + almUATDetailsPojo.get(i).getTestDesignFolderId().get(j);
				}

				tempAlmUATDetails.setTestDesignFolderIdString(testDesignFolderString);

				String testExecutionFolderString = null;
				for(int j = 0; j < almUATDetailsPojo.get(i).getTestExecutionFolderId().size(); j++)
				{
					if(testExecutionFolderString == null)
						testExecutionFolderString = almUATDetailsPojo.get(i).getTestExecutionFolderId().get(j);
					else
						testExecutionFolderString = testExecutionFolderString+ "," + almUATDetailsPojo.get(i).getTestExecutionFolderId().get(j);
				}

				tempAlmUATDetails.setTestExecutionFolderIdString(testExecutionFolderString);

				//tempAlmUATDetails.setTestDesignFolderID(almUATDetailsPojo.get(i).getTestDesignFolderId());
				//tempAlmUATDetails.setTestExecutionFolderID(almUATDetailsPojo.get(i).getTestExecutionFolderId());
				almUATDetailsRepository.save(tempAlmUATDetails);
			}
		}catch(Exception e) {
		}

		Iterable<MigrationModuleDetails> migrationModuleDetailsIterable = migrationModuleDetailsRepository.findAll();
		Iterator<MigrationModuleDetails> migrationModuleDetailsIterator = migrationModuleDetailsIterable.iterator();

		try {
			while (migrationModuleDetailsIterator.hasNext()) {
				MigrationModuleDetails next = migrationModuleDetailsIterator.next();

				if (next.getProject().getId() == id) {
					migrationModuleDetailsRepository.delete(next);
				}
			}
		}catch(Exception e){
		}


		try {
			ArrayList<MigrationModuleDetailsPojo> migrationModuleDetails = almSettingsPojo.getMigrationModuleDetailsPojo();
			for (int i = 0; i < migrationModuleDetails.size(); i++) {
				MigrationModuleDetails tempMigrationModuleDetails = new MigrationModuleDetails();
				tempMigrationModuleDetails.setProject(nextProject);
				tempMigrationModuleDetails.setModule(migrationModuleDetails.get(i).getModule());
				//tempMigrationModuleDetails.setTestDesignFolderID(migrationModuleDetails.get(i).getTestDesignFolderId());
				//tempMigrationModuleDetails.setTestExecutionFolderID(migrationModuleDetails.get(i).getTestExecutionFolderId());


				String testDesignFolderString = null;
				for(int j = 0; j < migrationModuleDetails.get(i).getTestDesignFolderId().size(); j++)
				{
					if(testDesignFolderString == null)
						testDesignFolderString = migrationModuleDetails.get(i).getTestDesignFolderId().get(j);
					else
						testDesignFolderString = testDesignFolderString+ "," + migrationModuleDetails.get(i).getTestDesignFolderId().get(j);
				}

				tempMigrationModuleDetails.setTestDesignFolderIdString(testDesignFolderString);


				String testExecutionFolderString = null;
				for(int j = 0; j < migrationModuleDetails.get(i).getTestExecutionFolderId().size(); j++)
				{
					if(testExecutionFolderString == null)
						testExecutionFolderString = migrationModuleDetails.get(i).getTestExecutionFolderId().get(j);
					else
						testExecutionFolderString = testExecutionFolderString+ "," + migrationModuleDetails.get(i).getTestExecutionFolderId().get(j);
				}

				tempMigrationModuleDetails.setTestExecutionFolderIdString(testExecutionFolderString);

				migrationModuleDetailsRepository.save(tempMigrationModuleDetails);
			}
		}catch(Exception e){

		}




		Iterable<ModuleLevelDefectsData> moduleLevelDefectsDataIterable = moduleLevelDefectsDataRepository.findAll();
		Iterator<ModuleLevelDefectsData> moduleLevelDefectsDataIterator = moduleLevelDefectsDataIterable.iterator();

		try {
			while (moduleLevelDefectsDataIterator.hasNext()) {
				ModuleLevelDefectsData next = moduleLevelDefectsDataIterator.next();

				if (next.getProject().getId() == id) {
					moduleLevelDefectsDataRepository.delete(next);
				}

			}
		}catch(Exception e){
		}


		try {
			ArrayList<ModuleLevelDefects> moduleLevelDefectsArrayLists = almSettingsPojo.getDefects().getModuleLevelDefects();

			for(int i =0; i < moduleLevelDefectsArrayLists.size(); i++) {
				ModuleLevelDefectsData moduleLevelDefectsData = new ModuleLevelDefectsData();

				moduleLevelDefectsData.setProjectFieldName(moduleLevelDefectsArrayLists.get(i).getProjectFieldName());
				moduleLevelDefectsData.setProjectName(moduleLevelDefectsArrayLists.get(i).getProjectName());

				moduleLevelDefectsData.setDefectModuleFieldName(moduleLevelDefectsArrayLists.get(i).getDefectModuleFieldName());
				moduleLevelDefectsData.setTags(moduleLevelDefectsArrayLists.get(i).getTags());
				moduleLevelDefectsData.setModuleName(moduleLevelDefectsArrayLists.get(i).getModuleName());

				moduleLevelDefectsData.setProject(nextProject);
				moduleLevelDefectsDataRepository.save(moduleLevelDefectsData);
			}


		}catch(Exception e){

		}




		Iterable<ProjectLevelDefectsEntity> projectLevelDefectIterable = projectLevelDefectsRepository.findAll();
		Iterator<ProjectLevelDefectsEntity> projectLevelDefectIterator = projectLevelDefectIterable.iterator();

		try {
			while (projectLevelDefectIterator.hasNext()) {
				ProjectLevelDefectsEntity next = projectLevelDefectIterator.next();

				if (next.getProject().getId() == id) {
					projectLevelDefectsRepository.delete(next);
				}
			}
		}catch(Exception e){
		}


		try {
			ProjectLevelDefectsEntity projectLevelDefects = new ProjectLevelDefectsEntity();
			projectLevelDefects.setName(almSettingsPojo.getDefects().getProjectLevelDefects().getName());
			projectLevelDefects.setProject(nextProject);
			projectLevelDefects.setSit(almSettingsPojo.getDefects().getProjectLevelDefects().getSit());
			projectLevelDefects.setUat(almSettingsPojo.getDefects().getProjectLevelDefects().getUat());
			projectLevelDefects.setProduction(almSettingsPojo.getDefects().getProjectLevelDefects().getProduction());
			projectLevelDefects.setManual(almSettingsPojo.getDefects().getProjectLevelDefects().getManual());
			projectLevelDefects.setAutomation(almSettingsPojo.getDefects().getProjectLevelDefects().getAutomation());

			projectLevelDefectsRepository.save(projectLevelDefects);


		}catch(Exception e){

		}


		Iterable<DefectsEntity> defectsIterable = defectsRepository.findAll();
		Iterator<DefectsEntity> defectsIterator = defectsIterable.iterator();

		try {
			while (defectsIterator.hasNext()) {
				DefectsEntity next = defectsIterator.next();

				if (next.getProject().getId() == id) {
					defectsRepository.delete(next);
				}
			}
		}catch(Exception e){
		}

		try {
			DefectsEntity defects = new DefectsEntity();
			defects.setProjectFieldName(almSettingsPojo.getDefects().getProjectFieldName());
			defects.setProjectName(almSettingsPojo.getDefects().getProjectName());
			defects.setProject(nextProject);
			defectsRepository.save(defects);
		}catch(Exception e){

		}


		return "OK";
	}

}