package com.packt.cardatabase.domain;

import javax.persistence.*;
import java.util.ArrayList;

@Entity
public class AutomationTestDesignProductivity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private ArrayList<String> numberOfAutomationScriptsAddedEachWeek;
    private ArrayList<String> moduleNames;
    private String effortSpentOnAutomationTestDesignEachWeek;
    private String automationTestDesignProductivity;
    private Integer weekNumber;
    private Integer yearNumber;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public AutomationTestDesignProductivity() {}

    public AutomationTestDesignProductivity(ArrayList<String> numberOfAutomationScriptsAddedEachWeek,
                                            String effortSpentOnAutomationTestDesignEachWeek,
                                            Integer weekNumber,
                                            Integer yearNumber,
                                            Project project) {

        super();
        this.numberOfAutomationScriptsAddedEachWeek = numberOfAutomationScriptsAddedEachWeek;
        this.effortSpentOnAutomationTestDesignEachWeek = effortSpentOnAutomationTestDesignEachWeek;
        this.weekNumber = weekNumber;
        this.yearNumber = yearNumber;
        this.project = project;
    }

    public Integer getWeekNumber() {
        return weekNumber;
    }
    public String getWeekNumberName() {
        return "Week";
    }
    public void setWeekNumber(Integer weekNumber) {
        this.weekNumber = weekNumber;
    }

    public Integer getYearNumber() {
        return yearNumber;
    }
    public String getYearNumberName() {
        return "Year";
    }
    public void setYearNumber(Integer yearNumber) {
        this.yearNumber = yearNumber;
    }


    public String getAutomationTestDesignProductivity() {
        return automationTestDesignProductivity;
    }
    public String getAutomationTestDesignProductivityName() {
        return "Automation Test Design Productivity";
    }
    public void setAutomationTestDesignProductivity(String automationTestDesignProductivity) {
        this.automationTestDesignProductivity = automationTestDesignProductivity;
    }

    public ArrayList<String> getNumberOfAutomationScriptsAddedEachWeek(){
        return numberOfAutomationScriptsAddedEachWeek;
    }
    public String getNumberOfAutomationScriptsAddedEachWeekName(){
        return "Number Of Automation Scripts added";
    }
    public void setNumberOfAutomationScriptsAddedEachWeek(ArrayList<String> numberOfAutomationScriptsAddedEachWeek){
        this.numberOfAutomationScriptsAddedEachWeek = numberOfAutomationScriptsAddedEachWeek;
    }

    public ArrayList<String> getModuleNames(){
        return moduleNames;
    }
    public String getModuleNamesName(){
        return "Module";
    }
    public void setModuleNames(ArrayList<String> moduleNames){
        this.moduleNames = moduleNames;
    }

    public String  getEffortSpentOnAutomationTestDesignEachWeek() {
        return effortSpentOnAutomationTestDesignEachWeek;
    }

    public String getEffortSpentOnAutomationTestDesignEachWeekName(){
        return "Effort Spent On Automation Test Design";
    }

    public void setEffortSpentOnAutomationTestDesignEachWeek(String effortSpentOnAutomationTestDesignEachWeek) {
        this.effortSpentOnAutomationTestDesignEachWeek = effortSpentOnAutomationTestDesignEachWeek;
    }



    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

}