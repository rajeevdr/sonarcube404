package com.packt.cardatabase.domain;

public class LastUpdatePojo {

    private int lastUpdatedDateNumber;
    private int lastUpdatedWeekNumber;
    private int lastUpdatedMonthNumber;
    private int lastUpdatedYearNumber;
    private int lastUpdatedQuarterNumber;


    public LastUpdatePojo() {}

    public LastUpdatePojo(int lastUpdatedDateNumber,
                          int lastUpdatedWeekNumber,
                      int lastUpdatedMonthNumber,
                      int lastUpdatedYearNumber,
                      int lastUpdatedQuarterNumber,
                      Project project) {
        this.lastUpdatedDateNumber = lastUpdatedDateNumber;
        this.lastUpdatedWeekNumber = lastUpdatedWeekNumber;
        this.lastUpdatedMonthNumber = lastUpdatedMonthNumber;
        this.lastUpdatedYearNumber = lastUpdatedYearNumber;
        this.lastUpdatedQuarterNumber = lastUpdatedQuarterNumber;
    }


    public int getLastUpdatedDateNumber() {
        return lastUpdatedDateNumber;
    }

    public void setLastUpdatedDateNumber(int lastUpdatedDateNumber) {
        this.lastUpdatedDateNumber = lastUpdatedDateNumber;
    }

    public int getLastUpdatedYearNumber() {
        return lastUpdatedYearNumber;
    }

    public void setLastUpdatedYearNumber(int lastUpdatedYearNumber) {
        this.lastUpdatedYearNumber = lastUpdatedYearNumber;
    }

    public int getLastUpdatedWeekNumber() {
        return lastUpdatedWeekNumber;
    }

    public void setLastUpdatedWeekNumber(int lastUpdatedWeekNumber) {
        this.lastUpdatedWeekNumber = lastUpdatedWeekNumber;
    }

    public int getLastUpdatedMonthNumber() {
        return lastUpdatedMonthNumber;
    }

    public void setLastUpdatedMonthNumber(int lastUpdatedMonthNumber) {
        this.lastUpdatedMonthNumber = lastUpdatedMonthNumber;
    }

    public int getLastUpdatedQuarterNumber() {
        return lastUpdatedQuarterNumber;
    }

    public void setLastUpdatedQuarterNumber(int lastUpdatedQuarterNumber) {
        this.lastUpdatedQuarterNumber = lastUpdatedQuarterNumber;
    }


}