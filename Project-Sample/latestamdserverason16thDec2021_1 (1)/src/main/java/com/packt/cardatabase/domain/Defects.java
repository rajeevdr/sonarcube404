package com.packt.cardatabase.domain;

import java.util.ArrayList;

public class Defects {

    private String projectFieldName;
    private ArrayList<String> projectName = null;

    private ProjectLevelDefects projectLevelDefects;


    private ArrayList<ModuleLevelDefects> moduleLevelDefects;

    public String getProjectFieldName() {
        return projectFieldName;
    }

    public void setProjectFieldName(String projectFieldName) {
        this.projectFieldName = projectFieldName;
    }

    public ArrayList<String> getProjectName() {
        return projectName;
    }

    public void setProjectName(ArrayList<String> projectName) {
        this.projectName = projectName;
    }


    public ArrayList<ModuleLevelDefects> getModuleLevelDefects() {
        return moduleLevelDefects;
    }

    public void setModuleLevelDefects(ArrayList<ModuleLevelDefects> moduleLevelDefects) {
        this.moduleLevelDefects = moduleLevelDefects;
    }


    public ProjectLevelDefects getProjectLevelDefects() {
        return projectLevelDefects;
    }

    public void setProjectLevelDefects(ProjectLevelDefects projectLevelDefects) {
        this.projectLevelDefects = projectLevelDefects;
    }



}
