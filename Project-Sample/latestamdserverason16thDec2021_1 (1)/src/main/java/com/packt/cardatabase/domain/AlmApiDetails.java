package com.packt.cardatabase.domain;

import javax.persistence.*;
import java.util.ArrayList;

@Entity
public class AlmApiDetails {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "project")
	private Project project;


	private ArrayList<String> testExecutionFolderId = null;
	private ArrayList<String> testDesignFolderId = null;

	private String testExecutionFolderIdString;
	private String testDesignFolderIdString;


	public Long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ArrayList<String> getTestExecutionFolderId() {
		return testExecutionFolderId;
	}

	public void setTestExecutionFolderId(ArrayList<String> testExecutionFolderId) {
		this.testExecutionFolderId = testExecutionFolderId;
	}

	public ArrayList<String> getTestDesignFolderId() {
		return testDesignFolderId;
	}

	public void setTestDesignFolderId(ArrayList<String> testDesignFolderId) {
		this.testDesignFolderId = testDesignFolderId;
	}

	public String getTestDesignFolderIdString() {
		return testDesignFolderIdString;
	}

	public void setTestDesignFolderIdString(String testDesignFolderIdString) {
		this.testDesignFolderIdString = testDesignFolderIdString;
	}

	public String getTestExecutionFolderIdString() {
		return testExecutionFolderIdString;
	}

	public void setTestExecutionFolderIdString(String testExecutionFolderIdString) {
		this.testExecutionFolderIdString = testExecutionFolderIdString;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

}