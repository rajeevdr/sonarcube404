package com.packt.cardatabase.domain;


public class QuarterlyMetricsPojo {

    private String revenueLeakage;
    private int quarterNumber;

    public QuarterlyMetricsPojo() {
    }

    QuarterlyMetricsPojo(String revenueLeakage,
                         String quarterNumber) {

        this.revenueLeakage = revenueLeakage;
        this.quarterNumber = this.quarterNumber;

    }


    public void setRevenueLeakage(String revenueLeakage){
        this.revenueLeakage = revenueLeakage;
    }

    public String getRevenueLeakage(){
        return revenueLeakage;
    }

    public int getQuarterNumber() {
        return quarterNumber;
    }

    public void setQuarterNumber(int quarterNumber) {
        this.quarterNumber = quarterNumber;
    }

}
