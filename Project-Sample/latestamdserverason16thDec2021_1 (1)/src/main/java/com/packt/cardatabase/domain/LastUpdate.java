package com.packt.cardatabase.domain;


import javax.persistence.*;


@Entity
public class LastUpdate {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;

    private int lastUpdatedDateNumber;
    private int lastUpdatedWeekNumber;
    private int lastUpdatedMonthNumber;
    private int lastUpdatedYearNumber;
    private int lastUpdatedQuarterNumber;


    public LastUpdate() {}

    public LastUpdate(int lastUpdatedDateNumber,
                      int lastUpdatedWeekNumber,
                      int lastUpdatedMonthNumber,
                      int lastUpdatedYearNumber,
                      int lastUpdatedQuarterNumber,
                      Project project) {
        super();
        this.lastUpdatedDateNumber = lastUpdatedDateNumber;
        this.lastUpdatedWeekNumber = lastUpdatedWeekNumber;
        this.lastUpdatedMonthNumber = lastUpdatedMonthNumber;
        this.lastUpdatedYearNumber = lastUpdatedYearNumber;
        this.lastUpdatedQuarterNumber = lastUpdatedQuarterNumber;
        this.project = project;
    }

    public int getLastUpdatedDateNumber() {
        return lastUpdatedDateNumber;
    }

    public void setLastUpdatedDateNumber(int lastUpdatedDateNumber) {
        this.lastUpdatedDateNumber = lastUpdatedDateNumber;
    }

    public int getLastUpdatedYearNumber() {
        return lastUpdatedYearNumber;
    }

    public void setLastUpdatedYearNumber(int lastUpdatedYearNumber) {
        this.lastUpdatedYearNumber = lastUpdatedYearNumber;
    }

    public int getLastUpdatedWeekNumber() {
        return lastUpdatedWeekNumber;
    }

    public void setLastUpdatedWeekNumber(int lastUpdatedWeekNumber) {
        this.lastUpdatedWeekNumber = lastUpdatedWeekNumber;
    }

    public int getLastUpdatedMonthNumber() {
        return lastUpdatedMonthNumber;
    }

    public void setLastUpdatedMonthNumber(int lastUpdatedMonthNumber) {
        this.lastUpdatedMonthNumber = lastUpdatedMonthNumber;
    }

    public int getLastUpdatedQuarterNumber() {
        return lastUpdatedQuarterNumber;
    }

    public void setLastUpdatedQuarterNumber(int lastUpdatedQuarterNumber) {
        this.lastUpdatedQuarterNumber = lastUpdatedQuarterNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

}