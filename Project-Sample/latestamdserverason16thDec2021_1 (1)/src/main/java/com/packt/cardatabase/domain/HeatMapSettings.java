package com.packt.cardatabase.domain;

import javax.persistence.*;

@Entity
public class HeatMapSettings {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private String metricName;
    private String lcl;
    private String ucl;
    private String goal;
    private String category;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public HeatMapSettings() {}

    public HeatMapSettings(String metricName,
                           String lcl,
                           String ucl,
                           String goal,
                           Project project)  {
        super();
        this.metricName = metricName;
        this.lcl = lcl;
        this.ucl = ucl;
        this.goal = goal;
        this.project = project;
    }

    public void setMetricName(String metricName) {
        this.metricName = metricName;
    }


    public String getMetricName() {
        return metricName;
    }

    public String getLCLName() {
        return "Lower Control Limit";
    }


    public String getLCL() {
        return this.lcl;
    }


    public void setLCL(String lcl) {
        this.lcl = lcl;
    }

    public String getUCLName() {
        return "Upper Control Limit";
    }


    public String getUCL() {
        return this.ucl;
    }


    public void setUCL(String ucl) {
        this.ucl = ucl;
    }

    public String getGoalName() {
        return "Desired Goal";
    }

    public String getGoal() {
        return this.goal;
    }


    public void setGoal(String goal) {
        this.goal = goal;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }


    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }


}