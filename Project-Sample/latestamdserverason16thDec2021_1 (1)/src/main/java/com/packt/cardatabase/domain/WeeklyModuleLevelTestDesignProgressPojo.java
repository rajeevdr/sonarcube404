package com.packt.cardatabase.domain;


public class WeeklyModuleLevelTestDesignProgressPojo {


    private String numberOfManualTestCasesDesignedForTheWeek;
    private String numberOfRegAutoTestsDesignedForTheWeek;
    private String numberOfSanityTestsDesignedForTheWeek;
    private String moduleName;



    public WeeklyModuleLevelTestDesignProgressPojo() {}

    public WeeklyModuleLevelTestDesignProgressPojo(String moduleName,
                                               String numberOfManualTestCasesDesignedForTheWeek,
                                               String numberOfRegAutoTestsDesignedForTheWeek,
                                                   String numberOfSanityTestsDesignedForTheWeek)
    {
        this.moduleName = moduleName;
        this.numberOfManualTestCasesDesignedForTheWeek = numberOfManualTestCasesDesignedForTheWeek;
        this.numberOfRegAutoTestsDesignedForTheWeek = numberOfRegAutoTestsDesignedForTheWeek;
        this.numberOfSanityTestsDesignedForTheWeek = numberOfSanityTestsDesignedForTheWeek;
    }


    public String getModuleName() {
        return moduleName;
    }
    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }


    public String getNumberOfManualTestCasesDesignedForTheWeek() {
        return numberOfManualTestCasesDesignedForTheWeek;
    }

    public void setNumberOfManualTestCasesDesignedForTheWeek(String numberOfManualTestCasesDesignedForTheWeek) {
        this.numberOfManualTestCasesDesignedForTheWeek = numberOfManualTestCasesDesignedForTheWeek;
    }


    public String getNumberOfRegressionAutomationTestScriptsDesignedForTheWeek() {
        return numberOfRegAutoTestsDesignedForTheWeek;
    }

    public String getNumberOfRegressionAutomationTestScriptsDesignedForTheWeekName() {
        return "NumberOfRegressionAutomationTestScriptsDesignedForTheWeek";
    }
    public void setNumberOfRegressionAutomationTestScriptsDesignedForTheWeek(String numberOfRegAutoTestsDesignedForTheWeek) {
        this.numberOfRegAutoTestsDesignedForTheWeek = numberOfRegAutoTestsDesignedForTheWeek;
    }


    public String getNumberOfSanityTestsDesignedForTheWeek() {
        return numberOfSanityTestsDesignedForTheWeek;
    }

    public String getNumberOfSanityTestsDesignedForTheWeekName() {
        return "NumberOfSanityTestsDesignedForTheWeek";
    }
    public void setNumberOfSanityTestsDesignedForTheWeek(String numberOfSanityTestsDesignedForTheWeek) {
        this.numberOfSanityTestsDesignedForTheWeek = numberOfSanityTestsDesignedForTheWeek;
    }


}