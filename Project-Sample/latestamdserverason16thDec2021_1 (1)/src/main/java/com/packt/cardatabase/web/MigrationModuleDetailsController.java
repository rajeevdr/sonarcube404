package com.packt.cardatabase.web;

import com.google.gson.Gson;
import com.packt.cardatabase.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;


@RestController
public class MigrationModuleDetailsController {

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private MigrationModuleDetailsRepository migrationModuleDetailsRepository;


	@RequestMapping(value="/projects/{id}/migrationModuleDetails", method=RequestMethod.GET)
	@ResponseBody
	public String getMigrationModuleDetails(@PathVariable("id") long id) {

		Iterable<MigrationModuleDetails> migrationModuleDetailsIterable = migrationModuleDetailsRepository.findAll();
		Iterator<MigrationModuleDetails> migrationModuleDetailsIterator = migrationModuleDetailsIterable.iterator();

		ArrayList<MigrationModuleDetailsPojo> migrationModuleDetailsArray = new ArrayList<MigrationModuleDetailsPojo>();
		boolean matchFound = false;

		try {
			while (migrationModuleDetailsIterator.hasNext()) {
				MigrationModuleDetails nextMigrationModuleDetails = migrationModuleDetailsIterator.next();

				if (nextMigrationModuleDetails.getProject().getId() == id) {
					matchFound = true;
					MigrationModuleDetailsPojo nextMigrationModuleDetailsPojo = new MigrationModuleDetailsPojo();
					nextMigrationModuleDetailsPojo.setModule(nextMigrationModuleDetails.getModule());
					nextMigrationModuleDetailsPojo.setTestDesignFolderId(nextMigrationModuleDetails.getTestDesignFolderId());
					nextMigrationModuleDetailsPojo.setTestExecutionFolderId(nextMigrationModuleDetails.getTestExecutionFolderId());
					migrationModuleDetailsArray.add(nextMigrationModuleDetailsPojo);

				}
			}
		}catch(Exception e){
		}


		if(matchFound == true) {
			Gson gson = new Gson();
			String response = gson.toJson(migrationModuleDetailsArray);
			response = "{\"MigrationModuleDetails\":" + response + "}";
			System.out.println(response);
			return response;
		}

		if(matchFound == false) {

			migrationModuleDetailsArray = new ArrayList<MigrationModuleDetailsPojo>();

			MigrationModuleDetailsPojo migrationModuleDetails = new MigrationModuleDetailsPojo();
			migrationModuleDetails.setModule(" ");
			ArrayList<String> migrationModules = new ArrayList<String>();
			migrationModules.add(" ");
			migrationModules.add(" ");
			migrationModuleDetails.setTestDesignFolderId(migrationModules);
			migrationModuleDetails.setTestExecutionFolderId(migrationModules);
			migrationModuleDetailsArray.add(migrationModuleDetails);

			migrationModuleDetails = new MigrationModuleDetailsPojo();
			migrationModuleDetails.setModule(" ");
			migrationModuleDetails.setTestDesignFolderId(migrationModules);
			migrationModuleDetails.setTestExecutionFolderId(migrationModules);
			migrationModuleDetailsArray.add(migrationModuleDetails);

			Gson gson = new Gson();
			String response = gson.toJson(migrationModuleDetailsArray);
			response = "{\"MigrationModuleDetails\":" + response + "}";
			return response;

		}
		return "Error";

	}

	@PostMapping(value = "/projects/{id}/migrationModuleDetails")
	@ResponseBody
	public String updateAlmSettingsProgress(@PathVariable("id") long id, @RequestBody ArrayList<MigrationModuleDetailsPojo> migrationModuleDetailsPojoArray) {

		Iterable<Project> allProjects = projectRepository.findAll();
		Iterator<Project> iterator = allProjects.iterator();
		Project nextProject = null;

		while (iterator.hasNext()) {
			nextProject = iterator.next();
			if (nextProject.getId() == id) {
				break;
			}
		}


		Iterable<MigrationModuleDetails> migrationModuleDetailsIterable = migrationModuleDetailsRepository.findAll();
		Iterator<MigrationModuleDetails> migrationModuleDetailsIterator = migrationModuleDetailsIterable.iterator();

		try {
			while (migrationModuleDetailsIterator.hasNext()) {
				MigrationModuleDetails next = migrationModuleDetailsIterator.next();

				if (next.getProject().getId() == id) {
					migrationModuleDetailsRepository.delete(next);
				}
			}
		}catch(Exception e){
		}

		for (int i = 0; i < migrationModuleDetailsPojoArray.size(); i++) {
				MigrationModuleDetails tempMigrationModuleDetails = new MigrationModuleDetails();
				tempMigrationModuleDetails.setProject(nextProject);
				tempMigrationModuleDetails.setModule(migrationModuleDetailsPojoArray.get(i).getModule());
				tempMigrationModuleDetails.setTestDesignFolderID(migrationModuleDetailsPojoArray.get(i).getTestDesignFolderId());
				tempMigrationModuleDetails.setTestExecutionFolderID(migrationModuleDetailsPojoArray.get(i).getTestExecutionFolderId());
				migrationModuleDetailsRepository.save(tempMigrationModuleDetails);
		}


		return "OK";
	}

}