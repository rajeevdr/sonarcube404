package com.packt.cardatabase.web;

import com.google.gson.Gson;
import com.packt.cardatabase.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.text.SimpleDateFormat;
import java.util.*;


@RestController
public class MonthlyTestExecutionProgressController {

	@Autowired
	private ProjectRepository projectRepository;


	@Autowired
	private MonthlyTestExecutionProgressRepository monthlyTestExecutionProgressRepository;


	@RequestMapping(value="/projects/{id}/testExecution/monthlyProgress/month/{month}/year/{year}", method=RequestMethod.GET)
	@ResponseBody
	public String getMonthlyTestExecutionProgress(@PathVariable("id") long id, @PathVariable(value="month") String month , @PathVariable(value="year") int year) {

		try {
			ArrayList<ProjectProgressPojo> progressPojoArray = new ArrayList<ProjectProgressPojo>();
			int monthNumber = 0;

			if(month.contains("Jan"))
			{
				monthNumber = 1;
			}
			else if(month.contains("Feb"))
			{
				monthNumber = 2;
			}
			else if(month.contains("Mar"))
			{
				monthNumber = 3;
			}
			else if(month.contains("Apr"))
			{
				monthNumber = 4;
			}
			else if(month.contains("May"))
			{
				monthNumber = 5;
			}
			else if(month.contains("Jun"))
			{
				monthNumber = 6;
			}
			else if(month.contains("Jul"))
			{
				monthNumber = 7;
			}
			else if(month.contains("Aug"))
			{
				monthNumber = 8;
			}
			else if(month.contains("Sep"))
			{
				monthNumber = 9;
			}
			else if(month.contains("Oct"))
			{
				monthNumber = 10;
			}
			else if(month.contains("Nov"))
			{
				monthNumber = 11;
			}
			else if(month.contains("Dec"))
			{
				monthNumber = 12;
			}
			else
			{
				monthNumber = Integer.parseInt(month);
			}

			int yearNumber = year;



		Iterable<MonthlyTestExecutionProgress> monthlyDesignTestExecutionProgress = monthlyTestExecutionProgressRepository.findAll();
			Iterator<MonthlyTestExecutionProgress> monthlyDesignTestExecutionProgressIterator = monthlyDesignTestExecutionProgress.iterator();
			String response = null;
			boolean recordFound = false;

			while (monthlyDesignTestExecutionProgressIterator.hasNext()) {
				MonthlyTestExecutionProgress next = monthlyDesignTestExecutionProgressIterator.next();
				if ((next.getMonthNumber() == monthNumber) && (next.getProject().getId() == id) && (next.getYearNumber() == yearNumber)) {
					recordFound = true;
					int index = 1;

					ProjectProgressPojo projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfManualUATTestCasesExecutedName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfManualUATTestCasesExecuted()));}catch(Exception e){projectProgressPojo.setValue("0");} 
					projectProgressPojo.setCategory("Test Execution Progress");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getSITServicesInscopeName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getSITServicesInscope()));}catch(Exception e){projectProgressPojo.setValue("0");} 
					projectProgressPojo.setCategory("Basic Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getSITServicesNotAvailableForTestingName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getSITServicesNotAvailableForTesting()));}catch(Exception e){projectProgressPojo.setValue("0");} 
					projectProgressPojo.setCategory("Basic Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfManualSITTestCasesExecutedName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfManualSITTestCasesExecuted()));}catch(Exception e){projectProgressPojo.setValue("0");} 
					projectProgressPojo.setCategory("Test Execution Progress");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfDefectsLoggedInSITName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfDefectsLoggedInSIT()));}catch(Exception e){projectProgressPojo.setValue("0");} 
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfValidDefectsLoggedInSITName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfValidDefectsLoggedInSIT()));}catch(Exception e){projectProgressPojo.setValue("0");} 
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfDefectsLoggedInUATName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfDefectsLoggedInUAT()));}catch(Exception e){projectProgressPojo.setValue("0");} 
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfValidDefectsLoggedInUATName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getNumberOfValidDefectsLoggedInUAT()));}catch(Exception e){projectProgressPojo.setValue("0");} 
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);



					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfValidAutomationDefectsName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfValidAutomationDefects()));}catch(Exception e){projectProgressPojo.setValue("0");} 
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfDefectsName());
					try{ projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfDefects()));}catch(Exception e){projectProgressPojo.setValue("0");} 
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);


					try {
						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						projectProgressPojo.setName(next.getTotalNumberOfDefectsInProductionName());
						try{ projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfDefectsInProduction()));}catch(Exception e){projectProgressPojo.setValue("0");} 
						projectProgressPojo.setCategory("Defects");
						progressPojoArray.add(projectProgressPojo);
					}catch(Exception e){

					}


					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("date");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getActualStartDateMilestoneName());
					try{ projectProgressPojo.setValue(next.getActualStartDateMilestone().toString());}catch(Exception e){projectProgressPojo.setValue("");}
					projectProgressPojo.setCategory("Basic Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("date");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getActualEndDateMilestoneName());
					try{ projectProgressPojo.setValue(next.getActualEndDateMilestone().toString());}catch(Exception e){projectProgressPojo.setValue("");}
					projectProgressPojo.setCategory("Basic Information");
					progressPojoArray.add(projectProgressPojo);

					Gson gson = new Gson();
					response = gson.toJson(progressPojoArray);
					response = "{\"progress\":" + response + "}";

					System.out.println(response);
					return response;
				}
			}


			if(recordFound == false){
				MonthlyTestExecutionProgress next = new MonthlyTestExecutionProgress();

					int index = 1;
					ProjectProgressPojo projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfManualUATTestCasesExecutedName());
					projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Test Execution Progress");
				progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getSITServicesInscopeName());
				    projectProgressPojo.setValue("0");
					projectProgressPojo.setCategory("Basic Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getSITServicesNotAvailableForTestingName());
					projectProgressPojo.setValue("0");
					projectProgressPojo.setCategory("Basic Information");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfManualSITTestCasesExecutedName());
					projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Test Execution Progress");
				progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfDefectsLoggedInSITName());
					projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Defects");
				progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfValidDefectsLoggedInSITName());
					projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Defects");
				progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfDefectsLoggedInUATName());
					projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Defects");
				progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfValidDefectsLoggedInUATName());
					projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Defects");
				progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfValidAutomationDefectsName());
					projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Defects");
				progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfDefectsName());
					projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Defects");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
				projectProgressPojo.setName(next.getTotalNumberOfDefectsInProductionName());
				projectProgressPojo.setValue("0");
				projectProgressPojo.setCategory("Defects");
				progressPojoArray.add(projectProgressPojo);


				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);projectProgressPojo.setType("date");projectProgressPojo.setSource("Manual");
				projectProgressPojo.setName(next.getActualStartDateMilestoneName());
				projectProgressPojo.setValue("");
				projectProgressPojo.setCategory("Basic Information");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);projectProgressPojo.setType("date");projectProgressPojo.setSource("Manual");
				projectProgressPojo.setName(next.getActualEndDateMilestoneName());
				projectProgressPojo.setValue("");
				projectProgressPojo.setCategory("Basic Information");
				progressPojoArray.add(projectProgressPojo);



					Gson gson = new Gson();
					response = gson.toJson(progressPojoArray);
				response = "{\"progress\":" + response + "}";

					System.out.println(response);
					return response;
				}


		} catch (Exception e) {
		}

		return null;
	}

	@RequestMapping(value="/projects/{id}/testExecution/totalMonthlyProgress", method=RequestMethod.GET)
	@ResponseBody
	public String getTotalMonthlyTestExecutionProgress(@PathVariable("id") long id) {

		try {
			ArrayList<ArrayList<ProjectProgressPojo>> monthlyTestExecutionProgressArray = new ArrayList<ArrayList<ProjectProgressPojo>>();

			Iterable<MonthlyTestExecutionProgress> monthlyDesignTestExecutionProgress = monthlyTestExecutionProgressRepository.findAll();
			Iterator<MonthlyTestExecutionProgress> monthlyDesignTestExecutionProgressIterator = monthlyDesignTestExecutionProgress.iterator();
			String response = null;

			while (monthlyDesignTestExecutionProgressIterator.hasNext()) {
				MonthlyTestExecutionProgress next = monthlyDesignTestExecutionProgressIterator.next();
				if (next.getProject().getId() == id) {
					ArrayList<ProjectProgressPojo> progressPojoArray = new ArrayList<ProjectProgressPojo>();

					int index = 0;

					ProjectProgressPojo projectProgressPojo = new ProjectProgressPojo();

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("Index");
					projectProgressPojo.setName(next.getMonthNumberName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getMonthNumber()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("Index");
					projectProgressPojo.setName(next.getYearNumberName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getYearNumber()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfManualUATTestCasesExecutedName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfManualUATTestCasesExecuted()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("TestExecutionProgress");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getSITServicesInscopeName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getSITServicesInscope()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("BasicInformation");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getSITServicesNotAvailableForTestingName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getSITServicesNotAvailableForTesting()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("BasicInformation");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfManualSITTestCasesExecutedName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfManualSITTestCasesExecuted()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("TestExecutionProgress");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfDefectsLoggedInSITName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfDefectsLoggedInSIT()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfValidDefectsLoggedInSITName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfValidDefectsLoggedInSIT()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfDefectsLoggedInUATName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfDefectsLoggedInUAT()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getNumberOfValidDefectsLoggedInUATName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getNumberOfValidDefectsLoggedInUAT()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);


					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfValidAutomationDefectsName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfValidAutomationDefects()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfDefectsName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfDefects()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("Defects");
					progressPojoArray.add(projectProgressPojo);


					try {
						index = index + 1;
						projectProgressPojo = new ProjectProgressPojo();
						projectProgressPojo.setId(index);
						projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
						projectProgressPojo.setName(next.getTotalNumberOfDefectsInProductionName());
						try {
							projectProgressPojo.setValue(Integer.toString(next.getTotalNumberOfDefectsInProduction()));
						} catch (Exception e) {
							projectProgressPojo.setValue("0");
						}
						projectProgressPojo.setCategory("Defects");
						progressPojoArray.add(projectProgressPojo);
					} catch (Exception e) {

					}


					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("date");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getActualStartDateMilestoneName());
					try {
						projectProgressPojo.setValue(next.getActualStartDateMilestone());
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("BasicInformation");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("date");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getActualEndDateMilestoneName());
					try {
						projectProgressPojo.setValue(next.getActualEndDateMilestone());
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setCategory("BasicInformation");
					progressPojoArray.add(projectProgressPojo);

					monthlyTestExecutionProgressArray.add(progressPojoArray);
				}
			}

					Gson gson = new Gson();
					response = gson.toJson(monthlyTestExecutionProgressArray);
					response = "{\"progress\":" + response + "}";

					System.out.println(response);
					return response;


		} catch (Exception e) {
		}

		return null;
	}

//	http://localhost:8082/springmvc/tutorial?topic=requestParam&author=daniel&site=jcg

	@PostMapping(value = "/projects/{id}/testExecution/monthlyProgress/month/{month}/year/{year}")
	@ResponseBody
	public String updateMonthlyTestExecutionProgress(@PathVariable("id") long id, @PathVariable(value="month") String month, @PathVariable(value="year") int year, @RequestBody List<ProjectProgressPojo> progressPojoArray) {

		try {
			int monthNumber = 0;

			if(month.contains("Jan"))
			{
				monthNumber = 1;
			}
			else if(month.contains("Feb"))
			{
				monthNumber = 2;
			}
			else if(month.contains("Mar"))
			{
				monthNumber = 3;
			}
			else if(month.contains("Apr"))
			{
				monthNumber = 4;
			}
			else if(month.contains("May"))
			{
				monthNumber = 5;
			}
			else if(month.contains("Jun"))
			{
				monthNumber = 6;
			}
			else if(month.contains("Jul"))
			{
				monthNumber = 7;
			}
			else if(month.contains("Aug"))
			{
				monthNumber = 8;
			}
			else if(month.contains("Sep"))
			{
				monthNumber = 9;
			}
			else if(month.contains("Oct"))
			{
				monthNumber = 10;
			}
			else if(month.contains("Nov"))
			{
				monthNumber = 11;
			}
			else if(month.contains("Dec"))
			{
				monthNumber = 12;
			}
			else
			{
				monthNumber = Integer.parseInt(month);
			}

			int yearNumber = year;


			boolean matchFound = false;

			Iterable<MonthlyTestExecutionProgress> monthlyTestExecutionProgress = monthlyTestExecutionProgressRepository.findAll();
			Iterator<MonthlyTestExecutionProgress> monthlyTestExecutionProgressIterator = monthlyTestExecutionProgress.iterator();


			while (monthlyTestExecutionProgressIterator.hasNext()) {
				MonthlyTestExecutionProgress next = monthlyTestExecutionProgressIterator.next();

				if ((next.getMonthNumber() == monthNumber) && (next.getProject().getId() == id) && (next.getYearNumber() == yearNumber)) {
					matchFound = true;

					for (int i = 0; i < progressPojoArray.size(); i++) {

						if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfManualUATTestCasesExecutedName())) {
							next.setTotalNumberOfManualUATTestCasesExecuted(progressPojoArray.get(i).getValue());
						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getSITServicesNotAvailableForTestingName())) {
							next.setSITServicesNotAvailableForTesting(progressPojoArray.get(i).getValue());
						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfManualSITTestCasesExecutedName())) {
							next.setTotalNumberOfManualSITTestCasesExecuted(progressPojoArray.get(i).getValue());
						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfDefectsLoggedInSITName())) {
							next.setTotalNumberOfDefectsLoggedInSIT(progressPojoArray.get(i).getValue());
						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfValidDefectsLoggedInSITName())) {
							next.setNumberOfValidDefectsLoggedInSIT(progressPojoArray.get(i).getValue());
						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfDefectsLoggedInUATName())) {
							next.setTotalNumberOfDefectsLoggedInUAT(progressPojoArray.get(i).getValue());
						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getNumberOfValidDefectsLoggedInUATName())) {
							next.setNumberOfValidDefectsLoggedInUAT(progressPojoArray.get(i).getValue());
						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfValidAutomationDefectsName())) {
							next.setTotalNumberOfValidAutomationDefects(progressPojoArray.get(i).getValue());
						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfDefectsName())) {
							next.setTotalNumberOfDefects(progressPojoArray.get(i).getValue());
						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getActualStartDateMilestoneName())) {
							next.setActualStartDateMilestone(progressPojoArray.get(i).getValue());
						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getActualEndDateMilestoneName())) {
							next.setActualEndDateMilestone(progressPojoArray.get(i).getValue());
						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfDefectsInProductionName())) {
							next.setTotalNumberOfDefectsInProduction(progressPojoArray.get(i).getValue());
						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getSITServicesInscopeName())) {
							next.setSITServicesInscope(progressPojoArray.get(i).getValue());
						}
					}

					next.setMonthNumber(monthNumber);
					next.setYearNumber(yearNumber);

					monthlyTestExecutionProgressRepository.save(next);
					break;
				}
			}
			if (matchFound == true)
				return "OK";
			else {

				MonthlyTestExecutionProgress progress = new MonthlyTestExecutionProgress();

				for (int i = 0; i < progressPojoArray.size(); i++) {


					if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getTotalNumberOfManualUATTestCasesExecutedName())) {
						progress.setTotalNumberOfManualUATTestCasesExecuted(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getSITServicesNotAvailableForTestingName())) {
						progress.setSITServicesNotAvailableForTesting(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getTotalNumberOfManualSITTestCasesExecutedName())) {
						progress.setTotalNumberOfManualSITTestCasesExecuted(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getTotalNumberOfDefectsLoggedInSITName())) {
						progress.setTotalNumberOfDefectsLoggedInSIT(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getNumberOfValidDefectsLoggedInSITName())) {
						progress.setNumberOfValidDefectsLoggedInSIT(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getTotalNumberOfDefectsLoggedInUATName())) {
						progress.setTotalNumberOfDefectsLoggedInUAT(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getNumberOfValidDefectsLoggedInUATName())) {
						progress.setNumberOfValidDefectsLoggedInUAT(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getTotalNumberOfValidAutomationDefectsName())) {
						progress.setTotalNumberOfValidAutomationDefects(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getTotalNumberOfDefectsName())) {
						progress.setTotalNumberOfDefects(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getActualStartDateMilestoneName())) {
						progress.setActualStartDateMilestone(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getActualEndDateMilestoneName())) {
						progress.setActualEndDateMilestone(progressPojoArray.get(i).getValue());
					}  else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getTotalNumberOfDefectsInProductionName())) {
						progress.setTotalNumberOfDefectsInProduction(progressPojoArray.get(i).getValue());
					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getSITServicesInscopeName())) {
						progress.setSITServicesInscope(progressPojoArray.get(i).getValue());
					}

				}

				Iterable<Project> allProjects = projectRepository.findAll();
				Iterator<Project> iterator = allProjects.iterator();

				while (iterator.hasNext()) {
					Project nextProject = iterator.next();

					if (nextProject.getId() == id) {
						progress.setProject(nextProject);
						break;
					}
				}
				progress.setMonthNumber(monthNumber);
				progress.setYearNumber(yearNumber);

				monthlyTestExecutionProgressRepository.save(progress);
				return "OK";
			}
		}catch (Exception e) {
		}
		return "Error";
	}
}