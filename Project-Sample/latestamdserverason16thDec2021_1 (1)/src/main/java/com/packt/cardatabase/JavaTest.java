package com.packt.cardatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class JavaTest {

public static void main(String args[]) {


    for (int yearsIndex = 2021; yearsIndex < 2051; yearsIndex++) {
        try {
            Calendar cal = Calendar.getInstance();
            String strDate = "1 Jan " + yearsIndex;
            Date date = new SimpleDateFormat("d MMM yyyy").parse(strDate);
            System.out.println(date);
            cal.setTime(date);

            for (int monthIndex = 1; monthIndex < 13; monthIndex++) {

                int res = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
                System.out.println("Today's Date = " + cal.getTime());
                System.out.println("Last Date of the current month = " + res);

            }
        } catch (Exception e) {
        }

    }
}
}