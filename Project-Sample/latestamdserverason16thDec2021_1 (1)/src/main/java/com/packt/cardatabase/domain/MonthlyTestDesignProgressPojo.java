package com.packt.cardatabase.domain;

import javax.persistence.*;

public class MonthlyTestDesignProgressPojo {


    private long id;

    private String totalNoOfTestCasesReusedForMultiCountryRollouts;
    private String totalNumberOfManualTestCasesDesigned;
    private String totalNumberOfTestCasesTaggedForMigrationExecution;
    private String actualStartDateMilestone;
    private String actualEndDateMilestone;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public MonthlyTestDesignProgressPojo() {}


    public String getTotalNoOfTestCasesReusedForMultiCountryRollouts() {
        return totalNoOfTestCasesReusedForMultiCountryRollouts;
    }

    public void setTotalNoOfTestCasesReusedForMultiCountryRollouts(String totalNoOfTestCasesReusedForMultiCountryRollouts) {
        this.totalNoOfTestCasesReusedForMultiCountryRollouts = totalNoOfTestCasesReusedForMultiCountryRollouts;
    }

    public String getTotalNumberOfManualTestCasesDesigned() {
        return totalNumberOfManualTestCasesDesigned;
    }

    public void setTotalNumberOfManualTestCasesDesigned(String totalNumberOfManualTestCasesDesigned) {
        this.totalNumberOfManualTestCasesDesigned = totalNumberOfManualTestCasesDesigned;
    }

    public String getTotalNumberOfTestCasesTaggedForMigrationExecution() {
        return totalNumberOfTestCasesTaggedForMigrationExecution;
    }

    public void setTotalNumberOfTestCasesTaggedForMigrationExecution(String totalNumberOfTestCasesTaggedForMigrationExecution) {
        this.totalNumberOfTestCasesTaggedForMigrationExecution = totalNumberOfTestCasesTaggedForMigrationExecution;
    }

    public String getActualStartDateMilestone() {
        return actualStartDateMilestone;
    }


    public void setActualStartDateMilestone(String actualStartDateMilestone) {
        this.actualStartDateMilestone = actualStartDateMilestone;
    }

    public String getActualEndDateMilestone() {
        return actualEndDateMilestone;
    }

    public void setActualEndDateMilestone(String actualEndDateMilestone) {
        this.actualEndDateMilestone = actualEndDateMilestone;
    }

}