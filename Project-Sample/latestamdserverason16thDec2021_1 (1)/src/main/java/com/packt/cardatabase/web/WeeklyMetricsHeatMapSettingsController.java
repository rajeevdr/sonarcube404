package com.packt.cardatabase.web;

import com.google.gson.Gson;
import com.packt.cardatabase.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@RestController
public class WeeklyMetricsHeatMapSettingsController {

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private WeeklyMetricsHeatMapSettingsRepository heatMapSettingsRepository;


	@RequestMapping(value="/projects/{id}/weeklyMetricsHeatMapSettings", method=RequestMethod.GET)
	@ResponseBody
	public String getMonthlyMetricsHeatMapSettings(@PathVariable("id") long id) {

		try {

    		ArrayList<WeeklyMetricsHeatMapSettingsPojo> heatMapSettingsPojoArray = new ArrayList<WeeklyMetricsHeatMapSettingsPojo>();

			Iterable<WeeklyMetricsHeatMapSettings> heatMapSettingsIterable = heatMapSettingsRepository.findAll();
			Iterator<WeeklyMetricsHeatMapSettings> heatMapSettingsIterator = heatMapSettingsIterable.iterator();
			String response = null;
			boolean recordFound = false;
			int index = 1;

			while (heatMapSettingsIterator.hasNext()) {
				WeeklyMetricsHeatMapSettings next = heatMapSettingsIterator.next();

				if (next.getProject().getId() == id) {
					recordFound = true;
					if(next.getCategory().contains("Productivity")) {
						WeeklyMetricsHeatMapSettingsPojo heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
						heatMapSettingsPojo.setId(index);
						heatMapSettingsPojo.setMetricName(next.getMetricName());
						heatMapSettingsPojo.setLCL(next.getLCL());
						heatMapSettingsPojo.setUCL(next.getUCL());
						heatMapSettingsPojo.setGoal(next.getGoal());
						heatMapSettingsPojo.setCategory(next.getCategory());
						heatMapSettingsPojoArray.add(heatMapSettingsPojo);
						index += 1;
					}
				}
			}




			heatMapSettingsIterable = heatMapSettingsRepository.findAll();
			heatMapSettingsIterator = heatMapSettingsIterable.iterator();

			while (heatMapSettingsIterator.hasNext()) {
				WeeklyMetricsHeatMapSettings next = heatMapSettingsIterator.next();

				if (next.getProject().getId() == id) {
					recordFound = true;
					if(next.getCategory().contains("Coverage")) {
						WeeklyMetricsHeatMapSettingsPojo heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
						heatMapSettingsPojo.setId(index);
						heatMapSettingsPojo.setMetricName(next.getMetricName());
						heatMapSettingsPojo.setLCL(next.getLCL());
						heatMapSettingsPojo.setUCL(next.getUCL());
						heatMapSettingsPojo.setGoal(next.getGoal());
						heatMapSettingsPojo.setCategory(next.getCategory());
						heatMapSettingsPojoArray.add(heatMapSettingsPojo);
						index += 1;
					}
				}
			}


			heatMapSettingsIterable = heatMapSettingsRepository.findAll();
			heatMapSettingsIterator = heatMapSettingsIterable.iterator();

			while (heatMapSettingsIterator.hasNext()) {
				WeeklyMetricsHeatMapSettings next = heatMapSettingsIterator.next();

				if (next.getProject().getId() == id) {
					recordFound = true;
					if(next.getCategory().contains("Defects")) {
						WeeklyMetricsHeatMapSettingsPojo heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
						heatMapSettingsPojo.setId(index);
						heatMapSettingsPojo.setMetricName(next.getMetricName());
						heatMapSettingsPojo.setLCL(next.getLCL());
						heatMapSettingsPojo.setUCL(next.getUCL());
						heatMapSettingsPojo.setGoal(next.getGoal());
						heatMapSettingsPojo.setCategory(next.getCategory());
						heatMapSettingsPojoArray.add(heatMapSettingsPojo);
						index += 1;
					}
				}
			}

			heatMapSettingsIterable = heatMapSettingsRepository.findAll();
			heatMapSettingsIterator = heatMapSettingsIterable.iterator();

			while (heatMapSettingsIterator.hasNext()) {
				WeeklyMetricsHeatMapSettings next = heatMapSettingsIterator.next();

				if (next.getProject().getId() == id) {
					recordFound = true;
					if(next.getCategory().contains("Attention")) {
						WeeklyMetricsHeatMapSettingsPojo heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
						heatMapSettingsPojo.setId(index);
						heatMapSettingsPojo.setMetricName(next.getMetricName());
						heatMapSettingsPojo.setLCL(next.getLCL());
						heatMapSettingsPojo.setUCL(next.getUCL());
						heatMapSettingsPojo.setGoal(next.getGoal());
						heatMapSettingsPojo.setCategory(next.getCategory());
						heatMapSettingsPojoArray.add(heatMapSettingsPojo);
						index += 1;
					}
				}
			}

			heatMapSettingsIterable = heatMapSettingsRepository.findAll();
			heatMapSettingsIterator = heatMapSettingsIterable.iterator();

			while (heatMapSettingsIterator.hasNext()) {
				WeeklyMetricsHeatMapSettings next = heatMapSettingsIterator.next();

				if (next.getProject().getId() == id) {
					recordFound = true;
					if(next.getCategory().contains("Misc")) {
						WeeklyMetricsHeatMapSettingsPojo heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
						heatMapSettingsPojo.setId(index);
						heatMapSettingsPojo.setMetricName(next.getMetricName());
						heatMapSettingsPojo.setLCL(next.getLCL());
						heatMapSettingsPojo.setUCL(next.getUCL());
						heatMapSettingsPojo.setGoal(next.getGoal());
						heatMapSettingsPojo.setCategory(next.getCategory());
						heatMapSettingsPojoArray.add(heatMapSettingsPojo);
						index += 1;
					}
				}
			}

			if(recordFound == false) {
				WeeklyMetricsHeatMapSettingsPojo heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getManualTestCaseAuthoringProductivityName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Productivity");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;

				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getRegressionTestAutomationScriptingProductivityName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Productivity");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;

				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getApiTestAutomationScriptingProductivityName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Productivity");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;

				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getInProjectAutomationScriptingProductivityName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Productivity");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;

				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getManualTestExecutionProductivityName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Productivity");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;

				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getRegressionAutomationTestExecutionProductivityName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Productivity");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;

				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getInProjectAutomationTestExecutionProductivityName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Productivity");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;

				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getAPIAutomationTestExecutionProductivityName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Productivity");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;

				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getDesignCoverageName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Coverage");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;

				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getSanityAutomationCoverageName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Coverage");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;


				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getPercentageOfRegressionAutomationName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Coverage");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;

				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getMigratedDataExecutionName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Coverage");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;


				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getDefectLeakageSITToUATName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Defects");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;

				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getDefectLeakageToProductionName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Defects");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;

				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getNumberOfDefectsRejectedForTheWeekName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Defects");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;

				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getTotalNumberOfDefectsReopenedForTheWeekName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Defects");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;

				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getDefectRejectionName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Defects");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;

				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getTestCaseNotExecutedManualName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Attention");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;

				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getTestCaseNotExecutedAutomationName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Attention");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;

				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getPercentageOfPendingRegressionAutomationName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Attention");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;


				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getPercentageOfRequirementsNotCoveredName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Attention");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;

				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getAverageTimeTakenToResolveSev1DefectsName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Attention");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;

				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getDowntimeAndImpactAnalysisDesignAvgProductivityLostName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Attention");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;

				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getDowntimeAndImpactAnalysisExecutionAvgProductivityLostName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Attention");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;

				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getTestReviewEfficiencyName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Misc");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;


				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getCurrentQualityRatioName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Misc");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;

				heatMapSettingsPojo = new WeeklyMetricsHeatMapSettingsPojo();
				heatMapSettingsPojo.setId(index);
				heatMapSettingsPojo.setMetricName(WeeklyMetrics.getQualityOfFixesName());
				heatMapSettingsPojo.setLCL("");
				heatMapSettingsPojo.setUCL("");
				heatMapSettingsPojo.setGoal("");
				heatMapSettingsPojo.setCategory("Misc");
				heatMapSettingsPojoArray.add(heatMapSettingsPojo);
				index += 1;



			}

			Gson gson = new Gson();
			response = gson.toJson(heatMapSettingsPojoArray);
			response = "{\"MetricsSettings\":" + response + "}";
			System.out.println(response);
			return response;

		} catch (Exception e) {

		}
		return "{\"MetricsSettings\":[] }";
	}



	@PostMapping(value = "/projects/{id}/weeklyMetricsHeatMapSettings")
	@ResponseBody
	public String updateMonthlyMetricsHeatMapSettings(@PathVariable("id") long id, @RequestBody List<WeeklyMetricsHeatMapSettingsPojo> heatMapSettingsPojoArray) {

		try {

			Iterable<WeeklyMetricsHeatMapSettings> heatMapSettingsIterable = heatMapSettingsRepository.findAll();
			Iterator<WeeklyMetricsHeatMapSettings> heatMapSettingsIterator = heatMapSettingsIterable.iterator();

			while (heatMapSettingsIterator.hasNext()) {
				WeeklyMetricsHeatMapSettings next = heatMapSettingsIterator.next();

				if (next.getProject().getId() == id) {

					try {
						heatMapSettingsRepository.delete(next);
					} catch (Exception e) {

					}
				}
			}

				for (int i = 0; i < heatMapSettingsPojoArray.size(); i++) {
					WeeklyMetricsHeatMapSettings next = new WeeklyMetricsHeatMapSettings();

					System.out.println(heatMapSettingsPojoArray.get(i).getMetricName());
					System.out.println(heatMapSettingsPojoArray.get(i).getLCL());
					System.out.println(heatMapSettingsPojoArray.get(i).getUCL());
					System.out.println(heatMapSettingsPojoArray.get(i).getGoal());

					boolean matchFound = false;

					if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getManualTestCaseAuthoringProductivityName()) == 0) {
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Productivity");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getRegressionTestAutomationScriptingProductivityName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Productivity");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getApiTestAutomationScriptingProductivityName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Productivity");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getInProjectAutomationScriptingProductivityName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Productivity");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getDesignCoverageName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Coverage");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getSanityAutomationCoverageName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Coverage");
						matchFound = true;
					}  else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getPercentageOfRegressionAutomationName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Coverage");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getTestReviewEfficiencyName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Misc");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getDowntimeAndImpactAnalysisDesignAvgProductivityLostName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Attention");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getManualTestExecutionProductivityName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Productivity");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getRegressionAutomationTestExecutionProductivityName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Productivity");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getInProjectAutomationTestExecutionProductivityName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Productivity");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getAPIAutomationTestExecutionProductivityName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Productivity");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getMigratedDataExecutionName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Coverage");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getDefectRejectionName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Defects");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getCurrentQualityRatioName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Misc");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getQualityOfFixesName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Misc");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getDowntimeAndImpactAnalysisExecutionAvgProductivityLostName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Attention");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getDefectLeakageSITToUATName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Defects");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getDefectLeakageToProductionName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Defects");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getTestCaseNotExecutedManualName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Attention");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getTestCaseNotExecutedAutomationName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Attention");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getPercentageOfPendingRegressionAutomationName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Attention");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getPercentageOfRequirementsNotCoveredName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Attention");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getAverageTimeTakenToResolveSev1DefectsName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Attention");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getNumberOfDefectsRejectedForTheWeekName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Defects");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getTotalNumberOfDefectsReopenedForTheWeekName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Defects");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getPercentageOfPendingRegressionAutomationName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Attention");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getPercentageOfRequirementsNotCoveredName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Attention");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(WeeklyMetrics.getAverageTimeTakenToResolveSev1DefectsName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Attention");
						matchFound = true;
					}



					if(matchFound == true) {
						Iterable<Project> allProjects = projectRepository.findAll();
						Iterator<Project> iterator = allProjects.iterator();

						while (iterator.hasNext()) {
							Project nextProject = iterator.next();

							if (nextProject.getId() == id) {
								next.setProject(nextProject);
								break;
							}
						}


						heatMapSettingsRepository.save(next);
					}
				}

				return "OK";

		}catch (Exception e) {
		}
		return "Error";
	}


}