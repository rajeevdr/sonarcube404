package com.packt.cardatabase.domain;


import javax.persistence.*;
import java.util.ArrayList;

@Entity
public class MigrationModuleDetails {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;

    private String module;
    private ArrayList<String> testExecutionFolderID;
    private ArrayList<String> testDesignFolderID;

    private String testExecutionFolderIdString;
    private String testDesignFolderIdString;


    public MigrationModuleDetails() {
        super();
    }

    public MigrationModuleDetails(String module,
                                  ArrayList<String> testExecutionFolderID,
                                  ArrayList<String> testDesignFolderID) {
        super();
        this.module = module;
        this.testExecutionFolderID = testExecutionFolderID;
        this.testDesignFolderID = testDesignFolderID;

    }

    public MigrationModuleDetails(MigrationModuleDetails almSettings) {
        super();
        this.module = almSettings.getModule();
        this.testExecutionFolderID = almSettings.getTestExecutionFolderId();
        this.testDesignFolderID = almSettings.getTestDesignFolderId();

    }

    public String getModule() {
        return module;
    }

    public String getModuleName() {
        return "module";
    }

    public void setModule(String module) {
        this.module = module;
    }

    public ArrayList<String> getTestExecutionFolderId() {
        return testExecutionFolderID;
    }

    public String getTestExecutionFolderIDName() {
        return "testExecutionFolderID";
    }

    public void setTestExecutionFolderID(ArrayList<String> testExecutionFolderID) {
        this.testExecutionFolderID = testExecutionFolderID;
    }

    public ArrayList<String> getTestDesignFolderId() {
        return testDesignFolderID;
    }

    public String getTestDesignFolderIDName() {
        return "testDesignFolderID";
    }

    public void setTestDesignFolderID(ArrayList<String> testDesignFolderID) {
        this.testDesignFolderID = testDesignFolderID;
    }

    public String getTestDesignFolderIdString() {
        return testDesignFolderIdString;
    }

    public void setTestDesignFolderIdString(String testDesignFolderIdString) {
        this.testDesignFolderIdString = testDesignFolderIdString;
    }

    public String getTestExecutionFolderIdString() {
        return testExecutionFolderIdString;
    }

    public void setTestExecutionFolderIdString(String testExecutionFolderIdString) {
        this.testExecutionFolderIdString = testExecutionFolderIdString;
    }


    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

}