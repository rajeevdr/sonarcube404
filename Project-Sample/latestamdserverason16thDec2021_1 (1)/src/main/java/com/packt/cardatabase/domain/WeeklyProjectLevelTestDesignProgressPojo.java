package com.packt.cardatabase.domain;


import javax.persistence.*;

public class WeeklyProjectLevelTestDesignProgressPojo {

    private String numberOfTestableRequirementsCoveredThisWeek;
    private String totalNumberOfRegressionTestCasesAutomated;
    private String totalNumberOfInProjectAutomationTestCasesAutomated;
    private String totalNumberOfAPIAutomationTestCasesAutomated;
    private String totalNumberOfTestDesignReviewDefectsFound;
    private String testPlanningEffort;
    private String manualTestCasesDesignEffort;
    private String regressionAutomationDesignEffort;
    private String apiAutomationDesignEffort;
    private String inProjectAutomationDesignEffort;
    private String manualTestCasesMaintenanceEffort;
    private String automationTestCasesMaintenanceEffort;
    private String environmentDowntimeDuringTestDesignPhase;
    private String numberOfResourcesAuthoringManualTestCases;
    private String numberOfResourcesAuthoringAutomationTestCases;



    public WeeklyProjectLevelTestDesignProgressPojo() {}


    public WeeklyProjectLevelTestDesignProgressPojo(String numberOfTestableRequirementsCoveredThisWeek,
                                                String totalNumberOfRegressionTestCasesAutomated,
                                                String totalNumberOfInProjectAutomationTestCasesAutomated,
                                                String totalNumberOfAPIAutomationTestCasesAutomated,
                                                String totalNumberOfTestDesignReviewDefectsFound,
                                                String testPlanningEffort,
                                                String manualTestCasesDesignEffort,
                                                String regressionAutomationDesignEffort,
                                                String inProjectAutomationDesignEffort,
                                                String apiAutomationDesignEffort,
                                                String manualTestCasesMaintenanceEffort,
                                                String automationTestCasesMaintenanceEffort,
                                                String environmentDowntimeDuringTestDesignPhase,
                                                String numberOfResourcesAuthoringManualTestCases,
                                                String numberOfResourcesAuthoringAutomationTestCases) {


        this.numberOfTestableRequirementsCoveredThisWeek = numberOfTestableRequirementsCoveredThisWeek;
        this.totalNumberOfRegressionTestCasesAutomated = totalNumberOfRegressionTestCasesAutomated;
        this.totalNumberOfInProjectAutomationTestCasesAutomated = totalNumberOfInProjectAutomationTestCasesAutomated;
        this.totalNumberOfAPIAutomationTestCasesAutomated = totalNumberOfAPIAutomationTestCasesAutomated;
        this.totalNumberOfTestDesignReviewDefectsFound = totalNumberOfTestDesignReviewDefectsFound;
        this.testPlanningEffort = testPlanningEffort;
        this.manualTestCasesDesignEffort = manualTestCasesDesignEffort;
        this.regressionAutomationDesignEffort = regressionAutomationDesignEffort;
        this.inProjectAutomationDesignEffort = inProjectAutomationDesignEffort;
        this.apiAutomationDesignEffort = apiAutomationDesignEffort;
        this.manualTestCasesMaintenanceEffort = manualTestCasesMaintenanceEffort;
        this.automationTestCasesMaintenanceEffort = automationTestCasesMaintenanceEffort;
        this.environmentDowntimeDuringTestDesignPhase = environmentDowntimeDuringTestDesignPhase;
        this.numberOfResourcesAuthoringManualTestCases = numberOfResourcesAuthoringManualTestCases;
        this.numberOfResourcesAuthoringAutomationTestCases = numberOfResourcesAuthoringAutomationTestCases;

    }


    public String getNumberOfTestableRequirementsCoveredThisWeek(){
        return this.numberOfTestableRequirementsCoveredThisWeek;
    }
    public void setNumberOfTestableRequirementsCoveredThisWeek(String numberOfTestableRequirementsCoveredThisWeek){
        this.numberOfTestableRequirementsCoveredThisWeek =  numberOfTestableRequirementsCoveredThisWeek;
    }

    public String getNumberOfTestableRequirementsCoveredThisWeekName(){
        return "NumberOfTestableRequirementsCoveredThisWeek";
    }



    public String getTotalNumberOfTestDesignReviewDefectsFound(){
        return this.totalNumberOfTestDesignReviewDefectsFound;
    }

    public String gettotalNumberOfRegressionTestCasesAutomated(){
        return this.totalNumberOfRegressionTestCasesAutomated;
    }


    public String getTotalNumberOfInProjectAutomationTestCasesAutomated(){
        return this.totalNumberOfInProjectAutomationTestCasesAutomated;
    }

    public String getTotalNumberOfAPIAutomationTestCasesAutomated(){
        return this.totalNumberOfAPIAutomationTestCasesAutomated;
    }

    public String getTestPlanningEffort(){
        return this.testPlanningEffort;
    }

    public String getManualTestCasesDesignEffort(){
        return this.manualTestCasesDesignEffort;
    }


    public String getRegressionAutomationDesignEffort(){
        return this.regressionAutomationDesignEffort;
    }


    public String getApiAutomationDesignEffort(){
        return this.apiAutomationDesignEffort;
    }

    public String getInProjectAutomationDesignEffort(){
        return this.inProjectAutomationDesignEffort;
    }

    public String getManualTestCasesMaintenanceEffort(){
        return this.manualTestCasesMaintenanceEffort;
    }

    public String getAutomationTestCasesMaintenanceEffort(){
        return this.automationTestCasesMaintenanceEffort;
    }

    public String getEnvironmentDowntimeDuringTestDesignPhase(){
        return this.environmentDowntimeDuringTestDesignPhase;
    }

    public String getNumberOfResourcesAuthoringManualTestCases(){
        return this.numberOfResourcesAuthoringManualTestCases;
    }

    public String getNumberOfResourcesAuthoringAutomationTestCases(){
        return this.numberOfResourcesAuthoringAutomationTestCases;
    }


    public void setTotalNumberOfInProjectAutomationTestCasesAutomated(String totalNumberOfInProjectAutomationTestCasesAutomated){
        this.totalNumberOfInProjectAutomationTestCasesAutomated =  totalNumberOfInProjectAutomationTestCasesAutomated;
    }

    public void setTotalNumberOfAPIAutomationTestCasesAutomated(String totalNumberOfAPIAutomationTestCasesAutomated){
        this.totalNumberOfAPIAutomationTestCasesAutomated =  totalNumberOfAPIAutomationTestCasesAutomated;
    }


    public void setTotalNumberOfTestDesignReviewDefectsFound(String totalNumberOfTestDesignReviewDefectsFound){
        this.totalNumberOfTestDesignReviewDefectsFound =  totalNumberOfTestDesignReviewDefectsFound;
    }

    public void setTestPlanningEffort(String testPlanningEffort){
        this.testPlanningEffort =  testPlanningEffort;
    }

    public void setManualTestCasesDesignEffort(String manualTestCasesDesignEffort){
        this.manualTestCasesDesignEffort =  manualTestCasesDesignEffort;
    }


    public void setRegressionAutomationDesignEffort(String regressionAutomationDesignEffort){
        this.regressionAutomationDesignEffort =  regressionAutomationDesignEffort;
    }

    public void setApiAutomationDesignEffort(String apiAutomationDesignEffort){
        this.apiAutomationDesignEffort =  apiAutomationDesignEffort;
    }

    public void setInProjectAutomationDesignEffort(String inProjectAutomationDesignEffort){
        this.inProjectAutomationDesignEffort =  inProjectAutomationDesignEffort;
    }


    public void setManualTestCasesMaintenanceEffort(String manualTestCasesMaintenanceEffort){
        this.manualTestCasesMaintenanceEffort =  manualTestCasesMaintenanceEffort;
    }

    public void setAutomationTestCasesMaintenanceEffort(String automationTestCasesMaintenanceEffort){
        this.automationTestCasesMaintenanceEffort =  automationTestCasesMaintenanceEffort;
    }

    public void setEnvironmentDowntimeDuringTestDesignPhase(String environmentDowntimeDuringTestDesignPhase){
        this.environmentDowntimeDuringTestDesignPhase =  environmentDowntimeDuringTestDesignPhase;
    }

    public void setNumberOfResourcesAuthoringManualTestCases(String numberOfResourcesAuthoringManualTestCases){
        this.numberOfResourcesAuthoringManualTestCases =  numberOfResourcesAuthoringManualTestCases;
    }

    public void setNumberOfResourcesAuthoringAutomationTestCases(String numberOfResourcesAuthoringAutomationTestCases){
        this.numberOfResourcesAuthoringAutomationTestCases =  numberOfResourcesAuthoringAutomationTestCases;
    }


    public String gettotalNumberOfRegressionTestCasesAutomatedName(){
        return "TotalNumberOfInProjectAutomationTestCasesAutomated";
    }


    public String getTotalNumberOfInProjectAutomationTestCasesAutomatedName(){
        return "TotalNumberOfInProjectAutomationTestCasesAutomated";
    }

    public String getTotalNumberOfAPIAutomationTestCasesAutomatedName(){
        return "TotalNumberOfAPIAutomationTestCasesAutomated";
    }


    public String getTotalNumberOfTestDesignReviewDefectsFoundName(){
        return "TotalNumberOfTestDesignReviewDefectsFound";
    }

    public String getTestPlanningEffortName(){
        return "TestPlanningEffort";
    }

    public String getManualTestCasesDesignEffortName(){
        return "ManualTestCasesDesignEffort";
    }

    public String getAutomationDesignEffortName(){
        return "AutomationDesignEffort";
    }

    public String getRegressionAutomationTestCasesDesignEffortName(){
        return "RegressionAutomationDesignEffort";
    }


    public String getApiAutomationDesignEffortName(){
        return "ApiAutomationDesignEffort";
    }

    public String getInProjectAutomationDesignEffortName(){
        return "InProjectAutomationDesignEffort";
    }


    public String getManualTestCasesMaintenanceEffortName(){
        return "ManualTestCasesMaintenanceEffort";
    }

    public String getAutomationTestCasesMaintenanceEffortName(){
        return "AutomationTestCasesMaintenanceEffort";
    }

    public String getEnvironmentDowntimeDuringTestDesignPhaseName(){
        return "EnvironmentDowntimeDuringTestDesignPhase";
    }

    public String getNumberOfResourcesAuthoringManualTestCasesName(){
        return "NumberOfResourcesAuthoringManualTestCases";
    }

    public String getNumberOfResourcesAuthoringAutomationTestCasesName(){
        return "NumberOfResourcesAuthoringAutomationTestCases";
    }


}