package com.packt.cardatabase.domain;

import javax.persistence.*;
import java.util.ArrayList;

@Entity
public class ProjectLevelDefectsEntity {
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private long id;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "project")
	private Project project;

	public ProjectLevelDefectsEntity(){
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	private String name;
	private ArrayList<String> sit;
	private ArrayList<String> uat;
	private ArrayList<String> production;
	private ArrayList<String> automation;
	private ArrayList<String> manual;


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public ArrayList<String> getSit() {
		return sit;
	}

	public void setSit(ArrayList<String> sit) {
		this.sit = sit;
	}

	public ArrayList<String> getUat() {
		return uat;
	}

	public void setUat(ArrayList<String> uat) {
		this.uat = uat;
	}

	public ArrayList<String> getProduction() {
		return production;
	}

	public void setProduction(ArrayList<String> production) {
		this.production = production;
	}

	public ArrayList<String> getAutomation() {
		return automation;
	}

	public void setAutomation(ArrayList<String> automation) {
		this.automation = automation;
	}

	public ArrayList<String> getManual() {
		return manual;
	}

	public void setManual(ArrayList<String> manual) {
		this.manual = manual;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

}