package com.packt.cardatabase.domain;


public class ModulePojo {

    private long id;

    private String name;
    private String estimatedManualTestCases;
    private String estimatedRegressionAutomationTestCases;
    private String estimatedSanityAutomationTestCases;
    private String complexity;


    public ModulePojo() {}

    public ModulePojo(String name,
                  String estimatedManualTestCases,
                  String estimatedRegressionAutomationTestCases,
                  String estimatedSanityAutomationTestCases,
                  String complexity) {
        super();
        this.name = name;
        this.estimatedManualTestCases = estimatedManualTestCases;
        this.estimatedRegressionAutomationTestCases = estimatedRegressionAutomationTestCases;
        this.estimatedSanityAutomationTestCases = estimatedSanityAutomationTestCases;
        this.complexity = complexity;
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEstimatedManualTestCases() {
        return estimatedManualTestCases;
    }

    public void setEstimatedManualTestCases(String estimatedManualTestCases) {
        this.estimatedManualTestCases = estimatedManualTestCases;
    }

    public String getEstimatedRegressionAutomationTestCases() {
        return estimatedRegressionAutomationTestCases;
    }

    public void setEstimatedRegressionAutomationTestCases(String estimatedRegressionAutomationTestCases) {
        this.estimatedRegressionAutomationTestCases = estimatedRegressionAutomationTestCases;
    }

    public String getEstimatedSanityAutomationTestCases() {
        return estimatedSanityAutomationTestCases;
    }

    public void setEstimatedSanityAutomationTestCases(String estimatedSanityAutomationTestCases) {
        this.estimatedSanityAutomationTestCases = estimatedSanityAutomationTestCases;
    }

    public String getComplexity() {
        return complexity;
    }

    public void setComplexity(String complexity) {
        this.complexity = complexity;
    }


}