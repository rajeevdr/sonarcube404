package com.packt.cardatabase.web;

import com.google.gson.Gson;
import com.packt.cardatabase.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@RestController
public class MonthlyMetricsHeatMapSettingsController {

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private MonthlyMetricsHeatMapSettingsRepository heatMapSettingsRepository;


	@RequestMapping(value="/projects/{id}/monthlyMetricsHeatMapSettings", method=RequestMethod.GET)
	@ResponseBody
	public String getMonthlyMetricsHeatMapSettings(@PathVariable("id") long id) {

		try {

    		ArrayList<MonthlyMetricsHeatMapSettingsPojo> heatMapSettingsPojoArray = new ArrayList<MonthlyMetricsHeatMapSettingsPojo>();

			Iterable<MonthlyMetricsHeatMapSettings> heatMapSettingsIterable = heatMapSettingsRepository.findAll();
			Iterator<MonthlyMetricsHeatMapSettings> heatMapSettingsIterator = heatMapSettingsIterable.iterator();
			String response = null;
			boolean recordFound = false;
			int index = 1;

			while (heatMapSettingsIterator.hasNext()) {
				MonthlyMetricsHeatMapSettings next = heatMapSettingsIterator.next();

				if (next.getProject().getId() == id) {
					recordFound = true;
					if(next.getCategory().contains("Variance")) {

						MonthlyMetricsHeatMapSettingsPojo heatMapSettingsPojo = new MonthlyMetricsHeatMapSettingsPojo();
						heatMapSettingsPojo.setId(index);
						index++;
						heatMapSettingsPojo.setMetricName(next.getMetricName());
						heatMapSettingsPojo.setLCL(next.getLCL());
						heatMapSettingsPojo.setUCL(next.getUCL());
						heatMapSettingsPojo.setGoal(next.getGoal());
						heatMapSettingsPojo.setCategory(next.getCategory());
						heatMapSettingsPojoArray.add(heatMapSettingsPojo);
					}
				}
			}


			heatMapSettingsIterable = heatMapSettingsRepository.findAll();
			heatMapSettingsIterator = heatMapSettingsIterable.iterator();

			while (heatMapSettingsIterator.hasNext()) {
				MonthlyMetricsHeatMapSettings next = heatMapSettingsIterator.next();

				if (next.getProject().getId() == id) {
					recordFound = true;
					if(next.getCategory().contains("Cost")) {

						MonthlyMetricsHeatMapSettingsPojo heatMapSettingsPojo = new MonthlyMetricsHeatMapSettingsPojo();
						heatMapSettingsPojo.setId(index);
						index++;
						heatMapSettingsPojo.setMetricName(next.getMetricName());
						heatMapSettingsPojo.setLCL(next.getLCL());
						heatMapSettingsPojo.setUCL(next.getUCL());
						heatMapSettingsPojo.setGoal(next.getGoal());
						heatMapSettingsPojo.setCategory(next.getCategory());
						heatMapSettingsPojoArray.add(heatMapSettingsPojo);
					}
				}
			}

			heatMapSettingsIterable = heatMapSettingsRepository.findAll();
			heatMapSettingsIterator = heatMapSettingsIterable.iterator();

			while (heatMapSettingsIterator.hasNext()) {
				MonthlyMetricsHeatMapSettings next = heatMapSettingsIterator.next();

				if (next.getProject().getId() == id) {
					recordFound = true;
					if(next.getCategory().contains("Defects")) {

						MonthlyMetricsHeatMapSettingsPojo heatMapSettingsPojo = new MonthlyMetricsHeatMapSettingsPojo();
						heatMapSettingsPojo.setId(index);
						index++;
						heatMapSettingsPojo.setMetricName(next.getMetricName());
						heatMapSettingsPojo.setLCL(next.getLCL());
						heatMapSettingsPojo.setUCL(next.getUCL());
						heatMapSettingsPojo.setGoal(next.getGoal());
						heatMapSettingsPojo.setCategory(next.getCategory());
						heatMapSettingsPojoArray.add(heatMapSettingsPojo);
					}
				}
			}

			heatMapSettingsIterable = heatMapSettingsRepository.findAll();
			heatMapSettingsIterator = heatMapSettingsIterable.iterator();

			while (heatMapSettingsIterator.hasNext()) {
				MonthlyMetricsHeatMapSettings next = heatMapSettingsIterator.next();

				if (next.getProject().getId() == id) {
					recordFound = true;
					if(next.getCategory().contains("Potential")) {

						MonthlyMetricsHeatMapSettingsPojo heatMapSettingsPojo = new MonthlyMetricsHeatMapSettingsPojo();
						heatMapSettingsPojo.setId(index);
						index++;
						heatMapSettingsPojo.setMetricName(next.getMetricName());
						heatMapSettingsPojo.setLCL(next.getLCL());
						heatMapSettingsPojo.setUCL(next.getUCL());
						heatMapSettingsPojo.setGoal(next.getGoal());
						heatMapSettingsPojo.setCategory(next.getCategory());
						heatMapSettingsPojoArray.add(heatMapSettingsPojo);
					}
				}
			}


if(recordFound == false) {
	MonthlyMetricsHeatMapSettingsPojo heatMapSettingsPojo = new MonthlyMetricsHeatMapSettingsPojo();
	heatMapSettingsPojo.setMetricName(MonthlyMetrics.getCostVarianceName());
	heatMapSettingsPojo.setLCL("");
	heatMapSettingsPojo.setUCL("");
	heatMapSettingsPojo.setGoal("");
	heatMapSettingsPojo.setCategory("Variance");
	heatMapSettingsPojo.setId(index);
	index += 1;
	heatMapSettingsPojoArray.add(heatMapSettingsPojo);

	heatMapSettingsPojo = new MonthlyMetricsHeatMapSettingsPojo();
	heatMapSettingsPojo.setMetricName(MonthlyMetrics.getEffortVarianceName());
	heatMapSettingsPojo.setLCL("");
	heatMapSettingsPojo.setUCL("");
	heatMapSettingsPojo.setGoal("");
	heatMapSettingsPojo.setCategory("Variance");
	heatMapSettingsPojo.setId(index);
	index += 1;
	heatMapSettingsPojoArray.add(heatMapSettingsPojo);

	heatMapSettingsPojo = new MonthlyMetricsHeatMapSettingsPojo();
	heatMapSettingsPojo.setMetricName(MonthlyMetrics.getScopeVarianceName());
	heatMapSettingsPojo.setLCL("");
	heatMapSettingsPojo.setUCL("");
	heatMapSettingsPojo.setGoal("");
	heatMapSettingsPojo.setCategory("Variance");
	heatMapSettingsPojoArray.add(heatMapSettingsPojo);

	heatMapSettingsPojo = new MonthlyMetricsHeatMapSettingsPojo();
	heatMapSettingsPojo.setMetricName(MonthlyMetrics.getScheduleVarianceName());
	heatMapSettingsPojo.setLCL("");
	heatMapSettingsPojo.setUCL("");
	heatMapSettingsPojo.setGoal("");
	heatMapSettingsPojo.setCategory("Variance");
	heatMapSettingsPojo.setId(index);
	index += 1;
	heatMapSettingsPojoArray.add(heatMapSettingsPojo);


	heatMapSettingsPojo = new MonthlyMetricsHeatMapSettingsPojo();
	heatMapSettingsPojo.setMetricName(MonthlyMetrics.getCostPerDefectName());
	heatMapSettingsPojo.setLCL("");
	heatMapSettingsPojo.setUCL("");
	heatMapSettingsPojo.setGoal("");
	heatMapSettingsPojo.setCategory("Cost");
	heatMapSettingsPojo.setId(index);
	index += 1;
	heatMapSettingsPojoArray.add(heatMapSettingsPojo);


	heatMapSettingsPojo = new MonthlyMetricsHeatMapSettingsPojo();
	heatMapSettingsPojo.setMetricName(MonthlyMetrics.getCostPerTestCaseName());
	heatMapSettingsPojo.setLCL("");
	heatMapSettingsPojo.setUCL("");
	heatMapSettingsPojo.setGoal("");
	heatMapSettingsPojo.setCategory("Cost");
	heatMapSettingsPojo.setId(index);
	index += 1;
	heatMapSettingsPojoArray.add(heatMapSettingsPojo);

	heatMapSettingsPojo = new MonthlyMetricsHeatMapSettingsPojo();
	heatMapSettingsPojo.setMetricName(MonthlyMetrics.getSITDefectDensityName());
	heatMapSettingsPojo.setLCL("");
	heatMapSettingsPojo.setUCL("");
	heatMapSettingsPojo.setGoal("");
	heatMapSettingsPojo.setCategory("Defects");
	heatMapSettingsPojo.setId(index);
	index += 1;
	heatMapSettingsPojoArray.add(heatMapSettingsPojo);

	heatMapSettingsPojo = new MonthlyMetricsHeatMapSettingsPojo();
	heatMapSettingsPojo.setMetricName(MonthlyMetrics.getUATDefectDensityName());
	heatMapSettingsPojo.setLCL("");
	heatMapSettingsPojo.setUCL("");
	heatMapSettingsPojo.setGoal("");
	heatMapSettingsPojo.setCategory("Defects");
	heatMapSettingsPojo.setId(index);
	index += 1;
	heatMapSettingsPojoArray.add(heatMapSettingsPojo);

	heatMapSettingsPojo = new MonthlyMetricsHeatMapSettingsPojo();
	heatMapSettingsPojo.setMetricName(MonthlyMetrics.getDefectLeakageSITToUATName());
	heatMapSettingsPojo.setLCL("");
	heatMapSettingsPojo.setUCL("");
	heatMapSettingsPojo.setGoal("");
	heatMapSettingsPojo.setCategory("Defects");
	heatMapSettingsPojo.setId(index);
	index += 1;
	heatMapSettingsPojoArray.add(heatMapSettingsPojo);

	heatMapSettingsPojo = new MonthlyMetricsHeatMapSettingsPojo();
	heatMapSettingsPojo.setMetricName(MonthlyMetrics.getDefectLeakageToProductionName());
	heatMapSettingsPojo.setLCL("");
	heatMapSettingsPojo.setUCL("");
	heatMapSettingsPojo.setGoal("");
	heatMapSettingsPojo.setCategory("Defects");
	heatMapSettingsPojo.setId(index);
	index += 1;
	heatMapSettingsPojoArray.add(heatMapSettingsPojo);

	heatMapSettingsPojo = new MonthlyMetricsHeatMapSettingsPojo();
	heatMapSettingsPojo.setMetricName(MonthlyMetrics.getInProjectAutomationPercentageExtentToPotentialName());
	heatMapSettingsPojo.setLCL("");
	heatMapSettingsPojo.setUCL("");
	heatMapSettingsPojo.setGoal("");
	heatMapSettingsPojo.setCategory("Potential");
	heatMapSettingsPojo.setId(index);
	index += 1;
	heatMapSettingsPojoArray.add(heatMapSettingsPojo);

	heatMapSettingsPojo = new MonthlyMetricsHeatMapSettingsPojo();
	heatMapSettingsPojo.setMetricName(MonthlyMetrics.getApiAutomationPercentageExtentToPotentialName());
	heatMapSettingsPojo.setLCL("");
	heatMapSettingsPojo.setUCL("");
	heatMapSettingsPojo.setGoal("");
	heatMapSettingsPojo.setCategory("Potential");
	heatMapSettingsPojo.setId(index);
	index += 1;
	heatMapSettingsPojoArray.add(heatMapSettingsPojo);

	heatMapSettingsPojo = new MonthlyMetricsHeatMapSettingsPojo();
	heatMapSettingsPojo.setMetricName(MonthlyMetrics.getPercentageOfTCsIdentifiedForMigrationName());
	heatMapSettingsPojo.setLCL("");
	heatMapSettingsPojo.setUCL("");
	heatMapSettingsPojo.setGoal("");
	heatMapSettingsPojo.setCategory("Potential");
	heatMapSettingsPojo.setId(index);
	index += 1;

	heatMapSettingsPojo = new MonthlyMetricsHeatMapSettingsPojo();
	heatMapSettingsPojo.setMetricName(MonthlyMetrics.getManualEffectivenessName());
	heatMapSettingsPojo.setLCL("");
	heatMapSettingsPojo.setUCL("");
	heatMapSettingsPojo.setGoal("");
	heatMapSettingsPojo.setCategory("Defects");
	heatMapSettingsPojo.setId(index);
	index += 1;

	heatMapSettingsPojo = new MonthlyMetricsHeatMapSettingsPojo();
	heatMapSettingsPojo.setMetricName(MonthlyMetrics.getAutomationEffectivenessName());
	heatMapSettingsPojo.setLCL("");
	heatMapSettingsPojo.setUCL("");
	heatMapSettingsPojo.setGoal("");
	heatMapSettingsPojo.setCategory("Defects");
	heatMapSettingsPojo.setId(index);
	index += 1;

	heatMapSettingsPojoArray.add(heatMapSettingsPojo);


}


			//}

			Gson gson = new Gson();
			response = gson.toJson(heatMapSettingsPojoArray);
			response = "{\"MetricsSettings\":" + response + "}";
			System.out.println(response);
			return response;

		} catch (Exception e) {

		}
		return "{\"MetricsSettings\":[] }";
	}



	@PostMapping(value = "/projects/{id}/monthlyMetricsHeatMapSettings")
	@ResponseBody
	public String updateMonthlyMetricsHeatMapSettings(@PathVariable("id") long id, @RequestBody List<MonthlyMetricsHeatMapSettingsPojo> heatMapSettingsPojoArray) {

		try {

			Iterable<MonthlyMetricsHeatMapSettings> heatMapSettingsIterable = heatMapSettingsRepository.findAll();
			Iterator<MonthlyMetricsHeatMapSettings> heatMapSettingsIterator = heatMapSettingsIterable.iterator();

			while (heatMapSettingsIterator.hasNext()) {
				MonthlyMetricsHeatMapSettings next = heatMapSettingsIterator.next();

				if (next.getProject().getId() == id) {

					try {
						heatMapSettingsRepository.delete(next);
					} catch (Exception e) {

					}
				}
			}

				for (int i = 0; i < heatMapSettingsPojoArray.size(); i++) {
					MonthlyMetricsHeatMapSettings next = new MonthlyMetricsHeatMapSettings();

					System.out.println(heatMapSettingsPojoArray.get(i).getMetricName());
					System.out.println(heatMapSettingsPojoArray.get(i).getLCL());
					System.out.println(heatMapSettingsPojoArray.get(i).getUCL());
					System.out.println(heatMapSettingsPojoArray.get(i).getGoal());
					System.out.println(heatMapSettingsPojoArray.get(i).getCategory());

					boolean matchFound = false;

					if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(MonthlyMetrics.getCostVarianceName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Variance");
						matchFound = true;
					}
					else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(MonthlyMetrics.getCostPerDefectName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Cost");
						matchFound = true;
					}
					else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(MonthlyMetrics.getCostPerTestCaseName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Cost");
						matchFound = true;
					}
					else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(MonthlyMetrics.getEffortVarianceName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Variance");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(MonthlyMetrics.getScopeVarianceName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Variance");
						matchFound = true;
					}  else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(MonthlyMetrics.getScheduleVarianceName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Variance");
						matchFound = true;
					}  else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(MonthlyMetrics.getDefectLeakageSITToUATName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Defects");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(MonthlyMetrics.getDefectLeakageToProductionName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Defects");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(MonthlyMetrics.getSITDefectDensityName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Defects");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(MonthlyMetrics.getUATDefectDensityName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Defects");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(MonthlyMetrics.getApiAutomationPercentageExtentToPotentialName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Potential");
						matchFound = true;
					} else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(MonthlyMetrics.getInProjectAutomationPercentageExtentToPotentialName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Potential");
						matchFound = true;
					}
					else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(MonthlyMetrics.getPercentageOfTCsIdentifiedForMigrationName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Coverage");
						matchFound = true;
					}
					else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(MonthlyMetrics.getManualEffectivenessName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Defects");
						matchFound = true;
					}
					else if (heatMapSettingsPojoArray.get(i).getMetricName().compareTo(MonthlyMetrics.getAutomationEffectivenessName()) == 0) {
						next.setMetricName(heatMapSettingsPojoArray.get(i).getMetricName());
						next.setLCL(heatMapSettingsPojoArray.get(i).getLCL());
						next.setUCL(heatMapSettingsPojoArray.get(i).getUCL());
						next.setGoal(heatMapSettingsPojoArray.get(i).getGoal());
						next.setCategory("Defects");
						matchFound = true;
					}

					if(matchFound == true) {

						Iterable<Project> allProjects = projectRepository.findAll();
						Iterator<Project> iterator = allProjects.iterator();

						while (iterator.hasNext()) {
							Project nextProject = iterator.next();

							if (nextProject.getId() == id) {
								next.setProject(nextProject);
								break;
							}
						}

						heatMapSettingsRepository.save(next);
					}
				}

				return "OK";

		}catch (Exception e) {
		}
		return "Error";
	}

}