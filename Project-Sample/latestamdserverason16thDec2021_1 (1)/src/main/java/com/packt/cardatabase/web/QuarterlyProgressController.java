package com.packt.cardatabase.web;

import com.google.gson.Gson;
import com.packt.cardatabase.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;


@RestController
public class QuarterlyProgressController {

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private QuarterlyProgressRepository quarterlyProgressRepository;


	@RequestMapping(value="/projects/{id}/governance/quarterlyProgress/quarter/{quarter}/year/{year}", method=RequestMethod.GET)
	@ResponseBody
	public String getQuarterlyProgress(@PathVariable("id") long id, @PathVariable(value="quarter") String quarterDetail, @PathVariable(value="year") int year) {

		try {

			ArrayList<ProjectProgressPojo> progressPojoArray = new ArrayList<ProjectProgressPojo>();

			int quarter = 0;

			if(quarterDetail.contains("Q") || quarterDetail.contains("q")) {

				if (quarterDetail.contains("1")) {
					quarter = 1;
				}
				else if (quarterDetail.contains("2")) {
					quarter = 2;
				}
				else if (quarterDetail.contains("3")) {
					quarter = 3;
				}
				else if (quarterDetail.contains("4")) {
					quarter = 4;
				}
				else
				{
					quarter = 5;
				}
			}
			else{
				quarter = Integer.parseInt(quarterDetail);
			}

			int yearNumber = year;


			Iterable<QuarterlyProgress> QuarterlyProgress = quarterlyProgressRepository.findAll();
			Iterator<QuarterlyProgress> QuarterlyProgressIterator = QuarterlyProgress.iterator();
			String response = null;
			boolean recordFound = false;

			while (QuarterlyProgressIterator.hasNext()) {
				QuarterlyProgress next = QuarterlyProgressIterator.next();

				if ((next.getQuarterNumber() == quarter) && (next.getProject().getId() == id) && (next.getYearNumber() == yearNumber)) {
					recordFound = true;

					int index = 1;
					ProjectProgressPojo projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setCategory("Quarterly Progress");projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalQuarterlyBusinessReviewMeetingsName());
					try{ projectProgressPojo.setValue(next.getTotalQuarterlyBusinessReviewMeetings().toString());}catch(Exception e){projectProgressPojo.setValue("0");} 
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setCategory("Quarterly Progress");projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getProjectedBillingForCurrentQuarterName());
					try{ projectProgressPojo.setValue(next.getProjectedBillingForCurrentQuarter().toString());}catch(Exception e){projectProgressPojo.setValue("0");} 
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setCategory("Quarterly Progress");projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getActualBillingForCurrentQuarterName());
					try{ projectProgressPojo.setValue(next.getActualBillingForCurrentQuarter().toString());}catch(Exception e){projectProgressPojo.setValue("0");} 
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setCategory("Quarterly Progress");projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfCRsRaisedName());
					try{ projectProgressPojo.setValue(next.getTotalNumberOfCRsRaised().toString());}catch(Exception e){projectProgressPojo.setValue("0");} 
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setCategory("Quarterly Progress");projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfCRsAcceptedName());
					try{ projectProgressPojo.setValue(next.getTotalNumberOfCRsAccepted().toString());}catch(Exception e){projectProgressPojo.setValue("0");} 
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);projectProgressPojo.setCategory("Quarterly Progress");projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfCRsInvoicedName());
					try{ projectProgressPojo.setValue(next.getTotalNumberOfCRsInvoiced().toString());}catch(Exception e){projectProgressPojo.setValue("0");} 
					progressPojoArray.add(projectProgressPojo);

					Gson gson = new Gson();
					response = gson.toJson(progressPojoArray);

					System.out.println(response);
					response = "{\"progress\":" + response + "}";
					return response;
				}
			}

			if(recordFound == false) {
				QuarterlyProgress next = new QuarterlyProgress();
				response = null;

				int index = 1;
				ProjectProgressPojo projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);projectProgressPojo.setCategory("Quarterly Progress");projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
				projectProgressPojo.setName(next.getProjectedBillingForCurrentQuarterName());
				projectProgressPojo.setValue("0");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);projectProgressPojo.setCategory("Quarterly Progress");projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
				projectProgressPojo.setName(next.getActualBillingForCurrentQuarterName());
				projectProgressPojo.setValue("0");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);projectProgressPojo.setCategory("Quarterly Progress");projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
				projectProgressPojo.setName(next.getTotalNumberOfCRsRaisedName());
				projectProgressPojo.setValue("0");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);projectProgressPojo.setCategory("Quarterly Progress");projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
				projectProgressPojo.setName(next.getTotalNumberOfCRsAcceptedName());
				projectProgressPojo.setValue("0");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);projectProgressPojo.setCategory("Quarterly Progress");projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
				projectProgressPojo.setName(next.getTotalNumberOfCRsInvoicedName());
				projectProgressPojo.setValue("0");
				progressPojoArray.add(projectProgressPojo);

				index = index + 1;
				projectProgressPojo = new ProjectProgressPojo();
				projectProgressPojo.setId(index);projectProgressPojo.setCategory("Quarterly Progress");projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
				projectProgressPojo.setName(next.getTotalQuarterlyBusinessReviewMeetingsName());
				projectProgressPojo.setValue("0");
				progressPojoArray.add(projectProgressPojo);

				Gson gson = new Gson();
				response = gson.toJson(progressPojoArray);

				System.out.println(response);
				response = "{\"progress\":" + response + "}";
				return response;
			}
		}catch(Exception e){

		}
		return "{\"progress\":[]\"}";
	}

	@RequestMapping(value="/projects/{id}/governance/totalQuarterlyProgress", method=RequestMethod.GET)
	@ResponseBody
	public String getTotalQuarterlyProgress(@PathVariable("id") long id) {

		try {

			ArrayList<ArrayList<ProjectProgressPojo>> quarterlyProgressArray = new ArrayList<ArrayList<ProjectProgressPojo>>();

			Iterable<QuarterlyProgress> QuarterlyProgress = quarterlyProgressRepository.findAll();
			Iterator<QuarterlyProgress> QuarterlyProgressIterator = QuarterlyProgress.iterator();
			String response = null;

			while (QuarterlyProgressIterator.hasNext()) {
				QuarterlyProgress next = QuarterlyProgressIterator.next();

				if (next.getProject().getId() == id) {
					ArrayList<ProjectProgressPojo> progressPojoArray = new ArrayList<ProjectProgressPojo>();

					int index = 0;

					ProjectProgressPojo projectProgressPojo = new ProjectProgressPojo();
					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("Index");
					projectProgressPojo.setName(next.getQuarterNumberName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getQuarterNumber()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setType("Index");
					projectProgressPojo.setName(next.getYearNumberName());
					try {
						projectProgressPojo.setValue(Integer.toString(next.getYearNumber()));
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					progressPojoArray.add(projectProgressPojo);

					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setCategory("QuarterlyProgress");
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalQuarterlyBusinessReviewMeetingsName());
					try {
						projectProgressPojo.setValue(next.getTotalQuarterlyBusinessReviewMeetings().toString());
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setCategory("QuarterlyProgress");
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getProjectedBillingForCurrentQuarterName());
					try {
						projectProgressPojo.setValue(next.getProjectedBillingForCurrentQuarter().toString());
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setCategory("QuarterlyProgress");
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getActualBillingForCurrentQuarterName());
					try {
						projectProgressPojo.setValue(next.getActualBillingForCurrentQuarter().toString());
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setCategory("QuarterlyProgress");
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfCRsRaisedName());
					try {
						projectProgressPojo.setValue(next.getTotalNumberOfCRsRaised().toString());
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setCategory("QuarterlyProgress");
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfCRsAcceptedName());
					try {
						projectProgressPojo.setValue(next.getTotalNumberOfCRsAccepted().toString());
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					progressPojoArray.add(projectProgressPojo);

					index = index + 1;
					projectProgressPojo = new ProjectProgressPojo();
					projectProgressPojo.setId(index);
					projectProgressPojo.setCategory("QuarterlyProgress");
					projectProgressPojo.setType("number");projectProgressPojo.setSource("Manual");
					projectProgressPojo.setName(next.getTotalNumberOfCRsInvoicedName());
					try {
						projectProgressPojo.setValue(next.getTotalNumberOfCRsInvoiced().toString());
					} catch (Exception e) {
						projectProgressPojo.setValue("0");
					}
					progressPojoArray.add(projectProgressPojo);

					quarterlyProgressArray.add(progressPojoArray);

				}
			}

					Gson gson = new Gson();
					response = gson.toJson(quarterlyProgressArray);

					System.out.println(response);
					response = "{\"progress\":" + response + "}";
					return response;



		}catch(Exception e){

		}
		return "{\"progress\":[]\"}";
	}

	@PostMapping(value = "/projects/{id}/governance/quarterlyProgress/quarter/{quarter}/year/{year}")
	@ResponseBody
	public String updateQuarterlyProgress(@PathVariable("id") long id, @PathVariable(value="quarter") String quarterDetail, @PathVariable(value="year") String year, @RequestBody List<ProjectProgressPojo> progressPojoArray) {

		try {

			int quarter = 0;

			if(quarterDetail.contains("Q") || quarterDetail.contains("q")) {

				if (quarterDetail.contains("1")) {
					quarter = 1;
				}
				else if (quarterDetail.contains("2")) {
					quarter = 2;
				}
				else if (quarterDetail.contains("3")) {
					quarter = 3;
				}
				else if (quarterDetail.contains("4")) {
					quarter = 4;
				}
				else
				{
					quarter = 5;
				}
			}
			else{
				quarter = Integer.parseInt(quarterDetail);
			}

			int yearNumber = Integer.parseInt(year);


			Iterable<QuarterlyProgress> quarterlyExecutionTestExecutionProgress = quarterlyProgressRepository.findAll();
			Iterator<QuarterlyProgress> quarterlyExecutionTestExecutionProgressIterator = quarterlyExecutionTestExecutionProgress.iterator();

			boolean matchFound = false;

			while (quarterlyExecutionTestExecutionProgressIterator.hasNext()) {
				QuarterlyProgress next = quarterlyExecutionTestExecutionProgressIterator.next();

				if ((next.getQuarterNumber() == quarter) && (next.getProject().getId() == id) && (next.getYearNumber() == yearNumber) ) {
					matchFound = true;
					for (int i = 0; i < progressPojoArray.size(); i++) {

						if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalQuarterlyBusinessReviewMeetingsName())) {

							String previousEntry = next.getTotalQuarterlyBusinessReviewMeetings();
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if(currentUpdatesEntry != previousEntry) {
								next.setTotalQuarterlyBusinessReviewMeetings(currentUpdatesEntry);
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getProjectedBillingForCurrentQuarterName())) {

							String previousEntry = next.getProjectedBillingForCurrentQuarter();
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if(currentUpdatesEntry != previousEntry) {
								next.setProjectedBillingForCurrentQuarter(currentUpdatesEntry);
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getActualBillingForCurrentQuarterName())) {

							String previousEntry = next.getActualBillingForCurrentQuarter();
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if(currentUpdatesEntry != previousEntry) {
								next.setActualBillingForCurrentQuarter(currentUpdatesEntry);
							}

						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfCRsRaisedName())) {

							String previousEntry = next.getTotalNumberOfCRsRaised();
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if(currentUpdatesEntry != previousEntry) {
								next.setTotalNumberOfCRsRaised(currentUpdatesEntry);
								}


						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfCRsAcceptedName())) {

							String previousEntry = next.getTotalNumberOfCRsAccepted();
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();

							if(currentUpdatesEntry != previousEntry) {
								next.setTotalNumberOfCRsAccepted(currentUpdatesEntry);
								}


						} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(next.getTotalNumberOfCRsInvoicedName())) {

							String previousEntry = next.getTotalNumberOfCRsInvoiced();
							String currentUpdatesEntry = progressPojoArray.get(i).getValue();


							if(currentUpdatesEntry != previousEntry) {
								next.setTotalNumberOfCRsInvoiced(currentUpdatesEntry);
								}
						}
					}

					next.setQuarterNumber(quarter);
					quarterlyProgressRepository.save(next);
					break;
				}
			}

			if (matchFound == true)
				return "OK";
			else {

				QuarterlyProgress progress = null;


				Iterable<Project> allProjects = projectRepository.findAll();
				Iterator<Project> iterator = allProjects.iterator();

				while (iterator.hasNext()) {
					Project nextProject = iterator.next();
					if (nextProject.getId() == id) {
						progress = new QuarterlyProgress("0","0","0","0","0","0",quarter,yearNumber,nextProject);
						break;
					}
				}

				for (int i = 0; i < progressPojoArray.size(); i++) {

					if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getTotalQuarterlyBusinessReviewMeetingsName())) {

						String previousEntry = "";
						String currentUpdatesEntry = progressPojoArray.get(i).getValue();

						if(currentUpdatesEntry != previousEntry) {
							progress.setTotalQuarterlyBusinessReviewMeetings(currentUpdatesEntry);
							}

					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getProjectedBillingForCurrentQuarterName())) {

						String previousEntry = "";
						String currentUpdatesEntry = progressPojoArray.get(i).getValue();

						if(currentUpdatesEntry != previousEntry) {
							progress.setProjectedBillingForCurrentQuarter(currentUpdatesEntry);
							}

					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getActualBillingForCurrentQuarterName())) {

						String previousEntry = "";
						String currentUpdatesEntry = progressPojoArray.get(i).getValue();

						if(currentUpdatesEntry != previousEntry) {
							progress.setActualBillingForCurrentQuarter(currentUpdatesEntry);
						}


					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getTotalNumberOfCRsRaisedName())) {

						String previousEntry = "";
						String currentUpdatesEntry = progressPojoArray.get(i).getValue();


						if(currentUpdatesEntry != previousEntry) {
							progress.setTotalNumberOfCRsRaised(currentUpdatesEntry);
							}

					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getTotalNumberOfCRsAcceptedName())) {

						String previousEntry = "";
						String currentUpdatesEntry = progressPojoArray.get(i).getValue();

						if(currentUpdatesEntry != previousEntry) {
							progress.setTotalNumberOfCRsAccepted(currentUpdatesEntry);
							}

					} else if (progressPojoArray.get(i).getName().equalsIgnoreCase(progress.getTotalNumberOfCRsInvoicedName())) {

						String previousEntry = "";
						String currentUpdatesEntry = progressPojoArray.get(i).getValue();

						if(currentUpdatesEntry != previousEntry) {
							progress.setTotalNumberOfCRsInvoiced(currentUpdatesEntry);
							}
					}
				}


				quarterlyProgressRepository.save(progress);
				return "OK";

			}
		} catch (Exception e) {
		}
		return "Error";
	}
}