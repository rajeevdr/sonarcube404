package com.packt.cardatabase.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Project {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private String name; // Free Text
    private String type; // Fix Bid Transformation, MTM Transformation, MTM BAU, T&M Staffing Transformation, T&M BAU
    private String cluster; // Free Text
    private String domain; // Retail Banking, Corporate Banking, Wealth MGMT, Telecom, Others
    private String deliveryManager; // Free Text
    private String technology; // Free text for e.g. T24, CRM, finnacle
    private String category; // Simple, Medium, Complex
    private String lastUpdateOn; // dd-mm-yyyy auto generated
    private String startDate;// dd-mm-yyyy (Cannot be changed after the first time)
    private String endDate;// dd-mm-yyyy


    @OneToMany(cascade = CascadeType.ALL, mappedBy="project")
    @JsonIgnore
    private List<Module> modules;

    @OneToMany(cascade = CascadeType.ALL, mappedBy="project")
    @JsonIgnore
    private List<ModuleEstimate> moduleEstimates;


    @OneToMany(cascade = CascadeType.ALL, mappedBy="project")
    @JsonIgnore
    private List<LatestModuleEstimate> latestModuleEstimates;


    @OneToMany(cascade = CascadeType.ALL, mappedBy="project")
    @JsonIgnore
    private List<ProjectEstimate> projectEstimates;

    @OneToMany(cascade = CascadeType.ALL, mappedBy="project")
    @JsonIgnore
    private List<LatestProjectEstimate> latestProjectEstimates;

    @OneToMany(cascade = CascadeType.ALL, mappedBy="project")
    @JsonIgnore
    private List<QuarterlyProgress> quarterlyProgress;

    @OneToMany(cascade = CascadeType.ALL, mappedBy="project")
    @JsonIgnore
    private List<MonthlyTestDesignProgress> monthlyTestDesignProgress;

    @OneToMany(cascade = CascadeType.ALL, mappedBy="project")
    @JsonIgnore
    private List<WeeklyProjectLevelTestDesignProgress> weeklyProjectLevelTestDesignProgress;

    @OneToMany(cascade = CascadeType.ALL, mappedBy="project")
    @JsonIgnore
    private List<WeeklyModuleLevelTestDesignProgress> weeklyModuleLevelTestDesignProgress;


    @OneToMany(cascade = CascadeType.ALL, mappedBy="project")
    @JsonIgnore
    private List<WeeklyProjectLevelTestExecutionProgress> weeklyProjectLevelTestExecutionProgress;

    @OneToMany(cascade = CascadeType.ALL, mappedBy="project")
    @JsonIgnore
    private List<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgress;

    @OneToMany(cascade = CascadeType.ALL, mappedBy="project")
    @JsonIgnore
    private List<MonthlyTestExecutionProgress> monthlyTestExecutionProgress;


    public void setQuarterlyProgress(List<QuarterlyProgress> quarterlyProgress) {
        this.quarterlyProgress = this.quarterlyProgress;
    }


    public List<MonthlyTestExecutionProgress> getMonthlyTestExecutionProgress() {
        return monthlyTestExecutionProgress;
    }

    public void setMonthlyTestExecutionProgress(List<MonthlyTestExecutionProgress> monthlyTestExecutionProgress) {
        this.monthlyTestExecutionProgress = this.monthlyTestExecutionProgress;
    }



    public List<WeeklyModuleLevelTestExecutionProgress> getWeeklyModuleTestExecutionProgress() {
        return weeklyModuleLevelTestExecutionProgress;
    }

    public void setWeeklyModuleTestExecutionProgress(List<WeeklyModuleLevelTestExecutionProgress> weeklyModuleLevelTestExecutionProgress) {
        this.weeklyModuleLevelTestExecutionProgress = this.weeklyModuleLevelTestExecutionProgress;
    }

    public List<WeeklyProjectLevelTestExecutionProgress> getWeeklyProjectLevelTestExecutionProgress() {
        return weeklyProjectLevelTestExecutionProgress;
    }


    public void setWeeklyProjectTestExecutionProgress(List<WeeklyProjectLevelTestExecutionProgress> weeklyProjectLevelTestExecutionProgress) {
        this.weeklyProjectLevelTestExecutionProgress = this.weeklyProjectLevelTestExecutionProgress;
    }


    public List<WeeklyModuleLevelTestDesignProgress> getWeeklyModuleLevelTestDesignProgress() {
        return weeklyModuleLevelTestDesignProgress;
    }

    public void get(List<WeeklyModuleLevelTestDesignProgress> setWeeklyModuleLevelTestDesignProgress) {
        this.weeklyModuleLevelTestDesignProgress = this.weeklyModuleLevelTestDesignProgress;
    }

    public List<WeeklyProjectLevelTestDesignProgress> getWeeklyProjectLevelTestDesignProgress() {
        return weeklyProjectLevelTestDesignProgress;
    }

    public void setWeeklyProjectLevelTestDesignProgress(List<WeeklyProjectLevelTestDesignProgress> weeklyProjectLevelTestDesignProgress) {
        this.weeklyProjectLevelTestDesignProgress = this.weeklyProjectLevelTestDesignProgress;
    }

    public List<MonthlyTestDesignProgress> getMonthlyTestDesignProgress() {
        return monthlyTestDesignProgress;
    }

    public void setMonthlyTestDesignProgress(List<MonthlyTestDesignProgress> monthlyTestDesignProgress) {
        this.monthlyTestDesignProgress = monthlyTestDesignProgress;
    }

    public List<LatestProjectEstimate> getLatestProjectEstimates() {
        return latestProjectEstimates;
    }

    public void setLatestProjectEstimates(List<LatestProjectEstimate> latestProjectEstimates) {
        this.latestProjectEstimates = latestProjectEstimates;
    }


    public List<ProjectEstimate> getProjectEstimates() {
        return projectEstimates;
    }

    public void setProjectEstimates(List<ProjectEstimate> projectEstimates) {
        this.projectEstimates = projectEstimates;
    }

    public List<Module> getModules() {
        return modules;
    }

    public void setModules(List<Module> modules) {
        this.modules = modules;
    }

    public List<ModuleEstimate> getModuleEstimates() {
        return moduleEstimates;
    }

    public void setModuleEstimates(List<ModuleEstimate> moduleEstimates) {
        this.moduleEstimates = moduleEstimates;
    }

    public List<LatestModuleEstimate> getLatestModuleEstimates() {
        return latestModuleEstimates;
    }

    public void setLatestModuleEstimates(List<LatestModuleEstimate> latestModuleEstimates) {
        this.latestModuleEstimates = latestModuleEstimates;
    }




    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Project() {}

    public Project(String name, String type, String cluster, String domain, String deliveryManager, String technology, String category, String lastUpdateOn, String startDate, String endDate) {
        super();
        this.name = name;
        this.type = type;
        this.cluster = cluster;
        this.domain = domain;
        this.deliveryManager = deliveryManager;
        this.technology = technology;
        this.category = category;
        this.lastUpdateOn = lastUpdateOn;
        this.startDate = startDate;
        this.endDate = endDate;
    }



    public String getName() {
        return name;
    }

    public void setName(String Name) {
        this.name = Name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCluster() {
        return cluster;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getDeliveryManager() {
        return deliveryManager;
    }

    public void setDeliveryManager(String deliveryManager) {
        this.deliveryManager = deliveryManager;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLastUpdateOn() {
        return lastUpdateOn;
    }

    public void setLastUpdateOn(String lastUpdateOn) {
        this.lastUpdateOn = lastUpdateOn;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

}