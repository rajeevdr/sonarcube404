package com.packt.cardatabase.domain;

import javax.persistence.*;
import java.util.ArrayList;

@Entity
public class RequirementsProjectFoldersEntity {
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "project")
	private Project project;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}


	private String projectFieldName;
	private ArrayList<String> projectName = null;

	public String getProjectFieldName() {
		return projectFieldName;
	}

	public void setProjectFieldName(String projectFieldName) {
		this.projectFieldName = projectFieldName;
	}

	public ArrayList<String> getProjectName() {
		return projectName;
	}

	public void setProjectName(ArrayList<String> projectName) {
		this.projectName = projectName;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}


}