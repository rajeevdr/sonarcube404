package com.packt.cardatabase.domain;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.StringTokenizer;

public class MetricsServerTestAutomation {

	String projectId = null;


	@BeforeTest
	    public void setUp() {
	        RestAssured.baseURI = "http://localhost:8080/projects";
	        }

	@Test(priority=1)
		public void createProject()  {

			String createProjectPayload = "      {\n" +
					"         \"name\":\"first Project\",\n" +
					"         \"type\":\"first Project\",\n" +
					"         \"cluster\":\"first Project\",\n" +
					"         \"domain\":\"first Project\",\n" +
					"         \"deliveryManager\":\"first Project\",\n" +
					"         \"technology\":\"first Project\",\n" +
					"         \"category\":\"first Project\",\n" +
					"         \"startDate\":\"01-05-2021\",\n" +
					"         \"endDate\":\"01-05-2022\"\n" +
					"      }";

			
			RequestSpecification request = RestAssured.given();
			Response response = request
			.when()
			.contentType("application/json")
	       .with()
	       .body(createProjectPayload)
	       .post();
		

		StringTokenizer tokenizer = new StringTokenizer(response.body().prettyPrint(), ",");

		for(;;) {
			if (tokenizer.hasMoreTokens()) {
				String tempHighLevelToken = tokenizer.nextToken();
				if(tempHighLevelToken.contains("id")) {
					tokenizer = new StringTokenizer(tempHighLevelToken, ":");
					tokenizer.nextToken();
					projectId = tokenizer.nextToken();
					System.out.println("The projectId is : " + projectId);
                    break;
				}
			}
		}

		// Get the project details and assert

	}


	@Test(priority=2)
		public void addModules() {
			RestAssured.baseURI = "http://localhost:8080/projects/" + projectId + "/addModule";
			System.out.println("The add module URL is : " + "http://localhost:8080/" + projectId + "/addModule");

			for (int i = 1; i < 11; i++) {

				try {

					String createModulePayload = "{" +
							"          \"name\":\"Module" + i + "\"," +
							"         \"estimatedManualTestCases\":" + i * 1000 + "," +
							"         \"estimatedRegressionAutomationTestCases\":" + i * 1000 + "" +
							"      }";

					System.out.println("The create module payload is : " + createModulePayload);

					RequestSpecification request = RestAssured.given();
					Response response = request
							.when()
							.contentType("application/json")
							.with()
							.body(createModulePayload)
							.post();

				}catch(Exception e){
				}
			}

		// Get the module details and assert
		// Update modules and assert
	}



	@Test(priority=3)
	public void AddEstimates() {

		RestAssured.baseURI = "http://localhost:8080/projects/" + projectId + "/estimates";
		String addEstimatesUrl =  "http://localhost:8080/projects/" + projectId + "/estimates";

		System.out.println("The add estimates url : " + addEstimatesUrl);

		String form_Param_payload = "[\n" +
				"      {\n" +
				"         \"id\":1,\n" +
				"         \"name\":\"TotalNumberOfRequirementsPlanned\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":2,\n" +
				"         \"name\":\"Module-ModuleName1-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":3,\n" +
				"         \"name\":\"Module-ModuleName1-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":4,\n" +
				"         \"name\":\"Module-ModuleName2-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"2000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":5,\n" +
				"         \"name\":\"Module-ModuleName2-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"2000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":6,\n" +
				"         \"name\":\"Module-ModuleName3-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"3000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":7,\n" +
				"         \"name\":\"Module-ModuleName3-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"3000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":8,\n" +
				"         \"name\":\"Module-ModuleName4-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"4000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":9,\n" +
				"         \"name\":\"Module-ModuleName4-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"4000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":10,\n" +
				"         \"name\":\"Module-ModuleName5-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"5000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":11,\n" +
				"         \"name\":\"Module-ModuleName5-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"5000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":12,\n" +
				"         \"name\":\"Module-ModuleName6-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"6000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":13,\n" +
				"         \"name\":\"Module-ModuleName6-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"6000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":14,\n" +
				"         \"name\":\"Module-ModuleName7-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"7000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":15,\n" +
				"         \"name\":\"Module-ModuleName7-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"7000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":16,\n" +
				"         \"name\":\"Module-ModuleName8-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"8000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":17,\n" +
				"         \"name\":\"Module-ModuleName8-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"8000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":18,\n" +
				"         \"name\":\"Module-ModuleName9-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"9000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":19,\n" +
				"         \"name\":\"Module-ModuleName9-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"9000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":20,\n" +
				"         \"name\":\"Module-ModuleName10-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"10000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":21,\n" +
				"         \"name\":\"Module-ModuleName10-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"10000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":22,\n" +
				"         \"name\":\"APIAutomationTestCaseEstimate\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":23,\n" +
				"         \"name\":\"InProjectAutomationTestCaseEstimate\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":24,\n" +
				"         \"name\":\"TotalNumberOfTestCasesTaggedForMigrationExecution\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":25,\n" +
				"         \"name\":\"TestPlanningEffort\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":26,\n" +
				"         \"name\":\"ManualTestCasesDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":27,\n" +
				"         \"name\":\"RegressionAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":28,\n" +
				"         \"name\":\"InProjectAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":29,\n" +
				"         \"name\":\"APIAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":30,\n" +
				"         \"name\":\"ManualTestCases-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":31,\n" +
				"         \"name\":\"RegressionAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":32,\n" +
				"         \"name\":\"InProjectAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":33,\n" +
				"         \"name\":\"APIAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":34,\n" +
				"         \"name\":\"ManualTestCases-MaintenanceEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":35,\n" +
				"         \"name\":\"AutomationTestCases-MaintenanceEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":36,\n" +
				"         \"name\":\"PlannedResourcesForTestCaseAuthoring\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":37,\n" +
				"         \"name\":\"PlannedResourcesForAutomationDesign\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":38,\n" +
				"         \"name\":\"PlannedResourcesForManualTestCaseExecution\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":39,\n" +
				"         \"name\":\"PlannedResourcesForAutomationExecution\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      }\n" +
				"   ]";


		System.out.println("The request payload is : " + form_Param_payload);

		RequestSpecification request = RestAssured.given();
		Response response = request
				.when()
				.contentType("application/json")
				.with()
				.body(form_Param_payload)
				.post();


		System.out.println(response.statusCode());
		System.out.println(response.body().prettyPrint());

		// Get the estimates and assert

		// Update the estimates and assert

	}


	@Test(priority=4)
	public void WeeklyTestDesignProgress() {

		RestAssured.baseURI = "http://localhost:8080/projects/" + projectId + "/estimates";
		String addEstimatesUrl =  "http://localhost:8080/projects/" + projectId + "/estimates";

		System.out.println("The add estimates url : " + addEstimatesUrl);

		String form_Param_payload = "[\n" +
				"      {\n" +
				"         \"id\":1,\n" +
				"         \"name\":\"TotalNumberOfRequirementsPlanned\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":2,\n" +
				"         \"name\":\"Module-ModuleName1-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":3,\n" +
				"         \"name\":\"Module-ModuleName1-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":4,\n" +
				"         \"name\":\"Module-ModuleName2-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"2000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":5,\n" +
				"         \"name\":\"Module-ModuleName2-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"2000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":6,\n" +
				"         \"name\":\"Module-ModuleName3-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"3000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":7,\n" +
				"         \"name\":\"Module-ModuleName3-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"3000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":8,\n" +
				"         \"name\":\"Module-ModuleName4-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"4000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":9,\n" +
				"         \"name\":\"Module-ModuleName4-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"4000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":10,\n" +
				"         \"name\":\"Module-ModuleName5-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"5000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":11,\n" +
				"         \"name\":\"Module-ModuleName5-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"5000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":12,\n" +
				"         \"name\":\"Module-ModuleName6-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"6000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":13,\n" +
				"         \"name\":\"Module-ModuleName6-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"6000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":14,\n" +
				"         \"name\":\"Module-ModuleName7-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"7000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":15,\n" +
				"         \"name\":\"Module-ModuleName7-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"7000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":16,\n" +
				"         \"name\":\"Module-ModuleName8-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"8000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":17,\n" +
				"         \"name\":\"Module-ModuleName8-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"8000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":18,\n" +
				"         \"name\":\"Module-ModuleName9-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"9000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":19,\n" +
				"         \"name\":\"Module-ModuleName9-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"9000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":20,\n" +
				"         \"name\":\"Module-ModuleName10-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"10000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":21,\n" +
				"         \"name\":\"Module-ModuleName10-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"10000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":22,\n" +
				"         \"name\":\"APIAutomationTestCaseEstimate\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":23,\n" +
				"         \"name\":\"InProjectAutomationTestCaseEstimate\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":24,\n" +
				"         \"name\":\"TotalNumberOfTestCasesTaggedForMigrationExecution\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":25,\n" +
				"         \"name\":\"TestPlanningEffort\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":26,\n" +
				"         \"name\":\"ManualTestCasesDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":27,\n" +
				"         \"name\":\"RegressionAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":28,\n" +
				"         \"name\":\"InProjectAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":29,\n" +
				"         \"name\":\"APIAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":30,\n" +
				"         \"name\":\"ManualTestCases-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":31,\n" +
				"         \"name\":\"RegressionAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":32,\n" +
				"         \"name\":\"InProjectAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":33,\n" +
				"         \"name\":\"APIAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":34,\n" +
				"         \"name\":\"ManualTestCases-MaintenanceEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":35,\n" +
				"         \"name\":\"AutomationTestCases-MaintenanceEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":36,\n" +
				"         \"name\":\"PlannedResourcesForTestCaseAuthoring\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":37,\n" +
				"         \"name\":\"PlannedResourcesForAutomationDesign\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":38,\n" +
				"         \"name\":\"PlannedResourcesForManualTestCaseExecution\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":39,\n" +
				"         \"name\":\"PlannedResourcesForAutomationExecution\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      }\n" +
				"   ]";


		System.out.println("The request payload is : " + form_Param_payload);

		RequestSpecification request = RestAssured.given();
		Response response = request
				.when()
				.contentType("application/json")
				.with()
				.body(form_Param_payload)
				.post();


		System.out.println(response.statusCode());
		System.out.println(response.body().prettyPrint());


		// get the weekly test design progress and assert

		// update the weekly test design progress and assert


	}


	@Test(priority=5)
	public void WeeklyTestExecutionProgress() {

		RestAssured.baseURI = "http://localhost:8080/projects/" + projectId + "/estimates";
		String addEstimatesUrl =  "http://localhost:8080/projects/" + projectId + "/estimates";

		System.out.println("The add estimates url : " + addEstimatesUrl);

		String form_Param_payload = "[\n" +
				"      {\n" +
				"         \"id\":1,\n" +
				"         \"name\":\"TotalNumberOfRequirementsPlanned\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":2,\n" +
				"         \"name\":\"Module-ModuleName1-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":3,\n" +
				"         \"name\":\"Module-ModuleName1-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":4,\n" +
				"         \"name\":\"Module-ModuleName2-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"2000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":5,\n" +
				"         \"name\":\"Module-ModuleName2-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"2000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":6,\n" +
				"         \"name\":\"Module-ModuleName3-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"3000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":7,\n" +
				"         \"name\":\"Module-ModuleName3-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"3000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":8,\n" +
				"         \"name\":\"Module-ModuleName4-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"4000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":9,\n" +
				"         \"name\":\"Module-ModuleName4-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"4000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":10,\n" +
				"         \"name\":\"Module-ModuleName5-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"5000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":11,\n" +
				"         \"name\":\"Module-ModuleName5-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"5000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":12,\n" +
				"         \"name\":\"Module-ModuleName6-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"6000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":13,\n" +
				"         \"name\":\"Module-ModuleName6-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"6000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":14,\n" +
				"         \"name\":\"Module-ModuleName7-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"7000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":15,\n" +
				"         \"name\":\"Module-ModuleName7-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"7000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":16,\n" +
				"         \"name\":\"Module-ModuleName8-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"8000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":17,\n" +
				"         \"name\":\"Module-ModuleName8-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"8000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":18,\n" +
				"         \"name\":\"Module-ModuleName9-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"9000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":19,\n" +
				"         \"name\":\"Module-ModuleName9-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"9000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":20,\n" +
				"         \"name\":\"Module-ModuleName10-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"10000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":21,\n" +
				"         \"name\":\"Module-ModuleName10-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"10000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":22,\n" +
				"         \"name\":\"APIAutomationTestCaseEstimate\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":23,\n" +
				"         \"name\":\"InProjectAutomationTestCaseEstimate\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":24,\n" +
				"         \"name\":\"TotalNumberOfTestCasesTaggedForMigrationExecution\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":25,\n" +
				"         \"name\":\"TestPlanningEffort\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":26,\n" +
				"         \"name\":\"ManualTestCasesDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":27,\n" +
				"         \"name\":\"RegressionAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":28,\n" +
				"         \"name\":\"InProjectAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":29,\n" +
				"         \"name\":\"APIAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":30,\n" +
				"         \"name\":\"ManualTestCases-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":31,\n" +
				"         \"name\":\"RegressionAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":32,\n" +
				"         \"name\":\"InProjectAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":33,\n" +
				"         \"name\":\"APIAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":34,\n" +
				"         \"name\":\"ManualTestCases-MaintenanceEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":35,\n" +
				"         \"name\":\"AutomationTestCases-MaintenanceEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":36,\n" +
				"         \"name\":\"PlannedResourcesForTestCaseAuthoring\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":37,\n" +
				"         \"name\":\"PlannedResourcesForAutomationDesign\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":38,\n" +
				"         \"name\":\"PlannedResourcesForManualTestCaseExecution\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":39,\n" +
				"         \"name\":\"PlannedResourcesForAutomationExecution\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      }\n" +
				"   ]";


		System.out.println("The request payload is : " + form_Param_payload);

		RequestSpecification request = RestAssured.given();
		Response response = request
				.when()
				.contentType("application/json")
				.with()
				.body(form_Param_payload)
				.post();


		System.out.println(response.statusCode());
		System.out.println(response.body().prettyPrint());


		// get the weekly test execution progress and assert

		// update the weekly test execution progress and assert

	}


	@Test(priority=6)
	public void MonthlyTestDesignProgress() {

		RestAssured.baseURI = "http://localhost:8080/projects/" + projectId + "/estimates";
		String addEstimatesUrl =  "http://localhost:8080/projects/" + projectId + "/estimates";

		System.out.println("The add estimates url : " + addEstimatesUrl);

		String form_Param_payload = "[\n" +
				"      {\n" +
				"         \"id\":1,\n" +
				"         \"name\":\"TotalNumberOfRequirementsPlanned\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":2,\n" +
				"         \"name\":\"Module-ModuleName1-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":3,\n" +
				"         \"name\":\"Module-ModuleName1-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":4,\n" +
				"         \"name\":\"Module-ModuleName2-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"2000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":5,\n" +
				"         \"name\":\"Module-ModuleName2-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"2000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":6,\n" +
				"         \"name\":\"Module-ModuleName3-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"3000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":7,\n" +
				"         \"name\":\"Module-ModuleName3-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"3000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":8,\n" +
				"         \"name\":\"Module-ModuleName4-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"4000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":9,\n" +
				"         \"name\":\"Module-ModuleName4-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"4000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":10,\n" +
				"         \"name\":\"Module-ModuleName5-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"5000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":11,\n" +
				"         \"name\":\"Module-ModuleName5-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"5000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":12,\n" +
				"         \"name\":\"Module-ModuleName6-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"6000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":13,\n" +
				"         \"name\":\"Module-ModuleName6-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"6000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":14,\n" +
				"         \"name\":\"Module-ModuleName7-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"7000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":15,\n" +
				"         \"name\":\"Module-ModuleName7-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"7000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":16,\n" +
				"         \"name\":\"Module-ModuleName8-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"8000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":17,\n" +
				"         \"name\":\"Module-ModuleName8-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"8000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":18,\n" +
				"         \"name\":\"Module-ModuleName9-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"9000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":19,\n" +
				"         \"name\":\"Module-ModuleName9-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"9000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":20,\n" +
				"         \"name\":\"Module-ModuleName10-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"10000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":21,\n" +
				"         \"name\":\"Module-ModuleName10-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"10000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":22,\n" +
				"         \"name\":\"APIAutomationTestCaseEstimate\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":23,\n" +
				"         \"name\":\"InProjectAutomationTestCaseEstimate\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":24,\n" +
				"         \"name\":\"TotalNumberOfTestCasesTaggedForMigrationExecution\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":25,\n" +
				"         \"name\":\"TestPlanningEffort\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":26,\n" +
				"         \"name\":\"ManualTestCasesDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":27,\n" +
				"         \"name\":\"RegressionAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":28,\n" +
				"         \"name\":\"InProjectAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":29,\n" +
				"         \"name\":\"APIAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":30,\n" +
				"         \"name\":\"ManualTestCases-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":31,\n" +
				"         \"name\":\"RegressionAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":32,\n" +
				"         \"name\":\"InProjectAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":33,\n" +
				"         \"name\":\"APIAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":34,\n" +
				"         \"name\":\"ManualTestCases-MaintenanceEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":35,\n" +
				"         \"name\":\"AutomationTestCases-MaintenanceEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":36,\n" +
				"         \"name\":\"PlannedResourcesForTestCaseAuthoring\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":37,\n" +
				"         \"name\":\"PlannedResourcesForAutomationDesign\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":38,\n" +
				"         \"name\":\"PlannedResourcesForManualTestCaseExecution\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":39,\n" +
				"         \"name\":\"PlannedResourcesForAutomationExecution\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      }\n" +
				"   ]";


		System.out.println("The request payload is : " + form_Param_payload);

		RequestSpecification request = RestAssured.given();
		Response response = request
				.when()
				.contentType("application/json")
				.with()
				.body(form_Param_payload)
				.post();


		System.out.println(response.statusCode());
		System.out.println(response.body().prettyPrint());

		// get the Monthly test design progress and assert
		// update the Monthly test design progress and assert

	}


	@Test(priority=6)
	public void MonthlyTestExecutionProgress() {

		RestAssured.baseURI = "http://localhost:8080/projects/" + projectId + "/estimates";
		String addEstimatesUrl =  "http://localhost:8080/projects/" + projectId + "/estimates";

		System.out.println("The add estimates url : " + addEstimatesUrl);

		String form_Param_payload = "[\n" +
				"      {\n" +
				"         \"id\":1,\n" +
				"         \"name\":\"TotalNumberOfRequirementsPlanned\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":2,\n" +
				"         \"name\":\"Module-ModuleName1-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":3,\n" +
				"         \"name\":\"Module-ModuleName1-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":4,\n" +
				"         \"name\":\"Module-ModuleName2-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"2000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":5,\n" +
				"         \"name\":\"Module-ModuleName2-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"2000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":6,\n" +
				"         \"name\":\"Module-ModuleName3-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"3000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":7,\n" +
				"         \"name\":\"Module-ModuleName3-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"3000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":8,\n" +
				"         \"name\":\"Module-ModuleName4-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"4000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":9,\n" +
				"         \"name\":\"Module-ModuleName4-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"4000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":10,\n" +
				"         \"name\":\"Module-ModuleName5-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"5000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":11,\n" +
				"         \"name\":\"Module-ModuleName5-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"5000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":12,\n" +
				"         \"name\":\"Module-ModuleName6-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"6000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":13,\n" +
				"         \"name\":\"Module-ModuleName6-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"6000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":14,\n" +
				"         \"name\":\"Module-ModuleName7-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"7000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":15,\n" +
				"         \"name\":\"Module-ModuleName7-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"7000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":16,\n" +
				"         \"name\":\"Module-ModuleName8-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"8000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":17,\n" +
				"         \"name\":\"Module-ModuleName8-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"8000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":18,\n" +
				"         \"name\":\"Module-ModuleName9-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"9000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":19,\n" +
				"         \"name\":\"Module-ModuleName9-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"9000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":20,\n" +
				"         \"name\":\"Module-ModuleName10-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"10000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":21,\n" +
				"         \"name\":\"Module-ModuleName10-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"10000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":22,\n" +
				"         \"name\":\"APIAutomationTestCaseEstimate\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":23,\n" +
				"         \"name\":\"InProjectAutomationTestCaseEstimate\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":24,\n" +
				"         \"name\":\"TotalNumberOfTestCasesTaggedForMigrationExecution\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":25,\n" +
				"         \"name\":\"TestPlanningEffort\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":26,\n" +
				"         \"name\":\"ManualTestCasesDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":27,\n" +
				"         \"name\":\"RegressionAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":28,\n" +
				"         \"name\":\"InProjectAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":29,\n" +
				"         \"name\":\"APIAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":30,\n" +
				"         \"name\":\"ManualTestCases-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":31,\n" +
				"         \"name\":\"RegressionAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":32,\n" +
				"         \"name\":\"InProjectAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":33,\n" +
				"         \"name\":\"APIAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":34,\n" +
				"         \"name\":\"ManualTestCases-MaintenanceEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":35,\n" +
				"         \"name\":\"AutomationTestCases-MaintenanceEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":36,\n" +
				"         \"name\":\"PlannedResourcesForTestCaseAuthoring\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":37,\n" +
				"         \"name\":\"PlannedResourcesForAutomationDesign\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":38,\n" +
				"         \"name\":\"PlannedResourcesForManualTestCaseExecution\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":39,\n" +
				"         \"name\":\"PlannedResourcesForAutomationExecution\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      }\n" +
				"   ]";


		System.out.println("The request payload is : " + form_Param_payload);

		RequestSpecification request = RestAssured.given();
		Response response = request
				.when()
				.contentType("application/json")
				.with()
				.body(form_Param_payload)
				.post();


		System.out.println(response.statusCode());
		System.out.println(response.body().prettyPrint());

		// get the Monthly test design progress and assert
		// update the Monthly test design progress and assert

	}


	@Test(priority=7)
	public void QuarterProgress() {

		RestAssured.baseURI = "http://localhost:8080/projects/" + projectId + "/estimates";
		String addEstimatesUrl =  "http://localhost:8080/projects/" + projectId + "/estimates";

		System.out.println("The add estimates url : " + addEstimatesUrl);

		String form_Param_payload = "[\n" +
				"      {\n" +
				"         \"id\":1,\n" +
				"         \"name\":\"TotalNumberOfRequirementsPlanned\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":2,\n" +
				"         \"name\":\"Module-ModuleName1-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":3,\n" +
				"         \"name\":\"Module-ModuleName1-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":4,\n" +
				"         \"name\":\"Module-ModuleName2-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"2000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":5,\n" +
				"         \"name\":\"Module-ModuleName2-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"2000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":6,\n" +
				"         \"name\":\"Module-ModuleName3-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"3000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":7,\n" +
				"         \"name\":\"Module-ModuleName3-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"3000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":8,\n" +
				"         \"name\":\"Module-ModuleName4-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"4000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":9,\n" +
				"         \"name\":\"Module-ModuleName4-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"4000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":10,\n" +
				"         \"name\":\"Module-ModuleName5-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"5000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":11,\n" +
				"         \"name\":\"Module-ModuleName5-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"5000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":12,\n" +
				"         \"name\":\"Module-ModuleName6-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"6000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":13,\n" +
				"         \"name\":\"Module-ModuleName6-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"6000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":14,\n" +
				"         \"name\":\"Module-ModuleName7-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"7000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":15,\n" +
				"         \"name\":\"Module-ModuleName7-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"7000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":16,\n" +
				"         \"name\":\"Module-ModuleName8-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"8000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":17,\n" +
				"         \"name\":\"Module-ModuleName8-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"8000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":18,\n" +
				"         \"name\":\"Module-ModuleName9-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"9000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":19,\n" +
				"         \"name\":\"Module-ModuleName9-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"9000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":20,\n" +
				"         \"name\":\"Module-ModuleName10-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"10000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":21,\n" +
				"         \"name\":\"Module-ModuleName10-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"10000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":22,\n" +
				"         \"name\":\"APIAutomationTestCaseEstimate\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":23,\n" +
				"         \"name\":\"InProjectAutomationTestCaseEstimate\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":24,\n" +
				"         \"name\":\"TotalNumberOfTestCasesTaggedForMigrationExecution\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":25,\n" +
				"         \"name\":\"TestPlanningEffort\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":26,\n" +
				"         \"name\":\"ManualTestCasesDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":27,\n" +
				"         \"name\":\"RegressionAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":28,\n" +
				"         \"name\":\"InProjectAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":29,\n" +
				"         \"name\":\"APIAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":30,\n" +
				"         \"name\":\"ManualTestCases-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":31,\n" +
				"         \"name\":\"RegressionAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":32,\n" +
				"         \"name\":\"InProjectAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":33,\n" +
				"         \"name\":\"APIAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":34,\n" +
				"         \"name\":\"ManualTestCases-MaintenanceEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":35,\n" +
				"         \"name\":\"AutomationTestCases-MaintenanceEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":36,\n" +
				"         \"name\":\"PlannedResourcesForTestCaseAuthoring\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":37,\n" +
				"         \"name\":\"PlannedResourcesForAutomationDesign\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":38,\n" +
				"         \"name\":\"PlannedResourcesForManualTestCaseExecution\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":39,\n" +
				"         \"name\":\"PlannedResourcesForAutomationExecution\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      }\n" +
				"   ]";


		System.out.println("The request payload is : " + form_Param_payload);

		RequestSpecification request = RestAssured.given();
		Response response = request
				.when()
				.contentType("application/json")
				.with()
				.body(form_Param_payload)
				.post();


		System.out.println(response.statusCode());
		System.out.println(response.body().prettyPrint());

		// get the Quarterly progress and assert
		// update the Quarterly progress and assert

	}


	@Test(priority=8)
	public void WeeklyMetrics() {

		RestAssured.baseURI = "http://localhost:8080/projects/" + projectId + "/estimates";
		String addEstimatesUrl =  "http://localhost:8080/projects/" + projectId + "/estimates";

		System.out.println("The add estimates url : " + addEstimatesUrl);

		String form_Param_payload = "[\n" +
				"      {\n" +
				"         \"id\":1,\n" +
				"         \"name\":\"TotalNumberOfRequirementsPlanned\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":2,\n" +
				"         \"name\":\"Module-ModuleName1-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":3,\n" +
				"         \"name\":\"Module-ModuleName1-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":4,\n" +
				"         \"name\":\"Module-ModuleName2-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"2000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":5,\n" +
				"         \"name\":\"Module-ModuleName2-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"2000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":6,\n" +
				"         \"name\":\"Module-ModuleName3-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"3000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":7,\n" +
				"         \"name\":\"Module-ModuleName3-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"3000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":8,\n" +
				"         \"name\":\"Module-ModuleName4-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"4000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":9,\n" +
				"         \"name\":\"Module-ModuleName4-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"4000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":10,\n" +
				"         \"name\":\"Module-ModuleName5-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"5000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":11,\n" +
				"         \"name\":\"Module-ModuleName5-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"5000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":12,\n" +
				"         \"name\":\"Module-ModuleName6-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"6000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":13,\n" +
				"         \"name\":\"Module-ModuleName6-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"6000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":14,\n" +
				"         \"name\":\"Module-ModuleName7-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"7000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":15,\n" +
				"         \"name\":\"Module-ModuleName7-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"7000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":16,\n" +
				"         \"name\":\"Module-ModuleName8-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"8000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":17,\n" +
				"         \"name\":\"Module-ModuleName8-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"8000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":18,\n" +
				"         \"name\":\"Module-ModuleName9-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"9000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":19,\n" +
				"         \"name\":\"Module-ModuleName9-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"9000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":20,\n" +
				"         \"name\":\"Module-ModuleName10-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"10000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":21,\n" +
				"         \"name\":\"Module-ModuleName10-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"10000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":22,\n" +
				"         \"name\":\"APIAutomationTestCaseEstimate\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":23,\n" +
				"         \"name\":\"InProjectAutomationTestCaseEstimate\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":24,\n" +
				"         \"name\":\"TotalNumberOfTestCasesTaggedForMigrationExecution\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":25,\n" +
				"         \"name\":\"TestPlanningEffort\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":26,\n" +
				"         \"name\":\"ManualTestCasesDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":27,\n" +
				"         \"name\":\"RegressionAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":28,\n" +
				"         \"name\":\"InProjectAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":29,\n" +
				"         \"name\":\"APIAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":30,\n" +
				"         \"name\":\"ManualTestCases-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":31,\n" +
				"         \"name\":\"RegressionAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":32,\n" +
				"         \"name\":\"InProjectAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":33,\n" +
				"         \"name\":\"APIAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":34,\n" +
				"         \"name\":\"ManualTestCases-MaintenanceEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":35,\n" +
				"         \"name\":\"AutomationTestCases-MaintenanceEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":36,\n" +
				"         \"name\":\"PlannedResourcesForTestCaseAuthoring\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":37,\n" +
				"         \"name\":\"PlannedResourcesForAutomationDesign\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":38,\n" +
				"         \"name\":\"PlannedResourcesForManualTestCaseExecution\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":39,\n" +
				"         \"name\":\"PlannedResourcesForAutomationExecution\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      }\n" +
				"   ]";


		System.out.println("The request payload is : " + form_Param_payload);

		RequestSpecification request = RestAssured.given();
		Response response = request
				.when()
				.contentType("application/json")
				.with()
				.body(form_Param_payload)
				.post();


		System.out.println(response.statusCode());
		System.out.println(response.body().prettyPrint());

		// Generate weekly metrics and verify

	}


	@Test(priority=9)
	public void monthlyMetrics() {

		RestAssured.baseURI = "http://localhost:8080/projects/" + projectId + "/estimates";
		String addEstimatesUrl =  "http://localhost:8080/projects/" + projectId + "/estimates";

		System.out.println("The add estimates url : " + addEstimatesUrl);

		String form_Param_payload = "[\n" +
				"      {\n" +
				"         \"id\":1,\n" +
				"         \"name\":\"TotalNumberOfRequirementsPlanned\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":2,\n" +
				"         \"name\":\"Module-ModuleName1-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":3,\n" +
				"         \"name\":\"Module-ModuleName1-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":4,\n" +
				"         \"name\":\"Module-ModuleName2-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"2000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":5,\n" +
				"         \"name\":\"Module-ModuleName2-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"2000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":6,\n" +
				"         \"name\":\"Module-ModuleName3-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"3000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":7,\n" +
				"         \"name\":\"Module-ModuleName3-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"3000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":8,\n" +
				"         \"name\":\"Module-ModuleName4-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"4000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":9,\n" +
				"         \"name\":\"Module-ModuleName4-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"4000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":10,\n" +
				"         \"name\":\"Module-ModuleName5-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"5000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":11,\n" +
				"         \"name\":\"Module-ModuleName5-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"5000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":12,\n" +
				"         \"name\":\"Module-ModuleName6-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"6000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":13,\n" +
				"         \"name\":\"Module-ModuleName6-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"6000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":14,\n" +
				"         \"name\":\"Module-ModuleName7-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"7000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":15,\n" +
				"         \"name\":\"Module-ModuleName7-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"7000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":16,\n" +
				"         \"name\":\"Module-ModuleName8-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"8000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":17,\n" +
				"         \"name\":\"Module-ModuleName8-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"8000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":18,\n" +
				"         \"name\":\"Module-ModuleName9-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"9000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":19,\n" +
				"         \"name\":\"Module-ModuleName9-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"9000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":20,\n" +
				"         \"name\":\"Module-ModuleName10-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"10000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":21,\n" +
				"         \"name\":\"Module-ModuleName10-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"10000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":22,\n" +
				"         \"name\":\"APIAutomationTestCaseEstimate\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":23,\n" +
				"         \"name\":\"InProjectAutomationTestCaseEstimate\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":24,\n" +
				"         \"name\":\"TotalNumberOfTestCasesTaggedForMigrationExecution\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":25,\n" +
				"         \"name\":\"TestPlanningEffort\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":26,\n" +
				"         \"name\":\"ManualTestCasesDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":27,\n" +
				"         \"name\":\"RegressionAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":28,\n" +
				"         \"name\":\"InProjectAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":29,\n" +
				"         \"name\":\"APIAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":30,\n" +
				"         \"name\":\"ManualTestCases-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":31,\n" +
				"         \"name\":\"RegressionAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":32,\n" +
				"         \"name\":\"InProjectAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":33,\n" +
				"         \"name\":\"APIAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":34,\n" +
				"         \"name\":\"ManualTestCases-MaintenanceEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":35,\n" +
				"         \"name\":\"AutomationTestCases-MaintenanceEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":36,\n" +
				"         \"name\":\"PlannedResourcesForTestCaseAuthoring\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":37,\n" +
				"         \"name\":\"PlannedResourcesForAutomationDesign\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":38,\n" +
				"         \"name\":\"PlannedResourcesForManualTestCaseExecution\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":39,\n" +
				"         \"name\":\"PlannedResourcesForAutomationExecution\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      }\n" +
				"   ]";


		System.out.println("The request payload is : " + form_Param_payload);

		RequestSpecification request = RestAssured.given();
		Response response = request
				.when()
				.contentType("application/json")
				.with()
				.body(form_Param_payload)
				.post();


		System.out.println(response.statusCode());
		System.out.println(response.body().prettyPrint());

		// Generate Monthly metrics and verify

	}

	@Test(priority=10)
	public void quarterlyMetrics() {

		RestAssured.baseURI = "http://localhost:8080/projects/" + projectId + "/estimates";
		String addEstimatesUrl =  "http://localhost:8080/projects/" + projectId + "/estimates";

		System.out.println("The add estimates url : " + addEstimatesUrl);

		String form_Param_payload = "[\n" +
				"      {\n" +
				"         \"id\":1,\n" +
				"         \"name\":\"TotalNumberOfRequirementsPlanned\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":2,\n" +
				"         \"name\":\"Module-ModuleName1-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":3,\n" +
				"         \"name\":\"Module-ModuleName1-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":4,\n" +
				"         \"name\":\"Module-ModuleName2-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"2000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":5,\n" +
				"         \"name\":\"Module-ModuleName2-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"2000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":6,\n" +
				"         \"name\":\"Module-ModuleName3-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"3000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":7,\n" +
				"         \"name\":\"Module-ModuleName3-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"3000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":8,\n" +
				"         \"name\":\"Module-ModuleName4-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"4000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":9,\n" +
				"         \"name\":\"Module-ModuleName4-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"4000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":10,\n" +
				"         \"name\":\"Module-ModuleName5-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"5000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":11,\n" +
				"         \"name\":\"Module-ModuleName5-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"5000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":12,\n" +
				"         \"name\":\"Module-ModuleName6-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"6000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":13,\n" +
				"         \"name\":\"Module-ModuleName6-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"6000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":14,\n" +
				"         \"name\":\"Module-ModuleName7-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"7000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":15,\n" +
				"         \"name\":\"Module-ModuleName7-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"7000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":16,\n" +
				"         \"name\":\"Module-ModuleName8-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"8000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":17,\n" +
				"         \"name\":\"Module-ModuleName8-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"8000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":18,\n" +
				"         \"name\":\"Module-ModuleName9-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"9000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":19,\n" +
				"         \"name\":\"Module-ModuleName9-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"9000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":20,\n" +
				"         \"name\":\"Module-ModuleName10-EstimatedManualTestCases\",\n" +
				"         \"estimate\":\"10000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":21,\n" +
				"         \"name\":\"Module-ModuleName10-EstimatedRegressionAutomationTestCases\",\n" +
				"         \"estimate\":\"10000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":22,\n" +
				"         \"name\":\"APIAutomationTestCaseEstimate\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":23,\n" +
				"         \"name\":\"InProjectAutomationTestCaseEstimate\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":24,\n" +
				"         \"name\":\"TotalNumberOfTestCasesTaggedForMigrationExecution\",\n" +
				"         \"estimate\":\"1000\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":25,\n" +
				"         \"name\":\"TestPlanningEffort\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":26,\n" +
				"         \"name\":\"ManualTestCasesDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":27,\n" +
				"         \"name\":\"RegressionAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":28,\n" +
				"         \"name\":\"InProjectAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":29,\n" +
				"         \"name\":\"APIAutomationTestDesignEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":30,\n" +
				"         \"name\":\"ManualTestCases-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":31,\n" +
				"         \"name\":\"RegressionAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":32,\n" +
				"         \"name\":\"InProjectAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":33,\n" +
				"         \"name\":\"APIAutomation-ExecutionEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":34,\n" +
				"         \"name\":\"ManualTestCases-MaintenanceEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":35,\n" +
				"         \"name\":\"AutomationTestCases-MaintenanceEffort\",\n" +
				"         \"estimate\":\"100\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":36,\n" +
				"         \"name\":\"PlannedResourcesForTestCaseAuthoring\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":37,\n" +
				"         \"name\":\"PlannedResourcesForAutomationDesign\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":38,\n" +
				"         \"name\":\"PlannedResourcesForManualTestCaseExecution\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      },\n" +
				"      {\n" +
				"         \"id\":39,\n" +
				"         \"name\":\"PlannedResourcesForAutomationExecution\",\n" +
				"         \"estimate\":\"10\"\n" +
				"      }\n" +
				"   ]";


		System.out.println("The request payload is : " + form_Param_payload);

		RequestSpecification request = RestAssured.given();
		Response response = request
				.when()
				.contentType("application/json")
				.with()
				.body(form_Param_payload)
				.post();


		System.out.println(response.statusCode());
		System.out.println(response.body().prettyPrint());

		// Generate Quarterly metrics and verify
	}
}
