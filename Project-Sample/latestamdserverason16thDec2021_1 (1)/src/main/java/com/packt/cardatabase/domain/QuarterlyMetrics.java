package com.packt.cardatabase.domain;

import javax.persistence.*;

@Entity
public class QuarterlyMetrics {


    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private String revenueLeakage;
    private String numberOfBusinessMeetings;
    private int quarterNumber;
    private int yearNumber;

    private String revenueLeakageStatus;



    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project")
    private Project project;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public QuarterlyMetrics() {
    }

    QuarterlyMetrics(String revenueLeakage,
                     String numberOfBusinessMeetings,
                     int quarterNumber,
                     int yearNumber,
                     Project project) {

        this.revenueLeakage = revenueLeakage;
        this.numberOfBusinessMeetings = numberOfBusinessMeetings;
        this.quarterNumber = this.quarterNumber;
        this.yearNumber = yearNumber;
        this.project = project;

    }


    public void setRevenueLeakage(String revenueLeakage){
        this.revenueLeakage = revenueLeakage;
    }

    public void setRevenueLeakageStatus(String revenueLeakageStatus){
        this.revenueLeakageStatus = revenueLeakageStatus;
    }

    public String getRevenueLeakageStatus(){
        return this.revenueLeakageStatus;
    }

    public String getRevenueLeakage(){
        return revenueLeakage;
    }

    public static String getRevenueLeakageName(){
        return "Revenue Leakage";
    }


    public void setNumberOfBusinessMeetings(String numberOfBusinessMeetings){
        this.numberOfBusinessMeetings = numberOfBusinessMeetings;
    }

    public String getNumberOfBusinessMeetings(){
        return numberOfBusinessMeetings;
    }

    public static String getNumberOfBusinessMeetingsName(){
        return "Number Of Business Meetings held in the current quarter";
    }



    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public int getQuarterNumber() {
        return quarterNumber;
    }

    public String getQuarterNumberName() {
        return "Quarter";
    }

    public void setQuarterNumber(int quarterNumber) {
        this.quarterNumber = quarterNumber;
    }

    public int getYearNumber() {
        return yearNumber;
    }
    public String getYearNumberName() {
        return "Year";
    }
    public void setYearNumber(int yearNumber) {
        this.yearNumber = yearNumber;
    }

}
